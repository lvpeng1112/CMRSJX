import httpRequest from '@/utils/httpRequest'

// teamManager Page

export function getTeamManagerList (data) {
  return httpRequest({
    url: httpRequest.adornUrl('/teamNew/teamManager/list'),
    method: 'get',
    params: data
  })
}

export function deleteTeamData (data) {
  return httpRequest({
    url: httpRequest.adornUrl('/teamNew/teamManager/deleteTeamData'),
    method: 'get',
    params: data
  })
}

export function batchTeamData (data) {
  return httpRequest({
    url: httpRequest.adornUrl('/teamNew/teamManager/batchTeamData'),
    method: 'get',
    params: data
  })
}

// teamManager-add-or-update Page

export function shouwDepartmentList (data) {
  return httpRequest({
    url: httpRequest.adornUrl('/team/teamManager/shouwDepartmentList'),
    method: 'get',
    params: data
  })
}

export function shouwTeamEmployeeList (data) {
  return httpRequest({
    url: httpRequest.adornUrl('/teamNew/teamManager/shouwTeamEmployeeList'),
    method: 'get',
    params: data
  })
}

export function insertTeamData (data) {
  return httpRequest({
    url: httpRequest.adornUrl('/teamNew/teamManager/insertTeamData'),
    method: 'post',
    data: data
  })
}

export function updateTeamData (data) {
  return httpRequest({
    url: httpRequest.adornUrl('/teamNew/teamManager/updateTeamData'),
    method: 'post',
    data: data
  })
}

export function empleeList (data) {
  return httpRequest({
    url: httpRequest.adornUrl('/teamNew/teamManager/empleeList'),
    method: 'post',
    params: data
  })
}

// teamDetail Page

export function teamEmployeelist (data) {
  return httpRequest({
    url: httpRequest.adornUrl('/teamNew/teamManager//teamEmployeelist'),
    method: 'get',
    params: data
  })
}

// teamHistory Page

export function teamHislist (data) {
  return httpRequest({
    url: httpRequest.adornUrl('/teamNew/teamManager/teamHislist'),
    method: 'get',
    params: data
  })
}
