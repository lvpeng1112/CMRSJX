export default {
    namespaced: true,
    state: {
        rowData: {
            chTeamCode: '',
            setupDt: ''
        }
    },
    getters: {
        getRowData(state){
            return state.rowData
        }
    },
    mutations: {
        SET_ROWDATA(state, data){
            for(let key in data){
                state.rowData[key] = data[key]
            }
        },
        RESET_ROWDATA(state){
            state.rowData = {
                chLastModifier:'',
                chLastModify: '',
                chOrgId: '',
                chTeamCode: '',
                chTeamName: '',
                chdeptname: '',
                createUserId: '',
                leaderId: '',
                leaderName: '',
                setupDt: '',
                termStartDt: '',
                termEndDt: '',
                list:[
                    {
                    chEmpleeName: '',
                    }
                ],
                status: '',
                chTeamDesc: '',
            }
        }
    }
  }
  