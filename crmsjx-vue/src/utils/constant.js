
//用于定义全局变量

export const passwordKey = {
  modulus: "C4B43A2AFBFAA02E2FED998A9C1642AA2A20C4E97793AB247137E41D9B76E05F81D37753B0B5CC13B288A2EF76C9F4DC6293BE164E701DFC126D2C564F929552A4EBF3FE87D19D45C39CC9F9DAAFD5911081400456512D7CB72211C47C6871A03418A4AB55F6D4A13E0EC3F33647E573A0D1F51B9201FFA6E283F27681AF3F71",
  exponent: "10001",
}

export const hint = {
  timeout: "请求超时，请稍后再试！",
}
