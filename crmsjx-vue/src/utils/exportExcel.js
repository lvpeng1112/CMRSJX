// 模板文件下载
export default function  export2Excel (excelList,tHeader,filterVal,name){
  require.ensure([], () => {
    const { export_json_to_excel } = require('../vendor/Export2Excel');
    //标签对应的内容  是一个数组结构
    const list = excelList;
    const data = formatJson(filterVal, list);
    export_json_to_excel(tHeader, data, name,true);
  })
}

// 模板文件下载
export function formatJson (filterVal, jsonData){
  return jsonData.map(v => filterVal.map(j => v[j]))
}

export function  downloadExcel  (url, name, params) {
  window.location.href = window.SITE_CONFIG.baseUrl + name
}
export function  acceptExportData  (data) {
  if (!data) {
    return
  }
  let name = decodeURI(data.headers['filename'])
  if (window.navigator.msSaveOrOpenBlob) {
    // 兼容IE
    const blob = new Blob([data.data])
    navigator.msSaveBlob(blob, name)
  } else {
    //  chrome/firefox
    let objUrl = window.URL.createObjectURL(data.data)
    let link = document.createElement('a')
    link.style.display = 'none'
    link.href = objUrl
    link.setAttribute('download', name)
    document.body.appendChild(link)
    link.click()
  }
}
