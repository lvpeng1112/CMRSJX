﻿/**
 * 随机数生成函数
 * @param range 范围
 * @param length 字符长度
 * @returns 范围内的整数
 */
function random(range, length) {
  let random = Math.random();
  let num = Math.round(random * range) + "";
  if (num.length < length) {
    for (let i = num.length; i < length; i++) {
      num = "0" + num;
    }
  }
  else if (num.length > length) {
    num = num.substr(0, length);
  }
  return num;
}

// 文件 /*Barrett.js*/
// BarrettMu, a class for performing Barrett modular reduction computations in
// JavaScript.
//
// Requires BigInt.js.
//
// Copyright 2004-2005 David Shapiro.
//
// You may use, re-use, abuse, copy, and modify this code to your liking, but
// please keep this header.
//
// Thanks!
//
// Dave Shapiro
// dave@ohdave.com

function BarrettMu(m) {
  this.modulus = biCopy(m);
  this.k = biHighIndex(this.modulus) + 1;
  let b2k = new BigInt();
  b2k.digits[2 * this.k] = 1; // b2k = b^(2k)
  this.mu = biDivide(b2k, this.modulus);
  this.bkplus1 = new BigInt();
  this.bkplus1.digits[this.k + 1] = 1; // bkplus1 = b^(k+1)
  this.modulo = BarrettMu_modulo;
  this.multiplyMod = BarrettMu_multiplyMod;
  this.powMod = BarrettMu_powMod;
}

function BarrettMu_modulo(x) {
  let q1 = biDivideByRadixPower(x, this.k - 1);
  let q2 = biMultiply(q1, this.mu);
  let q3 = biDivideByRadixPower(q2, this.k + 1);
  let r1 = biModuloByRadixPower(x, this.k + 1);
  let r2term = biMultiply(q3, this.modulus);
  let r2 = biModuloByRadixPower(r2term, this.k + 1);
  let r = biSubtract(r1, r2);
  if (r.isNeg) {
    r = biAdd(r, this.bkplus1);
  }
  let rgtem = biCompare(r, this.modulus) >= 0;
  while (rgtem) {
    r = biSubtract(r, this.modulus);
    rgtem = biCompare(r, this.modulus) >= 0;
  }
  return r;
}

function BarrettMu_multiplyMod(x, y) {
  /*
    x = this.modulo(x);
    y = this.modulo(y);
    */
  let xy = biMultiply(x, y);
  return this.modulo(xy);
}

function BarrettMu_powMod(x, y) {
  let result = new BigInt();
  result.digits[0] = 1;
  let a = x;
  let k = y;
  while (true) {
    if ((k.digits[0] & 1) != 0) result = this.multiplyMod(result, a);
    k = biShiftRight(k, 1);
    if (k.digits[0] == 0 && biHighIndex(k) == 0) break;
    a = this.multiplyMod(a, a);
  }
  return result;
}

//BigInt.js
// BigInt, a suite of routines for performing multiple-precision arithmetic in
// JavaScript.
//
// Copyright 1998-2005 David Shapiro.
//
// You may use, re-use, abuse,
// copy, and modify this code to your liking, but please keep this header.
// Thanks!
//
// Dave Shapiro
// dave@ohdave.com

// IMPORTANT THING: Be sure to set maxDigits according to your precision
// needs. Use the setMaxDigits() function to do this. See comments below.
//
// Tweaked by Ian Bunning
// Alterations:
// Fix bug in function biFromHex(s) to allow
// parsing of strings of length != 0 (mod 4)

// Changes made by Dave Shapiro as of 12/30/2004:
//
// The BigInt() constructor doesn't take a string anymore. If you want to
// create a BigInt from a string, use biFromDecimal() for base-10
// representations, biFromHex() for base-16 representations, or
// biFromString() for base-2-to-36 representations.
//
// biFromArray() has been removed. Use biCopy() instead, passing a BigInt
// instead of an array.
//
// The BigInt() constructor now only constructs a zeroed-out array.
// Alternatively, if you pass <true>, it won't construct any array. See the
// biCopy() method for an example of this.
//
// Be sure to set maxDigits depending on your precision needs. The default
// zeroed-out array ZERO_ARRAY is constructed inside the setMaxDigits()
// function. So use this function to set the letiable. DON'T JUST SET THE
// VALUE. USE THE FUNCTION.
//
// ZERO_ARRAY exists to hopefully speed up construction of BigInts(). By
// precalculating the zero array, we can just use slice(0) to make copies of
// it. Presumably this calls faster native code, as opposed to setting the
// elements one at a time. I have not done any timing tests to verify this
// claim.

// Max number = 10^16 - 2 = 9999999999999998;
//               2^53     = 9007199254740992;

let biRadixBase = 2;
let biRadixBits = 16;
let bitsPerDigit = biRadixBits;
let biRadix = 1 << 16; // = 2^16 = 65536
let biHalfRadix = biRadix >>> 1;
let biRadixSquared = biRadix * biRadix;
let maxDigitVal = biRadix - 1;
let maxInteger = 9999999999999998;

// maxDigits:
// Change this to accommodate your largest number size. Use setMaxDigits()
// to change it!
//
// In general, if you're working with numbers of size N bits, you'll need 2*N
// bits of storage. Each digit holds 16 bits. So, a 1024-bit key will need
//
// 1024 * 2 / 16 = 128 digits of storage.
//

let maxDigits;
let ZERO_ARRAY;
let bigZero, bigOne;

function setMaxDigits(value) {
  maxDigits = value;
  ZERO_ARRAY = new Array(maxDigits);
  for (let iza = 0; iza < ZERO_ARRAY.length; iza++) ZERO_ARRAY[iza] = 0;
  bigZero = new BigInt();
  bigOne = new BigInt();
  bigOne.digits[0] = 1;
}

setMaxDigits(20);

// The maximum number of digits in base 10 you can convert to an
// integer without JavaScript throwing up on you.
let dpl10 = 15;
// lr10 = 10 ^ dpl10
let lr10 = biFromNumber(1000000000000000);

function BigInt(flag) {
  if (typeof flag == "boolean" && flag == true) {
    this.digits = null;
  }
  else {
    this.digits = ZERO_ARRAY.slice(0);
  }
  this.isNeg = false;
}

function biFromDecimal(s) {
  let isNeg = s.charAt(0) == '-';
  let i = isNeg ? 1 : 0;
  let result;
  // Skip leading zeros.
  while (i < s.length && s.charAt(i) == '0') ++i;
  if (i == s.length) {
    result = new BigInt();
  }
  else {
    let digitCount = s.length - i;
    let fgl = digitCount % dpl10;
    if (fgl == 0) fgl = dpl10;
    result = biFromNumber(Number(s.substr(i, fgl)));
    i += fgl;
    while (i < s.length) {
      result = biAdd(biMultiply(result, lr10),
        biFromNumber(Number(s.substr(i, dpl10))));
      i += dpl10;
    }
    result.isNeg = isNeg;
  }
  return result;
}

function biCopy(bi) {
  let result = new BigInt(true);
  result.digits = bi.digits.slice(0);
  result.isNeg = bi.isNeg;
  return result;
}

function biFromNumber(i) {
  let result = new BigInt();
  result.isNeg = i < 0;
  i = Math.abs(i);
  let j = 0;
  while (i > 0) {
    result.digits[j++] = i & maxDigitVal;
    i = Math.floor(i / biRadix);
  }
  return result;
}

function reverseStr(s) {
  let result = "";
  for (let i = s.length - 1; i > -1; --i) {
    result += s.charAt(i);
  }
  return result;
}

let hexatrigesimalToChar = new Array(
  '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
  'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
  'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
  'U', 'V', 'W', 'X', 'Y', 'Z'
);

function biToString(x, radix)
// 2 <= radix <= 36
{
  let b = new BigInt();
  b.digits[0] = radix;
  let qr = biDivideModulo(x, b);
  let result = hexatrigesimalToChar[qr[1].digits[0]];
  while (biCompare(qr[0], bigZero) == 1) {
    qr = biDivideModulo(qr[0], b);
    digit = qr[1].digits[0];
    result += hexatrigesimalToChar[qr[1].digits[0]];
  }
  return (x.isNeg ? "-" : "") + reverseStr(result);
}

function biToDecimal(x) {
  let b = new BigInt();
  b.digits[0] = 10;
  let qr = biDivideModulo(x, b);
  let result = String(qr[1].digits[0]);
  while (biCompare(qr[0], bigZero) == 1) {
    qr = biDivideModulo(qr[0], b);
    result += String(qr[1].digits[0]);
  }
  return (x.isNeg ? "-" : "") + reverseStr(result);
}

let hexToChar = new Array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
  'A', 'B', 'C', 'D', 'E', 'F');

function digitToHex(n) {
  let mask = 0xf;
  let result = "";
  for (let i = 0; i < 4; ++i) {
    result += hexToChar[n & mask];
    n >>>= 4;
  }
  return reverseStr(result);
}

function biToHex(x) {
  let result = "";
  let n = biHighIndex(x);
  for (let i = biHighIndex(x); i > -1; --i) {
    result += digitToHex(x.digits[i]);
  }
  return result;
}

function charToHex(c) {
  let ZERO = 48;
  let NINE = ZERO + 9;
  let littleA = 97;
  let littleZ = littleA + 25;
  let bigA = 65;
  let bigZ = 65 + 25;
  let result;

  if (c >= ZERO && c <= NINE) {
    result = c - ZERO;
  } else if (c >= bigA && c <= bigZ) {
    result = 10 + c - bigA;
  } else if (c >= littleA && c <= littleZ) {
    result = 10 + c - littleA;
  } else {
    result = 0;
  }
  return result;
}

function hexToDigit(s) {
  let result = 0;
  let sl = Math.min(s.length, 4);
  for (let i = 0; i < sl; ++i) {
    result <<= 4;
    result |= charToHex(s.charCodeAt(i))
  }
  return result;
}

function biFromHex(s) {
  let result = new BigInt();
  let sl = s.length;
  for (let i = sl, j = 0; i > 0; i -= 4, ++j) {
    result.digits[j] = hexToDigit(s.substr(Math.max(i - 4, 0), Math.min(i, 4)));
  }
  return result;
}

function biFromString(s, radix) {
  let isNeg = s.charAt(0) == '-';
  let istop = isNeg ? 1 : 0;
  let result = new BigInt();
  let place = new BigInt();
  place.digits[0] = 1; // radix^0
  for (let i = s.length - 1; i >= istop; i--) {
    let c = s.charCodeAt(i);
    let digit = charToHex(c);
    let biDigit = biMultiplyDigit(place, digit);
    result = biAdd(result, biDigit);
    place = biMultiplyDigit(place, radix);
  }
  result.isNeg = isNeg;
  return result;
}

function biDump(b) {
  return (b.isNeg ? "-" : "") + b.digits.join(" ");
}

function biAdd(x, y) {
  let result;

  if (x.isNeg != y.isNeg) {
    y.isNeg = !y.isNeg;
    result = biSubtract(x, y);
    y.isNeg = !y.isNeg;
  }
  else {
    result = new BigInt();
    let c = 0;
    let n;
    for (let i = 0; i < x.digits.length; ++i) {
      n = x.digits[i] + y.digits[i] + c;
      result.digits[i] = n % biRadix;
      c = Number(n >= biRadix);
    }
    result.isNeg = x.isNeg;
  }
  return result;
}

function biSubtract(x, y) {
  let result;
  if (x.isNeg != y.isNeg) {
    y.isNeg = !y.isNeg;
    result = biAdd(x, y);
    y.isNeg = !y.isNeg;
  } else {
    result = new BigInt();
    let n, c;
    c = 0;
    for (let i = 0; i < x.digits.length; ++i) {
      n = x.digits[i] - y.digits[i] + c;
      result.digits[i] = n % biRadix;
      // Stupid non-conforming modulus operation.
      if (result.digits[i] < 0) result.digits[i] += biRadix;
      c = 0 - Number(n < 0);
    }
    // Fix up the negative sign, if any.
    if (c == -1) {
      c = 0;
      for (let i = 0; i < x.digits.length; ++i) {
        n = 0 - result.digits[i] + c;
        result.digits[i] = n % biRadix;
        // Stupid non-conforming modulus operation.
        if (result.digits[i] < 0) result.digits[i] += biRadix;
        c = 0 - Number(n < 0);
      }
      // Result is opposite sign of arguments.
      result.isNeg = !x.isNeg;
    } else {
      // Result is same sign.
      result.isNeg = x.isNeg;
    }
  }
  return result;
}


function biHighIndex(x) {
  let result = x.digits.length - 1;
  while (result > 0 && x.digits[result] == 0) --result;
  return result;
}

function biNumBits(x) {
  let n = biHighIndex(x);
  let d = x.digits[n];
  let m = (n + 1) * bitsPerDigit;
  let result;
  for (result = m; result > m - bitsPerDigit; --result) {
    if ((d & 0x8000) != 0) break;
    d <<= 1;
  }
  return result;
}

function biMultiply(x, y) {
  let result = new BigInt();
  let c;
  let n = biHighIndex(x);
  let t = biHighIndex(y);
  let u, uv, k;

  for (let i = 0; i <= t; ++i) {
    c = 0;
    k = i;
    for (let j = 0; j <= n; ++j, ++k) {
      uv = result.digits[k] + x.digits[j] * y.digits[i] + c;
      result.digits[k] = uv & maxDigitVal;
      c = uv >>> biRadixBits;
      //c = Math.floor(uv / biRadix);
    }
    result.digits[i + n + 1] = c;
  }
  // Someone give me a logical xor, please.
  result.isNeg = x.isNeg != y.isNeg;
  return result;
}

function biMultiplyDigit(x, y) {
  let n, c, uv;

  let result = new BigInt();
  n = biHighIndex(x);
  c = 0;
  for (let j = 0; j <= n; ++j) {
    uv = result.digits[j] + x.digits[j] * y + c;
    result.digits[j] = uv & maxDigitVal;
    c = uv >>> biRadixBits;
    //c = Math.floor(uv / biRadix);
  }
  result.digits[1 + n] = c;
  return result;
}

function arrayCopy(src, srcStart, dest, destStart, n) {
  let m = Math.min(srcStart + n, src.length);
  for (let i = srcStart, j = destStart; i < m; ++i, ++j) {
    dest[j] = src[i];
  }
}

let highBitMasks = new Array(0x0000, 0x8000, 0xC000, 0xE000, 0xF000, 0xF800,
  0xFC00, 0xFE00, 0xFF00, 0xFF80, 0xFFC0, 0xFFE0,
  0xFFF0, 0xFFF8, 0xFFFC, 0xFFFE, 0xFFFF);

function biShiftLeft(x, n) {
  let digitCount = Math.floor(n / bitsPerDigit);
  let result = new BigInt();
  arrayCopy(x.digits, 0, result.digits, digitCount,
    result.digits.length - digitCount);
  let bits = n % bitsPerDigit;
  let rightBits = bitsPerDigit - bits;
  for (var i = result.digits.length - 1, i1 = i - 1; i > 0; --i, --i1) {
    result.digits[i] = ((result.digits[i] << bits) & maxDigitVal) |
      ((result.digits[i1] & highBitMasks[bits]) >>>
        (rightBits));
  }
  result.digits[0] = ((result.digits[i] << bits) & maxDigitVal);
  result.isNeg = x.isNeg;
  return result;
}

let lowBitMasks = new Array(0x0000, 0x0001, 0x0003, 0x0007, 0x000F, 0x001F,
  0x003F, 0x007F, 0x00FF, 0x01FF, 0x03FF, 0x07FF,
  0x0FFF, 0x1FFF, 0x3FFF, 0x7FFF, 0xFFFF);

function biShiftRight(x, n) {
  let digitCount = Math.floor(n / bitsPerDigit);
  let result = new BigInt();
  arrayCopy(x.digits, digitCount, result.digits, 0,
    x.digits.length - digitCount);
  let bits = n % bitsPerDigit;
  let leftBits = bitsPerDigit - bits;
  for (let i = 0, i1 = i + 1; i < result.digits.length - 1; ++i, ++i1) {
    result.digits[i] = (result.digits[i] >>> bits) |
      ((result.digits[i1] & lowBitMasks[bits]) << leftBits);
  }
  result.digits[result.digits.length - 1] >>>= bits;
  result.isNeg = x.isNeg;
  return result;
}

function biMultiplyByRadixPower(x, n) {
  let result = new BigInt();
  arrayCopy(x.digits, 0, result.digits, n, result.digits.length - n);
  return result;
}

function biDivideByRadixPower(x, n) {
  let result = new BigInt();
  arrayCopy(x.digits, n, result.digits, 0, result.digits.length - n);
  return result;
}

function biModuloByRadixPower(x, n) {
  let result = new BigInt();
  arrayCopy(x.digits, 0, result.digits, 0, n);
  return result;
}

function biCompare(x, y) {
  if (x.isNeg != y.isNeg) {
    return 1 - 2 * Number(x.isNeg);
  }
  for (let i = x.digits.length - 1; i >= 0; --i) {
    if (x.digits[i] != y.digits[i]) {
      if (x.isNeg) {
        return 1 - 2 * Number(x.digits[i] > y.digits[i]);
      } else {
        return 1 - 2 * Number(x.digits[i] < y.digits[i]);
      }
    }
  }
  return 0;
}

function biDivideModulo(x, y) {
  let nb = biNumBits(x);
  let tb = biNumBits(y);
  let origYIsNeg = y.isNeg;
  let q, r;
  if (nb < tb) {
    // |x| < |y|
    if (x.isNeg) {
      q = biCopy(bigOne);
      q.isNeg = !y.isNeg;
      x.isNeg = false;
      y.isNeg = false;
      r = biSubtract(y, x);
      // Restore signs, 'cause they're references.
      x.isNeg = true;
      y.isNeg = origYIsNeg;
    } else {
      q = new BigInt();
      r = biCopy(x);
    }
    return new Array(q, r);
  }

  q = new BigInt();
  r = x;

  // Normalize Y.
  let t = Math.ceil(tb / bitsPerDigit) - 1;
  let lambda = 0;
  while (y.digits[t] < biHalfRadix) {
    y = biShiftLeft(y, 1);
    ++lambda;
    ++tb;
    t = Math.ceil(tb / bitsPerDigit) - 1;
  }
  // Shift r over to keep the quotient constant. We'll shift the
  // remainder back at the end.
  r = biShiftLeft(r, lambda);
  nb += lambda; // Update the bit count for x.
  let n = Math.ceil(nb / bitsPerDigit) - 1;

  let b = biMultiplyByRadixPower(y, n - t);
  while (biCompare(r, b) != -1) {
    ++q.digits[n - t];
    r = biSubtract(r, b);
  }
  for (let i = n; i > t; --i) {
    let ri = (i >= r.digits.length) ? 0 : r.digits[i];
    let ri1 = (i - 1 >= r.digits.length) ? 0 : r.digits[i - 1];
    let ri2 = (i - 2 >= r.digits.length) ? 0 : r.digits[i - 2];
    let yt = (t >= y.digits.length) ? 0 : y.digits[t];
    let yt1 = (t - 1 >= y.digits.length) ? 0 : y.digits[t - 1];
    if (ri == yt) {
      q.digits[i - t - 1] = maxDigitVal;
    } else {
      q.digits[i - t - 1] = Math.floor((ri * biRadix + ri1) / yt);
    }

    let c1 = q.digits[i - t - 1] * ((yt * biRadix) + yt1);
    let c2 = (ri * biRadixSquared) + ((ri1 * biRadix) + ri2);
    while (c1 > c2) {
      --q.digits[i - t - 1];
      c1 = q.digits[i - t - 1] * ((yt * biRadix) | yt1);
      c2 = (ri * biRadix * biRadix) + ((ri1 * biRadix) + ri2);
    }

    b = biMultiplyByRadixPower(y, i - t - 1);
    r = biSubtract(r, biMultiplyDigit(b, q.digits[i - t - 1]));
    if (r.isNeg) {
      r = biAdd(r, b);
      --q.digits[i - t - 1];
    }
  }
  r = biShiftRight(r, lambda);
  // Fiddle with the signs and stuff to make sure that 0 <= r < y.
  q.isNeg = x.isNeg != origYIsNeg;
  if (x.isNeg) {
    if (origYIsNeg) {
      q = biAdd(q, bigOne);
    } else {
      q = biSubtract(q, bigOne);
    }
    y = biShiftRight(y, lambda);
    r = biSubtract(y, r);
  }
  // Check for the unbelievably stupid degenerate case of r == -0.
  if (r.digits[0] == 0 && biHighIndex(r) == 0) r.isNeg = false;

  return new Array(q, r);
}

function biDivide(x, y) {
  return biDivideModulo(x, y)[0];
}

function biModulo(x, y) {
  return biDivideModulo(x, y)[1];
}

function biMultiplyMod(x, y, m) {
  return biModulo(biMultiply(x, y), m);
}

function biPow(x, y) {
  let result = bigOne;
  let a = x;
  while (true) {
    if ((y & 1) != 0) result = biMultiply(result, a);
    y >>= 1;
    if (y == 0) break;
    a = biMultiply(a, a);
  }
  return result;
}

function biPowMod(x, y, m) {
  let result = bigOne;
  let a = x;
  let k = y;
  while (true) {
    if ((k.digits[0] & 1) != 0) result = biMultiplyMod(result, a, m);
    k = biShiftRight(k, 1);
    if (k.digits[0] == 0 && biHighIndex(k) == 0) break;
    a = biMultiplyMod(a, a, m);
  }
  return result;
}

// RSA, a suite of routines for performing RSA public-key computations in
// JavaScript.
//
// Requires BigInt.js and Barrett.js.
//
// Copyright 1998-2005 David Shapiro.
//
// You may use, re-use, abuse, copy, and modify this code to your liking, but
// please keep this header.
//
// Thanks!
//
// Dave Shapiro
// dave@ohdave.com

function RSAKeyPair(encryptionExponent, decryptionExponent, modulus) {
  this.e = biFromHex(encryptionExponent);
  this.d = biFromHex(decryptionExponent);
  this.m = biFromHex(modulus);
  // We can do two bytes per digit, so
  // chunkSize = 2 * (number of digits in modulus - 1).
  // Since biHighIndex returns the high index, not the number of digits, 1 has
  // already been subtracted.
  this.chunkSize = 2 * (biHighIndex(this.m) + 1);
  this.radix = 16;
  this.barrett = new BarrettMu(this.m);
}

function twoDigit(n) {
  return (n < 10 ? "0" : "") + String(n);
}

function encryptedString(key, s, padding)
// Altered by Rob Saunders (rob@robsaunders.net). New routine pads the
// string after it has been converted to an array. This fixes an
// incompatibility with Flash MX's ActionScript.
{
  let a = new Array();
  let ua = new Array();
  let sl = s.length;
  let i = 0;
  while (i < sl) {
    ua[i] = s.charCodeAt(i);
    i++;
  }

  while (ua.length % key.chunkSize != 0) {
    if (padding)
      ua[i++] = 70;
    else
      ua[i++] = 0;
  }
  for (let z = 0; z < ua.length; z++) {
    a[z] = ua[ua.length - 1 - z];
  }
  let al = a.length;
  let result = "";
  let j, k, block;
  for (i = 0; i < al; i += key.chunkSize) {
    block = new BigInt();
    j = 0;
    for (k = i; k < i + key.chunkSize; ++j) {
      block.digits[j] = a[k++];
      block.digits[j] += a[k++] << 8;
    }
    let crypt = key.barrett.powMod(block, key.e);
    let text = key.radix == 16 ? biToHex(crypt) : biToString(crypt, key.radix);
    result = text + " " + result;
  }
  return result.substring(0, result.length - 1); // Remove last space.
}

function decryptedString(key, s) {
  let blocks = s.split(" ");
  let result = "";
  let i, j, block;
  for (i = 0; i < blocks.length; ++i) {
    let bi;
    if (key.radix == 16) {
      bi = biFromHex(blocks[i]);
    }
    else {
      bi = biFromString(blocks[i], key.radix);
    }
    block = key.barrett.powMod(bi, key.d);
    for (j = 0; j <= biHighIndex(block); ++j) {
      result += String.fromCharCode(block.digits[j] & 255,
        block.digits[j] >> 8);
    }
  }
  // Remove trailing null, if any.
  if (result.charCodeAt(result.length - 1) == 0) {
    result = result.substring(0, result.length - 1);
  }
  return result;
}

function RSAEnc(pin, publicKeyN, publicKeyE, maxDigit, padding) {
  setMaxDigits(maxDigit);//131,259 => publicKeyN的十六进制位数/2+3
  let key = new RSAKeyPair(publicKeyE, "", publicKeyN);
  let result = encryptedString(key, pin, padding);
  return result;
}

let encryptPassword = function (password) {
  let module = "9E7F6DED1B4D50BCDA5529E9FB4112ABE30089514FB378B981BBDD74EEA5640FD4F777CD6A23775282702A5D6F5D5BC4B929E1DC9CC59ABC7B7C7A2730C49E03E16DC107E2F841CC98181076DA6CAC23C22103F46A95002246F30F1697FE93F260A8A672FE7FD11F7BF0E178531883645263ADD0FEC28164ED52050EC3B49013";
  let exponent = "10001";
  password = "06" + password;
  let pinEnc = RSAEnc(password, module, exponent, 131);
  module = "B3A4D142D469AA5EB7A23C3C2242FBF58AF9111739B867D1559115728D51A6969BEFDD139B1CCB6CB024128B3C3CEDC9A3713127EA2EF03B6438819C63F2E889A2AA4FA764132DAC234F641C67F61E5D29C46B2F7646B62BF7B1D05278B7DD91AEF6C7DF1790625F3157A1868F0C415E91603DB93EB81E61AB889D98603AD9F7DFF1127D5ED95B3E6B9C998B74D037D47BDE7829EE9F46BEA74062ED004FD9EE0B3257C7729532328F45AD47DD820DDD610AF1C4C16CC7FCDAD3A5B99A72FE2420543CA8ABE34C8CD46A37F4D18A1CEB910DC593E67B255B0EFAEED63F44015BB7F017B3D463913249B611CEFF939681A7CE2558D14715A886B9940277287263";
  exponent = "10001";
  pinEnc = random(999999, 6) + pinEnc;
  pinEnc = RSAEnc(pinEnc, module, exponent, 259, true);
  return pinEnc;
};

let encryptInfo = function (info) {
  let module = "B3A4D142D469AA5EB7A23C3C2242FBF58AF9111739B867D1559115728D51A6969BEFDD139B1CCB6CB024128B3C3CEDC9A3713127EA2EF03B6438819C63F2E889A2AA4FA764132DAC234F641C67F61E5D29C46B2F7646B62BF7B1D05278B7DD91AEF6C7DF1790625F3157A1868F0C415E91603DB93EB81E61AB889D98603AD9F7DFF1127D5ED95B3E6B9C998B74D037D47BDE7829EE9F46BEA74062ED004FD9EE0B3257C7729532328F45AD47DD820DDD610AF1C4C16CC7FCDAD3A5B99A72FE2420543CA8ABE34C8CD46A37F4D18A1CEB910DC593E67B255B0EFAEED63F44015BB7F017B3D463913249B611CEFF939681A7CE2558D14715A886B9940277287263";
  let exponent = "10001";
  let pinEnc = RSAEnc(info, module, exponent, 259);
  return pinEnc;
};

export {
  encryptInfo,
  encryptPassword,
  RSAEnc,
  random
}
