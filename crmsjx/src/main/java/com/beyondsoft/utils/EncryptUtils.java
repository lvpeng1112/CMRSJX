package com.beyondsoft.utils;

import sun.misc.CEFormatException;
import sun.misc.CEStreamExhausted;
import sun.misc.CharacterDecoder;
import sun.misc.CharacterEncoder;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PushbackInputStream;
import java.security.spec.AlgorithmParameterSpec;

/*******************************************************************************
 * 与IPHONE android匹配的3DES加密算法
 *
 * @author lijian
 * @since 2012-02-23
 */
public class EncryptUtils {
	// 默认 初始化密钥
	private String algorithm = "DESede/CBC/PKCS5Padding";

	private Cipher enCipher;
	private Cipher deCipher;
	private CUPDBASE64Encoder encoder;
	private CUPDBASE64Decoder decoder;

	/**
	 * 初始化加密对象
	 *
	 * @throws Exception
	 */
	private EncryptUtils(String initKey, String initVec) throws Exception {
		if (initKey == null || initVec == null) {
			throw new NullPointerException("key or vec is null!");
		}
		if (initKey.length() != 24 || initVec.length() != 8) {
			throw new NullPointerException("key or vec's length is not correct");
		}
		initBase64();
		initCipher(initKey.getBytes(), initVec.getBytes());
	}

	public static EncryptUtils getInstance(String initKey, String initVec)
			throws Exception {
		return new EncryptUtils(initKey, initVec);
	}

	private void initCipher(byte[] secKey, byte[] secIv) throws Exception {
		// 生成加密密钥
		SecretKey key = new SecretKeySpec(secKey, "DESede");
		// 创建初始化向量对象
		IvParameterSpec iv = new IvParameterSpec(secIv);
		AlgorithmParameterSpec paramSpec = iv;
		// 为加密算法指定填充方式，创建加密会话对象
		enCipher = Cipher.getInstance(algorithm);
		// 为加密算法指定填充方式，创建解密会话对象
		deCipher = Cipher.getInstance(algorithm);
		// 初始化加解密会话对象
		enCipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
		deCipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
		encoder = new CUPDBASE64Encoder();
		decoder = new CUPDBASE64Decoder();
	}

	/**
	 * 加密数据
	 *
	 * @param data
	 *            待加密二进制数据
	 * @return 经BASE64编码过的加密数据
	 * @throws Exception
	 */
	private String encrypt(byte[] data) throws Exception {
		return encoder.encode(enCipher.doFinal(data));
	}

	/**
	 * 加密字符串
	 *
	 * @param strIn
	 *            需加密的字符串
	 * @return 加密后的字符串
	 * @throws Exception
	 */
	public String encrypt(String strIn) throws Exception {
		return encrypt(strIn.getBytes("UTF-8"));
	}

	/**
	 * 解密数据
	 *
	 * @param data
	 *            待解密字符串（经过BASE64编码）
	 * @return 解密后的二进制数据
	 * @throws Exception
	 */
	public byte[] decrypt2Byte(String data) throws Exception {
		return deCipher.doFinal(decoder.decodeBuffer(data));
	}

	/**
	 * 解密字符串
	 *
	 * @param strIn
	 *            构建字符串编码
	 * @return
	 * @throws Exception
	 */
	public String decrypt(String strIn) throws Exception {
		return new String(decrypt2Byte(strIn), "UTF-8");
	}

	public static void main(String[] args) throws Exception {
		String s = EncryptUtils.getInstance("CupDataMobileBankESystem",
				"CUPD37MD").encrypt("123456");
		System.out.println(s);
		System.out.println(EncryptUtils.getInstance("CupDataMobileBankESystem",
				"CUPD37MD").decrypt(s));
	}

	private char pem_array[] = {
			// 0 1 2 3 4 5 6 7
			'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', // 0
			'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', // 1
			'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', // 2
			'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', // 3
			'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', // 4
			'o', 'p', 'q', 'r', 's', 't', 'u', 'v', // 5
			'w', 'x', 'y', 'z', '0', '1', '2', '3', // 6
			'4', '5', '6', '7', '8', '9', '$', '/' // 7
	};

	private byte pem_convert_array[] = new byte[256];

	private void initBase64() {
		for (int i = 0; i < 255; i++) {
			pem_convert_array[i] = -1;
		}
		for (int i = 0; i < pem_array.length; i++) {
			pem_convert_array[pem_array[i]] = (byte) i;
		}
	}

	private class CUPDBASE64Encoder extends CharacterEncoder {

		/** this class encodes three bytes per atom. */
		protected int bytesPerAtom() {
			return (3);
		}

		/**
		 * this class encodes 57 bytes per line. This results in a maximum of 57/3 *
		 * 4 or 76 characters per output line. Not counting the line termination.
		 */
		protected int bytesPerLine() {
			return (57);
		}

		/** This array maps the characters to their 6 bit values */

		/**
		 * encodeAtom - Take three bytes of input and encode it as 4 printable
		 * characters. Note that if the length in len is less than three is encodes
		 * either one or two '=' signs to indicate padding characters.
		 */
		protected void encodeAtom(OutputStream outStream, byte data[],
								  int offset, int len) throws IOException {
			byte a, b, c;
			if (len == 1) {
				a = data[offset];
				b = 0;
				c = 0;
				outStream.write(pem_array[(a >>> 2) & 0x3F]);
				outStream
						.write(pem_array[((a << 4) & 0x30) + ((b >>> 4) & 0xf)]);
				outStream.write('=');
				outStream.write('=');
			} else if (len == 2) {
				a = data[offset];
				b = data[offset + 1];
				c = 0;
				outStream.write(pem_array[(a >>> 2) & 0x3F]);
				outStream
						.write(pem_array[((a << 4) & 0x30) + ((b >>> 4) & 0xf)]);
				outStream
						.write(pem_array[((b << 2) & 0x3c) + ((c >>> 6) & 0x3)]);
				outStream.write('=');
			} else {
				a = data[offset];
				b = data[offset + 1];
				c = data[offset + 2];
				outStream.write(pem_array[(a >>> 2) & 0x3F]);
				outStream
						.write(pem_array[((a << 4) & 0x30) + ((b >>> 4) & 0xf)]);
				outStream
						.write(pem_array[((b << 2) & 0x3c) + ((c >>> 6) & 0x3)]);
				outStream.write(pem_array[c & 0x3F]);
			}
		}
	}

	class CUPDBASE64Decoder extends CharacterDecoder {

		/** This class has 4 bytes per atom */
		protected int bytesPerAtom() {
			return (4);
		}

		/** Any multiple of 4 will do, 72 might be common */
		protected int bytesPerLine() {
			return (72);
		}

		byte decode_buffer[] = new byte[4];

		/**
		 103        * Decode one BASE64 atom into 1, 2, or 3 bytes of data.
		 104        */
		protected void decodeAtom(PushbackInputStream inStream,
								  OutputStream outStream, int rem) throws IOException {
			int i;
			byte a = -1, b = -1, c = -1, d = -1;

			if (rem < 2) {
				throw new CEFormatException(
						"BASE64Decoder: Not enough bytes for an atom.");
			}
			do {
				i = inStream.read();
				if (i == -1) {
					throw new CEStreamExhausted();
				}
			} while (i == '\n' || i == '\r');
			decode_buffer[0] = (byte) i;

			i = readFully(inStream, decode_buffer, 1, rem - 1);
			if (i == -1) {
				throw new CEStreamExhausted();
			}

			if (rem > 3 && decode_buffer[3] == '=') {
				rem = 3;
			}
			if (rem > 2 && decode_buffer[2] == '=') {
				rem = 2;
			}
			switch (rem) {
				case 4:
					d = pem_convert_array[decode_buffer[3] & 0xff];
					// NOBREAK
				case 3:
					c = pem_convert_array[decode_buffer[2] & 0xff];
					// NOBREAK
				case 2:
					b = pem_convert_array[decode_buffer[1] & 0xff];
					a = pem_convert_array[decode_buffer[0] & 0xff];
					break;
			}

			switch (rem) {
				case 2:
					outStream.write((byte) (((a << 2) & 0xfc) | ((b >>> 4) & 3)));
					break;
				case 3:
					outStream.write((byte) (((a << 2) & 0xfc) | ((b >>> 4) & 3)));
					outStream.write((byte) (((b << 4) & 0xf0) | ((c >>> 2) & 0xf)));
					break;
				case 4:
					outStream.write((byte) (((a << 2) & 0xfc) | ((b >>> 4) & 3)));
					outStream.write((byte) (((b << 4) & 0xf0) | ((c >>> 2) & 0xf)));
					outStream.write((byte) (((c << 6) & 0xc0) | (d & 0x3f)));
					break;
			}
			return;
		}
	}
}