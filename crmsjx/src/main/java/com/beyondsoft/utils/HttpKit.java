package com.beyondsoft.utils;

import cn.hutool.core.codec.Base64Encoder;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 此类为Http Client工具类及调试使用
 * 禁止在此类开发任何业务层代码
 * 只能进行http方法的补充
 *
 * @Author zhizhou
 */
@Slf4j
public class HttpKit {

    private HttpKit() {
    }

    private static final String GET = "GET";
    private static final String POST = "POST";
    private static final String CHARSET = "UTF-8";
    private static ThreadPoolExecutor executor;

    public static void main(String[] args) {
        //待发送的请求数据
//		Map<String, String> reqData = new HashMap<String, String>();
//		reqData.put("bank", "6311");
//		reqData.put("trchNum", "");
//		reqData.put("cardNbr", "6251910000000100");
//
//		String jsonStr = JSonKit.toJSon(reqData);
//		System.out.println(jsonStr);
//		String reqUrl = "http://onlineuat.cupdata.com:50001/CSIS/logistics/card";
// 		String strPost = HttpKit.postJson(reqUrl, null, jsonStr, null);
//		System.out.println(strPost);
//
//		strPost = strPost.substring(1, strPost.length()-1);
//		Map<String,String> map = new HashMap<String,String>();
//		map = JSonKit.readValue(strPost, Map.class);
//		String context = map.get("context");
//		System.out.println(context);
//
//		String[] jsonArray = context.split("\\;");
//		for(String temp : jsonArray){
//			System.out.println(temp);
//		}


        String appid = "wxc49e92c53a5bd3f2";
        String secret = "7ff464b13d19aa247bf0153839be11cf";


        //	String appid = "wx601be783cbccc6ab";
        //	String secret = "43e7702abe71c10593c1a972a3a9558d";
        String url = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=";

        Map<String, String> map = new HashMap<String, String>();
        map.put("scene", "source=web&flag=111111111101");
//		String scene = JSON.toJSONString(map);



        //	String url = "http://192.168.120.47:8080/uruleService/cxf/rest/ccam/insertUrule";
//		String json = "{"
//	   			 +"\"bankid\":\"8479\""
//	   			 +",\"opttype\":\"1\""
//	   			 +",\"jobid\":\"1001\""
//	   			 +",\"caltype\":\"1\""
//	   			 +",\"starttime\":\"20180105\""
//	   			 +",\"endtime\":\"20180305\""
//	   			 +",\"rules\":[{\"ruleid\":\"rule0001\",\"parameter\":{\"p1\":\"1\",\"p2\":\"2\"}}"
//	   			 +",{\"ruleid\":\"rule0002\",\"parameter\":{\"p1\":\"1\",\"p2\":\"2\"}}"
//	   			 +",{\"ruleid\":\"rule0003\",\"parameter\":{\"p1\":\"1\",\"p2\":\"2\"}}"
//	   			 +"]"
//	   			 +"}";
//
//		String res = postJson(url,null, json, null);
//		System.out.println(res);
    }


    //单例模式获取线程池
    private static ThreadPoolExecutor getThreadPool() {
        if (executor == null) {
            synchronized (HttpKit.class) {
                if (executor == null) {
                    executor = new ThreadPoolExecutor(8, 16, 200, TimeUnit.MILLISECONDS,
                            new ArrayBlockingQueue<Runnable>(5), new ThreadPoolExecutor.CallerRunsPolicy());
                }
            }
        }
        return executor;
    }

    private HttpURLConnection getHttpConnection(String url, String method, Map<String, String> headers) throws IOException, NoSuchAlgorithmException, NoSuchProviderException, KeyManagementException {
        URL _url = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) _url.openConnection();
        conn.setRequestMethod(method);
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setConnectTimeout(19000);
        conn.setReadTimeout(19000);
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        if (headers != null && !headers.isEmpty())
            for (Map.Entry<String, String> entry : headers.entrySet())
                conn.setRequestProperty(entry.getKey(), entry.getValue());

        return conn;
    }

    private HttpURLConnection getJsonHttpConnection(String url, String method, Map<String, String> headers) throws IOException, NoSuchAlgorithmException, NoSuchProviderException, KeyManagementException {
        URL _url = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) _url.openConnection();
        conn.setRequestMethod(method);
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setConnectTimeout(19000);
        conn.setReadTimeout(19000);
        conn.setRequestProperty("Content-Type", "application/json");
//		conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.146 Safari/537.36");
        if (headers != null && !headers.isEmpty())
            for (Map.Entry<String, String> entry : headers.entrySet())
                conn.setRequestProperty(entry.getKey(), entry.getValue());

        return conn;
    }

    /**
     * 发送 get 请求
     */
    private String get(String url, Map<String, String> queryParas, Map<String, String> headers) {
        HttpURLConnection conn = null;
        try {
            conn = getHttpConnection(buildUrlWithQueryString(url, queryParas), GET, headers);
            conn.connect();
            return readResponseString(conn);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }


    public static String get(String url, Map<String, String> queryParas) throws Exception {
        try {
            executor = getThreadPool();
            return executor.submit(() -> {
                HttpKit httpKit = new HttpKit();
                return httpKit.get(url, queryParas, null);
            }).get();
        } catch (Exception e) {
            throw e;
        }
    }

    public static String get(String url) throws Exception {
        try {
            executor = getThreadPool();
            return executor.submit(() -> {
                HttpKit httpKit = new HttpKit();
                return httpKit.get(url, null, null);
            }).get();
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * 发送 POST 请求
     * 考虑添加一个参数 Map<String, String> queryParas： getHttpConnection(buildUrl(url, queryParas), POST, headers);
     */
    private String post(String url, Map<String, String> queryParas, String data, Map<String, String> headers) {
        HttpURLConnection conn = null;
        try {
            conn = getHttpConnection(buildUrlWithQueryString(url, queryParas), POST, headers);
            conn.connect();

            OutputStream out = conn.getOutputStream();
            out.write(data.getBytes(CHARSET));
            out.flush();
            out.close();

            return readResponseString(conn);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    private String postBase64(String url, Map<String, String> queryParas, String data, Map<String, String> headers) {
        HttpURLConnection conn = null;
        try {
            conn = getHttpConnection(buildUrlWithQueryString(url, queryParas), POST, headers);
            conn.connect();

            OutputStream out = conn.getOutputStream();
            out.write(data.getBytes(CHARSET));
            out.flush();
            out.close();

            return readResponseBase64(conn);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    /**
     * 向{{url}}请求跳转为pageUri的图片二维码
     *
     * @param url
     * @param queryParas
     * @param pageUri
     * @return
     */
    public static String Base64ImgUrl(String url, Map<String, String> queryParas, String pageUri) throws Exception {
        Map<String, String> map = new HashMap<String, String>();
        map.put("path", pageUri);
        String JSONString = JSON.toJSONString(map);

        try {
            executor = getThreadPool();
            return executor.submit(() -> {
                HttpKit httpKit = new HttpKit();
                return httpKit.postBase64(url, queryParas, JSONString, null);
            }).get();
        } catch (Exception e) {
            throw e;
        }
    }

    private String postJson(String url, Map<String, String> queryParas, String data, Map<String, String> headers) {
        HttpURLConnection conn = null;
        try {
            conn = getJsonHttpConnection(buildUrlWithQueryString(url, queryParas), POST, headers);
            conn.connect();

            OutputStream out = conn.getOutputStream();
            out.write(data.getBytes(CHARSET));
            out.flush();
            out.close();

            return readResponseString(conn);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    public static String post(String url, Map<String, String> queryParas, String data) throws Exception {

        try {
            executor = getThreadPool();
            return executor.submit(() -> {
                HttpKit httpKit = new HttpKit();
                return httpKit.post(url, queryParas, data, null);
            }).get();
        } catch (Exception e) {
            throw e;
        }
    }

    public static String post(String url, String data, Map<String, String> headers) throws Exception {

        try {
            executor = getThreadPool();
            return executor.submit(() -> {
                HttpKit httpKit = new HttpKit();
                return httpKit.post(url, null, data, headers);
            }).get();
        } catch (Exception e) {
            throw e;
        }
    }

    public static String post(String url, String data) throws Exception {
        try {
            executor = getThreadPool();
            return executor.submit(() -> {
                HttpKit httpKit = new HttpKit();
                return httpKit.post(url, null, data, null);
            }).get();
        } catch (Exception e) {
            throw e;
        }
    }


    private String readResponseBase64(HttpURLConnection conn) throws Exception {
        InputStream is = null;
        String base64ImgStr = "";
        ByteArrayOutputStream byteArrayOs = new ByteArrayOutputStream();
        try {
            int len;
            byte[] bs = new byte[1024];
            is = conn.getInputStream();
            while ((len = is.read(bs)) != -1) {
                byteArrayOs.write(bs, 0, len);
            }

            Base64Encoder encoder = new Base64Encoder();
            base64ImgStr = encoder.encode(byteArrayOs.toByteArray());
            return base64ImgStr;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    private String readResponseString(HttpURLConnection conn) {
        StringBuilder sb = new StringBuilder();
        InputStream inputStream = null;
        try {
            inputStream = conn.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, CHARSET));
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
            return sb.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 在 url 之后构造 queryString
     */
    private  String buildUrlWithQueryString(String url, Map<String, String> queryParas) {
        if (queryParas == null || queryParas.isEmpty()) {
            return url;
        }

        StringBuilder sb = new StringBuilder(url);
        boolean isFirst;
        if (url.indexOf("?") == -1) {
            isFirst = true;
            sb.append("?");
        } else {
            isFirst = false;
        }

        for (Map.Entry<String, String> entry : queryParas.entrySet()) {
            if (isFirst) isFirst = false;
            else sb.append("&");

            String key = entry.getKey();
            String value = entry.getValue();
            sb.append(key);
            if (StrUtil.notBlank(value)) {
                try {
                    value = URLEncoder.encode(value, CHARSET);
                } catch (UnsupportedEncodingException e) {
                    throw new RuntimeException(e);
                }
                sb.append("=").append(value);
            }
        }
        String urlStr = sb.toString();
        log.debug("Http url:" + urlStr);
        return urlStr;
    }


    public static String readIncommingRequestData(HttpServletRequest request) {
        BufferedReader br = null;
        try {
            StringBuilder result = new StringBuilder();
            br = request.getReader();
            for (String line = null; (line = br.readLine()) != null; ) {
                result.append(line).append("\n");
            }

            return result.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    log.error("cloase Request reader error.", e);
                }
            }
        }
    }


    /**
     * http post 请求
     * @param url
     * @param param
     * @return
     */
    public static JSONObject httpPost(String url, String param){
        OutputStreamWriter out =null;
        BufferedReader reader = null;
        String response = "";
        JSONObject resJson = null;

        //创建连接
        try {
            URL httpUrl = null; //HTTP URL类 用这个类来创建连接
            //创建URL
            httpUrl = new URL(url);
            //建立连接
            HttpURLConnection conn = (HttpURLConnection) httpUrl.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            conn.setRequestProperty("connection", "keep-alive");
            conn.setUseCaches(false);//设置不要缓存
            conn.setInstanceFollowRedirects(true);
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.connect();
            //POST请求
            out = new OutputStreamWriter(
                    conn.getOutputStream());
            out.write(param);
            out.flush();
            //读取响应
            reader = new BufferedReader(new InputStreamReader(
                    conn.getInputStream()));
            String lines;
            while ((lines = reader.readLine()) != null) {
                lines = new String(lines.getBytes(), "utf-8");
                response+=lines;
            }
            resJson = JSONObject.parseObject(response);
            //resJson = JSONObject.fromObject(response);
            reader.close();
            // 断开连接
            conn.disconnect();

        } catch (Exception e) {
            log.error("发送 POST 请求出现异常！e:{}",e);
            e.printStackTrace();
        }
        //使用finally块来关闭输出流、输入流
        finally{
            try{
                if(out!=null){
                    out.close();
                }
                if(reader!=null){
                    reader.close();
                }
            }
            catch(IOException ex){
                ex.printStackTrace();
            }
        }

        return resJson;
    }


    /**
     * https方式发送请求
     * @param url
     * @param requestMethod
     * @param jsonReq
     * @return
     * @throws Exception
     */
    public static String sendHttp(String url, String requestMethod, JSONObject jsonReq) throws Exception{

        InputStream in = null;
        OutputStream out = null;
        HttpURLConnection conn = null;
        try {
            URL console = new URL(url);
            log.info("open connection ...");
            conn = (HttpURLConnection)console.openConnection();
            conn.setRequestMethod(requestMethod);
           // conn.setHostnameVerifier(new TrustAnyHostnameVerifier());
            conn.setConnectTimeout(3000);
            conn.setReadTimeout(3000);
            if("POST".equals(requestMethod)){
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            }
            log.info("begin connect ...");
            conn.connect();
            log.info("connect success ...");
            if("POST".equals(requestMethod)){
                out = conn.getOutputStream();
                out.write(jsonReq.toString().getBytes("utf-8"));
            }
            in = conn.getInputStream();
            byte b[] = new byte[4096];
            int len = 0;
            int temp = 0;
            while ((temp = in.read()) != -1){
                b[len] = (byte) temp;
                len++;
            }

            String str = new String(b, 0, len,"UTF-8");

            return str;
        } catch (ConnectException e) {
            log.info("https ConnectException", e);
            throw new Exception("0001");

        } catch (IOException e) {
            log.info("https IOException", e);
            throw new Exception("0002");

        } catch (Exception e) {
            if(e.getMessage().startsWith("errcode")){
                throw new Exception(e.getMessage().substring(7));
            }else{
                log.info("https Exception : ", e);
                throw new Exception("0003");
            }
        } finally {
            try{
                in.close();
                out.close();
                conn.disconnect();
            }catch (Exception e){
            }
        }
    }

}
