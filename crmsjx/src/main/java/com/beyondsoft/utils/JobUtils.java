package com.beyondsoft.utils;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.job.service.MsgPushService;
import com.beyondsoft.modules.tb.dao.StageLossDao;
import com.beyondsoft.modules.tb.entity.StageLossEntity;
import com.beyondsoft.modules.tb.service.LossCountService;
import com.beyondsoft.cache.PropCache;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class JobUtils extends ServiceImpl<StageLossDao, StageLossEntity> {

    private static Logger log = LogManager.getLogger(JobUtils.class);
    @Autowired
    private LossCountService lossCountService;
    @Autowired
    MsgPushService msgPushService;

    public static JobUtils jobUtils;

    @PostConstruct
    public void init(){
        jobUtils = this;
    }

    public void lossTrack(String startTime,String endTime,String currDate){

        Map reqPara = new HashMap();
        reqPara.put("startTime", startTime.trim());
        reqPara.put("endTime", endTime.trim());
        reqPara.put("currDate",currDate.trim());

        String resCode ="";

        List<StageLossEntity> queryList = baseMapper.queryStageLossList(reqPara);
        if (queryList.size() > 0) {
            //判断是否推送处理
            Map<String,String> resMap = new HashMap<>();
            for(int i=0; i<queryList.size();i++) {
                resMap = trackService(queryList.get(i));
                resCode = resMap.get("rtnCode");
                if("000000".equals(resCode)){
                    log.info("beehive push success");

                }
            }
        }

    }


    public Map<String,String> trackService(StageLossEntity stageLoss){

        Map<String,String> map = new HashMap<>();
        Map<String,Object> sendMap = new HashMap<>();

        String userId = stageLoss.getUserId();
        String bankId = stageLoss.getBankId();
        String systemId = stageLoss.getSystemId();
        String pageId = stageLoss.getPageId();

        String pushFlag = PropCache.getCacheInfo(bankId+systemId+pageId+"pushFlag");
        if(StrUtil.isNotBlank(pushFlag) && "1".equals(pushFlag)){
            sendMap.put("userId",userId);
            sendMap.put("bankId",bankId);
            sendMap.put("pageId",pageId);
            map = msgPushService.sendTrackMsg(sendMap);

        }

        return map;

    }

}
