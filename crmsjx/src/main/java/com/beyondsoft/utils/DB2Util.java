package com.beyondsoft.utils;

import java.sql.*;
import java.util.ResourceBundle;

public class DB2Util {

    public final static ResourceBundle bundle = ResourceBundle.getBundle("DB2ConnectionConfig");//DB2ConnectionConfig.properties配置文件的名称
    public final static String driverName = bundle.getString("db2sql.database.driver");
    public final static String dbURL = bundle.getString("db2sql.database.url");
    public final static String userName = bundle.getString("db2sql.database.user");
    public final static String userPwd = bundle.getString("db2sql.database.password");

    public  static Connection getConn(){
        Connection conn = null;
        try {
            Class.forName(driverName).newInstance();
            try {
                conn = DriverManager.getConnection(dbURL, userName, userPwd);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return conn ;
    }

    public static void close(Connection conn, PreparedStatement stmt, ResultSet rs) {
        try {
            if (rs != null) {
                rs.close();
                rs = null;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
            finally {
                try {
                    if (conn != null) {
                        conn.close();
                        conn = null;
                    }
                }
                catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
