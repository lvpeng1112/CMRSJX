/**
 * 
 */
package com.beyondsoft.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author zhenhuawang
 *
 */
public class DateUtil {
	
	/**
	 * yyyy-MM-dd
	 */
	public static String formaterDay = "yyyy-MM-dd";
	
	public static String yyyyMMdd = "yyyyMMdd";

	public static String HHmmss = "HHmmss";

	public static String yyyyMMddHHmmss = "yyyyMMddHHmmss";

	public static String MMddHHmmss = "MMddHHmmss";

	/**
	 * yyyy-MM-dd HH:mm:ss
	 */
	public static String formaterDateTime = "yyyy-MM-dd HH:mm:ss";
	
	/**
	 * 得到系统日期
	 * 
	 * @param format 时间格式
	 * @return
	 */
	public static String getCurrDate(String format) {
		SimpleDateFormat formater = new SimpleDateFormat(format);
		return formater.format(new Date());
	}
	
	
	public static String getPrevMonth() {
		return getPrevMonth(formaterDay);
	}
	
	/**
	 * 得到上月日期
	 * 
	 * @param format
	 * @return
	 */
	public static String getPrevMonth(String format) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		return getFormaterDate(format, cal);
	}
	
	/**
	 * 得到下月日期
	 * 
	 * @param format
	 * @return
	 */
	public static String getNextMonth(String format) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, 1);
		return getFormaterDate(format, cal);
	}
	
	/**
	 * 得到当月的第一天 
	 * @return yyyy-MM-dd
	 */
	public static String getMonthFirstDay()
	{
		 return getMonthFirstDay(formaterDay);
	}

	/**
	 * 
	 */
	public static String getNextDay(String format){
		 Calendar c = Calendar.getInstance();   
	     c.add(Calendar.DAY_OF_MONTH, 1);// 今天+1天  
	     return getFormaterDate(format, c);
	}

	/**
	 *
	 */
	public static String getPreDay(String format){
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_MONTH, -1);// 今天-1天
		return getFormaterDate(format, c);
	}

	/**
	 * 指定日期开始往后加n天
	 */
	public static String calcDay(String yyyyMMdd, int n){
		Calendar c = Calendar.getInstance();
		c.set(Integer.parseInt(yyyyMMdd.substring(0, 4)), Integer.parseInt(yyyyMMdd.substring(4, 6)) - 1, Integer.parseInt(yyyyMMdd.substring(6)));
		c.add(Calendar.DAY_OF_MONTH, n);
		return getFormaterDate(DateUtil.yyyyMMdd, c);
	}

	/**
	 * 指定日期开始往后加n月
	 */
	public static String calcMonth(String yyyyMMdd, int n){
		Calendar c = Calendar.getInstance();
		c.set(Integer.parseInt(yyyyMMdd.substring(0, 4)), Integer.parseInt(yyyyMMdd.substring(4, 6)) - 1, Integer.parseInt(yyyyMMdd.substring(6)));
		c.add(Calendar.MONTH, n);
		return getFormaterDate(DateUtil.yyyyMMdd, c);
	}
	
	/**
	 * 当天开始往后加n天
	 */
	public static String getNextDayFate(String format,int n){
		 Calendar c = Calendar.getInstance();   
	     c.add(Calendar.DAY_OF_MONTH, n);// 今天+1天  
	     return getFormaterDate(format, c);
	}
	
	public static String getNextNYear(String format,int n){
		Calendar c = Calendar.getInstance();
		c.add(Calendar.YEAR, n);
		return getFormaterDate(format, c);
	}
	
	public static void main(String[] args) {
		String s = getPreDay("yyyyMMdd");
		System.out.println(s);
	}
	
	/**
	 * 得到当月的第一天
	 * 
	 * @param format
	 * @return
	 */
	public static String getMonthFirstDay(String format) {
		Calendar calendar = Calendar.getInstance();     
	    //calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));  
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		return getFormaterDate(format, calendar);
	}

	/**
	 * curDate是否在startDate至endDate期间
	 * @param curDate
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static Boolean isBetweenDate(String curDate, String startDate, String endDate, String format) {
		Boolean isbetween = false;
		Date compDate = null;
		Date stDate = null;
		Date edDate = null;
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
		df.setLenient(false);
		try {
			if("00000000".equals(curDate)){
				return false;
			}
			compDate = df.parse(curDate);
			stDate = df.parse(startDate);
			edDate = df.parse(endDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		if(compDate.after(stDate) && compDate.before(edDate)){
			isbetween = true;
		}

		return isbetween;
	}
	
	/**
	 * 格式化时间
	 * @param format 时间格式
	 * @param cal 时间
	 * @return
	 */
	public static String getFormaterDate(String format, Calendar cal) {
		SimpleDateFormat formater = new SimpleDateFormat(format);
		return formater.format(cal.getTime());
	}
	
	/**
	 * 格式化时间
	 * @param format 时间格式
	 * @param date 时间
	 * @return
	 */
	public static String getFormaterDate(String format, Date date) {
		SimpleDateFormat formater = new SimpleDateFormat(format);
		return formater.format(date);
	}

	/**
	 * 格式化时间
	 * @param formatOld 原来时间格式
	 * @param formatNew 新的时间格式
	 * @param date string类型时间
	 * @return
	 */
	public static String getFormaterDate(String formatOld, String formatNew, String date) {
		try {
			SimpleDateFormat formaterO = new SimpleDateFormat(formatOld);
			SimpleDateFormat formaterN = new SimpleDateFormat(formatNew);
			Date d = formaterO.parse(date);
			date = formaterN.format(d);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * String转Date
	 * @param format 时间格式
	 * @param date 时间
	 * @return
	 */
	public static Date getFormaterDate(String format, String date) {
		try {
			SimpleDateFormat formater = new SimpleDateFormat(format);
			return formater.parse(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 时间格式转换HH:mm转换为HHmm
	 * @param date
	 * @return
	 */
	public static String fmtTimeHHmm1(String date) {
		String rtnDate = "";
		if(date==null||date.length()!=5) date = "0000";
		rtnDate = StrUtil.trim(date.replace(":",""));
		return rtnDate;
	}

	/**
	 * 时间格式转换 HHmm转换为HH:mm
	 * @param date
	 * @return
	 */
	public static String fmtTimeHHmm(String date) {
		String rtnDate = "";
		if(date==null||date.trim().length()!=4) date = "0000";
		rtnDate = date.substring(0,2)+":"+date.substring(2);
		return rtnDate;
	}

	private static boolean isRYear(int inputInt) {
		return inputInt % 100 == 0 && inputInt % 400 == 0 || inputInt % 100 != 0 && inputInt % 4 == 0;
	}

	public static boolean isDate(String inputStr) {
		if (inputStr.length() == 0) {
			return true;
		}
		if (inputStr.length() != 8) {
			return false;
		}
		for (int i = 0; i < inputStr.length(); i++) {
			char charI = inputStr.charAt(i);
			if (charI < '0' || charI > '9') {
				return false;
			}
		}

		if (inputStr.compareTo("19000101") < 0) {
			return false;
		}
		int year = Integer.parseInt(inputStr.substring(0, 4));
		int month = Integer.parseInt(inputStr.substring(4, 6));
		int day = Integer.parseInt(inputStr.substring(6, 8));
		if (month < 1 || month > 12 || day < 1 || day > 31) {
			return false;
		}
		if ((month == 4 || month == 6 || month == 9 || month == 11) && day > 30) {
			return false;
		}
		return (!isRYear(year) || month != 2 || day <= 29) && (isRYear(year) || month != 2 || day <= 28);
	}

}
