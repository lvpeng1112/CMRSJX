/**
 * Copyright (c) 2018 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.datasource.aspect;


import com.beyondsoft.datasource.annotation.DataSource;
import com.beyondsoft.datasource.config.DynamicDataSource;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * 多数据源，切面处理类
 *
 * @author luochao
 */
@Aspect
@Component
public class DataSourceAspect implements Ordered{
    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Pointcut("@annotation(com.beyondsoft.datasource.annotation.DataSource) " +
            "|| @within(com.beyondsoft.datasource.annotation.DataSource)")
        public void dataSourcePointCut() {

        }

        @Around("dataSourcePointCut()")
        public Object around(ProceedingJoinPoint point) throws Throwable {
            MethodSignature signature = (MethodSignature) point.getSignature();
            Method method = signature.getMethod();

            DataSource ds = method.getAnnotation(DataSource.class);
            if (ds == null) {
                DynamicDataSource.setDataSource("first");
                logger.debug("set datasource is " + "first");
            } else {
                DynamicDataSource.setDataSource(ds.value());
                logger.debug("set datasource is " + ds.value());
            }

            try {
                return point.proceed();
            } finally {
                DynamicDataSource.clearDataSource();
                logger.debug("clean datasource");
            }
        }

    @Override
    public int getOrder() {
        return 1;
    }
}