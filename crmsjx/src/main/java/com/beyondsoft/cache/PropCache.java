package com.beyondsoft.cache;

import java.util.HashMap;

public class PropCache {
	
	private static HashMap<String, String> cacheMap = new HashMap<String, String>();

	// 单实例构造方法
	private PropCache() {
		super();
	}
	
	// 得到缓存。同步静态方法
	private synchronized static String getCache(String paramKey) {
		return cacheMap.get(paramKey);
	}

	// 判断是否存在一个缓存
	public synchronized static boolean hasCache(String paramKey) {
		return cacheMap.containsKey(paramKey);
	}

	// 清除所有缓存
	public synchronized static void clearAll() {
		cacheMap.clear();
	}

	// 清除指定的缓存
	public synchronized static void clearOnly(String key) {
		cacheMap.remove(key);
	}


	// 载入缓存
	public synchronized static void putCache(String paramKey, String paramValue) {
		cacheMap.put(paramKey, paramValue);
	}

	/**
	 * 获取缓存信息
	 * 首先获取该银行自己的参数，未配置则返回空字符串
	 * @param paramKey
	 * @return
	 */
	
	public static String getCacheInfo(String paramKey) {
		if (hasCache(paramKey)) {
			return getCache(paramKey);
		} else{
			return "";
		}
			
	}

	// 获取缓存中的大小
	public static int getCacheSize() {
		return cacheMap.size();
	}
}
