package com.beyondsoft.cache;

import com.beyondsoft.modules.sys.entity.SysConfigEntity;
import com.beyondsoft.modules.sys.service.SysConfigService;
import com.beyondsoft.modules.tb.entity.BankCfgEntity;
import com.beyondsoft.modules.tb.service.BankCfgService;
import com.beyondsoft.common.utils.DateKit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 缓存处理类，负责初始化、刷新缓存
 * @author wwc
 *
 */
@Component("dealCache")
public class DealCache {
	
	private static Logger log = LogManager.getLogger(DealCache.class);

    @Autowired
    private SysConfigService sysConfigService;

	@Autowired
	private BankCfgService bankCfgService;

    /**
	 * 刷新缓存方法
	 * @return
	 */
	public String initProp(){
		String message = "";
		try{


            log.info("++++++加载参数开始 start time" + DateKit.getCurrDate(DateKit.yyyyMMddHHmmss));
            long start = System.currentTimeMillis();

            List<SysConfigEntity> SysConfigEntityList = sysConfigService.queryParamKeyList();

            List<BankCfgEntity> bankCfgEntityList = bankCfgService.queryBankCfgList();

			for(int i=0; i< SysConfigEntityList.size(); i++){
                SysConfigEntity prop = (SysConfigEntity)SysConfigEntityList.get(i);
				PropCache.putCache(prop.getParamKey(),
						prop.getParamValue());
			}

			for(int j=0; j<bankCfgEntityList.size() ;j++){
				BankCfgEntity bankProp = bankCfgEntityList.get(j);
				PropCache.putCache(bankProp.getBankid()+bankProp.getSystemid()+bankProp.getPropname()+bankProp.getProptype(),bankProp.getPropvalue());
			}

			long end = System.currentTimeMillis();
			log.info("加载参数缓存数：" + PropCache.getCacheSize() + " 耗时：" + (end - start) + "ms");

		}catch(Exception e){

			log.error(e.getMessage());
			message = e.getMessage();
		}
		return message;
	}
	/**
	 * 刷新缓存
	 * @return
	 */
	public String flushProp(){
		String message = "";
		try{

            log.info("++++++加载参数开始 start time" + DateKit.getCurrDate(DateKit.yyyyMMddHHmmss));
            long start = System.currentTimeMillis();

            List<SysConfigEntity> SysConfigEntityList = sysConfigService.queryParamKeyList();

            for(int i=0; i< SysConfigEntityList.size(); i++){
                SysConfigEntity prop = (SysConfigEntity)SysConfigEntityList.get(i);
                PropCache.putCache(prop.getParamKey(),
                        prop.getParamValue());
            }
            long end = System.currentTimeMillis();
            log.info("加载参数缓存数：" + PropCache.getCacheSize() + " 耗时：" + (end - start) + "ms");

		}catch(Exception e){
			log.info("刷新参数缓存出错"+e.getMessage());

			message = e.getMessage();
		}
		return message;
	}
}
