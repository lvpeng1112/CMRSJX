/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.common.validator.group;

/**
 * 阿里云
 *
 * @author shuaigao@cupdata.com
 */
public interface AliyunGroup {
}
