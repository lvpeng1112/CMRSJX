package com.beyondsoft.common.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * Auther shuaigao on 2018/10/12
 * 公用返回类
 **/
public class R extends HashMap<String, Object> {
    private static final long serialVersionUID = 1L;

    public R() {
        put("code", "000000");
        put("msg", "success");
    }

    public static R error(Throwable e) {
        R r = new R();
        r.put("code", "999999");
        r.put("msg", e.getMessage());
        //r.put("cause", e.getStackTrace());
        return r;
    }

    public static R ok(String msg) {
        R r = new R();
        r.put("msg", msg);
        return r;
    }

    public static R ok(String code, String msg) {
        R r = new R();
        r.put("msg", msg);
        r.put("code", code);
        return r;
    }

    public static R ok(Map<String, Object> map) {
        R r = new R();
        r.putAll(map);
        return r;
    }

    public static R ok() {
        return new R();
    }

    public R put(String key, Object value) {
        super.put(key, value);
        return this;
    }
}
