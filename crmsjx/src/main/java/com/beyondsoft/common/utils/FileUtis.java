
package com.beyondsoft.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;


/**
 * 文件工具类
 *
 * @author zhenhuawang
 */
public class FileUtis {
    private static Logger logger = LoggerFactory.getLogger(FileUtis.class);

    /**
     * 下载Excle
     *
     * @param response
     * @param excelPath
     * @param fileName
     */
    public static String downExcle(HttpServletResponse response, String excelPath, String fileName) throws Exception {
        BufferedInputStream br = null;
        OutputStream out = null;
        try {
            br = new BufferedInputStream(new FileInputStream(excelPath));
            byte[] buf = new byte[1024];
            int len = 0;
            response.reset(); // 非常重要
            response.setContentType("application/x-msdownload");
            response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(fileName, "UTF-8") + ".xlsx");
            out = response.getOutputStream();
            while ((len = br.read(buf)) > 0)
                out.write(buf, 0, len);
            br.close();
            out.close();
            logger.info("downExcle error success");
            return "SUCCESS";
        } catch (Exception e) {
            logger.info("downExcle error is", e);
            return "FAIL";
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }

    }


    /**
     * 删除PDF
     *
     * @param filePath
     */
    public static void deleteExcle(String filePath) {
        File file = new File(filePath);
        try {
            if (file.exists() && file.isFile()) {
                if (file.delete()) {
                    logger.info("删除文件" + filePath + "成功");
                } else {
                    logger.info("删除文件" + filePath + "失败");
                }
            } else {
                logger.info("文件" + filePath + "不存在");
            }
        } catch (Exception e) {
            logger.info("deleteExcle error is", e);

        }
    }

    /**
     * 获取某文件夹下的文件名和文件内容,存入map集合中
     *
     * @param filePath 需要获取的文件的 路径
     * @return 返回存储文件名和文件内容的map集合
     */
    public static Map<String, String> getFilesDatas(String filePath) {
        Map<String, String> files = new HashMap<>();
        File file = new File(filePath); //需要获取的文件的路径
        String[] fileNameLists = file.list(); //存储文件名的String数组
        File[] filePathLists = file.listFiles(); //存储文件路径的String数组
        for (int i = 0; i < filePathLists.length; i++) {
            if (filePathLists[i].isFile()) {
                try {//读取指定文件路径下的文件内容
                    String fileDatas = readFile(filePathLists[i]);
                    //把文件名作为key,文件内容为value 存储在map中
                    files.put(fileNameLists[i], fileDatas);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return files;
    }

    /**
     * 读取指定目录下的文件
     *
     * @param path 文件的路径
     * @return 文件内容
     * @throws IOException
     */
    public static String readFile(File path) throws IOException {
        StringBuilder buffer = new StringBuilder();
        String replace = "";
        try {
            if (!path.exists()) {
                return null;
            }
            InputStream inputStream = new FileInputStream(path);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(bufferedInputStream));

            while (bufferedReader.ready()) {
                buffer.append((char) bufferedReader.read());
                replace = buffer.toString().replace("\n","");
            }

            bufferedReader.close();
            bufferedInputStream.close();
            inputStream.close();

            return replace;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}

