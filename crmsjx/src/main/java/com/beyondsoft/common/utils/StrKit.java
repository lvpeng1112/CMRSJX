/**
 *
 */
package com.beyondsoft.common.utils;


/**
 *
 */


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * 字符串工具类
 *
 * @author zhenhuawang
 */
public class StrKit {
    public static final String EMPTY = "";
    public static final String SPACE = " ";

    public static final String ISO_8859_1 = "ISO-8859-1";
    public static final String UTF_8 = "UTF-8";
    public static final String GBK = "GBK";

    private static Random random = new Random();

    /**
     * 用于随机选的数字
     */
    private static final String BASE_NUMBER = "0123456789";
    /**
     * 用于随机选的字符
     */
    private static final String BASE_CHAR = "abcdefghijklmnopqrstuvwxyz";
    /**
     * 用于随机选的字符和数字
     */
    private static final String BASE_CHAR_NUMBER = BASE_CHAR + BASE_NUMBER;

    private static Logger logger = LoggerFactory.getLogger(StrKit.class);

    /**
     * 首字母变小写
     */
    public static String firstCharToLowerCase(String str) {
        Character firstChar = str.charAt(0);
        String tail = str.substring(1);
        str = Character.toLowerCase(firstChar) + tail;
        return str;
    }

    /**
     * 首字母变大写
     */
    public static String firstCharToUpperCase(String str) {
        Character firstChar = str.charAt(0);
        String tail = str.substring(1);
        str = Character.toUpperCase(firstChar) + tail;
        return str;
    }

    public static String trim(String str) {
        return str == null ? "" : str.trim();
    }

    /**
     * 字符串为 null 或者为 "" 时返回 true
     */
    public static boolean isBlank(String str) {
        return str == null || str.trim().length() == 0;
    }

    public static boolean isNotBlank(String str) {
        return !isBlank(str);
    }

    /**
     * 所有字符串都不为空 或者"" 时返回true
     *
     * @param strs
     * @return
     */
    public static boolean notBlank(String... strs) {
        if (strs == null)
            return false;
        for (String str : strs)
            if (str == null || "".equals(str.trim()))
                return false;
        return true;
    }

    /**
     * 所有对象不为null ，返回true
     *
     * @param paras
     * @return
     */
    public static boolean notNull(Object... paras) {
        if (paras == null)
            return false;
        for (Object obj : paras)
            if (obj == null)
                return false;
        return true;
    }

    /**
     * 两个字符串比较
     *
     * @param str1
     * @param str2
     * @return 不相同 false，相同 true
     */
    public static boolean equals(String str1, String str2) {
        if (str1 == null) {
            if (str2 != null)
                return false;
        } else if (!str1.equals(str2))
            return false;
        return true;
    }

    /**
     * @param str
     * @param remove
     * @return
     */
    public static String removeEnd(String str, String remove) {
        if (isBlank(str) || isBlank(remove)) {
            return str;
        }
        if (str.endsWith(remove)) {
            return str.substring(0, str.length() - remove.length());
        }
        return str;
    }

    public static String defaultString(final String str) {
        return str == null ? EMPTY : str;
    }


    /**
     * 邮箱脱敏
     * @param email
     * @return
     */
	public static String formatEmail(String email) {
		if (!Pattern.matches("^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$", email)) {
			return "";
		}
		try {
			return email.substring(0, 4) + "****" + email.substring(email.indexOf("@"));
		} catch (Exception e) {
			return "****" + email.substring(email.indexOf("@"));
		}
	}

    /**
     * 手机号脱敏（173 **** 3312）
     * @param mobile
     * @return
     */
	public static String fomatMobile(String mobile) {
		if (isBlank(mobile)) {
			return "";
		}
		mobile = mobile.trim();
		String firstNo = mobile.substring(0, 3);
		String lastNo = mobile.substring(mobile.length() - 4);
		return firstNo + " **** " + lastNo;
	}

    /**
     * 取卡号前四位和后四位，中间填四个*
     * @param string
     * @return
     */
	public static String getFormatCardNo(String string) {
		if (string == null || string.trim().equals("")) {
			return "";
		}
		String cardNo= string.trim();
		String lastNo = cardNo.substring(cardNo.length()-4);
		String firstNo = cardNo.substring(0,4);
		String formatCardNo = firstNo + "#####" + lastNo;
		return formatCardNo;
	}

    /**
     * 取卡号前四位和后四位，中间填八个*
     * @param string
     * @return
     */
	public static String formatCardNo(String string) {
		if (string == null || string.trim().equals("")) {
			return "";
		}
		String cardNo= string.trim();
		String lastNo = cardNo.substring(cardNo.length()-4);
		String firstNo = cardNo.substring(0,4);
		String formatCardNo = firstNo + " **** **** " + lastNo;
		return formatCardNo;
	}

    /**
     * 取证件号前三位和后四位，中间填*
     * @param string
     * @return
     */
	public static String formatIdNo(String string) {
		if (string == null || string.trim().equals("")) {
			return "";
		}
		String cardNo= string.trim();
		String lastNo = cardNo.substring(cardNo.length()-4);
		String firstNo = cardNo.substring(0,3);
		String formatCardNo = firstNo + " *** **** **** " + lastNo;
		return formatCardNo;
	}

    /**
     * 取身份证前六位和后四位，中间填四个*
     * @param string
     * @return
     */

	public static String getFormatCardID(String string) {
		String cardID= string.trim();
		String lastID = cardID.substring(cardID.length()-4);
		String firstID = cardID.substring(0,6);
		String formatCardID = firstID + "#####" + lastID;
		return formatCardID;
	}

    /**
     * 替换中间字符为*号
     *
     * @param oleStr   要替换的
     * @param firstLen
     * @param lastLen
     * @return
     */
    public static String replaceStr(String oleStr, int firstLen, int lastLen) {
        if (isBlank(oleStr)) {
            return "";
        }
        String newStr = oleStr.trim();
        int len = newStr.length() - firstLen - lastLen;
        if (len <= 0) {
            return oleStr;
        }
        StringBuffer sb = new StringBuffer();
        String lastStr = newStr.substring(newStr.length() - lastLen);
        sb.append(newStr.substring(0, firstLen));
        for (int i = 0; i < len; i++) {
            sb.append("*");
        }
        sb.append(lastStr);
        return sb.toString();
    }

    public static int convertToInt(String intStr, int defValue) {
        if (isBlank(intStr)) {
            return defValue;
        }
        int i = defValue;
        try {
            i = Integer.parseInt(intStr);
        } catch (Exception e) {
            i = defValue;
        }
        return i;
    }

    /**
     * 二行制转字符串
     *
     * @param b
     * @return
     */
    public static String byte2hex(byte[] b) {
        String hs = "";
        String stmp = "";
        for (int n = 0; n < b.length; n++) {
            stmp = (Integer.toHexString(b[n] & 0XFF));
            if (stmp.length() == 1)
                hs = hs + "0" + stmp;
            else
                hs = hs + stmp;
            if (n < b.length - 1)
                hs = hs + "";
        }
        return hs.toUpperCase();
    }

    /**
     * 填充字符串
     *
     * @param string
     * @param filler
     * @param totalLength
     * @param atEnd
     * @param encoding
     * @return
     */
    public static String fillStr(String string, char filler, int totalLength, boolean atEnd, String encoding) {
        byte[] tempbyte = null;
        try {
            tempbyte = string.getBytes(encoding);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("fillStr get bytes error.", e);
        }

        int currentLength = tempbyte.length;
        int delta = totalLength - currentLength;
        for (int i = 0; i < delta; i++) {
            if (atEnd)
                string += filler;
            else
                string = filler + string;
        }
        return string;

    }

    /**
     * 填充字符串
     *
     * @param string
     * @param filler
     * @param totalLength
     * @param atEnd
     * @return
     */
    public static String fillStr(String string, char filler, int totalLength, boolean atEnd) {
        byte[] tempbyte = string.getBytes();
        int currentLength = tempbyte.length;
        int delta = totalLength - currentLength;
        for (int i = 0; i < delta; i++) {
            if (atEnd)
                string += filler;
            else
                string = filler + string;
        }
        return string;

    }

    /**
     * 转换字符串的字符集编码
     *
     * @param source     字符串
     * @param srcCharset 源字符集，默认ISO-8859-1
     * @param newCharset 目标字符集，默认UTF-8
     * @return 转换后的字符集
     */
    public static String convertCharset(String source, String srcCharset, String newCharset) {
        if (StrKit.isBlank(srcCharset)) {
            srcCharset = ISO_8859_1;
        }

        if (StrKit.isBlank(newCharset)) {
            srcCharset = UTF_8;
        }

        if (StrKit.isBlank(source) || srcCharset.equals(newCharset)) {
            return source;
        }
        try {
            return new String(source.getBytes(srcCharset), newCharset);
        } catch (UnsupportedEncodingException unex) {
            throw new RuntimeException(unex);
        }
    }

    public static String convertCharset(String source, String newCharset) {
        if (StrKit.isBlank(source)) {
            return source;
        }
        try {
            return new String(source.getBytes(), newCharset);
        } catch (UnsupportedEncodingException unex) {
            throw new RuntimeException(unex);
        }
    }

    /**
     * 编码字符串
     *
     * @param str     字符串
     * @param charset 字符集
     * @return 编码后的字节码
     */
    public static byte[] encode(String str, String charset) {
        if (str == null) {
            return null;
        }

        try {
            return str.getBytes(charset);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(format("Charset [{}] unsupported!", charset));
        }
    }

    /**
     * 解码字节码
     *
     * @param data    字符串
     * @param charset 字符集
     * @return 解码后的字符串
     */
    public static String decode(byte[] data, String charset) {
        if (data == null) {
            return null;
        }

        try {
            return new String(data, charset);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(format("Charset [{}] unsupported!", charset));
        }
    }

    /**
     * 格式化文本
     *
     * @param template 文本模板，被替换的部分用 {} 表示
     * @param values   参数值
     * @return 格式化后的文本
     */
    public static String format(String template, Object... values) {
        if (isBlank(template)) {
            return template;
        }

        final StringBuilder sb = new StringBuilder();
        final int length = template.length();

        int valueIndex = 0;
        char currentChar;
        for (int i = 0; i < length; i++) {
            if (valueIndex >= values.length) {
                sb.append(sub(template, i, length));
                break;
            }

            currentChar = template.charAt(i);
            if (currentChar == '{') {
                final char nextChar = template.charAt(++i);
                if (nextChar == '}') {
                    Object obj = values[valueIndex++];
                    if (obj == null) {
                        obj = "null";
                    }
                    sb.append(obj);
                } else {
                    sb.append('{').append(nextChar);
                }
            } else {
                sb.append(currentChar);
            }

        }

        return sb.toString();
    }

    /**
     * 格式化文本
     *
     * @param template 文本模板，被替换的部分用 {key} 表示
     * @param map      参数值对
     * @return 格式化后的文本
     */
    public static String format(String template, Map<?, ?> map) {
        if (null == map || map.isEmpty()) {
            return template;
        }

        for (Entry<?, ?> entry : map.entrySet()) {
            template = template.replace("{" + entry.getKey() + "}", entry.getValue().toString());
        }
        return template;
    }

    /**
     * 改进JDK subString<br>
     * index从0开始计算，最后一个字符为-1<br>
     * 如果from和to位置一样，返回 "" example: abcdefgh 2 3 -> c abcdefgh 2 -3 -> cde
     *
     * @param string    String
     * @param fromIndex 开始的index（包括）
     * @param toIndex   结束的index（不包括）
     * @return 字串
     */
    public static String sub(String string, int fromIndex, int toIndex) {
        int len = string.length();

        if (fromIndex < 0) {
            fromIndex = len + fromIndex;

            if (toIndex == 0) {
                toIndex = len;
            }
        }

        if (toIndex < 0) {
            toIndex = len + toIndex;
        }

        if (toIndex < fromIndex) {
            int tmp = fromIndex;
            fromIndex = toIndex;
            toIndex = tmp;
        }

        if (fromIndex == toIndex) {
            return EMPTY;
        }

        char[] strArray = string.toCharArray();
        char[] newStrArray = Arrays.copyOfRange(strArray, fromIndex, toIndex);
        return new String(newStrArray);
    }

    /**
     * 包装指定字符串
     *
     * @param str    被包装的字符串
     * @param prefix 前缀
     * @param suffix 后缀
     * @return 包装后的字符串
     */
    public static String wrap(String str, String prefix, String suffix) {
        return format("{}{}{}", getNotNull(prefix), getNotNull(str), getNotNull(suffix));
    }

    public static String getNotNull(String str) {
        return str == null ? "" : str;
    }


    /**
     * 生成get方法名<br/>
     *
     * @param fieldName
     * @return
     */
    public static String genGetter(String fieldName) {
        if (isBlank(fieldName)) {
            return null;
        }

        return "get" + firstCharToUpperCase(fieldName);
    }

    /**
     * 生成set方法名<br/>
     * 例如：name 返回 setName
     *
     * @param fieldName 属性名
     * @return setXxx
     */
    public static String genSetter(String fieldName) {
        if (isBlank(fieldName)) {
            return null;
        }

        return "set" + firstCharToUpperCase(fieldName);
    }

    /**
     * 包含中文的截取，一个中文长度为2
     *
     * @param string
     * @param length 长度
     * @return 字串
     */
    public static String subStr(String string, int length) {
        if (isBlank(string)) {
            return EMPTY;
        }

        char[] newStrArray = null;
        int count = 0;
        int offset = 0;
        char[] strArray = string.toCharArray();
        for (int i = 0; i < strArray.length; i++) {
            if (strArray[i] > 256) {
                offset = 2;
                count += 2;
            } else {
                offset = 1;
                count++;
            }
            if (count == length) {
                newStrArray = Arrays.copyOfRange(strArray, 0, i + 1);
                break;
            } else if ((count == length + 1 && offset == 2)) {
                newStrArray = Arrays.copyOfRange(strArray, 0, i);
                break;
            }
        }
        if (length > count) {
            newStrArray = Arrays.copyOfRange(strArray, 0, strArray.length);
        }
        return new String(newStrArray);
    }


    /**
     * 获取字符串的长度，如果有中文，则每个中文字符计为2位
     *
     * @param value 指定的字符串
     * @return 字符串的长度
     */
    public static int chineseLen(String value) {
        int valueLength = 0;
        String chinese = "[\u0391-\uFFE5]";

        for (int i = 0; i < value.length(); i++) {
            String temp = value.substring(i, i + 1);
            if (temp.matches(chinese)) {
                valueLength += 2;
            } else {
                valueLength += 1;
            }
        }
        return valueLength;
    }


    /**
     * 获得一个随机的字符串（包含数字和字符）
     *
     * @param length 字符串的长度
     * @return 随机字符串
     */
    public static String genRandom(int length) {
        return genRandom(BASE_CHAR_NUMBER, length);
    }

    /**
     * 获得一个只包含数字的字符串
     *
     * @param length 字符串的长度
     * @return 随机字符串
     */
    public static String genRandomNum(int length) {
        return genRandom(BASE_NUMBER, length);
    }

    /**
     * 获得一个只包含字母的字符串
     *
     * @param length
     * @return
     */
    public static String genRandomStr(int length) {
        return genRandom(BASE_CHAR, length);
    }

    /**
     * 获得一个随机的字符串
     *
     * @param baseString 随机字符选取的样本
     * @param length     字符串的长度
     * @return 随机字符串
     */
    public static String genRandom(String baseString, int length) {
        StringBuffer sb = new StringBuffer();
        if (length < 1) {
            length = 1;
        }
        int baseLength = baseString.length();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(baseLength);
            sb.append(baseString.charAt(number));
        }
        return sb.toString();
    }

    /**
     * 字符串转换成 Integer
     *
     * @param value        需要转换的值
     * @param defaultValue 转换失败默认值
     * @return
     */
    public static Integer toInt(String value, Integer defaultValue) {
        if (isBlank(value)) {
            logger.error(format("Str:{}. is null,can not be converted to int", value));
            return defaultValue;
        }
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            logger.error(format("Str:{}. is null,can not be converted to int", value));
            return defaultValue;
        }

    }

    public static int toInt(String value) {
        Integer i = toInt(value, 0);
        return i.intValue();
    }

    /**
     * 字符串转换成 Boolean
     *
     * @param value        需要转换的值
     * @param defaultValue 转换失败默认值
     * @return
     */
    public static Boolean toBoolean(String value, Boolean defaultValue) {
        if (isBlank(value)) {
            logger.error(format("Str:{}. is null,can not be converted to Boolean", value));
            return defaultValue;
        }
        if ("1".equals(value) || "true".equalsIgnoreCase(value)) {
            return Boolean.TRUE;
        } else if ("0".equals(value) || "false".equalsIgnoreCase(value)) {
            return Boolean.FALSE;
        } else {
            return defaultValue;
        }
    }

    /**
     * 送信审拆分数据字典值专用
     *
     * @param str
     * @return
     */
    public static String toAps(String str) {
        if (isBlank(str)) {
            return EMPTY;
        }

        if (str.contains("-")) {
            return str.split("-")[1];
        } else {
            return str;
        }

    }

    /**
     * 替换字符串
     *
     * @param src
     * @param oldChar
     * @param newChar
     * @return
     */
    public static String replace(String src, String oldChar, String newChar) {
        if (isBlank(src)) {
            return EMPTY;
        }
        if (oldChar == null || newChar == null) {
            return src;
        }
        return src.replace(oldChar, newChar);
    }

    /**
     * 是否全部由数据组成
     *
     * @param val
     * @return
     */
    public static boolean isNum(String val) {
        return val == null || "".equals(val) ? false : val.matches("^[0-9]*$");
    }

    public static boolean isChinese(String val) {
        return val == null || "".equals(val) ? false : val.matches("([\u4e00-\u9fa5]+)");
    }

    /**
     * 去除空格
     *
     * @param str
     * @return
     */
    public static String replaceBlank(String str) {
        String dest = "";
        if (str != null) {
            Pattern p = Pattern.compile("\\s*|\t|\r|\n");
            Matcher m = p.matcher(str);
            dest = m.replaceAll("");
        }
        return dest;
    }

    /**
     * 是否包含
     */
    public static boolean containStr(String tobeCheck, String str) {
        if (tobeCheck.indexOf(str) != -1) {
            return true;
        }
        return false;
    }

    /**
     * string类型转换求和
     *
     * @param str1 待加数
     * @param str2 加数
     * @return 和
     */
    public static String stradd(String str1, String str2) {
        if (str1 == null) {
            str1 = "0";
        }
        if (str2 == null) {
            str2 = "0";
        }
        int s1 = Integer.parseInt(str1);
        int s2 = Integer.parseInt(str2);
        return String.valueOf(s1 + s2);

    }

    /**
     * String 相除
     */
    public static String Divide(String arg1, String arg2) {
        BigDecimal a1 = new BigDecimal(arg1);
        BigDecimal a2 = new BigDecimal(arg2);
        return a1.divide(a2, 2, BigDecimal.ROUND_HALF_DOWN).toString();
    }

    /**
     * String --> BigDecimal String相加
     *
     * @param args
     */
    public static String AddStrByBigDecimal(String arg, String... args) {
        BigDecimal sum = new BigDecimal(arg);
        BigDecimal bemp;
        for (String temp : args) {
            bemp = new BigDecimal(temp);
            sum = sum.add(bemp);
        }
        return sum.toString();
    }

    /**
     * String --> BigDecimal String相减
     *
     * @param args
     */
    public static String SubStrByBigDecimal(String arg, String... args) {
        BigDecimal sum = new BigDecimal(arg);
        BigDecimal bemp;
        for (String temp : args) {
            bemp = new BigDecimal(temp);
            sum = sum.subtract(bemp);
        }
        return sum.toString();
    }

    /**
     * String取最大
     *
     * @param args
     */
    public static String getMaxVal(String arg, String... args) {
        BigDecimal temp = new BigDecimal(arg);
        BigDecimal bemp;
        for (String a : args) {
            bemp = new BigDecimal(a);
            if (temp.compareTo(bemp) < 0) {
                temp = bemp;
            }
        }
        return temp.toString();
    }

    /**
     * String取最小
     *
     * @param args
     */
    public static String getMinVal(String arg, String... args) {
        BigDecimal temp = new BigDecimal(arg);
        BigDecimal bemp;
        for (String a : args) {
            bemp = new BigDecimal(a);
            if (temp.compareTo(bemp) > 0) {
                temp = bemp;
            }
        }
        return temp.toString();
    }

    /**
     * String 比较大小
     *
     * @param args
     */
    public static int compare(String arg1, String arg2) {
        BigDecimal a1 = new BigDecimal(arg1);
        BigDecimal a2 = new BigDecimal(arg2);
        return a1.compareTo(a2);
    }

    /*
     * 将数字转为图片序号  如  1 - 001
     * 暂定100以内
     * */
    public static String numToSerialNum(int num) {
        String serialNum = "";
        if (num <= 9) {
            serialNum = "00" + num;
        } else {
            serialNum = "0" + num;
        }
        return serialNum;
    }


    /**
     * 按分期类型标识返回分期名称
     * @param stageType  分期类型
     * @return
     */
    public static String stagesTranslate(String stageType) {
        String stageStr = "";
        if ("cashStage".equals(stageType)) {
            stageStr = "现金分期";
        } else if ("purStage".equals(stageType)) {
            stageStr = "消费分期";
        } else if ("billStage".equals(stageType)) {
            stageStr = "账单分期";
        } else if ("loandayStage".equals(stageType)) {
            stageStr = "按日贷";
        } else {
            stageStr = "";
        }

        return stageStr;
    }


    public static void main(String[] args) {
        int x = 2;

        System.out.println(numToSerialNum(x++));
    }
}

