package com.beyondsoft.common.utils;

import com.beyondsoft.modules.sys.service.SysConfigService;
import com.thoughtworks.xstream.core.util.Base64Encoder;
import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.crypto.Cipher;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

/**
 * 用于加密机转Pin过程中的加解密部分
 */
@Component
public class RSATool {
	/**
	 * 指定加密算法为RSA
	 */
	private static final String ALGORITHM = "RSA";

	private static final String PROVIDER = "RSA/ECB/NoPadding";
	/**
	 * 密钥长度，用来初始化
	 */
	private static final int KEYSIZE = 1024;
	/**
	 * 指定密钥对存放文件
	 */
	private static Map<Integer, KeyPair> KeyList = new HashMap<Integer, KeyPair>();

	private static Cipher cipher;

	private static SysConfigService sysConfigService;

	@Autowired
	public void setSysConfigService(SysConfigService sysConfigService){
		RSATool.sysConfigService = sysConfigService;
	}


	/**
	 * 生成密钥对
	 *
	 * @param index 密钥索引
	 * @throws Exception
	 */
	private static void generateKeyPair(Integer index) throws Exception {

		/** 为RSA算法创建一个KeyPairGenerator对象 */
		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(ALGORITHM);

		/** 利用上面的随机数据源初始化这个KeyPairGenerator对象 */
		keyPairGenerator.initialize(KEYSIZE);

		/** 生成密钥对 */
		KeyPair keyPair = keyPairGenerator.generateKeyPair();

		KeyList.put(index, keyPair);
	}

	private static String getModulus(Integer index) {
		RSAPublicKey rsaPublicKey = (RSAPublicKey) KeyList.get(index).getPublic();
		return new String(Hex.encodeHex(rsaPublicKey.getModulus().toByteArray()));
	}

	private static String getPublicExponent(Integer index) {
		RSAPublicKey rsaPublicKey = (RSAPublicKey) KeyList.get(index).getPublic();
		return new String(Hex.encodeHex(rsaPublicKey.getPublicExponent().toByteArray()));
	}

	/**
	 * 获得公钥
	 *
	 * @param index 公钥索引 如果没有则生成
	 * @return 数组 数组第一位为Modulus 第二位为 PublicExponent
	 */
	public static String[] getPublicKeys(Integer index) {
		String[] result = new String[2];
		if (!KeyList.containsKey(index)) {
			try {
				generateKeyPair(index);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		result[0] = getModulus(index).substring(2);
		result[1] = getPublicExponent(index).substring(1);
		return result;

	}

	/**
	 * 加密方法
	 *
	 * @param source 源数据
	 * @return
	 * @throws Exception
	 */
	public static String encrypt(String source, Key publicKey) throws Exception {

		/** 得到Cipher对象来实现对源数据的RSA加密 */
		Cipher cipher = Cipher.getInstance("RSA",
				new BouncyCastleProvider());
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);
		byte[] b = source.getBytes();
		/** 执行加密操作 */
		byte[] b1 = cipher.doFinal(b);

		return new String(Hex.encodeHex(b1));
	}

	/**
	 * 解密算法
	 *
	 * @param cryptograph 密文
	 * @param privateKey  密钥索引
	 * @return
	 * @throws Exception
	 */
	public static String decrypt(String cryptograph, Key privateKey) throws Exception {

		/** 得到Cipher对象对已用公钥加密的数据进行RSA解密 */
		if (cipher == null) {
			cipher = Cipher.getInstance("RSA", new BouncyCastleProvider());
		}
		try {
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			byte[] b1 = Hex.decodeHex(cryptograph.toCharArray());

			/** 执行解密操作 */
			byte[] b = cipher.doFinal(b1);
			return new String(b);
		} catch (Exception e) {
			//一旦发生RSA解密错误，重置cipher
			cipher = null;
			System.out.println("cipher reset");
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * String 转换为RSAPrivateKey
	 *
	 * @param keyStr
	 * @return
	 * @throws Exception
	 */
	public static RSAPrivateKey loadPrivateKeyByStr(String keyStr)
			throws Exception {
		try {
			Base64Encoder encoder = new Base64Encoder();
			byte[] buffer = encoder.decode(keyStr);
			PKCS8EncodedKeySpec privatekeySpec = new PKCS8EncodedKeySpec(buffer);
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			RSAPrivateKey rsaPrivateKey = (RSAPrivateKey) keyFactory.generatePrivate(privatekeySpec);
			return rsaPrivateKey;
		} catch (NoSuchAlgorithmException e) {
			throw new Exception("无此算法");
		} catch (InvalidKeySpecException e) {
			throw new Exception("私钥非法");
		} catch (NullPointerException e) {
			throw new Exception("私钥数据为空");
		}
	}

	/**
	 * String 转换为RSAPublickKey
	 *
	 * @param publicKeyStr
	 * @return
	 * @throws Exception
	 */
	public static RSAPublicKey loadPublicKeyByStr(String publicKeyStr)
			throws Exception {
		try {
			Base64Encoder encoder = new Base64Encoder();
			byte[] buffer = encoder.decode(publicKeyStr);
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			X509EncodedKeySpec keySpec = new X509EncodedKeySpec(buffer);
			return (RSAPublicKey) keyFactory.generatePublic(keySpec);
		} catch (NoSuchAlgorithmException e) {
			throw new Exception("无此算法");
		} catch (InvalidKeySpecException e) {
			throw new Exception("公钥非法");
		} catch (NullPointerException e) {
			throw new Exception("公钥数据为空");
		}
	}

	/**
	 * RSA公钥解密数据
	 *
	 * @param encodeStr
	 * @return
	 */
	public static String decodeByPrivateKey(String encodeStr, String bankNum) throws Exception {

		Security.addProvider(new BouncyCastleProvider());
		String privateKey = sysConfigService.getValue("rsaPrivateKey");

	/*	String privateKey = "MIICXAIBAAKBgQDEtDoq+/qgLi/tmYqcFkKqKiDE6XeTqyRxN+Qdm3bgX4HTd1OwtcwTsoii73bJ9Nxik74WTnAd/BJtLFZPkpVSpOvz/ofRnUXDnMn52q/VkRCBQARWUS18tyIRxHxocaA0GKSrVfbUoT4Ow/M2R+VzoNH1G5IB/6big/J2ga8/cQIDAQABAoGAZnZjHmTnZmzotmGWnDvUOgN70lay3Q73CqrMfseuoq3dWKP9yjzIszH6jrUiMY0FB6xiBgiRymy1xHyl8QkwElRcTq6Poy+BZLoki1dM"
				+"r501BkCnu1uYe91hvYNUCVDpgVUidmNf1ByOAuC0Z9uNk1hA3uY4lBF25Il0TKNNvakCQQDv4hVMsD3aM4+56OZI5hyy2h8nA6bbVyk2eKkTTPWhkNC4tNlWHAJMc4r1Gl011i+u9UPvGDNzBwPCmFw2Xl4HAkEA0et56r3uvvjqFbZ0xbnFwIbWFEdH2DHoX4ybLqWFw1Xvy7aug/hXZ7eyWzQH/9s7FQ+GyAYBaDPHp+TodYCYxwJAJ1ChdwknifyLYMfX/jigmXuAQY+qqCY33fKWm1AwrcMjQjJ5sFA03"
				+"DzPmG4yuqEY/Y67DJOl0wNiTWFVM5/xvwJAJ3fgqlQfqWRfjsmJVwvAtJwlcBae2XU4vY5QNwsEBWAJAAJO+Z0dwfB15lP4FREBIMqUitMmPJNPvK97TWxqTQJBAKIjH4C6c2RTzoJtNzh1pEp0qwjWLzgRsDFthYglTlt3GedpsRdqlhwDWnC1FrMQ8Cmkcg7Qk0nBnghV2Rvh7Rk=";
*/
		Base64Encoder encoder = new Base64Encoder();
		byte[] buffer = encoder.decode(privateKey);
		PKCS8EncodedKeySpec privatekeySpec = new PKCS8EncodedKeySpec(buffer);
		KeyFactory keyFactory;
		keyFactory = KeyFactory.getInstance("RSA");
		RSAPrivateKey rsaPrivateKey = (RSAPrivateKey) keyFactory.generatePrivate(privatekeySpec);
		String[] encstrArray = encodeStr.split(" ");
		String decstr = "";
		for (int i = 0; i < encstrArray.length; i++) {
			if (encstrArray[i].length() != 256) {
				System.out.println(("encstrArray[" + i + "] : " + encstrArray[i]));
				throw new Exception("encstrArray length fail : " + encstrArray[i].length());
			}
			decstr = RSATool.decrypt(encstrArray[i], rsaPrivateKey) + decstr;
		}
		return decstr;
	}

	public static void main(String[] args) throws Exception {
//        String source = "恭喜发财!";// 要加密的字符串
//        System.out.println("准备用公钥加密的字符串为：" + source);
//
//        generateKeyPair(1);
//
//        System.out.println("公共模数为:" + getPublicKeys(1)[0]);
//        System.out.println("公钥为:" + getPublicKeys(1)[1]);
//
//        String cryptograph = encrypt(source,1);// 生成的密文
//        System.out.print("用公钥加密后的结果为:" + cryptograph);
//        System.out.println();
//
//        String target = decrypt(cryptograph,1);// 解密密文
//        System.out.println("用私钥解密后的字符串为：" + target);
//        System.out.println();
		Security.addProvider(new BouncyCastleProvider());
		InputStream inputStream = null;
		inputStream = RSATool.class.getClassLoader().getResourceAsStream("rsa_public_key.pem");
		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
		StringBuilder sb = new StringBuilder();
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		String publicKey = sb.toString();
		publicKey = publicKey.replace("-----BEGIN PUBLIC KEY-----", "").replace("-----END PUBLIC KEY-----", "");
		Base64Encoder encoder = new Base64Encoder();
		byte[] buffer = encoder.decode(publicKey);
		X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(buffer);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		RSAPublicKey rsaPublicKey = (RSAPublicKey) keyFactory.generatePublic(publicKeySpec);

		//System.out.println(rsaPublicKey.getModulus().toString(16).toUpperCase());
		//System.out.println(rsaPublicKey.getPublicExponent().toString(16));

		String encStr = encrypt("06123456", rsaPublicKey);
		//System.out.println(encStr);

		inputStream = RSATool.class.getClassLoader().getResourceAsStream("rsa_private_key.pem");
		reader = new BufferedReader(new InputStreamReader(inputStream));
		sb = new StringBuilder();
		line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		String privateKey = sb.toString();
//    	String privateKey = "MIICXAIBAAKBgQDEtDoq+/qgLi/tmYqcFkKqKiDE6XeTqyRxN+Qdm3bgX4HTd1OwtcwTsoii73bJ9Nxik74WTnAd/BJtLFZPkpVSpOvz/ofRnUXDnMn52q/VkRCBQARWUS18tyIRxHxocaA0GKSrVfbUoT4Ow/M2R+VzoNH1G5IB/6big/J2ga8/cQIDAQABAoGAZnZjHmTnZmzotmGWnDvUOgN70lay3Q73CqrMfseuoq3dWKP9yjzIszH6jrUiMY0FB6xiBgiRymy1xHyl8QkwElRcTq6Poy+BZLoki1dMr501BkCnu1uYe91hvYNUCVDpgVUidmNf1ByOAuC0Z9uNk1hA3uY4lBF25Il0TKNNvakCQQDv4hVMsD3aM4+56OZI5hyy2h8nA6bbVyk2eKkTTPWhkNC4tNlWHAJMc4r1Gl011i+u9UPvGDNzBwPCmFw2Xl4HAkEA0et56r3uvvjqFbZ0xbnFwIbWFEdH2DHoX4ybLqWFw1Xvy7aug/hXZ7eyWzQH/9s7FQ+GyAYBaDPHp+TodYCYxwJAJ1ChdwknifyLYMfX/jigmXuAQY+qqCY33fKWm1AwrcMjQjJ5sFA03DzPmG4yuqEY/Y67DJOl0wNiTWFVM5/xvwJAJ3fgqlQfqWRfjsmJVwvAtJwlcBae2XU4vY5QNwsEBWAJAAJO+Z0dwfB15lP4FREBIMqUitMmPJNPvK97TWxqTQJBAKIjH4C6c2RTzoJtNzh1pEp0qwjWLzgRsDFthYglTlt3GedpsRdqlhwDWnC1FrMQ8Cmkcg7Qk0nBnghV2Rvh7Rk=";
		privateKey = privateKey.replace("-----BEGIN PRIVATE KEY-----", "").replace("-----END PRIVATE KEY-----", "");

		buffer = encoder.decode(privateKey);
		PKCS8EncodedKeySpec privatekeySpec = new PKCS8EncodedKeySpec(buffer);
		RSAPrivateKey rsaPrivateKey = (RSAPrivateKey) keyFactory.generatePrivate(privatekeySpec);
		String pwd = "BD2845941129FA44B037512EA948552000086E0A34A04A65D16AA73783F3E6EE21D2DB5B0B5F59933F9C398E043A906729125F3BB0FFAAC3F048FFA77A35B9418D2F15CE08E53D4A554D2D362633DA16588F9ACA5AAF400D945B4F72B2531C96CEB0FE6FAB09C03F65712C712F5F4534AF7ACFB241EA2D265D33321080FB7DE3 AB7C54D8D73DDEC41F71BC32D18EF930E845DFC65D51860E9FEE26F958F03221C21EF2563BCB8A6CB7AFE63520DFCADCBCC1F191AD1D518539080288B0098D7C4AFADBB3D94E58AD2A0C1EF375211ABEA28D6772BD350554A1D37E47AF8C3207749AAEBE0A3E91240C14B81B1AB04DD12B92AD47746BC2189F7E2851E30FE422 2191E6CB7517E27EE6C611BBB85C9009658AECA209284D52E9785CB75226AEF40673709E5AFD63B62ABAD2E867FAFE42B6DC7D38663725F8ADD3B673B465BD57CC75591B4254C80B10A94EF92ED220FCFA0E69481C718E17D1468AA91D162AD952EC6BA3CB65973AD9B32CA71AEA4D22A30B9B1D6A936A538C4008BAACAA9FB5";
		String[] encstrArray = pwd.split(" ");
		String decstr = "";
		for (int i = 0; i < encstrArray.length; i++) {
//			log.info("encstrArray["+i+"] : " + encstrArray[i]);
			if (encstrArray[i].length() != 256) {
				System.out.println(("encstrArray[" + i + "] : " + encstrArray[i]));
				throw new Exception("encstrArray length fail : " + encstrArray[i].length());
			}
			decstr = decrypt(encstrArray[i], rsaPrivateKey) + decstr;
		}


		//String dec = decrypt("80F7070B48FA83DD47FCFA6FDB5124CA1C889405B712F6DAFD2B6F09A18283914971A3C9195A72B6F94C61BFB652DA517AC077C2C8CAA42C13AD69C509137D1B7671F64C0ADE0538AF63B95075CE979DCAF39118B07829C38258171485912D5F72DA2631EFAF1B2081805A0B565DAD2667D01398B6B9189D7AA117D5AC09593A", rsaPrivateKey);
		System.out.println(decstr);
	}
}
