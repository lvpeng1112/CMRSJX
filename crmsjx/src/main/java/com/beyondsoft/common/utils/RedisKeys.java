/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.common.utils;

/**
 * Redis所有Keys
 *
 * @author shuaigao@cupdata.com
 */
public class RedisKeys {

    public static String getSysConfigKey(String key){
        return "sys:config:" + key;
    }


    /**
     * 常规
     * @param key
     * @return
     */
    public static String getBeehiveMonitorKey(String key){
        return "beehive:monitor:" + key;
    }

    /**
     * PV
     * @param key
     * @return
     */
    public static String getBeehiveMonitorPvKey(String key){
        return "beehive:pv:" + key;
    }

    /**
     * UV
     * @param key
     * @return
     */
    public static String getBeehiveMonitorUvKey(String key){
        return "beehive:uv:" + key;
    }

    /**
     * 分期stage
     * @param key
     * @return
     */
    public static String getBeehiveMonitorStageKey(String key){
        return "beehive:stage:" + key;
    }

    /**
     * 激活active
     * @param key
     * @return
     */
    public static String getBeehiveMonitorActiveKey(String key){
        return "beehive:active:" + key;
    }
}
