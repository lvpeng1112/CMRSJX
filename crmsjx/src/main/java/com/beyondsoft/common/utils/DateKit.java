/**
 * 
 */
package com.beyondsoft.common.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author zhenhuawang
 *
 */
public class DateKit {
	
	/**
	 * yyyy-MM-dd
	 */
	public static String formaterDay = "yyyy-MM-dd";
	
	public static String yyyyMMdd = "yyyyMMdd";

	public static String HHmmss = "HHmmss";

	public static String yyyyMMddHHmmss = "yyyyMMddHHmmss";

	public static String MMddHHmmss = "MMddHHmmss";

	/**
	 * yyyy-MM-dd HH:mm:ss
	 */
	public static String formaterDateTime = "yyyy-MM-dd HH:mm:ss";
	
	/**
	 * 得到系统日期
	 * 
	 * @param format 时间格式
	 * @return
	 */
	public static String getCurrDate(String format) {
		SimpleDateFormat formater = new SimpleDateFormat(format);
		return formater.format(new Date());
	}
	
	
	public static String getPrevMonth() {
		return getPrevMonth(formaterDay);
	}
	
	/**
	 * 得到上月日期
	 * 
	 * @param format
	 * @return
	 */
	public static String getPrevMonth(String format) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		return getFormaterDate(format, cal);
	}
	
	/**
	 * 得到下月日期
	 * 
	 * @param format
	 * @return
	 */
	public static String getNextMonth(String format) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, 1);
		return getFormaterDate(format, cal);
	}
	
	/**
	 * 得到当月的第一天 
	 * @return yyyy-MM-dd
	 */
	public static String getMonthFirstDay()
	{
		 return getMonthFirstDay(formaterDay);
	}

	/**
	 * 
	 */
	public static String getNextDay(String format){
		 Calendar c = Calendar.getInstance();   
	     c.add(Calendar.DAY_OF_MONTH, 1);// 今天+1天  
	     return getFormaterDate(format, c);
	}

	/**
	 * 指定日期开始往后加n天
	 */
	public static String calcDay(String yyyyMMdd, int n){
		Calendar c = Calendar.getInstance();
		c.set(Integer.parseInt(yyyyMMdd.substring(0, 4)), Integer.parseInt(yyyyMMdd.substring(4, 6)) - 1, Integer.parseInt(yyyyMMdd.substring(6)));
		c.add(Calendar.DAY_OF_MONTH, n);
		return getFormaterDate(DateKit.yyyyMMdd, c);
	}

	/**
	 * 指定日期开始往后加n月
	 */
	public static String calcMonth(String yyyyMMdd, int n){
		Calendar c = Calendar.getInstance();
		c.set(Integer.parseInt(yyyyMMdd.substring(0, 4)), Integer.parseInt(yyyyMMdd.substring(4, 6)) - 1, Integer.parseInt(yyyyMMdd.substring(6)));
		c.add(Calendar.MONTH, n);
		return getFormaterDate(DateKit.yyyyMMdd, c);
	}
	
	/**
	 * 当天开始往后加n天
	 */
	public static String getNextDayFate(String format,int n){
		 Calendar c = Calendar.getInstance();   
	     c.add(Calendar.DAY_OF_MONTH, n);// 今天+1天  
	     return getFormaterDate(format, c);
	}
	
	public static String getNextNYear(String format,int n){
		Calendar c = Calendar.getInstance();
		c.add(Calendar.YEAR, n);
		return getFormaterDate(format, c);
	}
	
	public static void main(String[] args) {
		String s = getNextNYear(DateKit.yyyyMMdd,4);
		System.out.println(s);
	}
	
	/**
	 * 得到当月的第一天
	 * 
	 * @param format
	 * @return
	 */
	public static String getMonthFirstDay(String format) {
		Calendar calendar = Calendar.getInstance();     
	    //calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));  
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		return getFormaterDate(format, calendar);
	}

	/**
	 * curDate是否在startDate至endDate期间
	 * @param curDate
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static Boolean isBetweenDate(String curDate, String startDate, String endDate, String format) {
		Boolean isbetween = false;
		Date compDate = null;
		Date stDate = null;
		Date edDate = null;
		SimpleDateFormat df = new SimpleDateFormat(format);
		df.setLenient(false);
		try {
			if("00000000".equals(curDate)){
				return false;
			}
			compDate = df.parse(curDate);
			stDate = df.parse(startDate);
			edDate = df.parse(endDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		if(compDate.after(stDate) && compDate.before(edDate)){
			isbetween = true;
		}

		return isbetween;
	}
	
	/**
	 * 格式化时间
	 * @param format 时间格式
	 * @param cal 时间
	 * @return
	 */
	public static String getFormaterDate(String format, Calendar cal) {
		SimpleDateFormat formater = new SimpleDateFormat(format);
		return formater.format(cal.getTime());
	}
	
	/**
	 * 格式化时间
	 * @param format 时间格式
	 * @param date 时间
	 * @return
	 */
	public static String getFormaterDate(String format, Date date) {
		SimpleDateFormat formater = new SimpleDateFormat(format);
		return formater.format(date);
	}

	/**
	 * 格式化时间
	 * @param formatOld 原来时间格式
	 * @param formatNew 新的时间格式
	 * @param date string类型时间
	 * @return
	 */
	public static String getFormaterDate(String formatOld, String formatNew, String date) {
		try {
			SimpleDateFormat formaterO = new SimpleDateFormat(formatOld);
			SimpleDateFormat formaterN = new SimpleDateFormat(formatNew);
			Date d = formaterO.parse(date);
			date = formaterN.format(d);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * String转Date
	 * @param format 时间格式
	 * @param date 时间
	 * @return
	 */
	public static Date getFormaterDate(String format, String date) {
		try {
			SimpleDateFormat formater = new SimpleDateFormat(format);
			return formater.parse(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 时间格式转换HH:mm转换为HHmm
	 * @param date
	 * @return
	 */
	public static String fmtTimeHHmm1(String date) {
		String rtnDate = "";
		if(date==null||date.length()!=5) date = "0000";
		rtnDate = StrKit.trim(date.replace(":",""));
		return rtnDate;
	}

	/**
	 * 时间格式转换 HHmm转换为HH:mm
	 * @param date
	 * @return
	 */
	public static String fmtTimeHHmm(String date) {
		String rtnDate = "";
		if(date==null||date.trim().length()!=4) date = "0000";
		rtnDate = date.substring(0,2)+":"+date.substring(2);
		return rtnDate;
	}

	/**
	 * 获取N天前的日期
	 *
	 * @param n
	 * @return yyyyMMdd
	 */
	public static String getLastDateByN(int n) {
		Date date = new Date(System.currentTimeMillis());
		for (int i = 0; i < n; i++) {
			date.setTime(date.getTime() - (24 * 60 * 60 * 1000));
		}
		return new SimpleDateFormat("yyyyMMdd").format(date);
	}

}
