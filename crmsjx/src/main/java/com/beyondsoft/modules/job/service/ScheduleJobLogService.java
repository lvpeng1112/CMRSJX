/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.job.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.modules.job.entity.ScheduleJobLogEntity;
import com.beyondsoft.common.utils.PageUtils;

import java.util.Map;

/**
 * 定时任务日志
 *
 * @author luochao
 */
public interface ScheduleJobLogService extends IService<ScheduleJobLogEntity> {

	PageUtils queryPage(Map<String, Object> params);
	
}
