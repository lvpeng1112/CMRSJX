/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.job.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.aofp.entity.CMngCheckBaseInfo;
import com.beyondsoft.modules.job.entity.CMngCheckBaseInfoJobEntity;
import com.beyondsoft.modules.job.entity.CmngCheckBaseInfoJobBatchHisEntity;
import com.beyondsoft.modules.job.entity.ScheduleJobEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 定时任务
 *
 * @author luochao
 */
@Mapper
public interface ScheduleJobDao extends BaseMapper<ScheduleJobEntity> {
	
	/**
	 * 批量更新状态
	 */
	int updateBatch(Map<String, Object> map);

	/**
	 * 查询营销人员表所有数据
	 * @return
	 */
	List<Map<String,Object>> selectBaseInfoDate (Map reqPara);

	/**
	 * 新增营销人员基本信息考核表数据
	 */
	void insertByBatch (CMngCheckBaseInfo cMngCheckBaseInfo);

	/**
	 * 大数据平台数据回灌时进行跑批历史日志记录
	 */
	void insertBatchHis(CmngCheckBaseInfoJobBatchHisEntity cmngCheckBaseInfoJobBatchHisEntity);

	/**
	 * 根据跑批日期进行删除
	 */
	void deleteBatchHisDate(Map map);
	/**
	 * 查询团队信息表数据
	 */
	List<Map<String,Object>> selectTeamDate (Map reqPara);

	/**
	 * 查询团队成员信息表数据
	 */
	List<Map<String,Object>> selectTeamEmployeeDate (Map reqPara);
}
