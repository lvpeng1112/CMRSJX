/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.job.task;

import com.beyondsoft.common.utils.FileUtis;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.job.dao.ScheduleJobDao;
import com.beyondsoft.utils.DB2Util;
import com.beyondsoft.utils.PgsqlUtil;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;


/**
 *
 * insertTeamDataTask为spring bean的名称
 * 次月插入上月团队数据
 * @author luochao
 */
@Component("insertTeamDataTask")
public class InsertTeamDataTask implements ITask{

	@Autowired(required = false)
	ScheduleJobDao scheduleJobDao;

	@SneakyThrows
	public void run(String param){
		Map params = new HashMap();
		//查询团队信息表数据
		List<Map<String, Object>> teamDataList = scheduleJobDao.selectTeamDate(params);
		//查询团队信息表数据
		List<Map<String,Object>> teamEmployeeDataList = scheduleJobDao.selectTeamEmployeeDate(params);
		//执行插入团队表数据操作
		Connection conn1 = PgsqlUtil.getConn();
		PreparedStatement pre1 = null; //预编译
		ResultSet rs1 = null;
		long TimeStart = System.currentTimeMillis();//获取当前时间
		//执行删除团队表数据操作
		String sqlOne = "delete from npmm.NB_TEAM_NEW";
		//执行插入数据操作
		String sqlTwo = "INSERT\n" +
				"INTO\n" +
				"    NPMM.NB_TEAM_NEW\n" +
				"    (\n" +
				"        CH_TEAM_CODE,\n" +
				"        CH_TEAM_NAME,\n" +
				"        CH_TEAM_DESC,\n" +
				"        CH_ORG_ID,\n" +
				"        LEADER_ID,\n" +
				"        TERM_START_DT,\n" +
				"        TERM_END_DT,\n" +
				"        SETUP_DT,\n" +
				"        CREATE_USER_ID,\n" +
				"        CH_LAST_MODIFY,\n" +
				"        CH_LAST_MODIFIER,\n" +
				"        STATUS\n" +
				"    )\n" +
				"    VALUES\n" +
				"    (\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?\n" +
				"    )";
		try {
			// 取消自动提交（由于要进行多次操作）
			conn1.setAutoCommit(false);
			pre1 = conn1.prepareStatement(sqlOne);
			pre1.executeUpdate();
			pre1 = conn1.prepareStatement(sqlTwo);
			for (int i = 0; i < teamDataList.size(); i++) {
				pre1.setString(1, teamDataList.get(i).get("CH_TEAM_CODE").toString());
				pre1.setString(2, teamDataList.get(i).get("CH_TEAM_NAME").toString());
				pre1.setString(3, teamDataList.get(i).get("CH_TEAM_DESC").toString());
				pre1.setString(4, teamDataList.get(i).get("CH_ORG_ID").toString());
				pre1.setString(5, teamDataList.get(i).get("LEADER_ID").toString());
				pre1.setString(6, teamDataList.get(i).get("TERM_START_DT").toString());
				pre1.setString(7, teamDataList.get(i).get("TERM_END_DT").toString());
				pre1.setString(8, teamDataList.get(i).get("SETUP_DT").toString());
				pre1.setString(9, teamDataList.get(i).get("CREATE_USER_ID").toString());
				pre1.setString(10, teamDataList.get(i).get("CH_LAST_MODIFY").toString());
				pre1.setString(11, teamDataList.get(i).get("CH_LAST_MODIFIER").toString());
				pre1.setString(12, teamDataList.get(i).get("STATUS").toString());
				pre1.addBatch();//批量插入
			}
			pre1.executeBatch();
			conn1.commit();
			pre1.clearBatch();
		} catch (SQLException e) {
			e.printStackTrace();
			// 发生异常，回滚
		} finally {
			PgsqlUtil.close(conn1, pre1, rs1);
		}
        //执行插入团队成员表数据操作
		Connection conn2 = PgsqlUtil.getConn();
		PreparedStatement pre2 = null; //预编译
		ResultSet rs2 = null;
		//先执行删除团队成员表数据操作
		String sqlThree = "delete from npmm.NB_TEAM_EMPLEE_NEW";
		//执行插入数据操作
		String sqlFour = "INSERT\n" +
				"INTO\n" +
				"    NPMM.NB_TEAM_EMPLEE_NEW\n" +
				"    (\n" +
				"        CH_TEAM_CODE,\n" +
				"        CH_ORG_ID,\n" +
				"        CH_EMPLEE_CODE,\n" +
				"        CH_EMPLEE_NAME,\n" +
				"        SETUP_DT,\n" +
				"        CH_LAST_MODIFY,\n" +
				"        CH_LAST_MODIFIER,\n" +
				"        STATUS\n" +
				"    )\n" +
				"    VALUES\n" +
				"    (\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?\n" +
				"    )";
		try {
			// 取消自动提交（由于要进行多次操作）
			conn2.setAutoCommit(false);
			pre2 = conn2.prepareStatement(sqlThree);
			pre2.executeUpdate();
			pre2 = conn2.prepareStatement(sqlFour);
			for (int i = 0; i <teamEmployeeDataList.size() ; i++) {
				pre2.setString(1,teamEmployeeDataList.get(i).get("CH_TEAM_CODE").toString());
				pre2.setString(2,teamEmployeeDataList.get(i).get("CH_ORG_ID").toString());
				pre2.setString(3,teamEmployeeDataList.get(i).get("CH_EMPLEE_CODE").toString());
				pre2.setString(4,teamEmployeeDataList.get(i).get("CH_EMPLEE_NAME").toString());
				pre2.setString(5,teamEmployeeDataList.get(i).get("SETUP_DT").toString());
				pre2.setString(6,teamEmployeeDataList.get(i).get("CH_LAST_MODIFY").toString());
				pre2.setString(7,teamEmployeeDataList.get(i).get("CH_LAST_MODIFIER").toString());
				pre2.setString(8,teamEmployeeDataList.get(i).get("STATUS").toString());
				pre2.addBatch();//批量插入
			}
			pre2.executeBatch();
			conn2.commit();
			pre2.executeBatch();
		} catch (SQLException e) {
			e.printStackTrace();
			// 发生异常，回滚
		} finally {
			PgsqlUtil.close(conn2, pre2, rs2);
		}
		long TimeStop = System.currentTimeMillis();
		System.out.println("总耗时：" + (TimeStop - TimeStart));
	}
}
