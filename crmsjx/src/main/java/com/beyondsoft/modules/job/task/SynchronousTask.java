/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.job.task;
import com.beyondsoft.modules.job.dao.ScheduleJobDao;
import com.beyondsoft.utils.PgsqlUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 *
 * synchronousTask为spring bean的名称
 *
 * @author luochao
 */
@Component("synchronousTask")
public class SynchronousTask implements ITask{
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired(required = false)
	ScheduleJobDao scheduleJobDao;

	public void run(String param){
        Map params = new HashMap();
		//查询营销人员考核基础信息表数据
		List<Map<String,Object>> baseInfoList = scheduleJobDao.selectBaseInfoDate(params);
		Connection conn = PgsqlUtil.getConn();
		PreparedStatement pre = null; //预编译
		ResultSet rs = null;
		long TimeStart = System.currentTimeMillis();//获取当前时间
		//先执行删除表数据操作
		String sqlOne = "delete from npmm.C_MNG_CHECK_BASE_INFO_NEW";
		//执行插入数据操作
		String sqlTwo = "INSERT\n" +
				"INTO\n" +
				"    NPMM.C_MNG_CHECK_BASE_INFO_NEW\n" +
				"    (\n" +
				"        YR,\n" +
				"        ORG_ID,\n" +
				"        CUST_MNGR_ID,\n" +
				"        CUST_MNGR_NM,\n" +
//				"        TEAM_ID,\n" +
//				"        TEAM_NM,\n" +
//				"        CH_TEAM_CODE,\n" +
//				"        CH_TEAM_NAME,\n" +
				"        IN_TM,\n" +
				"        DEPOSIT_DT_AVG_BAL_1,\n" +
				"        DEPOSIT_DT_AVG_BAL_2,\n" +
				"        DEPOSIT_DT_AVG_BAL_3,\n" +
				"        DEPOSIT_DT_AVG_BAL_4,\n" +
				"        DEPOSIT_DT_AVG_BAL_5,\n" +
				"        DEPOSIT_DT_AVG_BAL_6,\n" +
				"        DEPOSIT_DT_AVG_BAL_7,\n" +
				"        DEPOSIT_DT_AVG_BAL_8,\n" +
				"        DEPOSIT_DT_AVG_BAL_9,\n" +
				"        DEPOSIT_DT_AVG_BAL_10,\n" +
				"        DEPOSIT_DT_AVG_BAL_11,\n" +
				"        DEPOSIT_DT_AVG_BAL_12,\n" +
				"        LOAN_DT_AVG_BAL_1,\n" +
				"        LOAN_DT_AVG_BAL_2,\n" +
				"        LOAN_DT_AVG_BAL_3,\n" +
				"        LOAN_DT_AVG_BAL_4,\n" +
				"        LOAN_DT_AVG_BAL_5,\n" +
				"        LOAN_DT_AVG_BAL_6,\n" +
				"        LOAN_DT_AVG_BAL_7,\n" +
				"        LOAN_DT_AVG_BAL_8,\n" +
				"        LOAN_DT_AVG_BAL_9,\n" +
				"        LOAN_DT_AVG_BAL_10,\n" +
				"        LOAN_DT_AVG_BAL_11,\n" +
				"        LOAN_DT_AVG_BAL_12,\n" +
				"        NET_INCM_1,\n" +
				"        NET_INCM_2,\n" +
				"        NET_INCM_3,\n" +
				"        NET_INCM_4,\n" +
				"        NET_INCM_5,\n" +
				"        NET_INCM_6,\n" +
				"        NET_INCM_7,\n" +
				"        NET_INCM_8,\n" +
				"        NET_INCM_9,\n" +
				"        NET_INCM_10,\n" +
				"        NET_INCM_11,\n" +
				"        NET_INCM_12,\n" +
				"        DEPOSIT_NEW_OPEN_NUM_1,\n" +
				"        DEPOSIT_NEW_OPEN_NUM_2,\n" +
				"        DEPOSIT_NEW_OPEN_NUM_3,\n" +
				"        DEPOSIT_NEW_OPEN_NUM_4,\n" +
				"        DEPOSIT_NEW_OPEN_NUM_5,\n" +
				"        DEPOSIT_NEW_OPEN_NUM_6,\n" +
				"        DEPOSIT_NEW_OPEN_NUM_7,\n" +
				"        DEPOSIT_NEW_OPEN_NUM_8,\n" +
				"        DEPOSIT_NEW_OPEN_NUM_9,\n" +
				"        DEPOSIT_NEW_OPEN_NUM_10,\n" +
				"        DEPOSIT_NEW_OPEN_NUM_11,\n" +
				"        DEPOSIT_NEW_OPEN_NUM_12,\n" +
				"        LOAN_MNG_CUST_CUST_NUM_1,\n" +
				"        LOAN_MNG_CUST_CUST_NUM_2,\n" +
				"        LOAN_MNG_CUST_CUST_NUM_3,\n" +
				"        LOAN_MNG_CUST_CUST_NUM_4,\n" +
				"        LOAN_MNG_CUST_CUST_NUM_5,\n" +
				"        LOAN_MNG_CUST_CUST_NUM_6,\n" +
				"        LOAN_MNG_CUST_CUST_NUM_7,\n" +
				"        LOAN_MNG_CUST_CUST_NUM_8,\n" +
				"        LOAN_MNG_CUST_CUST_NUM_9,\n" +
				"        LOAN_MNG_CUST_CUST_NUM_10,\n" +
				"        LOAN_MNG_CUST_CUST_NUM_11,\n" +
				"        LOAN_MNG_CUST_CUST_NUM_12,\n" +
				"        NET_INCM_BASE_1,\n" +
				"        NET_INCM_BASE_2,\n" +
				"        NET_INCM_BASE_3,\n" +
				"        NET_INCM_BASE_4,\n" +
				"        NET_INCM_BASE_5,\n" +
				"        NET_INCM_BASE_6,\n" +
				"        NET_INCM_BASE_7,\n" +
				"        NET_INCM_BASE_8,\n" +
				"        NET_INCM_BASE_9,\n" +
				"        NET_INCM_BASE_10,\n" +
				"        NET_INCM_BASE_11,\n" +
				"        NET_INCM_BASE_12,\n" +
				"        OPERATEDATE,\n" +
				"        OPERATOR,\n" +
				"        REMARK,\n" +
				"        MD5\n" +
				"    )\n" +
				"    VALUES\n" +
				"    (\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
//				"        ?,\n" +
//				"        ?,\n" +
//				"        ?,\n" +
//				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?,\n" +
				"        ?\n" +
				"    )";
		try {
			// 取消自动提交（由于要进行多次操作）
			conn.setAutoCommit(false);
			pre = conn.prepareStatement(sqlOne);
			pre.executeUpdate();
			pre = conn.prepareStatement(sqlTwo);
			for (int i = 0; i <baseInfoList.size() ; i++) {
				pre.setString(1,baseInfoList.get(i).get("YR").toString());
				pre.setString(2,baseInfoList.get(i).get("ORG_ID").toString());
				pre.setString(3,baseInfoList.get(i).get("CUST_MNGR_ID").toString());
				pre.setString(4,baseInfoList.get(i).get("CUST_MNGR_NM").toString());
//				pre.setString(5,baseInfoList.get(i).get("TEAM_ID").toString());
//				pre.setString(6,baseInfoList.get(i).get("TEAM_NM").toString());
//				pre.setString(7,baseInfoList.get(i).get("CH_TEAM_CODE").toString());
//				pre.setString(8,baseInfoList.get(i).get("CH_TEAM_NAME").toString());
				pre.setString(5,baseInfoList.get(i).get("IN_TM").toString());
				pre.setBigDecimal(6,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_DT_AVG_BAL_1").toString())));
				pre.setBigDecimal(7,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_DT_AVG_BAL_2").toString())));
				pre.setBigDecimal(8,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_DT_AVG_BAL_3").toString())));
				pre.setBigDecimal(9,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_DT_AVG_BAL_4").toString())));
				pre.setBigDecimal(10,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_DT_AVG_BAL_5").toString())));
				pre.setBigDecimal(11,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_DT_AVG_BAL_6").toString())));
				pre.setBigDecimal(12,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_DT_AVG_BAL_7").toString())));
				pre.setBigDecimal(13,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_DT_AVG_BAL_8").toString())));
				pre.setBigDecimal(14,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_DT_AVG_BAL_9").toString())));
				pre.setBigDecimal(15,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_DT_AVG_BAL_10").toString())));
				pre.setBigDecimal(16,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_DT_AVG_BAL_11").toString())));
				pre.setBigDecimal(17,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_DT_AVG_BAL_12").toString())));
				pre.setBigDecimal(18,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_DT_AVG_BAL_1").toString())));
				pre.setBigDecimal(19,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_DT_AVG_BAL_2").toString())));
				pre.setBigDecimal(20,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_DT_AVG_BAL_3").toString())));
				pre.setBigDecimal(21,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_DT_AVG_BAL_4").toString())));
				pre.setBigDecimal(22,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_DT_AVG_BAL_5").toString())));
				pre.setBigDecimal(23,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_DT_AVG_BAL_6").toString())));
				pre.setBigDecimal(24,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_DT_AVG_BAL_7").toString())));
				pre.setBigDecimal(25,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_DT_AVG_BAL_8").toString())));
				pre.setBigDecimal(26,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_DT_AVG_BAL_9").toString())));
				pre.setBigDecimal(27,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_DT_AVG_BAL_10").toString())));
				pre.setBigDecimal(28,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_DT_AVG_BAL_11").toString())));
				pre.setBigDecimal(29,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_DT_AVG_BAL_12").toString())));
				pre.setBigDecimal(30,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_1").toString())));
				pre.setBigDecimal(31,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_2").toString())));
				pre.setBigDecimal(32,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_3").toString())));
				pre.setBigDecimal(33,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_4").toString())));
				pre.setBigDecimal(34,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_5").toString())));
				pre.setBigDecimal(35,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_6").toString())));
				pre.setBigDecimal(36,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_7").toString())));
				pre.setBigDecimal(37,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_8").toString())));
				pre.setBigDecimal(38,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_9").toString())));
				pre.setBigDecimal(39,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_10").toString())));
				pre.setBigDecimal(40,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_11").toString())));
				pre.setBigDecimal(41,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_12").toString())));
				pre.setBigDecimal(42,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_NEW_OPEN_NUM_1").toString())));
				pre.setBigDecimal(43,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_NEW_OPEN_NUM_2").toString())));
				pre.setBigDecimal(44,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_NEW_OPEN_NUM_3").toString())));
				pre.setBigDecimal(45,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_NEW_OPEN_NUM_4").toString())));
				pre.setBigDecimal(46,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_NEW_OPEN_NUM_5").toString())));
				pre.setBigDecimal(47,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_NEW_OPEN_NUM_6").toString())));
				pre.setBigDecimal(48,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_NEW_OPEN_NUM_7").toString())));
				pre.setBigDecimal(49,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_NEW_OPEN_NUM_8").toString())));
				pre.setBigDecimal(50,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_NEW_OPEN_NUM_9").toString())));
				pre.setBigDecimal(51,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_NEW_OPEN_NUM_10").toString())));
				pre.setBigDecimal(52,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_NEW_OPEN_NUM_11").toString())));
				pre.setBigDecimal(53,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_NEW_OPEN_NUM_12").toString())));
				pre.setBigDecimal(54,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_MNG_CUST_CUST_NUM_1").toString())));
				pre.setBigDecimal(55,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_MNG_CUST_CUST_NUM_2").toString())));
				pre.setBigDecimal(56,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_MNG_CUST_CUST_NUM_3").toString())));
				pre.setBigDecimal(57,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_MNG_CUST_CUST_NUM_4").toString())));
				pre.setBigDecimal(58,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_MNG_CUST_CUST_NUM_5").toString())));
				pre.setBigDecimal(59,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_MNG_CUST_CUST_NUM_6").toString())));
				pre.setBigDecimal(60,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_MNG_CUST_CUST_NUM_7").toString())));
				pre.setBigDecimal(61,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_MNG_CUST_CUST_NUM_8").toString())));
				pre.setBigDecimal(62,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_MNG_CUST_CUST_NUM_9").toString())));
				pre.setBigDecimal(63,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_MNG_CUST_CUST_NUM_10").toString())));
				pre.setBigDecimal(64,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_MNG_CUST_CUST_NUM_11").toString())));
				pre.setBigDecimal(65,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_MNG_CUST_CUST_NUM_12").toString())));
				pre.setBigDecimal(66,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_BASE_1").toString())));
				pre.setBigDecimal(67,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_BASE_2").toString())));
				pre.setBigDecimal(68,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_BASE_3").toString())));
				pre.setBigDecimal(69,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_BASE_4").toString())));
				pre.setBigDecimal(70,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_BASE_5").toString())));
				pre.setBigDecimal(71,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_BASE_6").toString())));
				pre.setBigDecimal(72,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_BASE_7").toString())));
				pre.setBigDecimal(73,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_BASE_8").toString())));
				pre.setBigDecimal(74,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_BASE_9").toString())));
				pre.setBigDecimal(75,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_BASE_10").toString())));
				pre.setBigDecimal(76,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_BASE_11").toString())));
				pre.setBigDecimal(77,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_BASE_12").toString())));
				pre.setString(78,baseInfoList.get(i).get("OPERATEDATE").toString());
				pre.setString(79,baseInfoList.get(i).get("OPERATOR").toString());
				pre.setString(80,baseInfoList.get(i).get("REMARK").toString());
				pre.setString(81,baseInfoList.get(i).get("MD5").toString());
				pre.addBatch();//批量插入
			}
			pre.executeBatch();
			conn.commit();
			pre.clearBatch();
		} catch (SQLException e) {
			e.printStackTrace();
			// 发生异常，回滚
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			PgsqlUtil.close(conn, pre, rs);
		}
		long TimeStop = System.currentTimeMillis();
		System.out.println("总耗时："+ (TimeStop - TimeStart));
	}
}
