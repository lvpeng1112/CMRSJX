package com.beyondsoft.modules.job.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@ApiModel(value = "营销人员考核")
@TableName("C_MNG_CHECK_BASE_INFO")
@Data
public class CMngCheckBaseInfoJobEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "年度")
    private String yr;

    @ApiModelProperty(value = "机构号")
    private String orgId;

    @ApiModelProperty(value = "客户经理号")
    private String custMngrId;

    @ApiModelProperty(value = "客户经理名称")
    private String custMngrNm;

    @ApiModelProperty(value = "团队负责人编号")
    private String teamId;

    @ApiModelProperty(value = "团队负责人名称")
    private String teamNm;

    @ApiModelProperty(value = "团队名称")
    private String team;

    @ApiModelProperty(value = "入职时间")
    private String inTm;

    @ApiModelProperty(value = "年度指标存款日均余额")
    private String yrNormDepositDtAvgBal;

    @ApiModelProperty(value = "年度指标贷款日均余额")
    private String yrNormLoanDtAvgBal;

    @ApiModelProperty(value = "年度指标FTP净收入")
    private String yrNormNetIncm;

    @ApiModelProperty(value = "年度指标存款新开户数")
    private String yrNormDepositNewOpenNum;

    @ApiModelProperty(value = "年度指标贷款管户户数")
    private String yrNormLoanMngCustCustNum;

    @ApiModelProperty(value = "FTP净收入基数1月")
    private String netIncmBase_1_Month;

    @ApiModelProperty(value = "FTP净收入基数2月")
    private String netIncmBase_2_Month;

    @ApiModelProperty(value = "FTP净收入基数3月")
    private String netIncmBaseMonth;

    @ApiModelProperty(value = "FTP净收入基数4月")
    private String netIncmBase_4_Month;

    @ApiModelProperty(value = "FTP净收入基数5月")
    private String netIncmBase_5_Month;

    @ApiModelProperty(value = "FTP净收入基数6月")
    private String netIncmBase_6_Month;

    @ApiModelProperty(value = "FTP净收入基数7月")
    private String netIncmBase_7_Month;

    @ApiModelProperty(value = "FTP净收入基数8月")
    private String netIncmBase_8_Month;

    @ApiModelProperty(value = "FTP净收入基数9月")
    private String netIncmBase_9_Month;

    @ApiModelProperty(value = "FTP净收入基数10月")
    private String netIncmBase_10_Month;

    @ApiModelProperty(value = "FTP净收入基数11月")
    private String netIncmBase_11_Month;

    @ApiModelProperty(value = "FTP净收入基数12月")
    private String netIncmBase_12_Month;

    @ApiModelProperty(value = "操作日期")
    private String operateDate;

    @ApiModelProperty(value = "操作员")
    private String operator;

    @ApiModelProperty(value = "备注")
    private String remark;

    private String md5;
}
