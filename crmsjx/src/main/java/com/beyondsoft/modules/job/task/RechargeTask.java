/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.job.task;

import com.beyondsoft.common.utils.DateUtils;
import com.beyondsoft.modules.aofp.dao.CMngCheckBaseInfoDao;
import com.beyondsoft.modules.aofp.entity.CMngCheckBaseInfo;
import com.beyondsoft.modules.aofp.entity.CMngCheckBaseInfoHis;
import com.beyondsoft.modules.job.dao.ScheduleJobDao;
import com.beyondsoft.modules.job.entity.CmngCheckBaseInfoJobBatchHisEntity;
import com.beyondsoft.utils.PgsqlUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


/**
 *
 * rechargeTask为spring bean的名称
 * 大数据平台数据回灌DB2 NPMM.C_MNG_CHECK_BASE_INFO，回流的数据是上月数据
 * @author luochao
 */
@Component("rechargeTask")
public class RechargeTask implements ITask{

	@Autowired(required = false)
	ScheduleJobDao scheduleJobDao;

	@Autowired(required = false)
	CMngCheckBaseInfoDao personnelMapper;


	@Transactional(rollbackFor= Exception.class)
	public void run(String param){
        String batchDate = "";
		//如果前台传过来的参数不为空，则使用，否则使用当前系统操作时间的前一个月作为跑批日期
		if(param != null && !"".equals(param)){
			batchDate = param;
		}else{
			//获取当前系统时间上一个月的年月
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.MONTH, -1);
			int year = calendar.get(Calendar.YEAR);
			int month =calendar.get(Calendar.MONTH)+1;
			batchDate = year+"-"+(month<10?"0"+month:month);
		}
		//根据跑批日期进行删除
		Map map = new HashMap();
		map.put("batchDate",batchDate);
		scheduleJobDao.deleteBatchHisDate(map);
		//获取大数据平台数据库连接
		Connection conn = PgsqlUtil.getConn();
		PreparedStatement pre = null; //预编译
		ResultSet rs = null;
		long TimeStart = System.currentTimeMillis();//获取当前时间
		//查询大数据平台npmm.C_MNG_CHECK_BASE_INFO表数据
		String sql = "SELECT\n" +
				"    YR,\n" +
				"    ORG_ID,\n" +
				"    CUST_MNGR_ID,\n" +
				"    CUST_MNGR_NM,\n" +
				"    IN_TM,\n" +
				"    DEPOSIT_DT_AVG_BAL_1,\n" +
				"    DEPOSIT_DT_AVG_BAL_2,\n" +
				"    DEPOSIT_DT_AVG_BAL_3,\n" +
				"    DEPOSIT_DT_AVG_BAL_4,\n" +
				"    DEPOSIT_DT_AVG_BAL_5,\n" +
				"    DEPOSIT_DT_AVG_BAL_6,\n" +
				"    DEPOSIT_DT_AVG_BAL_7,\n" +
				"    DEPOSIT_DT_AVG_BAL_8,\n" +
				"    DEPOSIT_DT_AVG_BAL_9,\n" +
				"    DEPOSIT_DT_AVG_BAL_10,\n" +
				"    DEPOSIT_DT_AVG_BAL_11,\n" +
				"    DEPOSIT_DT_AVG_BAL_12,\n" +
				"    LOAN_DT_AVG_BAL_1,\n" +
				"    LOAN_DT_AVG_BAL_2,\n" +
				"    LOAN_DT_AVG_BAL_3,\n" +
				"    LOAN_DT_AVG_BAL_4,\n" +
				"    LOAN_DT_AVG_BAL_5,\n" +
				"    LOAN_DT_AVG_BAL_6,\n" +
				"    LOAN_DT_AVG_BAL_7,\n" +
				"    LOAN_DT_AVG_BAL_8,\n" +
				"    LOAN_DT_AVG_BAL_9,\n" +
				"    LOAN_DT_AVG_BAL_10,\n" +
				"    LOAN_DT_AVG_BAL_11,\n" +
				"    LOAN_DT_AVG_BAL_12,\n" +
				"    NET_INCM_1,\n" +
				"    NET_INCM_2,\n" +
				"    NET_INCM_3,\n" +
				"    NET_INCM_4,\n" +
				"    NET_INCM_5,\n" +
				"    NET_INCM_6,\n" +
				"    NET_INCM_7,\n" +
				"    NET_INCM_8,\n" +
				"    NET_INCM_9,\n" +
				"    NET_INCM_10,\n" +
				"    NET_INCM_11,\n" +
				"    NET_INCM_12,\n" +
				"    DEPOSIT_NEW_OPEN_NUM_1,\n" +
				"    DEPOSIT_NEW_OPEN_NUM_2,\n" +
				"    DEPOSIT_NEW_OPEN_NUM_3,\n" +
				"    DEPOSIT_NEW_OPEN_NUM_4,\n" +
				"    DEPOSIT_NEW_OPEN_NUM_5,\n" +
				"    DEPOSIT_NEW_OPEN_NUM_6,\n" +
				"    DEPOSIT_NEW_OPEN_NUM_7,\n" +
				"    DEPOSIT_NEW_OPEN_NUM_8,\n" +
				"    DEPOSIT_NEW_OPEN_NUM_9,\n" +
				"    DEPOSIT_NEW_OPEN_NUM_10,\n" +
				"    DEPOSIT_NEW_OPEN_NUM_11,\n" +
				"    DEPOSIT_NEW_OPEN_NUM_12,\n" +
				"    LOAN_MNG_CUST_CUST_NUM_1,\n" +
				"    LOAN_MNG_CUST_CUST_NUM_2,\n" +
				"    LOAN_MNG_CUST_CUST_NUM_3,\n" +
				"    LOAN_MNG_CUST_CUST_NUM_4,\n" +
				"    LOAN_MNG_CUST_CUST_NUM_5,\n" +
				"    LOAN_MNG_CUST_CUST_NUM_6,\n" +
				"    LOAN_MNG_CUST_CUST_NUM_7,\n" +
				"    LOAN_MNG_CUST_CUST_NUM_8,\n" +
				"    LOAN_MNG_CUST_CUST_NUM_9,\n" +
				"    LOAN_MNG_CUST_CUST_NUM_10,\n" +
				"    LOAN_MNG_CUST_CUST_NUM_11,\n" +
				"    LOAN_MNG_CUST_CUST_NUM_12,\n" +
				"    NET_INCM_BASE_1,\n" +
				"    NET_INCM_BASE_2,\n" +
				"    NET_INCM_BASE_3,\n" +
				"    NET_INCM_BASE_4,\n" +
				"    NET_INCM_BASE_5,\n" +
				"    NET_INCM_BASE_6,\n" +
				"    NET_INCM_BASE_7,\n" +
				"    NET_INCM_BASE_8,\n" +
				"    NET_INCM_BASE_9,\n" +
				"    NET_INCM_BASE_10,\n" +
				"    NET_INCM_BASE_11,\n" +
				"    NET_INCM_BASE_12,\n" +
				"    OPERATEDATE,\n" +
				"    OPERATOR,\n" +
				"    REMARK\n" +
				"FROM\n" +
				"    NPMM.C_MNG_CHECK_BASE_INFO";
		try {
			// 取消自动提交（由于要进行多次操作）
			conn.setAutoCommit(false);
			pre = conn.prepareStatement(sql);
			rs = pre.executeQuery();
			while(rs.next()){
				CMngCheckBaseInfo cMngCheckBaseInfo= new CMngCheckBaseInfo();
				cMngCheckBaseInfo.setYr(rs.getString(1));
				cMngCheckBaseInfo.setOrgId(rs.getString(2));
				cMngCheckBaseInfo.setCustMngrId(rs.getString(3));
				cMngCheckBaseInfo.setCustMngrNm(rs.getString(4));
				cMngCheckBaseInfo.setInTm(rs.getString(5));
				cMngCheckBaseInfo.setDepositDtAvgBal1(rs.getString(6));
				cMngCheckBaseInfo.setDepositDtAvgBal2(rs.getString(7));
				cMngCheckBaseInfo.setDepositDtAvgBal3(rs.getString(8));
				cMngCheckBaseInfo.setDepositDtAvgBal4(rs.getString(9));
				cMngCheckBaseInfo.setDepositDtAvgBal5(rs.getString(10));
				cMngCheckBaseInfo.setDepositDtAvgBal6(rs.getString(11));
				cMngCheckBaseInfo.setDepositDtAvgBal7(rs.getString(12));
				cMngCheckBaseInfo.setDepositDtAvgBal8(rs.getString(13));
				cMngCheckBaseInfo.setDepositDtAvgBal9(rs.getString(14));
				cMngCheckBaseInfo.setDepositDtAvgBal10(rs.getString(15));
				cMngCheckBaseInfo.setDepositDtAvgBal11(rs.getString(16));
				cMngCheckBaseInfo.setDepositDtAvgBal12(rs.getString(17));
				cMngCheckBaseInfo.setLoanDtAvgBal1(rs.getString(18));
				cMngCheckBaseInfo.setLoanDtAvgBal2(rs.getString(19));
				cMngCheckBaseInfo.setLoanDtAvgBal3(rs.getString(20));
				cMngCheckBaseInfo.setLoanDtAvgBal4(rs.getString(21));
				cMngCheckBaseInfo.setLoanDtAvgBal5(rs.getString(22));
				cMngCheckBaseInfo.setLoanDtAvgBal6(rs.getString(23));
				cMngCheckBaseInfo.setLoanDtAvgBal7(rs.getString(24));
				cMngCheckBaseInfo.setLoanDtAvgBal8(rs.getString(25));
				cMngCheckBaseInfo.setLoanDtAvgBal9(rs.getString(26));
				cMngCheckBaseInfo.setLoanDtAvgBal10(rs.getString(27));
				cMngCheckBaseInfo.setLoanDtAvgBal11(rs.getString(28));
				cMngCheckBaseInfo.setLoanDtAvgBal12(rs.getString(29));
				cMngCheckBaseInfo.setNetIncm1(rs.getString(30));
				cMngCheckBaseInfo.setNetIncm2(rs.getString(31));
				cMngCheckBaseInfo.setNetIncm3(rs.getString(32));
				cMngCheckBaseInfo.setNetIncm4(rs.getString(33));
				cMngCheckBaseInfo.setNetIncm5(rs.getString(34));
				cMngCheckBaseInfo.setNetIncm6(rs.getString(35));
				cMngCheckBaseInfo.setNetIncm7(rs.getString(36));
				cMngCheckBaseInfo.setNetIncm8(rs.getString(37));
				cMngCheckBaseInfo.setNetIncm9(rs.getString(38));
				cMngCheckBaseInfo.setNetIncm10(rs.getString(39));
				cMngCheckBaseInfo.setNetIncm11(rs.getString(40));
				cMngCheckBaseInfo.setNetIncm12(rs.getString(41));
				cMngCheckBaseInfo.setDepositNewOpenNum1(rs.getString(42));
				cMngCheckBaseInfo.setDepositNewOpenNum2(rs.getString(43));
				cMngCheckBaseInfo.setDepositNewOpenNum3(rs.getString(44));
				cMngCheckBaseInfo.setDepositNewOpenNum4(rs.getString(45));
				cMngCheckBaseInfo.setDepositNewOpenNum5(rs.getString(46));
				cMngCheckBaseInfo.setDepositNewOpenNum6(rs.getString(47));
				cMngCheckBaseInfo.setDepositNewOpenNum7(rs.getString(48));
				cMngCheckBaseInfo.setDepositNewOpenNum8(rs.getString(49));
				cMngCheckBaseInfo.setDepositNewOpenNum9(rs.getString(50));
				cMngCheckBaseInfo.setDepositNewOpenNum10(rs.getString(51));
				cMngCheckBaseInfo.setDepositNewOpenNum11(rs.getString(52));
				cMngCheckBaseInfo.setDepositNewOpenNum12(rs.getString(53));
				cMngCheckBaseInfo.setLoanMngCustCustNum1(rs.getString(54));
				cMngCheckBaseInfo.setLoanMngCustCustNum2(rs.getString(55));
				cMngCheckBaseInfo.setLoanMngCustCustNum3(rs.getString(56));
				cMngCheckBaseInfo.setLoanMngCustCustNum4(rs.getString(57));
				cMngCheckBaseInfo.setLoanMngCustCustNum5(rs.getString(58));
				cMngCheckBaseInfo.setLoanMngCustCustNum6(rs.getString(59));
				cMngCheckBaseInfo.setLoanMngCustCustNum7(rs.getString(60));
				cMngCheckBaseInfo.setLoanMngCustCustNum8(rs.getString(61));
				cMngCheckBaseInfo.setLoanMngCustCustNum9(rs.getString(62));
				cMngCheckBaseInfo.setLoanMngCustCustNum10(rs.getString(63));
				cMngCheckBaseInfo.setLoanMngCustCustNum11(rs.getString(64));
				cMngCheckBaseInfo.setLoanMngCustCustNum12(rs.getString(65));
				cMngCheckBaseInfo.setNetIncmBase1(rs.getString(66));
				cMngCheckBaseInfo.setNetIncmBase2(rs.getString(67));
				cMngCheckBaseInfo.setNetIncmBase3(rs.getString(68));
				cMngCheckBaseInfo.setNetIncmBase4(rs.getString(69));
				cMngCheckBaseInfo.setNetIncmBase5(rs.getString(70));
				cMngCheckBaseInfo.setNetIncmBase6(rs.getString(71));
				cMngCheckBaseInfo.setNetIncmBase7(rs.getString(72));
				cMngCheckBaseInfo.setNetIncmBase8(rs.getString(73));
				cMngCheckBaseInfo.setNetIncmBase9(rs.getString(74));
				cMngCheckBaseInfo.setNetIncmBase10(rs.getString(75));
				cMngCheckBaseInfo.setNetIncmBase11(rs.getString(76));
				cMngCheckBaseInfo.setNetIncmBase12(rs.getString(77));
				cMngCheckBaseInfo.setOperateDate(rs.getString(78));
				cMngCheckBaseInfo.setOperator(rs.getString(79));
				cMngCheckBaseInfo.setRemark(rs.getString(80));
				//根据年度，机构，客户经理编号进行删除
				personnelMapper.deleteByYr(cMngCheckBaseInfo);
				scheduleJobDao.insertByBatch(cMngCheckBaseInfo);//执行插入营销人员基础信息表操作
				//执行插入营销人员基础信息表跑批历史表
				CmngCheckBaseInfoJobBatchHisEntity cmngCheckBaseInfoJobBatchHisEntity= new CmngCheckBaseInfoJobBatchHisEntity();
				cmngCheckBaseInfoJobBatchHisEntity.setYr(rs.getString(1));
				cmngCheckBaseInfoJobBatchHisEntity.setOrgId(rs.getString(2));
				cmngCheckBaseInfoJobBatchHisEntity.setCustMngrId(rs.getString(3));
				cmngCheckBaseInfoJobBatchHisEntity.setCustMngrNm(rs.getString(4));
				cmngCheckBaseInfoJobBatchHisEntity.setInTm(rs.getString(5));
				cmngCheckBaseInfoJobBatchHisEntity.setDepositDtAvgBal1(rs.getString(6));
				cmngCheckBaseInfoJobBatchHisEntity.setDepositDtAvgBal2(rs.getString(7));
				cmngCheckBaseInfoJobBatchHisEntity.setDepositDtAvgBal3(rs.getString(8));
				cmngCheckBaseInfoJobBatchHisEntity.setDepositDtAvgBal4(rs.getString(9));
				cmngCheckBaseInfoJobBatchHisEntity.setDepositDtAvgBal5(rs.getString(10));
				cmngCheckBaseInfoJobBatchHisEntity.setDepositDtAvgBal6(rs.getString(11));
				cmngCheckBaseInfoJobBatchHisEntity.setDepositDtAvgBal7(rs.getString(12));
				cmngCheckBaseInfoJobBatchHisEntity.setDepositDtAvgBal8(rs.getString(13));
				cmngCheckBaseInfoJobBatchHisEntity.setDepositDtAvgBal9(rs.getString(14));
				cmngCheckBaseInfoJobBatchHisEntity.setDepositDtAvgBal10(rs.getString(15));
				cmngCheckBaseInfoJobBatchHisEntity.setDepositDtAvgBal11(rs.getString(16));
				cmngCheckBaseInfoJobBatchHisEntity.setDepositDtAvgBal12(rs.getString(17));
				cmngCheckBaseInfoJobBatchHisEntity.setLoanDtAvgBal1(rs.getString(18));
				cmngCheckBaseInfoJobBatchHisEntity.setLoanDtAvgBal2(rs.getString(19));
				cmngCheckBaseInfoJobBatchHisEntity.setLoanDtAvgBal3(rs.getString(20));
				cmngCheckBaseInfoJobBatchHisEntity.setLoanDtAvgBal4(rs.getString(21));
				cmngCheckBaseInfoJobBatchHisEntity.setLoanDtAvgBal5(rs.getString(22));
				cmngCheckBaseInfoJobBatchHisEntity.setLoanDtAvgBal6(rs.getString(23));
				cmngCheckBaseInfoJobBatchHisEntity.setLoanDtAvgBal7(rs.getString(24));
				cmngCheckBaseInfoJobBatchHisEntity.setLoanDtAvgBal8(rs.getString(25));
				cmngCheckBaseInfoJobBatchHisEntity.setLoanDtAvgBal9(rs.getString(26));
				cmngCheckBaseInfoJobBatchHisEntity.setLoanDtAvgBal10(rs.getString(27));
				cmngCheckBaseInfoJobBatchHisEntity.setLoanDtAvgBal11(rs.getString(28));
				cmngCheckBaseInfoJobBatchHisEntity.setLoanDtAvgBal12(rs.getString(29));
				cmngCheckBaseInfoJobBatchHisEntity.setNetIncm1(rs.getString(30));
				cmngCheckBaseInfoJobBatchHisEntity.setNetIncm2(rs.getString(31));
				cmngCheckBaseInfoJobBatchHisEntity.setNetIncm3(rs.getString(32));
				cmngCheckBaseInfoJobBatchHisEntity.setNetIncm4(rs.getString(33));
				cmngCheckBaseInfoJobBatchHisEntity.setNetIncm5(rs.getString(34));
				cmngCheckBaseInfoJobBatchHisEntity.setNetIncm6(rs.getString(35));
				cmngCheckBaseInfoJobBatchHisEntity.setNetIncm7(rs.getString(36));
				cmngCheckBaseInfoJobBatchHisEntity.setNetIncm8(rs.getString(37));
				cmngCheckBaseInfoJobBatchHisEntity.setNetIncm9(rs.getString(38));
				cmngCheckBaseInfoJobBatchHisEntity.setNetIncm10(rs.getString(39));
				cmngCheckBaseInfoJobBatchHisEntity.setNetIncm11(rs.getString(40));
				cmngCheckBaseInfoJobBatchHisEntity.setNetIncm12(rs.getString(41));
				cmngCheckBaseInfoJobBatchHisEntity.setDepositNewOpenNum1(rs.getString(42));
				cmngCheckBaseInfoJobBatchHisEntity.setDepositNewOpenNum2(rs.getString(43));
				cmngCheckBaseInfoJobBatchHisEntity.setDepositNewOpenNum3(rs.getString(44));
				cmngCheckBaseInfoJobBatchHisEntity.setDepositNewOpenNum4(rs.getString(45));
				cmngCheckBaseInfoJobBatchHisEntity.setDepositNewOpenNum5(rs.getString(46));
				cmngCheckBaseInfoJobBatchHisEntity.setDepositNewOpenNum6(rs.getString(47));
				cmngCheckBaseInfoJobBatchHisEntity.setDepositNewOpenNum7(rs.getString(48));
				cmngCheckBaseInfoJobBatchHisEntity.setDepositNewOpenNum8(rs.getString(49));
				cmngCheckBaseInfoJobBatchHisEntity.setDepositNewOpenNum9(rs.getString(50));
				cmngCheckBaseInfoJobBatchHisEntity.setDepositNewOpenNum10(rs.getString(51));
				cmngCheckBaseInfoJobBatchHisEntity.setDepositNewOpenNum11(rs.getString(52));
				cmngCheckBaseInfoJobBatchHisEntity.setDepositNewOpenNum12(rs.getString(53));
				cmngCheckBaseInfoJobBatchHisEntity.setLoanMngCustCustNum1(rs.getString(54));
				cmngCheckBaseInfoJobBatchHisEntity.setLoanMngCustCustNum2(rs.getString(55));
				cmngCheckBaseInfoJobBatchHisEntity.setLoanMngCustCustNum3(rs.getString(56));
				cmngCheckBaseInfoJobBatchHisEntity.setLoanMngCustCustNum4(rs.getString(57));
				cmngCheckBaseInfoJobBatchHisEntity.setLoanMngCustCustNum5(rs.getString(58));
				cmngCheckBaseInfoJobBatchHisEntity.setLoanMngCustCustNum6(rs.getString(59));
				cmngCheckBaseInfoJobBatchHisEntity.setLoanMngCustCustNum7(rs.getString(60));
				cmngCheckBaseInfoJobBatchHisEntity.setLoanMngCustCustNum8(rs.getString(61));
				cmngCheckBaseInfoJobBatchHisEntity.setLoanMngCustCustNum9(rs.getString(62));
				cmngCheckBaseInfoJobBatchHisEntity.setLoanMngCustCustNum10(rs.getString(63));
				cmngCheckBaseInfoJobBatchHisEntity.setLoanMngCustCustNum11(rs.getString(64));
				cmngCheckBaseInfoJobBatchHisEntity.setLoanMngCustCustNum12(rs.getString(65));
				cmngCheckBaseInfoJobBatchHisEntity.setNetIncmBase1(rs.getString(66));
				cmngCheckBaseInfoJobBatchHisEntity.setNetIncmBase2(rs.getString(67));
				cmngCheckBaseInfoJobBatchHisEntity.setNetIncmBase3(rs.getString(68));
				cmngCheckBaseInfoJobBatchHisEntity.setNetIncmBase4(rs.getString(69));
				cmngCheckBaseInfoJobBatchHisEntity.setNetIncmBase5(rs.getString(70));
				cmngCheckBaseInfoJobBatchHisEntity.setNetIncmBase6(rs.getString(71));
				cmngCheckBaseInfoJobBatchHisEntity.setNetIncmBase7(rs.getString(72));
				cmngCheckBaseInfoJobBatchHisEntity.setNetIncmBase8(rs.getString(73));
				cmngCheckBaseInfoJobBatchHisEntity.setNetIncmBase9(rs.getString(74));
				cmngCheckBaseInfoJobBatchHisEntity.setNetIncmBase10(rs.getString(75));
				cmngCheckBaseInfoJobBatchHisEntity.setNetIncmBase11(rs.getString(76));
				cmngCheckBaseInfoJobBatchHisEntity.setNetIncmBase12(rs.getString(77));
				cmngCheckBaseInfoJobBatchHisEntity.setBatchDate(batchDate);
				scheduleJobDao.insertBatchHis(cmngCheckBaseInfoJobBatchHisEntity);
			}
			conn.commit();//提交事务
		} catch (SQLException e) {
			e.printStackTrace();
			// 发生异常，回滚
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			PgsqlUtil.close(conn, pre, rs);
		}
		long TimeStop = System.currentTimeMillis();
		System.out.println("总耗时："+ (TimeStop - TimeStart));
	}
}
