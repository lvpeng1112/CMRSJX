package com.beyondsoft.modules.protection.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@ApiModel(value = "保护期绩效维护表")
@TableName("C_PROTECTION_AMOUNT")
@Data
public class ProtectionAmount implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "机构编号")
    private String branchId;

    @ApiModelProperty(value = "机构名称")
    private String branchNm;

    @ApiModelProperty(value = "客户经理编号")
    private String custId;

    @ApiModelProperty(value = "客户经理名称")
    private String custNm;

    @ApiModelProperty(value = "保护期绩效金额")
    @JsonSerialize(using = ToStringSerializer.class)
    private Double protectionAmount;

    @ApiModelProperty(value = "生效日期")
    private String sDate;

    @ApiModelProperty(value = "失效日期")
    private String eDate;

    @ApiModelProperty(value = "操作员")
    private String operators;

    @ApiModelProperty(value = "操作时间")
    private String operateDate;

    @TableField(exist=false)
    private String oldSdate;

}
