package com.beyondsoft.modules.protection.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.common.utils.DateUtils;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.common.utils.ShiroUtils;
import com.beyondsoft.datasource.annotation.DataSource;
import com.beyondsoft.modules.protection.dao.ProtectionAmountDao;
import com.beyondsoft.modules.protection.entity.ProtectionAmount;
import com.beyondsoft.modules.protection.entity.ProtectionAmountHis;
import com.beyondsoft.modules.protection.service.ProtectionAmountService;
import com.beyondsoft.utils.DateUtil;
import com.beyondsoft.utils.StrUtil;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class ProtectionAmountServiceImpl extends ServiceImpl<ProtectionAmountDao, ProtectionAmount> implements ProtectionAmountService {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    ProtectionAmountDao protectionAmountDao;

    @Override
    @DataSource(value = "second")
    public List<ProtectionAmount> queryProtectionAmountListInfo(Map<String, Object> params){
        int pageNo = Integer.parseInt((String) params.get("page"));
        int pageCount = Integer.parseInt((String)params.get("limit"));
        int offset = ((pageNo - 1) * pageCount);
        params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
        params.put("offset", offset);
        return protectionAmountDao.queryProtectionAmountListInfo(params);
    }

    @Override
    @DataSource(value = "second")
    public int queryProtectionAmountNb(Map<String, Object> params){
        int nb = protectionAmountDao.queryProtectionAmountNb(params);
        return nb;
    }

    @Override
    @DataSource(value = "second")
    public R batchImport(String fileName, MultipartFile file, String org, String isKhy) {
        Map map = new HashMap();
        Workbook workbook = null;
        if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
            return R.error("上传格式不正确");
        }
        String extString = fileName.substring(fileName.lastIndexOf("."));

        List<ProtectionAmount> gradeList = new ArrayList<>();
        List<ProtectionAmountHis> hisList = new ArrayList<>();
        try {
            InputStream inputStream = file.getInputStream();
            if (".xls".equals(extString)) {
                HSSFWorkbook wb = new HSSFWorkbook(inputStream);
                workbook = wb;
            } else if (".xlsx".equals(extString)) {
                XSSFWorkbook wb = new XSSFWorkbook(inputStream);
                workbook = wb;
            }
            int sheets = workbook.getNumberOfSheets();
            for (int i = 0; i < sheets; i++) {
                Sheet sheetAt = workbook.getSheetAt(i);
                //获取多少行
                int lastRowNum = sheetAt.getLastRowNum();
                for (int j = 1; j <= lastRowNum; j++) {
                    Row row = sheetAt.getRow(j);
                    if (row != null) {
                        ProtectionAmount checkGrade = new ProtectionAmount();
                        for (Cell cell : row) {
                            cell.setCellType(Cell.CELL_TYPE_STRING);
                        }
                        // 必须输入项check
                        if (row.getCell(0) == null || row.getCell(0).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，机构编号列没有数据");
                        }else if (row.getCell(2) == null || row.getCell(2).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，客户经理编号列没有数据");
                        }else if (row.getCell(3) == null || row.getCell(3).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，客户经理名称列没有数据");
                        }else if (row.getCell(4) == null || row.getCell(4).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，保护期绩效金额列没有数据");
                        }else if (row.getCell(5) == null || row.getCell(5).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，生效日期列没有数据");
                        }else if (row.getCell(6) == null || row.getCell(6).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，到期日期列没有数据");
                        }
                        // 生效日期格式check
                        String sdate = row.getCell(5).getStringCellValue();
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        if (StrUtil.isNumeric(sdate)) {
                            Date date = HSSFDateUtil.getJavaDate(Double.valueOf(sdate));
                            sdate = format.format(date).toString();
                        }

                        if (sdate.length() != 10) {
                            return R.error("第" + (j + 1) + "行,生效日期【" + sdate + "】不是正确的日期格式！例:YYYY-MM-dd");
                        }
                        String inputStrS = sdate.substring(0,4) + sdate.substring(5,7) + sdate.substring(8,10);
                        if (!DateUtil.isDate(inputStrS)) {
                            return R.error("第" + (j + 1) + "行,生效日期【" + sdate + "】不是正确的日期格式！例:YYYY-MM-dd");
                        }

                        //失效日期格式check
                        String edate = row.getCell(6).getStringCellValue();
                        if (StrUtil.isNumeric(edate)) {
                            Date date = HSSFDateUtil.getJavaDate(Double.valueOf(edate));
                            edate = format.format(date).toString();
                        }

                        if (edate.length() != 10) {
                            return R.error("第" + (j + 1) + "行,到期日期【" + edate + "】不是正确的日期格式！例:YYYY-MM-dd");
                        }
                        String inputStrE = edate.substring(0,4) + edate.substring(5,7) + edate.substring(8,10);
                        if (!DateUtil.isDate(inputStrE)) {
                            return R.error("第" + (j + 1) + "行,到期日期【" + edate + "】不是正确的日期格式！例:YYYY-MM-dd");
                        }
                        if(inputStrS.compareTo(inputStrE)>0){
                            return R.error("第" + (j + 1) + "行,生效日期【" + sdate + "】不能大于到期日期【" + edate + "】");
                        }
                        //失效日期是否是月末时间
                        //查询输入生效日期当月的月初时间
                        Calendar cale = Calendar.getInstance();
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                        cale.setTime(formatter.parse(sdate));
                        cale.add(Calendar.MONTH, 0);
                        cale.set(Calendar.DAY_OF_MONTH, 1);
                        String firstDayOfMonth = formatter.format(cale.getTime()); // 当月第一天

                        //查询输入到期日期当月月末时间
                        Calendar expire = Calendar.getInstance();
                        expire.setTime(formatter.parse(edate));
                        expire.add(Calendar.MONTH, 1);
                        expire.set(Calendar.DAY_OF_MONTH, 0);
                        String expireLastDayOfMonth = formatter.format(expire.getTime()); // 当月最后一天

                        //新增之前查询数据是否已经存在
                        String branchId = row.getCell(0).getStringCellValue();//机构编号
                        String custId = row.getCell(2).getStringCellValue();//客户经理编号
                        String custNm = row.getCell(3).getStringCellValue();// 客户经理名称
                        Map params = new HashMap();
                        params.put("custId",custId);
                        params.put("firstDayOfMonth",firstDayOfMonth);
                        params.put("expireLastDayOfMonth",expireLastDayOfMonth);
                        params.put("branchId",branchId);
                        int amount = protectionAmountDao.queryProtectionAmount(params);
                        //截取日期
                        String D = sdate.substring(0,5) + sdate.substring(5,7);
                        if(amount > 0){//说明数据已经存在，则不允许添加
                            return R.error("客户经理："+ custNm + "，生效日期为："+ D +"月份的保护期绩效数据已存在！");
                        }else if (!expireLastDayOfMonth.equals(edate)) {
                            return R.error("第" + (j + 1) + "行,到期日期【" + edate + "】不是月末时间");
                        }

                        // 机构编号存在check
                        String orgId = row.getCell(0).getStringCellValue();
                        Map<String, Object> orgParams = new HashMap<>();
                        orgParams.put("orgId", orgId);
                        orgParams.put("org", org);
                        orgParams.put("isKhy", isKhy);
                        int count = protectionAmountDao.checkOrgId(orgParams);
                        if (count == 0) {
                            return R.error("第" + (j + 1) + "行,您没有权限导入其他机构的数据，机构编号【" + orgId + "】！");
                        }
                        String orgNm = queryOrgNm(orgParams);

                        if (custId.length() > 90) {
                            return R.error("第" + (j + 1) + "行,客户经理编号【" + custId + "】长度不能超过90位！");
                        }

                        if (StrUtil.chineseLen(custNm) > 50) {
                            return R.error("第" + (j + 1) + "行,客户名称【" + custNm + "】长度不能超过50位！");
                        }
                        // 保护期绩效金额check
                        String grade = row.getCell(4).getStringCellValue();
                        if (!StrUtil.isNumeric(grade)) {
                            return R.error("第" + (j + 1) + "行,保护期绩效金额【" + grade + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(grade, 18 , 2)) {
                            return R.error("第" + (j + 1) + "行,保护期绩效金额【" + grade + "】不能超过18位整数2位小数！");
                        }

                        //执行插入方法
                        checkGrade.setBranchId(orgId);
                        checkGrade.setBranchNm(orgNm);
                        checkGrade.setCustId(custId);
                        checkGrade.setCustNm(custNm);
                        Double protectionAmountDouble = Double.valueOf(grade);
                        checkGrade.setProtectionAmount(protectionAmountDouble);
                        checkGrade.setSDate(sdate);
                        checkGrade.setEDate(edate);
                        checkGrade.setOperators(ShiroUtils.getUserEntity().getName());
                        gradeList.add(checkGrade);
//                        protectionAmountDao.deleteProtectionAmountList(checkGrade);

                        ProtectionAmountHis protectionAmountHis = new ProtectionAmountHis();
                        protectionAmountHis.setBranchId(orgId);
                        protectionAmountHis.setBranchNm(orgNm);
                        protectionAmountHis.setCustId(custId);
                        protectionAmountHis.setCustNm(custNm);
                        protectionAmountHis.setProtectionAmountAfter(protectionAmountDouble);
                        protectionAmountHis.setSDateAfter(sdate);
                        protectionAmountHis.setEDateAfter(edate);
                        protectionAmountHis.setOperator(ShiroUtils.getUserEntity().getName());
                        protectionAmountHis.setOperateDate(DateUtils.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
                        protectionAmountHis.setLogo("批量导入");//操作标识
                        hisList.add(protectionAmountHis);

                    }
                }
            }
            protectionAmountDao.insertProtectionAmountList(gradeList);
            protectionAmountDao.insertProtectionAmountHisList(hisList);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            try {
                workbook.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return R.ok("导入成功");
    }

    @Override
    @DataSource(value = "second")
    public ProtectionAmount queryProtectionByParams(Map<String, Object> params) {
        return protectionAmountDao.queryProtectionByParams(params);
    }

    /**
     * 删除
     * @param protectionAmount
     */
    @Override
    @DataSource(value = "second")
    public void deleteProtectionAmountList(ProtectionAmount protectionAmount) {

        HashMap<String, Object> params = new HashMap<>();
        String branchId = protectionAmount.getBranchId();
        params.put("orgId",branchId);
        String custId = protectionAmount.getCustId();
        params.put("custId",custId);
        String sDate = protectionAmount.getSDate();
        params.put("sDate",sDate);
        ProtectionAmount protectionAmount1 = protectionAmountDao.queryProtectionByParams(params);

        List<ProtectionAmount> protectionAmountBefo = new ArrayList<>();
        List<ProtectionAmount> protectionAmountAfter = new ArrayList<>();
        protectionAmountBefo.add(protectionAmount1);
        record(protectionAmountBefo,protectionAmountAfter,"删除");
        protectionAmountDao.deleteProtectionAmountList(protectionAmount);
    }

    /**
     * 新增
     * @param protectionAmount
     */
    @Override
    @DataSource(value = "second")
    public void insertProtectionAmount(ProtectionAmount protectionAmount) {

        protectionAmountDao.insertProtectionAmount(protectionAmount);
        List<ProtectionAmount> protectionAmountBefo = new ArrayList<>();
        List<ProtectionAmount> protectionAmountAfter = new ArrayList<>();
        protectionAmountAfter.add(protectionAmount);
        record(protectionAmountBefo,protectionAmountAfter,"新增");
    }

    /**
     * 修改
     * @param protectionAmount
     */
    @Override
    @DataSource(value = "second")
    public void updateProtectionAmountData(ProtectionAmount protectionAmount) {

        HashMap<String, Object> params = new HashMap<>();
        String branchId = protectionAmount.getBranchId();
        params.put("orgId",branchId);
        String custId = protectionAmount.getCustId();
        params.put("custId",custId);
        String oldSdate = protectionAmount.getOldSdate();
        params.put("sDate",oldSdate);
        ProtectionAmount protectionAmount1 = protectionAmountDao.queryProtectionByParams(params);

        List<ProtectionAmount> protectionAmountBefo = new ArrayList<>();
        List<ProtectionAmount> protectionAmountAfter = new ArrayList<>();
        protectionAmountBefo.add(protectionAmount1);

        Map map = new HashMap();
        map.put("orgId",branchId);
        //机构名称
        String branchNm = queryOrgNm(map);
        protectionAmount.setBranchNm(branchNm);
        protectionAmountAfter.add(protectionAmount);

        record(protectionAmountBefo,protectionAmountAfter,"修改");
        protectionAmountDao.updateProtectionAmountData(protectionAmount);

    }

    @Override
    @DataSource(value = "second")
    public int queryProtectionAmount(Map reqPara) {
        return protectionAmountDao.queryProtectionAmount(reqPara);
    }

    @Override
    @DataSource(value = "second")
    public List<ProtectionAmountHis> queryProtectionAmountHisListInfo(Map<String, Object> params) {
        int pageNo = Integer.parseInt((String) params.get("page"));
        int pageCount = Integer.parseInt((String)params.get("limit"));
        int offset = ((pageNo - 1) * pageCount);
        params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
        params.put("offset", offset);
        List<ProtectionAmountHis> protectionAmountHis = protectionAmountDao.queryProtectionAmountHisListInfo(params);
        return protectionAmountHis;
    }

    @Override
    @DataSource(value = "second")
    public int queryProtectionAmountHisListInfoNb(Map<String, Object> params) {
        int i = protectionAmountDao.queryProtectionAmountHisListInfoNb(params);
        return i;
    }

    @Override
    @DataSource(value = "second")
    public String queryOrgNm(Map<String, Object> params) {
        return protectionAmountDao.queryOrgNm(params);
    }

    @DataSource(value = "second")
    public void record(List<ProtectionAmount> protectionAmountBefo, List<ProtectionAmount> protectionAmountAfter,String logo){
        ProtectionAmountHis protectionAmountHis = new ProtectionAmountHis();
        if(protectionAmountBefo.size() > 0){
            protectionAmountHis.setBranchId(protectionAmountBefo.get(0).getBranchId());
            protectionAmountHis.setBranchNm(protectionAmountBefo.get(0).getBranchNm());
            protectionAmountHis.setCustId(protectionAmountBefo.get(0).getCustId());
            protectionAmountHis.setCustNm(protectionAmountBefo.get(0).getCustNm());
            Double protectionAmountDouble = Double.valueOf(protectionAmountBefo.get(0).getProtectionAmount());
            protectionAmountHis.setProtectionAmountBefo(protectionAmountDouble);
            protectionAmountHis.setSDateBefo(protectionAmountBefo.get(0).getSDate());
            protectionAmountHis.setEDateBefo(protectionAmountBefo.get(0).getEDate());
        }
        if (protectionAmountAfter.size() > 0){
            protectionAmountHis.setBranchId(protectionAmountAfter.get(0).getBranchId());
            protectionAmountHis.setBranchNm(protectionAmountAfter.get(0).getBranchNm());
            protectionAmountHis.setCustId(protectionAmountAfter.get(0).getCustId());
            protectionAmountHis.setCustNm(protectionAmountAfter.get(0).getCustNm());
            Double protectionAmountDouble = Double.valueOf(protectionAmountAfter.get(0).getProtectionAmount());
            protectionAmountHis.setProtectionAmountAfter(protectionAmountDouble);
            protectionAmountHis.setSDateAfter(protectionAmountAfter.get(0).getSDate());
            protectionAmountHis.setEDateAfter(protectionAmountAfter.get(0).getEDate());
        }
        protectionAmountHis.setOperateDate(DateUtils.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
        protectionAmountHis.setOperator(ShiroUtils.getUserEntity().getName());
        protectionAmountHis.setLogo(logo);//操作标识

        protectionAmountDao.insertProtectionAmountHis(protectionAmountHis);
    }
}
