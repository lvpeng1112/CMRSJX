package com.beyondsoft.modules.protection.controller;

import com.alibaba.druid.util.StringUtils;
import com.beyondsoft.common.annotation.SysLog;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.common.utils.ShiroUtils;
import com.beyondsoft.datasource.annotation.DataSource;
import com.beyondsoft.modules.protection.dao.ProtectionAmountDao;
import com.beyondsoft.modules.protection.entity.ProtectionAmount;
import com.beyondsoft.modules.protection.entity.ProtectionAmountHis;
import com.beyondsoft.modules.protection.service.ProtectionAmountService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/protection")
public class ProtectionAmountController {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    ProtectionAmountService protectionAmountService;
    @Resource
    ProtectionAmountDao protectionAmountDao;

    @RequestMapping("/list")
    @ApiOperation("分页查询")
    public R list(@RequestParam Map<String, Object> params) throws ParseException {
        Map map = new HashMap();
        if(null == params){
            return R.error("入参为空");
        }
        if(StringUtils.isEmpty((String) params.get("page")) && StringUtils.isEmpty((String) params.get("limit"))){
            return R.error("缺少必传分页参数");
        }
        //生效日期
        String yr = (String) params.get("yr") + "-01";
        //生效日期月末时间
        // 获取当月第一天和最后一天
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        // 当月最后一天
        String lastday;
        Calendar cale = Calendar.getInstance();
        cale.setTime(format.parse(yr));
        cale.add(Calendar.MONTH, 1);
        cale.set(Calendar.DAY_OF_MONTH, 0);
        lastday = format.format(cale.getTime()); // 当月最后一天
        params.put("sDate",yr);
        params.put("sDateL",lastday);
        List<ProtectionAmount> protectionAmountList = protectionAmountService.queryProtectionAmountListInfo(params);
        int i = protectionAmountService.queryProtectionAmountNb(params);
        return R.ok().put("protectionAmountList",protectionAmountList).put("count",i);
    }

    @RequestMapping("/importExcel")
    @ApiOperation("导入excel")
    public R importExcel(@RequestParam(value = "file") MultipartFile file,String org, String isKhy) throws Exception {

        String fileName = file.getOriginalFilename();
        logger.info("导入表格文件为:{}",fileName);

        return protectionAmountService.batchImport(fileName, file, org, isKhy);
    }

    /**
     * 新增
     */
    @SysLog("新增保护期绩效数据")
    @PostMapping("/insertProtectionAmount")
    public R insertProtectionAmount(@RequestParam Map<String, Object> params) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Map map = new HashMap();
        //机构号
        String branchId = (String) params.get("branchId");
        map.put("orgId",branchId);
        //机构名称
        String branchNm = protectionAmountService.queryOrgNm(map);
        //客户经理编号
        String custId = (String) params.get("custId");
        //客户经理名称
        String custNm = (String) params.get("custNm");
        //保护期绩效金额
        String protectionAmount = (String) params.get("protectionAmount");
        //生效日期
        String sDate = (String) params.get("sDate");
        //到期日期
        String eDate = (String) params.get("eDate");

        //查询输入生效日期当月的月初和月末时间
        Calendar cale = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        cale.setTime(formatter.parse(sDate));
        cale.add(Calendar.MONTH, 0);
        cale.set(Calendar.DAY_OF_MONTH, 1);
        String firstDayOfMonth = formatter.format(cale.getTime()); // 当月第一天

        //查询输入到期日期当月月末时间
        Calendar expire = Calendar.getInstance();
        expire.setTime(formatter.parse(eDate));
        expire.add(Calendar.MONTH, 1);
        expire.set(Calendar.DAY_OF_MONTH, 0);
        String expireLastDayOfMonth = formatter.format(expire.getTime()); // 当月最后一天

        //新增之前查询数据是否已经存在
        map.put("custId",custId);
        map.put("firstDayOfMonth",firstDayOfMonth);
        map.put("expireLastDayOfMonth",expireLastDayOfMonth);
        map.put("branchId",branchId);
        int amount = protectionAmountService.queryProtectionAmount(map);
        if(amount > 0){//说明数据已经存在，则不允许添加
            return R.error(9999,"数据已存在，请勿重复添加！");
        }else if(!expireLastDayOfMonth.equals(eDate)){
            return R.error(9999,"到期日期不是到期日期的月末时间，请重新选择！");
        }else{
            ProtectionAmount protection = new ProtectionAmount();
            protection.setBranchId(branchId);
            protection.setBranchNm(branchNm);
            protection.setCustId(custId);
            protection.setCustNm(custNm);
            Double protectionAmountDouble = Double.valueOf(protectionAmount);
            protection.setProtectionAmount(protectionAmountDouble);
            protection.setSDate(sDate);
            protection.setEDate(eDate);
            protection.setOperators(ShiroUtils.getUserEntity().getName());
            protectionAmountService.insertProtectionAmount(protection);
            return R.ok();
        }
    }

    /**
     * 修改时回显人员信息
     */
    @RequestMapping("/shouwProtectionAmount")
    public R shouwProtectionAmount(@RequestParam Map<String, Object> params){
        //机构号
        String branchId = (String) params.get("branchId");
        //客户经理编号
        String custId = (String) params.get("custId");
        //生效日期
        String sDate = (String) params.get("sDate");

        ProtectionAmount protectionAmountList = protectionAmountService.queryProtectionByParams(params);

        return R.ok().put("protectionAmountList", protectionAmountList);
    }


    /**
     * 修改
     */
    @SysLog("修改保护期绩效数据")
    @PostMapping("/updateProtectionAmountData")
    public R updateProtectionAmountData(@RequestParam Map<String, Object> params) throws ParseException {
        Map map = new HashMap();
        //机构号
        String branchId = (String) params.get("branchId");
        //客户经理编号
        String custId = (String) params.get("custId");
        //客户经理名称
        String custNm = (String) params.get("custNm");
        //保护期绩效金额
        String protectionAmount = (String) params.get("protectionAmount");
        //生效日期(原本的数据)
        String oldSdate = (String) params.get("oldSdate");
        //生效日期
        String sDate = (String) params.get("sDate");
        //到期日期
        String eDate = (String) params.get("eDate");

        //查询输入生效日期当月的月初时间
        Calendar cale = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        cale.setTime(formatter.parse(sDate));
        cale.add(Calendar.MONTH, 0);
        cale.set(Calendar.DAY_OF_MONTH, 1);
        String firstDayOfMonth = formatter.format(cale.getTime()); // 当月第一天

        //查询输入到期日期当月月末时间
        Calendar expire = Calendar.getInstance();
        expire.setTime(formatter.parse(eDate));
        expire.add(Calendar.MONTH, 1);
        expire.set(Calendar.DAY_OF_MONTH, 0);
        String expireLastDayOfMonth = formatter.format(expire.getTime()); // 当月最后一天

        //新增之前查询数据是否已经存在
        map.put("custId",custId);
        map.put("firstDayOfMonth",firstDayOfMonth);
        map.put("expireLastDayOfMonth",expireLastDayOfMonth);
        map.put("branchId",branchId);
        int amount = protectionAmountService.queryProtectionAmount(map);
        //截取日期
        String D1 = sDate.substring(0,5) + sDate.substring(5,7);
        String D2 = oldSdate.substring(0,5) + oldSdate.substring(5,7);
//        if(amount > 0 && !oldSdate.equals(sDate)){//说明数据已经存在，则不允许添加
//            return R.error(9999,"客户经理："+ custNm + "，失效日期为："+ D1 +"月份的保护期绩效数据已存在！");
//        }else
        if(!expireLastDayOfMonth.equals(eDate)){
            return R.error(9999,"到期日期不是生效日期的月末时间，请重新选择！");
        }else{
            ProtectionAmount protection = new ProtectionAmount();
            protection.setBranchId(branchId);
            protection.setCustId(custId);
            protection.setCustNm(custNm);
            Double protectionAmountDouble = Double.valueOf(protectionAmount);
            protection.setProtectionAmount(protectionAmountDouble);
            protection.setOldSdate(oldSdate);
            protection.setSDate(sDate);
            protection.setEDate(eDate);
            protection.setOperators(ShiroUtils.getUserEntity().getName());
            protectionAmountService.updateProtectionAmountData(protection);
            return R.ok();
        }
    }

    /**
     * 删除
     */
    @SysLog("删除保护期绩效数据")
    @RequestMapping("/deleteProtectionAmount")
    public R deleteProtectionAmount(@RequestParam Map<String, Object> params){
        //机构号
        String branchId = (String) params.get("branchId");
        //客户经理编号
        String custId = (String) params.get("custId");
        //生效日期
        String sDate = (String) params.get("sdate");
        //失效日期
        String eDate = (String) params.get("edate");
        ProtectionAmount protection = new ProtectionAmount();
        protection.setBranchId(branchId);
        protection.setCustId(custId);
        protection.setSDate(sDate);
        protection.setEDate(eDate);
        protectionAmountService.deleteProtectionAmountList(protection);
        return R.ok();
    }

    @SysLog("查询保护期绩效历史数据")
    @RequestMapping("/queryProtectionAmountHis")
    public R queryProtectionAmountHisListInfo(@RequestParam Map<String, Object> params) throws ParseException{
        if(null == params){
            return R.error("入参为空");
        }
        if(StringUtils.isEmpty((String) params.get("page")) && StringUtils.isEmpty((String) params.get("limit"))){
            return R.error("缺少必传分页参数");
        }
        //生效日期
        String yr = (String) params.get("yr") + "-01";
        //生效日期月末时间
        // 获取当月第一天和最后一天
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        // 当月最后一天
        String lastday;
        Calendar cale = Calendar.getInstance();
        cale.setTime(format.parse(yr));
        cale.add(Calendar.MONTH, 1);
        cale.set(Calendar.DAY_OF_MONTH, 0);
        lastday = format.format(cale.getTime()); // 当月最后一天
        params.put("sDate",yr);
        params.put("sDateL",lastday);

        List<ProtectionAmountHis> protectionAmountHis = protectionAmountService.queryProtectionAmountHisListInfo(params);
        int i = protectionAmountService.queryProtectionAmountHisListInfoNb(params);
        return R.ok().put("protectionAmountHisList",protectionAmountHis).put("count",i);
    }
}