package com.beyondsoft.modules.protection.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.protection.entity.ProtectionAmount;
import com.beyondsoft.modules.protection.entity.ProtectionAmountHis;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;


public interface ProtectionAmountService extends IService<ProtectionAmount> {
      List<ProtectionAmount> queryProtectionAmountListInfo(Map<String, Object> params);

      int queryProtectionAmountNb(Map<String, Object> params);

      R batchImport(String fileName, MultipartFile file, String org, String isKhy) throws Exception;

      ProtectionAmount queryProtectionByParams(Map<String, Object> params);

      void deleteProtectionAmountList(ProtectionAmount checkGrade);

      void insertProtectionAmount(ProtectionAmount checkGrade);

      void updateProtectionAmountData(ProtectionAmount checkGrade);

      int queryProtectionAmount(Map reqPara);

      List<ProtectionAmountHis> queryProtectionAmountHisListInfo(Map<String, Object> params);

      int queryProtectionAmountHisListInfoNb(Map<String, Object> params);

      String queryOrgNm(Map<String, Object> params);
}
