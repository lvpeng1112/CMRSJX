package com.beyondsoft.modules.protection.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.aofp.entity.Town;
import com.beyondsoft.modules.protection.entity.ProtectionAmount;
import com.beyondsoft.modules.protection.entity.ProtectionAmountHis;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository("ProtectionAmountDao")
public interface ProtectionAmountDao extends BaseMapper<ProtectionAmount> {

    List<ProtectionAmount> queryProtectionAmountListInfo(Map<String, Object> params);

    int queryProtectionAmountNb(Map reqPara);

    int checkOrgId(Map<String, Object> params);

    String queryOrgNm(Map<String, Object> params);

    ProtectionAmount queryProtectionByParams(Map<String, Object> params);

    void deleteProtectionAmountList(ProtectionAmount checkGrade);

    void insertProtectionAmountList(List<ProtectionAmount> list);

    void insertProtectionAmount(ProtectionAmount checkGrade);

    void updateProtectionAmountData(ProtectionAmount checkGrade);

    int queryProtectionAmount(Map reqPara);

    List<ProtectionAmountHis> queryProtectionAmountHisListInfo(Map<String, Object> params);

    int queryProtectionAmountHisListInfoNb(Map<String, Object> params);

    void insertProtectionAmountHis(ProtectionAmountHis protectionAmountHis);

    void insertProtectionAmountHisList(List<ProtectionAmountHis> list);


}
