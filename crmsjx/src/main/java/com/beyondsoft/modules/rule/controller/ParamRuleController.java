package com.beyondsoft.modules.rule.controller;

import com.alibaba.druid.util.StringUtils;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.rule.entity.ParamRule;
import com.beyondsoft.modules.rule.entity.ParamRuleHis;
import com.beyondsoft.modules.rule.service.ParamRuleService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/paramRule")
public class ParamRuleController {

    @Resource
    ParamRuleService paramRuleService;

    @RequestMapping("/allList")
    @ApiOperation("查询全部数据")
    public R queryAllData(@RequestParam Map<String, Object> params){
        Map map = new HashMap();
        if(null == params){
            return R.error("入参为空");
        }
        if(StringUtils.isEmpty((String) params.get("page")) && StringUtils.isEmpty((String) params.get("limit"))){
            return R.error("缺少必传分页参数");
        }
        List<ParamRule> rules = paramRuleService.queryAllData(params);
        int i = paramRuleService.queryDataCount(params);
        return R.ok().put("data",rules).put("total",i);
    }

    @RequestMapping("/list")
    @ApiOperation("查询")
    public R queryData(@RequestParam Map<String, Object> params){
        Map map = new HashMap();
        if(null == params){
            return R.error("入参为空");
        }
        if(StringUtils.isEmpty((String) params.get("page")) && StringUtils.isEmpty((String) params.get("limit"))){
            return R.error("缺少必传分页参数");
        }
        List<ParamRule> rules = paramRuleService.queryData(params);
        int i = paramRuleService.queryDataCount(params);
        return R.ok().put("data",rules).put("total",i);
    }
    @RequestMapping("/detailList")
    @ApiOperation("查询详情")
    public R queryDataDetail(@RequestParam Map<String, Object> params){

        List<ParamRule> rules = paramRuleService.queryDataDetail(params);
        return R.ok().put("data",rules);
    }
    @RequestMapping("/update")
    @ApiOperation("修改参数值")
    public R updateParam(@RequestParam Map<String, Object> params){
        paramRuleService.updateParam(params);
        return R.ok();
    }
    @RequestMapping("/queryDataByRuleId")
    @ApiOperation("查询")
    public R queryDataByRuleId(@RequestParam Map<String, Object> params){
        ParamRule paramRule = paramRuleService.queryDataByRuleId(params);
        return R.ok().put("data",paramRule);
    }
    @RequestMapping("/queryDataHis")
    @ApiOperation("查询历史表")
    public R queryDataHis(@RequestParam Map<String, Object> params){
        List<ParamRuleHis> paramRuleHis = paramRuleService.queryDataHis(params);
        int i = paramRuleService.queryDataHisCount(params);
        return R.ok().put("data",paramRuleHis).put("total",i);
    }
    @RequestMapping("/updateParamVals")
    @ApiOperation("修改全行数据")
    public R updateParamValsByRuleId(@RequestParam Map<String, Object> params){
        paramRuleService.updateParamValsByRuleId(params);
        return R.ok();
    }


}
