package com.beyondsoft.modules.rule.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.datasource.annotation.DataSource;
import com.beyondsoft.modules.rule.dao.ParamRuleDao;
import com.beyondsoft.modules.rule.entity.ParamRule;
import com.beyondsoft.modules.rule.entity.ParamRuleHis;
import com.beyondsoft.modules.rule.service.ParamRuleService;
import com.beyondsoft.modules.sys.entity.SysUserEntity;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Service
public class ParamRuleServiceImpl extends ServiceImpl<ParamRuleDao,ParamRule> implements ParamRuleService {
    @Resource
    ParamRuleDao paramRuleDao;

    @Override
    @DataSource(value = "second")

    public List<ParamRule> queryAllData(Map<String, Object> params) {
        int pageNo = Integer.parseInt((String) params.get("page"));
        int pageCount = Integer.parseInt((String)params.get("limit"));
        int offset = ((pageNo - 1) * pageCount);
        params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
        params.put("offset", offset);
        List<ParamRule> paramRules = paramRuleDao.queryAllData(params);
        return paramRules;
    }

    @Override
    @DataSource(value = "second")
    public int queryDataCount(Map<String, Object> params) {
        int i = paramRuleDao.queryDataCount(params);
        return i;
    }

    @Override
    @DataSource(value = "second")
    public List<ParamRule> queryData(Map<String, Object> params) {
        int pageNo = Integer.parseInt((String) params.get("page"));
        int pageCount = Integer.parseInt((String)params.get("limit"));
        int offset = ((pageNo - 1) * pageCount);
        params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
        params.put("offset", offset);
        List<ParamRule> paramRules = paramRuleDao.queryData(params);
        return paramRules;
    }

    @Override
    @DataSource(value = "second")
    public List<ParamRule> queryDataDetail(Map<String, Object> params) {
        List<ParamRule> paramDetails = paramRuleDao.queryDataDetail(params);
        return paramDetails;
    }
    public SysUserEntity getUser() {
        return (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
    }
    @Override
    @DataSource(value = "second")
    public void updateParam(Map<String, Object> params) {
        String name=getUser().getName();
        params.put("name",name);

        //新增历史记录
        ParamRule paramRule = paramRuleDao.queryDataByRuleId(params);
        List<ParamRuleHis> paramRuleHis1 = paramRuleDao.queryHisDataByOrgRul(params);
        ParamRuleHis paramRuleHis = new ParamRuleHis();
        paramRuleHis.setOrgId(paramRule.getOrgId());
        paramRuleHis.setRuleId(paramRule.getRuleId());
        paramRuleHis.setRuleNm(paramRule.getRuleNm());
        paramRuleHis.setParamId(paramRule.getParamId());
        paramRuleHis.setParamNm(paramRule.getParamNm());
        paramRuleHis.setRuleDesc(paramRule.getRuleDesc());
        paramRuleHis.setParamVal(paramRule.getParamVal());
        paramRuleHis.setOperators(paramRule.getOperators());
        paramRuleHis.setOperateDate(paramRule.getOperateDate());
        if(paramRuleHis1.size()==0){
            paramRuleDao.insertDataHis(paramRuleHis);
        }

        //更新参数
        paramRuleDao.updateParam(params);
        String paramVal = (String)params.get("paramVal");
        Double paramValD = Double.valueOf(paramVal);
        paramRuleHis.setParamVal(paramValD);
        paramRuleHis.setOperators(name);
        paramRuleDao.insertDataHis(paramRuleHis);
    }

    @Override
    @DataSource(value = "second")
    public ParamRule queryDataByRuleId(Map map) {
        ParamRule paramRule = paramRuleDao.queryDataByRuleId(map);
        return paramRule;
    }

    @Override
    @DataSource(value = "second")
    public List<ParamRuleHis> queryDataHis(Map params) {
        int pageNo = Integer.parseInt((String) params.get("page"));
        int pageCount = Integer.parseInt((String)params.get("limit"));
        int offset = ((pageNo - 1) * pageCount);
        params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
        params.put("offset", offset);
        List<ParamRuleHis> paramRuleHis = paramRuleDao.queryDataHis(params);
        return paramRuleHis;
    }

    @Override
    @DataSource(value = "second")
    public int queryDataHisCount(Map map) {
        int i = paramRuleDao.queryDataHisCount(map);
        return i;
    }

    @Override
    @DataSource(value = "second")
    public void updateParamValsByRuleId(Map map){
        List<ParamRule> paramRules = paramRuleDao.queryDatasByRuleId(map);
        Iterator<ParamRule> iterator = paramRules.iterator();
        while (iterator.hasNext()){//如果历史表里存在该机构参数数据，不插入修改前数据
            ParamRule p = iterator.next();
            String orgId = p.getOrgId();
            String ruleId = p.getRuleId();
            HashMap<String, Object> param = new HashMap<>();
            param.put("orgId",orgId);
            param.put("ruleId",ruleId);
            List<ParamRuleHis> paramRuleHis = paramRuleDao.queryHisDataByOrgRul(param);
            if(paramRuleHis.size()>0){
                iterator.remove();
            }
        }
        //插入日志
        Map<String,Object> params= new HashMap<>();
        if(paramRules.size()>0){
            params.put("list",paramRules);
            paramRuleDao.insertDataHisList(params);
        }
        //全行修改
        String name=getUser().getName();
        map.put("name",name);
        paramRuleDao.updateParamValByRuleId(map);
        //插入修改后日志
        String paramVal = (String)map.get("paramVal");
        Double aDouble = Double.valueOf(paramVal);
        List<ParamRule> paramRulesAft = paramRuleDao.queryDatasByRuleId(map);
        for(ParamRule p:paramRulesAft){
            p.setParamVal(aDouble);
            p.setOperators(name);
        }
        params.put("list",paramRulesAft);
        paramRuleDao.insertDataHisList(params);
    }
}
