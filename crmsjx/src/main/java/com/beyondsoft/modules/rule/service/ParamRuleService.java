package com.beyondsoft.modules.rule.service;

import com.beyondsoft.modules.rule.entity.ParamRule;
import com.beyondsoft.modules.rule.entity.ParamRuleHis;

import java.util.List;
import java.util.Map;

public interface ParamRuleService {
    List<ParamRule> queryAllData(Map<String,Object> params);
    int queryDataCount(Map<String,Object> params);
    List<ParamRule> queryData(Map<String,Object> params);
    List<ParamRule> queryDataDetail(Map<String,Object> params);
    void updateParam(Map<String,Object> params);
    ParamRule queryDataByRuleId(Map map);
    List<ParamRuleHis> queryDataHis(Map map);
    int queryDataHisCount(Map map);
    void updateParamValsByRuleId(Map map);
}
