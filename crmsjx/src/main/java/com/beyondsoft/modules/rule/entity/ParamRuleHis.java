package com.beyondsoft.modules.rule.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@ApiModel(value = "参数化规则信息历史表")
@TableName("PARAM_RULE_INFO_HIS")
@Data
public class ParamRuleHis implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "机构编号")
    private String orgId;
    @TableField(exist = false)
    private String orgName;
    @ApiModelProperty(value = "参数化规则ID")
    private String ruleId;
    @ApiModelProperty(value = "参数化规则名称")
    private String ruleNm;
    @ApiModelProperty(value = "参数化规则描述")
    private String ruleDesc;
    @ApiModelProperty(value = "参数ID")
    private String ParamId;
    @ApiModelProperty(value = "参数名称")
    private String paramNm;
    @ApiModelProperty(value = "参数值")
    @JsonSerialize(using= ToStringSerializer.class)
    private Double paramVal;
    @ApiModelProperty(value = "操作员")
    private String operators;
    @ApiModelProperty(value = "操作时间")
    private String operateDate;
    @ApiModelProperty(value = "失效时间")
    private String invalidDate;
}
