package com.beyondsoft.modules.rule.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.rule.entity.ParamRule;
import com.beyondsoft.modules.rule.entity.ParamRuleHis;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository("ParamRuleDao")
public interface ParamRuleDao extends BaseMapper<ParamRule> {
    List<ParamRule> queryAllData(Map map);
    int queryDataCount(Map map);
    List<ParamRule> queryData(Map map);
    List<ParamRule> queryDataDetail(Map map);
    void updateParam(Map map);
    ParamRule queryDataByRuleId(Map map);

    List<ParamRuleHis> queryDataHis(Map map);
    int queryDataHisCount(Map map);
    void insertDataHis(ParamRuleHis paramRuleHis);
    //根据ruleId修改全行
    void updateParamValByRuleId(Map map);
    //根据ruleId查询数据
    List<ParamRule> queryDatasByRuleId(Map map);

    void insertDataHisList(Map map);
    List<ParamRuleHis> queryHisDataByOrgRul(Map map);
}
