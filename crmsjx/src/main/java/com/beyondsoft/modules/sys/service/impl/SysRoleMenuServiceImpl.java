/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.datasource.annotation.DataSource;
import com.beyondsoft.modules.sys.dao.SysRoleMenuDao;
import com.beyondsoft.modules.sys.entity.SysRoleMenuEntity;
import com.beyondsoft.modules.sys.service.SysRoleMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;



/**
 * 角色与菜单对应关系
 *
 * @author shuaigao@cupdata.com
 */

@Service("sysRoleMenuService")

public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuDao, SysRoleMenuEntity> implements SysRoleMenuService {
	@Autowired
	private SysRoleMenuDao sysRoleMenuDao;
	@Override
	@Transactional
	public void saveOrUpdate(Long roleId, List<Long> menuIdList) {
		//先删除角色与菜单关系
		deleteBatch(new Long[]{roleId});
		if(menuIdList.size() == 0){
			return ;
		}
		//菜单排序字段
		Integer menuSort=0;
		//保存角色与菜单关系

		for(Long menuId : menuIdList){
			SysRoleMenuEntity sysRoleMenuEntity = new SysRoleMenuEntity();
			sysRoleMenuEntity.setMenuId(menuId);
			sysRoleMenuEntity.setRoleId(roleId);
			sysRoleMenuEntity.setMenuSort(menuSort);

			insertSysRoleMenuEntity(sysRoleMenuEntity);

			menuSort++;

			}
		}

	@Override
		public void insertSysRoleMenuEntity(SysRoleMenuEntity sysRoleMenuEntity){
				sysRoleMenuDao.insert(sysRoleMenuEntity);
		}
	@Override
	public List<Long> queryMenuIdList(Long roleId) {
		return sysRoleMenuDao.queryMenuIdList(roleId);
	}

	@Override
	public int deleteBatch(Long[] roleIds){
		return baseMapper.deleteBatch(roleIds);
	}

}
