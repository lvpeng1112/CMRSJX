package com.beyondsoft.modules.sys.controller;

import com.beyondsoft.common.annotation.SysLog;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.sys.dao.DeptMgrDao;
import com.beyondsoft.modules.sys.entity.DeptMgrEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Map;
import java.util.List;
import com.beyondsoft.modules.sys.service.DeptMgrService;

/**
 * 机构树查询
 *
 * @author shuaigao@cupdata.com
 */
@RestController
@RequestMapping("/deptMgr")
public class DeptMgrController extends AbstractController {
	@Autowired
	private DeptMgrService deptMgrService;
    @Autowired
    private DeptMgrDao deptMgrDao;
	/**
	 * 查询机构树
	 */
	@GetMapping("/queryDeptMgrInfo")
	public R list(@RequestParam Map<String, Object> params){
		List<Map<String,Object>> deptMgrList = deptMgrService.queryDeptMgrInfo(params);
		return R.ok().put("deptMgrList", deptMgrList);
	}

    /**
     * 查询机构明细信息
     */
    @GetMapping("/queryDeptDtl")
    public R queryDeptDtl(@RequestParam Map<String, Object> params){
        List<DeptMgrEntity> deptDtlList = deptMgrService.queryDeptDtl(params);
        return R.ok().put("deptDtlList", deptDtlList);
    }

    /**
     * 新增机构明细信息
     */
    @SysLog("新增机构明细信息")
    @GetMapping("/addDeptDtl")
    public R addDeptDtl(@RequestParam Map<String, Object> params){
        String msg = deptMgrService.addDeptDtl(params);
        if ("".equals(msg)) {
            return R.ok();
        } else {
            return R.error(msg);
        }
    }

    /**
     * 修改机构明细信息
     */
    @SysLog("修改机构明细信息")
    @GetMapping("/updDeptDtl")
    public R updDeptDtl(@RequestParam Map<String, Object> params){
        String msg = deptMgrService.updDeptDtl(params);
        if ("".equals(msg)) {
            return R.ok();
        } else {
            return R.error(msg);
        }
    }

    /**
     * 查询字典表
     */
    @GetMapping("/dictList")
    public R dictList(@RequestParam Map<String, Object> params){
        // 获取区域列表
        params.put("busintypeid", "AREA");
        List<Map<String,Object>> areaList = deptMgrDao.queryDictInfo(params);

        // 获取机构种类列表
        params.put("busintypeid", "DEPT_TYP");
        List<Map<String,Object>> deptTypList = deptMgrDao.queryDictInfo(params);

        // 获取机构账务类型列表
        params.put("busintypeid", "DEPT_ACCT_TYP");
        List<Map<String,Object>> deptAcctTypList = deptMgrDao.queryDictInfo(params);

        // 获取机构种类(管理/营业)列表
        params.put("busintypeid", "DEPT_MB_TYPE");
        List<Map<String,Object>> deptMbtTypList = deptMgrDao.queryDictInfo(params);
        return R.ok().put("areaList", areaList).put("deptTypList", deptTypList).put("deptAcctTypList", deptAcctTypList).put("deptMbtTypList", deptMbtTypList);
    }
}
