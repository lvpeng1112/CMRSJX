package com.beyondsoft.modules.sys.entity;

import lombok.Data;

import java.io.Serializable;


/**
 *
 *
 * @author yuchong
 */
@Data
public class DictMgrEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 字典类型代码
	 */
	private String dictTypId;
	/**
	 * 字典类型名称
	 */
	private String dictTypNm;
	/**
	 * 字典明细代码(修改前)
	 */
	private String dictDtlIdOld;
	/**
	 * 字典明细代码
	 */
	private String dictDtlId;
	/**
	 * 字典明细名称
	 */
	private String dictDtlNm;
	/**
	 * 状态
	 */
	private String status;
	/**
	 * 私有标志
	 */
	private String privilege;
	/**
	 * 顺序
	 */
	private String indx;

}
