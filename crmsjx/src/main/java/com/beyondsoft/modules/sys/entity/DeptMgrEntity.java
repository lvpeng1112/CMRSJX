package com.beyondsoft.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
public class DeptMgrEntity {
    /**
     * 机构代码
     */
    private String chdeptcode;
    /**
     * 机构名称
     */
    private String chdeptname;
    /**
     * 机构成立日期
     */
    private String chopendate;
    /**
     * 机构撤销标志
     */
    private String chcloseflag;
    /**
     * 机构级别
     */
    private String chdeptlevel;
    /**
     * 上级机构代码
     */
    private String chparentdept;
    /**
     * 通讯地址
     */
    private String chaddress;
    /**
     * 机构种类
     */
    private String chdepttype;
    /**
     * 机构种类名
     */
    private String chdepttypenm;
    /**
     * 区域
     */
    private String chdeptarea;
    /**
     * 区域名
     */
    private String chdeptareanm;
    /**
     * 机构类型：管理机构、营业机构
     */
    private String depttype;
    /**
     * 机构类型名：管理机构、营业机构
     */
    private String depttypenm;
    /**
     * 机构类型
     */
    private String type;
    /**
     * 机构类型名
     */
    private String typenm;
}
