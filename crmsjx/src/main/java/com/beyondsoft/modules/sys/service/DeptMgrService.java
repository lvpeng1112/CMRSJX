package com.beyondsoft.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.modules.sys.entity.DeptMgrEntity;

import java.util.List;
import java.util.Map;

public interface DeptMgrService extends IService<DeptMgrEntity> {
    /**
     * 查询机构树
     * @return
     */
    List<Map<String,Object>>  queryDeptMgrInfo(Map<String, Object> params);

    /**
     * 查询机构明细信息
     * @return
     */
    List<DeptMgrEntity>  queryDeptDtl(Map<String, Object> params);

    /**
     * 新增机构明细信息
     * @return
     */
    String addDeptDtl(Map<String, Object> params);

    /**
     * 更新机构明细信息
     * @return
     */
    String updDeptDtl(Map<String, Object> params);

}
