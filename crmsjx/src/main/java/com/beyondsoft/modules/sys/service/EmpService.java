package com.beyondsoft.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.common.utils.PageUtils;
import com.beyondsoft.modules.aofp.entity.CMngCheckBaseInfo;
import com.beyondsoft.modules.sys.entity.DepartmentEntity;
import com.beyondsoft.modules.sys.entity.EmpEntity;

import java.util.List;
import java.util.Map;

public interface EmpService extends IService<CMngCheckBaseInfo> {

    /**
     * 查询全部配置
     * @return
     */
    PageUtils queryPage(Map<String, Object> params);

    List<CMngCheckBaseInfo> queryDataByOrgIdEmpIdTeamCode(Map<String, Object> params);

    int queryDataByOrgIdEmpIdTeamCodeCount(Map<String, Object> params);

}
