package com.beyondsoft.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.sys.entity.DepartmentEntity;
import com.beyondsoft.modules.sys.entity.SysConfigEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface DepartmentDao extends BaseMapper<DepartmentEntity> {
    @Select(" WITH temp (CHDEPTCODE,CHPARENTDEPT,CHDEPTNAME,CHDEPTLEVEL) AS ( SELECT CHDEPTCODE,CHPARENTDEPT,CHDEPTNAME,CHDEPTLEVEL FROM NPMM.DEPARTMENT WHERE CHDEPTCODE != 0 AND CHDEPTCODE = #{chdeptcode} UNION ALL SELECT b.CHDEPTCODE,b.CHPARENTDEPT,b.CHDEPTNAME,b.CHDEPTLEVEL FROM NPMM.DEPARTMENT b,temp t WHERE b.CHPARENTDEPT =t.CHDEPTCODE ) SELECT DISTINCT CHDEPTCODE,CHPARENTDEPT,CHDEPTNAME,CHDEPTLEVEL FROM temp ORDER BY CHPARENTDEPT ASC, CHDEPTCODE ASC ")
    @Results(value = {
            @Result(column = "CHDEPTCODE",property = "chdeptcode"),
            @Result(column = "CHPARENTDEPT",property = "chparentdept"),
            @Result(column = "CHDEPTNAME",property = "chdeptname"),
            @Result(column = "CHDEPTLEVEL",property = "chdeptlevel"),

    })
    List<DepartmentEntity> getDepartMentTree(String chdeptcode);

    @Select(" WITH temp (CHDEPTCODE) AS ( SELECT CHDEPTCODE FROM NPMM.DEPARTMENT WHERE  CHDEPTCODE = #{chdeptcode} UNION ALL SELECT b.CHDEPTCODE FROM NPMM.DEPARTMENT b,temp t WHERE b.CHPARENTDEPT =t.CHDEPTCODE ) SELECT DISTINCT CHDEPTCODE FROM temp")
    @Results(value = {
            @Result(column = "CHDEPTCODE",property = "chdeptcode"),
    })
    List<DepartmentEntity> findOrgById(String chdeptcode);
}
