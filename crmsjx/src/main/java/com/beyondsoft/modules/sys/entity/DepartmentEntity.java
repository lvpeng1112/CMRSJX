package com.beyondsoft.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("department")
public class DepartmentEntity {

    private String chdeptcode;
    private String chdeptname;
    private String chdeptdesc;
    private String chopendate;
    private String chcloseflag;
    private String chclosedate;
    private String chdeptlevel;
    private String chparentdept;
    private String chphone;
    private String chaddress;
    private String chemail;
    private String chmanager;
    private String chdepttype;
    private String chdeptarea;
    private String chrptparentdept;
    private String chaggflag;
    private String chrhbsbz;
    private String chrhbbdydqh;
    private String chyybbz;
    private String chdeptxh;
    private String chbllevel;
    private String bi_dept_id;
    private String cog_dept_id;
    private String dept_type;
    private String card_dept;
    private String card_p_dept;
    private String create_time;
    private String type;
}
