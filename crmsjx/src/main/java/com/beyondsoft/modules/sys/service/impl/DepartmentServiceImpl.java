package com.beyondsoft.modules.sys.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.modules.team.dao.TeamManagerDao;
import com.beyondsoft.modules.team.entity.TeamAndEmployeeEntity;
import com.beyondsoft.modules.team.entity.TeamManagerEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.beyondsoft.modules.sys.service.DepartmentService;
import com.beyondsoft.modules.sys.entity.DepartmentEntity;
import com.beyondsoft.modules.sys.dao.DepartmentDao;
import com.beyondsoft.modules.sys.dao.SysConfigDao;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.List;


@Service("departmentService")
public class DepartmentServiceImpl extends ServiceImpl<DepartmentDao, DepartmentEntity> implements DepartmentService {

    @Autowired
    private DepartmentDao departmentDao;
    @Autowired(required = false)
    private SysConfigDao sysConfigDao;
    @Autowired(required = false)
    private TeamManagerDao teamManagerDao;


    @Override
    public List<TeamManagerEntity>  queryTeamCdList(Map<String,Object> params) {
        List<TeamManagerEntity> rettlist = new ArrayList<TeamManagerEntity>();
        List<String> orgList = new ArrayList<String>();
            //获取登陆用户属机构下团队
            List<TeamManagerEntity> tlist = teamManagerDao.selectTeamByOrg(params);
            if(tlist != null && tlist.size() > 0){
                for (TeamManagerEntity tm : tlist){
                    //获取该团队下所有的客户经理
                    List<TeamAndEmployeeEntity> emps = teamManagerDao.selectTeamByTeamCode(tm.getChTeamCode());
                    if(emps != null && emps.size() > 0){
                        String str = "";
                        for(TeamAndEmployeeEntity emp: emps){
                            str = str + emp.getChEmpleeCode().trim() + ",";
                        }
                        tm.setChTeamCode(str);
                        rettlist.add(tm);
                    }
                }
            }

        return rettlist;
    }



    @Override
    public List<Map<String,Object>>  findDepartmentTree(Map<String,Object> map) {
        Map<String,Object> result = new HashMap<String,Object>();

        String chdeptcode = (String)map.get("orgId");
        Map<String,Object> param =  new HashMap<String,Object>();
        boolean isSearch = true;
        if(chdeptcode == null|| chdeptcode.length() == 0){
           //重seeion 中获取用户的机构 request.gets
        }
        List<DepartmentEntity> departmentList =departmentDao.getDepartMentTree(chdeptcode);
        if(isSearch && (departmentList== null || departmentList.size() == 0)){
            param =  new HashMap<String,Object>();
            param.put("chdeptcode",chdeptcode);
            departmentList =departmentDao.getDepartMentTree(chdeptcode);
        }
        Map<String,Map<String,Object>> nodes = new HashMap<String,Map<String,Object>>();
        //初始话的所有的node
        for(DepartmentEntity de :departmentList){
            Map<String,Object> node = new HashMap<String,Object>();
            node.put("id",de.getChdeptcode());
         //   node.put("label",de.getChdeptcode()+"("+de.getChdeptname()+")");
            node.put("label",de.getChdeptname()+"("+de.getChdeptcode()+")");
           /* if(chdeptcode.equals(de.getChdeptcode())){
                node.put("isNew","true");
            }*/
            //默认选中
            nodes.put(de.getChdeptcode(),node);
        }
        List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
        for(DepartmentEntity de :departmentList){
           if(de.getChparentdept() == null){
               de.setChparentdept("");
           }
            Map<String,Object> node = nodes.get(de.getChdeptcode());
           if(de.getChparentdept().length() > 0){
               if(nodes.containsKey(de.getChparentdept())){
                   Map<String,Object> parentNode = nodes.get(de.getChparentdept());
                   List<Map<String,Object>> chiled = null;
                   if(!parentNode.containsKey("children")){
                       chiled = new ArrayList<Map<String,Object>>();
                       parentNode.put("children",chiled);
                   }
                  /* if(!"00001".equals(de.getChdeptcode())){
                       parentNode.put("isDefaultExpanded",false);
                   }*/
                   chiled =   (List<Map<String,Object>>)parentNode.get("children");
                   chiled.add(node);
               }else {
                   list.add(node);
               }
           }else
           {
               list.add(node);
           }

        }
        //return baseMapper.queryParamKeyList();
        return list;

    }
    @Override
        public String queryByKey(Map<String,Object> map) {
        if(sysConfigDao.queryByKey(map.get("birtUrl").toString())!= null){
            return sysConfigDao.queryByKey(map.get("birtUrl").toString()).getParamValue();
        }
        return "";
    }
}
