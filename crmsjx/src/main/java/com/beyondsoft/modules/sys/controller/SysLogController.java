/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.sys.controller;

import com.beyondsoft.common.utils.HttpContextUtils;
import com.beyondsoft.common.utils.IPUtils;
import com.beyondsoft.common.utils.PageUtils;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.sys.entity.SysLogEntity;
import com.beyondsoft.modules.sys.entity.SysUserEntity;
import com.beyondsoft.modules.sys.service.SysLogService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;


/**
 * 系统日志
 *
 * @author shuaigao@cupdata.com
 */
@RestController
@RequestMapping("/sys/log")
public class SysLogController {
	@Autowired
	private SysLogService sysLogService;
	
	/**
	 * 列表
	 */
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("sys:log:list")
	public R list(@RequestParam Map<String, Object> params){
		PageUtils page = sysLogService.queryPage(params);

		return R.ok().put("page", page);
	}
	/**
	 * 菜单点击日志
	 */
	@PostMapping("/menuClick")
	public R menuClick(@RequestBody SysLogEntity sysLog){
		//获取request
		HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
		//设置IP地址
		sysLog.setIp(IPUtils.getIpAddr(request));

		//用户名
		String username = ((SysUserEntity) SecurityUtils.getSubject().getPrincipal()).getUsername();
		sysLog.setUsername(username);
		sysLog.setCreateDate(new Date());
		//保存系统日志
		sysLogService.save(sysLog);
		return R.ok();
	}

}
