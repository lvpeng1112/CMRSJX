package com.beyondsoft.modules.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.common.utils.PageUtils;
import com.beyondsoft.common.utils.Query;
import com.beyondsoft.modules.sys.dao.DepartmentDao;
import com.beyondsoft.modules.sys.dao.RetailEmpDao;
import com.beyondsoft.modules.sys.entity.DepartmentEntity;
import com.beyondsoft.modules.sys.entity.RetailEmpEntity;
import com.beyondsoft.modules.sys.service.RetailEmpService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Service("retailEmpService")
public class RetailEmpServiceImpl extends ServiceImpl<RetailEmpDao, RetailEmpEntity> implements RetailEmpService {

    @Autowired
    private DepartmentDao departmentDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String org = (String)params.get("org");
        String empCd = (String)params.get("empCd");
        List<DepartmentEntity> departmentList =departmentDao.getDepartMentTree(org);
        IPage<RetailEmpEntity> page = null;
        List<String> orgList = new ArrayList<String>();
        if(departmentList != null && departmentList.size()>0){
            for(DepartmentEntity dl:departmentList){
                orgList.add(dl.getChdeptcode());
            }
             page = this.page(
                    new Query<RetailEmpEntity>().getPage(params),
                    new QueryWrapper<RetailEmpEntity>()
                           .in("belong_org_id",orgList)
                            .like(StringUtils.isNotBlank(empCd),"emp_id", empCd)
                            .or()
                            .like(StringUtils.isNotBlank(empCd),"emp_nm", empCd)
            );
        }
        return new PageUtils(page);
    }
}
