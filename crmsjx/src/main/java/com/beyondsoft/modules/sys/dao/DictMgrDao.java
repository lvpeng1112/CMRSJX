package com.beyondsoft.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.sys.entity.DictMgrEntity;
import org.apache.ibatis.annotations.Mapper;
import com.beyondsoft.common.utils.R;

import java.util.List;
import java.util.Map;

/**
 * 字典管理
 *
 * @author yuchong
 */
@Mapper
public interface DictMgrDao extends BaseMapper<DictMgrEntity> {
    // 查询字典类型代码列表
    List<DictMgrEntity> queryDictTypInfo(Map<String, Object> params);
    // 查询字典类型代码是否存在
    int checkDictTypId(Map<String, Object> params);
    // 新增字典类型代码
    void addDictTyp(Map<String, Object> params);
    // 修改字典类型代码
    void updDictTyp(Map<String, Object> params);
    // 删除字典类型代码
    void delDictTyp(Map<String, Object> params);
    // 删除字典明细代码
    void delDictDtl(Map<String, Object> params);
    // 查询字典明细代码列表
    List<DictMgrEntity> queryDictDtlInfo(Map<String, Object> params);
    // 查询字典明细代码是否存在
    int checkDictDtlId(Map<String, Object> params);
    // 新增字典明细代码
    void addDictDtl(Map<String, Object> params);
    // 修改字典明细代码
    void updDictDtl(Map<String, Object> params);
}
