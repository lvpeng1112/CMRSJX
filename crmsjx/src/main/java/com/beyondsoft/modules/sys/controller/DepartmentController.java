/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.sys.controller;



import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.sys.entity.DictMgrEntity;
import com.beyondsoft.modules.sys.service.DictMgrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import com.beyondsoft.modules.sys.service.DepartmentService;
import com.beyondsoft.modules.team.entity.TeamManagerEntity;

/**
 * 机构树查询
 *
 * @author shuaigao@cupdata.com
 */
@RestController
@RequestMapping("/department")
public class DepartmentController extends AbstractController {
	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private DictMgrService dictMgrService;

	/**
	 * 所有配置列表
	 */
	@GetMapping("/findOrgTree")
	public R list(@RequestParam Map<String, Object> params){

		List<Map<String,Object>> page = departmentService.findDepartmentTree(params);
		return R.ok().put("page", page);
	}

	@GetMapping("/getBirtUrl")
	public String getDict(@RequestParam Map<String, Object> params){
		String v = departmentService.queryByKey(params);
		return v;
	}
	//参数初始画
	@GetMapping("/shouwTxnList")
	public R shouwTxnList(@RequestParam Map<String, Object> params){

		Map<String,Object> param1 = new HashMap<>();
		param1.put("dictTypId","TXN_STAT_CD");
		List<DictMgrEntity> txnStatCdList = dictMgrService.queryDictDtlInfo(param1);

		Map<String,Object> param2 = new HashMap<>();
		param2.put("dictTypId","TXN_CHAN_TYPE");
		List<DictMgrEntity> chanTypeList = dictMgrService.queryDictDtlInfo(param2);

		Map<String,Object> param3 = new HashMap<>();
		param2.put("dictTypId","TXN_PROD_TYPE");
		List<DictMgrEntity> prodTypeList = dictMgrService.queryDictDtlInfo(param2);


		List<TeamManagerEntity> teamCdList = departmentService.queryTeamCdList(params);


		return R.ok().put("txnStatCdList", txnStatCdList).put("chanTypeList", chanTypeList).put("teamCdList", teamCdList).put("prodTypeList", prodTypeList);
	}
}
