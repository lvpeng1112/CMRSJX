package com.beyondsoft.modules.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.aofp.dao.CMngCheckBaseInfoDao;
import com.beyondsoft.modules.aofp.entity.CMngCheckBaseInfo;
import com.beyondsoft.modules.sys.dao.DepartmentDao;
import com.beyondsoft.modules.sys.dao.EmpDao;
import com.beyondsoft.modules.sys.entity.DepartmentEntity;
import com.beyondsoft.modules.sys.entity.EmpEntity;
import com.beyondsoft.modules.sys.service.EmpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.beyondsoft.common.utils.PageUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.beyondsoft.common.utils.Query;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Resource;


@Service("empService")
public class EmpServiceImpl extends ServiceImpl<CMngCheckBaseInfoDao, CMngCheckBaseInfo> implements EmpService {

    @Autowired
    private DepartmentDao departmentDao;

    @Resource
    CMngCheckBaseInfoDao personnelMapper;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String org = (String)params.get("org");
        String empCd = (String)params.get("empCd");
        List<DepartmentEntity> departmentList =departmentDao.getDepartMentTree(org);

        IPage<CMngCheckBaseInfo> page = null;
        List<String> orgList = new ArrayList<String>();
        if(departmentList != null && departmentList.size()>0){
            for(DepartmentEntity dl:departmentList){
                orgList.add(dl.getChdeptcode());
            }
            page = this.page(
                    new Query<CMngCheckBaseInfo>().getPage(params),
                    new QueryWrapper<CMngCheckBaseInfo>()
                            .in("ORG_ID",orgList)
                            .like(StringUtils.isNotBlank(empCd),"CUST_MNGR_ID", empCd)
                            .or()
                            .like(StringUtils.isNotBlank(empCd),"CUST_MNGR_NM", empCd)
            );
        }
        return new PageUtils(page);
    }

    public  List<CMngCheckBaseInfo> queryDataByOrgIdEmpIdTeamCode(Map<String, Object> params){
        String org = (String)params.get("org");
        String empCd = (String)params.get("empCd");
        String teamId=(String)params.get("teamId");
        String isKhy=(String)params.get("isKhy");
        String isTdz=(String)params.get("isTdz");
        String userNm=(String)params.get("userNm");
        String yr=(String)params.get("yr");
        String endDate=(String)params.get("endDate");
        Map map = new HashMap();
        map.put("orgId",org);
        map.put("teamId",teamId);
        map.put("empCd",empCd);
        map.put("isKhy",isKhy);
        map.put("isTdz",isTdz);
        map.put("userNm",userNm);
        map.put("yr",yr);
        map.put("endDate",endDate);
        int pageNo = Integer.parseInt((String) params.get("page"));
        int pageCount = Integer.parseInt((String)params.get("limit"));
        int offset = ((pageNo - 1) * pageCount);
        map.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
        map.put("offset", offset);
        List list = personnelMapper.queryDataByOrgIdEmpIdTeamCode(map);
        return list;
    }

    @Override
    public int queryDataByOrgIdEmpIdTeamCodeCount(Map<String, Object> params) {
        String org = (String)params.get("org");
        String empCd = (String)params.get("empCd");
        String teamId=(String)params.get("teamId");
        String isKhy=(String)params.get("isKhy");
        String isTdz=(String)params.get("isTdz");
        String userNm=(String)params.get("userNm");
        String yr=(String)params.get("yr");
        String endDate=(String)params.get("endDate");
        Map map = new HashMap();
        map.put("orgId",org);
        map.put("teamId",teamId);
        map.put("empCd",empCd);
        map.put("isKhy",isKhy);
        map.put("isTdz",isTdz);
        map.put("userNm",userNm);
        map.put("yr",yr);
        map.put("endDate",endDate);
        int i = personnelMapper.queryDataByOrgIdEmpIdTeamCodeCount(map);
        return i;
    }
}
