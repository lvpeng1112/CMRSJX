/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.sys.controller;


import com.alibaba.druid.util.StringUtils;
import com.beyondsoft.common.utils.PageUtils;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.aofp.entity.CMngCheckBaseInfo;
import com.beyondsoft.modules.sys.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.beyondsoft.modules.sys.service.EmpService;

/**
 * 分页查询所有客户经理
 */
@RestController
@RequestMapping("/emp")
public class EmpController extends AbstractController {
	@Autowired
	private EmpService empService;

	/**
	 * 分页查询所有客户经理
	 */
	@GetMapping("/list")
	public R list(@RequestParam Map<String, Object> params){
		String yr = (String) params.get("yr");
		String endDate = (String) params.get("endDate");
		if(yr.length()<=4){
			yr=yr+"-01";
		}
		if(endDate.length()<=4){
			endDate=endDate+"-12";
		}
		params.put("yr",yr);
		params.put("endDate",endDate);
		if(null == params){
			return R.error("入参为空");
		}
		if(StringUtils.isEmpty((String) params.get("page")) && StringUtils.isEmpty((String) params.get("limit"))){
			return R.error("缺少必传分页参数");
		}
		List<CMngCheckBaseInfo> cMngCheckBaseInfos = empService.queryDataByOrgIdEmpIdTeamCode(params);
		int i = empService.queryDataByOrgIdEmpIdTeamCodeCount(params);
//		PageUtils page = empService.queryPage(params);
		return R.ok().put("page", cMngCheckBaseInfos).put("count",i);
	}

}
