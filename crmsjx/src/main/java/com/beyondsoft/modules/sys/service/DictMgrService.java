package com.beyondsoft.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.modules.sys.entity.DictMgrEntity;
import java.util.List;
import java.util.Map;
/**
 * 字典管理
 *
 * @author yuchong
 */
public interface DictMgrService extends IService<DictMgrEntity> {
	// 查询字典类型代码列表
	List<DictMgrEntity> queryDictTypInfo(Map<String, Object> params);
	// 新增字典类型代码
	String addDictTyp(Map<String, Object> params);
	// 修改字典类型代码
	String updDictTyp(Map<String, Object> params);
	// 删除字典类型代码
    String delDictTyp(Map<String, Object> params);
	// 查询字典明细代码列表
	List<DictMgrEntity> queryDictDtlInfo(Map<String, Object> params);
	// 新增字典明细代码
	String addDictDtl(Map<String, Object> params);
	// 修改字典明细代码
	String updDictDtl(Map<String, Object> params);
	// 删除字典明细代码
    String delDictDtl(Map<String, Object> params);
}
