/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.sys.controller;

import com.beyondsoft.common.annotation.SysLog;
import com.beyondsoft.common.utils.RSATool;
import com.beyondsoft.common.validator.Assert;
import com.beyondsoft.common.validator.ValidatorUtils;
import com.beyondsoft.common.validator.group.AddGroup;
import com.beyondsoft.common.validator.group.UpdateGroup;
import com.beyondsoft.common.utils.Constant;
import com.beyondsoft.common.utils.PageUtils;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.sys.dao.SysConfigDao;
import com.beyondsoft.modules.sys.dao.SysUserDao;
import com.beyondsoft.modules.sys.entity.SysUserEntity;
import com.beyondsoft.modules.sys.form.PasswordForm;
import com.beyondsoft.modules.sys.service.SysCaptchaService;
import com.beyondsoft.modules.sys.service.SysUserRoleService;
import com.beyondsoft.modules.sys.service.SysUserService;
import org.apache.commons.lang.ArrayUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 系统用户
 *
 * @author shuaigao@cupdata.com
 */
@RestController
@RequestMapping("/sys/user")
public class SysUserController extends AbstractController {
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private SysUserRoleService sysUserRoleService;
	@Autowired
	private SysCaptchaService sysCaptchaService;

	@Autowired(required = false)
    SysUserDao sysUserDao;


	/**
	 * 所有用户列表
	 */
	@GetMapping("/list")
	@RequiresPermissions("sys:user:list")
	public R list(@RequestParam Map<String, Object> params){
//		//只有超级管理员，才能查看所有管理员列表
//		if(getUserId() != Constant.SUPER_ADMIN){
//			params.put("createUserId", getUserId());
//		}
//		PageUtils page = sysUserService.queryPage(params);
//
//		return R.ok().put("page", page);
		//机构号
		String initCode = getUser().getOrgId();
		String orgId = (String)params.get("orgId");
		if(orgId !=null && orgId !=""){
			params.put("chOrgId", orgId);
		}else{
			params.put("chOrgId", initCode);
		}
		//分页参数
		int pageNo = Integer.parseInt((String) params.get("page"));
		int pageCount = Integer.parseInt((String)params.get("limit"));
		int offset = ((pageNo - 1) * pageCount);
		params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
		params.put("offset", offset);
		List<SysUserEntity> queryUserDataList = sysUserDao.queryUserDataList(params);
		int count = sysUserDao.queryUserDataListNb(params);
		return R.ok().put("count", count).put("queryUserDataList",queryUserDataList);
	}
	
	/**
	 * 获取登录的用户信息
	 */
	@GetMapping("/info")
	public R info(){
		return R.ok().put("user", getUser());
	}

	/**
	 * 初始化登录用户修改密码
	 */
	@SysLog("初始化登录用户修改密码")
	@PostMapping("/firstUpdatePassword")
	public R firstUpdatePassword(@RequestBody PasswordForm form){
		//获取修改密码用户信息
		SysUserEntity sysUserEntity = sysUserService.queryByUserName(form.getUsername());
		//设置用户信息
//		((SysUserEntity) SecurityUtils.getSubject().getPrincipal()).setUsername(sysUserEntity.getUsername());
		Assert.isBlank(form.getNewPassword(), "新密码不为能空");

		boolean captcha = sysCaptchaService.validate(form.getUuid(), form.getCaptcha());
		if(!captcha){
			return R.error("验证码不正确");
		}

		String password1 = "";       //新密码
		String newPassword1 = "";    //旧密码

		try {
			//旧密码
			String codePassword = RSATool.decodeByPrivateKey(form.getPassword(), "");
			password1 = codePassword.substring(2, Integer.parseInt(codePassword.substring(0, 2)) + 2);

			//新密码
			String codeNewPassword = RSATool.decodeByPrivateKey(form.getNewPassword(), "");
			newPassword1 = codeNewPassword.substring(2, Integer.parseInt(codeNewPassword.substring(0, 2)) + 2);
		}catch (Exception e){
			e.printStackTrace();
		}

		//如果新密码和原密码一样则进行提醒
		if(password1.equals(newPassword1)){
			return R.error("新密码和原密码不能一样！");
		}

		//sha256加密
		String password = new Sha256Hash(password1, sysUserEntity.getSalt()).toHex();
		//sha256加密
		String newPassword = new Sha256Hash(newPassword1, sysUserEntity.getSalt()).toHex();

		//更新密码
		boolean flag = sysUserService.updatePassword(sysUserEntity.getUserId(), password, newPassword);
		if(!flag){
			return R.error("原密码不正确");
		}

		return R.ok();
	}
	
	/**
	 * 修改登录用户密码
	 */
	@SysLog("修改密码")
	@PostMapping("/password")
	public R password(@RequestBody PasswordForm form){
		Assert.isBlank(form.getNewPassword(), "新密码不为能空");

		boolean captcha = sysCaptchaService.validate(form.getUuid(), form.getCaptcha());
		if(!captcha){
			return R.error("验证码不正确");
		}

		String password1 = "";       //新密码
		String newPassword1 = "";    //旧密码

		try {
			//旧密码
			String codePassword = RSATool.decodeByPrivateKey(form.getPassword(), "");
			password1 = codePassword.substring(2, Integer.parseInt(codePassword.substring(0, 2)) + 2);

			//新密码
			String codeNewPassword = RSATool.decodeByPrivateKey(form.getNewPassword(), "");
			newPassword1 = codeNewPassword.substring(2, Integer.parseInt(codeNewPassword.substring(0, 2)) + 2);
		}catch (Exception e){
			e.printStackTrace();
		}

		//如果新密码和原密码一样则进行提醒
		if(password1.equals(newPassword1)){
			return R.error("新密码和原密码不能一样！");
		}
		
		//sha256加密
		String password = new Sha256Hash(password1, getUser().getSalt()).toHex();
		//sha256加密
		String newPassword = new Sha256Hash(newPassword1, getUser().getSalt()).toHex();
				
		//更新密码
		boolean flag = sysUserService.updatePassword(getUserId(), password, newPassword);
		if(!flag){
			return R.error("原密码不正确");
		}
		
		return R.ok();
	}
	@SysLog("重置密码")
	@PostMapping("/resetPassword")
	public R resetPassword(@RequestParam Map<String, Object> params){
		String userId=(String)params.get("userId");

		String newPassword="000000";

		String salt = sysUserService.querySaltByUserId(userId);

		//sha256加密
		String newPassword1 = new Sha256Hash(newPassword, salt).toHex();

		boolean b = sysUserService.resetPassword(Long.valueOf(userId), newPassword1);
		return R.ok();
	}
	/**
	 * 用户信息
	 */
	@GetMapping("/info/{userId}")
	@RequiresPermissions("sys:user:info")
	public R info(@PathVariable("userId") Long userId){
		SysUserEntity user = sysUserService.getById(userId);
		
		//获取用户所属的角色列表
		List<Long> roleIdList = sysUserRoleService.queryRoleIdList(userId);
		user.setRoleIdList(roleIdList);
		
		return R.ok().put("user", user);
	}
	
	/**
	 * 保存用户
	 */
	@SysLog("保存用户")
	@PostMapping("/save")
	@RequiresPermissions("sys:user:save")
	public R save(@RequestBody SysUserEntity user){
		Map map = new HashMap();
		ValidatorUtils.validateEntity(user, AddGroup.class);

		//新增用户信息的时候，进行用户名是否存在校验
		map.put("uname",user.getUname());
        SysUserEntity count = sysUserDao.queryUserByNm(map);
        if(count != null){
            return R.error(999999, "用户名已存在，请重新输入！");
        }else{
            user.setCreateUserId(getUserId());

            sysUserService.saveUser(user);
            return R.ok();
        }
	}
	
	/**
	 * 修改用户
	 */
	@SysLog("修改用户")
	@PostMapping("/update")
	@RequiresPermissions("sys:user:update")
	public R update(@RequestBody SysUserEntity user){
        Map map = new HashMap();
		ValidatorUtils.validateEntity(user, UpdateGroup.class);
		map.put("userId",user.getUserId());
        SysUserEntity sysUserEntity = sysUserDao.queryUserByNm(map);
        //修改前用户名称
        String usernameBefore = sysUserEntity.getUsername();
        //修改后用户名称
        String usernameAfter = user.getUname();
        if(!usernameAfter.equals(usernameBefore)){//如果修改前后用户名称不一致，则进行用户名校验
            map.clear();
            map.put("uname",usernameAfter);
            SysUserEntity count = sysUserDao.queryUserByNm(map);
            if(count != null){
                return R.error(999999, "用户名已存在，请重新输入！");
            }else{
                user.setCreateUserId(getUserId());
                sysUserService.update(user);

                return R.ok();
            }
        }else{
            user.setCreateUserId(getUserId());
            sysUserService.update(user);

            return R.ok();
        }
	}
	
	/**
	 * 删除用户
	 */
	@SysLog("删除用户")
	@PostMapping("/delete")
	@RequiresPermissions("sys:user:delete")
	public R delete(@RequestBody Long[] userIds){
		if(ArrayUtils.contains(userIds, 1L)){
			return R.error("系统管理员不能删除");
		}
		
		if(ArrayUtils.contains(userIds, getUserId())){
			return R.error("当前用户不能删除");
		}
		
		sysUserService.deleteBatch(userIds);
		// 删除用户与角色的关系
		sysUserService.deleteUserRole(userIds);
		
		return R.ok();
	}
}
