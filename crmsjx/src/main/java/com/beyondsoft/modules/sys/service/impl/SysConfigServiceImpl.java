/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.modules.sys.dao.SysConfigDao;
import com.beyondsoft.modules.sys.entity.SysConfigEntity;
import com.beyondsoft.modules.sys.redis.SysConfigRedis;
import com.beyondsoft.modules.sys.service.SysConfigService;
import com.beyondsoft.cache.PropCache;
import com.google.gson.Gson;
import com.beyondsoft.common.exception.RRException;
import com.beyondsoft.common.utils.PageUtils;
import com.beyondsoft.common.utils.Query;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("sysConfigService")
public class SysConfigServiceImpl extends ServiceImpl<SysConfigDao, SysConfigEntity> implements SysConfigService {
	@Autowired
	private SysConfigRedis sysConfigRedis;

	@Override
	public PageUtils queryPage(Map<String, Object> params) {
		String paramKey = (String)params.get("paramKey");

		IPage<SysConfigEntity> page = this.page(
			new Query<SysConfigEntity>().getPage(params),
			new QueryWrapper<SysConfigEntity>()
				.like(StringUtils.isNotBlank(paramKey),"param_key", paramKey)
				.eq("status", 1)
		);

		return new PageUtils(page);
	}

	@Override
	public void saveConfig(SysConfigEntity config) {
		this.save(config);
		sysConfigRedis.saveOrUpdate(config);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void update(SysConfigEntity config) {
		this.updateById(config);
		sysConfigRedis.saveOrUpdate(config);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateValueByKey(String key, String value) {
		baseMapper.updateValueByKey(key, value);
		sysConfigRedis.delete(key);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteBatch(Long[] ids) {
		for(Long id : ids){
			SysConfigEntity config = this.getById(id);
			sysConfigRedis.delete(config.getParamKey());
		}

		this.removeByIds(Arrays.asList(ids));
	}

	@Override
	public String getValue(String key) {
		SysConfigEntity config = sysConfigRedis.get(key);
		if(config == null){
			config = baseMapper.queryByKey(key);
			sysConfigRedis.saveOrUpdate(config);
		}

		return config == null ? null : config.getParamValue();
	}
	
	@Override
	public <T> T getConfigObject(String key, Class<T> clazz) {
		String value = getValue(key);
		if(StringUtils.isNotBlank(value)){
			return new Gson().fromJson(value, clazz);
		}

		try {
			return clazz.newInstance();
		} catch (Exception e) {
			throw new RRException("获取参数失败");
		}
	}

	@Override
	public Map queryByKeyVague(Map reqPara) {

		Map<String, Object> rtnMap = new HashMap<>();

		String bankNum = (String) reqPara.get("bankNum");

		String paramKey = "showOrHide-" + "0000" + "%";
		reqPara.put("paramKey",paramKey);
		List<SysConfigEntity> SysConfigList = baseMapper.queryByKeyVague(reqPara);

		for (int i = 0; i < SysConfigList.size(); i++) {
			SysConfigEntity sysConfigEntity = SysConfigList.get(i);
			String[] str = sysConfigEntity.getParamKey().split("-");

			String paramKey1 = "showOrHide-" + bankNum + "-"+ str[2];
			String paramValue = PropCache.getCacheInfo(paramKey1);

			if(paramValue == null || "".equals(paramValue)){
				rtnMap.put(str[0]+str[2],sysConfigEntity.getParamValue());
			}else {
				rtnMap.put(str[0]+str[2],paramValue);
			}
		}

		return rtnMap;
	}


	@Override
	public Map queryStepConfig(String systemId, String bankNum, String type) {
		Map<String, Object> stepMap = new HashMap<>();

		String[] stepIds = null;
		String[] stepNames = null;

		//获取系统步骤标识
		String stepId = PropCache.getCacheInfo(systemId + "-" + bankNum + "-" +type+ "-stepId");
		//获取系统步骤名称
		String stepName = PropCache.getCacheInfo(systemId + "-" + bankNum + "-" +type+ "-stepName");

		if(stepId == null || "".equals(stepId)){
			stepId = PropCache.getCacheInfo(systemId + "-" + "0000" + "-" +type+ "-stepId");
		}
		if(stepName == null || "".equals(stepName)){
			stepName = PropCache.getCacheInfo(systemId + "-" + "0000" + "-" +type+ "-stepName");
		}

		stepIds = stepId.split(",");
		stepNames = stepName.split(",");
		stepMap.put("stepIds",stepIds);
		stepMap.put("stepNames",stepNames);

		return stepMap;
	}

	@Override
	public List<SysConfigEntity> queryParamKeyList() {
		return baseMapper.queryParamKeyList();
	}
}
