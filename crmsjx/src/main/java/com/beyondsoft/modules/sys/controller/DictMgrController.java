package com.beyondsoft.modules.sys.controller;


import com.beyondsoft.common.annotation.SysLog;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.sys.entity.DictMgrEntity;
import com.beyondsoft.modules.sys.service.DictMgrService;
import com.beyondsoft.modules.app.utils.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * 字典管理
 *
 * @author yuchong
 */
@RestController
@RequestMapping("/sys/dictMgr")
public class DictMgrController {
    @Autowired
    private DictMgrService dictMgrService;
    @Autowired
    private JwtUtils jwtUtils;

    /**
     * 查询字典类型代码列表
     */
    @RequestMapping("/dictTypList")
    public R dictTypList(@RequestParam Map<String, Object> params){
        List<DictMgrEntity> dictTypList = dictMgrService.queryDictTypInfo(params);
        return R.ok().put("dictTypList", dictTypList);
    }

    /**
     * 新增字典类型代码
     */
    @SysLog("新增字典类型代码")
    @RequestMapping("/addDictTyp")
    public R addDictTyp(@RequestParam Map<String, Object> params){
        String msg = dictMgrService.addDictTyp(params);
        if ("".equals(msg)) {
            return R.ok();
        } else {
            return R.error(msg);
        }
    }

    /**
     * 修改字典类型代码
     */
    @SysLog("修改字典类型代码")
    @RequestMapping("/updDictTyp")
    public R updDictTyp(@RequestParam Map<String, Object> params){
        String msg = dictMgrService.updDictTyp(params);
        if ("".equals(msg)) {
            return R.ok();
        } else {
            return R.error(msg);
        }
    }

    /**
     * 删除字典类型代码
     */
    @SysLog("删除字典类型代码")
    @RequestMapping("/delDictTyp")
    public R delDictTyp(@RequestParam Map<String, Object> params){
        String msg = dictMgrService.delDictTyp(params);
        if ("".equals(msg)) {
            return R.ok();
        } else {
            return R.error(msg);
        }
    }

    /**
     * 查询字典明细代码列表
     */
    @RequestMapping("/dictDtlList")
    public R dictDtlList(@RequestParam Map<String, Object> params){
        List<DictMgrEntity> dictDtlList = dictMgrService.queryDictDtlInfo(params);
        return R.ok().put("dictDtlList", dictDtlList);
    }

    /**
     * 新增字典明细代码
     */
    @SysLog("新增字典明细代码")
    @RequestMapping("/addDictDtl")
    public R addDictDtl(@RequestParam Map<String, Object> params){
        String msg = dictMgrService.addDictDtl(params);
        if ("".equals(msg)) {
            return R.ok();
        } else {
            return R.error(msg);
        }
    }

    /**
     * 修改字典明细代码
     */
    @SysLog("修改字典明细代码")
    @RequestMapping("/updDictDtl")
    public R updDictDtl(@RequestParam Map<String, Object> params){
        String msg = dictMgrService.updDictDtl(params);
        if ("".equals(msg)) {
            return R.ok();
        } else {
            return R.error(msg);
        }
    }

    /**
     * 删除字典明细代码
     */
    @SysLog("删除字典明细代码")
    @RequestMapping("/delDictDtl")
    public R delDictDtl(@RequestParam Map<String, Object> params){
        String msg = dictMgrService.delDictDtl(params);
        if ("".equals(msg)) {
            return R.ok();
        } else {
            return R.error(msg);
        }
    }
}
