/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.sys.entity.SysUserEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 系统用户
 *
 * @author shuaigao@cupdata.com
 */
@Mapper
public interface SysUserDao extends BaseMapper<SysUserEntity> {
	
	/**
	 * 查询用户的所有权限
	 * @param userId  用户ID
	 */
	List<String> queryAllPerms(Long userId);
	
	/**
	 * 查询用户的所有菜单ID
	 */
	List<Long> queryAllMenuId(Long userId);
	
	/**
	 * 根据用户名，查询系统用户
	 */
	SysUserEntity queryByUserName(String username);

	void insertCrmUser(String username);

	/**
	 * 查询用户名是否已经存在
	 */
	SysUserEntity queryUserByNm(Map reqPara);

	/**
	 * 查询用户数据信息
	 */
	List<SysUserEntity> queryUserDataList(Map reqPara);

	/**
	 * 查询用户总数
	 */
	public int queryUserDataListNb(Map reqPara);

	/**
	 * 删除用户与角色关系
	 */
	public void deleteUserRole(Long[] userId);

	public String querySaltByUserId(Map reqPara);
}
