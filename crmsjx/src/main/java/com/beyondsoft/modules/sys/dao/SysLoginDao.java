/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.sys.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.sys.entity.SysUserEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 登录
 *
 * @author shuaigao@cupdata.com
 */
@Mapper
public interface SysLoginDao extends BaseMapper<SysUserEntity> {

    //根据用户id查询用户机构号
    public String queryOrgIdByUserNm(Map map);

    //根据用户id查询是否考核员
    public String queryIsKhyeByUserNm(Map map);

    //根据用户id查询角色信息
    public List<Map<String,Object>> queryRoleNmByUserId(Map map);

    //根据用户id查询团队长信息
    public int queryLeadIdByUserId(Map map);
}
