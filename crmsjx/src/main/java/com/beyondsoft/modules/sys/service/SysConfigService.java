/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.modules.sys.entity.SysConfigEntity;
import com.beyondsoft.common.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * 系统配置信息
 *
 * @author shuaigao@cupdata.com
 */
public interface SysConfigService extends IService<SysConfigEntity> {

	PageUtils queryPage(Map<String, Object> params);
	
	/**
	 * 保存配置信息
	 */
	public void saveConfig(SysConfigEntity config);
	
	/**
	 * 更新配置信息
	 */
	public void update(SysConfigEntity config);
	
	/**
	 * 根据key，更新value
	 */
	public void updateValueByKey(String key, String value);
	
	/**
	 * 删除配置信息
	 */
	public void deleteBatch(Long[] ids);
	
	/**
	 * 根据key，获取配置的value值
	 * 
	 * @param key           key
	 */
	public String getValue(String key);
	
	/**
	 * 根据key，获取value的Object对象
	 * @param key    key
	 * @param clazz  Object对象
	 */
	public <T> T getConfigObject(String key, Class<T> clazz);

	/**
	 * 根据银行号查询功能模块显示权限
	 * @param reqPara
	 * @return
	 */
	public Map queryByKeyVague(Map reqPara);

	/**
	 *  * 根据 systemId, bankNum,type查询步骤配置
	 * @param systemId
	 * @param bankNum
	 * @param type
	 * @return
	 */

	public Map queryStepConfig(String systemId, String bankNum, String type);

	/**
	 * 查询全部配置
	 * @return
	 */
	List<SysConfigEntity> queryParamKeyList();

}
