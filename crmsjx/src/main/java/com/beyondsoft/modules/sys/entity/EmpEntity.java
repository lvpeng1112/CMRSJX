package com.beyondsoft.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("employee")
public class EmpEntity {
    private String dtDate;
    private String org;
    private String empCd;
    private String empNm;
    private String stDt;
    private String endDt;
}
