package com.beyondsoft.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.sys.entity.DeptMgrEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface DeptMgrDao extends BaseMapper<DeptMgrEntity> {
    // 查询机构树
    List<DeptMgrEntity> queryDeptMgrInfo(Map<String, Object> params);
    // 查询机构明细信息
    List<DeptMgrEntity> queryDeptDtl(Map<String, Object> params);
    // 新增机构明细信息
    void addDeptDtl(Map<String, Object> params);
    // 更新机构明细信息
    void updDeptDtl(Map<String, Object> params);
    // 查询字典表
    List<Map<String,Object>> queryDictInfo(Map<String, Object> params);
}
