package com.beyondsoft.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.common.utils.PageUtils;
import com.beyondsoft.modules.sys.entity.DepartmentEntity;
import com.beyondsoft.modules.sys.entity.SysConfigEntity;
import com.beyondsoft.modules.team.entity.TeamManagerEntity;

import java.util.List;
import java.util.Map;

public interface DepartmentService extends IService<DepartmentEntity> {

    /**
     * 查询全部配置
     * @return
     */
    List<Map<String,Object>>  findDepartmentTree(Map<String, Object> map);

    /**
     *获取birtUrl参数
     * @return
     */
    String  queryByKey(Map<String, Object> map);
    /*
    获取所属机构的团队数据
     */
    List<TeamManagerEntity> queryTeamCdList(Map<String, Object> map);

}
