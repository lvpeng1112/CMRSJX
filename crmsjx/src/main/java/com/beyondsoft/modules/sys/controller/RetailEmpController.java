/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.sys.controller;


import com.beyondsoft.common.utils.PageUtils;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.sys.service.EmpService;
import com.beyondsoft.modules.sys.service.RetailEmpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 机构树查询
 *
 * @author shuaigao@cupdata.com
 */
@RestController
@RequestMapping("/retailEmp")
public class RetailEmpController extends AbstractController {
	@Autowired
	private RetailEmpService retailEmpService;
	/**
	 * 零售客户经理查询分页查询所有客户经理
	 */
	@GetMapping("/list")
	public R list(@RequestParam Map<String, Object> params){
		PageUtils page = page = retailEmpService.queryPage(params);
		return R.ok().put("page", page);
	}

}

