package com.beyondsoft.modules.sys.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.beyondsoft.modules.sys.service.DeptMgrService;
import com.beyondsoft.modules.sys.entity.DeptMgrEntity;
import com.beyondsoft.modules.sys.dao.DeptMgrDao;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.List;


@Service("deptMgrService")
public class DeptMgrServiceImpl extends ServiceImpl<DeptMgrDao, DeptMgrEntity> implements DeptMgrService {
    @Autowired
    private DeptMgrDao deptMgrDao;


    @Override
    public List<Map<String,Object>>  queryDeptMgrInfo(Map<String,Object> params) {
        Map<String, Object> result = new HashMap<String, Object>();

        String chdeptcode = (String) params.get("chdeptcode");
        params.put("chdeptcode", chdeptcode);
        boolean isSearch = true;
        if (chdeptcode == null || chdeptcode.length() == 0) {
            //重seeion 中获取用户的机构 request.gets
        }
        List<DeptMgrEntity> departmentList = deptMgrDao.queryDeptMgrInfo(params);
        if (isSearch && (departmentList == null || departmentList.size() == 0)) {
            departmentList = deptMgrDao.queryDeptMgrInfo(params);
        }
        Map<String, Map<String, Object>> nodes = new HashMap<String, Map<String, Object>>();
        //初始话的所有的node
        for (DeptMgrEntity de : departmentList) {
            Map<String, Object> node = new HashMap<String, Object>();
            node.put("id", de.getChdeptcode());
            node.put("label", de.getChdeptcode() + "(" + de.getChdeptname() + ")");
           /* if(chdeptcode.equals(de.getChdeptcode())){
                node.put("isNew","true");
            }*/
            //默认选中
            nodes.put(de.getChdeptcode(), node);
        }
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        for (DeptMgrEntity de : departmentList) {
            if (de.getChparentdept() == null) {
                de.setChparentdept("");
            }
            Map<String, Object> node = nodes.get(de.getChdeptcode());
            if (de.getChparentdept().length() > 0) {
                if (nodes.containsKey(de.getChparentdept())) {
                    Map<String, Object> parentNode = nodes.get(de.getChparentdept());
                    List<Map<String, Object>> chiled = null;
                    if (!parentNode.containsKey("children")) {
                        chiled = new ArrayList<Map<String, Object>>();
                        parentNode.put("children", chiled);
                    }
                  /* if(!"00001".equals(de.getChdeptcode())){
                       parentNode.put("isDefaultExpanded",false);
                   }*/
                    chiled = (List<Map<String, Object>>) parentNode.get("children");
                    chiled.add(node);
                } else {
                    list.add(node);
                }
            } else {
                list.add(node);
            }

        }
        //return baseMapper.queryParamKeyList();
        return list;
    }
    @Override
    public List<DeptMgrEntity>  queryDeptDtl(Map<String,Object> params) {
        List<DeptMgrEntity> deptDtlList = new ArrayList<>();
        String chdeptcode = (String) params.get("chdeptcode");
        params.put("chdeptcode", chdeptcode);
        deptDtlList = deptMgrDao.queryDeptDtl(params);
        return deptDtlList;
    }
    @Override
    public String addDeptDtl(Map<String,Object> params) {
        // 机构代码
        String chdeptcode = (String) params.get("chdeptcode");
        // 机构名称
        String chdeptname = (String) params.get("chdeptname");
        // 机构成立日期
        String chopendate = (String) params.get("chopendate");
        // 上级机构代码
        String chparentdept = (String) params.get("chparentdept");
        // 通讯地址
        String chaddress = (String) params.get("chaddress");
        // 机构种类
        String chdepttype = (String) params.get("chdepttype");
        // 区域
        String chdeptarea = (String) params.get("chdeptarea");
        // 机构类型：管理机构、营业机构
        String depttype = (String) params.get("depttype");
        // 机构类型
        String type = (String) params.get("type");

        params.put("chdeptcode", chdeptcode);
        params.put("chdeptname", chdeptname);
        params.put("chopendate", chopendate);
        params.put("chparentdept", chparentdept);
        params.put("chaddress", chaddress);
        params.put("chdepttype", chdepttype);
        params.put("chdeptarea", chdeptarea);
        params.put("depttype", depttype);
        params.put("type", type);
        try {
            List<DeptMgrEntity> deptDtlList = deptMgrDao.queryDeptDtl(params);
            if (deptDtlList != null && deptDtlList.size() != 0) {
                return ("机构代码已经存在！");
            }
            deptMgrDao.addDeptDtl(params);
        } catch (Exception e){
            return ("新增机构代码异常！");
        }
        return "";
    }
    @Override
    public String updDeptDtl(Map<String,Object> params) {
        // 机构代码
        String chdeptcode = (String) params.get("chdeptcode");
        // 机构名称
        String chdeptname = (String) params.get("chdeptname");
        // 机构成立日期
        String chopendate = (String) params.get("chopendate");
        // 上级机构代码
        String chparentdept = (String) params.get("chparentdept");
        // 通讯地址
        String chaddress = (String) params.get("chaddress");
        // 机构种类
        String chdepttype = (String) params.get("chdepttype");
        // 区域
        String chdeptarea = (String) params.get("chdeptarea");
        // 机构类型：管理机构、营业机构
        String depttype = (String) params.get("depttype");
        // 机构类型
        String type = (String) params.get("type");

        params.put("chdeptcode", chdeptcode);
        params.put("chdeptname", chdeptname);
        params.put("chopendate", chopendate);
        params.put("chparentdept", chparentdept);
        params.put("chaddress", chaddress);
        params.put("chdepttype", chdepttype);
        params.put("chdeptarea", chdeptarea);
        params.put("depttype", depttype);
        params.put("type", type);
        try {
            deptMgrDao.updDeptDtl(params);
        } catch (Exception e){
            return ("更新机构代码异常！");
        }
        return "";
    }
}
