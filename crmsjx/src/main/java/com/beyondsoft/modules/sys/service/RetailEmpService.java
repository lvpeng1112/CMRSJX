package com.beyondsoft.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.common.utils.PageUtils;
import com.beyondsoft.modules.sys.entity.RetailEmpEntity;

import java.util.Map;

public interface RetailEmpService extends IService<RetailEmpEntity> {

    /**
     * 查询全部配置
     * @return
     */
    PageUtils queryPage(Map<String, Object> params);
}
