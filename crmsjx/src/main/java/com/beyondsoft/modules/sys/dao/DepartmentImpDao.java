package com.beyondsoft.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.sys.entity.DepartmentEntity;
import com.beyondsoft.modules.sys.entity.DeptMgrEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface DepartmentImpDao extends BaseMapper<DepartmentEntity> {
    List<DeptMgrEntity> getDepartments();
}
