package com.beyondsoft.modules.sys.service.impl;

import java.util.*;
import java.util.Map;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.sys.dao.DictMgrDao;
import com.beyondsoft.modules.sys.entity.DictMgrEntity;
import com.beyondsoft.modules.sys.service.DictMgrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("dictMgrService")
public class DictMgrServiceImpl extends ServiceImpl<DictMgrDao, DictMgrEntity> implements DictMgrService {
	@Autowired(required = false)
	private DictMgrDao dictMgrDao;

	@Override
	public List<DictMgrEntity> queryDictTypInfo(Map<String, Object> params) {
		String dictTypId = (String) params.get("dictTypId");
		String dictTypNm = (String) params.get("dictTypNm");
		params.put("dictTypId", dictTypId);
		params.put("dictTypNm", dictTypNm);
		List<DictMgrEntity> dictTypList = new ArrayList<>();
		dictTypList = dictMgrDao.queryDictTypInfo(params);
		return dictTypList;
	}

	@Override
	public String addDictTyp(Map<String, Object> params) {
		String dictTypId = (String) params.get("dictTypId");
		String dictTypNm = (String) params.get("dictTypNm");
		params.put("dictTypId", dictTypId);
		params.put("dictTypNm", dictTypNm);
        try {
            int count = dictMgrDao.checkDictTypId(params);
            if (count > 0) {
                return ("字典类型代码已经存在！");
            }
            dictMgrDao.addDictTyp(params);
        } catch (Exception e){
            return ("新增字典类型代码异常！");
        }
        return "";
	}

	@Override
	public String updDictTyp(Map<String, Object> params) {
		String dictTypId = (String) params.get("dictTypId");
		String dictTypNm = (String) params.get("dictTypNm");
		params.put("dictTypId", dictTypId);
		params.put("dictTypNm", dictTypNm);
        try {
            dictMgrDao.updDictTyp(params);
        } catch (Exception e){
            return ("更新字典类型代码异常！");
        }
		return "";
	}

	@Override
	public String delDictTyp(Map<String, Object> params) {
		String dictTypId = (String) params.get("dictTypId");
		params.put("dictTypId", dictTypId);
        try {
            dictMgrDao.delDictTyp(params);
            dictMgrDao.delDictDtl(params);
        } catch (Exception e){
            return ("删除字典类型代码异常！");
        }
        return "";
	}

	@Override
	public List<DictMgrEntity> queryDictDtlInfo(Map<String, Object> params) {
		String dictTypId = (String) params.get("dictTypId");
		String dictDtlId = (String) params.get("dictDtlId");
		params.put("dictTypId", dictTypId);
		params.put("dictDtlId", dictDtlId);
		List<DictMgrEntity> dictDtlList = new ArrayList<>();
		dictDtlList = dictMgrDao.queryDictDtlInfo(params);
		return dictDtlList;
	}

	@Override
	public String addDictDtl(Map<String, Object> params) {
		String dictTypId = (String) params.get("dictTypId");
		String dictDtlId = (String) params.get("dictDtlId");
		String dictDtlNm = (String) params.get("dictDtlNm");
		String privilege = (String) params.get("privilege");
		String status = (String) params.get("status");
		String indx = (String) params.get("indx");
		params.put("dictTypId", dictTypId);
		params.put("dictDtlId", dictDtlId);
        params.put("dictDtlNm", dictDtlNm);
		params.put("privilege", privilege);
        params.put("status", status);
        params.put("indx", indx);
        try {
            int count = dictMgrDao.checkDictDtlId(params);
            if (count > 0) {
                return ("字典明细代码已经存在！");
            }
            dictMgrDao.addDictDtl(params);
        } catch (Exception e){
            return ("新增字典明细代码异常！");
        }
		return "";
	}

	@Override
	public String updDictDtl(Map<String, Object> params) {
		String dictTypId = (String) params.get("dictTypId");
		String dictDtlIdOld = (String) params.get("dictDtlIdOld");
		String dictDtlId = (String) params.get("dictDtlId");
		String dictDtlNm = (String) params.get("dictDtlNm");
		String privilege = (String) params.get("privilege");
		String status = (String) params.get("status");
		String indx = (String) params.get("indx");
		params.put("dictTypId", dictTypId);
		params.put("dictDtlIdOld", dictDtlIdOld);
		params.put("dictDtlId", dictDtlId);
		params.put("dictDtlNm", dictDtlNm);
		params.put("privilege", privilege);
		params.put("status", status);
		params.put("indx", indx);
        try {
            dictMgrDao.updDictDtl(params);
        } catch (Exception e){
            return ("更新字典明细代码异常！");
        }
        return "";
	}

	@Override
	public String delDictDtl(Map<String, Object> params) {
		String dictTypId = (String) params.get("dictTypId");
		String dictDtlId = (String) params.get("dictDtlId");
		params.put("dictTypId", dictTypId);
		params.put("dictDtlId", dictDtlId);
        try {
            dictMgrDao.delDictDtl(params);
        } catch (Exception e){
            return ("删除字典明细代码异常！");
        }
        return "";
	}
}
