/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.sys.entity.SysRoleEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 角色管理
 *
 * @author shuaigao@cupdata.com
 */
@Mapper
public interface SysRoleDao extends BaseMapper<SysRoleEntity> {
	
	/**
	 * 查询用户创建的角色ID列表
	 */
	List<Long> queryRoleIdList(Long createUserId);

	/**
	 * 查询角色名是否已经存在
	 */
	SysRoleEntity queryRoleByNm(Map reqPara);

    /**
     * 查询角色是否与用户进行关联
     */
    Integer  queryRoleUser(Map reqPara);

	/**
	 * 查询角色数据信息
	 */
	List<SysRoleEntity> queryRoleDataList(Map reqPara);

	/**
	 * 查询角色总数
	 */
	public int queryRoleDataListNb(Map reqPara);

    List<SysRoleEntity> queryAllRoleByUserId(Map<String, Object> params);
}
