/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.sys.form;

import lombok.Data;

/**
 * 密码表单
 *
 * @author shuaigao@cupdata.com
 */
@Data
public class PasswordForm {
    /**
     * 修改账号
     */
    private String username;
    /**
     * 原密码
     */
    private String password;
    /**
     * 新密码
     */
    private String newPassword;
    /**
     * 验证码
     */
    private String captcha;

    private String uuid;

}
