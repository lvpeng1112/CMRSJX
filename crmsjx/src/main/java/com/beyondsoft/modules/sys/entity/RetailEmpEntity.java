package com.beyondsoft.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;


@Data
@TableName("npmm_rtl_custmgr")
public class RetailEmpEntity {
        private String empId;
        private String empNm;
        private String empType;
        private String modifyTime;
        private String operId;
        private String belongOrgId;

}
