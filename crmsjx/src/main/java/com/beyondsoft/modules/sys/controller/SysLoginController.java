/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.sys.controller;

import com.beyondsoft.common.utils.HttpContextUtils;
import com.beyondsoft.common.utils.IPUtils;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.common.utils.RSATool;
import com.beyondsoft.modules.sys.dao.SysLoginDao;
import com.beyondsoft.modules.sys.entity.SysLogEntity;
import com.beyondsoft.modules.sys.entity.SysUserEntity;
import com.beyondsoft.modules.sys.form.SysLoginForm;
import com.beyondsoft.modules.sys.service.*;
import org.apache.commons.io.IOUtils;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.*;

/**
 * 登录相关
 *
 * @author shuaigao@cupdata.com
 */
@RestController
public class SysLoginController extends AbstractController {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private SysUserTokenService sysUserTokenService;
	@Autowired
	private SysCaptchaService sysCaptchaService;

	@Autowired
	private SysLogService sysLogService;

	@Autowired
	private DepartmentService departmentService;

	@Autowired(required = false)
	private SysLoginDao sysLoginDao;

	/**
	 * 验证码
	 */
	@GetMapping("captcha.jpg")
	public void captcha(HttpServletResponse response, String uuid)throws IOException {
		response.setHeader("Cache-Control", "no-store, no-cache");
		response.setContentType("image/jpeg");

		//获取图片验证码
		BufferedImage image = sysCaptchaService.getCaptcha(uuid);

		ServletOutputStream out = response.getOutputStream();
		ImageIO.write(image, "jpg", out);
		IOUtils.closeQuietly(out);
	}

	/**
	 * 登录
	 */
	@PostMapping("/sys/login")
	public Map<String, Object> login(@RequestBody SysLoginForm form)throws IOException {
		Map map = new HashMap();
		boolean captcha = sysCaptchaService.validate(form.getUuid(), form.getCaptcha());
		if(!captcha){
			return R.error("验证码不正确");
		}

		String userName = "";
		String password = "";

		try {
			//账号
			String codeUserName = RSATool.decodeByPrivateKey(form.getUsername(), "");
			userName = codeUserName.substring(2, Integer.parseInt(codeUserName.substring(0, 2)) + 2);
			//密码
			String codePassword = RSATool.decodeByPrivateKey(form.getPassword(), "");
			password = codePassword.substring(2, Integer.parseInt(codePassword.substring(0, 2)) + 2);
		}catch (Exception e){
			e.printStackTrace();
		}

		//用户信息
		SysUserEntity user = sysUserService.queryByUserName(userName);

		//账号不存在、密码错误
		if(user == null || !user.getPassword().equals(new Sha256Hash(password, user.getSalt()).toHex())){
			return R.error("账号或密码不正确");
		}
//		if(user == null ){
////				|| !user.getPassword().equals(new Sha256Hash(password, user.getSalt()).toHex())) {
//			return R.error("账号或密码不正确");
//		}

		//账号密码是初始密码
		if("000000".equals(password)){
			return R.error(-1, "请勿使用初始密码！");
		}

		//账号锁定
		if(user.getStatus() == 0){
			return R.error("账号已被锁定,请联系管理员");
		}

		//生成token，并保存到数据库
		//根据用户id查询用户机构号
		map.put("userNm",user.getUsername());
//		String orgNm = sysLoginDao.queryOrgIdByUserNm(map);
		String orgNm = user.getOrgId();
		//获取登录用户中文名
		String name = user.getName();
//		//根据用户id查询角色信息
//		String isKhye = "";
//		List<Map<String,Object>> roleNm = sysLogService.queryRoleNmByUserId(map);
//		for (int i = 0; i < roleNm.size(); i++) {
//			String strRoleNm = (String)roleNm.get(i).get("ROLE_NAME");
//			if ("总行村管部管理员".equals(strRoleNm) || "省村管部管理员".equals(strRoleNm) || "网点管理员".equals(strRoleNm)) {
//				isKhye = "0";
//				break;
//			} else {
//				isKhye = "1";
//			}
//		}
		//根据用户id查询团队长信息
		String isTdz = "";
		String userNm = user.getUsername();
		map.put("userNm",userNm);
		int countTdz = sysLogService.queryLeadIdByUserId(map);
		if (countTdz > 0) {
			isTdz = "0";
		} else {
			isTdz = "1";
		}
//		String isKhye = sysLoginDao.queryIsKhyeByUserNm(map);
		String isKhye = user.getIsKhye();
//		String orgNm = "00001";
		//获取登录用户机构树
		List<Map<String,Object>> page = getLoginOrgTree(orgNm);
		R r = sysUserTokenService.createToken(user.getUserId());
		r.put("bankNum",user.getBankNum());
		r.put("orgNm",orgNm).put("isKhy",isKhye).put("isTdz",isTdz).put("orgTree",page).put("userNm",userNm).put("name",name);
		//记录登录日志
		SysLogEntity sysLog = new SysLogEntity();
		//获取request
		HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
		//设置IP地址
		sysLog.setIp(IPUtils.getIpAddr(request));
		sysLog.setUsername(userName);
		sysLog.setOperation("用户登录");
		sysLog.setMethod("login");
		sysLog.setParams("login");
		insertLoginLog(sysLog);
		return r;
	}


	/**
	 * 退出
	 */
	@PostMapping("/sys/logout")
	public R logout() {
		sysUserTokenService.logout(getUserId());
		return R.ok();
	}

	/**
	 * 从CRM跳转或者由老绩效跳转
	 */
	@GetMapping("/sys/logintest")
	public void  logintest(String name, HttpServletResponse httpServletResponse)throws IOException {
		System.out.println(name);
		String url = "http://127.0.0.1:8866/#/initLogin?name="+name;
		httpServletResponse.sendRedirect(url);
	}

	/**
	 * 外部登入初始化用户角色,token信息
	 * 工号/机构/条线/是否考核员
	 */
	@PostMapping("/sys/crmInit")
	public Map<String, Object> crmInit(@RequestBody SysUserEntity user1) {
		//若没有插入用户id,用户名
		String name = user1.getUsername();
		String[] parms = name.split("/");
		String userName = parms[0];
		String orgNm = parms[1];
		String lineTeam =parms[2];
		String isKhy = parms[3];
		List<Long> roleIdList = new ArrayList<>();
		if("0".equals(lineTeam)){
			roleIdList.add((long)2);
		}
		else if("1".equals(lineTeam)){
			roleIdList.add((long)3);
		}
		//用户信息
		SysUserEntity user = sysUserService.queryByUserName(userName);
		if(user == null){
			//插入用户信息
			user1.setUsername(userName);
			user1.setStatus(1);
			user1.setRoleIdList(roleIdList);
			user1.setEmail("srcb@crmlogin.com");
			sysUserService.saveCrmUser(user1);
			user = sysUserService.queryByUserName(userName);
		}
		else{
			//修改用户角色
			user.setRoleIdList(roleIdList);
			sysUserService.updateCrm(user);
		}
		//生成token，并保存到数据库
		R r = sysUserTokenService.createToken(user.getUserId());
		//获取登录用户机构树
		List<Map<String,Object>> page = getLoginOrgTree(orgNm);
		r.put("orgNm",orgNm).put("isKhy",isKhy).put("orgTree",page);
		//记录登录日志
		SysLogEntity sysLog = new SysLogEntity();
		//获取request
		HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
		//设置IP地址
		sysLog.setOperation("CRM用户登录");
		sysLog.setMethod("crmLogin");
		sysLog.setParams("crmLogin");
		sysLog.setIp(IPUtils.getIpAddr(request));
		sysLog.setUsername(userName);
		insertLoginLog(sysLog);
		return r ;
	}

	/*
	插入系统日志
	 */
	public void insertLoginLog(SysLogEntity sysLog){
		sysLog.setTime((long)1);
		sysLog.setCreateDate(new Date());
		//保存系统日志
		sysLogService.save(sysLog);
	}
	/*
	获取登录用户机构树
	 */
	public List<Map<String,Object>> getLoginOrgTree(String orgId){
		Map<String, Object> params = new HashMap<>();
		params.put("orgId",orgId);
		List<Map<String,Object>> page = departmentService.findDepartmentTree(params);
		return page;
	}

}
