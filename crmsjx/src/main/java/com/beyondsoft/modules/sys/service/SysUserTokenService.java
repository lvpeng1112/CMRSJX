/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.modules.sys.entity.SysUserTokenEntity;
import com.beyondsoft.common.utils.R;

/**
 * 用户Token
 *
 * @author shuaigao@cupdata.com
 */
public interface SysUserTokenService extends IService<SysUserTokenEntity> {

	/**
	 * 生成token
	 * @param userId  用户ID
	 */
	R createToken(long userId);

	/**
	 * 退出，修改token值
	 * @param userId  用户ID
	 */
	void logout(long userId);

}
