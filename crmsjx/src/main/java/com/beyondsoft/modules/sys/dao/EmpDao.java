package com.beyondsoft.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.sys.entity.EmpEntity;
import org.apache.ibatis.annotations.Mapper;


import java.util.List;

@Mapper
public interface EmpDao extends BaseMapper<EmpEntity> {

}
