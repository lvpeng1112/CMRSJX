/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.sys.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.modules.sys.entity.SysLogEntity;
import com.beyondsoft.common.utils.PageUtils;

import java.util.List;
import java.util.Map;


/**
 * 系统日志
 *
 * @author shuaigao@cupdata.com
 */
public interface SysLogService extends IService<SysLogEntity> {

    PageUtils queryPage(Map<String, Object> params);

    //根据用户id查询角色信息
    List<Map<String,Object>> queryRoleNmByUserId(Map map);

    //根据用户id查询团队长信息
    int queryLeadIdByUserId(Map map);
}
