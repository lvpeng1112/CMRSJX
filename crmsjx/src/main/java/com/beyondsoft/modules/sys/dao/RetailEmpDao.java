package com.beyondsoft.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.sys.entity.RetailEmpEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RetailEmpDao extends BaseMapper<RetailEmpEntity> {

}
