package com.beyondsoft.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("IND_SEARCH")
public class IndexSearchEntity {
    private String keyId;
    private String favorNm;
    private String indId;
    private String indNm;
    private String mngId;
    private String click;
    private String dataDt;
}
