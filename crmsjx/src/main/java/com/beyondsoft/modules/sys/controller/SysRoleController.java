/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.sys.controller;

import com.beyondsoft.common.annotation.SysLog;
import com.beyondsoft.common.utils.Constant;
import com.beyondsoft.common.utils.PageUtils;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.common.validator.ValidatorUtils;
import com.beyondsoft.modules.sys.dao.SysRoleDao;
import com.beyondsoft.modules.sys.dao.SysRoleMenuDao;
import com.beyondsoft.modules.sys.entity.SysRoleEntity;
import com.beyondsoft.modules.sys.service.SysRoleMenuService;
import com.beyondsoft.modules.sys.service.SysRoleService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 角色管理
 *
 * @author shuaigao@cupdata.com
 */
@RestController
@RequestMapping("/sys/role")
public class SysRoleController extends AbstractController {
	@Autowired
	private SysRoleService sysRoleService;
	@Autowired
	private SysRoleMenuService sysRoleMenuService;

	@Autowired(required = false)
	SysRoleDao sysRoleDao;
	@Autowired(required = false)
	SysRoleMenuDao sysRoleMenuDao;
	/**
	 * 角色列表
	 */
	@GetMapping("/list")
	@RequiresPermissions("sys:role:list")
	public List<SysRoleEntity> list(@RequestParam Map<String, Object> params){
		//如果不是超级管理员，则只查询自己角色以及子角色
		if(getUserId() != Constant.SUPER_ADMIN){
			params.put("userId", getUserId());
		}
		else{
			params.put("userId", null);
		}
		//查询所有的角色集合
		List<SysRoleEntity> queryRoleDataList = sysRoleDao.queryAllRoleByUserId(params);
		return queryRoleDataList;
	}
	
	/**
	 * 角色列表
	 */
	@GetMapping("/select/{bankNum}")
	@RequiresPermissions("sys:role:select")
	public R select(@PathVariable("bankNum") String bankNum){
		Map<String, Object> map = new HashMap<>();
		if(!"0000".equals(bankNum)){
			map.put("bank_num", bankNum);
		}

		//如果不是超级管理员，则只查询自己所拥有的角色列表
/*		if(getUserId() != Constant.SUPER_ADMIN){
			map.put("create_user_id", getUserId());
		}*/
		List<SysRoleEntity> list = (List<SysRoleEntity>) sysRoleService.listByMap(map);
		
		return R.ok().put("list", list);
	}
	
	/**
	 * 角色信息
	 */
	@GetMapping("/info/{roleId}")
	@RequiresPermissions("sys:role:info")
	public R info(@PathVariable("roleId") Long roleId){
		SysRoleEntity role = sysRoleService.getById(roleId);
		
		//查询角色对应的菜单
//		List<Long> menuIdList = sysRoleMenuService.queryMenuIdList(roleId);
		List<Long> menuIdList = sysRoleMenuDao.queryListByRoleID(roleId);
		role.setMenuIdList(menuIdList);
		return R.ok().put("role", role);
	}
	
	/**
	 * 保存角色
	 */
	@SysLog("保存角色")
	@PostMapping("/save")
	@RequiresPermissions("sys:role:save")
	public R save(@RequestBody SysRoleEntity role){
		Map map = new HashMap();
		ValidatorUtils.validateEntity(role);

		//新增角色信息的时候，进行角色名是否存在校验
		map.put("roleName",role.getRoleName());
        SysRoleEntity sysRoleEntity = sysRoleDao.queryRoleByNm(map);
        if(sysRoleEntity != null){
            return R.error(999999, "角色名已存在，请重新输入！");
        }else{
            role.setCreateUserId(getUserId());
            sysRoleService.saveRole(role);

            return R.ok();
        }
	}
	
	/**
	 * 修改角色
	 */
	@SysLog("修改角色")
	@PostMapping("/update")
	@RequiresPermissions("sys:role:update")
	public R update(@RequestBody SysRoleEntity role){
        Map map = new HashMap();
		ValidatorUtils.validateEntity(role);
        map.put("roleId",role.getRoleId());
        SysRoleEntity sysRoleEntity = sysRoleDao.queryRoleByNm(map);
        //修改前角色名称
        String roleNameBefor = sysRoleEntity.getRoleName();
        //修改后角色名称
        String roleNameAfter = role.getRoleName();
        if(!roleNameAfter.equals(roleNameBefor)){//如果修改前后角色名称不一致，则进行角色名校验
            map.clear();
            map.put("roleName",roleNameAfter);
            SysRoleEntity count = sysRoleDao.queryRoleByNm(map);
            if(count != null){
                return R.error(999999, "角色名已存在，请重新输入！");
            }else{
                role.setCreateUserId(getUserId());
                sysRoleService.update(role);

                return R.ok();
            }
        }else{
            role.setCreateUserId(getUserId());
            sysRoleService.update(role);

            return R.ok();
        }

	}
	
	/**
	 * 删除角色
	 */
	@SysLog("删除角色")
	@PostMapping("/delete")
	@RequiresPermissions("sys:role:delete")
	public R delete(@RequestBody Long[] roleIds){
	    boolean flag = true;
	    Map map = new HashMap();
        for (int i = 0; i <roleIds.length ; i++) {
            //删除之前进行角色是否与用户关联校验
            map.put("roleId",roleIds[i]);
            Integer count = sysRoleDao.queryRoleUser(map);
            if(count > 0){
                flag = false;
            }
        }

        if(flag == false){
            return R.error(999999, "该角色已分配用户，不可删除！");
        }else{
            sysRoleService.deleteBatch(roleIds);

            return R.ok();
        }
	}
}
