package com.beyondsoft.modules.deduction.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@ApiModel(value = "内控年度扣分导入表")
@TableName("C_MONTHLY_POINT_DEDUCTION")
@Data
public class MonthlyPointDeduction implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "年度")
    private String yr;

    @ApiModelProperty(value = "机构编号")
    private String branchId;

    @ApiModelProperty(value = "机构名称")
    private String branchNm;

    @ApiModelProperty(value = "客户经理编号")
    private String custId;

    @ApiModelProperty(value = "客户经理名称")
    private String custNm;

    @ApiModelProperty(value = "内控扣分")
    @JsonSerialize(using = ToStringSerializer.class)
    private Double pointDeduction;

    @ApiModelProperty(value = "操作员")
    private String operators;

    @ApiModelProperty(value = "操作时间")
    private String operateDate;

    @ApiModelProperty(value = "扣分原因")
    private String remark;
}
