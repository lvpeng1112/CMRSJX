package com.beyondsoft.modules.deduction.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.deduction.entity.YearPointDeduction;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository("YearPointDeductionDao")
public interface YearPointDeductionDao extends BaseMapper<YearPointDeduction> {

    List<YearPointDeduction> queryDeductionListInfo(Map<String,Object> reqPara);
    List<YearPointDeduction> queryYearHis(YearPointDeduction yearPointDeduction);
    int queryDeductionListNb(Map reqPara);

    int checkOrgId(Map<String, Object> params);

    String queryOrgNm(Map<String, Object> params);

    void deleteDeductionList(YearPointDeduction yearPointDeduction);

    void insertDeductionList(List<YearPointDeduction> list);

    void insertSingleData(YearPointDeduction YearPointDeduction);

    void updatedata(YearPointDeduction YearPointDeduction);
    void deletedata(YearPointDeduction YearPointDeduction);
}
