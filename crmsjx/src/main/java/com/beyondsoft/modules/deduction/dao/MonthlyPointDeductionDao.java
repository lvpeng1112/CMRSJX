package com.beyondsoft.modules.deduction.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.deduction.entity.MonthlyPointDeduction;
import com.beyondsoft.modules.deduction.entity.YearPointDeduction;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository("MonthlyPointDeductionDao")
public interface MonthlyPointDeductionDao extends BaseMapper<MonthlyPointDeduction> {

    List<MonthlyPointDeduction> queryDeductionListInfo(Map<String,Object> reqPara);
    List<MonthlyPointDeduction> queryMonthlyHis(MonthlyPointDeduction monthlyPointDeduction);
    int queryDeductionListNb(Map reqPara);

    int checkOrgId(Map<String, Object> params);

    String queryOrgNm(Map<String, Object> params);

    void deleteDeductionList(MonthlyPointDeduction monthlyPointDeduction);

    void insertDeductionList(List<MonthlyPointDeduction> list);

    void insertSingleData(MonthlyPointDeduction monthlyPointDeduction);

    void updatedata(MonthlyPointDeduction monthlyPointDeduction);
}
