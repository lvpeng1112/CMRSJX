package com.beyondsoft.modules.deduction.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.datasource.annotation.DataSource;
import com.beyondsoft.modules.deduction.dao.PointDeductionHisDao;

import com.beyondsoft.modules.deduction.entity.PointDeductionHis;

import com.beyondsoft.modules.deduction.service.PointDeductionHisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class PointDeductionHisServiceImpl extends ServiceImpl<PointDeductionHisDao, PointDeductionHis> implements PointDeductionHisService {
    @Autowired
    PointDeductionHisDao pointDeductionHisDao;
    @Override
    @DataSource(value = "second")
    public List<PointDeductionHis> querytHisData(Map<String, Object> params) {
        List<PointDeductionHis> list = pointDeductionHisDao.querytHisData(params);
        return list;
    }

    @Override
    @DataSource(value = "second")
    public int querytHisDataNb(Map<String, Object> params) {
        int i = pointDeductionHisDao.querytHisDataNb(params);
        return i;
    }
}
