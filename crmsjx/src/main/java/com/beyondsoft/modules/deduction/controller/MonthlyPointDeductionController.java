package com.beyondsoft.modules.deduction.controller;

import com.alibaba.druid.util.StringUtils;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.deduction.entity.MonthlyPointDeduction;
import com.beyondsoft.modules.deduction.entity.YearPointDeduction;
import com.beyondsoft.modules.deduction.service.MonthlyPointDeductionService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/deduction/monthlyPointDeduction")
public class MonthlyPointDeductionController {
    protected Logger logger = LoggerFactory.getLogger(getClass());
    @Resource
    MonthlyPointDeductionService monthlyPointDeductionService;

    @RequestMapping("/list")
    @ApiOperation("分页查询")
    public R list(@RequestParam Map<String, Object> params){
        Map map = new HashMap();
        if(null == params){
            return R.error("入参为空");
        }
        if(StringUtils.isEmpty((String) params.get("page")) && StringUtils.isEmpty((String) params.get("limit"))){
            return R.error("缺少必传分页参数");
        }
        List<MonthlyPointDeduction>  deductionList =monthlyPointDeductionService.queryDeductionListInfo(params);
        int i = monthlyPointDeductionService.queryDeductionListNb(params);
        return R.ok().put("deductionList",deductionList).put("count",i);
    }

    @RequestMapping("/importExcel")
    @ApiOperation("导入excel")
    public R importExcel(@RequestParam(value = "file") MultipartFile file, String org, String isKhy) throws Exception {
        String fileName = file.getOriginalFilename();
        logger.info("导入表格文件为:{}",fileName);
        return monthlyPointDeductionService.batchImport(fileName, file, org, isKhy);
    }

    @PostMapping("/insertSingleData")
    @ApiOperation("新增单条数据")
    public R insertSingleData(@RequestBody MonthlyPointDeduction monthlyPointDeduction){
        monthlyPointDeductionService.insertSingleData(monthlyPointDeduction);
        return R.ok();
    }

    @PostMapping("/updatedata")
    @ApiOperation("修改单条数据")
    public R updatedata(@RequestBody MonthlyPointDeduction monthlyPointDeduction){
        monthlyPointDeductionService.updatedata(monthlyPointDeduction);
        return R.ok();
    }

    @PostMapping("/deletedata")
    @ApiOperation("删除单条数据")
    public R deletedata(@RequestBody MonthlyPointDeduction monthlyPointDeduction){
        monthlyPointDeductionService.deletedata(monthlyPointDeduction);
        return R.ok();
    }
}
