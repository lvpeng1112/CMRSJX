package com.beyondsoft.modules.deduction.controller;

import com.alibaba.druid.util.StringUtils;
import com.beyondsoft.common.utils.ShiroUtils;
import com.beyondsoft.datasource.annotation.DataSource;
import com.beyondsoft.modules.aofp.entity.CMngCheckBaseInfo;
import com.beyondsoft.modules.deduction.dao.PointDeductionHisDao;
import com.beyondsoft.modules.deduction.dao.YearPointDeductionDao;
import com.beyondsoft.modules.deduction.entity.PointDeductionHis;
import com.beyondsoft.modules.deduction.entity.YearPointDeduction;
import com.beyondsoft.modules.deduction.service.PointDeductionHisService;
import com.beyondsoft.modules.deduction.service.YearPointDeductionService;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.sys.entity.SysUserEntity;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/deduction/yearPointDeduction")
public class YearPointDeductionController {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    YearPointDeductionService yearPointDeductionService;
    @Resource
    PointDeductionHisService pointDeductionHisService;

    @RequestMapping("/list")
    @ApiOperation("分页查询")
    public R list(@RequestParam Map<String, Object> params){
        Map map = new HashMap();
        if(null == params){
            return R.error("入参为空");
        }
        if(StringUtils.isEmpty((String) params.get("page")) && StringUtils.isEmpty((String) params.get("limit"))){
            return R.error("缺少必传分页参数");
        }
        List<YearPointDeduction> deductionList = yearPointDeductionService.queryDeductionListInfo(params);
        int i = yearPointDeductionService.queryDeductionListNb(params);
        return R.ok().put("deductionList",deductionList).put("count",i);
    }
    @RequestMapping("/importExcel")
    @ApiOperation("导入excel")
    public R importExcel(@RequestParam(value = "file") MultipartFile file, String org, String isKhy) throws Exception {
        String fileName = file.getOriginalFilename();
        logger.info("导入表格文件为:{}",fileName);
        return yearPointDeductionService.batchImport(fileName, file, org, isKhy);
    }

    @PostMapping("/insertSingleData")
    @ApiOperation("新增单条数据")
    public R insertSingleData(@RequestBody YearPointDeduction YearPointDeduction){
        yearPointDeductionService.insertSingleData(YearPointDeduction);
        return R.ok();
    }

    @PostMapping("/updatedata")
    @ApiOperation("修改单条数据")
    public R updatedata(@RequestBody YearPointDeduction YearPointDeduction){
        yearPointDeductionService.updatedata(YearPointDeduction);
        return R.ok();
    }

    @PostMapping("/deletedata")
    @ApiOperation("删除单条数据")
    public R deletedata(@RequestBody YearPointDeduction YearPointDeduction){
        yearPointDeductionService.deletedata(YearPointDeduction);
        return R.ok();
    }
    @ApiOperation("日志分页查询")
    @RequestMapping("/querytHisData")
    public R querytHisData(@RequestParam Map<String, Object> params){
        int pageNo = Integer.parseInt((String) params.get("page"));
        int pageCount = Integer.parseInt((String)params.get("limit"));
        int offset = ((pageNo - 1) * pageCount);
        params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
        params.put("offset", offset);
        List<PointDeductionHis> pointDeductionHisList = pointDeductionHisService.querytHisData(params);
        System.out.println(pointDeductionHisList);
        int i = pointDeductionHisService.querytHisDataNb(params);
        System.out.println(i);
        return R.ok().put("pointDeductionHisList",pointDeductionHisList).put("count",i);
    }
}
