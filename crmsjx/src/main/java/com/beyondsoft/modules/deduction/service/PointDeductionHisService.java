package com.beyondsoft.modules.deduction.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.modules.deduction.entity.PointDeductionHis;

import java.util.List;
import java.util.Map;


public interface PointDeductionHisService extends IService<PointDeductionHis> {
    List<PointDeductionHis> querytHisData(Map<String, Object> params);

    int querytHisDataNb(Map<String, Object> params);
}
