package com.beyondsoft.modules.deduction.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.deduction.entity.MonthlyPointDeduction;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface MonthlyPointDeductionService extends IService<MonthlyPointDeduction> {
    List<MonthlyPointDeduction> queryDeductionListInfo(Map<String, Object> params);

    int queryDeductionListNb(Map<String, Object> params);

    R batchImport(String fileName, MultipartFile file, String org, String isKhy) throws Exception;

    void insertSingleData(MonthlyPointDeduction monthlyPointDeduction);

    void updatedata(MonthlyPointDeduction MonthlyPointDeduction);
    void deletedata(MonthlyPointDeduction MonthlyPointDeduction);
}

