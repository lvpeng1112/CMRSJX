package com.beyondsoft.modules.deduction.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.deduction.entity.YearPointDeduction;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface YearPointDeductionService extends IService<YearPointDeduction> {
    List<YearPointDeduction> queryDeductionListInfo(Map<String, Object> params);

    int queryDeductionListNb(Map<String, Object> params);

    R batchImport(String fileName, MultipartFile file, String org, String isKhy) throws Exception;

    void insertSingleData(YearPointDeduction yearPointDeduction);

    void updatedata(YearPointDeduction yearPointDeduction);
    void deletedata(YearPointDeduction yearPointDeduction);
}
