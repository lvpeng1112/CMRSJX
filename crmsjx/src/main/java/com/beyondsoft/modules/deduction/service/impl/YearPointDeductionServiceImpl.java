package com.beyondsoft.modules.deduction.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.common.utils.ShiroUtils;
import com.beyondsoft.datasource.annotation.DataSource;
import com.beyondsoft.modules.deduction.dao.PointDeductionHisDao;
import com.beyondsoft.modules.deduction.dao.YearPointDeductionDao;
import com.beyondsoft.modules.deduction.entity.PointDeductionHis;
import com.beyondsoft.modules.deduction.entity.YearPointDeduction;
import com.beyondsoft.modules.deduction.service.YearPointDeductionService;
import com.beyondsoft.modules.grade.entity.YearCheckGrade;
import com.beyondsoft.modules.sys.entity.SysUserEntity;
import com.beyondsoft.utils.DateUtil;
import com.beyondsoft.utils.StrUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class YearPointDeductionServiceImpl extends ServiceImpl<YearPointDeductionDao, YearPointDeduction> implements YearPointDeductionService {
    protected Logger logger = LoggerFactory.getLogger(getClass());
    @Resource
    YearPointDeductionDao yearPointDeductionDao;
    @Resource
    PointDeductionHisDao pointDeductionHisDao;
    @Override
    @DataSource(value = "second")
    public List<YearPointDeduction> queryDeductionListInfo(Map<String, Object> params) {
        int pageNo = Integer.parseInt((String) params.get("page"));
        int pageCount = Integer.parseInt((String)params.get("limit"));
        int offset = ((pageNo - 1) * pageCount);
        params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
        params.put("offset", offset);
        return yearPointDeductionDao.queryDeductionListInfo(params);
    }

    @Override
    @DataSource(value = "second")
    public int queryDeductionListNb(Map<String, Object> params) {
        int nb = yearPointDeductionDao.queryDeductionListNb(params);
        return nb;
    }

    @Override
    @DataSource(value = "second")
    public R batchImport(String fileName, MultipartFile file, String org, String isKhy) {
        Map map = new HashMap();
        Workbook workbook = null;
        if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
            return R.error("上传格式不正确");
        }
        String extString = fileName.substring(fileName.lastIndexOf("."));
        List<YearPointDeduction> deductionList = new ArrayList<>();
        List<PointDeductionHis> pointDeductionHisList = new ArrayList<>();
        try {
            InputStream inputStream = file.getInputStream();
            if (".xls".equals(extString)){
                HSSFWorkbook wb = new HSSFWorkbook(inputStream);
                workbook = wb;
            }else if (".xlsx".equals(extString)){
                XSSFWorkbook wb = new XSSFWorkbook(inputStream);
                workbook = wb;
            }
            int sheets = workbook.getNumberOfSheets();
            for (int i = 0; i < sheets; i++){
                Sheet sheetAt = workbook.getSheetAt(i);
                //获取多少行
                int lastRowNum = sheetAt.getLastRowNum();
                for (int j = 1; j <= lastRowNum; j++) {
                    Row row = sheetAt.getRow(j);
                    if (row != null){
                        YearPointDeduction pointDeduction = new YearPointDeduction();
                        PointDeductionHis pointDeductionHis = new PointDeductionHis();
                        for (Cell cell : row) {
                            cell.setCellType(Cell.CELL_TYPE_STRING);
                        }
                        if (row.getCell(0) == null || row.getCell(0).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，年度列没有数据");
                        }else if (row.getCell(1) == null || row.getCell(1).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，机构编号列没有数据");
                        }else if (row.getCell(3) == null || row.getCell(3).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，客户经理编号列没有数据");
                        }else if (row.getCell(4) == null || row.getCell(4).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，客户经理名称列没有数据");
                        }else if (row.getCell(5) == null || row.getCell(5).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，内控扣分列没有数据");
                        }else if (row.getCell(6) == null || row.getCell(5).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，扣分原因列没有数据");
                        }
                        // 月度日期格式check
                        String yr = row.getCell(0).getStringCellValue();
                        String inputStr = yr + "0101";
                        if (!DateUtil.isDate(inputStr)) {
                            return R.error("第" + (j + 1) + "行,年度【" + yr + "】不是正确的日期格式！例:YYYY");
                        }
                        // 机构编号存在check
                        String orgId = row.getCell(1).getStringCellValue();
                        Map<String, Object> orgParams = new HashMap<>();
                        orgParams.put("orgId", orgId);
                        orgParams.put("org", org);
                        orgParams.put("isKhy", isKhy);
                        int count = yearPointDeductionDao.checkOrgId(orgParams);
                        if (count == 0) {
                            return R.error("第" + (j + 1) + "行,您没有权限导入其他机构的数据，机构编号【" + orgId + "】！");
                        }
                        String orgNm = yearPointDeductionDao.queryOrgNm(orgParams);
                        // 客户经理编号长度check
                        String custId = row.getCell(3).getStringCellValue();
                        if (custId.length() > 90) {
                            return R.error("第" + (j + 1) + "行,客户经理编号【" + custId + "】长度不能超过90位！");
                        }
                        // 客户经理名称长度check
                        String custNm = row.getCell(4).getStringCellValue();
                        if (StrUtil.chineseLen(custNm) > 300) {
                            return R.error("第" + (j + 1) + "行,客户名称【" + custNm + "】长度不能超过300位！");
                        }
                        // 内控扣分长度check
                        String deduction = row.getCell(5).getStringCellValue();
                        if (!StrUtil.isNumeric(deduction)) {
                            return R.error("第" + (j + 1) + "行,内控扣分【" + deduction + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(deduction, 3 , 2)) {
                            return R.error("第" + (j + 1) + "行,内控扣分【" + deduction + "】不能超过3位整数2位小数！");
                        }
                        double a = Double.parseDouble(deduction);
                        if(a>=100.00){
                            return R.error("第" + (j + 1) + "行,内控扣分【" + deduction + "】不能大于100！");
                        }else if (a<=0){
                            return R.error("第" + (j + 1) + "行,内控扣分【" + deduction + "】不能为负数！");
                        }

                        String remark = row.getCell(6).getStringCellValue();
                        //执行插入方法
                        pointDeduction.setYr(yr);
                        pointDeduction.setBranchId(orgId);
                        pointDeduction.setBranchNm(orgNm);
                        pointDeduction.setCustId(custId);
                        pointDeduction.setCustNm(custNm);
                        Double deductionDouble = Double.valueOf(deduction);
                        pointDeduction.setPointDeduction(deductionDouble);
                        pointDeduction.setOperators(ShiroUtils.getUserEntity().getName());
                        pointDeduction.setRemark(remark);
                        deductionList.add(pointDeduction);
                        yearPointDeductionDao.deleteDeductionList(pointDeduction);
                        pointDeductionHis.setYr(yr);
                        pointDeductionHis.setBranchId(orgId);
                        pointDeductionHis.setBranchNm(orgNm);
                        pointDeductionHis.setCustId(custId);
                        pointDeductionHis.setCustNm(custNm);
                        pointDeductionHis.setPointDeductionAfter(deductionDouble);
                        pointDeductionHis.setRemarkAfter(deduction);
                        pointDeductionHis.setOperators(ShiroUtils.getUserEntity().getName());
                        pointDeductionHis.setLogo("批量插入");
                        pointDeductionHisList.add(pointDeductionHis);
                    }
                }
            }
            yearPointDeductionDao.insertDeductionList(deductionList);
            pointDeductionHisDao.insertHisList(pointDeductionHisList);
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            try {
                workbook.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        }
        return R.ok("导入成功");
    }

    @Override
    @DataSource(value = "second")
    public void insertSingleData(YearPointDeduction yearPointDeduction) {
        String logo = "新增";
        Map map = new HashMap();
        String orgid = yearPointDeduction.getBranchId();
        map.put("orgId",orgid);
        String orgNm = yearPointDeductionDao.queryOrgNm(map);
        SysUserEntity userEntity = ShiroUtils.getUserEntity();
        yearPointDeduction.setOperators(userEntity.getName());
        yearPointDeduction.setBranchNm(orgNm);
        yearPointDeductionDao.deleteDeductionList(yearPointDeduction);
        yearPointDeductionDao.insertSingleData(yearPointDeduction);
        //新增将数据进行备份
        List<YearPointDeduction> yearPointDeductionsBefo = new ArrayList<>();
        List<YearPointDeduction> yearPointDeductionsAfter = new ArrayList<>();
        yearPointDeductionsAfter.add(yearPointDeduction);
        record(yearPointDeductionsBefo,yearPointDeductionsAfter,logo);

    }

    @Override
    @DataSource(value = "second")
    public void updatedata(YearPointDeduction yearPointDeduction) {
        String logo = "修改";
        SysUserEntity userEntity = ShiroUtils.getUserEntity();
        yearPointDeduction.setOperators(userEntity.getName());
        //调整前数据
        List<YearPointDeduction> yearPointDeductionsBefo = yearPointDeductionDao.queryYearHis(yearPointDeduction);
        //执行修改操作
        yearPointDeductionDao.updatedata(yearPointDeduction);
        //调整后数据
        List<YearPointDeduction> yearPointDeductionsAfter = yearPointDeductionDao.queryYearHis(yearPointDeduction);
        //将数据进行保存
        record(yearPointDeductionsBefo,yearPointDeductionsAfter,logo);
    }

    @Override
    @DataSource(value = "second")
    public void deletedata(YearPointDeduction yearPointDeduction) {
        String logo = "删除";
        List<YearPointDeduction> yearPointDeductionsBefo = yearPointDeductionDao.queryYearHis(yearPointDeduction);
        List<YearPointDeduction> yearPointDeductionsAfter = new ArrayList<>();
        record(yearPointDeductionsBefo,yearPointDeductionsAfter,logo);
        yearPointDeductionDao.deletedata(yearPointDeduction);
    }

    @DataSource(value = "second")
    public void record(List<YearPointDeduction> yearPointDeductionsBefo, List<YearPointDeduction> yearPointDeductionsAfter, String logo){
        PointDeductionHis pointDeductionHis = new PointDeductionHis();
        if (yearPointDeductionsBefo.size()>0){
            pointDeductionHis.setYr(yearPointDeductionsBefo.get(0).getYr());
            pointDeductionHis.setBranchId(yearPointDeductionsBefo.get(0).getBranchId());
            pointDeductionHis.setBranchNm(yearPointDeductionsBefo.get(0).getBranchNm());
            pointDeductionHis.setCustId(yearPointDeductionsBefo.get(0).getCustId());
            pointDeductionHis.setCustNm(yearPointDeductionsBefo.get(0).getCustNm());
            pointDeductionHis.setPointDeductionBefo(yearPointDeductionsBefo.get(0).getPointDeduction());
            pointDeductionHis.setRemarkBefo(yearPointDeductionsBefo.get(0).getRemark());
        }
        if (yearPointDeductionsAfter.size()>0){
            pointDeductionHis.setYr(yearPointDeductionsAfter.get(0).getYr());
            pointDeductionHis.setBranchId(yearPointDeductionsAfter.get(0).getBranchId());
            pointDeductionHis.setBranchNm(yearPointDeductionsAfter.get(0).getBranchNm());
            pointDeductionHis.setCustId(yearPointDeductionsAfter.get(0).getCustId());
            pointDeductionHis.setCustNm(yearPointDeductionsAfter.get(0).getCustNm());
            pointDeductionHis.setPointDeductionAfter(yearPointDeductionsAfter.get(0).getPointDeduction());
            pointDeductionHis.setRemarkAfter(yearPointDeductionsAfter.get(0).getRemark());
        }
        pointDeductionHis.setOperators(ShiroUtils.getUserEntity().getName());
        pointDeductionHis.setLogo(logo);
        pointDeductionHisDao.insertHis(pointDeductionHis);
    }
}
