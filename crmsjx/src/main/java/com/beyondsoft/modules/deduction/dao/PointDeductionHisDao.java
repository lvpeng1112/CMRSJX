package com.beyondsoft.modules.deduction.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.deduction.entity.PointDeductionHis;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository("PointDeductionHisDao")
public interface PointDeductionHisDao extends BaseMapper<PointDeductionHis> {

    void insertHis(PointDeductionHis pointDeductionHis);
    void insertHisList(List<PointDeductionHis> list);
    List<PointDeductionHis> querytHisData(Map<String, Object> params);
    List<Map<String, Object>> queryDictDetail(Map<String, Object> params);
    int querytHisDataNb(Map<String, Object> params);
}
