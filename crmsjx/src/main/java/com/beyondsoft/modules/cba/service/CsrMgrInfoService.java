package com.beyondsoft.modules.cba.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.modules.cba.entity.CsrMgrInfoEntity;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;
import java.util.Map;
/**
 * 客户经理名单导入及查询
 *
 * @author yuchong
 */
public interface CsrMgrInfoService extends IService<CsrMgrInfoEntity> {
	// 查询客户经理名单记录数
	int queryCsrMgrInfoNb(Map<String, Object> params);
	// 查询客户经理名单信息
	List<CsrMgrInfoEntity> queryCsrMgrInfo(Map<String, Object> params);


	String batchImport(String fileName, MultipartFile file);

}
