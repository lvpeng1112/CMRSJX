package com.beyondsoft.modules.cba.service.impl;

import java.io.InputStream;
import java.util.*;
import java.util.Map;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.utils.DateUtil;
import com.beyondsoft.utils.StrUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.beyondsoft.modules.cba.dao.SubBranOpinDao;
import com.beyondsoft.modules.cba.entity.SubBranOpinEntity;
import com.beyondsoft.modules.cba.service.SubBranOpinService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;


@Service("subBranOpinService")
public class SubBranOpinServiceImpl extends ServiceImpl<SubBranOpinDao, SubBranOpinEntity> implements SubBranOpinService {
	@Autowired(required = false)
	private SubBranOpinDao subBranOpinDao;

	@Override
	public int querySubBranOpinNb(Map<String, Object> params) {
		String dtDate = (String) params.get("dtDate");
		String apprStt = (String) params.get("apprStt");
		params.put("dtDate", dtDate);
		params.put("apprStt", apprStt);
		int count = subBranOpinDao.querySubBranOpinNb(params);
		return count;
	}

	@Override
	public List<SubBranOpinEntity> querySubBranOpin(Map<String, Object> params) {
		String dtDate = (String) params.get("dtDate");
		String apprStt = (String) params.get("apprStt");
		int pageNo = Integer.parseInt((String) params.get("page"));
		int pageCount = Integer.parseInt((String)params.get("limit"));
		int offset = ((pageNo - 1) * pageCount);
		params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
		params.put("offset", offset);
		params.put("dtDate", dtDate);
		params.put("apprStt", apprStt);
		List<SubBranOpinEntity> subBranOpinList = new ArrayList<>();
		subBranOpinList = subBranOpinDao.querySubBranOpin(params);
		return subBranOpinList;
	}

	@Override
	public String batchImport(String fileName, MultipartFile file) {

		if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
			return ("上传文件格式不正确");
		}

		// 调整上限值取得
		Map<String, Object> paramKey = new HashMap<>();
		paramKey.put("paramKey", "adjustUpper");
		String adjustUpper = subBranOpinDao.getAdjustLsl(paramKey);

		// 调整下限值取得
		paramKey.put("paramKey", "adjustLower");
		String adjustLower = subBranOpinDao.getAdjustLsl(paramKey);

		String extString = fileName.substring(fileName.lastIndexOf("."));
		List<SubBranOpinEntity> subBranOpinList = new ArrayList<>();
		List<SubBranOpinEntity> subBranOpinAddList = new ArrayList<>();
		try{
			InputStream is = file.getInputStream();
			Workbook workbook = null;
			if(".xls".equals(extString)){
				HSSFWorkbook wb = new HSSFWorkbook(is);
				workbook = wb;
			}else if(".xlsx".equals(extString)){
				XSSFWorkbook wb = new XSSFWorkbook(is);
				workbook = wb;
			}
			//有多少个sheet
			int sheets = workbook.getNumberOfSheets();

			for (int i = 0; i < sheets; i++) {
				Sheet sheet = workbook.getSheetAt(i);
				//获取多少行
				int rows = sheet.getPhysicalNumberOfRows();
				SubBranOpinEntity subBranOpin = null;
				//遍历每一行，注意：第 0 行为标题
				for (int j = 1; j < rows; j++) {
					subBranOpin = new SubBranOpinEntity();
					//获得第 j 行
					Row row = sheet.getRow(j);
					// 空行check
					if (row == null) {
						return ("第" + (j + 1) + "行,没有数据！");
					}
					// 必须输入项check
					if (row.getCell(0) == null) {
						return ("第" + (j + 1) + "行,年度为必输项！");
					} else if (row.getCell(1) == null) {
						return ("第" + (j + 1) + "行,客户经理号为必输项！");
					} else if (row.getCell(2) == null) {
						return ("第" + (j + 1) + "行,所属机构为必输项！");
					} else if (row.getCell(3) == null) {
						return ("第" + (j + 1) + "行,调整档次为必输项！");
					} else if (row.getCell(4) == null) {
						return ("第" + (j + 1) + "行,调整原因为必输项！");
					}
					// 年度日期格式check
					String dtDate = row.getCell(0).getStringCellValue();
					String inputStr = dtDate + "0101";
					if (!DateUtil.isDate(inputStr)) {
						return ("第" + (j + 1) + "行,年度【" + dtDate + "】不是正确的日期格式！例:YYYY");
					}

					// 客户经理号长度check
					String empCd = row.getCell(1).getStringCellValue();
					if (empCd.length() > 20) {
						return ("第" + (j + 1) + "行,客户经理号【" + empCd + "】长度不能超过20位！");
					}

					// 机构代码存在check
					String org = row.getCell(2).getStringCellValue();
					Map<String, Object> orgParams = new HashMap<>();
					orgParams.put("org", org);
					String orgNm = subBranOpinDao.queryOrgNm(orgParams);
					if ("".equals(orgNm) || orgNm == null) {
						return ("第" + (j + 1) + "行,机构代码【" + org + "】不存在！");
					}

					// 调整档次check
					String ajstLvl = row.getCell(4).getStringCellValue();
					if (!StrUtil.isInteger(ajstLvl)) {
						return ("第" + (j + 1) + "行,调整档次【" + ajstLvl + "】必须为整数！");
					}

					// 调整原因长度check
					String ajstRsn = row.getCell(5).getStringCellValue();
					if (ajstRsn.length() > 200) {
						return ("第" + (j + 1) + "行,调整原因【" + ajstRsn + "】长度不能超过200位！");
					}

					subBranOpin.setDtDate(dtDate);
					subBranOpin.setEmpCd(empCd);
					subBranOpin.setOrg(org);
					subBranOpin.setAjstLvl(ajstLvl);
					subBranOpin.setAjstRsn(ajstRsn);
					if (Integer.parseInt(ajstLvl) < Integer.parseInt(adjustLower) || Integer.parseInt(ajstLvl) > Integer.parseInt(adjustUpper)) {
						subBranOpin.setApprStt("2");
					} else {
						subBranOpin.setApprStt("1");
					}
					subBranOpinAddList.add(subBranOpin);
					if (j + 1 == 2) {
						subBranOpinList.add(subBranOpin);
					}
					for (int k = 0; k < subBranOpinList.size(); k++) {
						if (!dtDate.equals(subBranOpinList.get(k).getDtDate()) &&
								!org.equals(subBranOpinList.get(k).getOrg())) {
							subBranOpinList.add(subBranOpin);
						}
					}
				}
			}
			for (int l = 0; l < subBranOpinList.size(); l++) {
				Map<String, Object> params = new HashMap<>();
				params.put("dtDate",subBranOpinList.get(l).getDtDate());
				params.put("org",subBranOpinList.get(l).getOrg());
				subBranOpinDao.deleteSubBranOpin(params);
			}
			if (subBranOpinAddList.size() > 0) {
				subBranOpinDao.insertSubBranOpin(subBranOpinAddList);
			}
		}catch(Exception e){
			return "文件导入异常！";
		}
		return "";
	}

	@Override
	public void approve(Map<String, Object> params) {
		String dtDate = (String) params.get("dtDate");
		String empCd = (String) params.get("empCd");
		String org = (String) params.get("org");
		String apprOpin = (String) params.get("apprOpin");
		params.put("dtDate", dtDate);
		params.put("empCd", empCd);
		params.put("org", org);
		params.put("apprOpin", apprOpin);
		subBranOpinDao.approve(params);
	}
}
