package com.beyondsoft.modules.cba.controller;

import com.alibaba.druid.util.FnvHash;
import com.beyondsoft.common.annotation.SysLog;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.cba.entity.PubFanaInfoEntity;
import com.beyondsoft.modules.cba.service.PubFanaInfoService;
import com.beyondsoft.modules.app.utils.JwtUtils;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

import static org.springframework.util.ResourceUtils.CLASSPATH_URL_PREFIX;

/**
 * 对公理财中收导入及查询
 *
 * @author yuchong
 */
@RestController
@RequestMapping("/cba/pubFanaInfo")
public class PubFanaInfoController {
    @Autowired
    private PubFanaInfoService pubFanaInfoService;
    @Autowired
    private JwtUtils jwtUtils;

    /**
     * 查询
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        // 查询对公理财中收记录数
        int count = pubFanaInfoService.queryPubFanaInfoNb(params);
        // 查询对公理财中收信息
        List<PubFanaInfoEntity> pubFanaInfoList = pubFanaInfoService.queryPubFanaInfo(params);
        return R.ok().put("list", pubFanaInfoList).put("totalCount", count);
    }
    /**
     * 导入
     */
    @SysLog("对公理财中收导入")
    @RequestMapping("/importData")
    @ApiOperation("导入excel")
    public R importData(@RequestParam(value = "file") MultipartFile file) {
        String fileName = file.getOriginalFilename();
        String msg = pubFanaInfoService.batchImport(fileName, file);
        if ("".equals(msg)) {
            return R.ok();
        } else {
            return R.error(msg);
        }
    }
    /**
     * 模板文件下载
     */
    @RequestMapping("/downloadExcel")
    @ApiOperation("模板文件下载")
    public void downloadTemplate(HttpServletResponse response){
        try (OutputStream out = response.getOutputStream()){
            String fileName = "pubFanaInfo.xlsx";
            String path = "template/pubFanaInfo.xlsx";
            String resource = getClass().getResource("/template/"+fileName).getPath();
            resource = java.net.URLDecoder.decode(resource,"utf-8");

            response.setContentType("application/x-msdownload;charset=UTF-8");
            response.setHeader("Content-Disposition", "attachment; filename="
                    + URLEncoder.encode(fileName, StandardCharsets.UTF_8.toString()));
            response.setHeader("fileName", URLEncoder.encode(fileName, StandardCharsets.UTF_8.toString()));
            response.setHeader(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, "fileName");

            XSSFWorkbook wb = new XSSFWorkbook(resource);
            wb.write(out);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
