package com.beyondsoft.modules.cba.entity;

import lombok.Data;
import java.io.Serializable;


/**
 *
 *
 * @author yuchong
 */
@Data
public class CsrMgrInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 年度
	 */
	private String dtDate;
	/**
	 * 机构号
	 */
	private String org;
	/**
	 * 机构名称
	 */
	private String orgNm;
	/**
	 * 客户经理号
	 */
	private String empCd;
	/**
	 * 客户经理名称
	 */
	private String empNm;
	/**
	 * 起始日
	 */
	private String stDt;
	/**
	 * 到期日
	 */
	private String endDt;

}
