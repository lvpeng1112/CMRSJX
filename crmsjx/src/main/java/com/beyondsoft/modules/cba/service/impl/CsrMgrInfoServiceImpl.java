package com.beyondsoft.modules.cba.service.impl;

import java.io.InputStream;
import java.util.*;
import java.util.Map;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.utils.DateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.beyondsoft.modules.cba.dao.CsrMgrInfoDao;
import com.beyondsoft.modules.cba.entity.CsrMgrInfoEntity;
import com.beyondsoft.modules.cba.service.CsrMgrInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;


@Service("csrMgrInfoService")
public class CsrMgrInfoServiceImpl extends ServiceImpl<CsrMgrInfoDao, CsrMgrInfoEntity> implements CsrMgrInfoService {
	@Autowired(required = false)
	private CsrMgrInfoDao csrMgrInfoDao;

	@Override
	public int queryCsrMgrInfoNb(Map<String, Object> params) {
		String dtDate = (String) params.get("dtDate");
		params.put("dtDate", dtDate);
		int count = csrMgrInfoDao.queryCsrMgrInfoNb(params);
		return count;
	}

	@Override
	public List<CsrMgrInfoEntity> queryCsrMgrInfo(Map<String, Object> params) {
		String dtDate = (String) params.get("dtDate");
		int pageNo = Integer.parseInt((String) params.get("page"));
		int pageCount = Integer.parseInt((String)params.get("limit"));
		int offset = ((pageNo - 1) * pageCount);
		params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
		params.put("offset", offset);
		params.put("dtDate", dtDate);
		List<CsrMgrInfoEntity> csrMgrInfoList = new ArrayList<>();
		csrMgrInfoList = csrMgrInfoDao.queryCsrMgrInfo(params);
		return csrMgrInfoList;
	}

	@Override
	public String batchImport(String fileName, MultipartFile file) {

		if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
			return ("上传文件格式不正确");
		}
		String extString = fileName.substring(fileName.lastIndexOf("."));
		List<CsrMgrInfoEntity> crsMgrInfoList = new ArrayList<>();
		List<CsrMgrInfoEntity> crsMgrInfoAddList = new ArrayList<>();
		try{
			InputStream is = file.getInputStream();
			Workbook workbook = null;
			if(".xls".equals(extString)){
				HSSFWorkbook wb = new HSSFWorkbook(is);
				workbook = wb;
			}else if(".xlsx".equals(extString)){
				XSSFWorkbook wb = new XSSFWorkbook(is);
				workbook = wb;
			}
			//有多少个sheet
			int sheets = workbook.getNumberOfSheets();

			for (int i = 0; i < sheets; i++) {
				Sheet sheet = workbook.getSheetAt(i);
				//获取多少行
				int rows = sheet.getPhysicalNumberOfRows();
				CsrMgrInfoEntity crsMgrInfo = null;
				//遍历每一行，注意：第 0 行为标题
				for (int j = 1; j < rows; j++) {
					crsMgrInfo = new CsrMgrInfoEntity();
					//获得第 j 行
					Row row = sheet.getRow(j);
					// 空行check
					if (row == null) {
						return ("第" + (j + 1) + "行,没有数据！");
					}
					// 必须输入项check
					if (row.getCell(0) == null) {
						return ("第" + (j + 1) + "行,年度为必输项！");
					} else if (row.getCell(1) == null) {
						return ("第" + (j + 1) + "行,机构代码为必输项！");
					} else if (row.getCell(3) == null) {
						return ("第" + (j + 1) + "行,客户经理号为必输项！");
					} else if (row.getCell(4) == null) {
						return ("第" + (j + 1) + "行,客户经理名称为必输项！");
					} else if (row.getCell(5) == null) {
						return ("第" + (j + 1) + "行,起始日为必输项！");
					} else if (row.getCell(6) == null) {
						return ("第" + (j + 1) + "行,到期日为必输项！");
					}
					// 年度日期格式check
					String dtDate = row.getCell(0).getStringCellValue();
					if (!"-".equals(dtDate.substring(4,5))) {
						return ("第" + (j + 1) + "行,年度【" + dtDate + "】不是正确的日期格式！例:YYYY-MM");
					}
					String inputStr = dtDate.substring(0,4) + dtDate.substring(5,7) + "01";
					if (!DateUtil.isDate(inputStr)) {
						return ("第" + (j + 1) + "行,年度【" + dtDate + "】不是正确的日期格式！例:YYYY-MM");
					}

					// 机构代码存在check
					String org = row.getCell(1).getStringCellValue();
					Map<String, Object> orgParams = new HashMap<>();
					orgParams.put("org", org);
					String orgNm = csrMgrInfoDao.queryOrgNm(orgParams);
					if ("".equals(orgNm) || orgNm == null) {
						return ("第" + (j + 1) + "行,机构代码【" + org + "】不存在！");
					}

					// 客户经理号长度check
					String empCd = row.getCell(3).getStringCellValue();
					if (empCd.length() > 20) {
						return ("第" + (j + 1) + "行,客户经理号【" + empCd + "】长度不能超过20位！");
					}
					String empNm = row.getCell(4).getStringCellValue();

					// 开始日日期格式check
					String stDt = row.getCell(5).getStringCellValue();
					if (!DateUtil.isDate(stDt)) {
						return ("第" + (j + 1) + "行,开始日【" + stDt + "】不是正确的日期格式！例:YYYYMMDD");
					}

					// 结束日日期格式check
					String endDt = row.getCell(6).getStringCellValue();
					if (!DateUtil.isDate(endDt)) {
						return ("第" + (j + 1) + "行,到期日【" + endDt + "】不是正确的日期格式！例:YYYYMMDD");
					}

					// 开始日大于结束日check
					if (endDt.compareTo(stDt) <= 0) {
						return ("第" + (j + 1) + "行,开始日【" + stDt + "】不能大于到期日【" + endDt + "】！");
					}

					crsMgrInfo.setDtDate(dtDate);
					crsMgrInfo.setOrg(org);
					crsMgrInfo.setOrgNm(orgNm);
					crsMgrInfo.setEmpCd(empCd);
					crsMgrInfo.setEmpNm(empNm);
					crsMgrInfo.setStDt(stDt);
					crsMgrInfo.setEndDt(endDt);
					if (crsMgrInfoList.size() > 0) {
						for (int k = 0; k < crsMgrInfoList.size(); k++) {
							if (dtDate.equals(crsMgrInfoList.get(k).getDtDate()) &&
									org.equals(crsMgrInfoList.get(k).getOrg()) &&
									empCd.equals(crsMgrInfoList.get(k).getEmpCd())) {
								return ("第" + (j + 1) + "行,客户经理信息重复！");
							}
						}
					}
					crsMgrInfoList.add(crsMgrInfo);
				}
			}
			for (int l = 0; l < crsMgrInfoList.size(); l++) {
				Map<String, Object> params = new HashMap<>();
				params.put("dtDate",crsMgrInfoList.get(l).getDtDate());
				params.put("org",crsMgrInfoList.get(l).getOrg());
				params.put("empCd",crsMgrInfoList.get(l).getEmpCd());
				params.put("empNm",crsMgrInfoList.get(l).getEmpNm());
				params.put("stDt",crsMgrInfoList.get(l).getStDt());
				params.put("endDt",crsMgrInfoList.get(l).getEndDt());
				int count = csrMgrInfoDao.queryCsrMgrInfoNb(params);
				if (count == 0) {
					crsMgrInfoAddList.add(crsMgrInfoList.get(l));
				} else {
					csrMgrInfoDao.updateCrsMgrInfo(params);
				}
			}
			if (crsMgrInfoAddList.size() > 0) {
				csrMgrInfoDao.insertCrsMgrInfo(crsMgrInfoAddList);
			}
		}catch(Exception e){
			return "文件导入异常！";
		}
		return "";
	}
}
