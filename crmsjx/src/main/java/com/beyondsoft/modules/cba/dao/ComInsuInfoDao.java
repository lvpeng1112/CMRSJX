package com.beyondsoft.modules.cba.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.cba.entity.ComInsuInfoEntity;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import java.util.Map;

/**
 * 代理保险中收导入及查询
 *
 * @author yuchong
 */
@Mapper
public interface ComInsuInfoDao extends BaseMapper<ComInsuInfoEntity> {
    // 查询代理保险中收记录数
    int queryComInsuInfoNb(Map<String, Object> params);
    // 查询代理保险中收信息
    List<ComInsuInfoEntity> queryComInsuInfo(Map<String, Object> params);
    // 删除代理保险中收信息
    void deleteComInsuInfo(Map<String, Object> params);
    // 插入代理保险中收信息
    void insertComInsuInfo(List<ComInsuInfoEntity> list);
}
