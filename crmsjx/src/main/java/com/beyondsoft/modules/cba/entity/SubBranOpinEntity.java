package com.beyondsoft.modules.cba.entity;

import lombok.Data;
import java.io.Serializable;


/**
 *
 *
 * @author yuchong
 */
@Data
public class SubBranOpinEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 年度
	 */
	private String dtDate;
	/**
	 * 客户经理号
	 */
	private String empCd;
	/**
	 * 所属机构
	 */
	private String org;
	/**
	 * 机构名称
	 */
	private String orgNm;
	/**
	 * 调整档次
	 */
	private String ajstLvl;
	/**
	 * 调整理由
	 */
	private String ajstRsn;
	/**
	 * 审核状态
	 */
	private String apprStt;
    /**
     * 审核意见
     */
    private String apprOpin;

}
