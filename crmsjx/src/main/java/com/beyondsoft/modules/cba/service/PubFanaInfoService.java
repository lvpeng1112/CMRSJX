package com.beyondsoft.modules.cba.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.modules.cba.entity.PubFanaInfoEntity;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;
import java.util.Map;
/**
 * 对公理财中收导入及查询
 *
 * @author yuchong
 */
public interface PubFanaInfoService extends IService<PubFanaInfoEntity> {
	// 查询对公理财中收记录数
	int queryPubFanaInfoNb(Map<String, Object> params);
	// 查询对公理财中收信息
	List<PubFanaInfoEntity> queryPubFanaInfo(Map<String, Object> params);


	String batchImport(String fileName, MultipartFile file);

}
