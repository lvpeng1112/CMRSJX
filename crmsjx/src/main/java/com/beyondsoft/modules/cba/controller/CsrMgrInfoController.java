package com.beyondsoft.modules.cba.controller;

import com.beyondsoft.common.annotation.SysLog;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.cba.entity.CsrMgrInfoEntity;
import com.beyondsoft.modules.cba.service.CsrMgrInfoService;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

/**
 * 客户经理名单导入及查询
 *
 * @author yuchong
 */
@RestController
@RequestMapping("/cba/csrMgrInfo")
public class CsrMgrInfoController {
    @Autowired
    private CsrMgrInfoService csrMgrInfoService;

    /**
     * 查询
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        // 查询客户经理名单记录数
        int count = csrMgrInfoService.queryCsrMgrInfoNb(params);
        // 查询客户经理名单信息
        List<CsrMgrInfoEntity> csrMgrInfoList = csrMgrInfoService.queryCsrMgrInfo(params);
        return R.ok().put("list", csrMgrInfoList).put("totalCount", count);
    }
    /**
     * 导入
     */
    @SysLog("客户经理名单导入")
    @RequestMapping("/importData")
    @ApiOperation("导入excel")
    public R importData(@RequestParam(value = "file") MultipartFile file) {
        String fileName = file.getOriginalFilename();
        String msg = csrMgrInfoService.batchImport(fileName, file);
        if ("".equals(msg)) {
            return R.ok();
        } else {
            return R.error(msg);
        }
    }
    /**
     * 模板文件下载
     */
    @RequestMapping("/downloadExcel")
    @ApiOperation("模板文件下载")
    public void downloadTemplate(HttpServletResponse response){
        try (OutputStream out = response.getOutputStream()){
            String fileName = "csrMgrInfo.xlsx";
            String path = "template/csrMgrInfo.xlsx";
            String resource = getClass().getResource("/template/"+fileName).getPath();

            response.setContentType("application/x-msdownload;charset=UTF-8");
            response.setHeader("Content-Disposition", "attachment; filename="
                    + URLEncoder.encode(fileName, StandardCharsets.UTF_8.toString()));
            response.setHeader("fileName", URLEncoder.encode(fileName, StandardCharsets.UTF_8.toString()));
            response.setHeader(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, "fileName");

            XSSFWorkbook wb = new XSSFWorkbook(resource);
            wb.write(out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
