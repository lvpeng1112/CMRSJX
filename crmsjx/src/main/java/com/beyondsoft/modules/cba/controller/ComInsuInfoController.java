package com.beyondsoft.modules.cba.controller;

import com.alibaba.druid.util.FnvHash;
import com.beyondsoft.common.annotation.SysLog;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.cba.entity.ComInsuInfoEntity;
import com.beyondsoft.modules.cba.service.ComInsuInfoService;
import com.beyondsoft.modules.app.utils.JwtUtils;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

import static org.springframework.util.ResourceUtils.CLASSPATH_URL_PREFIX;

/**
 * 代理保险中收导入及查询
 *
 * @author yuchong
 */
@RestController
@RequestMapping("/cba/comInsuInfo")
public class ComInsuInfoController {
    @Autowired
    private ComInsuInfoService comInsuInfoService;
    @Autowired
    private JwtUtils jwtUtils;

    /**
     * 查询
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        // 查询代理保险中收记录数
        int count = comInsuInfoService.queryComInsuInfoNb(params);
        // 查询代理保险中收信息
        List<ComInsuInfoEntity> comInsuInfoList = comInsuInfoService.queryComInsuInfo(params);
        return R.ok().put("list", comInsuInfoList).put("totalCount", count);
    }
    /**
     * 导入
     */
    @SysLog("代理保险中收导入")
    @RequestMapping("/importData")
    @ApiOperation("导入excel")
    public R importData(@RequestParam(value = "file") MultipartFile file) {
        String fileName = file.getOriginalFilename();
        String msg = comInsuInfoService.batchImport(fileName, file);
        if ("".equals(msg)) {
            return R.ok();
        } else {
            return R.error(msg);
        }
    }
    /**
     * 模板文件下载
     */
    @RequestMapping("/downloadExcel")
    @ApiOperation("模板文件下载")
    public void downloadTemplate(HttpServletResponse response){
        try (OutputStream out = response.getOutputStream()){
            String fileName = "comInsuInfo.xlsx";
            String path = "template/comInsuInfo.xlsx";
            String resource = getClass().getResource("/template/"+fileName).getPath();

            response.setContentType("application/x-msdownload;charset=UTF-8");
            response.setHeader("Content-Disposition", "attachment; filename="
                    + URLEncoder.encode(fileName, StandardCharsets.UTF_8.toString()));
            response.setHeader("fileName", URLEncoder.encode(fileName, StandardCharsets.UTF_8.toString()));
            response.setHeader(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, "fileName");

            XSSFWorkbook wb = new XSSFWorkbook(resource);
            wb.write(out);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
