package com.beyondsoft.modules.cba.controller;

import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.cba.entity.CsrMgrAchvInfoEntity;
import com.beyondsoft.modules.cba.service.CsrMgrAchvInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;

/**
 * 客户经理业绩查询
 *
 * @author yuchong
 */
@RestController
@RequestMapping("/cba/csrMgrAchvInfo")
public class CsrMgrAchvInfoController {
    @Autowired
    private CsrMgrAchvInfoService csrMgrAchvInfoService;

    /**
     * 查询
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        // 查询客户经理业绩记录数
        int count = csrMgrAchvInfoService.queryCsrMgrAchvInfoNb(params);
        // 查询客户经理业绩信息
        List<CsrMgrAchvInfoEntity> achvInfoList = csrMgrAchvInfoService.queryCsrMgrAchvInfo(params);
        return R.ok().put("list", achvInfoList).put("totalCount", count);
    }
}
