package com.beyondsoft.modules.cba.entity;

import lombok.Data;
import java.io.Serializable;


/**
 *
 *
 * @author yuchong
 */
@Data
public class CsrMgrAchvInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 年月
	 */
	private String dateDt;
	/**
	 * 机构号
	 */
	private String orgId;
	/**
	 * 机构名称
	 */
	private String orgNm;
	/**
	 * 客户经理号
	 */
	private String empCd;
	/**
	 * 客户经理名称
	 */
	private String empNm;
	/**
	 * 客户经理EVA
	 */
	private String eva;
	/**
	 * 客户经理EVA对应等级
	 */
	private String evaLvl;
	/**
	 * 客户经理新开户企业数
	 */
	private String newOpenNm;
	/**
	 * 客户经理新开户对应调整等级
	 */
	private String newOpenLvl;
	/**
	 * 支行调整意见
	 */
	private String ajstLvl;
	/**
	 * 备注
	 */
	private String ajstRsn;
	/**
	 * 最终等级
	 */
	private String lastLvl;

}
