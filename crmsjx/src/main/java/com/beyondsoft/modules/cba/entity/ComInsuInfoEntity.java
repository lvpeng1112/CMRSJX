package com.beyondsoft.modules.cba.entity;

import lombok.Data;
import java.io.Serializable;


/**
 *
 *
 * @author yuchong
 */
@Data
public class ComInsuInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 年度
	 */
	private String dtDate;
	/**
	 * 导入日期
	 */
	private String inputDate;
	/**
	 * 投保人
	 */
	private String policyHolder;
	/**
	 * 中收收入
	 */
	private String rcvdIncome;
	/**
	 * 归属客户经理号
	 */
	private String empCd;
	/**
	 * 归属客户经理名称
	 */
	private String empNm;
	/**
	 * 归属客户经理比例
	 */
	private String empScale;

}
