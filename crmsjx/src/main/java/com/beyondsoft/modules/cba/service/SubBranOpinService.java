package com.beyondsoft.modules.cba.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.modules.cba.entity.SubBranOpinEntity;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;
import java.util.Map;
/**
 * 支行调整意见导入及查询
 *
 * @author yuchong
 */
public interface SubBranOpinService extends IService<SubBranOpinEntity> {
	// 查询支行调整意见记录数
	int querySubBranOpinNb(Map<String, Object> params);
	// 查询支行调整意见信息
	List<SubBranOpinEntity> querySubBranOpin(Map<String, Object> params);


	String batchImport(String fileName, MultipartFile file);
	// 调整意见审核
	void approve(Map<String, Object> params);

}
