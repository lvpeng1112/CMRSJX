package com.beyondsoft.modules.cba.entity;

import lombok.Data;
import java.io.Serializable;


/**
 *
 *
 * @author yuchong
 */
@Data
public class PubFanaInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 年度
	 */
	private String dtDate;
	/**
	 * 机构号
	 */
	private String org;
	/**
	 * 机构名称
	 */
	private String orgNm;
	/**
	 * 销售管理费（元）
	 */
	private String saleMgrRate;
	/**
	 * 投资管理费（元）
	 */
	private String inveMgrRate;

}
