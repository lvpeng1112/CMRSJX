package com.beyondsoft.modules.cba.service.impl;

import java.io.InputStream;
import java.util.*;
import java.util.Map;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.utils.DateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.beyondsoft.modules.cba.dao.ComInsuInfoDao;
import com.beyondsoft.modules.cba.entity.ComInsuInfoEntity;
import com.beyondsoft.modules.cba.service.ComInsuInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;


@Service("comInsuInfoService")
public class ComInsuInfoServiceImpl extends ServiceImpl<ComInsuInfoDao, ComInsuInfoEntity> implements ComInsuInfoService {
	@Autowired(required = false)
	private ComInsuInfoDao comInsuInfoDao;

	@Override
	public int queryComInsuInfoNb(Map<String, Object> params) {
		String dtDate = (String) params.get("dtDate");
		params.put("dtDate", dtDate);
		int count = comInsuInfoDao.queryComInsuInfoNb(params);
		return count;
	}

	@Override
	public List<ComInsuInfoEntity> queryComInsuInfo(Map<String, Object> params) {
		String dtDate = (String) params.get("dtDate");
		int pageNo = Integer.parseInt((String) params.get("page"));
		int pageCount = Integer.parseInt((String)params.get("limit"));
		int offset = ((pageNo - 1) * pageCount);
		params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
		params.put("offset", offset);
		params.put("dtDate", dtDate);
		List<ComInsuInfoEntity> comInsuInfoList = new ArrayList<>();
		comInsuInfoList = comInsuInfoDao.queryComInsuInfo(params);
		return comInsuInfoList;
	}

	@Override
	public String batchImport(String fileName, MultipartFile file) {

		if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
			return ("上传文件格式不正确");
		}
		String extString = fileName.substring(fileName.lastIndexOf("."));
		List<String> dtDateList = new ArrayList<>();
		List<ComInsuInfoEntity> comInsuInfoAddList = new ArrayList<>();
		try{
			InputStream is = file.getInputStream();
			Workbook workbook = null;
			if(".xls".equals(extString)){
				HSSFWorkbook wb = new HSSFWorkbook(is);
				workbook = wb;
			}else if(".xlsx".equals(extString)){
				XSSFWorkbook wb = new XSSFWorkbook(is);
				workbook = wb;
			}
			//有多少个sheet
			int sheets = workbook.getNumberOfSheets();

			for (int i = 0; i < sheets; i++) {
				Sheet sheet = workbook.getSheetAt(i);
				//获取多少行
				int rows = sheet.getPhysicalNumberOfRows();
				ComInsuInfoEntity comInsuInfo = null;
				//遍历每一行，注意：第 0 行为标题
				for (int j = 1; j < rows; j++) {
					comInsuInfo = new ComInsuInfoEntity();
					//获得第 j 行
					Row row = sheet.getRow(j);
					// 空行check
					if (row == null) {
						return ("第" + (j + 1) + "行,没有数据！");
					}
					// 必须输入项check
					if (row.getCell(0) == null) {
						return ("第" + (j + 1) + "行,年度为必输项！");
					} else if (row.getCell(1) == null) {
						return ("第" + (j + 1) + "行,导入日期为必输项！");
					}
					// 年度日期格式check
					String dtDate = row.getCell(0).getStringCellValue();
					if (!"-".equals(dtDate.substring(4,5))) {
						return ("第" + (j + 1) + "行,年度【" + dtDate + "】不是正确的日期格式！例:YYYY-MM");
					}
					String inputStr = dtDate.substring(0,4) + dtDate.substring(5,7) + "01";
					if (!DateUtil.isDate(inputStr)) {
						return ("第" + (j + 1) + "行,年度【" + dtDate + "】不是正确的日期格式！例:YYYY-MM");
					}

					// 导入日期格式check
					String inputDate = row.getCell(1).getStringCellValue();
					if (!DateUtil.isDate(inputDate)) {
						return ("第" + (j + 1) + "行,导入日期【" + inputDate + "】不是正确的日期格式！例:YYYYMMDD");
					}

					String policyHolder = row.getCell(2).getStringCellValue();
					String rcvdIncome = row.getCell(3).getStringCellValue();
					String empCd = row.getCell(4).getStringCellValue();
					String empNm = row.getCell(5).getStringCellValue();
					String empScale = row.getCell(6).getStringCellValue();
					comInsuInfo.setDtDate(dtDate);
					comInsuInfo.setInputDate(inputDate);
					comInsuInfo.setPolicyHolder(policyHolder);
					comInsuInfo.setRcvdIncome(rcvdIncome);
					comInsuInfo.setEmpCd(empCd);
					comInsuInfo.setEmpNm(empNm);
					comInsuInfo.setEmpScale(empScale);
					comInsuInfoAddList.add(comInsuInfo);
					if (j + 1 == 2) {
						dtDateList.add(dtDate);
					}
					for (int k = 0; k < dtDateList.size(); k++) {
						if (!dtDate.equals(dtDateList.get(k))) {
							dtDateList.add(dtDate);
						}
					}
				}
			}
			for (int l = 0; l < dtDateList.size(); l++) {
				Map<String, Object> params = new HashMap<>();
				params.put("dtDate",dtDateList.get(l));
				comInsuInfoDao.deleteComInsuInfo(params);
			}
			if (comInsuInfoAddList.size() > 0) {
				comInsuInfoDao.insertComInsuInfo(comInsuInfoAddList);
			}
		}catch(Exception e){
			return "文件导入异常！";
		}
		return "";
	}
}
