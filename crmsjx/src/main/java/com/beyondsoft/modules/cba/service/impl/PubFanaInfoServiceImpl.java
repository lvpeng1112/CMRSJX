package com.beyondsoft.modules.cba.service.impl;

import java.io.InputStream;
import java.util.*;
import java.util.Map;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.utils.DateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.beyondsoft.modules.cba.dao.PubFanaInfoDao;
import com.beyondsoft.modules.cba.entity.PubFanaInfoEntity;
import com.beyondsoft.modules.cba.service.PubFanaInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;


@Service("pubfanaInfoService")
public class PubFanaInfoServiceImpl extends ServiceImpl<PubFanaInfoDao, PubFanaInfoEntity> implements PubFanaInfoService {
	@Autowired(required = false)
	private PubFanaInfoDao pubFanaInfoDao;

	@Override
	public int queryPubFanaInfoNb(Map<String, Object> params) {
		String dtDate = (String) params.get("dtDate");
		params.put("dtDate", dtDate);
		int count = pubFanaInfoDao.queryPubFanaInfoNb(params);
		return count;
	}

	@Override
	public List<PubFanaInfoEntity> queryPubFanaInfo(Map<String, Object> params) {
		String dtDate = (String) params.get("dtDate");
		int pageNo = Integer.parseInt((String) params.get("page"));
		int pageCount = Integer.parseInt((String)params.get("limit"));
		int offset = ((pageNo - 1) * pageCount);
		params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
		params.put("offset", offset);
		params.put("dtDate", dtDate);
		List<PubFanaInfoEntity> pubFanaInfoList = new ArrayList<>();
		pubFanaInfoList = pubFanaInfoDao.queryPubFanaInfo(params);
		return pubFanaInfoList;
	}

	@Override
	public String batchImport(String fileName, MultipartFile file) {

		if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
			return ("上传文件格式不正确");
		}
		String extString = fileName.substring(fileName.lastIndexOf("."));
		List<PubFanaInfoEntity> pubFanaInfoList = new ArrayList<>();
		List<PubFanaInfoEntity> pubFanaInfoAddList = new ArrayList<>();
		try{
			InputStream is = file.getInputStream();
			Workbook workbook = null;
			if(".xls".equals(extString)){
				HSSFWorkbook wb = new HSSFWorkbook(is);
				workbook = wb;
			}else if(".xlsx".equals(extString)){
				XSSFWorkbook wb = new XSSFWorkbook(is);
				workbook = wb;
			}
			//有多少个sheet
			int sheets = workbook.getNumberOfSheets();

			for (int i = 0; i < sheets; i++) {
				Sheet sheet = workbook.getSheetAt(i);
				//获取多少行
				int rows = sheet.getPhysicalNumberOfRows();
				PubFanaInfoEntity pubFanaInfo = null;
				//遍历每一行，注意：第 0 行为标题
				for (int j = 1; j < rows; j++) {
					pubFanaInfo = new PubFanaInfoEntity();
					//获得第 j 行
					Row row = sheet.getRow(j);
					// 空行check
					if (row == null) {
						return ("第" + (j + 1) + "行,没有数据！");
					}
					// 必须输入项check
					if (row.getCell(0) == null) {
						return ("第" + (j + 1) + "行,年度为必输项！");
					} else if (row.getCell(1) == null) {
						return ("第" + (j + 1) + "行,机构代码为必输项！");
					} else if (row.getCell(3) == null) {
						return ("第" + (j + 1) + "行,销售管理费（元）为必输项！");
					} else if (row.getCell(4) == null) {
						return ("第" + (j + 1) + "行,投资管理费（元）为必输项！");
					}
					// 年度日期格式check
					String dtDate = row.getCell(0).getStringCellValue();
					if (!"-".equals(dtDate.substring(4,5))) {
						return ("第" + (j + 1) + "行,年度【" + dtDate + "】不是正确的日期格式！例:YYYY-MM");
					}
					String inputStr = dtDate.substring(0,4) + dtDate.substring(5,7) + "01";
					if (!DateUtil.isDate(inputStr)) {
						return ("第" + (j + 1) + "行,年度【" + dtDate + "】不是正确的日期格式！例:YYYY-MM");
					}

					// 机构代码存在check
					String org = row.getCell(1).getStringCellValue();
					Map<String, Object> orgParams = new HashMap<>();
					orgParams.put("org", org);
					String orgNm = pubFanaInfoDao.queryOrgNm(orgParams);
					if ("".equals(orgNm) || orgNm == null) {
						return ("第" + (j + 1) + "行,机构代码【" + org + "】不存在！");
					}

					String saleMgrRate = row.getCell(3).getStringCellValue();
					String inveMgrRate = row.getCell(4).getStringCellValue();
					pubFanaInfo.setDtDate(dtDate);
					pubFanaInfo.setOrg(org);
					pubFanaInfo.setOrgNm(orgNm);
					pubFanaInfo.setSaleMgrRate(saleMgrRate);
					pubFanaInfo.setInveMgrRate(inveMgrRate);
					if (pubFanaInfoList.size() > 0) {
						for (int k = 0; k < pubFanaInfoList.size(); k++) {
							if (dtDate.equals(pubFanaInfoList.get(k).getDtDate()) &&
									org.equals(pubFanaInfoList.get(k).getOrg())) {
								return ("第" + (j + 1) + "行,对公理财中收信息重复！");
							}
						}
					}
					pubFanaInfoList.add(pubFanaInfo);
				}
			}
			for (int l = 0; l < pubFanaInfoList.size(); l++) {
				Map<String, Object> params = new HashMap<>();
				params.put("dtDate",pubFanaInfoList.get(l).getDtDate());
				params.put("org",pubFanaInfoList.get(l).getOrg());
				params.put("saleMgrRate",pubFanaInfoList.get(l).getSaleMgrRate());
				params.put("inveMgrRate",pubFanaInfoList.get(l).getInveMgrRate());
				int count = pubFanaInfoDao.queryPubFanaInfoNb(params);
				if (count == 0) {
					pubFanaInfoAddList.add(pubFanaInfoList.get(l));
				} else {
					pubFanaInfoDao.updatePubFanaInfo(params);
				}
			}
			if (pubFanaInfoAddList.size() > 0) {
				pubFanaInfoDao.insertPubFanaInfo(pubFanaInfoAddList);
			}
		}catch(Exception e){
			return "文件导入异常！";
		}
		return "";
	}
}
