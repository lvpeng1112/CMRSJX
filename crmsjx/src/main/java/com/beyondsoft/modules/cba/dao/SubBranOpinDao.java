package com.beyondsoft.modules.cba.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.cba.entity.SubBranOpinEntity;
import org.apache.ibatis.annotations.Mapper;
import com.beyondsoft.common.utils.R;
import java.util.List;
import java.util.Map;

/**
 * 支行调整意见导入及查询
 *
 * @author yuchong
 */
@Mapper
public interface SubBranOpinDao extends BaseMapper<SubBranOpinEntity> {
    // 查询支行调整意见记录数
    int querySubBranOpinNb(Map<String, Object> params);
    // 查询支行调整意见信息
    List<SubBranOpinEntity> querySubBranOpin(Map<String, Object> params);
    // 查询机构名称
    String queryOrgNm(Map<String, Object> params);
    // 删除支行调整意见信息
    void deleteSubBranOpin(Map<String, Object> params);
    // 插入支行调整意见信息
    void insertSubBranOpin(List<SubBranOpinEntity> list);
    // 调整上下限参数取得
    String getAdjustLsl(Map<String, Object> params);
    // 调整意见审核
    void approve(Map<String, Object> params);
}
