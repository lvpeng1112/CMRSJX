package com.beyondsoft.modules.cba.service.impl;

import java.io.InputStream;
import java.util.*;
import java.util.Map;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.modules.cba.dao.CsrMgrAchvInfoDao;
import com.beyondsoft.modules.cba.entity.CsrMgrAchvInfoEntity;
import com.beyondsoft.modules.cba.service.CsrMgrAchvInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;


@Service("csrMgrAchvInfoService")
public class CsrMgrAchvInfoServiceImpl extends ServiceImpl<CsrMgrAchvInfoDao, CsrMgrAchvInfoEntity> implements CsrMgrAchvInfoService {
	@Autowired(required = false)
	private CsrMgrAchvInfoDao csrMgrAchvInfoDao;

	@Override
	public int queryCsrMgrAchvInfoNb(Map<String, Object> params) {
		String dtDate = (String) params.get("dtDate");
		params.put("dtDate", dtDate);
		int count = csrMgrAchvInfoDao.queryCsrMgrAchvInfoNb(params);
		return count;
	}

	@Override
	public List<CsrMgrAchvInfoEntity> queryCsrMgrAchvInfo(Map<String, Object> params) {
		String dtDate = (String) params.get("dtDate");
		int pageNo = Integer.parseInt((String) params.get("page"));
		int pageCount = Integer.parseInt((String)params.get("limit"));
		int offset = ((pageNo - 1) * pageCount);
		params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
		params.put("offset", offset);
		params.put("dtDate", dtDate);
		List<CsrMgrAchvInfoEntity> csrMgrInfoList = new ArrayList<>();
		csrMgrInfoList = csrMgrAchvInfoDao.queryCsrMgrAchvInfo(params);
		return csrMgrInfoList;
	}
}
