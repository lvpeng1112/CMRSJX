package com.beyondsoft.modules.cba.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.modules.cba.entity.ComInsuInfoEntity;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;
import java.util.Map;
/**
 * 代理保险中收导入及查询
 *
 * @author yuchong
 */
public interface ComInsuInfoService extends IService<ComInsuInfoEntity> {
	// 查询代理保险中收记录数
	int queryComInsuInfoNb(Map<String, Object> params);
	// 查询代理保险中收信息
	List<ComInsuInfoEntity> queryComInsuInfo(Map<String, Object> params);


	String batchImport(String fileName, MultipartFile file);

}
