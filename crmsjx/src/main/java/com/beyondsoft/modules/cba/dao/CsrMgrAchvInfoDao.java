package com.beyondsoft.modules.cba.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.cba.entity.CsrMgrAchvInfoEntity;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import java.util.Map;

/**
 * 客户经理业绩查询
 *
 * @author yuchong
 */
@Mapper
public interface CsrMgrAchvInfoDao extends BaseMapper<CsrMgrAchvInfoEntity> {
    // 查询客户经理业绩记录数
    int queryCsrMgrAchvInfoNb(Map<String, Object> params);
    // 查询客户经理业绩信息
    List<CsrMgrAchvInfoEntity> queryCsrMgrAchvInfo(Map<String, Object> params);
}
