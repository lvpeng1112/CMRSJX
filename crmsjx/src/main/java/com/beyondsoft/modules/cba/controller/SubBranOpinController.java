package com.beyondsoft.modules.cba.controller;

import com.alibaba.druid.util.FnvHash;

import com.beyondsoft.common.annotation.SysLog;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.cba.entity.SubBranOpinEntity;
import com.beyondsoft.modules.cba.service.SubBranOpinService;
import com.beyondsoft.modules.app.utils.JwtUtils;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

import static org.springframework.util.ResourceUtils.CLASSPATH_URL_PREFIX;

/**
 * 支行调整意见导入及查询
 *
 * @author yuchong
 */
@RestController
@RequestMapping("/cba/subBranOpin")
public class SubBranOpinController {
    @Autowired
    private SubBranOpinService subBranOpinService;
    @Autowired
    private JwtUtils jwtUtils;

    /**
     * 查询
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        // 查询支行调整意见记录数
        int count = subBranOpinService.querySubBranOpinNb(params);
        // 查询支行调整意见信息
        List<SubBranOpinEntity> subBranOpinList = subBranOpinService.querySubBranOpin(params);
        return R.ok().put("list", subBranOpinList).put("totalCount", count);
    }
    /**
     * 导入
     */
    @SysLog("支行调整意见导入")
    @RequestMapping("/importData")
    @ApiOperation("导入excel")
    public R importData(@RequestParam(value = "file") MultipartFile file) {
        String fileName = file.getOriginalFilename();
        String msg = subBranOpinService.batchImport(fileName, file);
        if ("".equals(msg)) {
            return R.ok();
        } else {
            return R.error(msg);
        }
    }
    /**
     * 模板文件下载
     */
    @RequestMapping("/downloadExcel")
    @ApiOperation("模板文件下载")
    public void downloadTemplate(HttpServletResponse response){
        try (OutputStream out = response.getOutputStream()){
            String fileName = "subBranOpin.xlsx";
            String path = "template/subBranOpin.xlsx";
            String resource = getClass().getResource("/template/"+fileName).getPath();

            response.setContentType("application/x-msdownload;charset=UTF-8");
            response.setHeader("Content-Disposition", "attachment; filename="
                    + URLEncoder.encode(fileName, StandardCharsets.UTF_8.toString()));
            response.setHeader("fileName", URLEncoder.encode(fileName, StandardCharsets.UTF_8.toString()));
            response.setHeader(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, "fileName");

            XSSFWorkbook wb = new XSSFWorkbook(resource);
            wb.write(out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 审核
     */
    @RequestMapping("/approve")
    public R approve(@RequestParam Map<String, Object> params){
        // 调整意见审核
        subBranOpinService.approve(params);
        return R.ok();
    }
}
