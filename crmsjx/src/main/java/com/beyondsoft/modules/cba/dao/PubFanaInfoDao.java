package com.beyondsoft.modules.cba.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.cba.entity.PubFanaInfoEntity;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import java.util.Map;

/**
 * 对公理财中收导入及查询
 *
 * @author yuchong
 */
@Mapper
public interface PubFanaInfoDao extends BaseMapper<PubFanaInfoEntity> {
    // 查询对公理财中收记录数
    int queryPubFanaInfoNb(Map<String, Object> params);
    // 查询对公理财中收信息
    List<PubFanaInfoEntity> queryPubFanaInfo(Map<String, Object> params);
    // 查询机构名称
    String queryOrgNm(Map<String, Object> params);
    // 更新对公理财中收信息
    void updatePubFanaInfo(Map<String, Object> params);
    // 插入对公理财中收信息
    void insertPubFanaInfo(List<PubFanaInfoEntity> list);
}
