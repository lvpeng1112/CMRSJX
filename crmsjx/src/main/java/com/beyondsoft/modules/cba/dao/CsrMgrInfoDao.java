package com.beyondsoft.modules.cba.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.cba.entity.CsrMgrInfoEntity;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import java.util.Map;

/**
 * 客户经理名单导入及查询
 *
 * @author yuchong
 */
@Mapper
public interface CsrMgrInfoDao extends BaseMapper<CsrMgrInfoEntity> {
    // 查询客户经理名单记录数
    int queryCsrMgrInfoNb(Map<String, Object> params);
    // 查询客户经理名单信息
    List<CsrMgrInfoEntity> queryCsrMgrInfo(Map<String, Object> params);
    // 查询机构名称
    String queryOrgNm(Map<String, Object> params);
    // 更新客户经理名单信息
    void updateCrsMgrInfo(Map<String, Object> params);
    // 插入客户经理名单信息
    void insertCrsMgrInfo(List<CsrMgrInfoEntity> list);
}
