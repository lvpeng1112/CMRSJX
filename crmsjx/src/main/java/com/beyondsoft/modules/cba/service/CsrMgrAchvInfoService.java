package com.beyondsoft.modules.cba.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.modules.cba.entity.CsrMgrAchvInfoEntity;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;
import java.util.Map;
/**
 * 客户经理业绩查询
 *
 * @author yuchong
 */
public interface CsrMgrAchvInfoService extends IService<CsrMgrAchvInfoEntity> {
	// 查询客户经理业绩记录数
	int queryCsrMgrAchvInfoNb(Map<String, Object> params);
	// 查询客户经理业绩信息
	List<CsrMgrAchvInfoEntity> queryCsrMgrAchvInfo(Map<String, Object> params);

}
