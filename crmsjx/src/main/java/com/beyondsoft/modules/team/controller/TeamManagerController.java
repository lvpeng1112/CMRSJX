package com.beyondsoft.modules.team.controller;

import com.beyondsoft.common.annotation.SysLog;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.job.dao.ScheduleJobDao;
import com.beyondsoft.modules.sys.entity.SysUserEntity;
import com.beyondsoft.modules.team.dao.TeamManagerDao;
import com.beyondsoft.modules.team.entity.*;
import com.beyondsoft.modules.team.service.TeamManagerService;
import com.beyondsoft.utils.PgsqlUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * @author luochao
 * @email
 * @date 2020-10-28
 */
@RestController
@RequestMapping("team/teamManager")
public class TeamManagerController {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private TeamManagerService teamManagerService;

    @Autowired(required = false)
    private TeamManagerDao teamManagerDao;

    @Autowired(required = false)
    ScheduleJobDao scheduleJobDao;

    public SysUserEntity getUser() {
        return (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
    }

    public Long getUserId() {
        return getUser().getUserId();
    }

    /**
     * 初始化团队页面机构下拉框数据
     */
    @GetMapping("/initSelectOrgID")
    public R initSelectOrgID(@RequestParam Map<String, Object> params) {
        String chdeptcode = (String)params.get("chdeptcode");
        List<String> stringList = new ArrayList<>();
        if(chdeptcode != null && chdeptcode !=""){
            stringList = teamManagerService.findOrgId(params);
        }

        List<String> arrayList= new  ArrayList<String>(stringList); //转换为ArrayLsit调用相关的remove方法
        if (stringList.contains( "0")){ //移除"0"
            arrayList.remove("0" );
        }
        return   R.ok().put("stringList",arrayList);
    }

    /**
     * 团队列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params) {
        String initCode =getUser().getOrgId();
        Map map = new HashMap();
        List<TeamManagerEntity> teamList = teamManagerService.queryParamKeyList(params);
        List<DepartmentEntity> departmentList = teamManagerService.departmentList(params);
        map.put("busintypeid","TEAM_TYPE");
        List<Map<String, Object>> teamType = teamManagerService.queryDictDetail(map);
        String parentOrgId = "";
        for (int i = 0; i < departmentList.size(); i++) {
            String deptCode = departmentList.get(i).getChdeptcode();
            if(initCode != null && initCode.equals(deptCode)){
                parentOrgId = departmentList.get(i).getChparentdept();
            }
        }
        if(initCode == null){
            parentOrgId = "0";
        }
        // 构建好的机构树
        List<DepartmentEntity> departmentListTree = buildOrgTree(departmentList, parentOrgId);
        int count = teamManagerService.queryParamKeyListNb(params);
        return   R.ok().put("teamList", teamList).put("count", count).put("departmentList", departmentListTree).put("teamType", teamType);
    }

    /**
     * 团队类型信息
     */
    @RequestMapping("/teamTypeList")
    public R teamTypeList(@RequestParam Map<String, Object> reqPara){
        Map map = new HashMap();
        map.put("busintypeid","TEAM_TYPE");
        List<Map<String, Object>> teamType = teamManagerService.queryDictDetail(map);
        return R.ok().put("teamType", teamType);
    }

    /**
     * 修改时回显人员信息
     */
    @RequestMapping("/shouwDepartmentList")
    public R shouwDepartmentList(@RequestParam Map<String, Object> params){
        String initCode = getUser().getOrgId();
        Map map = new HashMap();
        String leaderId = (String)params.get("leaderId");
        map.put("busintypeid","TEAM_TYPE");
        map.put("org","initCode");
        List<Map<String, Object>> teamType = teamManagerService.queryDictDetail(map);
//        if("00001".equals(initCode)){
////            teamType.
////        }
        //查询团队长信息
        params.put("ch_emplee_code",leaderId);
        List<TeamAndEmployeeEntity> leaderList = teamManagerDao.teamLeaderData(params);
        params.remove("ch_emplee_code");
        //查询团队关联的成员信息
        List<TeamAndEmployeeEntity> teamAndEmployeeList = teamManagerDao.queryTeamEmployee(params);

        return R.ok().put("empleeList", teamAndEmployeeList).put("leaderList",leaderList).put("teamType", teamType);
    }

    /**
     * 机构信息
     */
    @RequestMapping("/departmentList")
    public R departmentList(@RequestParam Map<String, Object> params){
        List<String> stringList = new ArrayList<>();
//        String echoOrgId = (String)params.get("echoOrgId");
        String initCode = getUser().getOrgId();
//        if(echoOrgId != null && echoOrgId !=""){
//            params.put("chdeptcode",echoOrgId);
//            stringList = teamManagerService.findOrgId(params);
//            params.clear();
//        }
        params.put("initCode",initCode);
        List<DepartmentEntity> departmentList = teamManagerService.departmentList(params);
        String parentOrgId = "";
        for (int i = 0; i < departmentList.size(); i++) {
            String deptCode = departmentList.get(i).getChdeptcode();
            if(initCode != null && initCode.equals(deptCode)){
                parentOrgId = departmentList.get(i).getChparentdept();
            }
        }
        if(initCode == null){
            parentOrgId = "0";
        }
        // 构建好的机构树
        List<DepartmentEntity> departmentListTree = buildOrgTree(departmentList, parentOrgId);
        return R.ok().put("departmentList", departmentListTree)/*.put("stringList", stringList)*/;
    }


    /**
     * 构建机构树
     *
     * @param menuList
     * @param pid
     * @return
     */
    private List<DepartmentEntity> buildOrgTree(List<DepartmentEntity> menuList, String pid) {
        List<DepartmentEntity> treeList = new ArrayList<>();
        menuList.forEach(menu -> {
            if (StringUtils.equals(pid, menu.getChparentdept())) {
                menu.setChildren(buildOrgTree(menuList, menu.getChdeptcode()));
                treeList.add(menu);
            }
        });
        return treeList;
    }

    /**
     * 人员信息
     */
    @RequestMapping("/empleeList")
    public R empleeList(@RequestParam Map<String, Object> reqPara){
//        String org = (String)reqPara.get("chOrgId");
//        if(org == ""){
//            reqPara.put("org", "9999999");
//        }else{
//            String[] orgIdList = org.split(",");
//            org = orgIdList[orgIdList.length-1];
//            reqPara.put("org", org);
//        }
        String initCode = getUser().getOrgId();
        reqPara.put("org", initCode);
        List<Map<String,String>> empleeList = teamManagerService.empleeList(reqPara);
        return R.ok().put("empleeList", empleeList);
    }

    /**
     * 更新或新增nb_team数据
     */
    @RequestMapping("/updateOrSaveTeamCfg")
    @SysLog("更新或新增nb_team数据")
    public R updateOrSaveTeamCfg(@RequestParam Map<String, Object> reqPara) {
        Map map = new HashMap();
        String initCode = getUser().getOrgId();//操作人机构ID
        String chParentTeamCode = "";
        String flag = (String)reqPara.get("flag");
        String chTeamCode = (String)reqPara.get("chTeamCode");
        String chTeamName = (String)reqPara.get("chTeamName");
        String empKtyp = (String)reqPara.get("empKtyp");
        String chTeamDesc = (String)reqPara.get("chTeamDesc");
//        String chOrgId = (String)reqPara.get("initCode");
//        String[] orgIdList = chOrgId.split(",");
//        chOrgId = orgIdList[orgIdList.length-1];//数组最后一个元素
//        if(orgIdList.length >= 2){
//            chParentTeamCode = orgIdList[orgIdList.length-2];
//        }else{
//            chParentTeamCode = "";
//        }
        reqPara.put("chdeptcode",initCode);
        List<String> orgIdList = teamManagerService.findOrgId(reqPara);

        if(orgIdList.size() >= 2){
            chParentTeamCode = orgIdList.get(orgIdList.size() - 2);

        }else{
            chParentTeamCode = "";
        }
//        String createUserId = (String)reqPara.get("createUserId");
        String leaderId = (String)reqPara.get("leaderId");
        String employee = (String)reqPara.get("employee");

        String[] array  = employee.split(",");//将字符串转化成数组
        List<String> list = Arrays.asList(array); //将数组转换为list集合
        List<String> arrayList= new  ArrayList<String>(list); //转换为ArrayLsit调用相关的remove方法
        if (list.contains( leaderId)){ //移除队长本身，队长可以是多个团队的队长
            arrayList.remove( leaderId );
        }
        String[] employeeList = arrayList.toArray(new String[arrayList.size()]);
        Map empTeamMap = new HashMap();
        boolean target = true;
        //查询团队长所属机构
        Map params = new HashMap();
        params.put("custMngrId",leaderId);
        String orgId = teamManagerDao.selectLeaderOrg(params);
        //查询添加机构下团队名称是否存在重名
        map.put("chOrgId",orgId);
        map.put("chTeamName",chTeamName);
        int count = teamManagerDao.checkTeamNmExist(map);
        if(!"update".equals(flag) && count > 0){
            target= false;
            return R.error(999999, "团队名称：" + chTeamName + ",已存在，请重新填写！");
        }
        for (int i = 0; i < employeeList.length; i++) {
            String empCd = employeeList[i];//员工编号
            empTeamMap.put("org", initCode);//机构编号
            empTeamMap.put("ch_emplee_code", empCd);//员工编号
            if(flag.equals("update")){
                empTeamMap.put("flag", flag);
                empTeamMap.put("chTeamCode", chTeamCode);//如果是修改，则过滤掉团队本身
            }
            List<TeamAndEmployeeEntity> teamAndEmployeeList = teamManagerDao.teamAndEmployeeList(empTeamMap);
            if(teamAndEmployeeList.size() > 0 && !("").equals(empCd)){
                String empNm = teamAndEmployeeList.get(0).getChEmpleeName();//员工名称
                String tNm = teamAndEmployeeList.get(0).getChTeamName();//所属团队名称
                target= false;
                return R.error(999999, "所选团队成员：" + empNm + ",已加入：" + tNm + "团队。请重新选择！");
            }
        }

        logger.info("updateOrSaveTeamCfg ****团队名称："  + ";chTeamName:" + chTeamName + ";empKtyp：" + empKtyp + ";chTeamDesc：" + chTeamDesc + ";chOrgId：" + initCode  + ";chParentTeamCode：" + chParentTeamCode  + ";leaderId：" + leaderId );

        if (StringUtils.isBlank(chTeamName) || StringUtils.isBlank(empKtyp) || StringUtils.isBlank(initCode) || StringUtils.isBlank(leaderId)) {
            return R.error(999999, "参数有误");
        } else {
            if(target = true) {
                return teamManagerService.updateOrSaveTeamCfg(chTeamCode, chTeamName, empKtyp, chTeamDesc, initCode,chParentTeamCode,leaderId,employee);
            }else{
                return null;
            }
        }
    }


    /**
     * 保存
     */
    @RequestMapping("/save")
    @SysLog("保存")
    public R save(@RequestBody TeamManagerEntity teamManagerEntity) {
        teamManagerService.save(teamManagerEntity);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @SysLog("修改")
    public R update(@RequestBody TeamManagerEntity teamManagerEntity) {
        teamManagerService.updateById(teamManagerEntity);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @SysLog("删除团队信息")
    public R delete(@RequestParam Map<String, Object> reqPara) {
        String chTeamCode = (String)reqPara.get("chTeamCode");
        int i = teamManagerDao.checkEmployeesExist(reqPara);
        if(i > 0){
            return R.error(999999, "该团队下面存在队员，不可删除！如果删除请将团队下面成员进行移除。");
        }else{
            teamManagerService.deleteTeamCfg(chTeamCode);
            return R.ok();
        }
    }

    @RequestMapping("/teamHislist")
    public R teamHislist(@RequestParam Map<String, Object> params) {
        String initCode = getUser().getOrgId();
        List<TeamManagerHisEntity> teamHislist = teamManagerService.queryTeamHisList(params);
        List<DepartmentEntity> departmentList = teamManagerService.departmentList(params);
        String parentOrgId = "";
        for (int i = 0; i < departmentList.size(); i++) {
            String deptCode = departmentList.get(i).getChdeptcode();
            if(initCode != null && initCode.equals(deptCode)){
                parentOrgId = departmentList.get(i).getChparentdept();
            }
        }
        // 构建好的机构树
        List<DepartmentEntity> departmentListTree = buildOrgTree(departmentList, parentOrgId);
        int count = teamManagerService.queryTeamHisListNb(params);
        return   R.ok().put("teamHislist", teamHislist).put("count", count).put("departmentList", departmentListTree);
    }

    @RequestMapping("/teamAndEmployeelist")
    public R teamAndEmployeelist(@RequestParam Map<String, Object> params) {
        List<TeamAndEmployeeEntity> teamAndEmployeelist = teamManagerService.queryTeamAndEmployeeList(params);
        int count = teamManagerService.queryTeamAndEmployeeNb(params);
        return   R.ok().put("teamAndEmployeelist", teamAndEmployeelist).put("count", count);
    }

    /**
     * 同步大数据平台团队数据
     * @param params
     * @return
     */
    @RequestMapping("/batchTeamData")
    public R batchTeamData(@RequestParam Map<String, Object> params) {
        //团队编号
        String chTeamCode = (String)params.get("chTeamCode");
        //查询营销人员考核基础信息表数据
        List<Map<String,Object>> baseInfoList = scheduleJobDao.selectBaseInfoDate(params);
        Connection conn = PgsqlUtil.getConn();
        PreparedStatement pre = null; //预编译
        ResultSet rs = null;
        long TimeStart = System.currentTimeMillis();//获取当前时间
        //先执行删除表数据操作
        String sqlOne = "delete from npmm.C_MNG_CHECK_BASE_INFO where ch_team_code = " + chTeamCode;
        //执行插入数据操作
        String sqlTwo = "INSERT\n" +
                "INTO\n" +
                "    NPMM.C_MNG_CHECK_BASE_INFO\n" +
                "    (\n" +
                "        YR,\n" +
                "        ORG_ID,\n" +
                "        CUST_MNGR_ID,\n" +
                "        CUST_MNGR_NM,\n" +
                "        TEAM_ID,\n" +
                "        TEAM_NM,\n" +
                "        CH_TEAM_CODE,\n" +
                "        CH_TEAM_NAME,\n" +
                "        IN_TM,\n" +
                "        DEPOSIT_DT_AVG_BAL_1,\n" +
                "        DEPOSIT_DT_AVG_BAL_2,\n" +
                "        DEPOSIT_DT_AVG_BAL_3,\n" +
                "        DEPOSIT_DT_AVG_BAL_4,\n" +
                "        DEPOSIT_DT_AVG_BAL_5,\n" +
                "        DEPOSIT_DT_AVG_BAL_6,\n" +
                "        DEPOSIT_DT_AVG_BAL_7,\n" +
                "        DEPOSIT_DT_AVG_BAL_8,\n" +
                "        DEPOSIT_DT_AVG_BAL_9,\n" +
                "        DEPOSIT_DT_AVG_BAL_10,\n" +
                "        DEPOSIT_DT_AVG_BAL_11,\n" +
                "        DEPOSIT_DT_AVG_BAL_12,\n" +
                "        LOAN_DT_AVG_BAL_1,\n" +
                "        LOAN_DT_AVG_BAL_2,\n" +
                "        LOAN_DT_AVG_BAL_3,\n" +
                "        LOAN_DT_AVG_BAL_4,\n" +
                "        LOAN_DT_AVG_BAL_5,\n" +
                "        LOAN_DT_AVG_BAL_6,\n" +
                "        LOAN_DT_AVG_BAL_7,\n" +
                "        LOAN_DT_AVG_BAL_8,\n" +
                "        LOAN_DT_AVG_BAL_9,\n" +
                "        LOAN_DT_AVG_BAL_10,\n" +
                "        LOAN_DT_AVG_BAL_11,\n" +
                "        LOAN_DT_AVG_BAL_12,\n" +
                "        NET_INCM_1,\n" +
                "        NET_INCM_2,\n" +
                "        NET_INCM_3,\n" +
                "        NET_INCM_4,\n" +
                "        NET_INCM_5,\n" +
                "        NET_INCM_6,\n" +
                "        NET_INCM_7,\n" +
                "        NET_INCM_8,\n" +
                "        NET_INCM_9,\n" +
                "        NET_INCM_10,\n" +
                "        NET_INCM_11,\n" +
                "        NET_INCM_12,\n" +
                "        DEPOSIT_NEW_OPEN_NUM_1,\n" +
                "        DEPOSIT_NEW_OPEN_NUM_2,\n" +
                "        DEPOSIT_NEW_OPEN_NUM_3,\n" +
                "        DEPOSIT_NEW_OPEN_NUM_4,\n" +
                "        DEPOSIT_NEW_OPEN_NUM_5,\n" +
                "        DEPOSIT_NEW_OPEN_NUM_6,\n" +
                "        DEPOSIT_NEW_OPEN_NUM_7,\n" +
                "        DEPOSIT_NEW_OPEN_NUM_8,\n" +
                "        DEPOSIT_NEW_OPEN_NUM_9,\n" +
                "        DEPOSIT_NEW_OPEN_NUM_10,\n" +
                "        DEPOSIT_NEW_OPEN_NUM_11,\n" +
                "        DEPOSIT_NEW_OPEN_NUM_12,\n" +
                "        LOAN_MNG_CUST_CUST_NUM_1,\n" +
                "        LOAN_MNG_CUST_CUST_NUM_2,\n" +
                "        LOAN_MNG_CUST_CUST_NUM_3,\n" +
                "        LOAN_MNG_CUST_CUST_NUM_4,\n" +
                "        LOAN_MNG_CUST_CUST_NUM_5,\n" +
                "        LOAN_MNG_CUST_CUST_NUM_6,\n" +
                "        LOAN_MNG_CUST_CUST_NUM_7,\n" +
                "        LOAN_MNG_CUST_CUST_NUM_8,\n" +
                "        LOAN_MNG_CUST_CUST_NUM_9,\n" +
                "        LOAN_MNG_CUST_CUST_NUM_10,\n" +
                "        LOAN_MNG_CUST_CUST_NUM_11,\n" +
                "        LOAN_MNG_CUST_CUST_NUM_12,\n" +
                "        NET_INCM_BASE_1,\n" +
                "        NET_INCM_BASE_2,\n" +
                "        NET_INCM_BASE_3,\n" +
                "        NET_INCM_BASE_4,\n" +
                "        NET_INCM_BASE_5,\n" +
                "        NET_INCM_BASE_6,\n" +
                "        NET_INCM_BASE_7,\n" +
                "        NET_INCM_BASE_8,\n" +
                "        NET_INCM_BASE_9,\n" +
                "        NET_INCM_BASE_10,\n" +
                "        NET_INCM_BASE_11,\n" +
                "        NET_INCM_BASE_12,\n" +
                "        OPERATEDATE,\n" +
                "        OPERATOR,\n" +
                "        REMARK,\n" +
                "        MD5\n" +
                "    )\n" +
                "    VALUES\n" +
                "    (\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?\n" +
                "    )";
        try {
            // 取消自动提交（由于要进行多次操作）
            conn.setAutoCommit(false);
            pre = conn.prepareStatement(sqlOne);
            pre.executeUpdate();
            pre = conn.prepareStatement(sqlTwo);
            for (int i = 0; i <baseInfoList.size() ; i++) {
                pre.setString(1,baseInfoList.get(i).get("YR").toString());
                pre.setString(2,baseInfoList.get(i).get("ORG_ID").toString());
                pre.setString(3,baseInfoList.get(i).get("CUST_MNGR_ID").toString());
                pre.setString(4,baseInfoList.get(i).get("CUST_MNGR_NM").toString());
                pre.setString(5,baseInfoList.get(i).get("TEAM_ID").toString());
                pre.setString(6,baseInfoList.get(i).get("TEAM_NM").toString());
                pre.setString(7,baseInfoList.get(i).get("CH_TEAM_CODE").toString());
                pre.setString(8,baseInfoList.get(i).get("CH_TEAM_NAME").toString());
                pre.setString(9,baseInfoList.get(i).get("IN_TM").toString());
                pre.setBigDecimal(10,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_DT_AVG_BAL_1").toString())));
                pre.setBigDecimal(11,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_DT_AVG_BAL_2").toString())));
                pre.setBigDecimal(12,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_DT_AVG_BAL_3").toString())));
                pre.setBigDecimal(13,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_DT_AVG_BAL_4").toString())));
                pre.setBigDecimal(14,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_DT_AVG_BAL_5").toString())));
                pre.setBigDecimal(15,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_DT_AVG_BAL_6").toString())));
                pre.setBigDecimal(16,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_DT_AVG_BAL_7").toString())));
                pre.setBigDecimal(17,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_DT_AVG_BAL_8").toString())));
                pre.setBigDecimal(18,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_DT_AVG_BAL_9").toString())));
                pre.setBigDecimal(19,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_DT_AVG_BAL_10").toString())));
                pre.setBigDecimal(20,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_DT_AVG_BAL_11").toString())));
                pre.setBigDecimal(21,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_DT_AVG_BAL_12").toString())));
                pre.setBigDecimal(22,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_DT_AVG_BAL_1").toString())));
                pre.setBigDecimal(23,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_DT_AVG_BAL_2").toString())));
                pre.setBigDecimal(24,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_DT_AVG_BAL_3").toString())));
                pre.setBigDecimal(25,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_DT_AVG_BAL_4").toString())));
                pre.setBigDecimal(26,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_DT_AVG_BAL_5").toString())));
                pre.setBigDecimal(27,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_DT_AVG_BAL_6").toString())));
                pre.setBigDecimal(28,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_DT_AVG_BAL_7").toString())));
                pre.setBigDecimal(29,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_DT_AVG_BAL_8").toString())));
                pre.setBigDecimal(30,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_DT_AVG_BAL_9").toString())));
                pre.setBigDecimal(31,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_DT_AVG_BAL_10").toString())));
                pre.setBigDecimal(32,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_DT_AVG_BAL_11").toString())));
                pre.setBigDecimal(33,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_DT_AVG_BAL_12").toString())));
                pre.setBigDecimal(34,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_1").toString())));
                pre.setBigDecimal(35,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_2").toString())));
                pre.setBigDecimal(36,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_3").toString())));
                pre.setBigDecimal(37,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_4").toString())));
                pre.setBigDecimal(38,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_5").toString())));
                pre.setBigDecimal(39,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_6").toString())));
                pre.setBigDecimal(40,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_7").toString())));
                pre.setBigDecimal(41,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_8").toString())));
                pre.setBigDecimal(42,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_9").toString())));
                pre.setBigDecimal(43,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_10").toString())));
                pre.setBigDecimal(44,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_11").toString())));
                pre.setBigDecimal(45,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_12").toString())));
                pre.setBigDecimal(46,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_NEW_OPEN_NUM_1").toString())));
                pre.setBigDecimal(47,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_NEW_OPEN_NUM_2").toString())));
                pre.setBigDecimal(48,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_NEW_OPEN_NUM_3").toString())));
                pre.setBigDecimal(49,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_NEW_OPEN_NUM_4").toString())));
                pre.setBigDecimal(50,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_NEW_OPEN_NUM_5").toString())));
                pre.setBigDecimal(51,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_NEW_OPEN_NUM_6").toString())));
                pre.setBigDecimal(52,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_NEW_OPEN_NUM_7").toString())));
                pre.setBigDecimal(53,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_NEW_OPEN_NUM_8").toString())));
                pre.setBigDecimal(54,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_NEW_OPEN_NUM_9").toString())));
                pre.setBigDecimal(55,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_NEW_OPEN_NUM_10").toString())));
                pre.setBigDecimal(56,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_NEW_OPEN_NUM_11").toString())));
                pre.setBigDecimal(57,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("DEPOSIT_NEW_OPEN_NUM_12").toString())));
                pre.setBigDecimal(58,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_MNG_CUST_CUST_NUM_1").toString())));
                pre.setBigDecimal(59,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_MNG_CUST_CUST_NUM_2").toString())));
                pre.setBigDecimal(60,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_MNG_CUST_CUST_NUM_3").toString())));
                pre.setBigDecimal(61,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_MNG_CUST_CUST_NUM_4").toString())));
                pre.setBigDecimal(62,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_MNG_CUST_CUST_NUM_5").toString())));
                pre.setBigDecimal(63,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_MNG_CUST_CUST_NUM_6").toString())));
                pre.setBigDecimal(64,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_MNG_CUST_CUST_NUM_7").toString())));
                pre.setBigDecimal(65,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_MNG_CUST_CUST_NUM_8").toString())));
                pre.setBigDecimal(66,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_MNG_CUST_CUST_NUM_9").toString())));
                pre.setBigDecimal(67,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_MNG_CUST_CUST_NUM_10").toString())));
                pre.setBigDecimal(68,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_MNG_CUST_CUST_NUM_11").toString())));
                pre.setBigDecimal(69,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("LOAN_MNG_CUST_CUST_NUM_12").toString())));
                pre.setBigDecimal(70,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_BASE_1").toString())));
                pre.setBigDecimal(71,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_BASE_2").toString())));
                pre.setBigDecimal(72,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_BASE_3").toString())));
                pre.setBigDecimal(73,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_BASE_4").toString())));
                pre.setBigDecimal(74,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_BASE_5").toString())));
                pre.setBigDecimal(75,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_BASE_6").toString())));
                pre.setBigDecimal(76,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_BASE_7").toString())));
                pre.setBigDecimal(77,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_BASE_8").toString())));
                pre.setBigDecimal(78,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_BASE_9").toString())));
                pre.setBigDecimal(79,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_BASE_10").toString())));
                pre.setBigDecimal(80,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_BASE_11").toString())));
                pre.setBigDecimal(81,BigDecimal.valueOf(Double.valueOf(baseInfoList.get(i).get("NET_INCM_BASE_12").toString())));
                pre.setString(82,baseInfoList.get(i).get("OPERATEDATE").toString());
                pre.setString(83,baseInfoList.get(i).get("OPERATOR").toString());
                pre.setString(84,baseInfoList.get(i).get("REMARK").toString());
                pre.setString(85,baseInfoList.get(i).get("MD5").toString());
                pre.addBatch();//批量插入
            }
            pre.executeBatch();
            conn.commit();
            pre.clearBatch();
        } catch (SQLException e) {
            e.printStackTrace();
            // 发生异常，回滚
            try {
                conn.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }finally {
            PgsqlUtil.close(conn, pre, rs);
        }
        long TimeStop = System.currentTimeMillis();
        System.out.println("总耗时："+ (TimeStop - TimeStart));
        return R.ok();
    }
}
