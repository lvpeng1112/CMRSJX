/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.team.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.sys.entity.SysUserEntity;
import com.beyondsoft.modules.team.entity.*;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 团队管理
 *
 */
@Mapper
public interface TeamManagerDao extends BaseMapper<TeamManagerEntity> {

	public List<Map<String, Object>> queryDictDetail(Map map);

	public List<String> findOrgId(Map reqPara);

	public List<String> findEmployeeId(Map reqPara);

	public List<String> findEmployeeNm(Map reqPara);

	public int queryTeamManagerDataListNb(Map reqPara);
	/**
	 * 查询全部配置
	 * @return
	 */
	List<TeamManagerEntity> queryTeamManagerDataList(Map reqPara);

	/**
	 * 查询机构信息
	 */
	public List<DepartmentEntity> departmentList(Map reqPara);

	/**
	 * 查询员工信息
	 */
	public List<Map<String,String>> empleeList(Map reqPara);

	/**
	 * 查询员工与团队绑定信息
	 */
	public List<TeamAndEmployeeEntity> teamAndEmployeeList(Map reqPara);

	/**
	 * 查询团队长数据信息
	 */
	public List<TeamAndEmployeeEntity> teamLeaderData(Map reqPara);

	/**
	 * 根据用户名编号，查询用户名称
	 */
	SysUserEntity queryByUserId(Long userId);

	/**
	 * 根据主键查询
	 * @return
	 */
	TeamManagerEntity queryParamKeyById(Map reqPara);

	/**
	 * 保存
	 * @return
	 */
	public void saveTeamManagerData(Map reqPara);

	/**
	 * 保存团队员工关联表数据
	 * @return
	 */
	public void saveEmployTeam(Map reqPara);


	/**
	 * 更新
	 * @return
	 */
	void updateTeamManagerData(Map reqPara);

	/**
	 * 删除
	 * @return
	 */
	void deleteParamKey(Map reqPara);

	int  checkEmployeesExist(Map reqPara);

	void deleteTeamAndEmployee(Map reqPara);

	public int queryTeamHisListNb(Map reqPara);

	List<TeamManagerHisEntity> queryTeamHisList(Map reqPara);

	/**
	 * 保存团队变更历史数据
	 * @return
	 */
	void saveTeamManagerHisData(Map reqPara);

	public int queryTeamAndEmployeeNb(Map reqPara);

	List<TeamAndEmployeeEntity> queryTeamAndEmployeeList(Map reqPara);


	/**
	 *根据团队编号获取团队成员编号 cl
	 * @return
	 */
	List<TeamAndEmployeeEntity> selectTeamByTeamCode(String chTeamCode);
	/**
	 * 根据机构获取团队
	 * @return
	 */
	List<TeamManagerEntity> selectTeamByOrg(Map reqPara);

	/**
	 * 查询添加机构下团队名称是否存在重复
	 */
	int  checkTeamNmExist(Map reqPara);

	/**
	 * 查询团队人员绑定信息
	 */
	public List<TeamAndEmployeeEntity> queryTeamEmployee(Map reqPara);

	/**
	 * 查询团队长所属机构
	 */
	public String selectLeaderOrg(Map map);
}
