/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.team.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 团队实体类
 *
 */
@Data
@TableName("nb_team")
public class TeamManagerEntity  implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/**
	 * 团队编号
	 */
	@TableId
	private String chTeamCode;

	/**
	 * 团队名称
	 */
	@NotBlank(message="团队名称不能为空")
	private String chTeamName;

	/**
	 * 团队所属考核类型
	 */
	@NotBlank(message="团队所属考核类型不能为空")
	private String empKtyp;
	
	/**
	 * 创建者ID
	 */
	private String createUserId;

	/**
	 * 团队描述
	 */
	private String chTeamDesc;

	/**
	 * 团队所属机构
	 */
	@NotBlank(message="团队所属机构不能为空")
	private String chOrgId;

	/**
	 * 团队所属机构中文名称
	 */
	private String chdeptname;

	/**
	 * 上级团队编号
	 */
	private String chParentTeamCode;


	/**
	 * 团队长ID
	 */
	private String leaderId;

	/**
	 * 团队长名称
	 */
	private String leaderName;

	/**
	 * 团队成员中文名称
	 */
	private String empNm;
	

	/**
	 * 最后修改日期
	 */
	private String chLastModify;

	/**
	 * 最后修改人
	 */
	private String chLastModifier;


}
