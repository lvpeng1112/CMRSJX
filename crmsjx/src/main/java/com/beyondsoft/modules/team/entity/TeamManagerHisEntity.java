/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.team.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 团队历史实体类
 *
 */
@Data
@TableName("nb_team_his")
public class TeamManagerHisEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	private String order;
	private String chTeamCode;
	private String chTeamName;
	private String chOrgId;
	private String chdeptname;
	private String chUpdateKtyp;
	private String chEmpleeCodeBefore;
	private String chEmpleeNameBefore;
	private String chEmpleeCodeAfter;
	private String chEmpleeNameAfter;
	private String chLastModify;
	private String chLastModifier;
}
