package com.beyondsoft.modules.team.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;


/**
 *
 *
 * @author luochao
 */
@Data
@TableName("nb_team_emplee")
public class TeamAndEmployeeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	private String chTeamCode;
	private String chTeamName;
	private String chEmpleeCode;
	private String chEmpleeName;
	private String chLastModify;
	private String chLastModifier;
}
