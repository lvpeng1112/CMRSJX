/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.team.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.common.utils.PageUtils;
import com.beyondsoft.common.utils.Query;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.common.utils.ShiroUtils;
import com.beyondsoft.modules.sys.entity.SysUserEntity;
import com.beyondsoft.modules.team.dao.TeamManagerDao;
import com.beyondsoft.modules.team.entity.*;
import com.beyondsoft.modules.team.service.TeamManagerService;
import com.beyondsoft.utils.StrUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 团队
 *
 */
@Service("teamManagerService")
@Transactional
public class TeamManagerServiceImpl extends ServiceImpl<TeamManagerDao,TeamManagerEntity> implements TeamManagerService {
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired(required = false)
	private TeamManagerDao teamManagerDao;

	public SysUserEntity getUser() {
		return (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
	}

	public Long getUserId() {
		return getUser().getUserId();
	}

	@Override
	public List<Map<String, Object>> queryDictDetail(Map map) {
		return teamManagerDao.queryDictDetail(map);
	}

	@Override
	public List<String> findOrgId(Map reqPara) {
		String chdeptcode = (String)reqPara.get("chdeptcode");
		reqPara.put("chdeptcode",chdeptcode);
		return teamManagerDao.findOrgId(reqPara);
	}

	@Override
	public List<String> findEmployeeId(Map reqPara) {
		String chTeamCode = (String)reqPara.get("chTeamCode");
		reqPara.put("chTeamCode",chTeamCode);
		return teamManagerDao.findEmployeeId(reqPara);
	}

	@Override
	public List<DepartmentEntity> departmentList(Map reqPara) {
		List<DepartmentEntity> departmentList = teamManagerDao.departmentList(reqPara);
		return departmentList;
	}

	@Override
	public List<Map<String,String>> empleeList(Map reqPara) {
		List<Map<String,String>> empleeList = teamManagerDao.empleeList(reqPara);
		return empleeList;
	}

	@Override
	public int queryParamKeyListNb(Map reqPara) {
		String chOrgId = (String)reqPara.get("chOrgId");
		reqPara.put("chOrgId", chOrgId);
		return teamManagerDao.queryTeamManagerDataListNb(reqPara);
	}

	@Override
	public List<TeamManagerEntity> queryParamKeyList(Map<String, Object> params) {
		String chOrgId = (String)params.get("chOrgId");
		int pageNo = Integer.parseInt((String) params.get("page"));
		int pageCount = Integer.parseInt((String)params.get("limit"));
		int offset = ((pageNo - 1) * pageCount);
		params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
		params.put("offset", offset);
		params.put("chOrgId", chOrgId);
		List<TeamManagerEntity> teamList = teamManagerDao.queryTeamManagerDataList(params);
		return teamList;
	}

	@Override
	public PageUtils queryPage(Map<String, Object> params) {
		String chTeamCode = (String)params.get("chTeamCode");
		String chTeamName = (String)params.get("chTeamName");
		String empKtyp = (String)params.get("empKtyp");
		String chOrgId = (String)params.get("chOrgId");
		IPage<TeamManagerEntity> page = this.page(
				new Query<TeamManagerEntity>().getPage(params),
				new QueryWrapper<TeamManagerEntity>()
						.like(StringUtils.isNotBlank(chTeamCode),"ch_team_code", chTeamCode)
						.eq(StringUtils.isNotBlank(chTeamName),"ch_team_name", chTeamName)
						.eq(StringUtils.isNotBlank(empKtyp),"emp_ktyp", empKtyp)
						.eq(StringUtils.isNotBlank(chOrgId),"ch_org_id", chOrgId)
		);

		List<TeamManagerEntity> pageList = page.getRecords();


		IPage<TeamManagerEntity> page1 = page.setRecords(pageList);

		return new PageUtils(page1);
	}

	@Override
	public R updateOrSaveTeamCfg(String chTeamCode, String chTeamName, String empKtyp, String chTeamDesc, String initCode, String chParentTeamCode, String leaderId, String employee) {
		List<String> afterTeamEmployNm = new ArrayList<>();
		Map reqPara = new HashMap();
		Map empTeamMap = new HashMap();
		Map mapHis = new HashMap();
		Map leaderMap = new HashMap();
		//查询团队长所属机构
		Map params = new HashMap();
		params.put("custMngrId",leaderId);
		String orgId = teamManagerDao.selectLeaderOrg(params);
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date(System.currentTimeMillis());
		SysUserEntity sysUserEntity = teamManagerDao.queryByUserId(getUserId());
		String[] employeeList = employee.split(",");
		List<String> asList = Arrays.asList(employeeList);//将员工编号进行转化成集合，为了进行团队变更比较
		try {
			reqPara.put("chTeamCode", chTeamCode);
			TeamManagerEntity teamCfgEntity = teamManagerDao.queryParamKeyById(reqPara);
			reqPara.put("chTeamCode", chTeamCode);//团队编号
			reqPara.put("chTeamName", chTeamName);//团队名称
			reqPara.put("empKtyp", empKtyp);//团队所属考核类型
			reqPara.put("leaderId", leaderId);//团队长id
			reqPara.put("createUserId",ShiroUtils.getUserEntity().getName());//登记人
			reqPara.put("chLastModifier", ShiroUtils.getUserEntity().getName());//最后修改人
			reqPara.put("chLastModify", formatter.format(date));//最后修改时间
			reqPara.put("chOrgId", orgId);//团队所属机构
			reqPara.put("chParentTeamCode", chParentTeamCode);//上级团队编号
			reqPara.put("chTeamDesc", chTeamDesc);//团队描述

			if(teamCfgEntity==null){
				//保存
				chTeamCode = StrUtil.genRandomNum(10);
				reqPara.put("chTeamCode", chTeamCode);//团队编号
				teamManagerDao.saveTeamManagerData(reqPara);
				for (int i = 0; i < employeeList.length; i++) {
					String empCd = employeeList[i];
					empTeamMap.put("org", initCode);//机构编号
					empTeamMap.put("ch_emplee_code", empCd);//员工编号
					List<Map<String, String>> mapList = teamManagerDao.empleeList(empTeamMap);
					String empNm = mapList.get(0).get("EMPNM");//员工名称
					empTeamMap.put("chTeamCode", chTeamCode);//团队编号
					empTeamMap.put("ch_emplee_name", empNm);//员工姓名
					empTeamMap.put("chLastModifier", ShiroUtils.getUserEntity().getName());//最后修改人
					empTeamMap.put("chLastModify", formatter.format(date));//最后修改时间
					if(!"".equals(empCd)){
						teamManagerDao.saveEmployTeam(empTeamMap);//执行新增团队员工关联表新增操作
					}
				}
			}else{
				//更新团队信息
				teamManagerDao.updateTeamManagerData(reqPara);
				mapHis.put("chTeamCode", chTeamCode);//团队编号
				mapHis.put("chTeamName", chTeamName);//团队名称
				mapHis.put("chOrgId", orgId);//团队所属机构
				mapHis.put("chLastModifier", ShiroUtils.getUserEntity().getName());//调整人
				mapHis.put("chLastModify", formatter.format(date));//调整时间
				String beforeLeaderId = teamCfgEntity.getLeaderId().trim();//变更前团队长编号
				List<String> employeeId = teamManagerDao.findEmployeeId(reqPara);//变更前团队成员编号
				String employeeIdToString = StringUtils.join(employeeId, ",");//将查询出来的成员编号集合转化成字符串
				List<String> employeeNm = teamManagerDao.findEmployeeNm(reqPara);//变更前团队成员名称
				String employeeNmToString = StringUtils.join(employeeNm, ",");//将查询出来的成员姓名集合转化成字符串
				//查询变更前团队长姓名以及变更后团队长姓名
				leaderMap.put("org", initCode);//机构编号
				leaderMap.put("ch_emplee_code", beforeLeaderId);//变更前团队长编号
				List<Map<String, String>> leaderBeforeData = teamManagerDao.empleeList(leaderMap);
				String leaderBeforeNm = leaderBeforeData.get(0).get("EMPNM");//变更前团队长姓名
				leaderMap.clear();
				leaderMap.put("org", initCode);//机构编号
				leaderMap.put("ch_emplee_code", leaderId);//变更后团队长编号
				List<Map<String, String>> leaderAfterData = teamManagerDao.empleeList(leaderMap);
				String leaderNmAfter = leaderAfterData.get(0).get("EMPNM");//变更后团队长姓名
				//查询变更后团队成员姓名
				for (int i = 0; i < employeeList.length; i++) {
					String empCd = employeeList[i];
					empTeamMap.put("org", initCode);//机构编号
					empTeamMap.put("ch_emplee_code", empCd);//员工编号
					if(!"".equals(empCd)){
						List<Map<String, String>> csrMgrNameListEntities = teamManagerDao.empleeList(empTeamMap);
						String empNm = csrMgrNameListEntities.get(0).get("EMPNM").trim();//员工名称
						afterTeamEmployNm.add(empNm);
					}
				}
				String afterTeamEmployNmToString = StringUtils.join(afterTeamEmployNm, ",");//将成员姓名集合转化成字符串(变更后团队成员姓名)
				if(!beforeLeaderId.equals(leaderId) && employeeIdToString.replace(" ", "").equals(employee)){
					mapHis.put("chUpdateKtyp", "团队长变更");//团队变更类型
					mapHis.put("chEmpleeCodeBefore", beforeLeaderId);//变更前团队长编号
					mapHis.put("chEmpleeNameBefore", leaderBeforeNm);///变更前团队长名称
					mapHis.put("chEmpleeCodeAfter", leaderId);//变更后团队长编号
					mapHis.put("chEmpleeNameAfter", leaderNmAfter);//变更后团队长姓名
					teamManagerDao.saveTeamManagerHisData(mapHis);//保存变更历史记录
				}else if(beforeLeaderId.equals(leaderId) && !employeeIdToString.replace(" ", "").equals(employee)){
					if(employeeId.size() > asList.size() || ("").equals(employee)){
						mapHis.put("chUpdateKtyp", "删除成员");//团队变更类型
						mapHis.put("chEmpleeCodeBefore", employeeIdToString.replace(" ", ""));//变更前团队成员编号
						mapHis.put("chEmpleeNameBefore", employeeNmToString.replace(" ", ""));///变更前团队成员名称
						mapHis.put("chEmpleeCodeAfter", employee);//变更后团队成员编号
						mapHis.put("chEmpleeNameAfter", afterTeamEmployNmToString);///变更后团队成员名称
						teamManagerDao.saveTeamManagerHisData(mapHis);//保存变更历史记录
					}else {
						mapHis.put("chUpdateKtyp", "增加成员");//团队变更类型
						mapHis.put("chEmpleeCodeBefore", employeeIdToString.replace(" ", ""));//变更前团队成员编号
						mapHis.put("chEmpleeNameBefore", employeeNmToString.replace(" ", ""));///变更前团队成员名称
						mapHis.put("chEmpleeCodeAfter", employee);//变更后团队成员编号
						mapHis.put("chEmpleeNameAfter", afterTeamEmployNmToString);///变更后团队成员名称
						teamManagerDao.saveTeamManagerHisData(mapHis);//保存变更历史记录
					}
				}else if(!beforeLeaderId.equals(leaderId) && !employeeIdToString.replace(" ", "").equals(employee)){
					//团队长与成员同时调整，增加两条数据
					mapHis.put("chUpdateKtyp", "团队长变更");//团队变更类型
					mapHis.put("chEmpleeCodeBefore", beforeLeaderId);//变更前团队长编号
					mapHis.put("chEmpleeNameBefore", leaderBeforeNm);///变更前团队长名称
					mapHis.put("chEmpleeCodeAfter", leaderId);//变更后团队长编号
					mapHis.put("chEmpleeNameAfter", leaderNmAfter);//变更后团队长姓名
					teamManagerDao.saveTeamManagerHisData(mapHis);//保存变更历史记录
					if(employeeId.size() > asList.size()){
						mapHis.put("chUpdateKtyp", "删除成员");//团队变更类型
						mapHis.put("chEmpleeCodeBefore", employeeIdToString.replace(" ", ""));//变更前团队成员编号
						mapHis.put("chEmpleeNameBefore", employeeNmToString.replace(" ", ""));///变更前团队成员名称
						mapHis.put("chEmpleeCodeAfter", employee);//变更后团队成员编号
						mapHis.put("chEmpleeNameAfter", afterTeamEmployNmToString);///变更后团队成员名称
						teamManagerDao.saveTeamManagerHisData(mapHis);//保存变更历史记录
					}else {
						mapHis.put("chUpdateKtyp", "增加成员");//团队变更类型
						mapHis.put("chEmpleeCodeBefore", employeeIdToString.replace(" ", ""));//变更前团队成员编号
						mapHis.put("chEmpleeNameBefore", employeeNmToString.replace(" ", ""));///变更前团队成员名称
						mapHis.put("chEmpleeCodeAfter", employee);//变更后团队成员编号
						mapHis.put("chEmpleeNameAfter", afterTeamEmployNmToString);///变更后团队成员名称
						teamManagerDao.saveTeamManagerHisData(mapHis);//保存变更历史记录
					}
				}
				//全增全删团队员工关联表数据
				teamManagerDao.deleteTeamAndEmployee(reqPara);
				for (int i = 0; i < employeeList.length; i++) {
					String empCd = employeeList[i];
					empTeamMap.put("org", initCode);//机构编号
					empTeamMap.put("ch_emplee_code", empCd);//员工编号
					List<Map<String, String>> csrMgrNameListEntities = teamManagerDao.empleeList(empTeamMap);
					String empNm = csrMgrNameListEntities.get(0).get("EMPNM");//员工名称
					empTeamMap.put("chTeamCode", chTeamCode);//团队编号
					empTeamMap.put("ch_emplee_name", empNm);//员工姓名
					empTeamMap.put("chLastModifier", ShiroUtils.getUserEntity().getName());//最后修改人
					empTeamMap.put("chLastModify", formatter.format(date));//最后修改时间
					if(!"".equals(empCd)){
						teamManagerDao.saveEmployTeam(empTeamMap);//执行新增团队员工关联表新增操作
					}
				}
			}
		}catch (Exception e){
			logger.info("updateOrSaveTeamCfg error  is", e);
			return R.error(999999, e.getMessage());
		}
		return R.ok();
	}

	@Override
	public R deleteTeamCfg(String chTeamCode) {
		Map reqPara = new HashMap();
		try {
			reqPara.put("chTeamCode", chTeamCode);
			teamManagerDao.deleteParamKey(reqPara);
		}catch (Exception e){
			logger.info("deleteTeamCfg error  is", e);
			return R.error(999999, e.getMessage());
		}
		return R.ok();
	}

	@Override
	public int queryTeamHisListNb(Map reqPara) {
		return teamManagerDao.queryTeamHisListNb(reqPara);
	}

	@Override
	public List<TeamManagerHisEntity> queryTeamHisList(Map reqPara) {
		int pageNo = Integer.parseInt((String) reqPara.get("page"));
		int pageCount = Integer.parseInt((String)reqPara.get("limit"));
		int offset = ((pageNo - 1) * pageCount);
		reqPara.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
		reqPara.put("offset", offset);
		List<TeamManagerHisEntity> teamHisList = teamManagerDao.queryTeamHisList(reqPara);
		return teamHisList;
	}

	@Override
	public int queryTeamAndEmployeeNb(Map reqPara) {
		return teamManagerDao.queryTeamAndEmployeeNb(reqPara);
	}

	@Override
	public List<TeamAndEmployeeEntity> queryTeamAndEmployeeList(Map params) {
		int pageNo = Integer.parseInt((String) params.get("page"));
		int pageCount = Integer.parseInt((String)params.get("limit"));
		int offset = ((pageNo - 1) * pageCount);
		params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
		params.put("offset", offset);
		List<TeamAndEmployeeEntity> teamAndEmployeeList = teamManagerDao.queryTeamAndEmployeeList(params);
		return teamAndEmployeeList;
	}
}
