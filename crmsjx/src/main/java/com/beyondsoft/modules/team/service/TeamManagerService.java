/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.team.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.common.utils.PageUtils;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.team.entity.*;

import java.util.List;
import java.util.Map;


/**
 * 团队管理
 *
 * @author
 */
public interface TeamManagerService extends IService<TeamManagerEntity> {

	public List<Map<String, Object>> queryDictDetail(Map map);

	public List<String> findOrgId(Map reqPara);

	public List<String> findEmployeeId(Map reqPara);

	/**
	 * 查询机构信息
	 */
	public List<DepartmentEntity> departmentList(Map reqPara);

	public List<Map<String,String>> empleeList(Map reqPara);

	public int queryParamKeyListNb(Map reqPara);

	public List<TeamManagerEntity> queryParamKeyList(Map<String, Object> params);

	PageUtils queryPage(Map<String, Object> params);

	/**
	 * 更新或新增nb_team参数
	 *
	 * @return
	 */
	public R updateOrSaveTeamCfg(String chTeamCode, String chTeamName, String empKtyp, String chTeamDesc, String chOrgId, String chParentTeamCode, String leaderId, String employee);

	/**
	 * 删除nb_team参数
	 *
	 * @return
	 */
	public R deleteTeamCfg(String chTeamCode);

    public int queryTeamHisListNb(Map reqPara);

    List<TeamManagerHisEntity> queryTeamHisList(Map reqPara);

	public int queryTeamAndEmployeeNb(Map reqPara);

	List<TeamAndEmployeeEntity> queryTeamAndEmployeeList(Map reqPara);

}
