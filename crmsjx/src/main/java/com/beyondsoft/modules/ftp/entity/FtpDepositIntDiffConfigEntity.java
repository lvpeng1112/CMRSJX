package com.beyondsoft.modules.ftp.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
@TableName("npmm.c_ftp_deposit_int_diff_config")
public class FtpDepositIntDiffConfigEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    //机构号
    private String branchId;
    //机构名称
    private String branchNm;
    //客户类型（P-个人、C-单位）
    private String custType;
    //客户类型名称（P-个人、C-单位）
    private String custTypeNm;
    //业务类型（D-存款、C-贷款）
    private String bizType;
    //业务类型名称
    private String bizTypeNm;

    private String stdProdCd;

    private String stdProdNm;
//    @JsonSerialize(using= ToStringSerializer.class)
    private Double ftpPrice;

    //期限
    private String term;

    private String startDt;

    private String endDt;

    private String operators;

    private String operateDate;

    private String stdProdSign;

    private String target;

    private String flag;

    private String newEndDt;
}
