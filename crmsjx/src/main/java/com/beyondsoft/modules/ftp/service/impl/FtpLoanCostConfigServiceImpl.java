package com.beyondsoft.modules.ftp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.datasource.annotation.DataSource;
import com.beyondsoft.modules.ftp.dao.FtpLoanConstConfigDao;
import com.beyondsoft.modules.ftp.entity.FtpDepositIntDiffConfigEntity;
import com.beyondsoft.modules.ftp.entity.FtpLoanCostConfigEntity;
import com.beyondsoft.modules.ftp.service.FtpLoanCostConfigService;
import com.beyondsoft.modules.sys.entity.SysUserEntity;
import lombok.SneakyThrows;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 *ftp存贷款详情
 */
@Service("ftpLoanCostConfigService")
public class FtpLoanCostConfigServiceImpl extends ServiceImpl<FtpLoanConstConfigDao, FtpLoanCostConfigEntity> implements FtpLoanCostConfigService {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private FtpLoanConstConfigDao ftpLoanConstConfigDao;

    public SysUserEntity getUser() {
        return (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
    }

    public Long getUserId() {
        return getUser().getUserId();
    }

    /**
     * 查询跑批日期
     * @return
     */
    @Override
    @DataSource(value = "second")
    public String queryCurdate() {
        return ftpLoanConstConfigDao.queryCurdate();
    }

    /**
     *分页查询ftp存款详情
     */
    @Override
    @DataSource(value = "second")
    public int queryDepositDataListNb(Map reqPara) {
        return ftpLoanConstConfigDao.queryDepositDataListNb(reqPara);
    }

    @Override
    @DataSource(value = "second")
    public List<FtpDepositIntDiffConfigEntity> queryDepositDataList(Map reqPara) {
        return ftpLoanConstConfigDao.queryDepositDataList(reqPara);
    }

    /**
     *分页查询ftp贷款详情
     */
    @Override
    @DataSource(value = "second")
    public int queryLoanConstDataListNb(Map reqPara) {
        return ftpLoanConstConfigDao.queryLoanConstDataListNb(reqPara);
    }

    @Override
    @DataSource(value = "second")
    public List<FtpLoanCostConfigEntity> queryLoanConstDataList(Map reqPara) {
        return ftpLoanConstConfigDao.queryLoanConstDataList(reqPara);
    }

    @Override
    @DataSource(value = "second")
    public List<FtpDepositIntDiffConfigEntity> queryDepositDateList(Map reqPara) {
        return ftpLoanConstConfigDao.queryDepositDateList(reqPara);
    }

    @Override
    @DataSource(value = "second")
    public List<FtpLoanCostConfigEntity> queryLoanDateList(Map reqPara) {
        return ftpLoanConstConfigDao.queryLoanDateList(reqPara);
    }

    /**
     * FTP存款数据修改
     * @param reqPara
     * @return
     */
    @SneakyThrows
    @Override
    @Transactional(rollbackFor= Exception.class)
    @DataSource(value = "second")
    public R updateDepositData(Map reqPara) {
        boolean flag = true;
        Map addMap = new HashMap();
        Map deleteMap = new HashMap();
        String branchId = (String) reqPara.get("branchId");
        String custType = (String) reqPara.get("custType");
        String bizType = (String) reqPara.get("bizType");
        String stdProdCd = (String) reqPara.get("stdProdCd");
        String stdProdNm = (String) reqPara.get("stdProdNm");
        String term = (String) reqPara.get("term");
        String ftpPrice = (String) reqPara.get("ftpPrice");
        String stdProdSign = (String) reqPara.get("stdProdSign");
        String startDt = (String) reqPara.get("startDt");//开始时间
        String endDt = (String) reqPara.get("endDt");//结束时间
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat df= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        if(("").equals(startDt) || startDt == null){//生效日期 如不填写，生效日期为操作当日
            startDt = formatter.format(date);
        }else{
            startDt = startDt;//生效日期
        }
        if(("").equals(endDt)  || endDt == null){//失效日期 如不填写，失效日期为"9999-12-31"
            endDt = "9999-12-31";//失效日期
        }else{
            endDt = endDt;//失效日期
        }
        //查询FTP存款数据表里面开始日期与结束日期
        List<FtpDepositIntDiffConfigEntity> dataList = ftpLoanConstConfigDao.queryDepositDateList(reqPara);
        FtpDepositIntDiffConfigEntity depositIntDiffConfigEntity = new FtpDepositIntDiffConfigEntity();
        for (int i = 0; i <dataList.size() ; i++) {
            Date sDate = formatter.parse(dataList.get(i).getStartDt()); //开始日期
            Date eDate = formatter.parse(dataList.get(i).getEndDt());//结束日期
            if(formatter.parse(startDt).compareTo(sDate) > 0) {//修改时：开始日期大于该产品里已存在的开始日期，将已存在的数据结束日期改为此次修改的开始日期，形成拉链表
                if(formatter.parse(startDt).compareTo(eDate) < 0){//为了生成拉链表，只修改该产品已存在结束时间大于或等于当前修改开始时间的数据
                    flag = true;
                    depositIntDiffConfigEntity.setBranchId(branchId);
                    depositIntDiffConfigEntity.setBizType(bizType);
                    depositIntDiffConfigEntity.setCustType(custType);
                    depositIntDiffConfigEntity.setStdProdCd(stdProdCd);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Calendar calendar = new GregorianCalendar();
                    calendar.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(startDt));
                    calendar.add(calendar.DATE,-1);
                    String newStartData= sdf.format(calendar.getTime());
                    depositIntDiffConfigEntity.setNewEndDt(newStartData);
                    depositIntDiffConfigEntity.setFlag("updateUp");//修改上一条数据
                    depositIntDiffConfigEntity.setEndDt(dataList.get(i).getEndDt());
                    ftpLoanConstConfigDao.updateDepositData(depositIntDiffConfigEntity);
                }
            }else if(formatter.parse(startDt).compareTo(sDate) == 0){//修改之后的开始时间等于该产品已存在的开始时间,说明是在当前该产品最新开始和结束这条数据上修改
                flag = false;
                deleteMap.put("branchId",branchId);
                deleteMap.put("custType",custType);
                deleteMap.put("bizType",bizType);
                deleteMap.put("stdProdCd",stdProdCd);
                deleteMap.put("startDt",startDt);//将等于生效日期存在的数据删除
                ftpLoanConstConfigDao.deleteDepositData(deleteMap);
                addMap.put("branchId",branchId);
                addMap.put("custType",custType);
                addMap.put("bizType",bizType);
                addMap.put("stdProdCd",stdProdCd);
                addMap.put("stdProdNm",stdProdNm);
                addMap.put("term",term);
                addMap.put("ftpPrice",ftpPrice);
                addMap.put("startDt",startDt);
                addMap.put("endDt",endDt);
                addMap.put("stdProdSign",stdProdSign);
                addMap.put("operators",getUser().getName());
                addMap.put("operateDate",df.format(date));
                ftpLoanConstConfigDao.addDepositData(addMap);
            }else{
                deleteMap.put("branchId",branchId);
                deleteMap.put("custType",custType);
                deleteMap.put("bizType",bizType);
                deleteMap.put("stdProdCd",stdProdCd);
                deleteMap.put("startDt",dataList.get(i).getStartDt());//修改时：开始日期小于该产品已存在的开始日期，将已存在的数据删除,并将上一条结束时间修改为最新的开始日期
                ftpLoanConstConfigDao.deleteDepositData(deleteMap);
                depositIntDiffConfigEntity.setBranchId(branchId);
                depositIntDiffConfigEntity.setBizType(bizType);
                depositIntDiffConfigEntity.setCustType(custType);
                depositIntDiffConfigEntity.setStdProdCd(stdProdCd);
                depositIntDiffConfigEntity.setFlag("delUpdate");
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Calendar calendar = new GregorianCalendar();
                calendar.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(startDt));
                calendar.add(calendar.DATE,-1);
                String newStartData= sdf.format(calendar.getTime());
                depositIntDiffConfigEntity.setNewEndDt(newStartData);//上一条的结束日期修改为当前最新的开始日期
                depositIntDiffConfigEntity.setEndDt(dataList.get(i).getStartDt());
                ftpLoanConstConfigDao.updateDepositData(depositIntDiffConfigEntity);
            }
        }
        if(flag == true){
            //重新赋值开始时间与结束时间，执行新增新的一条数据，以此形成拉链表
            addMap.put("branchId",branchId);
            addMap.put("custType",custType);
            addMap.put("bizType",bizType);
            addMap.put("stdProdCd",stdProdCd);
            addMap.put("stdProdNm",stdProdNm);
            addMap.put("term",term);
            addMap.put("ftpPrice",ftpPrice);
            addMap.put("startDt",startDt);
            addMap.put("endDt",endDt);
            addMap.put("stdProdSign",stdProdSign);
            addMap.put("operators",getUser().getName());
            addMap.put("operateDate",df.format(date));
            ftpLoanConstConfigDao.addDepositData(addMap);
        }
        return R.ok();
    }

    /**
     * FTP贷款数据修改
     * @param reqPara
     * @return
     */
    @SneakyThrows
    @Override
    @Transactional(rollbackFor= Exception.class)
    @DataSource(value = "second")
    public R updateLoanConstData(Map reqPara) {
        boolean flag = true;
        Map addMap = new HashMap();
        Map deleteMap = new HashMap();
        String branchId = (String) reqPara.get("branchId");
        String custType = (String) reqPara.get("custType");
        String bizType = (String) reqPara.get("bizType");
        String stdProdCd = (String) reqPara.get("stdProdCd");
        String stdProdNm = (String) reqPara.get("stdProdNm");
        String term = (String) reqPara.get("term");
        String guarType = (String) reqPara.get("guarType");
        String ftpPrice = (String) reqPara.get("ftpPrice");
        String stdProdSign = (String) reqPara.get("stdProdSign");
        String startDt = (String) reqPara.get("startDt");//开始时间
        String endDt = (String) reqPara.get("endDt");//结束时间
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat df= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        if(("").equals(startDt) || startDt == null){//生效日期 如不填写，生效日期为操作当日
            startDt = formatter.format(date);//生效日期
        }else{
            startDt = startDt;//生效日期
        }
        if(("").equals(endDt)  || endDt == null){//失效日期 如不填写，失效日期为"9999-12-31"
            endDt = "9999-12-31";//失效日期
        }else{
            endDt = endDt;//失效日期
        }
        //查询FTP贷款数据表里面开始日期与结束日期
        List<FtpLoanCostConfigEntity> dataList = ftpLoanConstConfigDao.queryLoanDateList(reqPara);
        FtpLoanCostConfigEntity ftpLoanCostConfigEntity = new FtpLoanCostConfigEntity();
        for (int i = 0; i <dataList.size() ; i++) {
            Date sDate = formatter.parse(dataList.get(i).getStartDt()); //开始日期
            Date eDate = formatter.parse(dataList.get(i).getEndDt());//结束日期
            if(formatter.parse(startDt).compareTo(sDate) > 0) {//修改时：开始日期大于该产品里已存在的开始日期，将已存在的数据结束日期改为此次修改的开始日期，形成拉链表
                if(formatter.parse(startDt).compareTo(eDate) < 0){//为了生成拉链表，只修改该产品已存在结束时间大于当前修改开始时间的数据
                    flag = true;
                    ftpLoanCostConfigEntity.setBranchId(branchId);
                    ftpLoanCostConfigEntity.setBizType(bizType);
                    ftpLoanCostConfigEntity.setCustType(custType);
                    ftpLoanCostConfigEntity.setGuarType(guarType);
                    ftpLoanCostConfigEntity.setStdProdCd(stdProdCd);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Calendar calendar = new GregorianCalendar();
                    calendar.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(startDt));
                    calendar.add(calendar.DATE,-1);
                    String newStartData= sdf.format(calendar.getTime());
                    ftpLoanCostConfigEntity.setNewEndDt(newStartData);
                    ftpLoanCostConfigEntity.setFlag("updateUp");//修改上一条数据
                    ftpLoanCostConfigEntity.setEndDt(dataList.get(i).getEndDt());
                    ftpLoanConstConfigDao.updateLoanConstData(ftpLoanCostConfigEntity);
                }
            }else if(formatter.parse(startDt).compareTo(sDate) == 0){//修改之后的开始时间等于该产品已存在的开始时间,说明是在当前该产品最新开始和结束这条数据上修改
                flag = false;
                deleteMap.put("branchId",branchId);
                deleteMap.put("custType",custType);
                deleteMap.put("bizType",bizType);
                deleteMap.put("guarType",guarType);
                deleteMap.put("stdProdCd",stdProdCd);
                deleteMap.put("startDt",startDt);//将等于生效日期存在的数据删除
                ftpLoanConstConfigDao.deleteLoanConstData(deleteMap);
                addMap.put("branchId",branchId);
                addMap.put("custType",custType);
                addMap.put("bizType",bizType);
                addMap.put("stdProdCd",stdProdCd);
                addMap.put("stdProdNm",stdProdNm);
                addMap.put("term",term);
                addMap.put("guarType",guarType);
                addMap.put("ftpPrice",ftpPrice);
                addMap.put("startDt",startDt);
                addMap.put("endDt",endDt);
                addMap.put("stdProdSign",stdProdSign);
                addMap.put("operators", getUser().getName());
                addMap.put("operateDate",df.format(date));
                ftpLoanConstConfigDao.addLoanData(addMap);
            }else{
                deleteMap.put("branchId",branchId);
                deleteMap.put("custType",custType);
                deleteMap.put("bizType",bizType);
                deleteMap.put("guarType",guarType);
                deleteMap.put("stdProdCd",stdProdCd);
                deleteMap.put("startDt",dataList.get(i).getStartDt());//修改时：开始日期小于该产品已存在的开始日期，将已存在的数据删除,并将上一条结束时间修改为最新的开始日期
                ftpLoanConstConfigDao.deleteLoanConstData(deleteMap);
                ftpLoanCostConfigEntity.setBranchId(branchId);
                ftpLoanCostConfigEntity.setBizType(bizType);
                ftpLoanCostConfigEntity.setCustType(custType);
                ftpLoanCostConfigEntity.setGuarType(guarType);
                ftpLoanCostConfigEntity.setStdProdCd(stdProdCd);
                ftpLoanCostConfigEntity.setFlag("delUpdate");
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Calendar calendar = new GregorianCalendar();
                calendar.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(startDt));
                calendar.add(calendar.DATE,-1);
                String newStartData= sdf.format(calendar.getTime());
                ftpLoanCostConfigEntity.setNewEndDt(newStartData);//上一条的结束日期修改为当前最新的开始日期
                ftpLoanCostConfigEntity.setEndDt(dataList.get(i).getStartDt());
                ftpLoanConstConfigDao.updateLoanConstData(ftpLoanCostConfigEntity);
            }
        }
        if(flag == true){
            //重新赋值开始时间与结束时间，执行新增新的一条数据，以此形成拉链表
            addMap.put("branchId",branchId);
            addMap.put("custType",custType);
            addMap.put("bizType",bizType);
            addMap.put("stdProdCd",stdProdCd);
            addMap.put("stdProdNm",stdProdNm);
            addMap.put("term",term);
            addMap.put("guarType",guarType);
            addMap.put("ftpPrice",ftpPrice);
            addMap.put("startDt",startDt);
            addMap.put("endDt",endDt);
            addMap.put("stdProdSign",stdProdSign);
            addMap.put("operators", getUser().getName());
            addMap.put("operateDate",df.format(date));
            ftpLoanConstConfigDao.addLoanData(addMap);
        }
        return R.ok();
    }

    /**
     * 批量修改存款利差表数据
     * @param reqPara
     * @return
     */
    @SneakyThrows
    @Override
    @Transactional(rollbackFor= Exception.class)
    @DataSource(value = "second")
    public R updateDepositBatchData(Map reqPara) {
        boolean flag = true;
        Map addMap = new HashMap();
        Map deleteMap = new HashMap();
        Map orgMap = new HashMap();//机构集合
        String chOrgId = (String) reqPara.get("branchId");
        String custType = (String) reqPara.get("custType");
        String bizType = (String) reqPara.get("bizType");
        String stdProdCd = (String) reqPara.get("stdProdCd");
        String stdProdNm = (String) reqPara.get("stdProdNm");
        String term = (String) reqPara.get("term");
        String ftpPrice = (String) reqPara.get("ftpPrice");
        String stdProdSign = (String) reqPara.get("stdProdSign");
        String startDt = (String) reqPara.get("startDt");//开始时间
        String endDt = (String) reqPara.get("endDt");//结束时间
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat df= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        if(("").equals(startDt) || startDt == null){//生效日期 如不填写，生效日期为操作当日
            startDt = formatter.format(date);
        }else{
            startDt = startDt;//生效日期
        }
        if(("").equals(endDt)  || endDt == null){//失效日期 如不填写，失效日期为"9999-12-31"
            endDt = "9999-12-31";//失效日期
        }else{
            endDt = endDt;//失效日期
        }
        //递归查询机构信息
        orgMap.put("chOrgId","80000");
        List<String> orgId = ftpLoanConstConfigDao.findOrgId(orgMap);
        for (int j = 0; j <orgId.size() ; j++) {
            //查询FTP存款数据表里面开始日期与结束日期
            reqPara.put("branchId",orgId.get(j));
            List<FtpDepositIntDiffConfigEntity> dataList = ftpLoanConstConfigDao.queryDepositDateList(reqPara);
            FtpDepositIntDiffConfigEntity depositIntDiffConfigEntity = new FtpDepositIntDiffConfigEntity();
            //如果查询结构为空，则flag = false
            if(dataList.size() == 0){
                flag = false;
            }
            for (int i = 0; i <dataList.size() ; i++) {
                Date sDate = formatter.parse(dataList.get(i).getStartDt()); //开始日期
                Date eDate = formatter.parse(dataList.get(i).getEndDt());//结束日期
                if(formatter.parse(startDt).compareTo(sDate) > 0) {//修改时：开始日期大于该产品里已存在的开始日期，将已存在的数据结束日期改为此次修改的开始日期，形成拉链表
                    if(formatter.parse(startDt).compareTo(eDate) < 0){//为了生成拉链表，只修改该产品已存在结束时间大于或等于当前修改开始时间的数据
                        flag = true;
                        depositIntDiffConfigEntity.setBranchId(orgId.get(j));
                        depositIntDiffConfigEntity.setBizType(bizType);
                        depositIntDiffConfigEntity.setCustType(custType);
                        depositIntDiffConfigEntity.setStdProdCd(stdProdCd);
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        Calendar calendar = new GregorianCalendar();
                        calendar.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(startDt));
                        calendar.add(calendar.DATE,-1);
                        String newStartData= sdf.format(calendar.getTime());
                        depositIntDiffConfigEntity.setNewEndDt(newStartData);
                        depositIntDiffConfigEntity.setFlag("updateUp");//修改上一条数据
                        depositIntDiffConfigEntity.setEndDt(dataList.get(i).getEndDt());
                        ftpLoanConstConfigDao.updateDepositData(depositIntDiffConfigEntity);
                    }
                }else if(formatter.parse(startDt).compareTo(sDate) == 0){//修改之后的开始时间等于该产品已存在的开始时间,说明是在当前该产品最新开始和结束这条数据上修改
                    flag = false;
                    deleteMap.put("branchId",orgId.get(j));
                    deleteMap.put("custType",custType);
                    deleteMap.put("bizType",bizType);
                    deleteMap.put("stdProdCd",stdProdCd);
                    deleteMap.put("startDt",startDt);//将等于生效日期存在的数据删除
                    ftpLoanConstConfigDao.deleteDepositData(deleteMap);
                    addMap.put("branchId",orgId.get(j));
                    addMap.put("custType",custType);
                    addMap.put("bizType",bizType);
                    addMap.put("stdProdCd",stdProdCd);
                    addMap.put("stdProdNm",stdProdNm);
                    addMap.put("term",term);
                    addMap.put("ftpPrice",ftpPrice);
                    addMap.put("startDt",startDt);
                    addMap.put("endDt",endDt);
                    addMap.put("stdProdSign",stdProdSign);
                    addMap.put("operators",getUser().getName());
                    addMap.put("operateDate",df.format(date));
                    ftpLoanConstConfigDao.addDepositData(addMap);
                }else{
                    deleteMap.put("branchId",orgId.get(j));
                    deleteMap.put("custType",custType);
                    deleteMap.put("bizType",bizType);
                    deleteMap.put("stdProdCd",stdProdCd);
                    deleteMap.put("startDt",dataList.get(i).getStartDt());//修改时：开始日期小于该产品已存在的开始日期，将已存在的数据删除,并将上一条结束时间修改为最新的开始日期
                    ftpLoanConstConfigDao.deleteDepositData(deleteMap);
                    depositIntDiffConfigEntity.setBranchId(orgId.get(j));
                    depositIntDiffConfigEntity.setBizType(bizType);
                    depositIntDiffConfigEntity.setCustType(custType);
                    depositIntDiffConfigEntity.setStdProdCd(stdProdCd);
                    depositIntDiffConfigEntity.setFlag("delUpdate");
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Calendar calendar = new GregorianCalendar();
                    calendar.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(startDt));
                    calendar.add(calendar.DATE,-1);
                    String newStartData= sdf.format(calendar.getTime());
                    depositIntDiffConfigEntity.setNewEndDt(newStartData);//上一条的结束日期修改为当前最新的开始日期
                    depositIntDiffConfigEntity.setEndDt(dataList.get(i).getStartDt());
                    ftpLoanConstConfigDao.updateDepositData(depositIntDiffConfigEntity);
                }
            }
            if(flag == true){
                //重新赋值开始时间与结束时间，执行新增新的一条数据，以此形成拉链表
                addMap.put("branchId",orgId.get(j));
                addMap.put("custType",custType);
                addMap.put("bizType",bizType);
                addMap.put("stdProdCd",stdProdCd);
                addMap.put("stdProdNm",stdProdNm);
                addMap.put("term",term);
                addMap.put("ftpPrice",ftpPrice);
                addMap.put("startDt",startDt);
                addMap.put("endDt",endDt);
                addMap.put("stdProdSign",stdProdSign);
                addMap.put("operators",getUser().getName());
                addMap.put("operateDate",df.format(date));
                ftpLoanConstConfigDao.addDepositData(addMap);
            }
        }
        return R.ok();
    }

    /**
     * 批量修改贷款成本表数据
     * @param reqPara
     * @return
     */
    @SneakyThrows
    @Override
    @Transactional(rollbackFor= Exception.class)
    @DataSource(value = "second")
    public R updateLoanConstBatchData(Map reqPara) {
        boolean flag = true;
        Map addMap = new HashMap();
        Map deleteMap = new HashMap();
        Map orgMap = new HashMap();//机构集合
        String chOrgId = (String) reqPara.get("branchId");
        String custType = (String) reqPara.get("custType");
        String bizType = (String) reqPara.get("bizType");
        String stdProdCd = (String) reqPara.get("stdProdCd");
        String stdProdNm = (String) reqPara.get("stdProdNm");
        String term = (String) reqPara.get("term");
        String guarType = (String) reqPara.get("guarType");
        String ftpPrice = (String) reqPara.get("ftpPrice");
        String stdProdSign = (String) reqPara.get("stdProdSign");
        String startDt = (String) reqPara.get("startDt");//开始时间
        String endDt = (String) reqPara.get("endDt");//结束时间
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat df= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        if(("").equals(startDt) || startDt == null){//生效日期 如不填写，生效日期为操作当日
            startDt = formatter.format(date);//生效日期
        }else{
            startDt = startDt;//生效日期
        }
        if(("").equals(endDt)  || endDt == null){//失效日期 如不填写，失效日期为"9999-12-31"
            endDt = "9999-12-31";//失效日期
        }else{
            endDt = endDt;//失效日期
        }
        //递归查询机构信息
        orgMap.put("chOrgId","80000");
        List<String> orgId = ftpLoanConstConfigDao.findOrgId(orgMap);

        for (int j = 0; j <orgId.size() ; j++) {
            reqPara.put("branchId",orgId.get(j));
            //查询FTP贷款数据表里面开始日期与结束日期
            List<FtpLoanCostConfigEntity> dataList = ftpLoanConstConfigDao.queryLoanDateList(reqPara);
            FtpLoanCostConfigEntity ftpLoanCostConfigEntity = new FtpLoanCostConfigEntity();
            if(dataList.size() == 0){//如果为空则该机构下面则没有改产品
                flag = false;
            }
            for (int i = 0; i < dataList.size(); i++) {
                Date sDate = formatter.parse(dataList.get(i).getStartDt()); //开始日期
                Date eDate = formatter.parse(dataList.get(i).getEndDt());//结束日期
                if (formatter.parse(startDt).compareTo(sDate) > 0) {//修改时：开始日期大于该产品里已存在的开始日期，将已存在的数据结束日期改为此次修改的开始日期，形成拉链表
                    if (formatter.parse(startDt).compareTo(eDate) < 0) {//为了生成拉链表，只修改该产品已存在结束时间大于当前修改开始时间的数据
                        flag = true;
                        ftpLoanCostConfigEntity.setBranchId(orgId.get(j));
                        ftpLoanCostConfigEntity.setBizType(bizType);
                        ftpLoanCostConfigEntity.setCustType(custType);
                        ftpLoanCostConfigEntity.setGuarType(guarType);
                        ftpLoanCostConfigEntity.setStdProdCd(stdProdCd);
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        Calendar calendar = new GregorianCalendar();
                        calendar.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(startDt));
                        calendar.add(calendar.DATE, -1);
                        String newStartData = sdf.format(calendar.getTime());
                        ftpLoanCostConfigEntity.setNewEndDt(newStartData);
                        ftpLoanCostConfigEntity.setFlag("updateUp");//修改上一条数据
                        ftpLoanCostConfigEntity.setEndDt(dataList.get(i).getEndDt());
                        ftpLoanConstConfigDao.updateLoanConstData(ftpLoanCostConfigEntity);
                    }
                } else if (formatter.parse(startDt).compareTo(sDate) == 0) {//修改之后的开始时间等于该产品已存在的开始时间,说明是在当前该产品最新开始和结束这条数据上修改
                    flag = false;
                    deleteMap.put("branchId", orgId.get(j));
                    deleteMap.put("custType", custType);
                    deleteMap.put("bizType", bizType);
                    deleteMap.put("guarType", guarType);
                    deleteMap.put("stdProdCd", stdProdCd);
                    deleteMap.put("startDt", startDt);//将等于生效日期存在的数据删除
                    ftpLoanConstConfigDao.deleteLoanConstData(deleteMap);
                    addMap.put("branchId", orgId.get(j));
                    addMap.put("custType", custType);
                    addMap.put("bizType", bizType);
                    addMap.put("stdProdCd", stdProdCd);
                    addMap.put("stdProdNm", stdProdNm);
                    addMap.put("term", term);
                    addMap.put("guarType", guarType);
                    addMap.put("ftpPrice", ftpPrice);
                    addMap.put("startDt", startDt);
                    addMap.put("endDt", endDt);
                    addMap.put("stdProdSign", stdProdSign);
                    addMap.put("operators", getUser().getName());
                    addMap.put("operateDate", df.format(date));
                    ftpLoanConstConfigDao.addLoanData(addMap);
                } else {
                    deleteMap.put("branchId", orgId.get(j));
                    deleteMap.put("custType", custType);
                    deleteMap.put("bizType", bizType);
                    deleteMap.put("guarType", guarType);
                    deleteMap.put("stdProdCd", stdProdCd);
                    deleteMap.put("startDt", dataList.get(i).getStartDt());//修改时：开始日期小于该产品已存在的开始日期，将已存在的数据删除,并将上一条结束时间修改为最新的开始日期
                    ftpLoanConstConfigDao.deleteLoanConstData(deleteMap);
                    ftpLoanCostConfigEntity.setBranchId(orgId.get(j));
                    ftpLoanCostConfigEntity.setBizType(bizType);
                    ftpLoanCostConfigEntity.setCustType(custType);
                    ftpLoanCostConfigEntity.setGuarType(guarType);
                    ftpLoanCostConfigEntity.setStdProdCd(stdProdCd);
                    ftpLoanCostConfigEntity.setFlag("delUpdate");
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Calendar calendar = new GregorianCalendar();
                    calendar.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(startDt));
                    calendar.add(calendar.DATE, -1);
                    String newStartData = sdf.format(calendar.getTime());
                    ftpLoanCostConfigEntity.setNewEndDt(newStartData);//上一条的结束日期修改为当前最新的开始日期
                    ftpLoanCostConfigEntity.setEndDt(dataList.get(i).getStartDt());
                    ftpLoanConstConfigDao.updateLoanConstData(ftpLoanCostConfigEntity);
                }
            }
            if (flag == true) {
                //重新赋值开始时间与结束时间，执行新增新的一条数据，以此形成拉链表
                addMap.put("branchId", orgId.get(j));
                addMap.put("custType", custType);
                addMap.put("bizType", bizType);
                addMap.put("stdProdCd", stdProdCd);
                addMap.put("stdProdNm", stdProdNm);
                addMap.put("term", term);
                addMap.put("guarType", guarType);
                addMap.put("ftpPrice", ftpPrice);
                addMap.put("startDt", startDt);
                addMap.put("endDt", endDt);
                addMap.put("stdProdSign", stdProdSign);
                addMap.put("operators", getUser().getName());
                addMap.put("operateDate", df.format(date));
                ftpLoanConstConfigDao.addLoanData(addMap);
            }
        }
        return R.ok();
    }
    @Override
    @DataSource(value = "second")
    public List<FtpLoanCostConfigEntity> getLoanDataListHis(Map<String, Object> params) {
        int pageNo = Integer.parseInt((String) params.get("page"));
        int pageCount = Integer.parseInt((String)params.get("limit"));
        int offset = ((pageNo - 1) * pageCount);
        params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
        params.put("offset", offset);
        List<FtpLoanCostConfigEntity> loanDataListHis = ftpLoanConstConfigDao.queryLoanConstDataList(params);

        return loanDataListHis;
    }
    @Override
    @DataSource(value = "second")
    public List<FtpDepositIntDiffConfigEntity> getDepositListHis(Map<String, Object> params) {
        int pageNo = Integer.parseInt((String) params.get("page"));
        int pageCount = Integer.parseInt((String)params.get("limit"));
        int offset = ((pageNo - 1) * pageCount);
        params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
        params.put("offset", offset);
        List<FtpDepositIntDiffConfigEntity> loanDataListHis = ftpLoanConstConfigDao.queryDepositDataList(params);
        return loanDataListHis;
    }
    @Override
    @DataSource(value = "second")
    public int getLoanDataListHisNb(Map<String, Object> params) {
        int total = ftpLoanConstConfigDao.queryLoanConstDataListNb(params);
        return total;
    }
    @Override
    @DataSource(value = "second")
    public void deleteData(Map reqPara) {
        String bizType = (String)reqPara.get("bizType");
        if(bizType.equals("D")){
            ftpLoanConstConfigDao.deleteDepositData(reqPara);
        }else {
            ftpLoanConstConfigDao.deleteLoanConstData(reqPara);
        }
    }
}
