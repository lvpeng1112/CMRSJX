package com.beyondsoft.modules.ftp.entity;

import lombok.Data;

import java.io.Serializable;
@Data
public class StdProd implements Serializable {
    private String id;
    private String label;
}
