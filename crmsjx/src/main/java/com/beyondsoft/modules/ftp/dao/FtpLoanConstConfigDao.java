package com.beyondsoft.modules.ftp.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.ftp.entity.FtpDepositIntDiffConfigEntity;
import com.beyondsoft.modules.ftp.entity.FtpLoanCostConfigEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 *ftp存贷款详情
 */
@Mapper
public interface FtpLoanConstConfigDao extends BaseMapper<FtpLoanCostConfigEntity> {

    /**
     * 查询跑批日期
     * @param reqPara
     * @return
     */
    String queryCurdate();

    /**
     * 递归查询机构信息
     */
    public List<String> findOrgId(Map reqPara);

    /**
     * 查询FTP存款利差所有标准产品编码数据
     * @return
     */
    List<FtpDepositIntDiffConfigEntity> queryDepositCodeList();

    /**
     * 查询FTP贷款成本参数表所有标准产品编码数据
     * @return
     */
    List<FtpLoanCostConfigEntity> queryLoanCodeList();

    /**
     *分页查询ftp存款详情
     */
    public int queryDepositDataListNb(Map params);

    List<FtpDepositIntDiffConfigEntity> queryDepositDataList(Map params);

    /**
     *分页查询ftp贷款详情
     */
    public int queryLoanConstDataListNb(Map params);

    List<FtpLoanCostConfigEntity> queryLoanConstDataList(Map params);

    /**
     * 查詢ftp存款的开始日期与结束日期
     */
    List<FtpDepositIntDiffConfigEntity> queryDepositDateList(Map reqPara);

    /**
     * 查詢ftp贷款的开始日期与结束日期
     */
    List<FtpLoanCostConfigEntity> queryLoanDateList(Map reqPara);

    /**
     * 新增ftp存款利差表数据
     */
    public void addDepositData(Map reqPara);

    /**
     * 新增ftp贷款成本表数据
     */
    public void addLoanData(Map reqPara);

    /**
     * 修改ftp存款详情
     */
    void updateDepositData(FtpDepositIntDiffConfigEntity ftpDepositIntDiffConfigEntity);

    /**
     * 修改ftp贷款详情
     */
    void updateLoanConstData(FtpLoanCostConfigEntity ftpLoanCostConfigEntity);

    /**
     * 修改ftp存款详情
     */
    public R updateDepositBatchData(Map reqPara);

    /**
     * 修改ftp贷款详情
     */
    public R updateLoanConstBatchData(Map reqPara);

    /**
     * 删除ftp存款利差表数据
     */
    void deleteDepositData(Map reqPara);

    /**
     * 删除ftp贷款款成本表数据
     */
    void deleteLoanConstData(Map reqPara);

    List<FtpLoanCostConfigEntity> getLoanDataListHis(Map<String, Object> params);

    int getLoanDataListHisNb(Map<String, Object> params);
}
