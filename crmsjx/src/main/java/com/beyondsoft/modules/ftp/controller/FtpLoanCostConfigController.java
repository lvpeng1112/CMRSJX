package com.beyondsoft.modules.ftp.controller;

import com.alibaba.druid.util.StringUtils;
import com.beyondsoft.common.annotation.SysLog;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.datasource.annotation.DataSource;
import com.beyondsoft.modules.ftp.dao.FtpLoanConstConfigDao;
import com.beyondsoft.modules.ftp.entity.FtpDepositIntDiffConfigEntity;
import com.beyondsoft.modules.ftp.entity.FtpLoanCostConfigEntity;
import com.beyondsoft.modules.ftp.service.FtpLoanCostConfigService;
import com.beyondsoft.modules.ftp.service.ProdStdProdCdService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * @author luochao
 * @email
 * @date 2021-02-02
 */
@RestController
@RequestMapping("ftp/ftpLoanCost")
public class FtpLoanCostConfigController {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private FtpLoanConstConfigDao ftpLoanConstConfigDao;

    @Autowired
    private FtpLoanCostConfigService ftpLoanCostConfigService;
    @Autowired
    private ProdStdProdCdService prodStdProdCdService;
    /**
     *分页查询ftp存款详情
     */
    @GetMapping("/list")
    public R list(@RequestParam Map<String, Object> params) {
        Map depMap = new HashMap();//存款map
        Map loanMap = new HashMap();//贷款map
        Map dateMap = new HashMap();//日期map
        List<String> custTypePList = new ArrayList<>();//客户类型（单位）日期数组集合
        List<String> custTypeCList = new ArrayList<>();//客户类型（个人）日期数组集合
        List<FtpDepositIntDiffConfigEntity> custTypePMap = new ArrayList<>();//客户类型（单位）日期实体类集合
        List<FtpDepositIntDiffConfigEntity> custTypeCMap = new ArrayList<>();//客户类型（个人）日期实体类集合
        //当前日期
        LocalDate now = LocalDate.now();
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String nowF = now.format(df);
        //登录系统人员机构
        String initCode = (String) params.get("chOrgId");
        //产品大类（存款（D）还是贷款（C））
        String bizType = (String) params.get("bizType");
        //产品小类（个人存款，个人贷款，单位存款，单位贷款）
        String custType = (String) params.get("custType");
        //担保方式
        String guarType = (String) params.get("guarType");
        //产品子类（标准产品代码）
        String stdProdCd = (String) params.get("stdProdCd");
        //价格
        String ftpPrice = (String) params.get("ftpPrice");
        //生效日期
        String startDt = (String) params.get("startDt");
        //失效日期
        String endDt = (String)params.get("endDt");
        int pageNo = Integer.parseInt((String) params.get("page"));
        int pageCount = Integer.parseInt((String)params.get("limit"));
        int offset = ((pageNo - 1) * pageCount);

        //判断是存款还是贷款
        if("D".equals(bizType)){//如果是存款，则查询FTP存款利差表
            depMap.put("branchId",initCode);
            depMap.put("bizType",bizType);
            depMap.put("custType",custType);
            depMap.put("stdProdCd",stdProdCd);
            depMap.put("ftpPrice",ftpPrice);
            depMap.put("startDt",startDt);
            depMap.put("endDt",endDt);
            depMap.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
            depMap.put("offset", offset);
            int count = ftpLoanCostConfigService.queryDepositDataListNb(depMap);
            List<FtpDepositIntDiffConfigEntity> list = ftpLoanCostConfigService.queryDepositDataList(depMap);
            //日期用中文显示
            for (int i = 0; i <list.size() ; i++) {
                list.remove(null);
                String term = list.get(i).getTerm();
                String bizType1 = list.get(i).getBizType();
                Map map = new HashMap();
                String termName = null;
                if (bizType1.equals("C")) {
                    map.put("busintypeid","FTP_COST_LOAN");//贷款
                    map.put("businid",term);
                    termName= prodStdProdCdService.queryTermName(map);
                    list.get(i).setTerm(termName);
                }
                else if (bizType1.equals("D")){
                    map.put("busintypeid","FTP_COST_DEPOSIT");//存款
                    map.put("businid",term);
                    termName= prodStdProdCdService.queryTermName(map);
                    list.get(i).setTerm(termName);
                }
            }
            for (int i = 0; i <list.size() ; i++) {
                if("D".equals(list.get(i).getBizType())){
                    list.get(i).setBizTypeNm("存款");
                }else{
                    list.get(i).setBizTypeNm("贷款");
                }
                if("C".equals(list.get(i).getCustType())){
                    list.get(i).setCustTypeNm("单位");
                }else{
                    list.get(i).setCustTypeNm("个人");
                }
                dateMap.put("branchId",list.get(i).getBranchId());
                dateMap.put("bizType",list.get(i).getBizType());
                dateMap.put("custType",list.get(i).getCustType());
                dateMap.put("stdProdCd",list.get(i).getStdProdCd());
                dateMap.put("ftpPrice",list.get(i).getFtpPrice());
                List<FtpDepositIntDiffConfigEntity> dataList = ftpLoanCostConfigService.queryDepositDateList(dateMap);
                for (int j = 0; j <dataList.size() ; j++) {
                    if("C".equals(list.get(i).getCustType())){
                        FtpDepositIntDiffConfigEntity ftpDepositIntDiffConfigEntity = new FtpDepositIntDiffConfigEntity();
                        ftpDepositIntDiffConfigEntity.setStartDt(dataList.get(j).getStartDt());//把该产品生效日期全部进行保存
                        ftpDepositIntDiffConfigEntity.setStdProdCd(list.get(i).getStdProdCd());//把该产品对应的产品号全部进行保存
                        custTypeCMap.add(ftpDepositIntDiffConfigEntity);//单位
                    }else if("P".equals(list.get(i).getCustType())){
                        FtpDepositIntDiffConfigEntity ftpDepositIntDiffConfigEntity = new FtpDepositIntDiffConfigEntity();
                        ftpDepositIntDiffConfigEntity.setStartDt(dataList.get(j).getStartDt());//把该产品生效日期全部进行保存
                        ftpDepositIntDiffConfigEntity.setStdProdCd(list.get(i).getStdProdCd());//把该产品对应的产品号全部进行保存
                        custTypePMap.add(ftpDepositIntDiffConfigEntity);//个人
                    }
                }
                if("C".equals(list.get(i).getCustType())){
                    for (int j = 0; j < custTypeCMap.size(); j++) {
                        if(list.get(i).getStdProdCd().equals(custTypeCMap.get(j).getStdProdCd())){
                            if(custTypeCMap.get(j).getStartDt().compareTo(nowF)<=0) {//当生效日期小于当前日期
                                custTypeCList.add(custTypeCMap.get(j).getStartDt());
                            }
                            else{
                                custTypeCList.add("0000");
                            }
                        }
                    }
                    for (int j = 0; j < custTypeCMap.size(); j++) {
                        if(list.get(i).getStartDt().equals(Collections.max(custTypeCList)) && list.get(i).getStdProdCd().equals(custTypeCMap.get(j).getStdProdCd())){
                            list.get(i).setTarget("true");//该条产品的生效日期是最新生效日期
                        }else if(list.get(i).getEndDt().compareTo(nowF)>0){//当失效日期大于当前日期时，为未失效，不置灰
                            list.get(i).setTarget("true");
                        }else{
                            list.get(i).setTarget("false");
                        }
                    }
                }else if("P".equals(list.get(i).getCustType())){
                    for (int j = 0; j < custTypePMap.size(); j++) {
                        if(custTypePMap.get(j).getStartDt().compareTo(nowF)<=0) {
                            if (list.get(i).getStdProdCd().equals(custTypePMap.get(j).getStdProdCd())) {
                                custTypePList.add(custTypePMap.get(j).getStartDt());
                            }

                        }
                        else{
                            custTypePList.add("0000");
                        }
                    }
                    for (int j = 0; j < custTypePMap.size(); j++) {
                        if(list.get(i).getStartDt().equals(Collections.max(custTypePList)) && list.get(i).getStdProdCd().equals(custTypePMap.get(j).getStdProdCd())){
                            list.get(i).setTarget("true");//该条产品的生效日期是最新生效日期
                        }else if(list.get(i).getEndDt().compareTo(nowF)>0){//当失效日期大于当前日期时，为未失效，不置灰
                            list.get(i).setTarget("true");
                        }else{
                            list.get(i).setTarget("false");
                        }
                    }
                }
                custTypeCMap.clear();
                custTypeCList.clear();
                custTypePMap.clear();
                custTypePList.clear();
            }

            return R.ok().put("list", list).put("count",count);
        }else{//否则查询FTP贷款成本参数表
            loanMap.put("branchId",initCode);
            loanMap.put("bizType",bizType);
            loanMap.put("custType",custType);
            loanMap.put("guarType",guarType);
            loanMap.put("stdProdCd",stdProdCd);
            loanMap.put("ftpPrice",ftpPrice);
            loanMap.put("startDt",startDt);
            loanMap.put("endDt",endDt);
            loanMap.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
            loanMap.put("offset", offset);
            int count = ftpLoanCostConfigService.queryLoanConstDataListNb(loanMap);
            List<FtpLoanCostConfigEntity> list = ftpLoanCostConfigService.queryLoanConstDataList(loanMap);
            for (int i = 0; i <list.size() ; i++) {
                if("D".equals(list.get(i).getBizType())){
                    list.get(i).setBizTypeNm("存款");
                }else{
                    list.get(i).setBizTypeNm("贷款");
                }
                if("C".equals(list.get(i).getCustType())){
                    list.get(i).setCustTypeNm("单位");
                }else{
                    list.get(i).setCustTypeNm("个人");
                }
                dateMap.put("branchId",list.get(i).getBranchId());
                dateMap.put("bizType",list.get(i).getBizType());
                dateMap.put("custType",list.get(i).getCustType());
                dateMap.put("guarType",list.get(i).getGuarType());
                dateMap.put("stdProdCd",list.get(i).getStdProdCd());
                dateMap.put("ftpPrice",list.get(i).getFtpPrice());
                List<FtpLoanCostConfigEntity> dataList = ftpLoanCostConfigService.queryLoanDateList(dateMap);
                for (int j = 0; j <dataList.size() ; j++) {
                    if("C".equals(list.get(i).getCustType())){
                        FtpDepositIntDiffConfigEntity ftpDepositIntDiffConfigEntity = new FtpDepositIntDiffConfigEntity();
                        ftpDepositIntDiffConfigEntity.setStartDt(dataList.get(j).getStartDt());//把该产品生效日期全部进行保存
                        ftpDepositIntDiffConfigEntity.setStdProdCd(list.get(i).getStdProdCd());//把该产品对应的产品号全部进行保存
                        custTypeCMap.add(ftpDepositIntDiffConfigEntity);//单位
                    }else if("P".equals(list.get(i).getCustType())){
                        FtpDepositIntDiffConfigEntity ftpDepositIntDiffConfigEntity = new FtpDepositIntDiffConfigEntity();
                        ftpDepositIntDiffConfigEntity.setStartDt(dataList.get(j).getStartDt());//把该产品生效日期全部进行保存
                        ftpDepositIntDiffConfigEntity.setStdProdCd(list.get(i).getStdProdCd());//把该产品对应的产品号全部进行保存
                        custTypePMap.add(ftpDepositIntDiffConfigEntity);//个人
                    }
                }
                if("C".equals(list.get(i).getCustType())){
                    for (int j = 0; j < custTypeCMap.size(); j++) {
                        if(custTypeCMap.get(j).getStartDt().compareTo(nowF)<=0) {
                            if (list.get(i).getStdProdCd().equals(custTypeCMap.get(j).getStdProdCd())) {
                                custTypeCList.add(custTypeCMap.get(j).getStartDt());
                            }

                        }
                        else{
                            custTypeCList.add("0000");
                        }
                    }
                    for (int j = 0; j < custTypeCMap.size(); j++) {
                        if(list.get(i).getStartDt().equals(Collections.max(custTypeCList)) && list.get(i).getStdProdCd().equals(custTypeCMap.get(j).getStdProdCd())){
                            list.get(i).setTarget("true");//该条产品的生效日期是最新生效日期
                        }else if(list.get(i).getEndDt().compareTo(nowF)>0){//当失效日期大于当前日期时，为未失效，不置灰
                            list.get(i).setTarget("true");
                        }else{
                            list.get(i).setTarget("false");
                        }
                    }
                }else if("P".equals(list.get(i).getCustType())){
                    for (int j = 0; j < custTypePMap.size(); j++) {
                        if(custTypePMap.get(j).getStartDt().compareTo(nowF)<=0) {
                            if (list.get(i).getStdProdCd().equals(custTypePMap.get(j).getStdProdCd())) {
                                custTypePList.add(custTypePMap.get(j).getStartDt());
                            }
                        }
                        else{
                            custTypePList.add("0000");
                        }
                    }
                    for (int j = 0; j < custTypePMap.size(); j++) {
                        if(list.get(i).getStartDt().equals(Collections.max(custTypePList)) && list.get(i).getStdProdCd().equals(custTypePMap.get(j).getStdProdCd())){
                            list.get(i).setTarget("true");//该条产品的生效日期是最新生效日期
                        }else if(list.get(i).getEndDt().compareTo(nowF)>0){//当失效日期大于当前日期时，为未失效，不置灰
                            list.get(i).setTarget("true");
                        }else{
                            list.get(i).setTarget("false");
                        }
                    }
                }
                custTypeCMap.clear();
                custTypeCList.clear();
                custTypePMap.clear();
                custTypePList.clear();
            }
            return R.ok().put("list", list).put("count",count);
        }
    }

    /**
     * FTP存贷款数据修改
     */
    @PostMapping("/updateData")
    @SysLog("FTP存贷款数据修改")
    public R updateData(@RequestParam Map<String, Object> params) {
        Map depMap = new HashMap();//存款map
        Map loanMap = new HashMap();//贷款map
        //机构
        String initCode = (String) params.get("chOrgId");
        //产品大类（存款（D）还是贷款（C））
        String bizType = (String) params.get("bizType");
        //产品小类（个人存款，个人贷款，单位存款，单位贷款）
        String custType = (String) params.get("custType");
        //担保方式
        String guarType = (String) params.get("guarType");
        //产品子类（标准产品代码）
        String stdProdCd = (String) params.get("stdProdCd");
        //产品名称
        String stdProdNm = (String) params.get("stdProdNm");
        //价格
        String ftpPrice = (String) params.get("ftpPrice");
        //期限
        String term1 = (String) params.get("term");
        //价格标签
        String stdProdSign = (String) params.get("stdProdSign");
        //开始时间
        String startDt = (String) params.get("startDt");
        //结束时间
        String endDt = (String) params.get("endDt");
        Map map1 = new HashMap();
        //判断是存款还是贷款
        if("D".equals(bizType)){//如果是存款，则修改FTP存款利差表
            depMap.put("branchId",initCode);
            depMap.put("bizType",bizType);
            depMap.put("custType",custType);
            depMap.put("stdProdCd",stdProdCd);
            depMap.put("stdProdNm",stdProdNm);
            map1.put("busintypeid","FTP_COST_DEPOSIT");
            map1.put("busininame", term1);
            String term = prodStdProdCdService.queryTermId(map1);
            depMap.put("term",term);
            depMap.put("ftpPrice",ftpPrice);
            depMap.put("stdProdSign",stdProdSign);
            depMap.put("startDt",startDt);
            depMap.put("endDt",endDt);
            try {
                ftpLoanCostConfigService.updateDepositData(depMap);
            }catch (Exception e) {
                logger.info("updateDepositData error  is", e);
                return R.error(999999, e.getMessage());
            }
            return R.ok();
        }else{//否则修改FTP贷款成本参数表
            loanMap.put("branchId",initCode);
            loanMap.put("bizType",bizType);
            loanMap.put("custType",custType);
            loanMap.put("stdProdCd",stdProdCd);
            loanMap.put("stdProdNm",stdProdNm);
            map1.put("busintypeid","FTP_COST_LOAN");
            map1.put("busininame", term1);
            String term = prodStdProdCdService.queryTermId(map1);
            loanMap.put("term",term);
            loanMap.put("guarType",guarType);
            loanMap.put("ftpPrice",ftpPrice);
            loanMap.put("stdProdSign",stdProdSign);
            loanMap.put("startDt",startDt);
            loanMap.put("endDt",endDt);
            try {
                ftpLoanCostConfigService.updateLoanConstData(loanMap);
            }catch (Exception e) {
                logger.info("updateLoanConstData error  is", e);
                return R.error(999999, e.getMessage());
            }
            return R.ok();
        }
    }

    /**
     * FTP存贷款数据批量修改
     */
    @PostMapping("/updateBatchData")
    @SysLog("FTP存贷款数据修改")
    public R updateBatchData(@RequestParam Map<String, Object> params){
        Map depMap = new HashMap();//存款map
        Map loanMap = new HashMap();//贷款map
        //机构
        String chOrgId = (String) params.get("chOrgId");
        //产品大类（存款（D）还是贷款（C））
        String bizType = (String) params.get("bizType");
        //产品小类（个人存款，个人贷款，单位存款，单位贷款）
        String custType = (String) params.get("custType");
        //担保方式
        String guarType = (String) params.get("guarType");
        //产品子类（标准产品代码）
        String stdProdCd = (String) params.get("stdProdCd");
        //产品名称
        String stdProdNm = (String) params.get("stdProdNm");
        //价格
        String ftpPrice = (String) params.get("ftpPrice");
        //期限
        String term1 = (String) params.get("term");


        //价格标签
        String stdProdSign = (String) params.get("stdProdSign");
        //开始时间
        String startDt = (String) params.get("startDt");
        //结束时间
        String endDt = (String) params.get("endDt");
        Map map1 = new HashMap<>();
        //判断是存款还是贷款
        if("D".equals(bizType)){//如果是存款，则修改FTP存款利差表
            depMap.put("branchId",chOrgId);
            depMap.put("bizType",bizType);
            depMap.put("custType",custType);
            depMap.put("stdProdCd",stdProdCd);
            depMap.put("stdProdNm",stdProdNm);
            map1.put("busintypeid","FTP_COST_DEPOSIT");
            map1.put("busininame", term1);
            String term = prodStdProdCdService.queryTermId(map1);
            depMap.put("term",term);
            depMap.put("ftpPrice",ftpPrice);
            depMap.put("stdProdSign",stdProdSign);
            depMap.put("startDt",startDt);
            depMap.put("endDt",endDt);
            try {
                ftpLoanCostConfigService.updateDepositBatchData(depMap);
            }catch (Exception e) {
                logger.info("updateDepositData error  is", e);
                return R.error(999999, e.getMessage());
            }
            return R.ok();
        }else{//否则修改FTP贷款成本参数表
            loanMap.put("branchId",chOrgId);
            loanMap.put("bizType",bizType);
            loanMap.put("custType",custType);
            loanMap.put("stdProdCd",stdProdCd);
            loanMap.put("stdProdNm",stdProdNm);
            map1.put("busintypeid","FTP_COST_LOAN");
            map1.put("busininame", term1);
            String term = prodStdProdCdService.queryTermId(map1);
            loanMap.put("term",term);
            loanMap.put("guarType",guarType);
            loanMap.put("ftpPrice",ftpPrice);
            loanMap.put("stdProdSign",stdProdSign);
            loanMap.put("startDt",startDt);
            loanMap.put("endDt",endDt);
            try {
                ftpLoanCostConfigService.updateLoanConstBatchData(loanMap);
            }catch (Exception e) {
                logger.info("updateLoanConstData error  is", e);
                return R.error(999999, e.getMessage());
            }
            return R.ok();
        }
    }
    @RequestMapping("/getLoanListHis")
    public R getDataListHis(@RequestParam Map<String, Object> params){
        Map map = new HashMap();
        String datefrom = (String)params.get("datefrom");
        String dateto = (String)params.get("dateto");
        if (datefrom != null) {
            String str = datefrom.substring(0, 10);
            params.put("datefrom", str);
        }
        if (dateto != null) {
            String str = dateto.substring(0, 10);
            params.put("dateto", str);
        }
        if(null == params){
            return R.error("入参为空");
        }
        if(StringUtils.isEmpty((String) params.get("page")) && StringUtils.isEmpty((String) params.get("limit"))){
            return R.error("缺少必传分页参数");
        }
        List<FtpLoanCostConfigEntity> loanDataListHis = ftpLoanCostConfigService.getLoanDataListHis(params);
        int total = ftpLoanCostConfigService.getLoanDataListHisNb(params);
        return R.ok().put("data",loanDataListHis).put("count",total);
    }
    @RequestMapping("/getDepositListHis")
    public R getDepositListHis(@RequestParam Map<String, Object> params){
        Map map = new HashMap();
        String datefrom = (String)params.get("datefrom");
        String dateto = (String)params.get("dateto");
        if (datefrom != null) {
            String str = datefrom.substring(0, 10);
            params.put("datefrom", str);
        }
        if (dateto != null) {
            String str = dateto.substring(0, 10);
            params.put("dateto", str);
        }
        if(null == params){
            return R.error("入参为空");
        }
        if(StringUtils.isEmpty((String) params.get("page")) && StringUtils.isEmpty((String) params.get("limit"))){
            return R.error("缺少必传分页参数");
        }
        List<FtpDepositIntDiffConfigEntity> loanDataListHis = ftpLoanCostConfigService.getDepositListHis(params);
        int total = ftpLoanCostConfigService.queryDepositDataListNb(params);
        return R.ok().put("data",loanDataListHis).put("count",total);
    }
    @RequestMapping("/deleteData")
    public R deleteData(@RequestParam Map<String, Object> params){
        ftpLoanCostConfigService.deleteData(params);
        return R.ok();
    }
}
