package com.beyondsoft.modules.ftp.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.ftp.entity.FtpDepositIntDiffConfigEntity;
import com.beyondsoft.modules.ftp.entity.FtpLoanCostConfigEntity;

import java.util.List;
import java.util.Map;

/**
 *ftp存贷款详情
 */
public interface FtpLoanCostConfigService extends IService<FtpLoanCostConfigEntity> {

    /**
     * 查询跑批日期
     * @param reqPara
     * @return
     */
    String queryCurdate();

    /**
     *分页查询ftp存款详情
     */
    public int queryDepositDataListNb(Map reqPara);

    List<FtpDepositIntDiffConfigEntity> queryDepositDataList(Map params);

    /**
     *分页查询ftp贷款详情
     */
    public int queryLoanConstDataListNb(Map params);

    List<FtpLoanCostConfigEntity> queryLoanConstDataList(Map params);

    /**
     * 查詢ftp存款的开始日期与结束日期
     */
    List<FtpDepositIntDiffConfigEntity> queryDepositDateList(Map params);

    /**
     * 查詢ftp贷款的开始日期与结束日期
     */
    List<FtpLoanCostConfigEntity> queryLoanDateList(Map reqPara);

    /**
     * 修改ftp存款详情
     */
    public R updateDepositData(Map reqPara);

    /**
     * 修改ftp贷款详情
     */
    public R updateLoanConstData(Map reqPara);

    /**
     * 修改ftp存款详情
     */
    public R updateDepositBatchData(Map reqPara);

    /**
     * 修改ftp贷款详情
     */
    public R updateLoanConstBatchData(Map reqPara);

    List<FtpLoanCostConfigEntity> getLoanDataListHis(Map<String, Object> params);
    List<FtpDepositIntDiffConfigEntity> getDepositListHis(Map<String, Object> params);
    int getLoanDataListHisNb(Map<String, Object> params);
    void deleteData(Map reqPara);
}
