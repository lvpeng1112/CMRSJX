package com.beyondsoft.modules.ftp.controller;

import com.beyondsoft.modules.ftp.dao.FtpDepositIntDiffConfigDao;
import com.beyondsoft.modules.ftp.entity.FtpDepositIntDiffConfigEntity;
import com.beyondsoft.modules.ftp.entity.FtpLoanCostConfigEntity;
import com.beyondsoft.modules.ftp.service.FtpDepositIntDiffConfigService;
import com.beyondsoft.modules.ftp.service.FtpLoanCostConfigService;
import com.beyondsoft.modules.sys.entity.SysUserEntity;
import io.swagger.models.auth.In;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.beyondsoft.common.utils.R;

import javax.swing.event.ListDataListener;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("ftp/FtpDepositIntDiffConfig")
public class FtpDepositIntDiffConfigController {
    @Autowired
    private FtpDepositIntDiffConfigService ftpDepositIntDiffConfigService;
    @Autowired
    private FtpDepositIntDiffConfigDao ftpDepositIntDiffConfigDao;
    private Logger logger = LoggerFactory.getLogger(getClass());

    public SysUserEntity getUser() {
        return (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
    }

    public Long getUserId() {
        return getUser().getUserId();
    }

    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        Integer count = ftpDepositIntDiffConfigService.querynumber(params);
        List<Map<String,Object>> listDeposit1 =ftpDepositIntDiffConfigService.querFtpDeposit(params);
        for(int i =0;i<listDeposit1.size();i++ ){
            String a = (String) listDeposit1.get(i).get("BIZ_TYPE");
            String b =(String) listDeposit1.get(i).get("CUST_TYPE");
            if(a.equals("D")){
                listDeposit1.get(i).put("BIZ","存款");
            }else {
                listDeposit1.get(i).put("BIZ","贷款");
            }
            if(b.equals("P")){
                listDeposit1.get(i).put("CUST","个人");
            }else {
                listDeposit1.get(i).put("CUST","单位");
            }
        }
        List<Map<String,Object>> listDeposit = new ArrayList<Map<String,Object>>();
        for (int i =0 ;i<listDeposit1.size();i++){
            Map<String,Object> obdmap = new HashMap<String, Object>();
            Set<String> se = listDeposit1.get(i).keySet();
            for (String set : se){
                obdmap.put(set.toUpperCase(),listDeposit1.get(i).get(set));
            }
            listDeposit.add(obdmap);
        }
        List<Integer> one = new ArrayList<>();
        List<Integer> two = new ArrayList<>();
        List<Integer> three = new ArrayList<>();
        List<String> listone = new ArrayList<>();
        List<String> listtwo = new ArrayList<>();
        List<String> listthree = new ArrayList<>();
        List<Map<String,Object>> xxxx =ftpDepositIntDiffConfigService.querFtpDeposit1(params);
        if(xxxx.size()>0) {
            for (int b = 0; b < xxxx.size(); b++) {
                String ch = (String) (xxxx.get(b).get("CHDEPTCODE"));
                String bi = (String) (xxxx.get(b).get("BIZ_TYPE"));
                String cu = (String) (xxxx.get(b).get("CUST_TYPE"));
                listone.add(ch);
                listtwo.add(bi);
                listthree.add(cu);
            }
            List<Integer> sss = new ArrayList<>();
            sss.add(0);
            List<Integer> bbb = new ArrayList<>();
            bbb.add(0);
            int y = 1;
            for (int x = 1; x < listone.size(); x++) {
                if (listone.get(x - 1).equals(listone.get(x))) {
                    y = y + 1;
                } else {
                    one.add(y);
                    y = 1;
                }
            }
            one.add(y);
            int arr = 0;
            for (int m = 0; m < one.size(); m++) {
                arr = arr + Integer.parseInt(one.get(m).toString());
                sss.add(arr);
            }
            for (int m = 1; m < sss.size(); m++) {
                List<String> aa = listtwo.subList(sss.get(m - 1), sss.get(m));
                int h = 1;
                for (int g = 1; g < aa.size(); g++) {
                    if (aa.get(g - 1).equals(aa.get(g))) {
                        h = h + 1;
                    } else {
                        two.add(h);
                        h = 1;
                    }
                }
                two.add(h);
            }
            int brr = 0;
            for (int o = 0; o < two.size(); o++) {
                brr = brr + Integer.parseInt(two.get(o).toString());
                bbb.add(brr);
            }
            for (int q = 1; q < bbb.size(); q++) {
                List<String> bb = listthree.subList(bbb.get(q - 1), bbb.get(q));
                int r = 1;
                for (int t = 1; t < bb.size(); t++) {
                    if (bb.get(t - 1).equals(bb.get(t))) {
                        r = r + 1;
                    } else {
                        three.add(r);
                        r = 1;
                    }
                }
                three.add(r);
            }
        }
        return R.ok().put("one",one).put("two",two).put("three",three)
                .put("listDeposit",listDeposit)
                .put("count",count);
    }
    @RequestMapping("/getDataListHis")
    public R getDataListHis(@RequestParam Map<String, Object> params) {
        Integer count = ftpDepositIntDiffConfigService.querynumber1(params);
        List<Map<String, Object>> listDeposit2 = ftpDepositIntDiffConfigService.getDepositDataListHis(params);
        return R.ok().put("dataList",listDeposit2).put("count",count);
    }

    @RequestMapping("/save")
    public R insert(@RequestParam Map<String, Object> params){

            Integer map2 = ftpDepositIntDiffConfigService.queryIsExist(params);
            if (map2 != 0) {
                return R.error("数据已存在");
            } else {
                List<String> listchOrgId = ftpDepositIntDiffConfigService.querychOrgId();
                Boolean sign = Boolean.valueOf((String) params.get("sign"));
                Map<String, Object> map = new HashMap<>();
                List<Integer> stringList = new ArrayList<>();
                stringList.add(0000000);
                String stringCode = "";
                Date date = new Date();
//        String branchId = (String)params.get("BRANCH_ID");
//        map.put("branchId",branchId);
                String term = (String) params.get("TERM");
                map.put("term", term);
                String bizType = (String) params.get("BIZ_TYPE");
                map.put("bizType", bizType);
                String custType = (String) params.get("CUST_TYPE");
                map.put("custType", custType);
                String stdProdNm = (String) params.get("STD_PROD_NM");
                map.put("stdProdNm", stdProdNm);
                String ftpPrice = (String) params.get("FTP_PRICE");
                map.put("ftpPrice", ftpPrice);
                String startDt = (String) params.get("START_DT");

                if (startDt == null || ("").equals(startDt)) {
                    Date date1 = new Date();
                    SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
                    startDt = sdf1.format(date1);
                }
                map.put("startDt", startDt);
                String endDt = (String) params.get("END_DT");
                if (endDt == null || ("").equals(endDt)) {
                    Date date2 = new Date();
                    SimpleDateFormat sdf2 = new SimpleDateFormat("9999-12-31");
                    endDt = sdf2.format(date2);
                }
                map.put("endDt", endDt);
                String operators = getUser().getName();
                map.put("operators", operators);
                Timestamp operateDate = new Timestamp(date.getTime());
                map.put("operateDate", operateDate);
                String stdProdSign = (String) params.get("STD_PROD_SIGN");
                map.put("stdProdSign", stdProdSign);
                if (bizType.equals("C")) {
                    String guarType = (String) params.get("GUAR_TYPE");
                    map.put("guarType", guarType);
                    List<FtpLoanCostConfigEntity> loanCostlist = ftpDepositIntDiffConfigService.queryLoanCodeList();
                    for (int j = 0; j < loanCostlist.size(); j++) {
                        String code = loanCostlist.get(j).getStdProdCd();
                        Integer dd = Integer.valueOf(code.substring(2, code.length()));
                        stringList.add(dd);
                    }
                    Integer maxCode = Collections.max(stringList) + 1;//当前最大数字+1
                    if (maxCode.toString().length() >= 5) {
                        stringCode = maxCode.toString();
                    }
                    if (maxCode.toString().length() == 4) {
                        stringCode = "0" + maxCode;
                    }
                    if (maxCode.toString().length() == 3) {
                        stringCode = "00" + maxCode;
                    }
                    if (maxCode.toString().length() == 2) {
                        stringCode = "000" + maxCode;
                    }
                    if (maxCode.toString().length() == 1) {
                        stringCode = "0000" + maxCode;
                    }
                    String stdProdCd = custType + bizType + stringCode;//标准产品编码
                    map.put("stdProdCd", stdProdCd);
                    if (sign) {
                        for (int i = 0; i < listchOrgId.size(); i++) {
                            String branchId = listchOrgId.get(i);
                            if (branchId.endsWith("99")) {
                                map.put("branchId", branchId);
                                ftpDepositIntDiffConfigService.savaLoan(map);
                            }
                        }
                    } else {
                        String branchId = (String) params.get("BRANCH_ID");
                        map.put("branchId", branchId);
                        ftpDepositIntDiffConfigService.savaLoan(map);
                    }
                    return R.ok();
                } else {
                    List<FtpDepositIntDiffConfigEntity> depositlist = ftpDepositIntDiffConfigService.queryDepositCodeList();
                    for (int j = 0; j < depositlist.size(); j++) {
                        String code = depositlist.get(j).getStdProdCd();
                        Integer dd = Integer.valueOf(code.substring(2, code.length()));
                        stringList.add(dd);
                    }
                    Integer maxCode = Collections.max(stringList) + 1;//当前最大数字+1
                    if (maxCode.toString().length() >= 5) {
                        stringCode = maxCode.toString();
                    }
                    if (maxCode.toString().length() == 4) {
                        stringCode = "0" + maxCode;
                    }
                    if (maxCode.toString().length() == 3) {
                        stringCode = "00" + maxCode;
                    }
                    if (maxCode.toString().length() == 2) {
                        stringCode = "000" + maxCode;
                    }
                    if (maxCode.toString().length() == 1) {
                        stringCode = "0000" + maxCode;
                    }
                    String stdProdCd = custType + bizType + stringCode;//标准产品编码
                    map.put("stdProdCd", stdProdCd);
                    if (sign) {
                        for (int i = 0; i < listchOrgId.size(); i++) {

                            String branchId = listchOrgId.get(i);
                            if (branchId.endsWith("99")) {
                                map.put("branchId", branchId);
                                ftpDepositIntDiffConfigService.savaDeposit(map);
                            }
                        }
                    } else {
                        String branchId = (String) params.get("BRANCH_ID");
                        map.put("branchId", branchId);
                        ftpDepositIntDiffConfigService.savaDeposit(map);
                    }
                    return R.ok();
                }
            }

    }


    //    @RequestMapping("/queryTerm")
//    public R queryTerm(@RequestParam Map<String, Object> params){
//        List<String> terms = ftpDepositIntDiffConfigService.queryTerm(params);
//        return R.ok().put("terms",terms);
//    }
    @GetMapping("/dictList")
    public R dictList(@RequestParam Map<String, Object> params) {
        // 获取考核对象类别字典
        params.put("busintypeid", "FTP_VOUCH_TYPE");
        List<Map<String, Object>> guarList = ftpDepositIntDiffConfigDao.queryDictInfo(params);
        List<Map<String, Object>> termsList = ftpDepositIntDiffConfigService.queryTerm(params);

        return R.ok().put("guarList", guarList).put("termsList", termsList);
    }
    @GetMapping("/sourch")
    @ResponseBody
    public R sourch(@RequestParam Map<String, Object> params) {
        String bizType = (String)params.get("BIZ_TYPE");
        List<Map<String,Object>> stdList = null;
        if (bizType.equals("D")){
            stdList = ftpDepositIntDiffConfigService.queryDepositStp(params);
        }
        else if (bizType.equals("C")){
            stdList = ftpDepositIntDiffConfigService.queryLoanStp(params);
        }

        return R.ok().put("stdList", stdList);
    }
    @GetMapping("/searchbySdt")
    @ResponseBody
    public R searchbySdt(@RequestParam Map<String, Object> params) {
        String bizType = (String)params.get("BIZ_TYPE");
        String stdProdCd = (String)params.get("STD_PROD_CD");
        String branchId = (String)params.get("branchId");
        List<String> list = null;
        Map<String,Object> termList1 = null;
        List<Map<String,Object>> dataList = new ArrayList<>();
        Map map = new HashMap();
        Map reqMap = new HashMap()
                ;        if (bizType.equals("D")){
            map.put("bizType", bizType);
            map.put("stdProdCd", stdProdCd);
            map.put("branchId", branchId);
            list = ftpDepositIntDiffConfigService.queryDepositTerm(map);
            list.remove(null);
            if(!list.isEmpty()) {
                for (int i = 0; i < list.size(); i++) {
                    reqMap.put("busintypeid", "FTP_COST_DEPOSIT");//存款
                    reqMap.put("businid", list.get(i));
                    termList1 = ftpDepositIntDiffConfigDao.selectDictInfo(reqMap);
                    dataList.add(termList1);
                }
            }
        }
        else if (bizType.equals("C")){
            map.put("bizType", bizType);
            map.put("stdProdCd", stdProdCd);
            map.put("branchId", branchId);
            list = ftpDepositIntDiffConfigService.queryLoanTerm(map);
            list.remove(null);
            if(!list.isEmpty()) {
                for (int i = 0; i < list.size(); i++) {
                    reqMap.put("busintypeid", "FTP_COST_LOAN");//贷款
                    reqMap.put("businid", list.get(i));
                    termList1 = ftpDepositIntDiffConfigDao.selectDictInfo(reqMap);
                    dataList.add(termList1);
                }
            }
        }
        return R.ok().put("termList1", dataList);
    }
}