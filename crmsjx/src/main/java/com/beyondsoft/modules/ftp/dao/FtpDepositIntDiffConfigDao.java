package com.beyondsoft.modules.ftp.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.ftp.entity.FtpDepositIntDiffConfigEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface FtpDepositIntDiffConfigDao extends BaseMapper<FtpDepositIntDiffConfigEntity> {
    List<Map<String,Object>> querFtpDeposit(Map<String, Object> params);
    List<Map<String,Object>> querFtpDeposit1(Map<String, Object> params);

    List<String> queryChdeptName(Map<String, Object> params);

    Integer querynumber(Map<String, Object> params);

    List<Map<String,Object>> queryDictDetail(Map<String, Object> params);

    void savaDeposit(Map<String, Object> params);

    void savaLoan(Map<String, Object> params);

    List<Map<String,Object>> queryDictInfo(Map params);

    Map<String,Object> selectDictInfo(Map params);

    List<String> querychOrgId();

    List<String> queryDepositTerm();

    List<String> queryLoanTerm();

    List<Map<String,Object>> queryDepositStp(Map map);

    List<Map<String,Object>> queryLoanStp(Map map);

    List<String> queryDepositTerm(Map map);

    List<String> queryLoanTerm(Map map);

    String queryDepositStpNm(String string);

    String queryLoanStpNm(String string);

    List<Map<String,Object>> getDepositDataListHis(Map<String, Object> params);

    int getDepositDataListHisNb(Map<String, Object> params);

    Integer querynumber1(Map<String, Object> params);

    Integer queryIsExist(Map<String, Object> params);
}
