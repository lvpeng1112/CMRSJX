package com.beyondsoft.modules.ftp.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;


/**
 *
 *
 * @author luochao
 */
@Data
@TableName("NPMM.FTP_LOAN_COST_CONFIG")
public class FtpLoanCostConfigEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	//机构号
	private String branchId;
    //机构名称
    private String branchNm;
	//客户类型（P-个人、C-单位）
    private String custType;
    //客户类型名称（P-个人、C-单位）
    private String custTypeNm;
	//业务类型（D-存款、C-贷款）
	private String bizType;
	//业务类型名称
    private String bizTypeNm;
	//标准产品代码
	private String stdProdCd;
	//标准产品名称
	private String stdProdNm;
	//期限
	private String term;
	//担保方式
	private String guarType;
    //担保方式名称
    private String guarTypeNm;
	//FTP价格
	@JsonSerialize(using= ToStringSerializer.class)
	private Double ftpPrice;
	//生效日期
	private String startDt;
	//失效日期
	private String endDt;
	//操作员
	private String operators;
	//操作时间
	private String operateDate;
	//标准产品标签
	private String stdProdSign;
	//为了加以区分是否是最新有效数据
	private String target;
	//删除并修改上一条数据标识
	private String flag;
	//删除并修改时结束日期
	private String newEndDt;
}
