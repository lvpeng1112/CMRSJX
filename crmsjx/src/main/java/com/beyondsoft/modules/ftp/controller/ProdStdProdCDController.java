package com.beyondsoft.modules.ftp.controller;

import com.alibaba.druid.util.StringUtils;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.ftp.dao.FtpDepositIntDiffConfigDao;
import com.beyondsoft.modules.ftp.dao.ProdStdProdCdDao;
import com.beyondsoft.modules.ftp.entity.ProdStdProdCd;
import com.beyondsoft.modules.ftp.service.ProdStdProdCdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author wzy
 */
@RestController
@RequestMapping("/prodStdProdCD")
public class ProdStdProdCDController {
    @Autowired(required = false)
    private ProdStdProdCdService prodStdProdCdService;
    @RequestMapping("/list")
    public R queryData(@RequestParam Map<String, Object> params) {

        if (null == params) {
            return R.error("入参为空");
        }
        if (StringUtils.isEmpty((String) params.get("page")) && StringUtils.isEmpty((String) params.get("limit"))) {
            return R.error("缺少必传分页参数");
        }

        List<ProdStdProdCd> prodStdProdCds = prodStdProdCdService.queryData(params);
        for (int i = 0; i <prodStdProdCds.size() ; i++) {
            prodStdProdCds.remove(null);
            String term = prodStdProdCds.get(i).getTerm();
            String bizType = prodStdProdCds.get(i).getBizType();
            Map map = new HashMap();
            String termName = null;
            if (bizType.equals("C")) {
                map.put("busintypeid","FTP_COST_LOAN");//贷款
                map.put("businid",term);
                termName= prodStdProdCdService.queryTermName(map);
                prodStdProdCds.get(i).setTerm(termName);
            }
            else if (bizType.equals("D")){
                map.put("busintypeid","FTP_COST_DEPOSIT");//存款
                map.put("businid",term);
                termName= prodStdProdCdService.queryTermName(map);
                prodStdProdCds.get(i).setTerm(termName);
            }
        }
        int i = prodStdProdCdService.queryDataCount(params);

        return R.ok().put("prodStdProdCds", prodStdProdCds).put("count", i);
    }

    /**
     * 信息
     */


    @RequestMapping("/updateStdProdBatch")
    public R updateStdProdBatch(@RequestParam Map<String, Object> params) {
        prodStdProdCdService.updateStdProdBatch(params);
        return R.ok();
    }

    @RequestMapping("/updateStdProdOne")
    public R updateStdProdOne(@RequestParam Map<String, Object> params) {
        prodStdProdCdService.updateStdProdOne(params);
        return R.ok();
    }

    @RequestMapping("/queryStdProds")
    public R queryStdProdsByOrgIdBizType(@RequestParam Map<String, Object> params) {
        prodStdProdCdService.queryStdProdsByOrgId(params);
        return R.ok();
    }
    @GetMapping("/searchType")
    @ResponseBody
    public R sourch(@RequestParam Map<String, Object> params) {
//        String bizType = (String)params.get("BIZ_TYPE");

        List<Map<String,Object>> stdList = null;
//        Map map = new HashMap();
//        map.put("bizType", bizType);
        stdList = prodStdProdCdService.queryProdType(params);

        return R.ok().put("prodTypeList", stdList);
    }
    @GetMapping("/searchbyType")
    @ResponseBody
    public R searchbyType(@RequestParam Map<String, Object> params) {
        String bizType = (String)params.get("BIZ_TYPE");
        String PROD_TYPE = (String)params.get("PROD_TYPE");
        String branchId = (String)params.get("branchId");

        List<String> list = null;
        Map<String,Object> termList1 = null;
        List<Map<String,Object>> dataList = new ArrayList<>();
        Map map = new HashMap();
        Map reqMap = new HashMap();
        if (bizType.equals("D")){
            map.put("bizType", bizType);
            map.put("PROD_TYPE", PROD_TYPE);
            map.put("branchId", branchId);
            list = prodStdProdCdService.queryDepositTerm(map);
            for(int i=0;i<list.size();i++){
                reqMap.put("busintypeid","FTP_COST_DEPOSIT");//存款
                reqMap.put("businid",list.get(i));
                termList1 = prodStdProdCdService.selectDictInfo(reqMap);
                dataList.add(termList1);
            }
        }
        else if (bizType.equals("C")){
            map.put("bizType", bizType);
            map.put("PROD_TYPE", PROD_TYPE);
            map.put("branchId", branchId);
            list = prodStdProdCdService.queryLoanTerm(map);
            for(int i=0;i<list.size();i++){
                reqMap.put("busintypeid","FTP_COST_LOAN");//贷款
                reqMap.put("businid",list.get(i));
                termList1 = prodStdProdCdService.selectDictInfo(reqMap);
                dataList.add(termList1);
            }
        }

        return R.ok().put("termList", dataList);
    }
}
