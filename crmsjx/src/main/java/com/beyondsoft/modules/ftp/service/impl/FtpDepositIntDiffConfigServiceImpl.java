package com.beyondsoft.modules.ftp.service.impl;

import com.beyondsoft.datasource.annotation.DataSource;
import com.beyondsoft.modules.ftp.dao.FtpDepositIntDiffConfigDao;
import com.beyondsoft.modules.ftp.dao.FtpLoanConstConfigDao;
import com.beyondsoft.modules.ftp.entity.FtpDepositIntDiffConfigEntity;
import com.beyondsoft.modules.ftp.entity.FtpLoanCostConfigEntity;
import com.beyondsoft.modules.ftp.service.FtpDepositIntDiffConfigService;
import com.beyondsoft.modules.scheduling.dao.JobMetadataDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service("FtpDepositIntDiffConfigService")
public class FtpDepositIntDiffConfigServiceImpl extends ServiceImpl<FtpDepositIntDiffConfigDao, FtpDepositIntDiffConfigEntity> implements FtpDepositIntDiffConfigService{
    @Autowired(required = false)
    private FtpDepositIntDiffConfigDao ftpDepositIntDiffConfigDao;
    @Autowired(required = false)
    private FtpLoanConstConfigDao ftpLoanConstConfigDao;

    @Override
    @DataSource(value = "second")
    public List<Map<String, Object>> querFtpDeposit(Map<String, Object> params) {
        int pageNo = Integer.parseInt((String)params.get("page"));
        int pageCount = Integer.parseInt((String)params.get("limit"));
        int offset = ((pageNo - 1) * pageCount);
        params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
        params.put("offset", offset);
        List<Map<String, Object>> s = ftpDepositIntDiffConfigDao.querFtpDeposit(params);
        return s;
    }

    @Override
    @DataSource(value = "second")
    public List<Map<String, Object>> querFtpDeposit1(Map<String, Object> params) {
        List<Map<String, Object>> s = ftpDepositIntDiffConfigDao.querFtpDeposit1(params);
        return s;
    }

    @Override
    @DataSource(value = "second")
    public List<String> queryChdeptName(Map<String, Object> params) {
        List<String> list = ftpDepositIntDiffConfigDao.queryChdeptName(params);
        return list;
    }

    @Override
    @DataSource(value = "second")
    public Integer querynumber(Map<String, Object> params) {
        Integer number = ftpDepositIntDiffConfigDao.querynumber(params);
        return number;
    }

    @Override
    @DataSource(value = "second")
    public List<Map<String, Object>> queryDictDetail(Map<String, Object> params) {
        List<Map<String, Object>> deticl = ftpDepositIntDiffConfigDao.queryDictDetail(params);
        return deticl;
    }

    @Override
    @DataSource(value = "second")
    public List<FtpDepositIntDiffConfigEntity> queryDepositCodeList() {
        List<FtpDepositIntDiffConfigEntity> s = ftpLoanConstConfigDao.queryDepositCodeList();
        return s;
    }

    @Override
    @DataSource(value = "second")
    public List<FtpLoanCostConfigEntity> queryLoanCodeList() {
        List<FtpLoanCostConfigEntity> s = ftpLoanConstConfigDao.queryLoanCodeList();
        return s;
    }

    @Override
    @DataSource(value = "second")
    public void savaDeposit(Map<String, Object> params) {
        ftpLoanConstConfigDao.addDepositData(params);
    }

    @Override
    @DataSource(value = "second")
    public void savaLoan(Map<String, Object> params) {
        ftpLoanConstConfigDao.addLoanData(params);
    }

    @Override
    @DataSource(value = "second")
    public List<String> querychOrgId() {
        List<String> list = ftpDepositIntDiffConfigDao.querychOrgId();
        return list;
    }

    @Override
    public List<Map<String, Object>> queryTerm(Map<String, Object> params) {
        String bizType = (String)params.get("BIZ_TYPE");
        List<Map<String, Object>> terms = new ArrayList<>();
        if (bizType.equals("D")){
            params.put("busintypeid", "FTP_COST_DEPOSIT");
            terms = ftpDepositIntDiffConfigDao.queryDictInfo(params);
        }else if (bizType.equals("C")){
            params.put("busintypeid", "FTP_COST_LOAN");
            terms = ftpDepositIntDiffConfigDao.queryDictInfo(params);
        }
        return terms;
    }

    @Override
    @DataSource(value = "second")
    public List<Map<String,Object>> getDepositDataListHis(Map<String, Object> params) {
        int pageNo = Integer.parseInt((String) params.get("page"));
        int pageCount = Integer.parseInt((String)params.get("limit"));
        int offset = ((pageNo - 1) * pageCount);
        params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
        params.put("offset", offset);
        List<Map<String,Object>> depositDataListHis = ftpDepositIntDiffConfigDao.getDepositDataListHis(params);

        return depositDataListHis;
    }

    @Override
    @DataSource(value = "second")
    public int getDepositDataListHisNb(Map<String, Object> params) {
        int total = ftpDepositIntDiffConfigDao.getDepositDataListHisNb(params);
        return total;
    }

    @Override
    public int queryNumByNm(Map<String, Object> params) {
        return 0;
    }

    @Override
    public int queryLoanNumByNm(Map<String, Object> params) {
        return 0;
    }

    @Override
    @DataSource(value = "second")
    public List<Map<String, Object>> queryDepositStp(Map map) {
        return ftpDepositIntDiffConfigDao.queryDepositStp(map);
    }

    @Override
    @DataSource(value = "second")
    public List<Map<String, Object>> queryLoanStp(Map map) {
        return ftpDepositIntDiffConfigDao.queryLoanStp(map);
    }

    @Override
    @DataSource(value = "second")
    public List<String>  queryDepositTerm(Map map) {
        return ftpDepositIntDiffConfigDao.queryDepositTerm(map);
    }

    @Override
    @DataSource(value = "second")
    public List<String> queryLoanTerm(Map map) {
        return ftpDepositIntDiffConfigDao.queryLoanTerm(map);
    }

    @Override
    public Integer querynumber1(Map<String, Object> params) {
        Integer number = ftpDepositIntDiffConfigDao.querynumber1(params);
        return number;
    }

    @Override
    public Integer queryIsExist(Map<String, Object> params) {
        return ftpDepositIntDiffConfigDao.queryIsExist(params);
    }
}
