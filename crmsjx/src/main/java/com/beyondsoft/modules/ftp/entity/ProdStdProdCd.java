package com.beyondsoft.modules.ftp.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@TableName("C_PROD_STD_PROD_CD_CMP")
public class ProdStdProdCd implements Serializable {
    private static final long serialVersionUID = 1L;
    private String branchId;
    @TableField(exist = false)
    private String busininame;
    @TableField(exist = false)
    private  String chdeptname;
    private String bizType;
    private String prodType;
    private String prodTypeDesc;
    private String term;
    private String guarType;
    private String stdProdCd;
    private String stdProdNm;

    @TableField(exist = false)
    private String stdProNm;
    //FTP价格
    @JsonSerialize(using= ToStringSerializer.class)
    @TableField(exist = false)
    private String ftpPrice;
    //生效日期
    @TableField(exist = false)
    private String startDt;
    //失效日期
    @TableField(exist = false)
    private String endDt;

    @TableField(exist = false)
    private List<StdProd> stdProds;

    private String stdProdList;
}
