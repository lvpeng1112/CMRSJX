package com.beyondsoft.modules.ftp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.datasource.annotation.DataSource;
import com.beyondsoft.modules.ftp.dao.ProdStdProdCdDao;
import com.beyondsoft.modules.ftp.entity.FtpDepositIntDiffConfigEntity;
import com.beyondsoft.modules.ftp.entity.FtpLoanCostConfigEntity;
import com.beyondsoft.modules.ftp.entity.ProdStdProdCd;
import com.beyondsoft.modules.ftp.entity.StdProd;
import com.beyondsoft.modules.ftp.service.ProdStdProdCdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;

@Service("ProdStdProdCdService")
public class ProdStdProdCdServiceImpl extends ServiceImpl<ProdStdProdCdDao, ProdStdProdCd> implements ProdStdProdCdService {
    @Autowired(required = false)
    private ProdStdProdCdDao prodStdProdCdDao;

    @Override
    @DataSource(value = "second")
    public List<ProdStdProdCd> queryData(Map<String, Object> params) {
        int pageNo = Integer.parseInt((String) params.get("page"));
        int pageCount = Integer.parseInt((String) params.get("limit"));
        int offset = ((pageNo - 1) * pageCount);

        params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
        params.put("offset", offset);
        List<ProdStdProdCd> prodStdProdCds = prodStdProdCdDao.queryData(params);
        ArrayList<ProdStdProdCd> prodStdProds = new ArrayList<>();
//        添加标准产品下拉框数据
        for (ProdStdProdCd p : prodStdProdCds) {
            String branchId = p.getBranchId();
            String bizType = p.getBizType();
            String stdProdList = p.getStdProdList();
            List<FtpDepositIntDiffConfigEntity> ftpDeposit = null;
            List<FtpLoanCostConfigEntity> ftpLoan = null;
            List<StdProd> stdProds = new ArrayList<StdProd>();
            Map map = new HashMap();
            map.put("orgId", branchId);
            String[] stdProdSplit = stdProdList.split(",");
            for (String s : stdProdSplit) {
                StdProd stdProd = new StdProd();
                //将下拉列字符串进行拆分
                String[] split = s.split("\\)", 2);
                String stdProdCd = split[0].substring(1);
                String stdProdNm = s;
                stdProd.setId(stdProdCd);
                stdProd.setLabel(stdProdNm);
                stdProds.add(stdProd);
            }
            //下拉列按id进行排序
            stdProds.remove(null);
            stdProds.sort(Comparator.comparing(StdProd::getId));
            p.setStdProds(stdProds);

        }
        //stdProNm字段拼接标准产品代码，和业务类型变为文字,
        for (ProdStdProdCd p : prodStdProdCds) {
            String stdProNm = p.getStdProdNm();
            String stdProdCd = p.getStdProdCd();
            String branchId = p.getBranchId();
            stdProNm = "(" + stdProdCd + ")" + stdProNm;
            p.setStdProNm(stdProNm);
            if (p.getBizType().equals("D")) {
                p.setBusininame("存款");
            } else if (p.getBizType().equals("C")) {
                p.setBusininame("贷款");
            }
        }
        return prodStdProdCds;
    }

    @Override
    @DataSource(value = "second")
    public int queryDataCount(Map<String, Object> params) {
        int i = prodStdProdCdDao.queryDataCount(params);
        return i;
    }

    @Override
    @DataSource(value = "second")
    public void updateStdProdOne(Map<String, Object> params) {
        String stdProdCd = (String) params.get("stdProdCd");
        if (stdProdCd.indexOf("(") != -1) {
            String cdInfo = stdProdCd.substring(stdProdCd.indexOf("(") + 1, stdProdCd.indexOf(")"));
            params.put("stdProdCd", cdInfo);
        }
        prodStdProdCdDao.updateStdProdOne(params);
    }

    @Override
    @DataSource(value = "second")
    public void updateStdProdBatch(Map<String, Object> params) {
        HashMap<String, Object> map = new HashMap<>();
        String stdProdCd = (String) params.get("stdProdCd");
        if (stdProdCd.indexOf("(") != -1) {
            String cdInfo = stdProdCd.substring(stdProdCd.indexOf("(") + 1, stdProdCd.indexOf(")"));
            map.put("stdProdCd", cdInfo);
        } else {
            map.put("stdProdCd", stdProdCd);
        }
        String bizType = (String)params.get("bizType");
        map.put("bizType",bizType);
        //查询要一键修改的机构号
        List<String> stdProdCds = this.queryStdProdCdsByOrgId(map);
        map.put("list", stdProdCds);
        prodStdProdCdDao.updateStdProdBatchByStdProdCds(map);
    }

    @Override
    @DataSource(value = "second")
    public List<ProdStdProdCd> queryStdProdsByOrgId(Map<String, Object> params) {
        String orgId = (String) params.get("orgId");
        String bizType = (String) params.get("bizType");
        if (bizType.equals('D')) {
            prodStdProdCdDao.queryDepositStdProdsByOrgId(params);
        }
        if (bizType.equals('C')) {
            prodStdProdCdDao.queryLoanStdProdsByOrgId(params);
        }
        return null;
    }

    @Override
    @DataSource(value = "second")
    public List<String> queryStdProdCdsByOrgId(Map<String, Object> params) {
        List<String> strings = prodStdProdCdDao.queryStdProdCdsByOrgId(params);
        return strings;
    }

    @Override
    @DataSource(value = "second")
    public List<Map<String, Object>> queryProdType(Map map) {
        return prodStdProdCdDao.queryProdType(map);
    }

    @Override
    @DataSource(value = "second")
    public List<String>  queryDepositTerm(Map map) {
        return prodStdProdCdDao.queryDepositTerm(map);
    }

    @Override
    @DataSource(value = "second")
    public List<String> queryLoanTerm(Map map) {
        return prodStdProdCdDao.queryLoanTerm(map);
    }

    @Override
    public Map<String, Object> selectDictInfo(Map map) {
        return prodStdProdCdDao.selectDictInfo(map);
    }
    @Override
    public String queryTermName(Map map) {
        return prodStdProdCdDao.queryTermName(map);
    }

    @Override
    public String queryTermId(Map map) {
        return prodStdProdCdDao.queryTermId(map);
    }
}
