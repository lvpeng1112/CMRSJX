package com.beyondsoft.modules.ftp.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.modules.ftp.entity.ProdStdProdCd;

import java.util.List;
import java.util.Map;

public interface ProdStdProdCdService extends IService<ProdStdProdCd> {


    List<ProdStdProdCd> queryData(Map<String, Object> params);
    int queryDataCount(Map<String, Object> params);
    void updateStdProdOne(Map<String, Object> params);
    void updateStdProdBatch(Map<String, Object> params);
    List<ProdStdProdCd> queryStdProdsByOrgId(Map<String, Object> params);
    List<String> queryStdProdCdsByOrgId(Map<String, Object> params);

    List<Map<String, Object>> queryProdType(Map map);

    List<String> queryDepositTerm(Map map);

    List<String> queryLoanTerm(Map map);

    Map<String, Object> selectDictInfo(Map reqMap);

    String queryTermName(Map map);
     String queryTermId(Map map);

}
