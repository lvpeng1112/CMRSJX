package com.beyondsoft.modules.ftp.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.modules.ftp.entity.FtpDepositIntDiffConfigEntity;
import com.beyondsoft.modules.ftp.entity.FtpLoanCostConfigEntity;

import java.util.List;
import java.util.Map;

public interface FtpDepositIntDiffConfigService extends IService<FtpDepositIntDiffConfigEntity> {

    List<Map<String, Object>> querFtpDeposit(Map<String, Object> params);
    List<Map<String, Object>> querFtpDeposit1(Map<String, Object> params);

    List<String>  queryChdeptName(Map<String, Object> params);

    Integer querynumber(Map<String, Object> params);

    List<Map<String, Object>> queryDictDetail(Map<String, Object> params);

    List<FtpDepositIntDiffConfigEntity> queryDepositCodeList();

    List<FtpLoanCostConfigEntity> queryLoanCodeList();

    void savaDeposit(Map<String, Object> params);

    void savaLoan(Map<String, Object> params);

    List<String> querychOrgId();

    List<Map<String, Object>> queryTerm(Map<String, Object> params);

    List<Map<String,Object>> getDepositDataListHis(Map<String, Object> params);

    int getDepositDataListHisNb(Map<String, Object> params);

    int queryNumByNm(Map<String, Object> params);

    int queryLoanNumByNm(Map<String, Object> params);

    List<Map<String,Object>> queryDepositStp(Map map);

    List<Map<String,Object>> queryLoanStp(Map map);
    List<String> queryDepositTerm(Map map);

    List<String> queryLoanTerm(Map map);

    Integer querynumber1(Map<String, Object> params);

    Integer queryIsExist(Map<String, Object> params);
}
