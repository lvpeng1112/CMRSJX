package com.beyondsoft.modules.ftp.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.ftp.entity.FtpDepositIntDiffConfigEntity;
import com.beyondsoft.modules.ftp.entity.FtpLoanCostConfigEntity;
import com.beyondsoft.modules.ftp.entity.ProdStdProdCd;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface ProdStdProdCdDao  extends BaseMapper<ProdStdProdCd> {
    List<ProdStdProdCd> queryData(Map params);
    void updateStdProdOne(Map params);
    void updateStdProdBatch(Map params);
    List<ProdStdProdCd> queryStdProdsByOrgIdBizType(Map params);
    List<FtpDepositIntDiffConfigEntity> queryDepositStdProdsByOrgId(Map params);
    List<FtpLoanCostConfigEntity> queryLoanStdProdsByOrgId(Map params);
    int queryDataCount(Map params);
    List<String> queryStdProdCdsByOrgId(Map params);
    void updateStdProdBatchByStdProdCds(Map params);
    Double queryDPriceByStdCdStarDate(Map params);
    Double queryCPriceByStdCdStarDate(Map params);

    List<Map<String, Object>> queryProdType(Map map);

    List<String> queryDepositTerm(Map map);

    List<String> queryLoanTerm(Map map);

    Map<String,Object> selectDictInfo(Map map);

    String queryTermName(Map map);
    String queryTermId(Map map);
}
