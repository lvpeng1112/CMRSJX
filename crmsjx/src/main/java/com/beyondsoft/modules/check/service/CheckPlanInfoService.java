package com.beyondsoft.modules.check.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.modules.check.entity.CheckPlanInfoEntity;

import java.util.List;
import java.util.Map;

/**
 * 方案管理
 *
 * @author liushuai
 */
public interface CheckPlanInfoService extends IService<CheckPlanInfoEntity> {
	// 查询方案管理记录数
	int queryCheckInfoNb(Map<String, Object> params);
	// 查询方案管理名单信息
	List<CheckPlanInfoEntity> queryCheckPlanInfo(Map<String, Object> params);

	void deleteById(String id);

	CheckPlanInfoEntity queryById(String id);

	void insertCheck(CheckPlanInfoEntity param);

	void updateCheck(CheckPlanInfoEntity param);
}
