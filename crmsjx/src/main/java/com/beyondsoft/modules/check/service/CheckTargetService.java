package com.beyondsoft.modules.check.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.modules.check.entity.CheckTargetEntity;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;
import java.util.Map;
/**
 * 考核目标导入及查询
 *
 * @author yuchong
 */
public interface CheckTargetService extends IService<CheckTargetEntity> {
	// 查询考核目标记录数
	int queryChkTrgInfoNb(Map<String, Object> params);
	// 查询考核目标信息
	List<CheckTargetEntity> queryChkTrgInfo(Map<String, Object> params);
	// 更新考核目标信息
	String updChkTrgInfo(Map<String, Object> params);

	String batchImport(String fileName, MultipartFile file, String checkPlanId);
	// 更新考核方案信息
	List<CheckTargetEntity>  queryChkPlanInfo(Map<String, Object> params);
}