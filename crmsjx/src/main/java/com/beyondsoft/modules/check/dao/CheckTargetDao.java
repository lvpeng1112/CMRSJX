package com.beyondsoft.modules.check.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.check.entity.CheckTargetEntity;
import com.beyondsoft.modules.sys.entity.SysUserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 考核目标导入及查询
 *
 * @author yuchong
 */
@Mapper
@Repository
public interface CheckTargetDao extends BaseMapper<CheckTargetEntity> {
    // 查询考核目标记录数
    int queryChkTrgInfoNb(Map<String, Object> params);
    // 查询考核目标信息
    List<CheckTargetEntity> queryChkTrgInfo(Map<String, Object> params);
    // 更新考核目标信息
    void updChkTrgInfo(Map<String, Object> params);
    // 查询字典表
    List<Map<String,Object>> queryDictInfo(Map<String, Object> params);
    // 查询考核目标表信息是否存在
    int countCsrMgrInfoNb(Map<String, Object> params);
    // 插入考核目标信息
    void insChkTrgInfo(List<CheckTargetEntity> list);
    // 根据用户名编号，查询用户名称
    SysUserEntity queryByUserId(Long userId);
    // 查询考核方案信息
    List<CheckTargetEntity> queryChkPlanInfo(Map<String, Object> params);
}
