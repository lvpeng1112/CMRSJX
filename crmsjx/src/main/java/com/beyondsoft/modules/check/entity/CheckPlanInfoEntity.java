package com.beyondsoft.modules.check.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class CheckPlanInfoEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 考核方案编号
     */
    private String checkPlanId;
    /**
    * 考核方案名称
    */
    private String checkPlanNm;
    /**
     * 考核方案描述
     */
    private String checkPlanDesc;
    /**
     * 考核对象类别:E-客户经理,O-机构,T-团队
     */
    private String checkObjType;
    /**
     * 方案设置条线:1-零售、2-公金
     */
    private String setPlanBuss;
    /**
     * 方案设置机构
     */
    private String setPlanOrg;
    /**
     * 方案有效期开始日
     */
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date planStartDate;
    /**
     * 方案有效期到期日
     */
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date planEndDate;
    /**
     * 方案状态：1-生效；0-失效
     */
    private String planStatus;
    /**
     * 操作人
     */
    private String operId;
    /**
     * 操作时间
     */
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date operTime;

    //考核对象类别字典
    private String objectNm;

    //方案设置条线字典
    private String setNm;

    //方案状态字典
    private String statusNm;

    //机构名称
    private String chdeptNm;
}
