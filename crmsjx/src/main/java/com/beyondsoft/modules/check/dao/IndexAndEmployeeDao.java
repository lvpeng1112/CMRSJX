/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.check.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.check.entity.*;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 指标管理
 *
 */
@Mapper
public interface IndexAndEmployeeDao extends BaseMapper<IndexEntity> {

	List<CheckPlanInfoEntity> queryPlanIdName();

	List<String> findIndexIdByPlanId(Map reqPara);

	List<String> findEmployeeIdByPlanId(Map reqPara);

	List<IndexEntity> queryIndexDataList(Map reqPara);

	int queryExistIndexPlan(Map reqPara);

	int queryExistEmployeePlan(Map reqPara);

	IndexPlanRelaEntity queryIndexDataByPlanId(Map reqPara);

	EmployeePlanRelaEntity queryEmployeeDataByPlanId(Map reqPara);

	/**
	 * 查询员工信息
	 */
	List<EmployeeEntity> empleeList(Map reqPara);

	void saveIndexPlanData(Map reqPara);

	void saveEmployeePlanData(Map reqPara);

	void deletePlanAndIndex(Map reqPara);

	void deletePlanAndEmployee(Map reqPara);
}
