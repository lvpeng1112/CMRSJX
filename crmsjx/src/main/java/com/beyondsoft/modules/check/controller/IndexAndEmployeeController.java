package com.beyondsoft.modules.check.controller;

import com.beyondsoft.common.annotation.SysLog;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.check.dao.IndexAndEmployeeDao;
import com.beyondsoft.modules.check.entity.CheckPlanInfoEntity;
import com.beyondsoft.modules.check.entity.EmployeeEntity;
import com.beyondsoft.modules.check.entity.IndexEntity;
import com.beyondsoft.modules.check.service.IndexAndEmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * @author luochao
 * @email
 * @date 2020-11-19
 */
@RestController
@RequestMapping("plan/queryIndexAndEmployee")
public class IndexAndEmployeeController {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private IndexAndEmployeeService indexAndEmployeeService;

    @Autowired(required = false)
    private IndexAndEmployeeDao indexAndEmployeeDao;

    /**
     * 考核方案列表
     */
    @GetMapping("/queryPlanInfosList")
    public R queryPlanInfosList() {
        List<CheckPlanInfoEntity> planInfosList = indexAndEmployeeDao.queryPlanIdName();
        return R.ok().put("planInfosList",planInfosList);
    }

    /**
     * 指标与客户经理名称列表
     */
    @GetMapping("/queryIndexAndEmployeeList")
    public R queryIndexAndEmployeeList(@RequestParam Map<String, Object> params) {
        String checkPlanId = (String)params.get("checkPlanId");
        String initCode = (String)params.get("initCode");
        params.put("checkPlanId",checkPlanId);
        if(initCode != null && initCode != ""){
            params.put("org",initCode);
        }
        List<CheckPlanInfoEntity> planInfosList = new ArrayList<CheckPlanInfoEntity>();
        List<String> indexIdList = new ArrayList<>();
        List<String> employeeIdList = new ArrayList<>();
        if( checkPlanId != null && checkPlanId !=""){
            planInfosList = indexAndEmployeeDao.queryPlanIdName();
            indexIdList = indexAndEmployeeDao.findIndexIdByPlanId(params);//该方案已关联的指标id
            employeeIdList= indexAndEmployeeDao.findEmployeeIdByPlanId(params);//该方案已存在的客户经理id
        }
        List<IndexEntity> queryIndexDataList = indexAndEmployeeService.queryIndexDataList(params);//指标列表数据
        List<EmployeeEntity> employeeEntityList = indexAndEmployeeService.empleeList(params);//客户经理列表数据
        return R.ok().put("queryIndexDataList", queryIndexDataList).put("employeeEntityList",employeeEntityList)
                .put("indexIdList",indexIdList).put("employeeIdList",employeeIdList).put("planInfosList",planInfosList);
    }

    /**
     * 方案与指标考核对象新增/修改
     */
    @PostMapping(("/saveIndexAndEmployeeForPlanData"))
    @SysLog("方案与指标考核对象新增/修改")
    public R saveIndexAndEmployeeForPlanData(@RequestParam Map<String, Object> params) {
        boolean falg = true;
        String checkPlanId = (String)params.get("checkPlanId");
        params.put("checkPlanId",checkPlanId);
        int indexPlanCount = indexAndEmployeeDao.queryExistIndexPlan(params);//查询该方案是否已经存在指标相关数据
        int existEmployeeCount = indexAndEmployeeDao.queryExistEmployeePlan(params);//查询该方案是否已经存在客户经理相关数据
        if (indexPlanCount > 0) {//如果存在则进行删除
            try {
                indexAndEmployeeDao.deletePlanAndIndex(params);
            } catch (Exception e) {
                logger.info("deletePlanAndIndex error  is", e);
                return R.error(999999, e.getMessage());
            }
        }

        if (existEmployeeCount > 0) {//如果存在则进行删除
            try {
                indexAndEmployeeDao.deletePlanAndEmployee(params);
            } catch (Exception e) {
                logger.info("deletePlanAndEmployee error  is", e);
                return R.error(999999, e.getMessage());
            }
        }
        try {
            indexAndEmployeeService.saveIndexAndEmployeeForPlanData(params);
        } catch (Exception e) {
            logger.info("saveIndexAndEmployeeForPlanData error  is", e);
            return R.error(999999, e.getMessage());
        }
        return R.ok();
//        IndexPlanRelaEntity indexPlanRelaEntity = null;
//        EmployeePlanRelaEntity employeePlanRelaEntity = null;
//        String refIndName = null;
//        String refObjName = null;
//        String indId = (String)params.get("indId");
//        String[] indexList = indId.split(",");//将前台传回来的指标ID字符串进行分割
//        for (int i = 0; i < indexList.length; i++) {
//            params.put("refIndId",indexList[i]);
//            indexPlanRelaEntity = indexAndEmployeeDao.queryIndexDataByPlanId(params);//判断该方案下面是否存所选指标
//            if (indexPlanRelaEntity != null) {
//                refIndName = indexPlanRelaEntity.getRefIndName();
//                falg = false;
//                return R.error(999999, "该方案已存在:'"+refIndName+"' 指标数据，请重新选择！");
//            }
//        }
//        String empCd = (String)params.get("empCd");
//        String[] empCdList = empCd.split(",");//将前台传回来的客户经理编号字符串进行分割
//        for (int i = 0; i < empCdList.length; i++) {
//            params.put("refObjId",empCdList[i]);
//            employeePlanRelaEntity = indexAndEmployeeDao.queryEmployeeDataByPlanId(params);//判断该方案下面是否存所选客户经理
//            if(employeePlanRelaEntity != null){
//                falg = false;
//                refObjName = employeePlanRelaEntity.getRefObjName();
//                return R.error(999999, "该方案已存在:'"+refObjName+"' 客户经理数据，请重新选择！");
//            }
//        }
//        if( falg =true){
//            try {
//                indexAndEmployeeService.saveIndexAndEmployeeForPlanData(params);
//            } catch (Exception e) {
//                logger.info("saveIndexAndEmployeeForPlanData error  is", e);
//                return R.error(999999, e.getMessage());
//            }
//            return R.ok();
//        }else{
//            return null;
//        }
    }
}
