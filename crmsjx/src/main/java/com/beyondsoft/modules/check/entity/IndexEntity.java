/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.check.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 指标实体类
 *
 */
@Data
@TableName("ind_info")
public class IndexEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	private String indId;
	private String indName;
	private String indDesc;
	private String indType;
	private String indTypeNm;
	private String indDim;
	private String indDimNm;
	private String compType;
	private String compTypeNm;
	private String bussLine;
	private String bussLineNm;
	private String bussType;
	private String bussTypeNm;
	private String valType;
	private String valTypeNm;
	private String actObj;
	private String actObjNm;
	private String operId;
	private String operTime;
	private String sdate;
	private String edate;
	private String remark1;
	private String remark2;
}
