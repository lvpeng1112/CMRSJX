package com.beyondsoft.modules.check.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.modules.check.dao.CheckPlanInfoDao;
import com.beyondsoft.modules.check.entity.CheckPlanInfoEntity;
import com.beyondsoft.modules.check.service.CheckPlanInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Service("checkPlanInfoService")
public class CheckPlanInfoServiceImpl extends ServiceImpl<CheckPlanInfoDao, CheckPlanInfoEntity> implements CheckPlanInfoService {
	@Autowired(required = false)
	private CheckPlanInfoDao checkPlanInfoDao;

	@Override
	public int queryCheckInfoNb(Map<String, Object> params) {
		String checkPlanNm = (String) params.get("checkPlanNm");
		params.put("checkPlanNm", checkPlanNm);
		int count = checkPlanInfoDao.queryCheckInfoNb(params);
		return count;
	}

	@Override
	public List<CheckPlanInfoEntity> queryCheckPlanInfo(Map<String, Object> params) {
		String checkPlanNm = (String) params.get("checkPlanNm");
		int pageNo = Integer.parseInt((String) params.get("page"));
		int pageCount = Integer.parseInt((String)params.get("limit"));
		int offset = ((pageNo - 1) * pageCount);
		params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
		params.put("offset", offset);
		params.put("checkPlanNm", checkPlanNm);
		List<CheckPlanInfoEntity> checkPlanInfoEntityList = new ArrayList<>();
		checkPlanInfoEntityList = checkPlanInfoDao.queryCheckPlanInfo(params);
		return checkPlanInfoEntityList;
	}

	@Override
	public void deleteById(String id) {
		checkPlanInfoDao.deleteById(id);
		checkPlanInfoDao.deleteIndById(id);
		checkPlanInfoDao.deleteObjById(id);
		checkPlanInfoDao.deleteTargetById(id);
	}

	@Override
	public CheckPlanInfoEntity queryById(String id) {
		return checkPlanInfoDao.queryById(id);
	}

	@Override
	public void insertCheck(CheckPlanInfoEntity param) {
		checkPlanInfoDao.insertCheck(param);
	}

	@Override
	public void updateCheck(CheckPlanInfoEntity param) {
		checkPlanInfoDao.updateCheck(param);
	}
}
