/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.check.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.check.entity.CheckPlanInfoEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 方案管理
 *
 * @author liushuai
 */
@Mapper
public interface CheckPlanInfoDao extends BaseMapper<CheckPlanInfoEntity> {
	// 查询方案管理记录数
	int queryCheckInfoNb(Map<String, Object> params);
	// 查询方案管理名单信息
	List<CheckPlanInfoEntity> queryCheckPlanInfo(Map<String, Object> params);

	void deleteById(String id);

	void deleteIndById(String id);

	void deleteObjById(String id);

	void deleteTargetById(String id);

	CheckPlanInfoEntity queryById(String id);

	void insertCheck(CheckPlanInfoEntity param);

	void updateCheck(CheckPlanInfoEntity param);
}
