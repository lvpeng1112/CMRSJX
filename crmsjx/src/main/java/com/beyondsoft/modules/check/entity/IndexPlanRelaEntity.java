package com.beyondsoft.modules.check.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("check_plan_ind_rela")
public class IndexPlanRelaEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    private String checkPlanId;
    private String refIndId;
    private String refIndName;
}
