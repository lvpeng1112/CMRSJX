/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.check.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.check.dao.IndexAndEmployeeDao;
import com.beyondsoft.modules.check.entity.EmployeeEntity;
import com.beyondsoft.modules.check.entity.IndexEntity;
import com.beyondsoft.modules.check.service.IndexAndEmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * 指标
 *
 */
@Service("IndexAndEmployeeService")
@Transactional
public class IndexAndEmployeeServiceImpl extends ServiceImpl<IndexAndEmployeeDao,IndexEntity> implements IndexAndEmployeeService {
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired(required = false)
	private IndexAndEmployeeDao indexAndEmployeeDao;


    @Override
    public List<EmployeeEntity> empleeList(Map reqPara) {
        List<EmployeeEntity> empleeList = indexAndEmployeeDao.empleeList(reqPara);
        return empleeList;
    }

    @Override
    public List<IndexEntity> queryIndexDataList(Map reqPara) {
        List<IndexEntity> queryIndexDetail = indexAndEmployeeDao.queryIndexDataList(reqPara);
        return queryIndexDetail;
    }

    @Override
    public R saveIndexAndEmployeeForPlanData(Map params) {
        String checkPlanId = (String)params.get("checkPlanId");
        params.put("checkPlanId",checkPlanId);
        String indId = (String)params.get("indId");
        String[] indexList = indId.split(",");//将前台传回来的指标ID字符串进行分割
        for (int i = 0; i < indexList.length; i++) {
            params.put("refIndId",indexList[i]);
            List<IndexEntity> indexEntities = indexAndEmployeeDao.queryIndexDataList(params);//查询所选指标名称
            String refIndName = indexEntities.get(0).getIndName();
            params.put("refIndName",refIndName);
            indexAndEmployeeDao.saveIndexPlanData(params);
        }
        String empCd = (String)params.get("empCd");
        String[] empCdList = empCd.split(",");//将前台传回来的客户经理编号字符串进行分割
        for (int i = 0; i < empCdList.length; i++) {
            params.put("refObjId",empCdList[i]);
            List<EmployeeEntity> employeeEntityList = indexAndEmployeeDao.empleeList(params);//查询所选客户经理名字
            String refObjName = employeeEntityList.get(0).getEmpNm();
            params.put("refObjName",refObjName);
            indexAndEmployeeDao.saveEmployeePlanData(params);
        }
        return R.ok();
    }

}
