package com.beyondsoft.modules.check.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("employee")
public class EmployeeEntity  implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 年度
     */
    private String dtDate;
    /**
     * 机构号
     */
    private String org;
    /**
     * 客户经理号
     */
    private String empCd;
    /**
     * 客户经理名称
     */
    private String empNm;
    /**
     * 起始日
     */
    private String stDt;
    /**
     * 到期日
     */
    private String endDt;
}
