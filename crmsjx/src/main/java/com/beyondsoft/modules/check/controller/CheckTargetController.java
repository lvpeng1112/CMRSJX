package com.beyondsoft.modules.check.controller;

import com.alibaba.druid.util.FnvHash;
import com.beyondsoft.common.annotation.SysLog;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.check.dao.CheckTargetDao;
import com.beyondsoft.modules.check.entity.CheckTargetEntity;
import com.beyondsoft.modules.check.service.CheckTargetService;
import com.beyondsoft.modules.app.utils.JwtUtils;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

import static org.springframework.util.ResourceUtils.CLASSPATH_URL_PREFIX;

/**
 * 考核目标导入及查询
 *
 * @author yuchong
 */
@RestController
@RequestMapping("/check/target")
public class CheckTargetController {
    @Autowired
    private CheckTargetService checkTargetService;
    @Autowired
    private CheckTargetDao checkTargetDao;
    @Autowired
    private JwtUtils jwtUtils;

    /**
     * 查询
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        // 查询考核目标记录数
        int count = checkTargetService.queryChkTrgInfoNb(params);
        // 查询考核目标信息
        List<CheckTargetEntity> chkTrgInfoList = checkTargetService.queryChkTrgInfo(params);
        return R.ok().put("list", chkTrgInfoList).put("totalCount", count);
    }

    /**
     * 查询字典表
     */
    @GetMapping("/dictList")
    public R dictList(@RequestParam Map<String, Object> params){
        // 获取计划值周期列表
        params.put("busintypeid", "PLAN_CYCLE");
        List<Map<String,Object>> planCycleList = checkTargetDao.queryDictInfo(params);

        // 获取计划值类型列表
        params.put("busintypeid", "PLAN_VAL_TYPE");
        List<Map<String,Object>> planValTypList = checkTargetDao.queryDictInfo(params);
        return R.ok().put("planCycleList", planCycleList).put("planValTypList", planValTypList);
    }

    /**
     * 更新
     */
    @SysLog("考核目标更新")
    @RequestMapping("/updChkTrgInfo")
    public R updChkTrgInfo(@RequestParam Map<String, Object> params){
        String msg = checkTargetService.updChkTrgInfo(params);
        if ("".equals(msg)) {
            return R.ok();
        } else {
            return R.error(msg);
        }
    }
    /**
     * 导入
     */
    @SysLog("考核目标导入")
    @RequestMapping("/importData")
    @ApiOperation("导入excel")
    public R importData(@RequestParam(value="checkPlanId") String checkPlanId,@RequestParam(value = "file") MultipartFile file) {
        String fileName = file.getOriginalFilename();
//        String checkPlanId = (String) params.get("checkPlanId");
        String msg = checkTargetService.batchImport(fileName, file, checkPlanId);
        if ("".equals(msg)) {
            return R.ok();
        } else {
            return R.error(msg);
        }
    }

    /**
     * 模板文件下载
     */
    @RequestMapping("/queryChkPlanInfo")
    @ApiOperation("模板文件数据查询")
    public R queryChkPlanInfo(@RequestParam Map<String, Object> params){
        List<CheckTargetEntity> chkPlanInfoList = checkTargetService.queryChkPlanInfo(params);
        return R.ok().put("list", chkPlanInfoList);
    }
}
