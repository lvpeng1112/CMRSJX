package com.beyondsoft.modules.check.entity;

import lombok.Data;
import java.io.Serializable;


/**
 *
 *
 * @author yuchong
 */
@Data
public class CheckTargetEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 考核方案编号
	 */
	private String checkPlanId;

	/**
	 * 考核方案名称
	 */
	private String checkPlanNm;
	/**
	 * 考核对象ID
	 */
	private String refObjId;
	/**
	 * 考核对象名称
	 */
	private String refObjName;
	/**
	 * 引用指标ID
	 */
	private String refIndId;
	/**
	 * 引用指标名称
	 */
	private String refIndName;
	/**
	 * 计划值周期 ：M-月，Q-季度，Y-年
	 */
	private String planCycle;
    /**
     * 计划值周期名 ：M-月，Q-季度，Y-年
     */
    private String planCycleNm;
	/**
	 * 计划周期开始日期
	 */
	private String planCycleStart;
	/**
	 * 计划周期结束日期
	 */
	private String planCycleEnd;
	/**
	 * 计划值
	 */
	private String planVal;
	/**
	 * 计划值类型 0-余额、1-金额、2-笔数、3-户数、4-百分比、5-分数
	 */
	private String planValType;
    /**
     * 计划值类型名 0-余额、1-金额、2-笔数、3-户数、4-百分比、5-分数
     */
    private String planValTypeNm;
	/**
	 * 操作人
	 */
	private String operId;
	/**
	 * 操作时间
	 */
	private String operTime;

}
