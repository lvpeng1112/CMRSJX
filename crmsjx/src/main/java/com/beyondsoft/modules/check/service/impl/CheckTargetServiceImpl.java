package com.beyondsoft.modules.check.service.impl;

import java.io.InputStream;
import java.util.*;
import java.util.Map;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.utils.DateUtil;
import com.beyondsoft.utils.StrUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.beyondsoft.modules.check.dao.CheckTargetDao;
import com.beyondsoft.modules.sys.entity.SysUserEntity;
import com.beyondsoft.modules.check.entity.CheckTargetEntity;
import com.beyondsoft.modules.check.service.CheckTargetService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;


@Service("checkTargetService")
public class CheckTargetServiceImpl extends ServiceImpl< CheckTargetDao, CheckTargetEntity> implements  CheckTargetService {
	@Autowired(required = false)
	private CheckTargetDao checkTargetDao;
    public SysUserEntity getUser() {
        return (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
    }
    public Long getUserId() {
        return getUser().getUserId();
    }

	@Override
	public int queryChkTrgInfoNb(Map<String, Object> params) {
        String checkPlanId = (String) params.get("checkPlanId");
        String checkPlanNm = (String) params.get("checkPlanNm");
        String refObjName = (String) params.get("refObjName");
        params.put("checkPlanId", checkPlanId);
        params.put("checkPlanNm", checkPlanNm);
        params.put("refObjName", refObjName);
		int count = checkTargetDao.queryChkTrgInfoNb(params);
		return count;
	}

	@Override
	public List<CheckTargetEntity> queryChkTrgInfo(Map<String, Object> params) {
		String checkPlanId = (String) params.get("checkPlanId");
		String checkPlanNm = (String) params.get("checkPlanNm");
		String refObjName = (String) params.get("refObjName");
		int pageNo = Integer.parseInt((String) params.get("page"));
		int pageCount = Integer.parseInt((String)params.get("limit"));
		int offset = ((pageNo - 1) * pageCount);
		params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
		params.put("offset", offset);
		params.put("checkPlanId", checkPlanId);
		params.put("checkPlanNm", checkPlanNm);
		params.put("refObjName", refObjName);
		List<CheckTargetEntity> chkTrgInfoList = new ArrayList<>();
		chkTrgInfoList = checkTargetDao.queryChkTrgInfo(params);
		return chkTrgInfoList;
	}

	@Override
	public String updChkTrgInfo(Map<String, Object> params) {
        SysUserEntity sysUserEntity = checkTargetDao.queryByUserId(getUserId());
		String checkPlanId = (String) params.get("checkPlanId");
		String refObjId = (String) params.get("refObjId");
        String refIndId = (String) params.get("refIndId");
        String planCycle = (String) params.get("planCycle");
        String planCycleStart = (String) params.get("planCycleStart");
        String planCycleEnd = (String) params.get("planCycleEnd");
        String planValType = (String) params.get("planValType");
        String planVal = (String) params.get("planVal");

		// 计划值开始日期-计划值结束日期周期check
		String stDate = planCycleStart.substring(0,4) + planCycleStart.substring(5,7) + planCycleStart.substring(8,10);
		String endDate = planCycleEnd.substring(0,4) + planCycleEnd.substring(5,7) + planCycleEnd.substring(8,10);
		if ("Y".equals(planCycle)) {
			if (!DateUtil.calcMonth(stDate, 12).equals(endDate)) {
				return ("计划值周期为【年】时,计划值开始日期【" + planCycleStart + "】至计划值结束日期【" + planCycleEnd + "】必须为一整年");
			}
		} else if ("Q".equals(planCycle)) {
			if (!DateUtil.calcMonth(stDate, 3).equals(endDate)) {
				return ("计划值周期为【季】时,计划值开始日期【" + planCycleStart + "】至计划值结束日期【" + planCycleEnd + "】必须为一整季度");
			}
		} else if ("M".equals(planCycle)) {
			if (!DateUtil.calcMonth(stDate, 1).equals(endDate)) {
				return ("计划值周期为【月】时,计划值开始日期【" + planCycleStart + "】至计划值结束日期【" + planCycleEnd + "】必须为一整月");
			}
		}

		// 计划值check
		if (!StrUtil.isNumeric(planVal)) {
			return ("计划值【" + planVal + "】必须为数字！");
		}
		params.put("checkPlanId", checkPlanId);
		params.put("refObjId", refObjId);
		params.put("refIndId", refIndId);
        params.put("planCycle", planCycle);
        params.put("planCycleStart", planCycleStart);
        params.put("planCycleEnd", planCycleEnd);
        params.put("planValType", planValType);
        params.put("planVal", planVal);
        params.put("operId", sysUserEntity.getUsername());

		try {
			checkTargetDao.updChkTrgInfo(params);
		} catch (Exception e){
			return ("更新考核目标信息异常！");
		}
		return "";
	}
	@Override
	public String batchImport(String fileName, MultipartFile file, String checkPlanId) {

		if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
			return ("上传文件格式不正确");
		}
		String extString = fileName.substring(fileName.lastIndexOf("."));
		List<CheckTargetEntity> chkTrgInfoList = new ArrayList<>();
        List<CheckTargetEntity> chkTrgInfoAddList = new ArrayList<>();
		try{
            SysUserEntity sysUserEntity = checkTargetDao.queryByUserId(getUserId());
			InputStream is = file.getInputStream();
			Workbook workbook = null;
			if(".xls".equals(extString)){
				HSSFWorkbook wb = new HSSFWorkbook(is);
				workbook = wb;
			}else if(".xlsx".equals(extString)){
				XSSFWorkbook wb = new XSSFWorkbook(is);
				workbook = wb;
			}
			//有多少个sheet
			int sheets = workbook.getNumberOfSheets();

			for (int i = 0; i < sheets; i++) {
				Sheet sheet = workbook.getSheetAt(i);
				//获取多少行
				int rows = sheet.getPhysicalNumberOfRows();
                CheckTargetEntity chkTrgInfo = null;
				//遍历每一行，注意：第 0 行为标题
				for (int j = 1; j < rows; j++) {
                    chkTrgInfo = new CheckTargetEntity();
					//获得第 j 行
					Row row = sheet.getRow(j);
					// 空行check
					if (row == null) {
						return ("第" + (j + 1) + "行,没有数据！");
					}
					// 必须输入项check
					if (row.getCell(6) == null) {
						return ("第" + (j + 1) + "行,计划值周期为必输项！");
					} else if (row.getCell(7) == null) {
						return ("第" + (j + 1) + "行,计划值开始日期为必输项！");
					} else if (row.getCell(8) == null) {
						return ("第" + (j + 1) + "行,计划值结束日期为必输项！");
					} else if (row.getCell(9) == null) {
						return ("第" + (j + 1) + "行,计划值类型为必输项！");
					} else if (row.getCell(10) == null) {
						return ("第" + (j + 1) + "行,计划值为必输项！");
					}

					// 考核方案编号
					String checkPlanIdTemp = row.getCell(0).getStringCellValue();
					if (checkPlanIdTemp.equals(checkPlanId)) {
						return ("第" + (j + 1) + "行,考核方案编号【" + checkPlanIdTemp + "】不正确！");
					}
					String refObjId = row.getCell(2).getStringCellValue();
					String refIndId = row.getCell(4).getStringCellValue();
					// 计划值周期格式check
					String planCycle = row.getCell(6).getStringCellValue();
					if (!"Y".equals(planCycle) && !"Q".equals(planCycle) && !"M".equals(planCycle)) {
						return ("第" + (j + 1) + "行,计划值周期【" + planCycle + "】不正确！例:Y-年、Q-季度、M-月");
					}

					// 计划值开始日期格式check
					String planCycleStart = row.getCell(7).getStringCellValue();
                    String stDate = planCycleStart.substring(0,4) + planCycleStart.substring(5,7) + planCycleStart.substring(8,10);
                    if (!"-".equals(planCycleStart.substring(4,5)) || !"-".equals(planCycleStart.substring(7,8))) {
                        return ("第" + (j + 1) + "行,计划值开始日期【" + planCycleStart + "】不是正确的日期格式！例:YYYY-MM-DD");
                    }
					if (!DateUtil.isDate(stDate)) {
						return ("第" + (j + 1) + "行,计划值开始日期【" + planCycleStart + "】不是正确的日期格式！例:YYYY-MM-DD");
					}

					// 计划值结束日期格式check
					String planCycleEnd = row.getCell(8).getStringCellValue();
					String endDate = planCycleEnd.substring(0,4) + planCycleEnd.substring(5,7) + planCycleEnd.substring(8,10);
					if (!"-".equals(planCycleEnd.substring(4,5)) || !"-".equals(planCycleEnd.substring(7,8))) {
                        return ("第" + (j + 1) + "行,计划值结束日期【" + planCycleEnd + "】不是正确的日期格式！例:YYYY-MM-DD");
                    }
                    if (!DateUtil.isDate(endDate)) {
                        return ("第" + (j + 1) + "行,计划值结束日期【" + planCycleEnd + "】不是正确的日期格式！例:YYYY-MM-DD");
                    }

                    // 计划值开始日期-计划值结束日期周期check
                    if ("Y".equals(planCycle)) {
                        if (!DateUtil.calcMonth(stDate, 12).equals(endDate)) {
                            return ("第" + (j + 1) + "行,计划值周期为【" + planCycle + ":年】时,计划值开始日期【" + planCycleStart + "】至计划值结束日期【" + planCycleEnd + "】必须为一整年");
                        }
                    } else if ("Q".equals(planCycle)) {
                        if (!DateUtil.calcMonth(stDate, 3).equals(endDate)) {
                            return ("第" + (j + 1) + "行,计划值周期为【" + planCycle + ":季】时,计划值开始日期【" + planCycleStart + "】至计划值结束日期【" + planCycleEnd + "】必须为一整季度");
                        }
                    } else if ("M".equals(planCycle)) {
                        if (!DateUtil.calcMonth(stDate, 1).equals(endDate)) {
                            return ("第" + (j + 1) + "行,计划值周期为【" + planCycle + ":月】时,计划值开始日期【" + planCycleStart + "】至计划值结束日期【" + planCycleEnd + "】必须为一整月");
                        }
                    }

                    // 计划值类型格式check
                    String planValType = row.getCell(9).getStringCellValue();
                    if (!"0".equals(planValType) && !"1".equals(planValType) && !"2".equals(planValType)
                            && !"3".equals(planValType) && !"4".equals(planValType) && !"5".equals(planValType)) {
                        return ("第" + (j + 1) + "行,计划值类型【" + planValType + "】不正确！例:0-余额、1-金额、2-笔数、3-户数、4-百分比、5-分数");
                    }

                    // 计划值check
                    String planVal = row.getCell(10).getStringCellValue();
                    if (!StrUtil.isNumeric(planVal)) {
                        return ("第" + (j + 1) + "行,计划值【" + planVal + "】必须为数字！");
                    }

                    chkTrgInfo.setCheckPlanId(checkPlanId);
                    chkTrgInfo.setRefObjId(refObjId);
                    chkTrgInfo.setRefIndId(refIndId);
                    chkTrgInfo.setPlanCycle(planCycle);
                    chkTrgInfo.setPlanCycleStart(planCycleStart);
                    chkTrgInfo.setPlanCycleEnd(planCycleEnd);
                    chkTrgInfo.setPlanValType(planValType);
                    chkTrgInfo.setPlanVal(planVal);
                    chkTrgInfo.setOperId(sysUserEntity.getUsername());
                    chkTrgInfoList.add(chkTrgInfo);
				}
			}
			for (int l = 0; l < chkTrgInfoList.size(); l++) {
				Map<String, Object> params = new HashMap<>();
				params.put("checkPlanId",chkTrgInfoList.get(l).getCheckPlanId());
				params.put("refObjId",chkTrgInfoList.get(l).getRefObjId());
				params.put("refIndId",chkTrgInfoList.get(l).getRefIndId());
                params.put("planCycle",chkTrgInfoList.get(l).getPlanCycle());
                params.put("planCycleStart",chkTrgInfoList.get(l).getPlanCycleStart());
                params.put("planCycleEnd",chkTrgInfoList.get(l).getPlanCycleEnd());
                params.put("planValType",chkTrgInfoList.get(l).getPlanValType());
                params.put("planVal",chkTrgInfoList.get(l).getPlanVal());
                params.put("operId",sysUserEntity.getUsername());
				int count = checkTargetDao.countCsrMgrInfoNb(params);
				if (count == 0) {
                    chkTrgInfoAddList.add(chkTrgInfoList.get(l));
				} else {
                    checkTargetDao.updChkTrgInfo(params);
				}
			}
			if (chkTrgInfoAddList.size() > 0) {
                checkTargetDao.insChkTrgInfo(chkTrgInfoAddList);
			}
		}catch(Exception e){
		    System.out.println(e.getMessage());
			return "文件导入异常！";
		}
		return "";
	}

	@Override
	public List<CheckTargetEntity> queryChkPlanInfo(Map<String, Object> params) {
		List<CheckTargetEntity> chkPlanInfoList = new ArrayList<>();
		chkPlanInfoList = checkTargetDao.queryChkPlanInfo(params);
		return chkPlanInfoList;
	}
}
