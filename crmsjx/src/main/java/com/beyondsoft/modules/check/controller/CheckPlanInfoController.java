package com.beyondsoft.modules.check.controller;

import com.beyondsoft.common.utils.R;
import com.beyondsoft.common.utils.ShiroUtils;
import com.beyondsoft.modules.check.dao.CheckTargetDao;
import com.beyondsoft.modules.check.entity.CheckPlanInfoEntity;
import com.beyondsoft.modules.check.service.CheckPlanInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 方案管理
 *
 * @author liushuai
 */
@RestController
@RequestMapping("/check/checkPlan")
public class CheckPlanInfoController {
    @Autowired
    private CheckPlanInfoService checkPlanInfoService;
    @Autowired
    private CheckTargetDao checkTargetDao;

    /**
     * 查询
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        // 查询方案管理记录数
        int count = checkPlanInfoService.queryCheckInfoNb(params);
        // 查询方案管理名单信息
        List<CheckPlanInfoEntity> checkPlanInfoEntityList = checkPlanInfoService.queryCheckPlanInfo(params);
        return R.ok().put("list", checkPlanInfoEntityList).put("totalCount", count);
    }
    /**
     * 通过考核方案编号查询
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") String id){
        CheckPlanInfoEntity config = checkPlanInfoService.queryById(id);
        return R.ok().put("config", config);
    }

    /**
     * 查询字典表
     */
    @GetMapping("/dictList")
    public R dictList(@RequestParam Map<String, Object> params){
        // 获取考核对象类别字典
        params.put("busintypeid", "INSPECTION_OBJECT_TYPE");
        List<Map<String,Object>> objectList = checkTargetDao.queryDictInfo(params);
        // 获取方案设置条线字典
        params.put("busintypeid", "SOLUTION_SET_TYPE");
        List<Map<String,Object>> setList = checkTargetDao.queryDictInfo(params);
        // 获取方案状态字典
        params.put("busintypeid", "PLAN_STATUS");
        List<Map<String,Object>> statusList = checkTargetDao.queryDictInfo(params);
        return R.ok().put("objectList", objectList).put("setList", setList).put("statusList", statusList);
    }

    /**
     * 新增
     */
    @RequestMapping("/save")
    public R save(@RequestBody CheckPlanInfoEntity param){
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmssSS");
        String formDate = sdf.format(date);
        param.setCheckPlanId(formDate);
        param.setOperId(ShiroUtils.getUserEntity().getUsername());
        checkPlanInfoService.insertCheck(param);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody CheckPlanInfoEntity param){
        checkPlanInfoService.updateCheck(param);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody String[] ids){
        checkPlanInfoService.deleteById(ids[0]);
        return R.ok();
    }
}
