/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.check.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.check.entity.EmployeeEntity;
import com.beyondsoft.modules.check.entity.IndexEntity;

import java.util.List;
import java.util.Map;


/**
 * 指标管理
 *
 * @author
 */
public interface IndexAndEmployeeService extends IService<IndexEntity> {
	/**
	 * 查询员工信息
	 */
	public List<EmployeeEntity> empleeList(Map reqPara);

	List<IndexEntity> queryIndexDataList(Map reqPara);

	public R saveIndexAndEmployeeForPlanData(Map reqPara);
}
