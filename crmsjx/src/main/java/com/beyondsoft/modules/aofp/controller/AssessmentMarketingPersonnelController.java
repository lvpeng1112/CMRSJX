package com.beyondsoft.modules.aofp.controller;
import com.alibaba.druid.util.StringUtils;
import com.beyondsoft.common.utils.DateUtils;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.common.utils.ShiroUtils;
import com.beyondsoft.modules.aofp.dao.CMngCheckBaseInfoDao;
import com.beyondsoft.modules.aofp.entity.CMngCheckBaseInfo;
import com.beyondsoft.modules.aofp.entity.CMngCheckBaseInfoHis;
import com.beyondsoft.modules.aofp.service.AssessmentMarketingPersonnelService;
import com.beyondsoft.modules.sys.entity.SysUserEntity;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;

@RestController
@RequestMapping("/aofp")
public class AssessmentMarketingPersonnelController {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    AssessmentMarketingPersonnelService assessmentMarketingPersonnelService;
    @Autowired
    CMngCheckBaseInfoDao cMngCheckBaseInfoDao;

    @RequestMapping("/importExcel")
    @ApiOperation("导入excel")
    public R importExcel(@RequestParam(value = "file") MultipartFile file,String org, String isKhy) throws Exception {

        String fileName = file.getOriginalFilename();
        logger.info("导入表格文件为:{}",fileName);

        return assessmentMarketingPersonnelService.batchImport(fileName, file, org, isKhy);
    }

    @RequestMapping("/list")
    @ApiOperation("分页查询")
    public R querytData(@RequestParam Map<String, Object> params) {
        Map map = new HashMap();
        if(null == params){
            return R.error("入参为空");
        }
        if(StringUtils.isEmpty((String) params.get("page")) && StringUtils.isEmpty((String) params.get("limit"))){
            return R.error("缺少必传分页参数");
        }
        List<CMngCheckBaseInfo> cMngCheckBaseInfoList = assessmentMarketingPersonnelService.queryByLimit(params);
        int i = assessmentMarketingPersonnelService.queryDataListNb(params);
        //客户经理下拉框数据
        List<Map> employeeView = assessmentMarketingPersonnelService.queryEmployeeView(params);
//        List<Map> employeeView = assessmentMarketingPersonnelService.queryEmployeeViewLimit(params);
        //团队负责人下拉框数据
        List<Map> teamView = assessmentMarketingPersonnelService.queryTeamView(params);
        return R.ok().put("cMngCheckBaseInfoList",cMngCheckBaseInfoList).put("count",i).put("employeeList",employeeView).put("teamList",teamView);
    }

    @RequestMapping("/list1")
    public R querytData1(@RequestParam Map<String, Object> params){
        List<Map> teamView = assessmentMarketingPersonnelService.queryTeamViewRep(params);
        return R.ok().put("teamList",teamView);
    }


    @RequestMapping("/querytHisData")
    @ApiOperation("分页查询")
    public R querytHisData(@RequestParam Map<String, Object> params) {
        Map map = new HashMap();
        if(null == params){
            return R.error("入参为空");
        }
        if(StringUtils.isEmpty((String) params.get("page")) && StringUtils.isEmpty((String) params.get("limit"))){
            return R.error("缺少必传分页参数");
        }
        int pageNo = Integer.parseInt((String) params.get("page"));
        int pageCount = Integer.parseInt((String)params.get("limit"));
        int offset = ((pageNo - 1) * pageCount);
        params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
        params.put("offset", offset);
        List<CMngCheckBaseInfoHis> cMngCheckBaseInfoList = cMngCheckBaseInfoDao.querytHisData(params);
        int i = cMngCheckBaseInfoDao.querytHisDataNb(params);
        //客户经理下拉框数据
        List<Map> employeeView = cMngCheckBaseInfoDao.queryEmployeeView(params);
        //团队负责人下拉框数据
        List<Map> teamView = cMngCheckBaseInfoDao.queryTeamView(params);
        //操作类型下拉框数据
        map.clear();
        map.put("busintypeid","DATA_HIS_TYPE");
        List<Map<String, Object>> operationType = cMngCheckBaseInfoDao.queryDictDetail(map);
        return R.ok().put("cMngCheckBaseInfoList",cMngCheckBaseInfoList).put("count",i).put("employeeList",employeeView).put("teamList",teamView).put("operationType",operationType);
    }
    //营销人员考核数据日均余额乘10000
    public CMngCheckBaseInfo DtAvgBalMultiply(CMngCheckBaseInfo cMngCheckBaseInfo){
        String depositDtAvgBal1 = cMngCheckBaseInfo.getDepositDtAvgBal1();
        BigDecimal bignum1_1 = new BigDecimal(depositDtAvgBal1);
        BigDecimal bignum1_2 = new BigDecimal(10000);
        BigDecimal bignum1_3 = bignum1_1.multiply(bignum1_2);
        cMngCheckBaseInfo.setDepositDtAvgBal1(String.valueOf(bignum1_3));
        String depositDtAvgBal2 = cMngCheckBaseInfo.getDepositDtAvgBal2();
        BigDecimal bignum2_1 = new BigDecimal(depositDtAvgBal2);
        BigDecimal bignum2_2 = new BigDecimal(10000);
        BigDecimal bignum2_3 = bignum2_1.multiply(bignum2_2);
        cMngCheckBaseInfo.setDepositDtAvgBal2(String.valueOf(bignum2_3));
        String depositDtAvgBal3 = cMngCheckBaseInfo.getDepositDtAvgBal3();
        BigDecimal bignum3_1 = new BigDecimal(depositDtAvgBal3);
        BigDecimal bignum3_2 = new BigDecimal(10000);
        BigDecimal bignum3_3 = bignum3_1.multiply(bignum3_2);
        cMngCheckBaseInfo.setDepositDtAvgBal3(String.valueOf(bignum3_3));
        String depositDtAvgBal4 = cMngCheckBaseInfo.getDepositDtAvgBal4();
        BigDecimal bignum4_1 = new BigDecimal(depositDtAvgBal4);
        BigDecimal bignum4_2 = new BigDecimal(10000);
        BigDecimal bignum4_3 = bignum4_1.multiply(bignum4_2);
        cMngCheckBaseInfo.setDepositDtAvgBal4(String.valueOf(bignum4_3));
        String depositDtAvgBal5 = cMngCheckBaseInfo.getDepositDtAvgBal5();
        BigDecimal bignum5_1 = new BigDecimal(depositDtAvgBal5);
        BigDecimal bignum5_2 = new BigDecimal(10000);
        BigDecimal bignum5_3 = bignum5_1.multiply(bignum5_2);
        cMngCheckBaseInfo.setDepositDtAvgBal5(String.valueOf(bignum5_3));
        String depositDtAvgBal6 = cMngCheckBaseInfo.getDepositDtAvgBal6();
        BigDecimal bignum6_1 = new BigDecimal(depositDtAvgBal6);
        BigDecimal bignum6_2 = new BigDecimal(10000);
        BigDecimal bignum6_3 = bignum6_1.multiply(bignum6_2);
        cMngCheckBaseInfo.setDepositDtAvgBal6(String.valueOf(bignum6_3));
        String depositDtAvgBal7 = cMngCheckBaseInfo.getDepositDtAvgBal7();
        BigDecimal bignum7_1 = new BigDecimal(depositDtAvgBal7);
        BigDecimal bignum7_2 = new BigDecimal(10000);
        BigDecimal bignum7_3 = bignum7_1.multiply(bignum7_2);
        cMngCheckBaseInfo.setDepositDtAvgBal7(String.valueOf(bignum7_3));
        String depositDtAvgBal8 = cMngCheckBaseInfo.getDepositDtAvgBal8();
        BigDecimal bignum8_1 = new BigDecimal(depositDtAvgBal8);
        BigDecimal bignum8_2 = new BigDecimal(10000);
        BigDecimal bignum8_3 = bignum8_1.multiply(bignum8_2);
        cMngCheckBaseInfo.setDepositDtAvgBal8(String.valueOf(bignum8_3));
        String depositDtAvgBal9 = cMngCheckBaseInfo.getDepositDtAvgBal9();
        BigDecimal bignum9_1 = new BigDecimal(depositDtAvgBal9);
        BigDecimal bignum9_2 = new BigDecimal(10000);
        BigDecimal bignum9_3 = bignum9_1.multiply(bignum9_2);
        cMngCheckBaseInfo.setDepositDtAvgBal9(String.valueOf(bignum9_3));
        String depositDtAvgBal10 = cMngCheckBaseInfo.getDepositDtAvgBal10();
        BigDecimal bignum10_1 = new BigDecimal(depositDtAvgBal10);
        BigDecimal bignum10_2 = new BigDecimal(10000);
        BigDecimal bignum10_3 = bignum10_1.multiply(bignum10_2);
        cMngCheckBaseInfo.setDepositDtAvgBal10(String.valueOf(bignum10_3));
        String depositDtAvgBal11 = cMngCheckBaseInfo.getDepositDtAvgBal11();
        BigDecimal bignum11_1 = new BigDecimal(depositDtAvgBal11);
        BigDecimal bignum11_2 = new BigDecimal(10000);
        BigDecimal bignum11_3 = bignum11_1.multiply(bignum11_2);
        cMngCheckBaseInfo.setDepositDtAvgBal11(String.valueOf(bignum11_3));
        String depositDtAvgBal12 = cMngCheckBaseInfo.getDepositDtAvgBal12();
        BigDecimal bignum12_1 = new BigDecimal(depositDtAvgBal12);
        BigDecimal bignum12_2 = new BigDecimal(10000);
        BigDecimal bignum12_3 = bignum12_1.multiply(bignum12_2);
        cMngCheckBaseInfo.setDepositDtAvgBal12(String.valueOf(bignum12_3));
        String loanDtAvgBal1 = cMngCheckBaseInfo.getLoanDtAvgBal1();
        BigDecimal bignum1_4 = new BigDecimal(loanDtAvgBal1);
        BigDecimal bignum1_5 = new BigDecimal(10000);
        BigDecimal bignum1_6 = bignum1_4.multiply(bignum1_5);
        cMngCheckBaseInfo.setLoanDtAvgBal1(String.valueOf(bignum1_6));
        String loanDtAvgBal2 = cMngCheckBaseInfo.getLoanDtAvgBal2();
        BigDecimal bignum2_4 = new BigDecimal(loanDtAvgBal2);
        BigDecimal bignum2_5 = new BigDecimal(10000);
        BigDecimal bignum2_6 = bignum2_4.multiply(bignum2_5);
        cMngCheckBaseInfo.setLoanDtAvgBal2(String.valueOf(bignum2_6));
        String loanDtAvgBal3 = cMngCheckBaseInfo.getLoanDtAvgBal3();
        BigDecimal bignum3_4 = new BigDecimal(loanDtAvgBal3);
        BigDecimal bignum3_5 = new BigDecimal(10000);
        BigDecimal bignum3_6 = bignum3_4.multiply(bignum3_5);
        cMngCheckBaseInfo.setLoanDtAvgBal3(String.valueOf(bignum3_6));
        String loanDtAvgBal4 = cMngCheckBaseInfo.getLoanDtAvgBal4();
        BigDecimal bignum4_4 = new BigDecimal(loanDtAvgBal4);
        BigDecimal bignum4_5 = new BigDecimal(10000);
        BigDecimal bignum4_6 = bignum4_4.multiply(bignum4_5);
        cMngCheckBaseInfo.setLoanDtAvgBal4(String.valueOf(bignum4_6));
        String loanDtAvgBal5 = cMngCheckBaseInfo.getLoanDtAvgBal5();
        BigDecimal bignum5_4 = new BigDecimal(loanDtAvgBal5);
        BigDecimal bignum5_5 = new BigDecimal(10000);
        BigDecimal bignum5_6 = bignum5_4.multiply(bignum5_5);
        cMngCheckBaseInfo.setLoanDtAvgBal5(String.valueOf(bignum5_6));
        String loanDtAvgBal6 = cMngCheckBaseInfo.getLoanDtAvgBal6();
        BigDecimal bignum6_4 = new BigDecimal(loanDtAvgBal6);
        BigDecimal bignum6_5 = new BigDecimal(10000);
        BigDecimal bignum6_6 = bignum6_4.multiply(bignum6_5);
        cMngCheckBaseInfo.setLoanDtAvgBal6(String.valueOf(bignum6_6));
        String loanDtAvgBal7 = cMngCheckBaseInfo.getLoanDtAvgBal7();
        BigDecimal bignum7_4 = new BigDecimal(loanDtAvgBal7);
        BigDecimal bignum7_5 = new BigDecimal(10000);
        BigDecimal bignum7_6 = bignum7_4.multiply(bignum7_5);
        cMngCheckBaseInfo.setLoanDtAvgBal7(String.valueOf(bignum7_6));
        String loanDtAvgBal8 = cMngCheckBaseInfo.getLoanDtAvgBal8();
        BigDecimal bignum8_4 = new BigDecimal(loanDtAvgBal8);
        BigDecimal bignum8_5 = new BigDecimal(10000);
        BigDecimal bignum8_6 = bignum8_4.multiply(bignum8_5);
        cMngCheckBaseInfo.setLoanDtAvgBal8(String.valueOf(bignum8_6));
        String loanDtAvgBal9 = cMngCheckBaseInfo.getLoanDtAvgBal9();
        BigDecimal bignum9_4 = new BigDecimal(loanDtAvgBal9);
        BigDecimal bignum9_5 = new BigDecimal(10000);
        BigDecimal bignum9_6 = bignum9_4.multiply(bignum9_5);
        cMngCheckBaseInfo.setLoanDtAvgBal9(String.valueOf(bignum9_6));
        String loanDtAvgBal10 = cMngCheckBaseInfo.getLoanDtAvgBal10();
        BigDecimal bignum10_4 = new BigDecimal(loanDtAvgBal10);
        BigDecimal bignum10_5 = new BigDecimal(10000);
        BigDecimal bignum10_6 = bignum10_4.multiply(bignum10_5);
        cMngCheckBaseInfo.setLoanDtAvgBal10(String.valueOf(bignum10_6));
        String loanDtAvgBal11 = cMngCheckBaseInfo.getLoanDtAvgBal11();
        BigDecimal bignum11_4 = new BigDecimal(loanDtAvgBal11);
        BigDecimal bignum11_5 = new BigDecimal(10000);
        BigDecimal bignum11_6 = bignum11_4.multiply(bignum11_5);
        cMngCheckBaseInfo.setLoanDtAvgBal11(String.valueOf(bignum11_6));
        String loanDtAvgBal12 = cMngCheckBaseInfo.getLoanDtAvgBal12();
        BigDecimal bignum12_4 = new BigDecimal(loanDtAvgBal12);
        BigDecimal bignum12_5 = new BigDecimal(10000);
        BigDecimal bignum12_6 = bignum12_4.multiply(bignum12_5);
        cMngCheckBaseInfo.setLoanDtAvgBal12(String.valueOf(bignum12_6));
        return cMngCheckBaseInfo;
    }
    //记录营销人员考核数据操作历史信息公共方法
    public void record(List<CMngCheckBaseInfo> cMngCheckBaseInfosBefo, List<CMngCheckBaseInfo> cMngCheckBaseInfosAfter,String logo){
        CMngCheckBaseInfoHis baseInfoHis = new CMngCheckBaseInfoHis();
        if(cMngCheckBaseInfosBefo.size() > 0){

            CMngCheckBaseInfo cMngCheckBaseInfo = cMngCheckBaseInfosBefo.get(0);
            if(logo.equals("UPDATE")||logo.equals("DELETE")){
                CMngCheckBaseInfo cMngCheckBaseInfo1 = DtAvgBalMultiply(cMngCheckBaseInfo);
                cMngCheckBaseInfosBefo.add(0,cMngCheckBaseInfo1);
            }

            baseInfoHis.setYr(cMngCheckBaseInfosBefo.get(0).getYr());
            baseInfoHis.setOrgId(cMngCheckBaseInfosBefo.get(0).getOrgId());
            baseInfoHis.setCustMngrId(cMngCheckBaseInfosBefo.get(0).getCustMngrId());
            baseInfoHis.setCustMngrNm(cMngCheckBaseInfosBefo.get(0).getCustMngrNm());
            baseInfoHis.setInTm(cMngCheckBaseInfosBefo.get(0).getInTm());
            baseInfoHis.setDepositDtAvgBal1_Befo(cMngCheckBaseInfosBefo.get(0).getDepositDtAvgBal1());
            baseInfoHis.setDepositDtAvgBal2_Befo(cMngCheckBaseInfosBefo.get(0).getDepositDtAvgBal2());
            baseInfoHis.setDepositDtAvgBal3_Befo(cMngCheckBaseInfosBefo.get(0).getDepositDtAvgBal3());
            baseInfoHis.setDepositDtAvgBal4_Befo(cMngCheckBaseInfosBefo.get(0).getDepositDtAvgBal4());
            baseInfoHis.setDepositDtAvgBal5_Befo(cMngCheckBaseInfosBefo.get(0).getDepositDtAvgBal5());
            baseInfoHis.setDepositDtAvgBal6_Befo(cMngCheckBaseInfosBefo.get(0).getDepositDtAvgBal6());
            baseInfoHis.setDepositDtAvgBal7_Befo(cMngCheckBaseInfosBefo.get(0).getDepositDtAvgBal7());
            baseInfoHis.setDepositDtAvgBal8_Befo(cMngCheckBaseInfosBefo.get(0).getDepositDtAvgBal8());
            baseInfoHis.setDepositDtAvgBal9_Befo(cMngCheckBaseInfosBefo.get(0).getDepositDtAvgBal9());
            baseInfoHis.setDepositDtAvgBal10_Befo(cMngCheckBaseInfosBefo.get(0).getDepositDtAvgBal10());
            baseInfoHis.setDepositDtAvgBal11_Befo(cMngCheckBaseInfosBefo.get(0).getDepositDtAvgBal11());
            baseInfoHis.setDepositDtAvgBal12_Befo(cMngCheckBaseInfosBefo.get(0).getDepositDtAvgBal12());
            baseInfoHis.setLoanDtAvgBal1_Befo(cMngCheckBaseInfosBefo.get(0).getLoanDtAvgBal1());
            baseInfoHis.setLoanDtAvgBal2_Befo(cMngCheckBaseInfosBefo.get(0).getLoanDtAvgBal2());
            baseInfoHis.setLoanDtAvgBal3_Befo(cMngCheckBaseInfosBefo.get(0).getLoanDtAvgBal3());
            baseInfoHis.setLoanDtAvgBal4_Befo(cMngCheckBaseInfosBefo.get(0).getLoanDtAvgBal4());
            baseInfoHis.setLoanDtAvgBal5_Befo(cMngCheckBaseInfosBefo.get(0).getLoanDtAvgBal5());
            baseInfoHis.setLoanDtAvgBal6_Befo(cMngCheckBaseInfosBefo.get(0).getLoanDtAvgBal6());
            baseInfoHis.setLoanDtAvgBal7_Befo(cMngCheckBaseInfosBefo.get(0).getLoanDtAvgBal7());
            baseInfoHis.setLoanDtAvgBal8_Befo(cMngCheckBaseInfosBefo.get(0).getLoanDtAvgBal8());
            baseInfoHis.setLoanDtAvgBal9_Befo(cMngCheckBaseInfosBefo.get(0).getLoanDtAvgBal9());
            baseInfoHis.setLoanDtAvgBal10_Befo(cMngCheckBaseInfosBefo.get(0).getLoanDtAvgBal10());
            baseInfoHis.setLoanDtAvgBal11_Befo(cMngCheckBaseInfosBefo.get(0).getLoanDtAvgBal11());
            baseInfoHis.setLoanDtAvgBal12_Befo(cMngCheckBaseInfosBefo.get(0).getLoanDtAvgBal12());
            baseInfoHis.setNetIncm1_Befo(cMngCheckBaseInfosBefo.get(0).getNetIncm1());
            baseInfoHis.setNetIncm2_Befo(cMngCheckBaseInfosBefo.get(0).getNetIncm2());
            baseInfoHis.setNetIncm3_Befo(cMngCheckBaseInfosBefo.get(0).getNetIncm3());
            baseInfoHis.setNetIncm4_Befo(cMngCheckBaseInfosBefo.get(0).getNetIncm4());
            baseInfoHis.setNetIncm5_Befo(cMngCheckBaseInfosBefo.get(0).getNetIncm5());
            baseInfoHis.setNetIncm6_Befo(cMngCheckBaseInfosBefo.get(0).getNetIncm6());
            baseInfoHis.setNetIncm7_Befo(cMngCheckBaseInfosBefo.get(0).getNetIncm7());
            baseInfoHis.setNetIncm8_Befo(cMngCheckBaseInfosBefo.get(0).getNetIncm8());
            baseInfoHis.setNetIncm9_Befo(cMngCheckBaseInfosBefo.get(0).getNetIncm9());
            baseInfoHis.setNetIncm10_Befo(cMngCheckBaseInfosBefo.get(0).getNetIncm10());
            baseInfoHis.setNetIncm11_Befo(cMngCheckBaseInfosBefo.get(0).getNetIncm11());
            baseInfoHis.setNetIncm12_Befo(cMngCheckBaseInfosBefo.get(0).getNetIncm12());
            baseInfoHis.setDepositNewOpenNum1_Befo(cMngCheckBaseInfosBefo.get(0).getDepositNewOpenNum1());
            baseInfoHis.setDepositNewOpenNum2_Befo(cMngCheckBaseInfosBefo.get(0).getDepositNewOpenNum2());
            baseInfoHis.setDepositNewOpenNum3_Befo(cMngCheckBaseInfosBefo.get(0).getDepositNewOpenNum3());
            baseInfoHis.setDepositNewOpenNum4_Befo(cMngCheckBaseInfosBefo.get(0).getDepositNewOpenNum4());
            baseInfoHis.setDepositNewOpenNum5_Befo(cMngCheckBaseInfosBefo.get(0).getDepositNewOpenNum5());
            baseInfoHis.setDepositNewOpenNum6_Befo(cMngCheckBaseInfosBefo.get(0).getDepositNewOpenNum6());
            baseInfoHis.setDepositNewOpenNum7_Befo(cMngCheckBaseInfosBefo.get(0).getDepositNewOpenNum7());
            baseInfoHis.setDepositNewOpenNum8_Befo(cMngCheckBaseInfosBefo.get(0).getDepositNewOpenNum8());
            baseInfoHis.setDepositNewOpenNum9_Befo(cMngCheckBaseInfosBefo.get(0).getDepositNewOpenNum9());
            baseInfoHis.setDepositNewOpenNum10_Befo(cMngCheckBaseInfosBefo.get(0).getDepositNewOpenNum10());
            baseInfoHis.setDepositNewOpenNum11_Befo(cMngCheckBaseInfosBefo.get(0).getDepositNewOpenNum11());
            baseInfoHis.setDepositNewOpenNum12_Befo(cMngCheckBaseInfosBefo.get(0).getDepositNewOpenNum12());
            baseInfoHis.setLoanMngCustCustNum1_Befo(cMngCheckBaseInfosBefo.get(0).getLoanMngCustCustNum1());
            baseInfoHis.setLoanMngCustCustNum2_Befo(cMngCheckBaseInfosBefo.get(0).getLoanMngCustCustNum2());
            baseInfoHis.setLoanMngCustCustNum3_Befo(cMngCheckBaseInfosBefo.get(0).getLoanMngCustCustNum3());
            baseInfoHis.setLoanMngCustCustNum4_Befo(cMngCheckBaseInfosBefo.get(0).getLoanMngCustCustNum4());
            baseInfoHis.setLoanMngCustCustNum5_Befo(cMngCheckBaseInfosBefo.get(0).getLoanMngCustCustNum5());
            baseInfoHis.setLoanMngCustCustNum6_Befo(cMngCheckBaseInfosBefo.get(0).getLoanMngCustCustNum6());
            baseInfoHis.setLoanMngCustCustNum7_Befo(cMngCheckBaseInfosBefo.get(0).getLoanMngCustCustNum7());
            baseInfoHis.setLoanMngCustCustNum8_Befo(cMngCheckBaseInfosBefo.get(0).getLoanMngCustCustNum8());
            baseInfoHis.setLoanMngCustCustNum9_Befo(cMngCheckBaseInfosBefo.get(0).getLoanMngCustCustNum9());
            baseInfoHis.setLoanMngCustCustNum10_Befo(cMngCheckBaseInfosBefo.get(0).getLoanMngCustCustNum10());
            baseInfoHis.setLoanMngCustCustNum11_Befo(cMngCheckBaseInfosBefo.get(0).getLoanMngCustCustNum11());
            baseInfoHis.setLoanMngCustCustNum12_Befo(cMngCheckBaseInfosBefo.get(0).getLoanMngCustCustNum12());
            baseInfoHis.setNetIncmBase1_Befo(cMngCheckBaseInfosBefo.get(0).getNetIncmBase1());
            baseInfoHis.setNetIncmBase2_Befo(cMngCheckBaseInfosBefo.get(0).getNetIncmBase2());
            baseInfoHis.setNetIncmBase3_Befo(cMngCheckBaseInfosBefo.get(0).getNetIncmBase3());
            baseInfoHis.setNetIncmBase4_Befo(cMngCheckBaseInfosBefo.get(0).getNetIncmBase4());
            baseInfoHis.setNetIncmBase5_Befo(cMngCheckBaseInfosBefo.get(0).getNetIncmBase5());
            baseInfoHis.setNetIncmBase6_Befo(cMngCheckBaseInfosBefo.get(0).getNetIncmBase6());
            baseInfoHis.setNetIncmBase7_Befo(cMngCheckBaseInfosBefo.get(0).getNetIncmBase7());
            baseInfoHis.setNetIncmBase8_Befo(cMngCheckBaseInfosBefo.get(0).getNetIncmBase8());
            baseInfoHis.setNetIncmBase9_Befo(cMngCheckBaseInfosBefo.get(0).getNetIncmBase9());
            baseInfoHis.setNetIncmBase10_Befo(cMngCheckBaseInfosBefo.get(0).getNetIncmBase10());
            baseInfoHis.setNetIncmBase11_Befo(cMngCheckBaseInfosBefo.get(0).getNetIncmBase11());
            baseInfoHis.setNetIncmBase12_Befo(cMngCheckBaseInfosBefo.get(0).getNetIncmBase12());
        }
        if (cMngCheckBaseInfosAfter.size() > 0){
            CMngCheckBaseInfo cMngCheckBaseInfo = cMngCheckBaseInfosAfter.get(0);
            if(logo.equals("UPDATE")||logo.equals("DELETE")){
            CMngCheckBaseInfo cMngCheckBaseInfo2 = DtAvgBalMultiply(cMngCheckBaseInfo);
                cMngCheckBaseInfosAfter.add(0,cMngCheckBaseInfo2);
            }

            baseInfoHis.setYr(cMngCheckBaseInfosAfter.get(0).getYr());
            baseInfoHis.setOrgId(cMngCheckBaseInfosAfter.get(0).getOrgId());
            baseInfoHis.setCustMngrId(cMngCheckBaseInfosAfter.get(0).getCustMngrId());
            baseInfoHis.setCustMngrNm(cMngCheckBaseInfosAfter.get(0).getCustMngrNm());
            baseInfoHis.setInTm(cMngCheckBaseInfosAfter.get(0).getInTm());
            baseInfoHis.setDepositDtAvgBal1_After(cMngCheckBaseInfosAfter.get(0).getDepositDtAvgBal1());
            baseInfoHis.setDepositDtAvgBal2_After(cMngCheckBaseInfosAfter.get(0).getDepositDtAvgBal2());
            baseInfoHis.setDepositDtAvgBal3_After(cMngCheckBaseInfosAfter.get(0).getDepositDtAvgBal3());
            baseInfoHis.setDepositDtAvgBal4_After(cMngCheckBaseInfosAfter.get(0).getDepositDtAvgBal4());
            baseInfoHis.setDepositDtAvgBal5_After(cMngCheckBaseInfosAfter.get(0).getDepositDtAvgBal5());
            baseInfoHis.setDepositDtAvgBal6_After(cMngCheckBaseInfosAfter.get(0).getDepositDtAvgBal6());
            baseInfoHis.setDepositDtAvgBal7_After(cMngCheckBaseInfosAfter.get(0).getDepositDtAvgBal7());
            baseInfoHis.setDepositDtAvgBal8_After(cMngCheckBaseInfosAfter.get(0).getDepositDtAvgBal8());
            baseInfoHis.setDepositDtAvgBal9_After(cMngCheckBaseInfosAfter.get(0).getDepositDtAvgBal9());
            baseInfoHis.setDepositDtAvgBal10_After(cMngCheckBaseInfosAfter.get(0).getDepositDtAvgBal10());
            baseInfoHis.setDepositDtAvgBal11_After(cMngCheckBaseInfosAfter.get(0).getDepositDtAvgBal11());
            baseInfoHis.setDepositDtAvgBal12_After(cMngCheckBaseInfosAfter.get(0).getDepositDtAvgBal12());
            baseInfoHis.setLoanDtAvgBal1_After(cMngCheckBaseInfosAfter.get(0).getLoanDtAvgBal1());
            baseInfoHis.setLoanDtAvgBal2_After(cMngCheckBaseInfosAfter.get(0).getLoanDtAvgBal2());
            baseInfoHis.setLoanDtAvgBal3_After(cMngCheckBaseInfosAfter.get(0).getLoanDtAvgBal3());
            baseInfoHis.setLoanDtAvgBal4_After(cMngCheckBaseInfosAfter.get(0).getLoanDtAvgBal4());
            baseInfoHis.setLoanDtAvgBal5_After(cMngCheckBaseInfosAfter.get(0).getLoanDtAvgBal5());
            baseInfoHis.setLoanDtAvgBal6_After(cMngCheckBaseInfosAfter.get(0).getLoanDtAvgBal6());
            baseInfoHis.setLoanDtAvgBal7_After(cMngCheckBaseInfosAfter.get(0).getLoanDtAvgBal7());
            baseInfoHis.setLoanDtAvgBal8_After(cMngCheckBaseInfosAfter.get(0).getLoanDtAvgBal8());
            baseInfoHis.setLoanDtAvgBal9_After(cMngCheckBaseInfosAfter.get(0).getLoanDtAvgBal9());
            baseInfoHis.setLoanDtAvgBal10_After(cMngCheckBaseInfosAfter.get(0).getLoanDtAvgBal10());
            baseInfoHis.setLoanDtAvgBal11_After(cMngCheckBaseInfosAfter.get(0).getLoanDtAvgBal11());
            baseInfoHis.setLoanDtAvgBal12_After(cMngCheckBaseInfosAfter.get(0).getLoanDtAvgBal12());
            baseInfoHis.setNetIncm1_After(cMngCheckBaseInfosAfter.get(0).getNetIncm1());
            baseInfoHis.setNetIncm2_After(cMngCheckBaseInfosAfter.get(0).getNetIncm2());
            baseInfoHis.setNetIncm3_After(cMngCheckBaseInfosAfter.get(0).getNetIncm3());
            baseInfoHis.setNetIncm4_After(cMngCheckBaseInfosAfter.get(0).getNetIncm4());
            baseInfoHis.setNetIncm5_After(cMngCheckBaseInfosAfter.get(0).getNetIncm5());
            baseInfoHis.setNetIncm6_After(cMngCheckBaseInfosAfter.get(0).getNetIncm6());
            baseInfoHis.setNetIncm7_After(cMngCheckBaseInfosAfter.get(0).getNetIncm7());
            baseInfoHis.setNetIncm8_After(cMngCheckBaseInfosAfter.get(0).getNetIncm8());
            baseInfoHis.setNetIncm9_After(cMngCheckBaseInfosAfter.get(0).getNetIncm9());
            baseInfoHis.setNetIncm10_After(cMngCheckBaseInfosAfter.get(0).getNetIncm10());
            baseInfoHis.setNetIncm11_After(cMngCheckBaseInfosAfter.get(0).getNetIncm11());
            baseInfoHis.setNetIncm12_After(cMngCheckBaseInfosAfter.get(0).getNetIncm12());
            baseInfoHis.setDepositNewOpenNum1_After(cMngCheckBaseInfosAfter.get(0).getDepositNewOpenNum1());
            baseInfoHis.setDepositNewOpenNum2_After(cMngCheckBaseInfosAfter.get(0).getDepositNewOpenNum2());
            baseInfoHis.setDepositNewOpenNum3_After(cMngCheckBaseInfosAfter.get(0).getDepositNewOpenNum3());
            baseInfoHis.setDepositNewOpenNum4_After(cMngCheckBaseInfosAfter.get(0).getDepositNewOpenNum4());
            baseInfoHis.setDepositNewOpenNum5_After(cMngCheckBaseInfosAfter.get(0).getDepositNewOpenNum5());
            baseInfoHis.setDepositNewOpenNum6_After(cMngCheckBaseInfosAfter.get(0).getDepositNewOpenNum6());
            baseInfoHis.setDepositNewOpenNum7_After(cMngCheckBaseInfosAfter.get(0).getDepositNewOpenNum7());
            baseInfoHis.setDepositNewOpenNum8_After(cMngCheckBaseInfosAfter.get(0).getDepositNewOpenNum8());
            baseInfoHis.setDepositNewOpenNum9_After(cMngCheckBaseInfosAfter.get(0).getDepositNewOpenNum9());
            baseInfoHis.setDepositNewOpenNum10_After(cMngCheckBaseInfosAfter.get(0).getDepositNewOpenNum10());
            baseInfoHis.setDepositNewOpenNum11_After(cMngCheckBaseInfosAfter.get(0).getDepositNewOpenNum11());
            baseInfoHis.setDepositNewOpenNum12_After(cMngCheckBaseInfosAfter.get(0).getDepositNewOpenNum12());
            baseInfoHis.setLoanMngCustCustNum1_After(cMngCheckBaseInfosAfter.get(0).getLoanMngCustCustNum1());
            baseInfoHis.setLoanMngCustCustNum2_After(cMngCheckBaseInfosAfter.get(0).getLoanMngCustCustNum2());
            baseInfoHis.setLoanMngCustCustNum3_After(cMngCheckBaseInfosAfter.get(0).getLoanMngCustCustNum3());
            baseInfoHis.setLoanMngCustCustNum4_After(cMngCheckBaseInfosAfter.get(0).getLoanMngCustCustNum4());
            baseInfoHis.setLoanMngCustCustNum5_After(cMngCheckBaseInfosAfter.get(0).getLoanMngCustCustNum5());
            baseInfoHis.setLoanMngCustCustNum6_After(cMngCheckBaseInfosAfter.get(0).getLoanMngCustCustNum6());
            baseInfoHis.setLoanMngCustCustNum7_After(cMngCheckBaseInfosAfter.get(0).getLoanMngCustCustNum7());
            baseInfoHis.setLoanMngCustCustNum8_After(cMngCheckBaseInfosAfter.get(0).getLoanMngCustCustNum8());
            baseInfoHis.setLoanMngCustCustNum9_After(cMngCheckBaseInfosAfter.get(0).getLoanMngCustCustNum9());
            baseInfoHis.setLoanMngCustCustNum10_After(cMngCheckBaseInfosAfter.get(0).getLoanMngCustCustNum10());
            baseInfoHis.setLoanMngCustCustNum11_After(cMngCheckBaseInfosAfter.get(0).getLoanMngCustCustNum11());
            baseInfoHis.setLoanMngCustCustNum12_After(cMngCheckBaseInfosAfter.get(0).getLoanMngCustCustNum12());
            baseInfoHis.setNetIncmBase1_After(cMngCheckBaseInfosAfter.get(0).getNetIncmBase1());
            baseInfoHis.setNetIncmBase2_After(cMngCheckBaseInfosAfter.get(0).getNetIncmBase2());
            baseInfoHis.setNetIncmBase3_After(cMngCheckBaseInfosAfter.get(0).getNetIncmBase3());
            baseInfoHis.setNetIncmBase4_After(cMngCheckBaseInfosAfter.get(0).getNetIncmBase4());
            baseInfoHis.setNetIncmBase5_After(cMngCheckBaseInfosAfter.get(0).getNetIncmBase5());
            baseInfoHis.setNetIncmBase6_After(cMngCheckBaseInfosAfter.get(0).getNetIncmBase6());
            baseInfoHis.setNetIncmBase7_After(cMngCheckBaseInfosAfter.get(0).getNetIncmBase7());
            baseInfoHis.setNetIncmBase8_After(cMngCheckBaseInfosAfter.get(0).getNetIncmBase8());
            baseInfoHis.setNetIncmBase9_After(cMngCheckBaseInfosAfter.get(0).getNetIncmBase9());
            baseInfoHis.setNetIncmBase10_After(cMngCheckBaseInfosAfter.get(0).getNetIncmBase10());
            baseInfoHis.setNetIncmBase11_After(cMngCheckBaseInfosAfter.get(0).getNetIncmBase11());
            baseInfoHis.setNetIncmBase12_After(cMngCheckBaseInfosAfter.get(0).getNetIncmBase12());
        }
        baseInfoHis.setOperateDate(DateUtils.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
        baseInfoHis.setOperator(ShiroUtils.getUserEntity().getName());
        baseInfoHis.setLogo(logo);//操作标识
        cMngCheckBaseInfoDao.insertByYrHis(baseInfoHis);
    }

    @RequestMapping("/deleteData")
    @ApiOperation("删除数据")
    public R deleteData(@RequestBody CMngCheckBaseInfo cMngCheckBaseInfo){
        if(null == cMngCheckBaseInfo){
            return R.error("入参为空");
        }
        //删除之前将数据进行备份
        List<CMngCheckBaseInfo> cMngCheckBaseInfosBefo = cMngCheckBaseInfoDao.queryByYr(cMngCheckBaseInfo);
        List<CMngCheckBaseInfo> cMngCheckBaseInfosAfter = new ArrayList<>();
        String logo = "DELETE";
        record(cMngCheckBaseInfosBefo,cMngCheckBaseInfosAfter,logo);
        int i = cMngCheckBaseInfoDao.queryTeamLeaBycusId(cMngCheckBaseInfo);
        if(i>0){
            return R.error("该客户经理为团队长，请先退出团队");
        }
        int i1 = cMngCheckBaseInfoDao.queryTeamEmpBycusId(cMngCheckBaseInfo);
        if(i1>0){
            return R.error("该客户经理已加入团队，请先退出团队");
        }
        assessmentMarketingPersonnelService.deleteData(cMngCheckBaseInfo);
        return R.ok();
    }

    @RequestMapping("/updateData")
    @ApiOperation("修改数据")
    public R updateData(@RequestBody CMngCheckBaseInfo cMngCheckBaseInfo){
        if(null == cMngCheckBaseInfo){
            return R.error("入参为空");
        }

        cMngCheckBaseInfo.setOperator(ShiroUtils.getUserEntity().getName());
        //调整前数据
        List<CMngCheckBaseInfo> cMngCheckBaseInfosBefo = cMngCheckBaseInfoDao.queryByYr(cMngCheckBaseInfo);
        //执行修改操作
        assessmentMarketingPersonnelService.updateData(cMngCheckBaseInfo);
        //调整后数据
        List<CMngCheckBaseInfo> cMngCheckBaseInfosAfter = cMngCheckBaseInfoDao.queryByYr(cMngCheckBaseInfo);
        //将数据进行保存
        String logo = "UPDATE";
        record(cMngCheckBaseInfosBefo,cMngCheckBaseInfosAfter,logo);
        return R.ok();
    }

    @PostMapping("/insertSingleData")
    @ApiOperation("新增单条数据")
    public R insertSingleData(@RequestBody CMngCheckBaseInfo cMngCheckBaseInfo){
        Map map = new HashMap();
        SysUserEntity userEntity = ShiroUtils.getUserEntity();
        cMngCheckBaseInfo.setOperator(userEntity.getName());
        if(null == cMngCheckBaseInfo){
            return R.error("入参为空");
        }
        //查询客户经理id是否已经存在
        map.put("empCd",cMngCheckBaseInfo.getCustMngrId());
        map.put("orgId",cMngCheckBaseInfo.getOrgId());
        //        List<Map> maps = assessmentMarketingPersonnelService.queryEmployeeView(map);
        int count = assessmentMarketingPersonnelService.queryCountMng(map);

        if(count > 0){
            return R.error("客户经理编号已经存在");
        }else{
            assessmentMarketingPersonnelService.insertSingleData(cMngCheckBaseInfo);
        }
        //新增将数据进行备份
        List<CMngCheckBaseInfo> cMngCheckBaseInfosBefo = new ArrayList<>();
        List<CMngCheckBaseInfo> cMngCheckBaseInfosAfter = new ArrayList<>();
        cMngCheckBaseInfosAfter.add(cMngCheckBaseInfo);
        String logo = "INSERT";
        record(cMngCheckBaseInfosBefo,cMngCheckBaseInfosAfter,logo);
        return R.ok();
    }

    @RequestMapping("/queryAllChdetCode")
    @ApiOperation("查询符号条件的村镇银行，和编号")
    public R queryAllChdetCode(){
        return R.ok().put("queryAllChdetCode",cMngCheckBaseInfoDao.queryChadDress());
    }
    @RequestMapping("/queryAll")
    @ApiOperation("查询所有的村镇银行，和编号")
    public R queryChdetCode(){
        return R.ok().put("queryAllChdetCode",cMngCheckBaseInfoDao.queryAllChadDress());
    }
    @RequestMapping("/queryAllEmp")
    @ApiOperation("查询客户经理号")
    public R queryAllEmp(){
        return R.ok().put("queryAllChdetCode",assessmentMarketingPersonnelService.queryAllEmp());
    }
    @RequestMapping("/downloadExcel")
    @ApiOperation("下载excel模板")
    public void  downloadExcel(HttpServletResponse response) {
        System.out.println(1);
        try (OutputStream out = response.getOutputStream()){
            String fileName = "aofp.xlsx";
            String path = "template/aofp.xlsx";
            String resource = getClass().getResource("/template/"+fileName).getPath();
            response.setContentType("application/x-msdownload;charset=UTF-8");
            response.setHeader("Content-Disposition", "attachment; filename="
                    + URLEncoder.encode(fileName, StandardCharsets.UTF_8.toString()));
            response.setHeader("fileName", URLEncoder.encode(fileName, StandardCharsets.UTF_8.toString()));
            response.setHeader(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, "fileName");
            XSSFWorkbook wb = new XSSFWorkbook(resource);
            wb.write(out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * 模板文件下载
     */
    @RequestMapping("/downLoadTemplate")
    @ApiOperation("模板文件数据查询")
    public R downLoadTemplate(@RequestParam Map<String, Object> params){
        List<CMngCheckBaseInfo> downLoadList = assessmentMarketingPersonnelService.downLoadTemplate(params);
        return R.ok().put("list", downLoadList);
    }

    @RequestMapping("/queryByOrgId")
    @ApiOperation("根据机构查询客户经理和团队信息")
    public R queryByOrgId(@RequestParam Map<String, Object> params){
        //客户经理下拉框数据
        List<Map> employeeView = assessmentMarketingPersonnelService.queryEmployeeView(params);
        //团队负责人下拉框数据
        List<Map> teamView = assessmentMarketingPersonnelService.queryTeamView(params);
        //团队下拉框数据
        List<Map> team= assessmentMarketingPersonnelService.queryTeam(params);
        return R.ok().put("employeeList",employeeView).put("teamList",teamView).put("teamNmList",team);
    }

    @RequestMapping("/queryByteamId")
    @ApiOperation("根据团队查询客户经理")
    public R queryByteamId(@RequestParam Map<String, Object> params) {
        //客户经理下拉框数据
        List<Map> employeeView = assessmentMarketingPersonnelService.queryEmployeeView(params);
        return R.ok().put("employeeList",employeeView);
    }

    @RequestMapping("/queryBitchHis")
    @ApiOperation("跑批历史记录分页查询")
    public R queryBitchHis(@RequestParam Map<String, Object> params) {
        Map map = new HashMap();
        if(null == params){
            return R.error("入参为空");
        }
        if(StringUtils.isEmpty((String) params.get("page")) && StringUtils.isEmpty((String) params.get("limit"))){
            return R.error("缺少必传分页参数");
        }
        List<CMngCheckBaseInfo> cMngCheckBaseInfoList = assessmentMarketingPersonnelService.queryBitchHisByLimit(params);
        int i = assessmentMarketingPersonnelService.queryBitchHisByLimitNb(params);
        return R.ok().put("cMngCheckBaseInfoList",cMngCheckBaseInfoList).put("count",i);
    }

    @RequestMapping("/queryByOrgIdRep")
    @ApiOperation("根据机构查询客户经理和团队信息8张报表页面")
    public R queryByOrgIdRep(@RequestParam Map<String, Object> params) {
        String st = (String) params.get("st");
        String end = (String) params.get("end");
        if(st.length()<=4){
            st=st+"-01";
        }
        params.put("st",st);
        if(end.length()<=4){
            end=end+"-12";
        }
        params.put("end",end);
        //客户经理下拉框数据
        List<Map> employeeView = assessmentMarketingPersonnelService.queryEmployeeView(params);
        //团队负责人下拉框数据
        List<Map> teamView = assessmentMarketingPersonnelService.queryTeamViewRep(params);
        //团队下拉框数据
        List<Map> team= assessmentMarketingPersonnelService.queryTeamRep(params);
        return R.ok().put("employeeList",employeeView).put("teamList",teamView).put("teamNmList",team);
    }
    //报表团队负责人下拉框查询
    @RequestMapping("/teamViewInit")
    public R teamViewInit(@RequestParam Map<String, Object> params){
        String st = (String) params.get("st");
        String end = (String) params.get("end");
        if(st.length()<=4){
            st=st+"-01";
        }
        params.put("st",st);
        if(end.length()<=4){
            end=end+"-12";
        }
        params.put("end",end);
        List<Map> teamView = assessmentMarketingPersonnelService.queryTeamViewRep(params);
//        List<Map> team= assessmentMarketingPersonnelService.queryTeamRep(params);
        return R.ok().put("teamList",teamView);
    }
}