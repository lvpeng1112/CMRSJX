package com.beyondsoft.modules.aofp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.common.utils.DateUtils;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.common.utils.ShiroUtils;
import com.beyondsoft.modules.aofp.entity.CMngCheckBaseInfo;
import com.beyondsoft.modules.aofp.dao.CMngCheckBaseInfoDao;
import com.beyondsoft.modules.aofp.entity.CMngCheckBaseInfoHis;
import com.beyondsoft.modules.aofp.service.AssessmentMarketingPersonnelService;
import com.beyondsoft.modules.aofp.utils.Utils;
import com.beyondsoft.utils.DateUtil;
import com.beyondsoft.utils.StrUtil;
import com.microsoft.schemas.office.visio.x2012.main.CellType;
import net.bytebuddy.asm.Advice;
import net.bytebuddy.implementation.bytecode.Throw;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class AssessmentMarketingPersonnelServiceImpl extends ServiceImpl<CMngCheckBaseInfoDao, CMngCheckBaseInfo> implements AssessmentMarketingPersonnelService {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    CMngCheckBaseInfoDao personnelMapper;

    @Override
    public R batchImport(String fileName, MultipartFile file, String org, String isKhy) {
        Map map = new HashMap();
        Workbook workbook = null;
        if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
            return R.error("上传格式不正确");
        }
        String extString = fileName.substring(fileName.lastIndexOf("."));
        try {
            InputStream inputStream = file.getInputStream();
            if (".xls".equals(extString)) {
                HSSFWorkbook wb = new HSSFWorkbook(inputStream);
                workbook = wb;
            } else if (".xlsx".equals(extString)) {
                XSSFWorkbook wb = new XSSFWorkbook(inputStream);
                workbook = wb;
            }
            int sheets = workbook.getNumberOfSheets();
            for (int i = 0; i < sheets; i++) {
                Sheet sheetAt = workbook.getSheetAt(i);
                //获取多少行
                int lastRowNum = sheetAt.getLastRowNum();
                for (int j = 2; j <= lastRowNum; j++) {
                    Row row = sheetAt.getRow(j);
                    if (row != null) {
//                        int a = 0;
//                        CMngCheckBaseInfo cMngCheckBaseInfo = null;
                        CMngCheckBaseInfo cMngCheckBaseInfo = new CMngCheckBaseInfo();
//                        List<String> list = new ArrayList<>();
//                        for (Cell cell : row) {
//                            a++;
//                            cell.setCellType(Cell.CELL_TYPE_STRING);
//                            if (cell.getStringCellValue() != null && !"".equals(cell.getStringCellValue())) {
//                                 map.put("empCd", row.getCell(4).getStringCellValue());
//                                 map.put("orgId", row.getCell(2).getStringCellValue());
//                                 List<Map> maps = personnelMapper.queryEmployeeView(map);
//                                 list.add(cell.getStringCellValue());
//                            }else if(cell.getStringCellValue() == null){
//                                list.add("null");
//                            }else {
//                                return R.error("第" + (j + 1) + "行，第" + a + "列没有数据");
//                            }
//                        }
                        for (Cell cell : row) {
                            cell.setCellType(Cell.CELL_TYPE_STRING);
                        }
//                        row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
//                        row.getCell(1).setCellType(Cell.CELL_TYPE_STRING);
//                        row.getCell(2).setCellType(Cell.CELL_TYPE_STRING);
//                        row.getCell(3).setCellType(Cell.CELL_TYPE_STRING);
//                        row.getCell(7).setCellType(Cell.CELL_TYPE_STRING);
//                        row.getCell(8).setCellType(Cell.CELL_TYPE_STRING);
//                        row.getCell(9).setCellType(Cell.CELL_TYPE_STRING);
//                        row.getCell(10).setCellType(Cell.CELL_TYPE_STRING);
//                        row.getCell(11).setCellType(Cell.CELL_TYPE_STRING);
//                        row.getCell(12).setCellType(Cell.CELL_TYPE_STRING);
//                        row.getCell(13).setCellType(Cell.CELL_TYPE_STRING);
//                        row.getCell(14).setCellType(Cell.CELL_TYPE_STRING);
//                        row.getCell(15).setCellType(Cell.CELL_TYPE_STRING);
//                        row.getCell(16).setCellType(Cell.CELL_TYPE_STRING);
//                        row.getCell(17).setCellType(Cell.CELL_TYPE_STRING);
//                        row.getCell(18).setCellType(Cell.CELL_TYPE_STRING);
//                        row.getCell(19).setCellType(Cell.CELL_TYPE_STRING);
//                        row.getCell(20).setCellType(Cell.CELL_TYPE_STRING);
//                        row.getCell(21).setCellType(Cell.CELL_TYPE_STRING);
//                        row.getCell(22).setCellType(Cell.CELL_TYPE_STRING);
//                        row.getCell(23).setCellType(Cell.CELL_TYPE_STRING);
                        // 必须输入项check
                        if (row.getCell(2) == null || row.getCell(2).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，机构编号列没有数据");
                        }else if (row.getCell(3) == null || row.getCell(3).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，村镇银行列没有数据");
                        }else if (row.getCell(4) == null || row.getCell(4).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，客户经理编号列没有数据");
                        }else if (row.getCell(5) == null || row.getCell(5).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，客户经理列没有数据");
                        }else if (row.getCell(6) == null || row.getCell(6).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，入职时间列没有数据");
//                        }else if (row.getCell(7) == null || row.getCell(7).getStringCellValue() == "") {
////                            return R.error("第" + (j + 1) + "行，团队负责人编号列没有数据");
////                        }else if (row.getCell(8) == null || row.getCell(8).getStringCellValue() == "") {
////                            return R.error("第" + (j + 1) + "行，团队负责人列没有数据");
//                        }else if (row.getCell(9) == null || row.getCell(9).getStringCellValue() == "") {
//                            return R.error("第" + (j + 1) + "行，团队名称列没有数据");
                        }else if (row.getCell(7) == null || row.getCell(7).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，存款日均余额1月列没有数据");
                        }else if (row.getCell(8) == null || row.getCell(8).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，存款日均余额2月列没有数据");
                        }else if (row.getCell(9) == null || row.getCell(9).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，存款日均余额3月列没有数据");
                        }else if (row.getCell(10) == null || row.getCell(10).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，存款日均余额4月列没有数据");
                        }else if (row.getCell(11) == null || row.getCell(11).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，存款日均余额5月列没有数据");
                        }else if (row.getCell(12) == null || row.getCell(12).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，存款日均余额6月列没有数据");
                        }else if (row.getCell(13) == null || row.getCell(13).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，存款日均余额7月列没有数据");
                        }else if (row.getCell(14) == null || row.getCell(14).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，存款日均余额8月列没有数据");
                        }else if (row.getCell(15) == null || row.getCell(15).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，存款日均余额9月列没有数据");
                        }else if (row.getCell(16) == null || row.getCell(16).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，存款日均余额10月列没有数据");
                        }else if (row.getCell(17) == null || row.getCell(17).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，存款日均余额11月列没有数据");
                        }else if (row.getCell(18) == null || row.getCell(18).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，存款日均余额12月列没有数据");
                        }else if (row.getCell(19) == null || row.getCell(19).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，贷款日均余额1月列没有数据");
                        }else if (row.getCell(20) == null || row.getCell(20).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，贷款日均余额2月列没有数据");
                        }else if (row.getCell(21) == null || row.getCell(21).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，贷款日均余额3月列没有数据");
                        }else if (row.getCell(22) == null || row.getCell(22).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，贷款日均余额4月列没有数据");
                        }else if (row.getCell(23) == null || row.getCell(23).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，贷款日均余额5月列没有数据");
                        }else if (row.getCell(24) == null || row.getCell(24).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，贷款日均余额6月列没有数据");
                        }else if (row.getCell(25) == null || row.getCell(25).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，贷款日均余额7月列没有数据");
                        }else if (row.getCell(26) == null || row.getCell(26).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，贷款日均余额8月列没有数据");
                        }else if (row.getCell(27) == null || row.getCell(27).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，贷款日均余额9月列没有数据");
                        }else if (row.getCell(28) == null || row.getCell(28).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，贷款日均余额10月列没有数据");
                        }else if (row.getCell(29) == null || row.getCell(29).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，贷款日均余额11月列没有数据");
                        }else if (row.getCell(30) == null || row.getCell(30).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，贷款日均余额12月列没有数据");
                        }else if (row.getCell(31) == null || row.getCell(31).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，FTP净收入1月列没有数据");
                        }else if (row.getCell(32) == null || row.getCell(32).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，FTP净收入2月列没有数据");
                        }else if (row.getCell(33) == null || row.getCell(33).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，FTP净收入3月列没有数据");
                        }else if (row.getCell(34) == null || row.getCell(34).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，FTP净收入4月列没有数据");
                        }else if (row.getCell(35) == null || row.getCell(35).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，FTP净收入5月列没有数据");
                        }else if (row.getCell(36) == null || row.getCell(36).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，FTP净收入6月列没有数据");
                        }else if (row.getCell(37) == null || row.getCell(37).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，FTP净收入7月列没有数据");
                        }else if (row.getCell(38) == null || row.getCell(38).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，FTP净收入8月列没有数据");
                        }else if (row.getCell(39) == null || row.getCell(39).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，FTP净收入9月列没有数据");
                        }else if (row.getCell(40) == null || row.getCell(40).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，FTP净收入10月列没有数据");
                        }else if (row.getCell(41) == null || row.getCell(41).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，FTP净收入11月列没有数据");
                        }else if (row.getCell(42) == null || row.getCell(42).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，FTP净收入12月列没有数据");
                        }else if (row.getCell(43) == null || row.getCell(43).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，存款新开户数1月列没有数据");
                        }else if (row.getCell(44) == null || row.getCell(44).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，存款新开户数2月列没有数据");
                        }else if (row.getCell(45) == null || row.getCell(45).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，存款新开户数3月列没有数据");
                        }else if (row.getCell(46) == null || row.getCell(46).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，存款新开户数4月列没有数据");
                        }else if (row.getCell(47) == null || row.getCell(47).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，存款新开户数5月列没有数据");
                        }else if (row.getCell(48) == null || row.getCell(48).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，存款新开户数6月列没有数据");
                        }else if (row.getCell(49) == null || row.getCell(49).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，存款新开户数7月列没有数据");
                        }else if (row.getCell(50) == null || row.getCell(50).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，存款新开户数8月列没有数据");
                        }else if (row.getCell(51) == null || row.getCell(51).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，存款新开户数9月列没有数据");
                        }else if (row.getCell(52) == null || row.getCell(52).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，存款新开户数10月列没有数据");
                        }else if (row.getCell(53) == null || row.getCell(53).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，存款新开户数11月列没有数据");
                        }else if (row.getCell(54) == null || row.getCell(54).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，存款新开户数12月列没有数据");
                        }else if (row.getCell(55) == null || row.getCell(55).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，贷款管户户数1月列没有数据");
                        }else if (row.getCell(56) == null || row.getCell(56).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，贷款管户户数2月列没有数据");
                        }else if (row.getCell(57) == null || row.getCell(57).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，贷款管户户数3月列没有数据");
                        }else if (row.getCell(58) == null || row.getCell(58).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，贷款管户户数4月列没有数据");
                        }else if (row.getCell(59) == null || row.getCell(59).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，贷款管户户数5月列没有数据");
                        }else if (row.getCell(60) == null || row.getCell(60).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，贷款管户户数6月列没有数据");
                        }else if (row.getCell(61) == null || row.getCell(61).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，贷款管户户数7月列没有数据");
                        }else if (row.getCell(62) == null || row.getCell(62).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，贷款管户户数8月列没有数据");
                        }else if (row.getCell(63) == null || row.getCell(63).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，贷款管户户数9月列没有数据");
                        }else if (row.getCell(64) == null || row.getCell(64).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，贷款管户户数10月列没有数据");
                        }else if (row.getCell(65) == null || row.getCell(65).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，贷款管户户数11月列没有数据");
                        }else if (row.getCell(66) == null || row.getCell(66).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，贷款管户户数12月列没有数据");
                        }else if (row.getCell(67) == null || row.getCell(67).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，FTP净收入基数1月列没有数据");
                        }else if (row.getCell(68) == null || row.getCell(68).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，FTP净收入基数2月列没有数据");
                        }else if (row.getCell(69) == null || row.getCell(69).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，FTP净收入基数3月列没有数据");
                        }else if (row.getCell(70) == null || row.getCell(70).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，FTP净收入基数4月列没有数据");
                        }else if (row.getCell(71) == null || row.getCell(71).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，FTP净收入基数5月列没有数据");
                        }else if (row.getCell(72) == null || row.getCell(72).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，FTP净收入基数6月列没有数据");
                        }else if (row.getCell(73) == null|| row.getCell(73).getStringCellValue() == "")  {
                            return R.error("第" + (j + 1) + "行，FTP净收入基数7月列没有数据");
                        }else if (row.getCell(74) == null || row.getCell(74).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，FTP净收入基数8月列没有数据");
                        }else if (row.getCell(75) == null || row.getCell(75).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，FTP净收入基数9月列没有数据");
                        }else if (row.getCell(76) == null || row.getCell(76).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，FTP净收入基数10月列没有数据");
                        }else if (row.getCell(77) == null || row.getCell(77).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，FTP净收入基数11月列没有数据");
                        }else if (row.getCell(78) == null || row.getCell(78).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，FTP净收入基数12月列没有数据");
                        }

                        // 机构代码存在check
                        String orgId = row.getCell(2).getStringCellValue();
                        Map<String, Object> orgParams = new HashMap<>();
                        orgParams.put("orgId", orgId);
                        orgParams.put("org", org);
                        orgParams.put("isKhy", isKhy);
                        int countOrgId = personnelMapper.queryOrgNm(orgParams);
                        if (countOrgId == 0) {
                            return R.error("第" + (j + 1) + "行,您没有权限导入其他机构的数据，机构编号【" + orgId + "】！");
                        }
                        // 客户经理编号长度check
                        String custMngrId = row.getCell(4).getStringCellValue();
                        if (custMngrId.length() > 15) {
                            return R.error("第" + (j + 1) + "行,客户经理编号【" + custMngrId + "】长度不能超过15位！");
                        }
                        // 客户经理check
                        String custMngrNm = row.getCell(5).getStringCellValue();
                        if (StrUtil.chineseLen(custMngrNm) > 50) {
                            return R.error("第" + (j + 1) + "行,客户经理【" + custMngrNm + "】长度不能超过50位！");
                        }
                        // 入职时间日期格式check
                        String inTm =  row.getCell(6).getStringCellValue();
                        if (StrUtil.isNumeric(inTm)) {
                            Date date = HSSFDateUtil.getJavaDate(Double.valueOf(inTm));
                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                            inTm = format.format(date).toString();
                        }
                        if (inTm.length() != 10) {
                            return R.error("第" + (j + 1) + "行,入职时间【" + inTm + "】不是正确的日期格式！例:YYYY-MM-DD");
                        } else {
                            if (!"-".equals(inTm.substring(4, 5)) || !"-".equals(inTm.substring(7, 8))) {
                                return R.error("第" + (j + 1) + "行,入职时间【" + inTm + "】不是正确的日期格式！例:YYYY-MM-DD");
                            }
                            String inputStr = inTm.substring(0, 4) + inTm.substring(5, 7) + inTm.substring(8, 10);
                            if (!DateUtil.isDate(inputStr)) {
                                return R.error("第" + (j + 1) + "行,入职时间【" + inTm + "】不是正确的日期格式！例:YYYY-MM-DD");
                            }

                        }
//                        String teamId = row.getCell(7).getStringCellValue();
//                        String teamNm = row.getCell(8).getStringCellValue();
//                        String team = row.getCell(9).getStringCellValue();
                        // 存款日均余额格式check
                        String depositDtAvgBal1 = row.getCell(7).getStringCellValue();
                        if (!StrUtil.isNumeric(depositDtAvgBal1)) {
                            return R.error("第" + (j + 1) + "行,1月存款日均余额【" + depositDtAvgBal1 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(depositDtAvgBal1, 16 , 6)) {
                            return R.error("第" + (j + 1) + "行,1月存款日均余额【" + depositDtAvgBal1 + "】不能超过16位整数6位小数！");
                        }
                        String depositDtAvgBal2 = row.getCell(8).getStringCellValue();
                        if (!StrUtil.isNumeric(depositDtAvgBal2)) {
                            return R.error("第" + (j + 1) + "行,2月存款日均余额【" + depositDtAvgBal2 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(depositDtAvgBal2, 16 , 6)) {
                            return R.error("第" + (j + 1) + "行,2月存款日均余额【" + depositDtAvgBal2 + "】不能超过16位整数6位小数！");
                        }
                        String depositDtAvgBal3 = row.getCell(9).getStringCellValue();
                        if (!StrUtil.isNumeric(depositDtAvgBal3)) {
                            return R.error("第" + (j + 1) + "行,3月存款日均余额【" + depositDtAvgBal3 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(depositDtAvgBal3, 16 , 6)) {
                            return R.error("第" + (j + 1) + "行,3月存款日均余额【" + depositDtAvgBal3 + "】不能超过16位整数6位小数！");
                        }
                        String depositDtAvgBal4 = row.getCell(10).getStringCellValue();
                        if (!StrUtil.isNumeric(depositDtAvgBal4)) {
                            return R.error("第" + (j + 1) + "行,4月存款日均余额【" + depositDtAvgBal4 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(depositDtAvgBal4, 16 , 6)) {
                            return R.error("第" + (j + 1) + "行,4月存款日均余额【" + depositDtAvgBal4 + "】不能超过16位整数6位小数！");
                        }
                        String depositDtAvgBal5 = row.getCell(11).getStringCellValue();
                        if (!StrUtil.isNumeric(depositDtAvgBal5)) {
                            return R.error("第" + (j + 1) + "行,5月存款日均余额【" + depositDtAvgBal5 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(depositDtAvgBal5, 16 , 6)) {
                            return R.error("第" + (j + 1) + "行,5月存款日均余额【" + depositDtAvgBal5 + "】不能超过16位整数6位小数！");
                        }
                        String depositDtAvgBal6 = row.getCell(12).getStringCellValue();
                        if (!StrUtil.isNumeric(depositDtAvgBal6)) {
                            return R.error("第" + (j + 1) + "行,6月存款日均余额【" + depositDtAvgBal6 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(depositDtAvgBal6, 16 , 6)) {
                            return R.error("第" + (j + 1) + "行,6月存款日均余额【" + depositDtAvgBal6 + "】不能超过16位整数6位小数！");
                        }
                        String depositDtAvgBal7 = row.getCell(13).getStringCellValue();
                        if (!StrUtil.isNumeric(depositDtAvgBal7)) {
                            return R.error("第" + (j + 1) + "行,7月存款日均余额【" + depositDtAvgBal7 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(depositDtAvgBal7, 16 , 6)) {
                            return R.error("第" + (j + 1) + "行,7月存款日均余额【" + depositDtAvgBal7 + "】不能超过16位整数6位小数！");
                        }
                        String depositDtAvgBal8 = row.getCell(14).getStringCellValue();
                        if (!StrUtil.isNumeric(depositDtAvgBal8)) {
                            return R.error("第" + (j + 1) + "行,8月存款日均余额【" + depositDtAvgBal8 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(depositDtAvgBal8, 16 , 6)) {
                            return R.error("第" + (j + 1) + "行,8月存款日均余额【" + depositDtAvgBal8 + "】不能超过16位整数6位小数！");
                        }
                        String depositDtAvgBal9 = row.getCell(15).getStringCellValue();
                        if (!StrUtil.isNumeric(depositDtAvgBal9)) {
                            return R.error("第" + (j + 1) + "行,9月存款日均余额【" + depositDtAvgBal9 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(depositDtAvgBal9, 16 , 6)) {
                            return R.error("第" + (j + 1) + "行,9月存款日均余额【" + depositDtAvgBal9 + "】不能超过16位整数6位小数！");
                        }
                        String depositDtAvgBal10 = row.getCell(16).getStringCellValue();
                        if (!StrUtil.isNumeric(depositDtAvgBal10)) {
                            return R.error("第" + (j + 1) + "行,10月存款日均余额【" + depositDtAvgBal10 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(depositDtAvgBal10, 16 , 6)) {
                            return R.error("第" + (j + 1) + "行,10月存款日均余额【" + depositDtAvgBal10 + "】不能超过16位整数6位小数！");
                        }
                        String depositDtAvgBal11 = row.getCell(17).getStringCellValue();
                        if (!StrUtil.isNumeric(depositDtAvgBal11)) {
                            return R.error("第" + (j + 1) + "行,11月存款日均余额【" + depositDtAvgBal11 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(depositDtAvgBal11, 16 , 6)) {
                            return R.error("第" + (j + 1) + "行,11月存款日均余额【" + depositDtAvgBal11 + "】不能超过16位整数6位小数！");
                        }
                        String depositDtAvgBal12 = row.getCell(18).getStringCellValue();
                        if (!StrUtil.isNumeric(depositDtAvgBal12)) {
                            return R.error("第" + (j + 1) + "行,12月存款日均余额【" + depositDtAvgBal12 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(depositDtAvgBal12, 16 , 6)) {
                            return R.error("第" + (j + 1) + "行,12月存款日均余额【" + depositDtAvgBal12 + "】不能超过16位整数6位小数！");
                        }
                        // 贷款日均余额格式check
                        String loanDtAvgBal1 = row.getCell(19).getStringCellValue();
                        if (!StrUtil.isNumeric(loanDtAvgBal1)) {
                            return R.error("第" + (j + 1) + "行,1月贷款日均余额【" + loanDtAvgBal1 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(loanDtAvgBal1, 16 , 6)) {
                            return R.error("第" + (j + 1) + "行,1月贷款日均余额【" + loanDtAvgBal1 + "】不能超过16位整数6位小数！");
                        }
                        String loanDtAvgBal2 = row.getCell(20).getStringCellValue();
                        if (!StrUtil.isNumeric(loanDtAvgBal2)) {
                            return R.error("第" + (j + 1) + "行,2月贷款日均余额【" + loanDtAvgBal2 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(loanDtAvgBal2, 16 , 6)) {
                            return R.error("第" + (j + 1) + "行,2月贷款日均余额【" + loanDtAvgBal2 + "】不能超过16位整数6位小数！");
                        }
                        String loanDtAvgBal3 = row.getCell(21).getStringCellValue();
                        if (!StrUtil.isNumeric(loanDtAvgBal3)) {
                            return R.error("第" + (j + 1) + "行,3月贷款日均余额【" + loanDtAvgBal3 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(loanDtAvgBal3, 16 , 6)) {
                            return R.error("第" + (j + 1) + "行,3月贷款日均余额【" + loanDtAvgBal3 + "】不能超过16位整数6位小数！");
                        }
                        String loanDtAvgBal4 = row.getCell(22).getStringCellValue();
                        if (!StrUtil.isNumeric(loanDtAvgBal4)) {
                            return R.error("第" + (j + 1) + "行,4月贷款日均余额【" + loanDtAvgBal4 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(loanDtAvgBal4, 16 , 6)) {
                            return R.error("第" + (j + 1) + "行,4月贷款日均余额【" + loanDtAvgBal4 + "】不能超过16位整数6位小数！");
                        }
                        String loanDtAvgBal5 = row.getCell(23).getStringCellValue();
                        if (!StrUtil.isNumeric(loanDtAvgBal5)) {
                            return R.error("第" + (j + 1) + "行,5月贷款日均余额【" + loanDtAvgBal5 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(loanDtAvgBal5, 16 , 6)) {
                            return R.error("第" + (j + 1) + "行,5月贷款日均余额【" + loanDtAvgBal5 + "】不能超过16位整数6位小数！");
                        }
                        String loanDtAvgBal6 = row.getCell(24).getStringCellValue();
                        if (!StrUtil.isNumeric(loanDtAvgBal6)) {
                            return R.error("第" + (j + 1) + "行,6月贷款日均余额【" + loanDtAvgBal6 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(loanDtAvgBal6, 16 , 6)) {
                            return R.error("第" + (j + 1) + "行,6月贷款日均余额【" + loanDtAvgBal6 + "】不能超过16位整数6位小数！");
                        }
                        String loanDtAvgBal7 = row.getCell(25).getStringCellValue();
                        if (!StrUtil.isNumeric(loanDtAvgBal7)) {
                            return R.error("第" + (j + 1) + "行,7月贷款日均余额【" + loanDtAvgBal7 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(loanDtAvgBal7, 16 , 6)) {
                            return R.error("第" + (j + 1) + "行,7月贷款日均余额【" + loanDtAvgBal7 + "】不能超过16位整数6位小数！");
                        }
                        String loanDtAvgBal8 = row.getCell(26).getStringCellValue();
                        if (!StrUtil.isNumeric(loanDtAvgBal8)) {
                            return R.error("第" + (j + 1) + "行,8月贷款日均余额【" + loanDtAvgBal8 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(loanDtAvgBal8, 16 , 6)) {
                            return R.error("第" + (j + 1) + "行,8月贷款日均余额【" + loanDtAvgBal8 + "】不能超过16位整数6位小数！");
                        }
                        String loanDtAvgBal9 = row.getCell(27).getStringCellValue();
                        if (!StrUtil.isNumeric(loanDtAvgBal9)) {
                            return R.error("第" + (j + 1) + "行,9月贷款日均余额【" + loanDtAvgBal9 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(loanDtAvgBal9, 16 , 6)) {
                            return R.error("第" + (j + 1) + "行,9月贷款日均余额【" + loanDtAvgBal9 + "】不能超过16位整数6位小数！");
                        }
                        String loanDtAvgBal10 = row.getCell(28).getStringCellValue();
                        if (!StrUtil.isNumeric(loanDtAvgBal10)) {
                            return R.error("第" + (j + 1) + "行,10月贷款日均余额【" + loanDtAvgBal10 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(loanDtAvgBal10, 16 , 6)) {
                            return R.error("第" + (j + 1) + "行,10月贷款日均余额【" + loanDtAvgBal10 + "】不能超过16位整数6位小数！");
                        }
                        String loanDtAvgBal11 = row.getCell(29).getStringCellValue();
                        if (!StrUtil.isNumeric(loanDtAvgBal11)) {
                            return R.error("第" + (j + 1) + "行,11月贷款日均余额【" + loanDtAvgBal11 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(loanDtAvgBal11, 16 , 6)) {
                            return R.error("第" + (j + 1) + "行,11月贷款日均余额【" + loanDtAvgBal11 + "】不能超过16位整数6位小数！");
                        }
                        String loanDtAvgBal12 = row.getCell(30).getStringCellValue();
                        if (!StrUtil.isNumeric(loanDtAvgBal12)) {
                            return R.error("第" + (j + 1) + "行,12月贷款日均余额【" + loanDtAvgBal12 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(loanDtAvgBal12, 16 , 6)) {
                            return R.error("第" + (j + 1) + "行,12月贷款日均余额【" + loanDtAvgBal12 + "】不能超过16位整数6位小数！");
                        }
                        // FTP净收入格式check
                        String netIncm1 = row.getCell(31).getStringCellValue();
                        if (!StrUtil.isNumeric(netIncm1)) {
                            return R.error("第" + (j + 1) + "行,1月FTP净收入【" + netIncm1 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(netIncm1, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,1月FTP净收入【" + netIncm1 + "】不能超过20位整数2位小数！");
                        }
                        String netIncm2 = row.getCell(32).getStringCellValue();
                        if (!StrUtil.isNumeric(netIncm2)) {
                            return R.error("第" + (j + 1) + "行,2月FTP净收入【" + netIncm2 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(netIncm2, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,2月FTP净收入【" + netIncm2 + "】不能超过20位整数2位小数！");
                        }
                        String netIncm3 = row.getCell(33).getStringCellValue();
                        if (!StrUtil.isNumeric(netIncm3)) {
                            return R.error("第" + (j + 1) + "行,3月FTP净收入【" + netIncm3 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(netIncm3, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,3月FTP净收入【" + netIncm3 + "】不能超过20位整数2位小数！");
                        }
                        String netIncm4 = row.getCell(34).getStringCellValue();
                        if (!StrUtil.isNumeric(netIncm4)) {
                            return R.error("第" + (j + 1) + "行,4月FTP净收入【" + netIncm4 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(netIncm4, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,4月FTP净收入【" + netIncm4 + "】不能超过20位整数2位小数！");
                        }
                        String netIncm5 = row.getCell(35).getStringCellValue();
                        if (!StrUtil.isNumeric(netIncm5)) {
                            return R.error("第" + (j + 1) + "行,5月FTP净收入【" + netIncm5 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(netIncm5, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,5月FTP净收入【" + netIncm5 + "】不能超过20位整数2位小数！");
                        }
                        String netIncm6 = row.getCell(36).getStringCellValue();
                        if (!StrUtil.isNumeric(netIncm6)) {
                            return R.error("第" + (j + 1) + "行,6月FTP净收入【" + netIncm6 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(netIncm6, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,6月FTP净收入【" + netIncm6 + "】不能超过20位整数2位小数！");
                        }
                        String netIncm7 = row.getCell(37).getStringCellValue();
                        if (!StrUtil.isNumeric(netIncm7)) {
                            return R.error("第" + (j + 1) + "行,7月FTP净收入【" + netIncm7 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(netIncm7, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,7月FTP净收入【" + netIncm7 + "】不能超过20位整数2位小数！");
                        }
                        String netIncm8 = row.getCell(38).getStringCellValue();
                        if (!StrUtil.isNumeric(netIncm8)) {
                            return R.error("第" + (j + 1) + "行,8月FTP净收入【" + netIncm8 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(netIncm8, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,8月FTP净收入【" + netIncm8 + "】不能超过20位整数2位小数！");
                        }
                        String netIncm9 = row.getCell(39).getStringCellValue();
                        if (!StrUtil.isNumeric(netIncm9)) {
                            return R.error("第" + (j + 1) + "行,9月FTP净收入【" + netIncm9 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(netIncm9, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,9月FTP净收入【" + netIncm9 + "】不能超过20位整数2位小数！");
                        }
                        String netIncm10 = row.getCell(40).getStringCellValue();
                        if (!StrUtil.isNumeric(netIncm10)) {
                            return R.error("第" + (j + 1) + "行,10月FTP净收入【" + netIncm10 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(netIncm10, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,10月FTP净收入【" + netIncm10 + "】不能超过20位整数2位小数！");
                        }
                        String netIncm11 = row.getCell(41).getStringCellValue();
                        if (!StrUtil.isNumeric(netIncm11)) {
                            return R.error("第" + (j + 1) + "行,11月FTP净收入【" + netIncm11 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(netIncm11, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,11月FTP净收入【" + netIncm11 + "】不能超过20位整数2位小数！");
                        }
                        String netIncm12 = row.getCell(42).getStringCellValue();
                        if (!StrUtil.isNumeric(netIncm12)) {
                            return R.error("第" + (j + 1) + "行,12月FTP净收入【" + netIncm12 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(netIncm12, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,12月FTP净收入【" + netIncm12 + "】不能超过20位整数2位小数！");
                        }
                        // 存款新开户数格式check
                        String depositNewOpenNum1 = row.getCell(43).getStringCellValue();
                        if (!StrUtil.isNumeric(depositNewOpenNum1)) {
                            return R.error("第" + (j + 1) + "行,1月存款新开户数【" + depositNewOpenNum1 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(depositNewOpenNum1, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,1月存款新开户数【" + depositNewOpenNum1 + "】不能超过20位整数2位小数！");
                        }
                        String depositNewOpenNum2 = row.getCell(44).getStringCellValue();
                        if (!StrUtil.isNumeric(depositNewOpenNum2)) {
                            return R.error("第" + (j + 1) + "行,2月存款新开户数【" + depositNewOpenNum2 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(depositNewOpenNum2, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,2月存款新开户数【" + depositNewOpenNum2 + "】不能超过20位整数2位小数！");
                        }
                        String depositNewOpenNum3 = row.getCell(45).getStringCellValue();
                        if (!StrUtil.isNumeric(depositNewOpenNum3)) {
                            return R.error("第" + (j + 1) + "行,3月存款新开户数【" + depositNewOpenNum3 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(depositNewOpenNum3, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,3月存款新开户数【" + depositNewOpenNum3 + "】不能超过20位整数2位小数！");
                        }
                        String depositNewOpenNum4 = row.getCell(46).getStringCellValue();
                        if (!StrUtil.isNumeric(depositNewOpenNum4)) {
                            return R.error("第" + (j + 1) + "行,4月存款新开户数【" + depositNewOpenNum4 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(depositNewOpenNum4, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,4月存款新开户数【" + depositNewOpenNum4 + "】不能超过20位整数2位小数！");
                        }
                        String depositNewOpenNum5 = row.getCell(47).getStringCellValue();
                        if (!StrUtil.isNumeric(depositNewOpenNum5)) {
                            return R.error("第" + (j + 1) + "行,5月存款新开户数【" + depositNewOpenNum5 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(depositNewOpenNum5, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,5月存款新开户数【" + depositNewOpenNum5 + "】不能超过20位整数2位小数！");
                        }
                        String depositNewOpenNum6 = row.getCell(48).getStringCellValue();
                        if (!StrUtil.isNumeric(depositNewOpenNum6)) {
                            return R.error("第" + (j + 1) + "行,6月存款新开户数【" + depositNewOpenNum6 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(depositNewOpenNum6, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,6月存款新开户数【" + depositNewOpenNum6 + "】不能超过20位整数2位小数！");
                        }
                        String depositNewOpenNum7 = row.getCell(49).getStringCellValue();
                        if (!StrUtil.isNumeric(depositNewOpenNum7)) {
                            return R.error("第" + (j + 1) + "行,7月存款新开户数【" + depositNewOpenNum7 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(depositNewOpenNum7, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,7月存款新开户数【" + depositNewOpenNum7 + "】不能超过20位整数2位小数！");
                        }
                        String depositNewOpenNum8 = row.getCell(50).getStringCellValue();
                        if (!StrUtil.isNumeric(depositNewOpenNum8)) {
                            return R.error("第" + (j + 1) + "行,8月存款新开户数【" + depositNewOpenNum8 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(depositNewOpenNum8, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,8月存款新开户数【" + depositNewOpenNum8 + "】不能超过20位整数2位小数！");
                        }
                        String depositNewOpenNum9 = row.getCell(51).getStringCellValue();
                        if (!StrUtil.isNumeric(depositNewOpenNum9)) {
                            return R.error("第" + (j + 1) + "行,9月存款新开户数【" + depositNewOpenNum9 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(depositNewOpenNum9, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,9月存款新开户数【" + depositNewOpenNum9 + "】不能超过20位整数2位小数！");
                        }
                        String depositNewOpenNum10 = row.getCell(52).getStringCellValue();
                        if (!StrUtil.isNumeric(depositNewOpenNum10)) {
                            return R.error("第" + (j + 1) + "行,10月存款新开户数【" + depositNewOpenNum10 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(depositNewOpenNum10, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,10月存款新开户数【" + depositNewOpenNum10 + "】不能超过20位整数2位小数！");
                        }
                        String depositNewOpenNum11 = row.getCell(53).getStringCellValue();
                        if (!StrUtil.isNumeric(depositNewOpenNum11)) {
                            return R.error("第" + (j + 1) + "行,11月存款新开户数【" + depositNewOpenNum11 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(depositNewOpenNum11, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,11月存款新开户数【" + depositNewOpenNum11 + "】不能超过20位整数2位小数！");
                        }
                        String depositNewOpenNum12 = row.getCell(54).getStringCellValue();
                        if (!StrUtil.isNumeric(depositNewOpenNum12)) {
                            return R.error("第" + (j + 1) + "行,12月存款新开户数【" + depositNewOpenNum12 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(depositNewOpenNum12, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,12月存款新开户数【" + depositNewOpenNum12 + "】不能超过20位整数2位小数！");
                        }
                        // 年度指标贷款管户户数格式check
                        String loanMngCustCustNum1 = row.getCell(55).getStringCellValue();
                        if (!StrUtil.isNumeric(loanMngCustCustNum1)) {
                            return R.error("第" + (j + 1) + "行,1月贷款管户户数【" + loanMngCustCustNum1 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(loanMngCustCustNum1, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,1月贷款管户户数【" + loanMngCustCustNum1 + "】不能超过20位整数2位小数！");
                        }
                        String loanMngCustCustNum2 = row.getCell(56).getStringCellValue();
                        if (!StrUtil.isNumeric(loanMngCustCustNum2)) {
                            return R.error("第" + (j + 1) + "行,2月贷款管户户数【" + loanMngCustCustNum2 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(loanMngCustCustNum2, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,2月贷款管户户数【" + loanMngCustCustNum2 + "】不能超过20位整数2位小数！");
                        }
                        String loanMngCustCustNum3 = row.getCell(57).getStringCellValue();
                        if (!StrUtil.isNumeric(loanMngCustCustNum3)) {
                            return R.error("第" + (j + 1) + "行,3月贷款管户户数【" + loanMngCustCustNum3 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(loanMngCustCustNum3, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,3月贷款管户户数【" + loanMngCustCustNum3 + "】不能超过20位整数2位小数！");
                        }
                        String loanMngCustCustNum4 = row.getCell(58).getStringCellValue();
                        if (!StrUtil.isNumeric(loanMngCustCustNum4)) {
                            return R.error("第" + (j + 1) + "行,4月贷款管户户数【" + loanMngCustCustNum4 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(loanMngCustCustNum4, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,4月贷款管户户数【" + loanMngCustCustNum4 + "】不能超过20位整数2位小数！");
                        }
                        String loanMngCustCustNum5 = row.getCell(59).getStringCellValue();
                        if (!StrUtil.isNumeric(loanMngCustCustNum5)) {
                            return R.error("第" + (j + 1) + "行,5月贷款管户户数【" + loanMngCustCustNum5 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(loanMngCustCustNum5, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,5月贷款管户户数【" + loanMngCustCustNum5 + "】不能超过20位整数2位小数！");
                        }
                        String loanMngCustCustNum6 = row.getCell(60).getStringCellValue();
                        if (!StrUtil.isNumeric(loanMngCustCustNum6)) {
                            return R.error("第" + (j + 1) + "行,6月贷款管户户数【" + loanMngCustCustNum6 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(loanMngCustCustNum6, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,6月贷款管户户数【" + loanMngCustCustNum6 + "】不能超过20位整数2位小数！");
                        }
                        String loanMngCustCustNum7 = row.getCell(61).getStringCellValue();
                        if (!StrUtil.isNumeric(loanMngCustCustNum7)) {
                            return R.error("第" + (j + 1) + "行,7月贷款管户户数【" + loanMngCustCustNum7 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(loanMngCustCustNum7, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,7月贷款管户户数【" + loanMngCustCustNum7 + "】不能超过20位整数2位小数！");
                        }
                        String loanMngCustCustNum8 = row.getCell(62).getStringCellValue();
                        if (!StrUtil.isNumeric(loanMngCustCustNum8)) {
                            return R.error("第" + (j + 1) + "行,8月贷款管户户数【" + loanMngCustCustNum8 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(loanMngCustCustNum8, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,8月贷款管户户数【" + loanMngCustCustNum8 + "】不能超过20位整数2位小数！");
                        }
                        String loanMngCustCustNum9 = row.getCell(63).getStringCellValue();
                        if (!StrUtil.isNumeric(loanMngCustCustNum9)) {
                            return R.error("第" + (j + 1) + "行,9月贷款管户户数【" + loanMngCustCustNum9 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(loanMngCustCustNum9, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,9月贷款管户户数【" + loanMngCustCustNum9 + "】不能超过20位整数2位小数！");
                        }
                        String loanMngCustCustNum10 = row.getCell(64).getStringCellValue();
                        if (!StrUtil.isNumeric(loanMngCustCustNum10)) {
                            return R.error("第" + (j + 1) + "行,10月贷款管户户数【" + loanMngCustCustNum10 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(loanMngCustCustNum10, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,10月贷款管户户数【" + loanMngCustCustNum10 + "】不能超过20位整数2位小数！");
                        }
                        String loanMngCustCustNum11 = row.getCell(65).getStringCellValue();
                        if (!StrUtil.isNumeric(loanMngCustCustNum11)) {
                            return R.error("第" + (j + 1) + "行,11月贷款管户户数【" + loanMngCustCustNum11 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(loanMngCustCustNum11, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,11月贷款管户户数【" + loanMngCustCustNum11 + "】不能超过20位整数2位小数！");
                        }
                        String loanMngCustCustNum12 = row.getCell(66).getStringCellValue();
                        if (!StrUtil.isNumeric(loanMngCustCustNum12)) {
                            return R.error("第" + (j + 1) + "行,12月贷款管户户数【" + loanMngCustCustNum12 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(loanMngCustCustNum12, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,12月贷款管户户数【" + loanMngCustCustNum12 + "】不能超过20位整数2位小数！");
                        }
                        // 1月FTP净收入基数格式check
                        String netIncmBase1 = row.getCell(67).getStringCellValue();
                        if (!StrUtil.isNumeric(netIncmBase1)) {
                            return R.error("第" + (j + 1) + "行,1月FTP净收入基数【" + netIncmBase1 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(netIncmBase1, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,1月FTP净收入基数【" + netIncmBase1 + "】不能超过20位整数2位小数！");
                        }
                        // 2月FTP净收入基数格式check
                        String netIncmBase2 = row.getCell(68).getStringCellValue();
                        if (!StrUtil.isNumeric(netIncmBase2)) {
                            return R.error("第" + (j + 1) + "行,2月FTP净收入基数【" + netIncmBase2 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(netIncmBase2, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,2月FTP净收入基数【" + netIncmBase2 + "】不能超过20位整数2位小数！");
                        }
                        // 3月FTP净收入基数格式check
                        String netIncmBase3 = row.getCell(69).getStringCellValue();
                        if (!StrUtil.isNumeric(netIncmBase3)) {
                            return R.error("第" + (j + 1) + "行,3月FTP净收入基数【" + netIncmBase3 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(netIncmBase3, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,3月FTP净收入基数【" + netIncmBase3 + "】不能超过20位整数2位小数！");
                        }
                        // 4月FTP净收入基数格式check
                        String netIncmBase4 = row.getCell(70).getStringCellValue();
                        if (!StrUtil.isNumeric(netIncmBase4)) {
                            return R.error("第" + (j + 1) + "行,4月FTP净收入基数【" + netIncmBase4 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(netIncmBase4, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,4月FTP净收入基数【" + netIncmBase4 + "】不能超过20位整数2位小数！");
                        }
                        // 5月FTP净收入基数格式check
                        String netIncmBase5 = row.getCell(71).getStringCellValue();
                        if (!StrUtil.isNumeric(netIncmBase5)) {
                            return R.error("第" + (j + 1) + "行,5月FTP净收入基数【" + netIncmBase5 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(netIncmBase5, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,5月FTP净收入基数【" + netIncmBase5 + "】不能超过20位整数2位小数！");
                        }
                        // 6月FTP净收入基数格式check
                        String netIncmBase6 = row.getCell(72).getStringCellValue();
                        if (!StrUtil.isNumeric(netIncmBase6)) {
                            return R.error("第" + (j + 1) + "行,6月FTP净收入基数【" + netIncmBase6 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(netIncmBase6, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,6月FTP净收入基数【" + netIncmBase6 + "】不能超过20位整数2位小数！");
                        }
                        // 7月FTP净收入基数格式check
                        String netIncmBase7 = row.getCell(73).getStringCellValue();
                        if (!StrUtil.isNumeric(netIncmBase7)) {
                            return R.error("第" + (j + 1) + "行,7月FTP净收入基数【" + netIncmBase7 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(netIncmBase7, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,7月FTP净收入基数【" + netIncmBase7 + "】不能超过20位整数2位小数！");
                        }
                        // 8月FTP净收入基数格式check
                        String netIncmBase8 = row.getCell(74).getStringCellValue();
                        if (!StrUtil.isNumeric(netIncmBase8)) {
                            return R.error("第" + (j + 1) + "行,8月FTP净收入基数【" + netIncmBase8 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(netIncmBase8, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,8月FTP净收入基数【" + netIncmBase8 + "】不能超过20位整数2位小数！");
                        }
                        // 9月FTP净收入基数格式check
                        String netIncmBase9 = row.getCell(75).getStringCellValue();
                        if (!StrUtil.isNumeric(netIncmBase9)) {
                            return R.error("第" + (j + 1) + "行,9月FTP净收入基数【" + netIncmBase9 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(netIncmBase9, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,9月FTP净收入基数【" + netIncmBase9 + "】不能超过20位整数2位小数！");
                        }
                        // 10月FTP净收入基数格式check
                        String netIncmBase10 = row.getCell(76).getStringCellValue();
                        if (!StrUtil.isNumeric(netIncmBase10)) {
                            return R.error("第" + (j + 1) + "行,10月FTP净收入基数【" + netIncmBase10 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(netIncmBase10, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,10月FTP净收入基数【" + netIncmBase10 + "】不能超过20位整数2位小数！");
                        }
                        // 11月FTP净收入基数格式check
                        String netIncmBase11 = row.getCell(77).getStringCellValue();
                        if (!StrUtil.isNumeric(netIncmBase11)) {
                            return R.error("第" + (j + 1) + "行,11月FTP净收入基数【" + netIncmBase11 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(netIncmBase11, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,11月FTP净收入基数【" + netIncmBase11 + "】不能超过20位整数2位小数！");
                        }
                        // 12月FTP净收入基数格式check
                        String netIncmBase12 = row.getCell(78).getStringCellValue();
                        if (!StrUtil.isNumeric(netIncmBase12)) {
                            return R.error("第" + (j + 1) + "行,12月FTP净收入基数【" + netIncmBase12 + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(netIncmBase12, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,12月FTP净收入基数【" + netIncmBase12 + "】不能超过20位整数2位小数！");
                        }
                        // 备注格式check
                        String remark = "";
                        if(row.getCell(79) != null){
                            remark = row.getCell(79).getStringCellValue();
                        }
                        if (StrUtil.chineseLen(remark) > 200) {
                            return R.error("第" + (j + 1) + "行,备注【" + remark + "】长度不能超过200位！");
                        }
                        //执行插入方法
                        cMngCheckBaseInfo.setYr(DateUtils.format(new Date(),"yyyy"));
                        cMngCheckBaseInfo.setOrgId(orgId);
                        cMngCheckBaseInfo.setCustMngrId(custMngrId);
                        cMngCheckBaseInfo.setCustMngrNm(custMngrNm);
                        cMngCheckBaseInfo.setInTm(inTm);
//                        cMngCheckBaseInfo.setTeamId(teamId);
//                        cMngCheckBaseInfo.setTeamNm(teamNm);
//                        cMngCheckBaseInfo.setTeam(team);
                        BigDecimal bignum1_1 = new BigDecimal(depositDtAvgBal1);
                        BigDecimal bignum1_2 = new BigDecimal(10000);
                        BigDecimal bignum1_3 = bignum1_1.multiply(bignum1_2);
                        cMngCheckBaseInfo.setDepositDtAvgBal1(String.valueOf(bignum1_3));
                        BigDecimal bignum2_1 = new BigDecimal(depositDtAvgBal2);
                        BigDecimal bignum2_2 = new BigDecimal(10000);
                        BigDecimal bignum2_3 = bignum2_1.multiply(bignum2_2);
                        cMngCheckBaseInfo.setDepositDtAvgBal2(String.valueOf(bignum2_3));
                        BigDecimal bignum3_1 = new BigDecimal(depositDtAvgBal3);
                        BigDecimal bignum3_2 = new BigDecimal(10000);
                        BigDecimal bignum3_3 = bignum3_1.multiply(bignum3_2);
                        cMngCheckBaseInfo.setDepositDtAvgBal3(String.valueOf(bignum3_3));
                        BigDecimal bignum4_1 = new BigDecimal(depositDtAvgBal4);
                        BigDecimal bignum4_2 = new BigDecimal(10000);
                        BigDecimal bignum4_3 = bignum4_1.multiply(bignum4_2);
                        cMngCheckBaseInfo.setDepositDtAvgBal4(String.valueOf(bignum4_3));
                        BigDecimal bignum5_1 = new BigDecimal(depositDtAvgBal5);
                        BigDecimal bignum5_2 = new BigDecimal(10000);
                        BigDecimal bignum5_3 = bignum5_1.multiply(bignum5_2);
                        cMngCheckBaseInfo.setDepositDtAvgBal5(String.valueOf(bignum5_3));
                        BigDecimal bignum6_1 = new BigDecimal(depositDtAvgBal6);
                        BigDecimal bignum6_2 = new BigDecimal(10000);
                        BigDecimal bignum6_3 = bignum6_1.multiply(bignum6_2);
                        cMngCheckBaseInfo.setDepositDtAvgBal6(String.valueOf(bignum6_3));
                        BigDecimal bignum7_1 = new BigDecimal(depositDtAvgBal7);
                        BigDecimal bignum7_2 = new BigDecimal(10000);
                        BigDecimal bignum7_3 = bignum7_1.multiply(bignum7_2);
                        cMngCheckBaseInfo.setDepositDtAvgBal7(String.valueOf(bignum7_3));
                        BigDecimal bignum8_1 = new BigDecimal(depositDtAvgBal8);
                        BigDecimal bignum8_2 = new BigDecimal(10000);
                        BigDecimal bignum8_3 = bignum8_1.multiply(bignum8_2);
                        cMngCheckBaseInfo.setDepositDtAvgBal8(String.valueOf(bignum8_3));
                        BigDecimal bignum9_1 = new BigDecimal(depositDtAvgBal9);
                        BigDecimal bignum9_2 = new BigDecimal(10000);
                        BigDecimal bignum9_3 = bignum9_1.multiply(bignum9_2);
                        cMngCheckBaseInfo.setDepositDtAvgBal9(String.valueOf(bignum9_3));
                        BigDecimal bignum10_1 = new BigDecimal(depositDtAvgBal10);
                        BigDecimal bignum10_2 = new BigDecimal(10000);
                        BigDecimal bignum10_3 = bignum10_1.multiply(bignum10_2);
                        cMngCheckBaseInfo.setDepositDtAvgBal10(String.valueOf(bignum10_3));
                        BigDecimal bignum11_1 = new BigDecimal(depositDtAvgBal11);
                        BigDecimal bignum11_2 = new BigDecimal(10000);
                        BigDecimal bignum11_3 = bignum11_1.multiply(bignum11_2);
                        cMngCheckBaseInfo.setDepositDtAvgBal11(String.valueOf(bignum11_3));
                        BigDecimal bignum12_1 = new BigDecimal(depositDtAvgBal12);
                        BigDecimal bignum12_2 = new BigDecimal(10000);
                        BigDecimal bignum12_3 = bignum12_1.multiply(bignum12_2);
                        cMngCheckBaseInfo.setDepositDtAvgBal12(String.valueOf(bignum12_3));
                        BigDecimal bignum1_4 = new BigDecimal(loanDtAvgBal1);
                        BigDecimal bignum1_5 = new BigDecimal(10000);
                        BigDecimal bignum1_6 = bignum1_4.multiply(bignum1_5);
                        cMngCheckBaseInfo.setLoanDtAvgBal1(String.valueOf(bignum1_6));
                        BigDecimal bignum2_4 = new BigDecimal(loanDtAvgBal2);
                        BigDecimal bignum2_5 = new BigDecimal(10000);
                        BigDecimal bignum2_6 = bignum2_4.multiply(bignum2_5);
                        cMngCheckBaseInfo.setLoanDtAvgBal2(String.valueOf(bignum2_6));
                        BigDecimal bignum3_4 = new BigDecimal(loanDtAvgBal3);
                        BigDecimal bignum3_5 = new BigDecimal(10000);
                        BigDecimal bignum3_6 = bignum3_4.multiply(bignum3_5);
                        cMngCheckBaseInfo.setLoanDtAvgBal3(String.valueOf(bignum3_6));
                        BigDecimal bignum4_4 = new BigDecimal(loanDtAvgBal4);
                        BigDecimal bignum4_5 = new BigDecimal(10000);
                        BigDecimal bignum4_6 = bignum4_4.multiply(bignum4_5);
                        cMngCheckBaseInfo.setLoanDtAvgBal4(String.valueOf(bignum4_6));
                        BigDecimal bignum5_4 = new BigDecimal(loanDtAvgBal5);
                        BigDecimal bignum5_5 = new BigDecimal(10000);
                        BigDecimal bignum5_6 = bignum5_4.multiply(bignum5_5);
                        cMngCheckBaseInfo.setLoanDtAvgBal5(String.valueOf(bignum5_6));
                        BigDecimal bignum6_4 = new BigDecimal(loanDtAvgBal6);
                        BigDecimal bignum6_5 = new BigDecimal(10000);
                        BigDecimal bignum6_6 = bignum6_4.multiply(bignum6_5);
                        cMngCheckBaseInfo.setLoanDtAvgBal6(String.valueOf(bignum6_6));
                        BigDecimal bignum7_4 = new BigDecimal(loanDtAvgBal7);
                        BigDecimal bignum7_5 = new BigDecimal(10000);
                        BigDecimal bignum7_6 = bignum7_4.multiply(bignum7_5);
                        cMngCheckBaseInfo.setLoanDtAvgBal7(String.valueOf(bignum7_6));
                        BigDecimal bignum8_4 = new BigDecimal(loanDtAvgBal8);
                        BigDecimal bignum8_5 = new BigDecimal(10000);
                        BigDecimal bignum8_6 = bignum8_4.multiply(bignum8_5);
                        cMngCheckBaseInfo.setLoanDtAvgBal8(String.valueOf(bignum8_6));
                        BigDecimal bignum9_4 = new BigDecimal(loanDtAvgBal9);
                        BigDecimal bignum9_5 = new BigDecimal(10000);
                        BigDecimal bignum9_6 = bignum9_4.multiply(bignum9_5);
                        cMngCheckBaseInfo.setLoanDtAvgBal9(String.valueOf(bignum9_6));
                        BigDecimal bignum10_4 = new BigDecimal(loanDtAvgBal10);
                        BigDecimal bignum10_5 = new BigDecimal(10000);
                        BigDecimal bignum10_6 = bignum10_4.multiply(bignum10_5);
                        cMngCheckBaseInfo.setLoanDtAvgBal10(String.valueOf(bignum10_6));
                        BigDecimal bignum11_4 = new BigDecimal(loanDtAvgBal11);
                        BigDecimal bignum11_5 = new BigDecimal(10000);
                        BigDecimal bignum11_6 = bignum11_4.multiply(bignum11_5);
                        cMngCheckBaseInfo.setLoanDtAvgBal11(String.valueOf(bignum11_6));
                        BigDecimal bignum12_4 = new BigDecimal(loanDtAvgBal12);
                        BigDecimal bignum12_5 = new BigDecimal(10000);
                        BigDecimal bignum12_6 = bignum12_4.multiply(bignum12_5);
                        cMngCheckBaseInfo.setLoanDtAvgBal12(String.valueOf(bignum12_6));
                        cMngCheckBaseInfo.setNetIncm1(netIncm1);
                        cMngCheckBaseInfo.setNetIncm2(netIncm2);
                        cMngCheckBaseInfo.setNetIncm3(netIncm3);
                        cMngCheckBaseInfo.setNetIncm4(netIncm4);
                        cMngCheckBaseInfo.setNetIncm5(netIncm5);
                        cMngCheckBaseInfo.setNetIncm6(netIncm6);
                        cMngCheckBaseInfo.setNetIncm7(netIncm7);
                        cMngCheckBaseInfo.setNetIncm8(netIncm8);
                        cMngCheckBaseInfo.setNetIncm9(netIncm9);
                        cMngCheckBaseInfo.setNetIncm10(netIncm10);
                        cMngCheckBaseInfo.setNetIncm11(netIncm11);
                        cMngCheckBaseInfo.setNetIncm12(netIncm12);
                        cMngCheckBaseInfo.setDepositNewOpenNum1(depositNewOpenNum1);
                        cMngCheckBaseInfo.setDepositNewOpenNum2(depositNewOpenNum2);
                        cMngCheckBaseInfo.setDepositNewOpenNum3(depositNewOpenNum3);
                        cMngCheckBaseInfo.setDepositNewOpenNum4(depositNewOpenNum4);
                        cMngCheckBaseInfo.setDepositNewOpenNum5(depositNewOpenNum5);
                        cMngCheckBaseInfo.setDepositNewOpenNum6(depositNewOpenNum6);
                        cMngCheckBaseInfo.setDepositNewOpenNum7(depositNewOpenNum7);
                        cMngCheckBaseInfo.setDepositNewOpenNum8(depositNewOpenNum8);
                        cMngCheckBaseInfo.setDepositNewOpenNum9(depositNewOpenNum9);
                        cMngCheckBaseInfo.setDepositNewOpenNum10(depositNewOpenNum10);
                        cMngCheckBaseInfo.setDepositNewOpenNum11(depositNewOpenNum11);
                        cMngCheckBaseInfo.setDepositNewOpenNum12(depositNewOpenNum12);
                        cMngCheckBaseInfo.setLoanMngCustCustNum1(loanMngCustCustNum1);
                        cMngCheckBaseInfo.setLoanMngCustCustNum2(loanMngCustCustNum2);
                        cMngCheckBaseInfo.setLoanMngCustCustNum3(loanMngCustCustNum3);
                        cMngCheckBaseInfo.setLoanMngCustCustNum4(loanMngCustCustNum4);
                        cMngCheckBaseInfo.setLoanMngCustCustNum5(loanMngCustCustNum5);
                        cMngCheckBaseInfo.setLoanMngCustCustNum6(loanMngCustCustNum6);
                        cMngCheckBaseInfo.setLoanMngCustCustNum7(loanMngCustCustNum7);
                        cMngCheckBaseInfo.setLoanMngCustCustNum8(loanMngCustCustNum8);
                        cMngCheckBaseInfo.setLoanMngCustCustNum9(loanMngCustCustNum9);
                        cMngCheckBaseInfo.setLoanMngCustCustNum10(loanMngCustCustNum10);
                        cMngCheckBaseInfo.setLoanMngCustCustNum11(loanMngCustCustNum11);
                        cMngCheckBaseInfo.setLoanMngCustCustNum12(loanMngCustCustNum12);
                        cMngCheckBaseInfo.setNetIncmBase1(netIncmBase1);
                        cMngCheckBaseInfo.setNetIncmBase2(netIncmBase2);
                        cMngCheckBaseInfo.setNetIncmBase3(netIncmBase3);
                        cMngCheckBaseInfo.setNetIncmBase4(netIncmBase4);
                        cMngCheckBaseInfo.setNetIncmBase5(netIncmBase5);
                        cMngCheckBaseInfo.setNetIncmBase6(netIncmBase6);
                        cMngCheckBaseInfo.setNetIncmBase7(netIncmBase7);
                        cMngCheckBaseInfo.setNetIncmBase8(netIncmBase8);
                        cMngCheckBaseInfo.setNetIncmBase9(netIncmBase9);
                        cMngCheckBaseInfo.setNetIncmBase10(netIncmBase10);
                        cMngCheckBaseInfo.setNetIncmBase11(netIncmBase11);
                        cMngCheckBaseInfo.setNetIncmBase12(netIncmBase12);
                        cMngCheckBaseInfo.setOperateDate(DateUtils.format(new Date(),"yyyy-MM-dd"));
                        cMngCheckBaseInfo.setOperator(ShiroUtils.getUserEntity().getUsername());
                        if(remark != null && remark != ""){
                            cMngCheckBaseInfo.setRemark(remark);
                        }
                        personnelMapper.deleteByYr(cMngCheckBaseInfo);
                        personnelMapper.insertByYr(cMngCheckBaseInfo);
                        //批量导入时进行日志备份
                        CMngCheckBaseInfoHis baseInfoHis = new CMngCheckBaseInfoHis();
                        List<CMngCheckBaseInfo> cMngCheckBaseInfosAfter = new ArrayList<>();
                        cMngCheckBaseInfosAfter.add(cMngCheckBaseInfo);
                        baseInfoHis.setYr(cMngCheckBaseInfosAfter.get(0).getYr());
                        baseInfoHis.setOrgId(cMngCheckBaseInfosAfter.get(0).getOrgId());
                        baseInfoHis.setCustMngrId(cMngCheckBaseInfosAfter.get(0).getCustMngrId());
                        baseInfoHis.setCustMngrNm(cMngCheckBaseInfosAfter.get(0).getCustMngrNm());
//                        baseInfoHis.setTeamId(cMngCheckBaseInfosAfter.get(0).getTeamId());
//                        baseInfoHis.setTeamNm(cMngCheckBaseInfosAfter.get(0).getTeamNm());
                        baseInfoHis.setInTm(cMngCheckBaseInfosAfter.get(0).getInTm());
                        baseInfoHis.setDepositDtAvgBal1_After(cMngCheckBaseInfosAfter.get(0).getDepositDtAvgBal1());
                        baseInfoHis.setDepositDtAvgBal2_After(cMngCheckBaseInfosAfter.get(0).getDepositDtAvgBal2());
                        baseInfoHis.setDepositDtAvgBal3_After(cMngCheckBaseInfosAfter.get(0).getDepositDtAvgBal3());
                        baseInfoHis.setDepositDtAvgBal4_After(cMngCheckBaseInfosAfter.get(0).getDepositDtAvgBal4());
                        baseInfoHis.setDepositDtAvgBal5_After(cMngCheckBaseInfosAfter.get(0).getDepositDtAvgBal5());
                        baseInfoHis.setDepositDtAvgBal6_After(cMngCheckBaseInfosAfter.get(0).getDepositDtAvgBal6());
                        baseInfoHis.setDepositDtAvgBal7_After(cMngCheckBaseInfosAfter.get(0).getDepositDtAvgBal7());
                        baseInfoHis.setDepositDtAvgBal8_After(cMngCheckBaseInfosAfter.get(0).getDepositDtAvgBal8());
                        baseInfoHis.setDepositDtAvgBal9_After(cMngCheckBaseInfosAfter.get(0).getDepositDtAvgBal9());
                        baseInfoHis.setDepositDtAvgBal10_After(cMngCheckBaseInfosAfter.get(0).getDepositDtAvgBal10());
                        baseInfoHis.setDepositDtAvgBal11_After(cMngCheckBaseInfosAfter.get(0).getDepositDtAvgBal11());
                        baseInfoHis.setDepositDtAvgBal12_After(cMngCheckBaseInfosAfter.get(0).getDepositDtAvgBal12());
                        baseInfoHis.setLoanDtAvgBal1_After(cMngCheckBaseInfosAfter.get(0).getLoanDtAvgBal1());
                        baseInfoHis.setLoanDtAvgBal2_After(cMngCheckBaseInfosAfter.get(0).getLoanDtAvgBal2());
                        baseInfoHis.setLoanDtAvgBal3_After(cMngCheckBaseInfosAfter.get(0).getLoanDtAvgBal3());
                        baseInfoHis.setLoanDtAvgBal4_After(cMngCheckBaseInfosAfter.get(0).getLoanDtAvgBal4());
                        baseInfoHis.setLoanDtAvgBal5_After(cMngCheckBaseInfosAfter.get(0).getLoanDtAvgBal5());
                        baseInfoHis.setLoanDtAvgBal6_After(cMngCheckBaseInfosAfter.get(0).getLoanDtAvgBal6());
                        baseInfoHis.setLoanDtAvgBal7_After(cMngCheckBaseInfosAfter.get(0).getLoanDtAvgBal7());
                        baseInfoHis.setLoanDtAvgBal8_After(cMngCheckBaseInfosAfter.get(0).getLoanDtAvgBal8());
                        baseInfoHis.setLoanDtAvgBal9_After(cMngCheckBaseInfosAfter.get(0).getLoanDtAvgBal9());
                        baseInfoHis.setLoanDtAvgBal10_After(cMngCheckBaseInfosAfter.get(0).getLoanDtAvgBal10());
                        baseInfoHis.setLoanDtAvgBal11_After(cMngCheckBaseInfosAfter.get(0).getLoanDtAvgBal11());
                        baseInfoHis.setLoanDtAvgBal12_After(cMngCheckBaseInfosAfter.get(0).getLoanDtAvgBal12());
                        baseInfoHis.setNetIncm1_After(cMngCheckBaseInfosAfter.get(0).getNetIncm1());
                        baseInfoHis.setNetIncm2_After(cMngCheckBaseInfosAfter.get(0).getNetIncm2());
                        baseInfoHis.setNetIncm3_After(cMngCheckBaseInfosAfter.get(0).getNetIncm3());
                        baseInfoHis.setNetIncm4_After(cMngCheckBaseInfosAfter.get(0).getNetIncm4());
                        baseInfoHis.setNetIncm5_After(cMngCheckBaseInfosAfter.get(0).getNetIncm5());
                        baseInfoHis.setNetIncm6_After(cMngCheckBaseInfosAfter.get(0).getNetIncm6());
                        baseInfoHis.setNetIncm7_After(cMngCheckBaseInfosAfter.get(0).getNetIncm7());
                        baseInfoHis.setNetIncm8_After(cMngCheckBaseInfosAfter.get(0).getNetIncm8());
                        baseInfoHis.setNetIncm9_After(cMngCheckBaseInfosAfter.get(0).getNetIncm9());
                        baseInfoHis.setNetIncm10_After(cMngCheckBaseInfosAfter.get(0).getNetIncm10());
                        baseInfoHis.setNetIncm11_After(cMngCheckBaseInfosAfter.get(0).getNetIncm11());
                        baseInfoHis.setNetIncm12_After(cMngCheckBaseInfosAfter.get(0).getNetIncm12());
                        baseInfoHis.setDepositNewOpenNum1_After(cMngCheckBaseInfosAfter.get(0).getDepositNewOpenNum1());
                        baseInfoHis.setDepositNewOpenNum2_After(cMngCheckBaseInfosAfter.get(0).getDepositNewOpenNum2());
                        baseInfoHis.setDepositNewOpenNum3_After(cMngCheckBaseInfosAfter.get(0).getDepositNewOpenNum3());
                        baseInfoHis.setDepositNewOpenNum4_After(cMngCheckBaseInfosAfter.get(0).getDepositNewOpenNum4());
                        baseInfoHis.setDepositNewOpenNum5_After(cMngCheckBaseInfosAfter.get(0).getDepositNewOpenNum5());
                        baseInfoHis.setDepositNewOpenNum6_After(cMngCheckBaseInfosAfter.get(0).getDepositNewOpenNum6());
                        baseInfoHis.setDepositNewOpenNum7_After(cMngCheckBaseInfosAfter.get(0).getDepositNewOpenNum7());
                        baseInfoHis.setDepositNewOpenNum8_After(cMngCheckBaseInfosAfter.get(0).getDepositNewOpenNum8());
                        baseInfoHis.setDepositNewOpenNum9_After(cMngCheckBaseInfosAfter.get(0).getDepositNewOpenNum9());
                        baseInfoHis.setDepositNewOpenNum10_After(cMngCheckBaseInfosAfter.get(0).getDepositNewOpenNum10());
                        baseInfoHis.setDepositNewOpenNum11_After(cMngCheckBaseInfosAfter.get(0).getDepositNewOpenNum11());
                        baseInfoHis.setDepositNewOpenNum12_After(cMngCheckBaseInfosAfter.get(0).getDepositNewOpenNum12());
                        baseInfoHis.setLoanMngCustCustNum1_After(cMngCheckBaseInfosAfter.get(0).getLoanMngCustCustNum1());
                        baseInfoHis.setLoanMngCustCustNum2_After(cMngCheckBaseInfosAfter.get(0).getLoanMngCustCustNum2());
                        baseInfoHis.setLoanMngCustCustNum3_After(cMngCheckBaseInfosAfter.get(0).getLoanMngCustCustNum3());
                        baseInfoHis.setLoanMngCustCustNum4_After(cMngCheckBaseInfosAfter.get(0).getLoanMngCustCustNum4());
                        baseInfoHis.setLoanMngCustCustNum5_After(cMngCheckBaseInfosAfter.get(0).getLoanMngCustCustNum5());
                        baseInfoHis.setLoanMngCustCustNum6_After(cMngCheckBaseInfosAfter.get(0).getLoanMngCustCustNum6());
                        baseInfoHis.setLoanMngCustCustNum7_After(cMngCheckBaseInfosAfter.get(0).getLoanMngCustCustNum7());
                        baseInfoHis.setLoanMngCustCustNum8_After(cMngCheckBaseInfosAfter.get(0).getLoanMngCustCustNum8());
                        baseInfoHis.setLoanMngCustCustNum9_After(cMngCheckBaseInfosAfter.get(0).getLoanMngCustCustNum9());
                        baseInfoHis.setLoanMngCustCustNum10_After(cMngCheckBaseInfosAfter.get(0).getLoanMngCustCustNum10());
                        baseInfoHis.setLoanMngCustCustNum11_After(cMngCheckBaseInfosAfter.get(0).getLoanMngCustCustNum11());
                        baseInfoHis.setLoanMngCustCustNum12_After(cMngCheckBaseInfosAfter.get(0).getLoanMngCustCustNum12());
                        baseInfoHis.setNetIncmBase1_After(cMngCheckBaseInfosAfter.get(0).getNetIncmBase1());
                        baseInfoHis.setNetIncmBase2_After(cMngCheckBaseInfosAfter.get(0).getNetIncmBase2());
                        baseInfoHis.setNetIncmBase3_After(cMngCheckBaseInfosAfter.get(0).getNetIncmBase3());
                        baseInfoHis.setNetIncmBase4_After(cMngCheckBaseInfosAfter.get(0).getNetIncmBase4());
                        baseInfoHis.setNetIncmBase5_After(cMngCheckBaseInfosAfter.get(0).getNetIncmBase5());
                        baseInfoHis.setNetIncmBase6_After(cMngCheckBaseInfosAfter.get(0).getNetIncmBase6());
                        baseInfoHis.setNetIncmBase7_After(cMngCheckBaseInfosAfter.get(0).getNetIncmBase7());
                        baseInfoHis.setNetIncmBase8_After(cMngCheckBaseInfosAfter.get(0).getNetIncmBase8());
                        baseInfoHis.setNetIncmBase9_After(cMngCheckBaseInfosAfter.get(0).getNetIncmBase9());
                        baseInfoHis.setNetIncmBase10_After(cMngCheckBaseInfosAfter.get(0).getNetIncmBase10());
                        baseInfoHis.setNetIncmBase11_After(cMngCheckBaseInfosAfter.get(0).getNetIncmBase11());
                        baseInfoHis.setNetIncmBase12_After(cMngCheckBaseInfosAfter.get(0).getNetIncmBase12());
                        baseInfoHis.setOperateDate(DateUtils.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
                        baseInfoHis.setOperator(ShiroUtils.getUserEntity().getName());
                        baseInfoHis.setLogo("BATCHINSERT");//操作标识
                        personnelMapper.insertByYrHis(baseInfoHis);
//                        if (list.size() == 24) {
//                            cMngCheckBaseInfo = new CMngCheckBaseInfo();
//                            setCMngCheckBaseInfo(cMngCheckBaseInfo, list);
//                        } else if (list.size() == 25) {
//                            cMngCheckBaseInfo = new CMngCheckBaseInfo();
//                            setCMngCheckBaseInfo(cMngCheckBaseInfo, list);
//                        }
//                        else if(list.size()!=24&&list.size()!=25) {
//                            return R.error(500,"上传模板不正确");
//                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                workbook.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return R.ok("导入成功");
    }

    public void setCMngCheckBaseInfo(CMngCheckBaseInfo cMngCheckBaseInfo,List<String> list){

        cMngCheckBaseInfo.setYr(list.get(0));
        cMngCheckBaseInfo.setOrgId(list.get(1));
        cMngCheckBaseInfo.setCustMngrId(list.get(2));
        cMngCheckBaseInfo.setCustMngrNm(list.get(3));
        cMngCheckBaseInfo.setTeamId(list.get(4));
        cMngCheckBaseInfo.setTeamNm(list.get(5));
//        cMngCheckBaseInfo.setInTm(Utils.excelFormatData(list.get(6)));
        cMngCheckBaseInfo.setInTm(list.get(6));
        cMngCheckBaseInfo.setDepositDtAvgBal1(list.get(7));
        cMngCheckBaseInfo.setDepositDtAvgBal2(list.get(8));
        cMngCheckBaseInfo.setDepositDtAvgBal3(list.get(9));
        cMngCheckBaseInfo.setDepositDtAvgBal4(list.get(10));
        cMngCheckBaseInfo.setDepositDtAvgBal5(list.get(11));
        cMngCheckBaseInfo.setDepositDtAvgBal6(list.get(12));
        cMngCheckBaseInfo.setDepositDtAvgBal7(list.get(13));
        cMngCheckBaseInfo.setDepositDtAvgBal8(list.get(14));
        cMngCheckBaseInfo.setDepositDtAvgBal9(list.get(15));
        cMngCheckBaseInfo.setDepositDtAvgBal10(list.get(16));
        cMngCheckBaseInfo.setDepositDtAvgBal11(list.get(17));
        cMngCheckBaseInfo.setDepositDtAvgBal12(list.get(18));
        cMngCheckBaseInfo.setLoanDtAvgBal1(list.get(19));
        cMngCheckBaseInfo.setLoanDtAvgBal2(list.get(20));
        cMngCheckBaseInfo.setLoanDtAvgBal3(list.get(21));
        cMngCheckBaseInfo.setLoanDtAvgBal4(list.get(22));
        cMngCheckBaseInfo.setLoanDtAvgBal5(list.get(23));
        cMngCheckBaseInfo.setLoanDtAvgBal6(list.get(24));
        cMngCheckBaseInfo.setLoanDtAvgBal7(list.get(25));
        cMngCheckBaseInfo.setLoanDtAvgBal8(list.get(26));
        cMngCheckBaseInfo.setLoanDtAvgBal9(list.get(27));
        cMngCheckBaseInfo.setLoanDtAvgBal10(list.get(28));
        cMngCheckBaseInfo.setLoanDtAvgBal11(list.get(29));
        cMngCheckBaseInfo.setLoanDtAvgBal12(list.get(30));
        cMngCheckBaseInfo.setNetIncm1(list.get(31));
        cMngCheckBaseInfo.setNetIncm2(list.get(32));
        cMngCheckBaseInfo.setNetIncm3(list.get(33));
        cMngCheckBaseInfo.setNetIncm4(list.get(34));
        cMngCheckBaseInfo.setNetIncm5(list.get(35));
        cMngCheckBaseInfo.setNetIncm6(list.get(36));
        cMngCheckBaseInfo.setNetIncm7(list.get(37));
        cMngCheckBaseInfo.setNetIncm8(list.get(38));
        cMngCheckBaseInfo.setNetIncm9(list.get(39));
        cMngCheckBaseInfo.setNetIncm10(list.get(40));
        cMngCheckBaseInfo.setNetIncm11(list.get(41));
        cMngCheckBaseInfo.setNetIncm12(list.get(42));
        cMngCheckBaseInfo.setDepositNewOpenNum1(list.get(43));
        cMngCheckBaseInfo.setDepositNewOpenNum2(list.get(44));
        cMngCheckBaseInfo.setDepositNewOpenNum3(list.get(45));
        cMngCheckBaseInfo.setDepositNewOpenNum4(list.get(46));
        cMngCheckBaseInfo.setDepositNewOpenNum5(list.get(47));
        cMngCheckBaseInfo.setDepositNewOpenNum6(list.get(48));
        cMngCheckBaseInfo.setDepositNewOpenNum7(list.get(49));
        cMngCheckBaseInfo.setDepositNewOpenNum8(list.get(50));
        cMngCheckBaseInfo.setDepositNewOpenNum9(list.get(51));
        cMngCheckBaseInfo.setDepositNewOpenNum10(list.get(52));
        cMngCheckBaseInfo.setDepositNewOpenNum11(list.get(53));
        cMngCheckBaseInfo.setDepositNewOpenNum12(list.get(54));
        cMngCheckBaseInfo.setLoanMngCustCustNum1(list.get(55));
        cMngCheckBaseInfo.setLoanMngCustCustNum2(list.get(56));
        cMngCheckBaseInfo.setLoanMngCustCustNum3(list.get(57));
        cMngCheckBaseInfo.setLoanMngCustCustNum4(list.get(58));
        cMngCheckBaseInfo.setLoanMngCustCustNum5(list.get(59));
        cMngCheckBaseInfo.setLoanMngCustCustNum6(list.get(60));
        cMngCheckBaseInfo.setLoanMngCustCustNum7(list.get(61));
        cMngCheckBaseInfo.setLoanMngCustCustNum8(list.get(62));
        cMngCheckBaseInfo.setLoanMngCustCustNum9(list.get(63));
        cMngCheckBaseInfo.setLoanMngCustCustNum10(list.get(64));
        cMngCheckBaseInfo.setLoanMngCustCustNum11(list.get(65));
        cMngCheckBaseInfo.setLoanMngCustCustNum12(list.get(66));
        cMngCheckBaseInfo.setNetIncmBase1(list.get(67));
        cMngCheckBaseInfo.setNetIncmBase2(list.get(68));
        cMngCheckBaseInfo.setNetIncmBase3(list.get(69));
        cMngCheckBaseInfo.setNetIncmBase4(list.get(70));
        cMngCheckBaseInfo.setNetIncmBase5(list.get(71));
        cMngCheckBaseInfo.setNetIncmBase6(list.get(72));
        cMngCheckBaseInfo.setNetIncmBase7(list.get(73));
        cMngCheckBaseInfo.setNetIncmBase8(list.get(74));
        cMngCheckBaseInfo.setNetIncmBase9(list.get(75));
        cMngCheckBaseInfo.setNetIncmBase10(list.get(76));
        cMngCheckBaseInfo.setNetIncmBase11(list.get(77));
        cMngCheckBaseInfo.setNetIncmBase12(list.get(78));
        cMngCheckBaseInfo.setOperateDate(DateUtils.format(new Date(),"yyyy-MM-dd"));
        cMngCheckBaseInfo.setOperator(ShiroUtils.getUserEntity().getName());
//        if(list.size() == 25){
        cMngCheckBaseInfo.setRemark(list.get(79));
//        }
        personnelMapper.deleteByYr(cMngCheckBaseInfo);
        personnelMapper.insertByYr(cMngCheckBaseInfo);
    }


    @Override
    public List<CMngCheckBaseInfo> queryByLimit(Map<String, Object> params){
        int pageNo = Integer.parseInt((String) params.get("page"));
        int pageCount = Integer.parseInt((String)params.get("limit"));
        int offset = ((pageNo - 1) * pageCount);
        params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
        params.put("offset", offset);
        return personnelMapper.queryByLimit(params);
    }

    @Override
    public int queryDataListNb(Map<String, Object> params){
        int nb = personnelMapper.queryDataListNb(params);
        return nb;
    }

    @Override
    public void deleteData(CMngCheckBaseInfo cMngCheckBaseInfo){

        personnelMapper.deleteByYr(cMngCheckBaseInfo);
    }


    @Override
    public void updateData(CMngCheckBaseInfo cMngCheckBaseInfo){
        cMngCheckBaseInfo.setOperateDate(DateUtils.format(new Date(),"yyyy-MM-dd"));

        String depositDtAvgBal1 = cMngCheckBaseInfo.getDepositDtAvgBal1();
        BigDecimal bignum1_1 = new BigDecimal(depositDtAvgBal1);
        BigDecimal bignum1_2 = new BigDecimal(10000);
        BigDecimal bignum1_3 = bignum1_1.multiply(bignum1_2);
        cMngCheckBaseInfo.setDepositDtAvgBal1(String.valueOf(bignum1_3));
        String depositDtAvgBal2 = cMngCheckBaseInfo.getDepositDtAvgBal2();
        BigDecimal bignum2_1 = new BigDecimal(depositDtAvgBal2);
        BigDecimal bignum2_2 = new BigDecimal(10000);
        BigDecimal bignum2_3 = bignum2_1.multiply(bignum2_2);
        cMngCheckBaseInfo.setDepositDtAvgBal2(String.valueOf(bignum2_3));
        String depositDtAvgBal3 = cMngCheckBaseInfo.getDepositDtAvgBal3();
        BigDecimal bignum3_1 = new BigDecimal(depositDtAvgBal3);
        BigDecimal bignum3_2 = new BigDecimal(10000);
        BigDecimal bignum3_3 = bignum3_1.multiply(bignum3_2);
        cMngCheckBaseInfo.setDepositDtAvgBal3(String.valueOf(bignum3_3));
        String depositDtAvgBal4 = cMngCheckBaseInfo.getDepositDtAvgBal4();
        BigDecimal bignum4_1 = new BigDecimal(depositDtAvgBal4);
        BigDecimal bignum4_2 = new BigDecimal(10000);
        BigDecimal bignum4_3 = bignum4_1.multiply(bignum4_2);
        cMngCheckBaseInfo.setDepositDtAvgBal4(String.valueOf(bignum4_3));
        String depositDtAvgBal5 = cMngCheckBaseInfo.getDepositDtAvgBal5();
        BigDecimal bignum5_1 = new BigDecimal(depositDtAvgBal5);
        BigDecimal bignum5_2 = new BigDecimal(10000);
        BigDecimal bignum5_3 = bignum5_1.multiply(bignum5_2);
        cMngCheckBaseInfo.setDepositDtAvgBal5(String.valueOf(bignum5_3));
        String depositDtAvgBal6 = cMngCheckBaseInfo.getDepositDtAvgBal6();
        BigDecimal bignum6_1 = new BigDecimal(depositDtAvgBal6);
        BigDecimal bignum6_2 = new BigDecimal(10000);
        BigDecimal bignum6_3 = bignum6_1.multiply(bignum6_2);
        cMngCheckBaseInfo.setDepositDtAvgBal6(String.valueOf(bignum6_3));
        String depositDtAvgBal7 = cMngCheckBaseInfo.getDepositDtAvgBal7();
        BigDecimal bignum7_1 = new BigDecimal(depositDtAvgBal7);
        BigDecimal bignum7_2 = new BigDecimal(10000);
        BigDecimal bignum7_3 = bignum7_1.multiply(bignum7_2);
        cMngCheckBaseInfo.setDepositDtAvgBal7(String.valueOf(bignum7_3));
        String depositDtAvgBal8 = cMngCheckBaseInfo.getDepositDtAvgBal8();
        BigDecimal bignum8_1 = new BigDecimal(depositDtAvgBal8);
        BigDecimal bignum8_2 = new BigDecimal(10000);
        BigDecimal bignum8_3 = bignum8_1.multiply(bignum8_2);
        cMngCheckBaseInfo.setDepositDtAvgBal8(String.valueOf(bignum8_3));
        String depositDtAvgBal9 = cMngCheckBaseInfo.getDepositDtAvgBal9();
        BigDecimal bignum9_1 = new BigDecimal(depositDtAvgBal9);
        BigDecimal bignum9_2 = new BigDecimal(10000);
        BigDecimal bignum9_3 = bignum9_1.multiply(bignum9_2);
        cMngCheckBaseInfo.setDepositDtAvgBal9(String.valueOf(bignum9_3));
        String depositDtAvgBal10 = cMngCheckBaseInfo.getDepositDtAvgBal10();
        BigDecimal bignum10_1 = new BigDecimal(depositDtAvgBal10);
        BigDecimal bignum10_2 = new BigDecimal(10000);
        BigDecimal bignum10_3 = bignum10_1.multiply(bignum10_2);
        cMngCheckBaseInfo.setDepositDtAvgBal10(String.valueOf(bignum10_3));
        String depositDtAvgBal11 = cMngCheckBaseInfo.getDepositDtAvgBal11();
        BigDecimal bignum11_1 = new BigDecimal(depositDtAvgBal11);
        BigDecimal bignum11_2 = new BigDecimal(10000);
        BigDecimal bignum11_3 = bignum11_1.multiply(bignum11_2);
        cMngCheckBaseInfo.setDepositDtAvgBal11(String.valueOf(bignum11_3));
        String depositDtAvgBal12 = cMngCheckBaseInfo.getDepositDtAvgBal12();
        BigDecimal bignum12_1 = new BigDecimal(depositDtAvgBal12);
        BigDecimal bignum12_2 = new BigDecimal(10000);
        BigDecimal bignum12_3 = bignum12_1.multiply(bignum12_2);
        cMngCheckBaseInfo.setDepositDtAvgBal12(String.valueOf(bignum12_3));
        String loanDtAvgBal1 = cMngCheckBaseInfo.getLoanDtAvgBal1();
        BigDecimal bignum1_4 = new BigDecimal(loanDtAvgBal1);
        BigDecimal bignum1_5 = new BigDecimal(10000);
        BigDecimal bignum1_6 = bignum1_4.multiply(bignum1_5);
        cMngCheckBaseInfo.setLoanDtAvgBal1(String.valueOf(bignum1_6));
        String loanDtAvgBal2 = cMngCheckBaseInfo.getLoanDtAvgBal2();
        BigDecimal bignum2_4 = new BigDecimal(loanDtAvgBal2);
        BigDecimal bignum2_5 = new BigDecimal(10000);
        BigDecimal bignum2_6 = bignum2_4.multiply(bignum2_5);
        cMngCheckBaseInfo.setLoanDtAvgBal2(String.valueOf(bignum2_6));
        String loanDtAvgBal3 = cMngCheckBaseInfo.getLoanDtAvgBal3();
        BigDecimal bignum3_4 = new BigDecimal(loanDtAvgBal3);
        BigDecimal bignum3_5 = new BigDecimal(10000);
        BigDecimal bignum3_6 = bignum3_4.multiply(bignum3_5);
        cMngCheckBaseInfo.setLoanDtAvgBal3(String.valueOf(bignum3_6));
        String loanDtAvgBal4 = cMngCheckBaseInfo.getLoanDtAvgBal4();
        BigDecimal bignum4_4 = new BigDecimal(loanDtAvgBal4);
        BigDecimal bignum4_5 = new BigDecimal(10000);
        BigDecimal bignum4_6 = bignum4_4.multiply(bignum4_5);
        cMngCheckBaseInfo.setLoanDtAvgBal4(String.valueOf(bignum4_6));
        String loanDtAvgBal5 = cMngCheckBaseInfo.getLoanDtAvgBal5();
        BigDecimal bignum5_4 = new BigDecimal(loanDtAvgBal5);
        BigDecimal bignum5_5 = new BigDecimal(10000);
        BigDecimal bignum5_6 = bignum5_4.multiply(bignum5_5);
        cMngCheckBaseInfo.setLoanDtAvgBal5(String.valueOf(bignum5_6));
        String loanDtAvgBal6 = cMngCheckBaseInfo.getLoanDtAvgBal6();
        BigDecimal bignum6_4 = new BigDecimal(loanDtAvgBal6);
        BigDecimal bignum6_5 = new BigDecimal(10000);
        BigDecimal bignum6_6 = bignum6_4.multiply(bignum6_5);
        cMngCheckBaseInfo.setLoanDtAvgBal6(String.valueOf(bignum6_6));
        String loanDtAvgBal7 = cMngCheckBaseInfo.getLoanDtAvgBal7();
        BigDecimal bignum7_4 = new BigDecimal(loanDtAvgBal7);
        BigDecimal bignum7_5 = new BigDecimal(10000);
        BigDecimal bignum7_6 = bignum7_4.multiply(bignum7_5);
        cMngCheckBaseInfo.setLoanDtAvgBal7(String.valueOf(bignum7_6));
        String loanDtAvgBal8 = cMngCheckBaseInfo.getLoanDtAvgBal8();
        BigDecimal bignum8_4 = new BigDecimal(loanDtAvgBal8);
        BigDecimal bignum8_5 = new BigDecimal(10000);
        BigDecimal bignum8_6 = bignum8_4.multiply(bignum8_5);
        cMngCheckBaseInfo.setLoanDtAvgBal8(String.valueOf(bignum8_6));
        String loanDtAvgBal9 = cMngCheckBaseInfo.getLoanDtAvgBal9();
        BigDecimal bignum9_4 = new BigDecimal(loanDtAvgBal9);
        BigDecimal bignum9_5 = new BigDecimal(10000);
        BigDecimal bignum9_6 = bignum9_4.multiply(bignum9_5);
        cMngCheckBaseInfo.setLoanDtAvgBal9(String.valueOf(bignum9_6));
        String loanDtAvgBal10 = cMngCheckBaseInfo.getLoanDtAvgBal10();
        BigDecimal bignum10_4 = new BigDecimal(loanDtAvgBal10);
        BigDecimal bignum10_5 = new BigDecimal(10000);
        BigDecimal bignum10_6 = bignum10_4.multiply(bignum10_5);
        cMngCheckBaseInfo.setLoanDtAvgBal10(String.valueOf(bignum10_6));
        String loanDtAvgBal11 = cMngCheckBaseInfo.getLoanDtAvgBal11();
        BigDecimal bignum11_4 = new BigDecimal(loanDtAvgBal11);
        BigDecimal bignum11_5 = new BigDecimal(10000);
        BigDecimal bignum11_6 = bignum11_4.multiply(bignum11_5);
        cMngCheckBaseInfo.setLoanDtAvgBal11(String.valueOf(bignum11_6));
        String loanDtAvgBal12 = cMngCheckBaseInfo.getLoanDtAvgBal12();
        BigDecimal bignum12_4 = new BigDecimal(loanDtAvgBal12);
        BigDecimal bignum12_5 = new BigDecimal(10000);
        BigDecimal bignum12_6 = bignum12_4.multiply(bignum12_5);
        cMngCheckBaseInfo.setLoanDtAvgBal12(String.valueOf(bignum12_6));
        personnelMapper.updateData(cMngCheckBaseInfo);
    }

    @Override
    public void insertSingleData(CMngCheckBaseInfo cMngCheckBaseInfo){
        cMngCheckBaseInfo.setOperateDate(DateUtils.format(new Date(),"yyyy-MM-dd"));

        String depositDtAvgBal1 = cMngCheckBaseInfo.getDepositDtAvgBal1();
        BigDecimal bignum1_1 = new BigDecimal(depositDtAvgBal1);
        BigDecimal bignum1_2 = new BigDecimal(10000);
        BigDecimal bignum1_3 = bignum1_1.multiply(bignum1_2);
        cMngCheckBaseInfo.setDepositDtAvgBal1(String.valueOf(bignum1_3));
        String depositDtAvgBal2 = cMngCheckBaseInfo.getDepositDtAvgBal2();
        BigDecimal bignum2_1 = new BigDecimal(depositDtAvgBal2);
        BigDecimal bignum2_2 = new BigDecimal(10000);
        BigDecimal bignum2_3 = bignum2_1.multiply(bignum2_2);
        cMngCheckBaseInfo.setDepositDtAvgBal2(String.valueOf(bignum2_3));
        String depositDtAvgBal3 = cMngCheckBaseInfo.getDepositDtAvgBal3();
        BigDecimal bignum3_1 = new BigDecimal(depositDtAvgBal3);
        BigDecimal bignum3_2 = new BigDecimal(10000);
        BigDecimal bignum3_3 = bignum3_1.multiply(bignum3_2);
        cMngCheckBaseInfo.setDepositDtAvgBal3(String.valueOf(bignum3_3));
        String depositDtAvgBal4 = cMngCheckBaseInfo.getDepositDtAvgBal4();
        BigDecimal bignum4_1 = new BigDecimal(depositDtAvgBal4);
        BigDecimal bignum4_2 = new BigDecimal(10000);
        BigDecimal bignum4_3 = bignum4_1.multiply(bignum4_2);
        cMngCheckBaseInfo.setDepositDtAvgBal4(String.valueOf(bignum4_3));
        String depositDtAvgBal5 = cMngCheckBaseInfo.getDepositDtAvgBal5();
        BigDecimal bignum5_1 = new BigDecimal(depositDtAvgBal5);
        BigDecimal bignum5_2 = new BigDecimal(10000);
        BigDecimal bignum5_3 = bignum5_1.multiply(bignum5_2);
        cMngCheckBaseInfo.setDepositDtAvgBal5(String.valueOf(bignum5_3));
        String depositDtAvgBal6 = cMngCheckBaseInfo.getDepositDtAvgBal6();
        BigDecimal bignum6_1 = new BigDecimal(depositDtAvgBal6);
        BigDecimal bignum6_2 = new BigDecimal(10000);
        BigDecimal bignum6_3 = bignum6_1.multiply(bignum6_2);
        cMngCheckBaseInfo.setDepositDtAvgBal6(String.valueOf(bignum6_3));
        String depositDtAvgBal7 = cMngCheckBaseInfo.getDepositDtAvgBal7();
        BigDecimal bignum7_1 = new BigDecimal(depositDtAvgBal7);
        BigDecimal bignum7_2 = new BigDecimal(10000);
        BigDecimal bignum7_3 = bignum7_1.multiply(bignum7_2);
        cMngCheckBaseInfo.setDepositDtAvgBal7(String.valueOf(bignum7_3));
        String depositDtAvgBal8 = cMngCheckBaseInfo.getDepositDtAvgBal8();
        BigDecimal bignum8_1 = new BigDecimal(depositDtAvgBal8);
        BigDecimal bignum8_2 = new BigDecimal(10000);
        BigDecimal bignum8_3 = bignum8_1.multiply(bignum8_2);
        cMngCheckBaseInfo.setDepositDtAvgBal8(String.valueOf(bignum8_3));
        String depositDtAvgBal9 = cMngCheckBaseInfo.getDepositDtAvgBal9();
        BigDecimal bignum9_1 = new BigDecimal(depositDtAvgBal9);
        BigDecimal bignum9_2 = new BigDecimal(10000);
        BigDecimal bignum9_3 = bignum9_1.multiply(bignum9_2);
        cMngCheckBaseInfo.setDepositDtAvgBal9(String.valueOf(bignum9_3));
        String depositDtAvgBal10 = cMngCheckBaseInfo.getDepositDtAvgBal10();
        BigDecimal bignum10_1 = new BigDecimal(depositDtAvgBal10);
        BigDecimal bignum10_2 = new BigDecimal(10000);
        BigDecimal bignum10_3 = bignum10_1.multiply(bignum10_2);
        cMngCheckBaseInfo.setDepositDtAvgBal10(String.valueOf(bignum10_3));
        String depositDtAvgBal11 = cMngCheckBaseInfo.getDepositDtAvgBal11();
        BigDecimal bignum11_1 = new BigDecimal(depositDtAvgBal11);
        BigDecimal bignum11_2 = new BigDecimal(10000);
        BigDecimal bignum11_3 = bignum11_1.multiply(bignum11_2);
        cMngCheckBaseInfo.setDepositDtAvgBal11(String.valueOf(bignum11_3));
        String depositDtAvgBal12 = cMngCheckBaseInfo.getDepositDtAvgBal12();
        BigDecimal bignum12_1 = new BigDecimal(depositDtAvgBal12);
        BigDecimal bignum12_2 = new BigDecimal(10000);
        BigDecimal bignum12_3 = bignum12_1.multiply(bignum12_2);
        cMngCheckBaseInfo.setDepositDtAvgBal12(String.valueOf(bignum12_3));
        String loanDtAvgBal1 = cMngCheckBaseInfo.getLoanDtAvgBal1();
        BigDecimal bignum1_4 = new BigDecimal(loanDtAvgBal1);
        BigDecimal bignum1_5 = new BigDecimal(10000);
        BigDecimal bignum1_6 = bignum1_4.multiply(bignum1_5);
        cMngCheckBaseInfo.setLoanDtAvgBal1(String.valueOf(bignum1_6));
        String loanDtAvgBal2 = cMngCheckBaseInfo.getLoanDtAvgBal2();
        BigDecimal bignum2_4 = new BigDecimal(loanDtAvgBal2);
        BigDecimal bignum2_5 = new BigDecimal(10000);
        BigDecimal bignum2_6 = bignum2_4.multiply(bignum2_5);
        cMngCheckBaseInfo.setLoanDtAvgBal2(String.valueOf(bignum2_6));
        String loanDtAvgBal3 = cMngCheckBaseInfo.getLoanDtAvgBal3();
        BigDecimal bignum3_4 = new BigDecimal(loanDtAvgBal3);
        BigDecimal bignum3_5 = new BigDecimal(10000);
        BigDecimal bignum3_6 = bignum3_4.multiply(bignum3_5);
        cMngCheckBaseInfo.setLoanDtAvgBal3(String.valueOf(bignum3_6));
        String loanDtAvgBal4 = cMngCheckBaseInfo.getLoanDtAvgBal4();
        BigDecimal bignum4_4 = new BigDecimal(loanDtAvgBal4);
        BigDecimal bignum4_5 = new BigDecimal(10000);
        BigDecimal bignum4_6 = bignum4_4.multiply(bignum4_5);
        cMngCheckBaseInfo.setLoanDtAvgBal4(String.valueOf(bignum4_6));
        String loanDtAvgBal5 = cMngCheckBaseInfo.getLoanDtAvgBal5();
        BigDecimal bignum5_4 = new BigDecimal(loanDtAvgBal5);
        BigDecimal bignum5_5 = new BigDecimal(10000);
        BigDecimal bignum5_6 = bignum5_4.multiply(bignum5_5);
        cMngCheckBaseInfo.setLoanDtAvgBal5(String.valueOf(bignum5_6));
        String loanDtAvgBal6 = cMngCheckBaseInfo.getLoanDtAvgBal6();
        BigDecimal bignum6_4 = new BigDecimal(loanDtAvgBal6);
        BigDecimal bignum6_5 = new BigDecimal(10000);
        BigDecimal bignum6_6 = bignum6_4.multiply(bignum6_5);
        cMngCheckBaseInfo.setLoanDtAvgBal6(String.valueOf(bignum6_6));
        String loanDtAvgBal7 = cMngCheckBaseInfo.getLoanDtAvgBal7();
        BigDecimal bignum7_4 = new BigDecimal(loanDtAvgBal7);
        BigDecimal bignum7_5 = new BigDecimal(10000);
        BigDecimal bignum7_6 = bignum7_4.multiply(bignum7_5);
        cMngCheckBaseInfo.setLoanDtAvgBal7(String.valueOf(bignum7_6));
        String loanDtAvgBal8 = cMngCheckBaseInfo.getLoanDtAvgBal8();
        BigDecimal bignum8_4 = new BigDecimal(loanDtAvgBal8);
        BigDecimal bignum8_5 = new BigDecimal(10000);
        BigDecimal bignum8_6 = bignum8_4.multiply(bignum8_5);
        cMngCheckBaseInfo.setLoanDtAvgBal8(String.valueOf(bignum8_6));
        String loanDtAvgBal9 = cMngCheckBaseInfo.getLoanDtAvgBal9();
        BigDecimal bignum9_4 = new BigDecimal(loanDtAvgBal9);
        BigDecimal bignum9_5 = new BigDecimal(10000);
        BigDecimal bignum9_6 = bignum9_4.multiply(bignum9_5);
        cMngCheckBaseInfo.setLoanDtAvgBal9(String.valueOf(bignum9_6));
        String loanDtAvgBal10 = cMngCheckBaseInfo.getLoanDtAvgBal10();
        BigDecimal bignum10_4 = new BigDecimal(loanDtAvgBal10);
        BigDecimal bignum10_5 = new BigDecimal(10000);
        BigDecimal bignum10_6 = bignum10_4.multiply(bignum10_5);
        cMngCheckBaseInfo.setLoanDtAvgBal10(String.valueOf(bignum10_6));
        String loanDtAvgBal11 = cMngCheckBaseInfo.getLoanDtAvgBal11();
        BigDecimal bignum11_4 = new BigDecimal(loanDtAvgBal11);
        BigDecimal bignum11_5 = new BigDecimal(10000);
        BigDecimal bignum11_6 = bignum11_4.multiply(bignum11_5);
        cMngCheckBaseInfo.setLoanDtAvgBal11(String.valueOf(bignum11_6));
        String loanDtAvgBal12 = cMngCheckBaseInfo.getLoanDtAvgBal12();
        BigDecimal bignum12_4 = new BigDecimal(loanDtAvgBal12);
        BigDecimal bignum12_5 = new BigDecimal(10000);
        BigDecimal bignum12_6 = bignum12_4.multiply(bignum12_5);
        cMngCheckBaseInfo.setLoanDtAvgBal12(String.valueOf(bignum12_6));
        personnelMapper.insertByYr(cMngCheckBaseInfo);
    }

    @Override
    public List<CMngCheckBaseInfo> queryAllChdetCode(){
        return personnelMapper.queryAllChdetCode();
    }

    @Override
    public List<String> queryAllEmp(){
        return personnelMapper.queryAllEmp();
    }

    @Override
    public List<Map> queryEmployeeView(Map reqPara) {
        return personnelMapper.queryEmployeeView(reqPara);
    }


    @Override
    public List<Map> queryTeamView(Map reqPara) {
        return personnelMapper.queryTeamView(reqPara);
    }

    @Override
    public List<Map> queryTeam(Map reqPara) {
        return personnelMapper.queryTeam(reqPara);
    }

    @Override
    public List<CMngCheckBaseInfo> downLoadTemplate(Map<String, Object> params) {
        List<CMngCheckBaseInfo> downLoadList = new ArrayList<>();
        downLoadList = personnelMapper.downLoadTemplate(params);
        return downLoadList;
    }

    @Override
    public int queryCountMng(Map<String, Object> params) {
        int count = personnelMapper.queryCountMng(params);
        return count;
    }

    @Override
    public List<CMngCheckBaseInfo> queryBitchHisByLimit(Map<String, Object> params) {
        int pageNo = Integer.parseInt((String) params.get("page"));
        int pageCount = Integer.parseInt((String)params.get("limit"));
        int offset = ((pageNo - 1) * pageCount);
        params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
        params.put("offset", offset);
        List<CMngCheckBaseInfo> cMngCheckBaseInfos = personnelMapper.queryBitchHisByLimit(params);
        return cMngCheckBaseInfos;
    }

    @Override
    public Integer queryBitchHisByLimitNb(Map<String, Object> params) {
        Integer integer = personnelMapper.queryBitchHisByLimitNb(params);
        return integer;
    }

    @Override
    public List<Map> queryEmployeeViewLimit(Map reqPara){
        int pageNo = Integer.parseInt((String) reqPara.get("droplistPage"));
        int pageCount = Integer.parseInt((String)reqPara.get("droplistLimit"));
        int offset = ((pageNo - 1) * pageCount);
        reqPara.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
        reqPara.put("offset", offset);
        List<Map> maps = personnelMapper.queryEmployeeViewLimit(reqPara);
        return maps;
    }

    @Override
    public List<Map> queryTeamRep(Map reqPara) {
        return personnelMapper.queryTeamRep(reqPara);
    }

    @Override
    public List<Map> queryTeamViewRep(Map reqPara) {
        return personnelMapper.queryTeamViewRep(reqPara);
    }

    @Override
    public List<Map> queryEmployeeViewRep(Map reqPara) {
        List<Map> maps = personnelMapper.queryEmployeeViewRep(reqPara);
        return maps;
    }
}
