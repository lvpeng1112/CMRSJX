package com.beyondsoft.modules.aofp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.common.utils.DateUtils;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.common.utils.ShiroUtils;
import com.beyondsoft.datasource.annotation.DataSource;
import com.beyondsoft.modules.aofp.entity.ExceptListMgr;
import com.beyondsoft.modules.aofp.dao.ExceptListMgrDao;
import com.beyondsoft.modules.aofp.service.ExceptListMgrService;
import com.beyondsoft.modules.aofp.utils.Utils;
import com.beyondsoft.modules.sys.dao.DepartmentImpDao;
import com.beyondsoft.modules.sys.dao.DeptMgrDao;
import com.beyondsoft.modules.sys.entity.DeptMgrEntity;
import com.beyondsoft.utils.DateUtil;
import com.beyondsoft.utils.StrUtil;
import com.microsoft.schemas.office.visio.x2012.main.CellType;
import net.bytebuddy.asm.Advice;
import net.bytebuddy.implementation.bytecode.Throw;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.*;

@Service
public class ExceptListMgrServiceImpl extends ServiceImpl<ExceptListMgrDao, ExceptListMgr> implements ExceptListMgrService {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    ExceptListMgrDao exceptListMgrDao;
    @Resource
    DepartmentImpDao departmentImpDao;
    @Override
    @DataSource(value = "second")
    public List<ExceptListMgr> queryExceptListInfo(Map<String, Object> params){
        int pageNo = Integer.parseInt((String) params.get("page"));
        int pageCount = Integer.parseInt((String)params.get("limit"));
        int offset = ((pageNo - 1) * pageCount);
        params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
        params.put("offset", offset);
        return exceptListMgrDao.queryExceptListInfo(params);
    }

    @Override
    @DataSource(value = "second")
    public int queryExceptListNb(Map<String, Object> params){
        int nb = exceptListMgrDao.queryExceptListNb(params);
        return nb;
    }

    @Override
    @DataSource(value = "second")
    public R batchImport(String fileName, MultipartFile file, String org, String isKhy,String yrN) {
        Map map = new HashMap();
        Workbook workbook = null;
        if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
            return R.error("上传格式不正确");
        }
        String extString = fileName.substring(fileName.lastIndexOf("."));

        List<ExceptListMgr> ExceptList = new ArrayList<>();
        try {
            InputStream inputStream = file.getInputStream();
            if (".xls".equals(extString)) {
                HSSFWorkbook wb = new HSSFWorkbook(inputStream);
                workbook = wb;
            } else if (".xlsx".equals(extString)) {
                XSSFWorkbook wb = new XSSFWorkbook(inputStream);
                workbook = wb;
            }
            int sheets = workbook.getNumberOfSheets();

            List<DeptMgrEntity> departments = departmentImpDao.getDepartments();
            for (int i = 0; i < sheets; i++) {
                Sheet sheetAt = workbook.getSheetAt(i);
                //获取多少行
                int lastRowNum = sheetAt.getLastRowNum();
                for (int j = 1; j <= lastRowNum; j++) {
                    Row row = sheetAt.getRow(j);
                    if (row != null) {
                        ExceptListMgr exceptListMgr = new ExceptListMgr();
                        for (Cell cell : row) {
                            cell.setCellType(Cell.CELL_TYPE_STRING);
                        }
                        // 必须输入项check
                        if (row.getCell(0) == null || row.getCell(0).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，年度列没有数据");
                        }else if (row.getCell(1) == null || row.getCell(1).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，村行号列没有数据");
                        }else if (row.getCell(3) == null || row.getCell(3).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，客户号列没有数据");
                        }else if (row.getCell(4) == null || row.getCell(4).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，客户名称没有数据");
                        }
                        // 年度日期格式check
                        String yr =  row.getCell(0).getStringCellValue();
                        String inputStr = yr + "0101";
                        if (!DateUtil.isDate(inputStr)) {
                            return R.error("第" + (j + 1) + "行,年度【" + yr + "】不是正确的日期格式！例:YYYY");
                        }
                        // 年度校验
                        if(!yrN.equals(yr)){
                            return R.error("第" + (j + 1) + "行,请与导入时选择的年度参数保持一致");
                        }

                        // 村行号存在check
                        String orgId = row.getCell(1).getStringCellValue();
                        boolean flag=false;
                        for (DeptMgrEntity d:departments){
                            String chdeptcode = d.getChdeptcode();
                            if(chdeptcode.equals(orgId)){
                                flag=true;
                            }
                        }
                        if(!flag){
                            return R.error("第" + (j + 1) + "行,机构编号【" + orgId + "】不存在！");
                        }
                        // 村行机构权限判断
                        Map<String, Object> orgParams = new HashMap<>();
                        orgParams.put("orgId", orgId);
                        orgParams.put("org", org);
                        orgParams.put("isKhy", isKhy);
                        int count = exceptListMgrDao.checkOrgId(orgParams);
                        if (count == 0) {

                            return R.error("第" + (j + 1) + "行,您没有权限导入其他机构的数据，机构编号【" + orgId + "】！");
                        }
                        String orgNm = exceptListMgrDao.queryOrgNm(orgParams);

                        // 客户号长度check
                        String custId = row.getCell(3).getStringCellValue();
                        if (custId.length() > 90) {
                            return R.error("第" + (j + 1) + "行,客户号【" + custId + "】长度不能超过90位！");
                        }
                        // 客户名称长度check
                        String custNm = row.getCell(4).getStringCellValue();
                        if (StrUtil.chineseLen(custNm) > 300) {
                            return R.error("第" + (j + 1) + "行,客户名称【" + custNm + "】长度不能超过300位！");
                        }

                        //执行插入方法
                        exceptListMgr.setYr(yr);
                        exceptListMgr.setBranchId(orgId);
                        exceptListMgr.setBranchNm(orgNm);
                        exceptListMgr.setCustId(custId);
                        exceptListMgr.setCustNm(custNm);
                        exceptListMgr.setOperators(ShiroUtils.getUserEntity().getUsername());
                        ExceptList.add(exceptListMgr);
                        exceptListMgrDao.deleteExceptList(exceptListMgr);
                    }
                }
            }
            if(ExceptList.size() > 0){
                exceptListMgrDao.insertExceptList(ExceptList);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                workbook.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return R.ok("导入成功");
    }
}
