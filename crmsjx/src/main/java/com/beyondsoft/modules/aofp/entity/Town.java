package com.beyondsoft.modules.aofp.entity;

import lombok.Data;

@Data
public class Town {
    private String chdeptcode;
    private String chdeptname;
}
