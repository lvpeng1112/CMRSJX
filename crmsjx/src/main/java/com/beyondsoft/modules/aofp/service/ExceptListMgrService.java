package com.beyondsoft.modules.aofp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.aofp.entity.ExceptListMgr;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;


public interface ExceptListMgrService  extends IService<ExceptListMgr> {
      List<ExceptListMgr> queryExceptListInfo(Map<String, Object> params);

      int queryExceptListNb(Map<String, Object> params);

      R batchImport(String fileName, MultipartFile file, String org, String isKhy,String yrN) throws Exception;
}
