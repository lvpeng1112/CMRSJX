package com.beyondsoft.modules.aofp.controller;

import com.alibaba.druid.util.StringUtils;
import com.beyondsoft.common.utils.DateUtils;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.common.utils.ShiroUtils;
import com.beyondsoft.modules.aofp.dao.ExceptListMgrDao;
import com.beyondsoft.modules.aofp.entity.ExceptListMgr;
import com.beyondsoft.modules.aofp.service.ExceptListMgrService;
import com.beyondsoft.modules.sys.entity.SysUserEntity;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;

@RestController
@RequestMapping("/aofp/exceptListMgr")
public class ExceptListMgrController {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    ExceptListMgrService exceptListMgrService;
    @Autowired
    ExceptListMgrDao exceptListMgrDao;

    @RequestMapping("/list")
    @ApiOperation("分页查询")
    public R list(@RequestParam Map<String, Object> params) {
        Map map = new HashMap();
        if(null == params){
            return R.error("入参为空");
        }
        if(StringUtils.isEmpty((String) params.get("page")) && StringUtils.isEmpty((String) params.get("limit"))){
            return R.error("缺少必传分页参数");
        }
        List<ExceptListMgr> exceptList = exceptListMgrService.queryExceptListInfo(params);
        int i = exceptListMgrService.queryExceptListNb(params);
        return R.ok().put("exceptList",exceptList).put("count",i);
    }

    @RequestMapping("/importExcel")
    @ApiOperation("导入excel")
    public R importExcel(@RequestParam(value = "file") MultipartFile file,String org, String isKhy,String yr) throws Exception {

        String fileName = file.getOriginalFilename();
        logger.info("导入表格文件为:{}",fileName);

        return exceptListMgrService.batchImport(fileName, file, org, isKhy,yr);
    }
}