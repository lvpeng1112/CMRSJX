package com.beyondsoft.modules.aofp.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Utils {
    public static String excelFormatData(String day){
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date format = null;
            format = dateFormat.parse("1899-12-30");
            String format1 = dateFormat.format(format);
            Calendar cal = Calendar.getInstance();
            cal.setTime(format);
            cal.add(Calendar.DATE, Integer.parseInt(day));
           return dateFormat.format(cal.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    };
}
