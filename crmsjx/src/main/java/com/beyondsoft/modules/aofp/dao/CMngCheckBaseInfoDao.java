package com.beyondsoft.modules.aofp.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.aofp.entity.CMngCheckBaseInfo;
import com.beyondsoft.modules.aofp.entity.CMngCheckBaseInfoHis;
import com.beyondsoft.modules.aofp.entity.Town;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository("CMngCheckBaseInfoDao")
public interface CMngCheckBaseInfoDao extends BaseMapper<CMngCheckBaseInfo> {
    /**
     * 查询村镇银行编号和名称
     * @return  List<Town>
     */
    @Select("select b.chdeptcode chdeptcode ,b.chdeptname  chdeptname from NPMM.C_MNG_CHECK_BASE_INFO a inner JOIN npmm.DEPARTMENT b ON a.ORG_ID = b.CHDEPTCODE\n" +
            "GROUP  BY  b.chdeptcode  ,b.chdeptname ")
    List<Town> queryChadDress();

    /**
     * 查询所有的村镇银行编号
     * @return
     */
    @Select("select chdeptcode, chdeptname  from npmm.DEPARTMENT")
    List<Town> queryAllChadDress();
    List<CMngCheckBaseInfo> queryByYr(CMngCheckBaseInfo cMngCheckBaseInfo);

    List<CMngCheckBaseInfo> queryAllChdetCode();

    List<String> queryAllEmp();

    void updateData(CMngCheckBaseInfo cMngCheckBaseInfo);

    void insertByYr (CMngCheckBaseInfo cMngCheckBaseInfoList);

    void deleteByYr(CMngCheckBaseInfo cMngCheckBaseInfo);

    List<CMngCheckBaseInfo> queryByLimit (Map<String,Object> reqPara);

    List<Map> queryEmployeeView (Map reqPara);

    List<Map> queryTeamView (Map reqPara);

    List<Map> queryTeam (Map reqPara);

    int queryDataListNb(Map reqPara);

    void insertByYrHis (CMngCheckBaseInfoHis cMngCheckBaseInfoList);

    List<CMngCheckBaseInfoHis> querytHisData(Map<String, Object> params);

    int querytHisDataNb(Map<String, Object> params);

    public List<Map<String, Object>> queryDictDetail(Map map);

    List<Map> queryEmployeeViewHis ();

    List<Map> queryTeamViewHis ();

    List<CMngCheckBaseInfo> downLoadTemplate(Map<String, Object> params);
    // 查询机构名称
    int queryOrgNm(Map<String, Object> params);
    int queryCountMng(Map<String, Object> params);

    List<CMngCheckBaseInfo> queryDataByOrgIdEmpIdTeamCode(Map<String, Object> params);
    int queryDataByOrgIdEmpIdTeamCodeCount(Map<String, Object> params);

    List<CMngCheckBaseInfo> queryBitchHisByLimit(Map<String, Object> params);

    Integer queryBitchHisByLimitNb(Map<String, Object> params);

    List<Map> queryEmployeeViewLimit(Map reqPara);

    List<Map> queryTeamRep(Map reqPara);

    List<Map> queryTeamViewRep(Map reqPara);

    List<Map> queryEmployeeViewRep(Map reqPara);

    int queryTeamLeaBycusId(CMngCheckBaseInfo cMngCheckBaseInfo);

    int queryTeamEmpBycusId(CMngCheckBaseInfo cMngCheckBaseInfo);
}
