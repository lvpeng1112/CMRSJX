package com.beyondsoft.modules.aofp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.aofp.entity.CMngCheckBaseInfo;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;


public interface AssessmentMarketingPersonnelService  extends IService<CMngCheckBaseInfo> {

      R batchImport(String fileName, MultipartFile file, String org, String isKhy) throws Exception;

      List<CMngCheckBaseInfo> queryByLimit(Map<String, Object> params);

      int queryDataListNb(Map<String, Object> params);

      void deleteData(CMngCheckBaseInfo cMngCheckBaseInfo);

      void updateData(CMngCheckBaseInfo cMngCheckBaseInfo);

      void insertSingleData(CMngCheckBaseInfo cMngCheckBaseInfo);

      List<CMngCheckBaseInfo> queryAllChdetCode();

      List<String> queryAllEmp();

      List<Map> queryEmployeeView (Map reqPara);

      List<Map> queryTeamView (Map reqPara);

      List<Map> queryTeam (Map reqPara);

      List<CMngCheckBaseInfo>  downLoadTemplate(Map<String, Object> params);
      int queryCountMng(Map<String, Object> params);

      List<CMngCheckBaseInfo> queryBitchHisByLimit(Map<String, Object> params);

      Integer queryBitchHisByLimitNb(Map<String, Object> params);

      List<Map> queryEmployeeViewLimit(Map reqPara);

      List<Map> queryTeamRep(Map reqPara);

      List<Map> queryTeamViewRep(Map reqPara);

      List<Map> queryEmployeeViewRep(Map reqPara);
}
