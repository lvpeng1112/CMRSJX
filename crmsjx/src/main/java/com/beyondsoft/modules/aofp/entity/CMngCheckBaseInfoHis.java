package com.beyondsoft.modules.aofp.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@ApiModel(value = "营销人员考核")
@Data
public class CMngCheckBaseInfoHis implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "年度")
    private String yr;

    @ApiModelProperty(value = "村镇银行名字")
    private String chdeptName;

    @ApiModelProperty(value = "机构号")
    private String orgId;

    @ApiModelProperty(value = "省份")
    private String  chdeptarea;

    @ApiModelProperty(value = "客户经理号")
    private String custMngrId;

    @ApiModelProperty(value = "客户经理名称")
    private String custMngrNm;

    @ApiModelProperty(value = "团队负责人编号")
    private String teamId;

    @ApiModelProperty(value = "团队负责人名称")
    private String teamNm;

    @ApiModelProperty(value = "团队负责人")
    private String team;

    @ApiModelProperty(value = "入职时间")
    private String inTm;

    @ApiModelProperty(value = "存款日均余额1月——调整前")
    private String depositDtAvgBal1_Befo;

    @ApiModelProperty(value = "存款日均余额2月——调整前")
    private String depositDtAvgBal2_Befo;

    @ApiModelProperty(value = "存款日均余额3月——调整前")
    private String depositDtAvgBal3_Befo;

    @ApiModelProperty(value = "存款日均余额4月——调整前")
    private String depositDtAvgBal4_Befo;

    @ApiModelProperty(value = "存款日均余额5月——调整前")
    private String depositDtAvgBal5_Befo;

    @ApiModelProperty(value = "存款日均余额6月——调整前")
    private String depositDtAvgBal6_Befo;

    @ApiModelProperty(value = "存款日均余额7月——调整前")
    private String depositDtAvgBal7_Befo;

    @ApiModelProperty(value = "存款日均余额8月——调整前")
    private String depositDtAvgBal8_Befo;

    @ApiModelProperty(value = "存款日均余额9月——调整前")
    private String depositDtAvgBal9_Befo;

    @ApiModelProperty(value = "存款日均余额10月——调整前")
    private String depositDtAvgBal10_Befo;

    @ApiModelProperty(value = "存款日均余额11月——调整前")
    private String depositDtAvgBal11_Befo;

    @ApiModelProperty(value = "存款日均余额12月——调整前")
    private String depositDtAvgBal12_Befo;

    @ApiModelProperty(value = "贷款日均余额1月——调整前")
    private String loanDtAvgBal1_Befo;

    @ApiModelProperty(value = "贷款日均余额2月——调整前")
    private String loanDtAvgBal2_Befo;

    @ApiModelProperty(value = "贷款日均余额3月——调整前")
    private String loanDtAvgBal3_Befo;

    @ApiModelProperty(value = "贷款日均余额4月——调整前")
    private String loanDtAvgBal4_Befo;

    @ApiModelProperty(value = "贷款日均余额5月——调整前")
    private String loanDtAvgBal5_Befo;

    @ApiModelProperty(value = "贷款日均余额6月——调整前")
    private String loanDtAvgBal6_Befo;

    @ApiModelProperty(value = "贷款日均余额7月——调整前")
    private String loanDtAvgBal7_Befo;

    @ApiModelProperty(value = "贷款日均余额8月——调整前")
    private String loanDtAvgBal8_Befo;

    @ApiModelProperty(value = "贷款日均余额9月——调整前")
    private String loanDtAvgBal9_Befo;

    @ApiModelProperty(value = "贷款日均余额10月——调整前")
    private String loanDtAvgBal10_Befo;

    @ApiModelProperty(value = "贷款日均余额11月——调整前")
    private String loanDtAvgBal11_Befo;

    @ApiModelProperty(value = "贷款日均余额12月——调整前")
    private String loanDtAvgBal12_Befo;

    @ApiModelProperty(value = "FTP净收入1月——调整前")
    private String netIncm1_Befo;

    @ApiModelProperty(value = "FTP净收入2月——调整前")
    private String netIncm2_Befo;

    @ApiModelProperty(value = "FTP净收入3月——调整前")
    private String netIncm3_Befo;

    @ApiModelProperty(value = "FTP净收入4月——调整前")
    private String netIncm4_Befo;

    @ApiModelProperty(value = "FTP净收入5月——调整前")
    private String netIncm5_Befo;

    @ApiModelProperty(value = "FTP净收入6月——调整前")
    private String netIncm6_Befo;

    @ApiModelProperty(value = "FTP净收入7月——调整前")
    private String netIncm7_Befo;

    @ApiModelProperty(value = "FTP净收入8月——调整前")
    private String netIncm8_Befo;

    @ApiModelProperty(value = "FTP净收入9月——调整前")
    private String netIncm9_Befo;

    @ApiModelProperty(value = "FTP净收入10月——调整前")
    private String netIncm10_Befo;

    @ApiModelProperty(value = "FTP净收入11月——调整前")
    private String netIncm11_Befo;

    @ApiModelProperty(value = "FTP净收入12月——调整前")
    private String netIncm12_Befo;

    @ApiModelProperty(value = "存款新开户数1月——调整前")
    private String depositNewOpenNum1_Befo;

    @ApiModelProperty(value = "存款新开户数2月——调整前")
    private String depositNewOpenNum2_Befo;

    @ApiModelProperty(value = "存款新开户数3月——调整前")
    private String depositNewOpenNum3_Befo;

    @ApiModelProperty(value = "存款新开户数4月——调整前")
    private String depositNewOpenNum4_Befo;

    @ApiModelProperty(value = "存款新开户数5月——调整前")
    private String depositNewOpenNum5_Befo;

    @ApiModelProperty(value = "存款新开户数6月——调整前")
    private String depositNewOpenNum6_Befo;

    @ApiModelProperty(value = "存款新开户数7月——调整前")
    private String depositNewOpenNum7_Befo;

    @ApiModelProperty(value = "存款新开户数8月——调整前")
    private String depositNewOpenNum8_Befo;

    @ApiModelProperty(value = "存款新开户数9月——调整前")
    private String depositNewOpenNum9_Befo;

    @ApiModelProperty(value = "存款新开户数10月——调整前")
    private String depositNewOpenNum10_Befo;

    @ApiModelProperty(value = "存款新开户数11月——调整前")
    private String depositNewOpenNum11_Befo;

    @ApiModelProperty(value = "存款新开户数12月——调整前")
    private String depositNewOpenNum12_Befo;

    @ApiModelProperty(value = "贷款管户户数1月——调整前")
    private String loanMngCustCustNum1_Befo;

    @ApiModelProperty(value = "贷款管户户数2月——调整前")
    private String loanMngCustCustNum2_Befo;

    @ApiModelProperty(value = "贷款管户户数3月——调整前")
    private String loanMngCustCustNum3_Befo;

    @ApiModelProperty(value = "贷款管户户数4月——调整前")
    private String loanMngCustCustNum4_Befo;

    @ApiModelProperty(value = "贷款管户户数5月——调整前")
    private String loanMngCustCustNum5_Befo;

    @ApiModelProperty(value = "贷款管户户数6月——调整前")
    private String loanMngCustCustNum6_Befo;

    @ApiModelProperty(value = "贷款管户户数7月——调整前")
    private String loanMngCustCustNum7_Befo;

    @ApiModelProperty(value = "贷款管户户数8月——调整前")
    private String loanMngCustCustNum8_Befo;

    @ApiModelProperty(value = "贷款管户户数9月——调整前")
    private String loanMngCustCustNum9_Befo;

    @ApiModelProperty(value = "贷款管户户数10月——调整前")
    private String loanMngCustCustNum10_Befo;

    @ApiModelProperty(value = "贷款管户户数11月——调整前")
    private String loanMngCustCustNum11_Befo;

    @ApiModelProperty(value = "贷款管户户数12月——调整前")
    private String loanMngCustCustNum12_Befo;

    @ApiModelProperty(value = "FTP净收入基数1月——调整前")
    private String netIncmBase1_Befo;

    @ApiModelProperty(value = "FTP净收入基数2月——调整前")
    private String netIncmBase2_Befo;

    @ApiModelProperty(value = "FTP净收入基数3月——调整前")
    private String netIncmBase3_Befo;

    @ApiModelProperty(value = "FTP净收入基数4月——调整前")
    private String netIncmBase4_Befo;

    @ApiModelProperty(value = "FTP净收入基数5月——调整前")
    private String netIncmBase5_Befo;

    @ApiModelProperty(value = "FTP净收入基数6月——调整前")
    private String netIncmBase6_Befo;

    @ApiModelProperty(value = "FTP净收入基数7月——调整前")
    private String netIncmBase7_Befo;

    @ApiModelProperty(value = "FTP净收入基数8月——调整前")
    private String netIncmBase8_Befo;

    @ApiModelProperty(value = "FTP净收入基数9月——调整前")
    private String netIncmBase9_Befo;

    @ApiModelProperty(value = "FTP净收入基数10月——调整前")
    private String netIncmBase10_Befo;

    @ApiModelProperty(value = "FTP净收入基数11月——调整前")
    private String netIncmBase11_Befo;

    @ApiModelProperty(value = "FTP净收入基数12月——调整前")
    private String netIncmBase12_Befo;

    @ApiModelProperty(value = "存款日均余额1月——调整后")
    private String depositDtAvgBal1_After;

    @ApiModelProperty(value = "存款日均余额2月——调整后")
    private String depositDtAvgBal2_After;

    @ApiModelProperty(value = "存款日均余额3月——调整后")
    private String depositDtAvgBal3_After;

    @ApiModelProperty(value = "存款日均余额4月——调整后")
    private String depositDtAvgBal4_After;

    @ApiModelProperty(value = "存款日均余额5月——调整后")
    private String depositDtAvgBal5_After;

    @ApiModelProperty(value = "存款日均余额6月——调整后")
    private String depositDtAvgBal6_After;

    @ApiModelProperty(value = "存款日均余额7月——调整后")
    private String depositDtAvgBal7_After;

    @ApiModelProperty(value = "存款日均余额8月——调整后")
    private String depositDtAvgBal8_After;

    @ApiModelProperty(value = "存款日均余额9月——调整后")
    private String depositDtAvgBal9_After;

    @ApiModelProperty(value = "存款日均余额10月——调整后")
    private String depositDtAvgBal10_After;

    @ApiModelProperty(value = "存款日均余额11月——调整后")
    private String depositDtAvgBal11_After;

    @ApiModelProperty(value = "存款日均余额12月——调整后")
    private String depositDtAvgBal12_After;

    @ApiModelProperty(value = "贷款日均余额1月——调整后")
    private String loanDtAvgBal1_After;

    @ApiModelProperty(value = "贷款日均余额2月——调整后")
    private String loanDtAvgBal2_After;

    @ApiModelProperty(value = "贷款日均余额3月——调整后")
    private String loanDtAvgBal3_After;

    @ApiModelProperty(value = "贷款日均余额4月——调整后")
    private String loanDtAvgBal4_After;

    @ApiModelProperty(value = "贷款日均余额5月——调整后")
    private String loanDtAvgBal5_After;

    @ApiModelProperty(value = "贷款日均余额6月——调整后")
    private String loanDtAvgBal6_After;

    @ApiModelProperty(value = "贷款日均余额7月——调整后")
    private String loanDtAvgBal7_After;

    @ApiModelProperty(value = "贷款日均余额8月——调整后")
    private String loanDtAvgBal8_After;

    @ApiModelProperty(value = "贷款日均余额9月——调整后")
    private String loanDtAvgBal9_After;

    @ApiModelProperty(value = "贷款日均余额10月——调整后")
    private String loanDtAvgBal10_After;

    @ApiModelProperty(value = "贷款日均余额11月——调整后")
    private String loanDtAvgBal11_After;

    @ApiModelProperty(value = "贷款日均余额12月——调整后")
    private String loanDtAvgBal12_After;

    @ApiModelProperty(value = "FTP净收入1月——调整后")
    private String netIncm1_After;

    @ApiModelProperty(value = "FTP净收入2月——调整后")
    private String netIncm2_After;

    @ApiModelProperty(value = "FTP净收入3月——调整后")
    private String netIncm3_After;

    @ApiModelProperty(value = "FTP净收入4月——调整后")
    private String netIncm4_After;

    @ApiModelProperty(value = "FTP净收入5月——调整后")
    private String netIncm5_After;

    @ApiModelProperty(value = "FTP净收入6月——调整后")
    private String netIncm6_After;

    @ApiModelProperty(value = "FTP净收入7月——调整后")
    private String netIncm7_After;

    @ApiModelProperty(value = "FTP净收入8月——调整后")
    private String netIncm8_After;

    @ApiModelProperty(value = "FTP净收入9月——调整后")
    private String netIncm9_After;

    @ApiModelProperty(value = "FTP净收入10月——调整后")
    private String netIncm10_After;

    @ApiModelProperty(value = "FTP净收入11月——调整后")
    private String netIncm11_After;

    @ApiModelProperty(value = "FTP净收入12月——调整后")
    private String netIncm12_After;

    @ApiModelProperty(value = "存款新开户数1月——调整后")
    private String depositNewOpenNum1_After;

    @ApiModelProperty(value = "存款新开户数2月——调整后")
    private String depositNewOpenNum2_After;

    @ApiModelProperty(value = "存款新开户数3月——调整后")
    private String depositNewOpenNum3_After;

    @ApiModelProperty(value = "存款新开户数4月——调整后")
    private String depositNewOpenNum4_After;

    @ApiModelProperty(value = "存款新开户数5月——调整后")
    private String depositNewOpenNum5_After;

    @ApiModelProperty(value = "存款新开户数6月——调整后")
    private String depositNewOpenNum6_After;

    @ApiModelProperty(value = "存款新开户数7月——调整后")
    private String depositNewOpenNum7_After;

    @ApiModelProperty(value = "存款新开户数8月——调整后")
    private String depositNewOpenNum8_After;

    @ApiModelProperty(value = "存款新开户数9月——调整后")
    private String depositNewOpenNum9_After;

    @ApiModelProperty(value = "存款新开户数10月——调整后")
    private String depositNewOpenNum10_After;

    @ApiModelProperty(value = "存款新开户数11月——调整后")
    private String depositNewOpenNum11_After;

    @ApiModelProperty(value = "存款新开户数12月——调整后")
    private String depositNewOpenNum12_After;

    @ApiModelProperty(value = "贷款管户户数1月——调整后")
    private String loanMngCustCustNum1_After;

    @ApiModelProperty(value = "贷款管户户数2月——调整后")
    private String loanMngCustCustNum2_After;

    @ApiModelProperty(value = "贷款管户户数3月——调整后")
    private String loanMngCustCustNum3_After;

    @ApiModelProperty(value = "贷款管户户数4月——调整后")
    private String loanMngCustCustNum4_After;

    @ApiModelProperty(value = "贷款管户户数5月——调整后")
    private String loanMngCustCustNum5_After;

    @ApiModelProperty(value = "贷款管户户数6月——调整后")
    private String loanMngCustCustNum6_After;

    @ApiModelProperty(value = "贷款管户户数7月——调整后")
    private String loanMngCustCustNum7_After;

    @ApiModelProperty(value = "贷款管户户数8月——调整后")
    private String loanMngCustCustNum8_After;

    @ApiModelProperty(value = "贷款管户户数9月——调整后")
    private String loanMngCustCustNum9_After;

    @ApiModelProperty(value = "贷款管户户数10月——调整后")
    private String loanMngCustCustNum10_After;

    @ApiModelProperty(value = "贷款管户户数11月——调整后")
    private String loanMngCustCustNum11_After;

    @ApiModelProperty(value = "贷款管户户数12月——调整后")
    private String loanMngCustCustNum12_After;

    @ApiModelProperty(value = "FTP净收入基数1月——调整后")
    private String netIncmBase1_After;

    @ApiModelProperty(value = "FTP净收入基数2月——调整后")
    private String netIncmBase2_After;

    @ApiModelProperty(value = "FTP净收入基数3月——调整后")
    private String netIncmBase3_After;

    @ApiModelProperty(value = "FTP净收入基数4月——调整后")
    private String netIncmBase4_After;

    @ApiModelProperty(value = "FTP净收入基数5月——调整后")
    private String netIncmBase5_After;

    @ApiModelProperty(value = "FTP净收入基数6月——调整后")
    private String netIncmBase6_After;

    @ApiModelProperty(value = "FTP净收入基数7月——调整后")
    private String netIncmBase7_After;

    @ApiModelProperty(value = "FTP净收入基数8月——调整后")
    private String netIncmBase8_After;

    @ApiModelProperty(value = "FTP净收入基数9月——调整后")
    private String netIncmBase9_After;

    @ApiModelProperty(value = "FTP净收入基数10月——调整后")
    private String netIncmBase10_After;

    @ApiModelProperty(value = "FTP净收入基数11月——调整后")
    private String netIncmBase11_After;

    @ApiModelProperty(value = "FTP净收入基数12月——调整后")
    private String netIncmBase12_After;

    @ApiModelProperty(value = "操作日期")
    private String operateDate;

    @ApiModelProperty(value = "操作员")
    private String operator;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "修改标识")
    private String logo;

    @TableField(exist=false)
    @ApiModelProperty(value = "月份")
    private String setupDt;
}
