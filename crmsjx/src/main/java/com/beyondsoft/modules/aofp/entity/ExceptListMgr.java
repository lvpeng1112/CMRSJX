package com.beyondsoft.modules.aofp.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
@ApiModel(value = "例外名单维护")
@TableName("C_OUT_NM_BILL_MAINTAIN")
@Data
public class ExceptListMgr implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "年度")
    private String yr;

    @ApiModelProperty(value = "村行号")
    private String branchId;

    @ApiModelProperty(value = "村行名称")
    private String branchNm;

    @ApiModelProperty(value = "客户号")
    private String custId;

    @ApiModelProperty(value = "客户名称")
    private String custNm;

    @ApiModelProperty(value = "操作员")
    private String operators;

    @ApiModelProperty(value = "操作时间")
    private String operateDate;

}
