package com.beyondsoft.modules.aofp.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
@ApiModel(value = "营销人员考核")
@TableName("C_MNG_CHECK_BASE_INFO")
@Data
public class CMngCheckBaseInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    @TableField(exist=false)
    @ApiModelProperty(value = "序号")
    private String id;

    @ApiModelProperty(value = "年度")
    private String yr;

    @TableField(exist=false)
    @ApiModelProperty(value = "村镇银行名字")
    private String chdeptName;

    @TableField(exist=false)
    @ApiModelProperty(value = "月份")
    private String setupDt;

    @ApiModelProperty(value = "机构号")
    private String orgId;

    @TableField(exist=false)
    @ApiModelProperty(value = "省份")
    private String  chdeptarea;

    @ApiModelProperty(value = "客户经理号")
        private String custMngrId;

    @ApiModelProperty(value = "客户经理名称")
    private String custMngrNm;

    @TableField(exist=false)
    @ApiModelProperty(value = "团队负责人编号")
    private String teamId;

    @TableField(exist=false)
    @ApiModelProperty(value = "团队负责人名称")
    private String teamNm;

    @TableField(exist=false)
    @ApiModelProperty(value = "团队名称")
    private String team;

    @ApiModelProperty(value = "入职时间")
    private String inTm;

    @ApiModelProperty(value = "存款日均余额1月")
    private String depositDtAvgBal1;

    @ApiModelProperty(value = "存款日均余额2月")
    private String depositDtAvgBal2;

    @ApiModelProperty(value = "存款日均余额3月")
    private String depositDtAvgBal3;

    @ApiModelProperty(value = "存款日均余额4月")
    private String depositDtAvgBal4;

    @ApiModelProperty(value = "存款日均余额5月")
    private String depositDtAvgBal5;

    @ApiModelProperty(value = "存款日均余额6月")
    private String depositDtAvgBal6;

    @ApiModelProperty(value = "存款日均余额7月")
    private String depositDtAvgBal7;

    @ApiModelProperty(value = "存款日均余额8月")
    private String depositDtAvgBal8;

    @ApiModelProperty(value = "存款日均余额9月")
    private String depositDtAvgBal9;

    @ApiModelProperty(value = "存款日均余额10月")
    private String depositDtAvgBal10;

    @ApiModelProperty(value = "存款日均余额11月")
    private String depositDtAvgBal11;

    @ApiModelProperty(value = "存款日均余额12月")
    private String depositDtAvgBal12;

    @ApiModelProperty(value = "贷款日均余额1月")
    private String loanDtAvgBal1;

    @ApiModelProperty(value = "贷款日均余额2月")
    private String loanDtAvgBal2;

    @ApiModelProperty(value = "贷款日均余额3月")
    private String loanDtAvgBal3;

    @ApiModelProperty(value = "贷款日均余额4月")
    private String loanDtAvgBal4;

    @ApiModelProperty(value = "贷款日均余额5月")
    private String loanDtAvgBal5;

    @ApiModelProperty(value = "贷款日均余额6月")
    private String loanDtAvgBal6;

    @ApiModelProperty(value = "贷款日均余额7月")
    private String loanDtAvgBal7;

    @ApiModelProperty(value = "贷款日均余额8月")
    private String loanDtAvgBal8;

    @ApiModelProperty(value = "贷款日均余额9月")
    private String loanDtAvgBal9;

    @ApiModelProperty(value = "贷款日均余额10月")
    private String loanDtAvgBal10;

    @ApiModelProperty(value = "贷款日均余额11月")
    private String loanDtAvgBal11;

    @ApiModelProperty(value = "贷款日均余额12月")
    private String loanDtAvgBal12;

    @ApiModelProperty(value = "FTP净收入1月")
    private String netIncm1;

    @ApiModelProperty(value = "FTP净收入2月")
    private String netIncm2;

    @ApiModelProperty(value = "FTP净收入3月")
    private String netIncm3;

    @ApiModelProperty(value = "FTP净收入4月")
    private String netIncm4;

    @ApiModelProperty(value = "FTP净收入5月")
    private String netIncm5;

    @ApiModelProperty(value = "FTP净收入6月")
    private String netIncm6;

    @ApiModelProperty(value = "FTP净收入7月")
    private String netIncm7;

    @ApiModelProperty(value = "FTP净收入8月")
    private String netIncm8;

    @ApiModelProperty(value = "FTP净收入9月")
    private String netIncm9;

    @ApiModelProperty(value = "FTP净收入10月")
    private String netIncm10;

    @ApiModelProperty(value = "FTP净收入11月")
    private String netIncm11;

    @ApiModelProperty(value = "FTP净收入12月")
    private String netIncm12;

    @ApiModelProperty(value = "存款新开户数1月")
    private String depositNewOpenNum1;

    @ApiModelProperty(value = "存款新开户数2月")
    private String depositNewOpenNum2;

    @ApiModelProperty(value = "存款新开户数3月")
    private String depositNewOpenNum3;

    @ApiModelProperty(value = "存款新开户数4月")
    private String depositNewOpenNum4;

    @ApiModelProperty(value = "存款新开户数5月")
    private String depositNewOpenNum5;

    @ApiModelProperty(value = "存款新开户数6月")
    private String depositNewOpenNum6;

    @ApiModelProperty(value = "存款新开户数7月")
    private String depositNewOpenNum7;

    @ApiModelProperty(value = "存款新开户数8月")
    private String depositNewOpenNum8;

    @ApiModelProperty(value = "存款新开户数9月")
    private String depositNewOpenNum9;

    @ApiModelProperty(value = "存款新开户数10月")
    private String depositNewOpenNum10;

    @ApiModelProperty(value = "存款新开户数11月")
    private String depositNewOpenNum11;

    @ApiModelProperty(value = "存款新开户数12月")
    private String depositNewOpenNum12;

    @ApiModelProperty(value = "贷款管户户数1月")
    private String loanMngCustCustNum1;

    @ApiModelProperty(value = "贷款管户户数2月")
    private String loanMngCustCustNum2;

    @ApiModelProperty(value = "贷款管户户数3月")
    private String loanMngCustCustNum3;

    @ApiModelProperty(value = "贷款管户户数4月")
    private String loanMngCustCustNum4;

    @ApiModelProperty(value = "贷款管户户数5月")
    private String loanMngCustCustNum5;

    @ApiModelProperty(value = "贷款管户户数6月")
    private String loanMngCustCustNum6;

    @ApiModelProperty(value = "贷款管户户数7月")
    private String loanMngCustCustNum7;

    @ApiModelProperty(value = "贷款管户户数8月")
    private String loanMngCustCustNum8;

    @ApiModelProperty(value = "贷款管户户数9月")
    private String loanMngCustCustNum9;

    @ApiModelProperty(value = "贷款管户户数10月")
    private String loanMngCustCustNum10;

    @ApiModelProperty(value = "贷款管户户数11月")
    private String loanMngCustCustNum11;

    @ApiModelProperty(value = "贷款管户户数12月")
    private String loanMngCustCustNum12;

    @ApiModelProperty(value = "FTP净收入基数1月")
    private String netIncmBase1;

    @ApiModelProperty(value = "FTP净收入基数2月")
    private String netIncmBase2;

    @ApiModelProperty(value = "FTP净收入基数3月")
    private String netIncmBase3;

    @ApiModelProperty(value = "FTP净收入基数4月")
    private String netIncmBase4;

    @ApiModelProperty(value = "FTP净收入基数5月")
    private String netIncmBase5;

    @ApiModelProperty(value = "FTP净收入基数6月")
    private String netIncmBase6;

    @ApiModelProperty(value = "FTP净收入基数7月")
    private String netIncmBase7;

    @ApiModelProperty(value = "FTP净收入基数8月")
    private String netIncmBase8;

    @ApiModelProperty(value = "FTP净收入基数9月")
    private String netIncmBase9;

    @ApiModelProperty(value = "FTP净收入基数10月")
    private String netIncmBase10;

    @ApiModelProperty(value = "FTP净收入基数11月")
    private String netIncmBase11;

    @ApiModelProperty(value = "FTP净收入基数12月")
    private String netIncmBase12;

    @TableField("OPERATEDATE")
    @ApiModelProperty(value = "操作日期")
    private String operateDate;

    @ApiModelProperty(value = "操作员")
    private String operator;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "跑批日期")
    @TableField(exist=false)
    private String batchdate;
}
