package com.beyondsoft.modules.tb.controller;

import java.util.*;

import com.beyondsoft.common.utils.DateKit;
import com.beyondsoft.modules.tb.entity.jsonEntity.funnelEntity.FunnelRoot;
import com.beyondsoft.modules.tb.entity.jsonEntity.graphEntity.GraphRoot;
import com.beyondsoft.modules.tb.entity.jsonEntity.pieEntity.PieRoot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.beyondsoft.modules.tb.service.StageCountService;
import com.beyondsoft.common.utils.R;


/**
 * 当日分期笔数
 *
 * @author wanchangweng
 * @email
 * @date 2019-07-12 09:52:42
 */
@RestController
@RequestMapping("tb/stageCount")
public class StageCountController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private StageCountService stageCountService;


    /**
     * 根据系统，分期类型查询每日分期笔数  折线图，柱状图
     * 上送systemId不为：null,空时，查询单一渠道渠道，否则查询所有渠道
     */
    @RequestMapping("/queryStageCountList")
    public R queryStageCountList(@RequestParam Map<String, Object> reqPara) {


       /* public R queryStageCountList() {
            Map reqPara = new HashMap<String, String>();
            reqPara.put("bankNum","0461");
            //reqPara.put("systemId","weixin");
            reqPara.put("stageType","purStage");
            reqPara.put("ctType","bar");*/




        //查询数据并处理
        GraphRoot graphRoot = stageCountService.queryStageCountList(reqPara);

        return R.ok().put("rtnMap", graphRoot);
    }


    /**
     * 根据系统，分期类型查询近7日分期流失趋势   折线图，柱状图
     * 上送systemId不为：null,空时，查询单一渠道渠道，否则查询所有渠道
     */
    @RequestMapping("/queryStageAwayCountList")
    public R queryStageAwayCountList(@RequestParam Map<String, Object> reqPara) {


         /*public R queryStageAwayCountList() {
            Map reqPara = new HashMap<String, String>();
            reqPara.put("bankNum","0461");
            //reqPara.put("systemId","weixin");
            reqPara.put("stageType","purStage");
            reqPara.put("ctType","bar");*/



        //查询数据并处理
        GraphRoot graphRoot = stageCountService.queryStageAwayCountList(reqPara);

        return R.ok().put("rtnMap", graphRoot);
    }


    /**
     * 根据系统，分期类型查询分期流失漏斗
     */
    @RequestMapping("/queryStageFunnelCountList")
    public R queryStageFunnelCountList(@RequestParam Map<String, Object> reqPara) {

        /*public R queryStageFunnelCountList() {
            Map reqPara = new HashMap<String, String>();
            reqPara.put("bankNum","6501");
            //reqPara.put("systemId","weixin");
            reqPara.put("stageType","billStage");
            reqPara.put("ctType","bar");*/

        //查询数据并处理
        FunnelRoot funnelRoot = stageCountService.queryStageFunnelCountList(reqPara);
        return R.ok().put("rtnMap", funnelRoot);
    }

    /**
     * 根据银行号查询各系统时间段分期笔数  饼图展示
     * 上送systemId不为：null,空时，查询单一渠道渠道，否则查询所有渠道
     */
    @RequestMapping("/queryStageTimeList")
    public R queryStageTimeList(@RequestParam Map<String, Object> reqPara){

        //查询数据并处理
        PieRoot pieRoot = stageCountService.queryStageTimeList(reqPara);

        return R.ok().put("rtnMap", pieRoot);
    }


    /**
     * 根据银行号查询各系统时间段分期笔数  折线图展示
     * 上送systemId不为：null,空时，查询单一渠道渠道，否则查询所有渠道
     */
    @RequestMapping("/queryStageLineTimeList")
    public R queryStageLineTimeList(@RequestParam Map<String, Object> reqPara){

        //获取昨天日期
        String date = DateKit.getNextDayFate("yyyyMMdd",-1);
        reqPara.put("date", date);

        //查询数据并处理
        GraphRoot graphRoot = stageCountService.queryStageLineTimeList(reqPara);

        return R.ok().put("rtnMap", graphRoot);
    }


    /**
     * 根据系统，分期类型查询每日分期金额
     * 上送systemId不为：null,空时，查询单一渠道渠道，否则查询所有渠道
     */
    @RequestMapping("/queryStageMoneyList")
    public R queryStageMoneyList(@RequestParam Map<String, Object> reqPara){

        //查询数据并处理
        GraphRoot graphRoot = stageCountService.queryStageMoneyList(reqPara);

        return R.ok().put("rtnMap", graphRoot);
    }

}
