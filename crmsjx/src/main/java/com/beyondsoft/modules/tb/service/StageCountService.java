package com.beyondsoft.modules.tb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.modules.tb.entity.StageCountEntity;
import com.beyondsoft.modules.tb.entity.jsonEntity.funnelEntity.FunnelRoot;
import com.beyondsoft.modules.tb.entity.jsonEntity.graphEntity.GraphRoot;
import com.beyondsoft.modules.tb.entity.jsonEntity.pieEntity.PieRoot;

import java.util.Map;

/**
 * 当日分期笔数
 *
 * @author wanchangweng
 * @email 
 * @date 2019-07-12 09:52:42
 */
public interface StageCountService extends IService<StageCountEntity> {

    /**
     * 分期笔数趋势，用于柱状图，折线图展示
     */
    GraphRoot queryStageCountList(Map reqPara);

    /**
     * 分期流失趋势，用于柱状图，折线图展示
     */
    GraphRoot queryStageAwayCountList(Map reqPara);

    /**
     * 分期流失漏斗查询
     */
    FunnelRoot queryStageFunnelCountList(Map reqPara);

    /**
     *  根据银行号查询各系统时间段分期笔数 饼图展示
     * @param reqPara
     * @return
     */
    PieRoot queryStageTimeList(Map reqPara);

    /**
     *  根据银行号查询各系统时间段分期笔数 折线图展示
     * @param reqPara
     * @return
     */
    GraphRoot queryStageLineTimeList(Map reqPara);

    /**
     *  根据系统，银行号，分期类型查询每日分期金额  柱状图，饼图展示
     * @param reqPara
     * @return
     */
    GraphRoot queryStageMoneyList(Map reqPara);

    /**
     * 按系统，银行，日期查询分期笔数
     * @param reqPara
     * @return
     */
    public Map queryCurrStageCount(Map reqPara);



}

