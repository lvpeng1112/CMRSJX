package com.beyondsoft.modules.tb.service;


import java.util.Map;

/**
 * 供外部调用接口
 *
 * @author wanchangweng
 * @email 
 * @date 20220-03-24 16:34:08
 */
public interface ExternalCountService {

    /**
     * 网申系统查询接口
     * @param reqPara
     * @return
     */
    public Map  queryApplyCountList(Map reqPara);

    /**
     * 刷新缓存接口
     * @param reqPara
     * @return
     */
    public Map  flushProp(Map reqPara);


}

