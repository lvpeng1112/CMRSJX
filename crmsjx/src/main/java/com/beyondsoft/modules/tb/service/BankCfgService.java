package com.beyondsoft.modules.tb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.modules.tb.entity.BankCfgEntity;
import com.beyondsoft.common.utils.PageUtils;
import com.beyondsoft.common.utils.R;

import java.util.List;
import java.util.Map;

/**
 * @author wanchangweng
 * @email
 * @date 2020-04-15 15:00:08
 */
public interface BankCfgService extends IService<BankCfgEntity> {

    public List<BankCfgEntity> queryBankCfgList();

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 刷新tb_bank_cfg缓存
     */
    public R flushBankCfg();

    /**
     * 更新或新增tb_bank_cfg参数
     *
     * @return
     */
    public R updateOrSaveFlushBankCfg(String bankNum, String systemId, String propName, String propType, String propValue, String rsv1, String rsv2, String rsv3);

    /**
     * 删除tb_bank_cfg参数
     *
     * @return
     */
    public R deleteFlushBankCfg(String bankNum, String systemId, String propName, String propType);

}

