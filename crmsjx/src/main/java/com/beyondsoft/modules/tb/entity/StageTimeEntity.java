package com.beyondsoft.modules.tb.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import lombok.Data;

/**
 * 分期时间分布
 * 
 * @author wanchangweng
 * @email 
 * @date 2019-07-12 09:52:42
 */
@Data
@TableName("tb_stage_time")
public class StageTimeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 系统标识
	 */
	@TableId
	private String system;
	/**
	 * 银行号
	 */
	private String banknum;
	/**
	 * 导入日期
	 */
	private String date;
	/**
	 * 分期类型
	 */
	private String stageType;
	/**
	 * 分布时间段
	 */
	private String timePeriod;
	/**
	 * 当前时间段分期笔数
	 */
	private String count;

}
