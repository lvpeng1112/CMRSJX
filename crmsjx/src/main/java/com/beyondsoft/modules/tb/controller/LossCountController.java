package com.beyondsoft.modules.tb.controller;

import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.tb.service.LossCountService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * stageLoss
 *
 * @author huilingwang
 * @email
 * @date 2020-04-14 09:52:42
 */
@RestController
@RequestMapping("tb/lossCount")
public class LossCountController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private LossCountService lossCountService;
    /**
     * stageLoss报表下载
     */
    @RequestMapping(value = "/quaryLossReport")
    public R queryLossCountList(@RequestParam Map<String, Object> reqPara , HttpServletResponse response) {
        String bankNum = (String) reqPara.get("bankNum");
        String startTime = (String) reqPara.get("startTime");
        String endTime = (String) reqPara.get("endTime");
        logger.info("queryLossCountList ****银行号：" + bankNum + ";startTime:" + startTime + "endTime：" + endTime);
        if (StringUtils.isBlank(bankNum) || StringUtils.isBlank(startTime) || StringUtils.isBlank(endTime)) {
            return R.error(999999, "参数有误");
        } else {
            //查询报表并下载报表
            return lossCountService.lossReportDown(bankNum.trim(),startTime.trim(),endTime.trim(), response);
        }
    }


}
