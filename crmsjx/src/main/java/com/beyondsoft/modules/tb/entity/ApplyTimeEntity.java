package com.beyondsoft.modules.tb.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 网申分时访问数据统计表
 * 
 * @author wanchangweng
 * @email 
 * @date 2019-11-12 16:34:08
 */
@Data
@TableName("tb_apply_time")
public class ApplyTimeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 系统标识
	 */
	@TableId
	private String system;
	/**
	 * 银行号
	 */
	private String banknum;
	/**
	 * 导入日期
	 */
	private String date;
	/**
	 * 页面Id
	 */
	private String pageId;
	/**
	 * 时间段
	 */
	private String timePeriod;
	/**
	 * 网申访问人数
	 */
	private String count;

}
