package com.beyondsoft.modules.tb.entity.jsonEntity.pieEntity;

import lombok.Data;

@Data
public class PieData {

    //名称
    private String name;

    //数值
    private String value;
}
