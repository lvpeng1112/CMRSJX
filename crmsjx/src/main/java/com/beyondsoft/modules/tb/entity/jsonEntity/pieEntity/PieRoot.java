package com.beyondsoft.modules.tb.entity.jsonEntity.pieEntity;

import lombok.Data;

import java.util.List;

/**
 * 饼图模型
 */
@Data
public class PieRoot {

    //标题
    private String title;

    //数据类别（如，weixin,app）
    private List<String> legData;

    //数据集合
    private List<PieSeries> series;
}
