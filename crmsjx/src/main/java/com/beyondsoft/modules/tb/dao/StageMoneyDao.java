package com.beyondsoft.modules.tb.dao;

import com.beyondsoft.modules.tb.entity.StageMoneyEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 每日分期金额
 * 
 * @author wanchangweng
 * @email 
 * @date 2019-07-12 09:52:42
 */
@Mapper
public interface StageMoneyDao extends BaseMapper<StageMoneyEntity> {

    List<StageMoneyEntity> queryAllStageMoneyList(Map reqPara);

    List<StageMoneyEntity> queryStageMoneyList(Map reqPara);

    List<String> queryStageMoneySystemLis(Map reqPara);
	
}
