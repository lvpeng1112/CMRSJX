package com.beyondsoft.modules.tb.service.impl;

import com.beyondsoft.common.utils.DateKit;
import com.beyondsoft.common.utils.PageUtils;
import com.beyondsoft.common.utils.Query;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.tb.dao.BankCfgDao;
import com.beyondsoft.modules.tb.entity.BankCfgEntity;
import com.beyondsoft.cache.PropCache;
import com.beyondsoft.common.utils.*;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.beyondsoft.modules.tb.service.BankCfgService;


@Service("bankCfgService")
public class BankCfgServiceImpl extends ServiceImpl<BankCfgDao, BankCfgEntity> implements BankCfgService {
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired(required = false)
    private BankCfgDao bankCfgDao;


    @Override
    public List<BankCfgEntity> queryBankCfgList() {
        return bankCfgDao.queryParamKeyList();
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String bankNum = (String)params.get("bankNum");
        String systemId = (String)params.get("systemId");
        String propName = (String)params.get("propName");
        String propType = (String)params.get("propType");

        IPage<BankCfgEntity> page = this.page(
                new Query<BankCfgEntity>().getPage(params),
                new QueryWrapper<BankCfgEntity>()
                        .like(StringUtils.isNotBlank(bankNum),"bankId", bankNum)
                        .eq(StringUtils.isNotBlank(systemId),"systemId", systemId)
                        .eq(StringUtils.isNotBlank(propName),"propName", propName)
                        .eq(StringUtils.isNotBlank(propType),"propType", propType)
        );

        List<BankCfgEntity> pageList = page.getRecords();


        IPage<BankCfgEntity> page1 = page.setRecords(pageList);

        return new PageUtils(page1);
    }

    @Override
    public R flushBankCfg() {
        try {
            logger.info("++++++flushBankCfg 加载参数开始 start time" + DateKit.getCurrDate(DateKit.yyyyMMddHHmmss));
            long start = System.currentTimeMillis();
            List<BankCfgEntity> bankCfgEntityList = bankCfgDao.queryParamKeyList();
            for (int i = 0; i < bankCfgEntityList.size(); i++) {
                BankCfgEntity prop = bankCfgEntityList.get(i);
                PropCache.putCache(prop.getBankid() + prop.getSystemid() + prop.getPropname() + prop.getProptype(),
                        prop.getPropvalue());
            }
            long end = System.currentTimeMillis();
            logger.info("flushBankCfg加载参数缓存数：" + PropCache.getCacheSize() + " 耗时：" + (end - start) + "ms");

        } catch (Exception e) {
            logger.info("flushBankCfg刷新参数缓存出错", e);
            return R.error(999999, e.getMessage());
        }
        return R.ok();
    }

    @Override
    public R updateOrSaveFlushBankCfg(String bankNum, String systemId, String propName, String propType, String propValue,String rsv1,String rsv2,String rsv3) {
        Map reqPara = new HashMap();
        try {
            reqPara.put("bankNum", bankNum);
            reqPara.put("systemId", systemId);
            reqPara.put("propName", propName);
            reqPara.put("propType", propType);
            BankCfgEntity bankCfgEntity = bankCfgDao.queryParamKeyById(reqPara);
            reqPara.put("propValue", propValue);
            reqPara.put("rsv1", rsv1);
            reqPara.put("rsv2", rsv2);
            reqPara.put("rsv3", rsv3);

            if(bankCfgEntity==null){
                //保存
                bankCfgDao.saveParamKey(reqPara);
            }else{
                //更新
                bankCfgDao.updateParamKey(reqPara);
            }
        }catch (Exception e){
            logger.info("updateOrSaveFlushBankCfg error  is", e);
            return R.error(999999, e.getMessage());
        }
        return R.ok();
    }


    @Override
    public R deleteFlushBankCfg(String bankNum, String systemId, String propName, String propType) {
        Map reqPara = new HashMap();
        try {
            reqPara.put("bankNum", bankNum);
            reqPara.put("systemId", systemId);
            reqPara.put("propName", propName);
            reqPara.put("propType", propType);
            bankCfgDao.deleteParamKey(reqPara);
        }catch (Exception e){
            logger.info("deleteFlushBankCfg error  is", e);
            return R.error(999999, e.getMessage());
        }
        return R.ok();
    }
}