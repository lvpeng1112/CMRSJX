package com.beyondsoft.modules.tb.entity.jsonEntity.graphEntity;

import lombok.Data;

import java.util.List;

@Data
public class GraphSeries {

    //数据类别/名称
    private String name;

    //数据标示
    //private String stack;

    //数据类型
    private String type;

    //数据集合
    private List<String> data;

}
