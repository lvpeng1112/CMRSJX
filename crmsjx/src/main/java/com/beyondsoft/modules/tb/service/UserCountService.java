package com.beyondsoft.modules.tb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.modules.tb.entity.UserCountEntity;
import com.beyondsoft.modules.tb.entity.jsonEntity.gaugeEntity.GaugeRoot;
import com.beyondsoft.modules.tb.entity.jsonEntity.graphEntity.GraphRoot;
import com.beyondsoft.modules.tb.entity.jsonEntity.motionEntity.MotionRoot;
import com.beyondsoft.modules.tb.entity.jsonEntity.pieEntity.PieRoot;

import java.util.Map;

/**
 * tb_user_count
 *
 * @author wanchangweng
 * @email 
 * @date 2019-07-18 15:11:03
 */
public interface UserCountService extends IService<UserCountEntity> {

    /**
     *  查询用户PV，用于柱状图，折线图展示
     * @param reqPara
     * @return
     */
    GraphRoot queryPvList(Map reqPara);

    /**
     *  查询用户UV，用于柱状图，折线图展示
     * @param reqPara
     * @return
     */
    GraphRoot queryUvList(Map reqPara);

    /**
     *  查询用户User，用于柱状图，折线图展示
     * @param reqPara
     * @return
     */
    GraphRoot queryUserList(Map reqPara);

    /**
     *  查询新增用户，饼图
     * @param reqPara
     * @return
     */
    PieRoot queryNewUserList(Map reqPara);



    /**
     *  查看每秒访问实时流量  仪表盘
     * @param reqPara
     * @return
     */
    GaugeRoot querySpeedList(Map reqPara);


    /**
     * 按系统，银行，日期查询 pv、uv、user
     * @param reqPara
     * @return
     */
    public Map  queryCurrUserCount(Map reqPara);

    /**
     *查询当前实时监控数据
     * @param reqPara
     * @return
     */
    public Map queryPresentCount(Map reqPara);

    /**
     *查询所有渠道访问量动态折线图
     * @param reqPara
     * @return
     */
    public MotionRoot queryPVDynamicCount(Map reqPara);


    /**
     *  查询昨日不同系统PV量，饼图展示
     * @param reqPara
     * @return
     */
    PieRoot queryYesterdayPVCount(Map reqPara);


}

