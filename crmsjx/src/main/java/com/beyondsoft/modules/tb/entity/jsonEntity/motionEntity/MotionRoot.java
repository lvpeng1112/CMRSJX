package com.beyondsoft.modules.tb.entity.jsonEntity.motionEntity;

import lombok.Data;

import java.util.List;

/**
 * 折线图，柱状图模型
 */

@Data
public class MotionRoot {

    //标题
    private String title;

    //数据类别（如，weixin,app）
    private List<String> legData;

    //数据集合
    private  List<MotionSeries> series;


}
