package com.beyondsoft.modules.tb.entity.jsonEntity.funnelEntity;

import lombok.Data;

import java.util.List;

/**
 *漏斗模型
 */
@Data
public class FunnelRoot {

    private String title;

    //数据类别（如，weixin,app）
    private List<String> legData;

    private boolean calculable = true;

    private List<FunnelSeries> series;
}
