package com.beyondsoft.modules.tb.entity.jsonEntity.funnelEntity;

import lombok.Data;

@Data
public class FunnelData {

    private String name;

    private String value;

}
