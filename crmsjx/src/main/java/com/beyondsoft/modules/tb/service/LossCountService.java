package com.beyondsoft.modules.tb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.modules.tb.entity.StageLossEntity;
import com.beyondsoft.common.utils.R;

import javax.servlet.http.HttpServletResponse;

/**
 * stageLoss报表下载
 *
 * @author hlwang
 * @email
 * @date 2019-07-12 09:52:42
 */
public interface LossCountService extends IService<StageLossEntity> {

    /**
     * stageLoss报表下载
     */
    R lossReportDown(String bankNum, String startTime, String endTime, HttpServletResponse response);

    /**
     * stageLoss按时抓取
     */
    R lossTrack(String startTime, String endTime, String currDate);




}

