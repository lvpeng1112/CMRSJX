package com.beyondsoft.modules.tb.controller;

import java.util.Map;

import com.beyondsoft.common.utils.DateKit;
import com.beyondsoft.modules.tb.entity.jsonEntity.funnelEntity.FunnelRoot;
import com.beyondsoft.modules.tb.entity.jsonEntity.graphEntity.GraphRoot;
import com.beyondsoft.modules.tb.entity.jsonEntity.pieEntity.PieRoot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.beyondsoft.modules.tb.service.ApplyCountService;
import com.beyondsoft.common.utils.R;


/**
 * 网申数量
 *
 * @author wanchangweng
 * @email
 * @date 2019-11-12 16:34:08
 */
@RestController
@RequestMapping("tb/applyCount")
public class ApplyCountController {
    @Autowired
    private ApplyCountService applyCountService;


    /**
     * 查询昨日不同系统PV量，饼图展示
     */
    @RequestMapping("/queryApplyTimeCount")
    public R queryApplyTimeCount(@RequestParam Map<String, Object> reqPara) {
        //获取昨天日期
        String date = DateKit.getNextDayFate("yyyyMMdd",-1);
        reqPara.put("date", date);

        //查询数据并处理
        PieRoot pieRoot = applyCountService.queryApplyTimeCount(reqPara);

        return R.ok().put("rtnMap", pieRoot);
    }


    /**
     *  网申流失漏斗查询  漏斗图
     */
    @RequestMapping("/queryApplyFunnelCountList")
    public R queryApplyFunnelCountList(@RequestParam Map<String, Object> reqPara) {

        //查询数据并处理
        FunnelRoot funnelRoot = applyCountService.queryApplyFunnelCountList(reqPara);
        return R.ok().put("rtnMap", funnelRoot);
    }


    /**
     *  网申元素停留时间排行榜，用于柱状图，折线图展示
     */
    @RequestMapping("/queryApplyAvgCountList")
    public R queryApplyAvgCountList(@RequestParam Map<String, Object> reqPara) {
        //获取昨天日期
        String date = DateKit.getNextDayFate("yyyyMMdd",-1);
        reqPara.put("date", date);

        //查询数据并处理
        GraphRoot graphRoot = applyCountService.queryApplyAvgCountList(reqPara);
        return R.ok().put("rtnMap", graphRoot);
    }


    /**
     * 根据系统,pageId查询近7日网申流失趋势   折线图，柱状图
     * 上送systemId,bankId,pageId
     */
    @RequestMapping("/queryApplyAwayCountList")
    public R queryApplyAwayCountList(@RequestParam Map<String, Object> reqPara) {

        //查询数据并处理
        GraphRoot graphRoot = applyCountService.queryApplyAwayCountList(reqPara);

        return R.ok().put("rtnMap", graphRoot);
    }


    /**
     * 按系统，银行，日期查询网申进件，网申流失量 面板展示
     *
     */
    @RequestMapping("/queryCurrApplyCountList")
    public R queryCurrApplyCountList(@RequestParam Map<String, Object> reqPara) {

        //查询数据并处理
        Map rtnMap = applyCountService.queryCurrApplyCountList(reqPara);

        return R.ok().put("rtnMap", rtnMap);
    }


}
