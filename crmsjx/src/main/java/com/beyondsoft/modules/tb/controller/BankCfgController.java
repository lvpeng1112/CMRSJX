package com.beyondsoft.modules.tb.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.beyondsoft.modules.tb.entity.BankCfgEntity;
import com.beyondsoft.modules.tb.service.BankCfgService;
import com.beyondsoft.common.utils.PageUtils;
import com.beyondsoft.common.utils.R;

/**
 * @author wanchangweng
 * @email
 * @date 2020-04-15 15:00:08
 */
@RestController
@RequestMapping("tb/bankcfg")
public class BankCfgController {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private BankCfgService bankCfgService;

    /**
     * 刷新tb_bank_cfg缓存
     */
    @RequestMapping("/flushBankCfg")
    public R flushBankCfg() {
        return bankCfgService.flushBankCfg();

    }


    /**
     * 更新或新增tb_bank_cfg参数
     */
   @RequestMapping("/updateOrSaveFlushBankCfg")
    public R updateOrSaveFlushBankCfg(@RequestParam Map<String, Object> reqPara) {
        String bankNum = (String) reqPara.get("bankNum");
        String systemId = (String) reqPara.get("systemId");
        String propName = (String) reqPara.get("propName");
        String propType = (String) reqPara.get("propType");
        String propValue = (String) reqPara.get("propValue");
        String rsv1 = (String) reqPara.get("rsv1");
        String rsv2 = (String) reqPara.get("rsv2");
        String rsv3 = (String) reqPara.get("rsv3");

        logger.info("updateOrSaveFlushBankCfg ****银行号：" + bankNum + ";systemId:" + systemId + ";propName：" + propName + ";propType：" + propType + ";propValue：" + propValue);

        if (StringUtils.isBlank(bankNum) || StringUtils.isBlank(systemId) || StringUtils.isBlank(propName) || StringUtils.isBlank(propType) || StringUtils.isBlank(propValue)) {
            return R.error(999999, "参数有误");
        } else {
            return bankCfgService.updateOrSaveFlushBankCfg(bankNum, systemId, propName, propType, propValue,rsv1,rsv2,rsv3);
        }
    }


    /**
     * 删除tb_bank_cfg参数
     */
    @RequestMapping("/deleteFlushBankCfg")
    public R deleteFlushBankCfg(@RequestParam Map<String, Object> reqPara) {
        String bankNum = (String) reqPara.get("bankNum");
        String systemId = (String) reqPara.get("systemId");
        String propName = (String) reqPara.get("propName");
        String propType = (String) reqPara.get("propType");
        logger.info("deleteFlushBankCfg ****银行号：" + bankNum + ";systemId:" + systemId + ";propName：" + propName + ";propType：" + propType);

        if (StringUtils.isBlank(bankNum) || StringUtils.isBlank(systemId) || StringUtils.isBlank(propName) || StringUtils.isBlank(propType)) {
            return R.error(999999, "参数有误");
        } else {
            return bankCfgService.deleteFlushBankCfg(bankNum, systemId, propName, propType);
        }
    }


    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = bankCfgService.queryPage(params);

        return R.ok().put("page", page);
    }




    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody BankCfgEntity bankCfg) {
        bankCfgService.save(bankCfg);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody BankCfgEntity bankCfg) {
        bankCfgService.updateById(bankCfg);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody String[] bankids) {
        bankCfgService.removeByIds(Arrays.asList(bankids));
        return R.ok();
    }

}
