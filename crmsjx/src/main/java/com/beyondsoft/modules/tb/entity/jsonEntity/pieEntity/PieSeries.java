package com.beyondsoft.modules.tb.entity.jsonEntity.pieEntity;

import lombok.Data;

import java.util.List;

@Data
public class PieSeries {

    //访问来源
    private String name;

    //数据类型
    private String type = "pie";

    //半径
    private String radius = "55%";

    //中心位置
    private String[] center = {"50%", "60%"};

    //数据集合
    private List<PieData> data;
}
