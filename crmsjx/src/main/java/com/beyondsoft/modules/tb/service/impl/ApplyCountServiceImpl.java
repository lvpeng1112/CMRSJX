package com.beyondsoft.modules.tb.service.impl;

import com.beyondsoft.modules.sys.dao.SysConfigDao;
import com.beyondsoft.modules.sys.service.SysConfigService;
import com.beyondsoft.modules.tb.dao.ApplyAvgDao;
import com.beyondsoft.modules.tb.dao.ApplyCountDao;
import com.beyondsoft.modules.tb.dao.ApplyTimeDao;
import com.beyondsoft.modules.tb.entity.ApplyAvgEntity;
import com.beyondsoft.modules.tb.entity.ApplyCountEntity;
import com.beyondsoft.modules.tb.entity.ApplyTimeEntity;
import com.beyondsoft.modules.tb.entity.jsonEntity.funnelEntity.FunnelData;
import com.beyondsoft.modules.tb.entity.jsonEntity.funnelEntity.FunnelRoot;
import com.beyondsoft.modules.tb.entity.jsonEntity.funnelEntity.FunnelSeries;
import com.beyondsoft.modules.tb.entity.jsonEntity.graphEntity.GraphRoot;
import com.beyondsoft.modules.tb.entity.jsonEntity.graphEntity.GraphSeries;
import com.beyondsoft.common.utils.DateUtils;
import com.beyondsoft.common.utils.StrKit;
import com.beyondsoft.modules.tb.entity.jsonEntity.pieEntity.PieData;
import com.beyondsoft.modules.tb.entity.jsonEntity.pieEntity.PieRoot;
import com.beyondsoft.modules.tb.entity.jsonEntity.pieEntity.PieSeries;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.beyondsoft.modules.tb.service.ApplyCountService;


@Service("applyCountService")
public class ApplyCountServiceImpl extends ServiceImpl<ApplyCountDao, ApplyCountEntity> implements ApplyCountService {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private ApplyTimeDao applyTimeDao;

    @Autowired(required = false)
    private ApplyAvgDao applyAvgDao;

    @Autowired(required = false)
    private SysConfigDao sysConfigDao;

    @Autowired
    private SysConfigService sysConfigService;

    @Override
    public PieRoot queryApplyTimeCount(Map reqPara) {
        //查询昨日网申用户访问量分时统计 饼图展示
        String bankNum = (String) reqPara.get("bankNum");            //银行号
        logger.info("****银行号：" + bankNum);

        PieRoot pieRoot = new PieRoot();
        PieSeries pieSeries = new PieSeries();
        PieData pieData = new PieData();
        pieRoot.setTitle("网申分时访问量分布");

        List<PieSeries> pieSeriesList = new ArrayList<>();           //组装后list
        List<PieData> pieDataList = new ArrayList<>();               //数据data
        List<String> legData = new ArrayList<>();                    //系统名称

        //查出分组数据 近30日用户增量
        List<ApplyTimeEntity> querySumList = applyTimeDao.queryApplyTimeCount(reqPara);
        for (ApplyTimeEntity querySumEntity : querySumList) {
            legData.add(querySumEntity.getSystem());
            pieData = new PieData();
            pieData.setName(querySumEntity.getTimePeriod());
            pieData.setValue(querySumEntity.getCount());
            pieDataList.add(pieData);
        }

        pieSeries.setName("网申");
        pieSeries.setData(pieDataList);
        pieSeriesList.add(pieSeries);

        pieRoot.setLegData(legData);
        pieRoot.setSeries(pieSeriesList);

        return pieRoot;
    }

    @Override
    public FunnelRoot queryApplyFunnelCountList(Map reqPara) {
        //网申流失漏斗
        String systemId = (String) reqPara.get("systemId");       //渠道id
        String bankNum = (String) reqPara.get("bankNum");         //银行号
        logger.info("****银行号：" + bankNum + ";渠道ID:" + systemId);

        FunnelRoot funnelRoot = new FunnelRoot();
        FunnelSeries funnelSeries = new FunnelSeries();
        FunnelData funnelData = new FunnelData();
        funnelRoot.setTitle("网申流失漏斗");

        List<FunnelSeries> FunnelSeriesList = new ArrayList<>();           //组装后list
        List<FunnelData> FunnelDataList = new ArrayList<>();               //数据data
        List<String> legData = new ArrayList<>();                          //步骤

        Map<String, Object> stepMap = sysConfigService.queryStepConfig(systemId,bankNum,"apply");
        String[] stepIds = (String[])stepMap.get("stepIds");
        String[] stepNames = (String[])stepMap.get("stepNames");

        List<ApplyCountEntity> queryFunnelList = baseMapper.queryApplyFunnelCountList(reqPara);
        legData.add(systemId);

        for(int i= 0; i<stepIds.length; i++){
            for (ApplyCountEntity queryFunnelEntity : queryFunnelList) {
                if(stepIds[i].equals(queryFunnelEntity.getStepId())){
                    funnelData = new FunnelData();
                    funnelData.setName(stepNames[i]);
                    funnelData.setValue(queryFunnelEntity.getCount());
                    FunnelDataList.add(funnelData);
                    break;
                }
            }
        }

        funnelSeries.setName(systemId);
        funnelSeries.setData(FunnelDataList);
        FunnelSeriesList.add(funnelSeries);

        funnelRoot.setLegData(legData);
        funnelRoot.setSeries(FunnelSeriesList);

        return funnelRoot;
    }



    @Override
    public GraphRoot queryApplyAvgCountList(Map reqPara) {
        //网申元素停留时间排行榜，用于柱状图，折线图展示
        String systemId = (String) reqPara.get("systemId");      //渠道id
        String bankNum = (String) reqPara.get("bankNum");        //银行号
        String ctType = (String) reqPara.get("ctType");          //图表类型  line：折线图  bar：柱状图
        logger.info("****银行号：" + bankNum + ";渠道ID:" + systemId);

        GraphRoot graphRoot = new GraphRoot();
        GraphSeries series = new GraphSeries();
        graphRoot.setTitle("网申元素停留时间排行榜");

        List<GraphSeries> rtnList = new ArrayList<>();      //组装后list
        List<String> dataList = new ArrayList<>();          //存放data
        List<String> xNameList = new ArrayList<>();         //存放date（横坐标）
        List<String> systemList = new ArrayList<>();        //系统名称

        List<ApplyAvgEntity> queryList = applyAvgDao.queryApplyAvgCountList(reqPara);
        systemList.add(systemId);
        for (ApplyAvgEntity queryEntity : queryList) {
            dataList.add(queryEntity.getItemSpendAvg());
            xNameList.add(queryEntity.getItemId());
        }

        //series.setStack("元素平均时间");
        series.setType(ctType);
        series.setName(systemId);
        series.setData(dataList);
        rtnList.add(series);

        graphRoot.setLegData(systemList);
        graphRoot.setXName(xNameList);
        graphRoot.setSeries(rtnList);


        return graphRoot;
    }



    @Override
    public GraphRoot queryApplyAwayCountList(Map reqPara) {
        //网申流失趋势查询，用于柱状图，折线图展示
        String systemId = (String) reqPara.get("systemId");          //渠道id
        String bankNum = (String) reqPara.get("bankNum");            //银行号
        String applyType = (String) reqPara.get("applyType");        //网申类型（pageId）
        String ctType = (String) reqPara.get("ctType");              //图表类型  line：折线图  bar：柱状图

        GraphRoot graphRoot = new GraphRoot();
        GraphSeries series = new GraphSeries();
        graphRoot.setTitle("网申流失趋势");

        List<GraphSeries> rtnList = new ArrayList<>();      //组装后list
        List<String> dataList = new ArrayList<>();          //存放data
        List<String> xNameList = new ArrayList<>();         //存放date（横坐标）
        List<String> systemList = new ArrayList<>();        //系统名称
        String[] dateArray = DateUtils.getWithinSevenDays();

        //根据系统，银行号，pageId查询网申访问数据
        List<ApplyCountEntity> queryList = baseMapper.queryApplyCountList(reqPara);

        Map<String, Object> stepMap = sysConfigService.queryStepConfig(systemId,bankNum,"apply");
        String[] stepIds = (String[])stepMap.get("stepIds");

        //处理queryList得到每日流失人数
        List<ApplyCountEntity> applyCountList = getCalculateApplyCountList(queryList,stepIds[0],stepIds[stepIds.length-1]);
        systemList.add(systemId);
        for(String date:dateArray){
            xNameList.add(date);
            int a = 0;
            for (ApplyCountEntity queryEntity : applyCountList) {
                if(date.equals(queryEntity.getDate())){
                    dataList.add(queryEntity.getCount());
                    a = 1;
                    break;
                }
            }
            if(a == 0){
                dataList.add("0");
            }
        }

        //series.setStack("总量");
        series.setType(ctType);
        series.setName(systemId);
        series.setData(dataList);
        rtnList.add(series);

        graphRoot.setLegData(systemList);
        graphRoot.setXName(xNameList);
        graphRoot.setSeries(rtnList);


        return graphRoot;
    }



    /**
     * 处理queryList得到每日流失人数
     * 流失人数 = 进入第一步 - 申请成功数量
     *
     * @param queryList
     * @return
     */

    public List<ApplyCountEntity> getCalculateApplyCountList(List<ApplyCountEntity> queryList, String stepId1, String stepId2) {

        List<ApplyCountEntity> applyCountList = new ArrayList<>();  //每日流失人数集合
        ApplyCountEntity applyCountEntity = new ApplyCountEntity();

        for (int i = 0; i < queryList.size(); i++) {
            ApplyCountEntity queryEntity1 = queryList.get(i);
            if (stepId1.equals(queryEntity1.getStepId())) {
                //步骤1
                for (int j = 0; j < queryList.size(); j++) {
                    ApplyCountEntity queryEntity2 = queryList.get(j);
                    if (queryEntity1.getSystem().equals(queryEntity2.getSystem())
                            && queryEntity1.getDate().equals(queryEntity2.getDate())
                            && queryEntity1.getPageId().equals(queryEntity2.getPageId())
                            && stepId2.equals(queryEntity2.getStepId())) {
                        //同一天且分期成功,计算当天流失人数
                        String countStr = StrKit.SubStrByBigDecimal(queryEntity1.getCount(), queryEntity2.getCount());
                        applyCountEntity = new ApplyCountEntity();
                        applyCountEntity.setSystem(queryEntity1.getSystem());
                        applyCountEntity.setBanknum(queryEntity1.getBanknum());
                        applyCountEntity.setDate(queryEntity1.getDate());
                        applyCountEntity.setPageId(queryEntity1.getPageId());
                        applyCountEntity.setCount(countStr);    //当天流失人数
                        applyCountList.add(applyCountEntity);

                        continue;
                    }
                }
            }
        }

        return applyCountList;

    }


    @Override
    public Map queryCurrApplyCountList(Map reqPara) {

        String systemId = (String) reqPara.get("systemId");          //渠道id
        String bankNum = (String) reqPara.get("bankNum");            //银行号

        List<ApplyCountEntity> applyCountList = baseMapper.queryCurrApplyCountList(reqPara);

        String applySuc = "0";     //网申进件数量
        String applyLoss = "0";    //网申流失数量
        String applyIndex = "0";   //进入第一步量

        Map<String, Object> stepMap = sysConfigService.queryStepConfig(systemId,bankNum,"apply");
        String[] stepIds = (String[])stepMap.get("stepIds");

        for (ApplyCountEntity applyCountEntity:applyCountList) {
            if(stepIds[0].equals(applyCountEntity.getStepId())){
                applyIndex = applyCountEntity.getCount();
            }

            if(stepIds[stepIds.length-1].equals(applyCountEntity.getStepId())){
                applySuc = applyCountEntity.getCount();
            }
        }

        applyLoss=  StrKit.SubStrByBigDecimal(applyIndex,applySuc);

        Map<String, Object> rtnMap = new HashMap<>();
        rtnMap.put("applySuc",applySuc);
        rtnMap.put("applyLoss",applyLoss);
        return rtnMap;
    }


/*    *//**
     * 获取网申步骤配置信息
     * @param systemId
     * @param bankNum
     * @return
     *//*
    private Map getStepConfig(String systemId, String bankNum, String type){

        Map<String, Object> stepMap = new HashMap<>();

        String[] stepIds = null;
        String[] stepNames = null;

        //获取系统步骤标识
        SysConfigEntity sysConfigEntity1 = sysConfigDao.queryByKey(systemId + "-" + bankNum + "-" +type+ "-stepId");
        //获取系统步骤名称
        SysConfigEntity sysConfigEntity2 = sysConfigDao.queryByKey(systemId + "-" + bankNum + "-" +type+ "-stepName");

        if(sysConfigEntity1 == null){
            sysConfigEntity1 = sysConfigDao.queryByKey(systemId + "-" + "0000" + "-" +type+ "-stepId");
        }
        if(sysConfigEntity2 == null){
            sysConfigEntity2 = sysConfigDao.queryByKey(systemId + "-" + "0000" + "-" +type+ "-stepName");
        }

        stepIds = sysConfigEntity1.getParamValue().split(",");
        stepNames = sysConfigEntity2.getParamValue().split(",");
        stepMap.put("stepIds",stepIds);
        stepMap.put("stepNames",stepNames);

        return stepMap;

    }*/
}