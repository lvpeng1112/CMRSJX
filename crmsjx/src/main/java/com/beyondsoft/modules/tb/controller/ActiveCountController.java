package com.beyondsoft.modules.tb.controller;

import java.util.*;

import com.beyondsoft.modules.tb.entity.jsonEntity.funnelEntity.FunnelRoot;
import com.beyondsoft.modules.tb.entity.jsonEntity.graphEntity.GraphRoot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.beyondsoft.modules.tb.service.ActiveCountService;
import com.beyondsoft.common.utils.R;



/**
 * 近30日激活人数
 *
 * @author wanchangweng
 * @email 
 * @date 2019-07-12 09:52:42
 */
@RestController
@RequestMapping("tb/activeCount")
public class ActiveCountController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ActiveCountService activeCountService;

    /**
     * 根据系统，查询近7日激活人数  用于柱状图，折线图展示
     * 上送systemId不为：null,空时，查询单一渠道渠道，否则查询所有渠道
     */
    @RequestMapping("/queryActiveCountList")
    public R queryActiveCountList(@RequestParam Map<String, Object> reqPara){

        //查询数据并处理
        GraphRoot graphRoot =activeCountService.queryActiveCountList(reqPara);

        return R.ok().put("rtnMap", graphRoot);
    }


    /**
     * 根据系统，查询激活流失趋势  用于柱状图，折线图展示
     */
    @RequestMapping("/queryActiveAwayCountList")
    public R queryActiveAwayCountList(@RequestParam Map<String, Object> reqPara) {

        //查询数据并处理
        GraphRoot graphRoot = activeCountService.queryActiveAwayCountList(reqPara);
        return R.ok().put("rtnMap", graphRoot);
    }



    /**
     * 根据系统，查询激活流失漏斗
     */
    @RequestMapping("/queryActiveFunnelCountList")
    public R queryActiveFunnelCountList(@RequestParam Map<String, Object> reqPara) {

        //查询数据并处理
        FunnelRoot funnelRoot = activeCountService.queryActiveFunnelCountList(reqPara);
        return R.ok().put("rtnMap", funnelRoot);
    }


}
