package com.beyondsoft.modules.tb.dao;

import com.beyondsoft.modules.tb.entity.ApplyTimeEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 网申分时访问数据统计表
 * 
 * @author wanchangweng
 * @email 
 * @date 2019-11-12 16:34:08
 */
@Mapper
public interface ApplyTimeDao extends BaseMapper<ApplyTimeEntity> {

    List<ApplyTimeEntity> queryApplyTimeCount(Map reqPara);
	
}
