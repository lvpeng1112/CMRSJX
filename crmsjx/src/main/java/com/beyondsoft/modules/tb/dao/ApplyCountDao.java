package com.beyondsoft.modules.tb.dao;

import com.beyondsoft.modules.tb.entity.ApplyCountEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 网申流失数量表
 * 
 * @author wanchangweng
 * @email 
 * @date 2019-11-12 16:34:08
 */
@Mapper
public interface ApplyCountDao extends BaseMapper<ApplyCountEntity> {

    List<ApplyCountEntity> queryApplyFunnelCountList(Map reqPara);

    List<ApplyCountEntity> queryApplyCountList(Map reqPara);

    List<ApplyCountEntity> queryCurrApplyCountList(Map reqPara);

}
