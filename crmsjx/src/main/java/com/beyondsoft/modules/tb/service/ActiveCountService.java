package com.beyondsoft.modules.tb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.modules.tb.entity.ActiveCountEntity;
import com.beyondsoft.modules.tb.entity.jsonEntity.funnelEntity.FunnelRoot;
import com.beyondsoft.modules.tb.entity.jsonEntity.graphEntity.GraphRoot;

import java.util.Map;

/**
 * 近30日激活人数
 *
 * @author wanchangweng
 * @email 
 * @date 2019-07-12 09:52:42
 */
public interface ActiveCountService extends IService<ActiveCountEntity> {

    /**
     *  根据系统，银行号查询每日激活人数  用于柱状图，折线图展示
     * @param reqPara
     * @return
     */
    GraphRoot queryActiveCountList(Map reqPara);

    /**
     * 激活流失趋势，用于柱状图，折线图展示
     */
    GraphRoot queryActiveAwayCountList(Map reqPara);


    /**
     * 激活流失漏斗查询
     */
    FunnelRoot queryActiveFunnelCountList(Map reqPara);

    /**
     * 按系统，银行，日期查询激活数量
     * @param reqPara
     * @return
     */
    public Map queryCurrActiveCount(Map reqPara);
}

