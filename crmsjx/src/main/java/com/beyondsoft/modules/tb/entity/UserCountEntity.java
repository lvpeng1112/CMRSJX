package com.beyondsoft.modules.tb.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import lombok.Data;

/**
 * tb_user_count
 * 
 * @author wanchangweng
 * @email 
 * @date 2019-07-18 15:11:03
 */
@Data
@TableName("tb_user_count")
public class UserCountEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 系统标识
	 */
	@TableId
	private String system;
	/**
	 * 银行号
	 */
	private String banknum;
	/**
	 * 导入日期
	 */
	private String date;
	/**
	 * pv量
	 */
	private String pv;
	/**
	 * uv量
	 */
	private String uv;
	/**
	 * 用户量
	 */
	private String user;

}
