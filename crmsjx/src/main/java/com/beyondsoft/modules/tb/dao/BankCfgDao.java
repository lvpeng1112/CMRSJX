package com.beyondsoft.modules.tb.dao;

import com.beyondsoft.modules.tb.entity.BankCfgEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author wanchangweng
 * @email 
 * @date 2020-04-15 15:00:08
 */
@Mapper
public interface BankCfgDao extends BaseMapper<BankCfgEntity> {
    /**
     * 查询全部配置
     * @return
     */
    List<BankCfgEntity> queryParamKeyList();

    /**
     * 根据主键查询
     * @return
     */
    BankCfgEntity queryParamKeyById(Map reqPara);

    /**
     * 保存
     * @return
     */
    BankCfgEntity saveParamKey(Map reqPara);


    /**
     * 更新
     * @return
     */
    BankCfgEntity updateParamKey(Map reqPara);

    /**
     * 删除
     * @return
     */
    void deleteParamKey(Map reqPara);



}
