package com.beyondsoft.modules.tb.dao;

import com.beyondsoft.modules.tb.entity.UserCountEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.*;

/**
 * tb_user_count
 * 
 * @author wanchangweng
 * @email 
 * @date 2019-07-18 15:11:03
 */
@Mapper
public interface UserCountDao extends BaseMapper<UserCountEntity> {

    List<UserCountEntity> queryAllUserCountList(Map reqPara);

    List<UserCountEntity> queryUserCountList(Map reqPara);

    List<String> queryUserCountSystemLis(Map reqPara);

    List<UserCountEntity> queryUserIncLis(Map reqPara);

    List<UserCountEntity> queryCurrUserCount(Map reqPara);

    List<UserCountEntity> queryYesterdayPVCount(Map reqPara);

}
