package com.beyondsoft.modules.tb.dao;

import com.beyondsoft.modules.tb.entity.StageCountEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 当日分期笔数
 * 
 * @author wanchangweng
 * @email 
 * @date 2019-07-12 09:52:42
 */
@Mapper
public interface StageCountDao extends BaseMapper<StageCountEntity> {

    List<StageCountEntity> queryAllStageCountList(Map reqPara);

    List<StageCountEntity> queryStageCountList(Map reqPara);

    List<String> queryStageCountSystemLis(Map reqPara);

    List<StageCountEntity> queryStageFunnelCountList(Map reqPara);

    List<StageCountEntity> queryCurrStageCountList(Map reqPara);

    List<StageCountEntity> queryAllStageFunnelCountList(Map reqPara);


}
