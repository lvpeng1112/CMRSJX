package com.beyondsoft.modules.tb.controller;

import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.tb.service.ExternalCountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;


/**
 * 供外部调用
 *
 * @author wanchangweng
 * @email
 * @date 20220-03-24 16:34:08
 */
@RestController
@RequestMapping("tb/externalCount")
public class ExternalCountController {
    @Autowired
    private ExternalCountService externalCountService;



    /**
     * 按系统，银行，日期查询网申数量
     *
     */
    @RequestMapping("/queryApplyCountList")
    public R queryApplyCountList(@RequestParam Map<String, Object> reqPara) {

        //查询数据并处理
        Map rtnMap = externalCountService.queryApplyCountList(reqPara);

        return R.ok().put("rtnMap", rtnMap);
    }


    /**
     * 刷新缓存
     *
     */
    @RequestMapping("/flushProp")
    public R flushProp(@RequestParam Map<String, Object> reqPara) {

        //查询数据并处理
        Map rtnMap = externalCountService.flushProp(reqPara);

        return R.ok().put("rtnMap", rtnMap);
    }


}
