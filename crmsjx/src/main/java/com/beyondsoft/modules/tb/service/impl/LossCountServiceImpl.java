package com.beyondsoft.modules.tb.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.job.service.MsgPushService;
import com.beyondsoft.modules.tb.dao.StageLossDao;
import com.beyondsoft.modules.tb.entity.StageLossEntity;
import com.beyondsoft.modules.tb.service.LossCountService;
import com.beyondsoft.utils.StrUtil;
import com.beyondsoft.cache.PropCache;
import com.beyondsoft.common.utils.FileUtis;
import com.beyondsoft.common.utils.R;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service("lossCountService")
public class LossCountServiceImpl extends ServiceImpl<StageLossDao, StageLossEntity> implements LossCountService {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    MsgPushService msgPushService;

    @Override
    public R lossReportDown(String bankNum, String startTime, String endTime, HttpServletResponse response) {
        Map reqPara = new HashMap();
        reqPara.put("bankNum", bankNum.trim());
        reqPara.put("startTime", startTime.trim());
        reqPara.put("endTime", endTime.trim());
        String rtnMes = null;
        String rtnExcleMes = null;
        List<StageLossEntity> queryList = baseMapper.queryStageLossCountList(reqPara);
        if (queryList.size() > 0) {
            //表头信息
            String[] args = {"userId", "bankId","oprDate", "systemId", "stepId", "pageId", "numCount"};  //表头
            String fileName = "beehiveStageLoss" + "_" + bankNum + "_" + startTime + "_" + endTime;
            String filePath = PropCache.getCacheInfo("stageLossReportfilePath");
            String excelPath =filePath+ fileName + ".xlsx";
            rtnMes = stageLossExcel(args, excelPath, queryList);
            if ("SUCCESS".equals(rtnMes)) {
                try {
                    rtnExcleMes = FileUtis.downExcle(response, excelPath, fileName);
                } catch (Exception e) {
                    logger.info("stageLossReportService downExcle error is:", e);
                    return R.error(999999, e.getMessage());
                }
                if ("SUCCESS".equals(rtnExcleMes)) {
                    FileUtis.deleteExcle(excelPath);
                }
            } else if ("FAIL".equals(rtnMes)) {
                return R.error(999999, "生成报表失败。");
            }

        } else {
            return R.error(999999, "未查到任何数据。");
        }

        return R.ok();
    }

    @Override
    public R lossTrack(String startTime, String endTime, String currDate) {

        Map reqPara = new HashMap();
        reqPara.put("startTime", startTime.trim());
        reqPara.put("endTime", endTime.trim());
        reqPara.put("currDate",currDate.trim());

        List<StageLossEntity> queryList = baseMapper.queryStageLossList(reqPara);
        if (queryList.size() > 0) {
            //判断是否推送处理
            for(int i=0; i<queryList.size();i++){
                trackService(queryList.get(i));
            }

        } else {
            return R.error(999999, "未查到任何数据。");
        }

        return R.ok();

    }


    /**
     * 将推荐关系(List集合)数据写入，存放指定路径下
     */
    private String stageLossExcel(String[] args, String excelPath, List<StageLossEntity> queryList) {
        //第一步，创建一个workbook对应一个excel文件
        XSSFWorkbook workbook = new XSSFWorkbook();
        //第二部，在workbook中创建一个sheet对应excel中的sheet
        XSSFSheet sheet = workbook.createSheet("beehiveStageLoss report");
        //第三部，在sheet表中添加表头第0行，老版本的poi对sheet的行列有限制
        XSSFRow row = sheet.createRow(0);
        //第四步，创建单元格，设置表头
        XSSFCell cell = null;
        for (int i = 0; i < args.length; i++) {
            cell = row.createCell(i);
            cell.setCellValue(args[i]);
        }

        //第五步，写入实体数据，实际应用中这些数据从数据库得到,对象封装数据，集合包对象。对象的属性值对应表的每行的值
        for (int i = 0; i < queryList.size(); i++) {
            StageLossEntity list = queryList.get(i);

            //创建单元格设值
            XSSFRow row1 = sheet.createRow(i + 1);
            row1.createCell(0).setCellValue(list.getUserId());
            row1.createCell(1).setCellValue(list.getBankId());
            row1.createCell(2).setCellValue(list.getOprDate());
            row1.createCell(3).setCellValue(list.getSystemId());
            row1.createCell(4).setCellValue(list.getStepId());
            row1.createCell(5).setCellValue(list.getPageId());
            row1.createCell(6).setCellValue(list.getNumCount());
        }

        //将文件保存到指定的位置
        try {
            FileOutputStream fos = new FileOutputStream(excelPath);
            workbook.write(fos);
            logger.info("stageLossExcel报表写入excel成功");
            fos.close();
            return "SUCCESS";
        } catch (IOException e) {
            logger.info("stageLossExcel报表写入excel error for :", e);
            e.printStackTrace();
            return "FAIL";
        }
    }


    public Map<String,String> trackService(StageLossEntity stageLoss){

        Map<String,String> map = new HashMap<>();
        Map<String,Object> sendMap = new HashMap<>();

        String userId = stageLoss.getUserId();
        String bankId = stageLoss.getBankId();
        String systemId = stageLoss.getSystemId();
        String pageId = stageLoss.getPageId();

        String pushFlag =PropCache.getCacheInfo(bankId+systemId+pageId+"pushFlag");
        if(StrUtil.isNotBlank(pushFlag) && "1".equals(pushFlag)){
            sendMap.put("userId",userId);
            sendMap.put("bankId",bankId);
            sendMap.put("pageId",pageId);
            msgPushService.sendTrackMsg(sendMap);
        }

        return map;

    }

}