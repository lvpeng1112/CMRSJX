package com.beyondsoft.modules.tb.entity.jsonEntity.gaugeEntity;

import lombok.Data;

import java.util.List;

@Data
public class GaugeSeries {

    private String name;

    private String type = "gauge";

    private GaugeDetail detail;

    private List<GaugeData> data;
}
