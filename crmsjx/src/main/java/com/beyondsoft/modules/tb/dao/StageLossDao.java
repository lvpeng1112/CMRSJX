package com.beyondsoft.modules.tb.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.tb.entity.StageLossEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author hlwang
 * @email
 * @date 2020-04-12 09:52:42
 */
@Mapper
public interface StageLossDao extends BaseMapper<StageLossEntity> {


    List<StageLossEntity> queryStageLossCountList(Map reqPara);


    List<StageLossEntity> queryStageLossList(Map reqPara);
}
