package com.beyondsoft.modules.tb.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import lombok.Data;

/**
 * 
 * 
 * @author wanchangweng
 * @email 
 * @date 2020-04-15 15:00:08
 */
@Data
@TableName("tb_bank_cfg")
public class BankCfgEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 银行号
	 */
	@TableId
	private String bankid;
	/**
	 * 系统标识
	 */
	@TableId
	private String systemid;
	/**
	 * 参数名称
	 */
	@TableId
	private String propname;
	/**
	 * 参数类型
	 */
	@TableId
	private String proptype;
	/**
	 * 参数值
	 */
	private String propvalue;
	/**
	 * 
	 */
	private String rsv1;
	/**
	 * 
	 */
	private String rsv2;
	/**
	 * 
	 */
	private String rsv3;

}
