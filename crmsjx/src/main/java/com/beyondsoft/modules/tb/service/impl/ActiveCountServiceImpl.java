package com.beyondsoft.modules.tb.service.impl;

import com.beyondsoft.modules.sys.service.SysConfigService;
import com.beyondsoft.modules.tb.dao.ActiveCountDao;
import com.beyondsoft.modules.tb.entity.ActiveCountEntity;
import com.beyondsoft.modules.tb.entity.jsonEntity.funnelEntity.FunnelData;
import com.beyondsoft.modules.tb.entity.jsonEntity.funnelEntity.FunnelRoot;
import com.beyondsoft.modules.tb.entity.jsonEntity.funnelEntity.FunnelSeries;
import com.beyondsoft.modules.tb.entity.jsonEntity.graphEntity.GraphRoot;
import com.beyondsoft.modules.tb.entity.jsonEntity.graphEntity.GraphSeries;
import com.beyondsoft.common.utils.DateUtils;
import com.beyondsoft.common.utils.StrKit;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.beyondsoft.modules.tb.service.ActiveCountService;


@Service("activeCountService")
public class ActiveCountServiceImpl extends ServiceImpl<ActiveCountDao, ActiveCountEntity> implements ActiveCountService {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private SysConfigService sysConfigService;


    @Override
    public GraphRoot queryActiveCountList(Map reqPara) {
        //每日激活人数
        String systemId = (String) reqPara.get("systemId");    //渠道id
        String bankNum = (String) reqPara.get("bankNum");        //银行号
        String ctType = (String) reqPara.get("ctType"); //图表类型  line：折线图  bar：柱状图
        logger.info("****银行号：" + bankNum + ";渠道ID:" + systemId);

        GraphRoot graphRoot = new GraphRoot();
        GraphSeries series = new GraphSeries();
        graphRoot.setTitle("每日激活人数");

        List<GraphSeries> rtnList = new ArrayList<>();      //组装后list
        List<String> dataList = new ArrayList<>();   //存放data
        List<String> xNameList = new ArrayList<>();   //存放date（横坐标）
        List<String> systemList = new ArrayList<>();   //系统名称

        if (StringUtils.isNotBlank(systemId)) {
            Map<String, Object> stepMap = sysConfigService.queryStepConfig(systemId,bankNum,"active");
            String[] stepIds = (String[])stepMap.get("stepIds");

            //查询单渠道
            List<ActiveCountEntity> queryList = baseMapper.queryActiveSucCountList(reqPara);
            systemList.add(systemId);
            for (ActiveCountEntity queryEntity : queryList) {
                if(stepIds[stepIds.length-1].equals(queryEntity.getStepId())){
                    dataList.add(queryEntity.getCount());
                    xNameList.add(queryEntity.getDate());
                }

            }

            //series.setStack("总量");
            series.setType(ctType);
            series.setName(systemId);
            series.setData(dataList);
            rtnList.add(series);

            graphRoot.setLegData(systemList);
            graphRoot.setXName(xNameList);
            graphRoot.setSeries(rtnList);
        } else {

            //查询所有渠道
            List<String> systemLis = baseMapper.queryActiveCountSystemLis(reqPara);  //查询渠道名称
            List<ActiveCountEntity> queryAllList = baseMapper.queryAllActiveSucCountList(reqPara);
            String[] dateArray = DateUtils.getWithinSevenDays();

            for (String system : systemLis) {

                Map<String, Object> stepMap = sysConfigService.queryStepConfig(system,bankNum,"active");
                String[] stepIds = (String[])stepMap.get("stepIds");

                systemList.add(system);
                dataList = new ArrayList<>();   //存放data
                xNameList = new ArrayList<>();   //存放date（横坐标）
                for(String date:dateArray){
                    xNameList.add(date);
                    int a = 0;
                    for (ActiveCountEntity queryAllEntity : queryAllList) {
                        if (system.equals(queryAllEntity.getSystem()) && date.equals(queryAllEntity.getDate())) {
                            if(stepIds[stepIds.length-1].equals(queryAllEntity.getStepId())){
                                dataList.add(queryAllEntity.getCount());
                                a = 1;
                                break;
                            }

                        }
                    }
                    if(a == 0){
                        dataList.add("0");
                    }
                }


                series = new GraphSeries();
                //series.setStack("总量");
                series.setType(ctType);
                series.setName(system);
                series.setData(dataList);
                rtnList.add(series);
            }

            graphRoot.setLegData(systemList);
            graphRoot.setXName(xNameList);
            graphRoot.setSeries(rtnList);
        }

        return graphRoot;

    }

    @Override
    public GraphRoot queryActiveAwayCountList(Map reqPara) {
        //激活流失趋势
        String systemId = (String) reqPara.get("systemId");      //渠道id
        String bankNum = (String) reqPara.get("bankNum");        //银行号
        String ctType = (String) reqPara.get("ctType");          //图表类型  line：折线图  bar：柱状图
        logger.info("****银行号：" + bankNum + ";渠道ID:" +systemId);

        GraphRoot graphRoot = new GraphRoot();
        GraphSeries series = new GraphSeries();
        graphRoot.setTitle("激活流失趋势");

        List<GraphSeries> rtnList = new ArrayList<>();      //组装后list
        List<String> dataList = new ArrayList<>();          //存放data
        List<String> xNameList = new ArrayList<>();         //存放date（横坐标）
        List<String> systemList = new ArrayList<>();        //系统名称

        if (StringUtils.isNotBlank(systemId)) {
            //查询单渠道
            List<ActiveCountEntity> queryList = baseMapper.queryActiveCountList(reqPara);

            //处理queryList得到每日流失人数
            List<ActiveCountEntity> awayCountList = getCalculateAwayCountList(queryList);

            systemList.add(systemId);
            for (ActiveCountEntity queryEntity : awayCountList) {
                dataList.add(queryEntity.getCount());
                xNameList.add(queryEntity.getDate());
            }

            //series.setStack("总量");
            series.setType(ctType);
            series.setName(systemId);
            series.setData(dataList);
            rtnList.add(series);

            graphRoot.setLegData(systemList);
            graphRoot.setXName(xNameList);
            graphRoot.setSeries(rtnList);
        } else {
            //查询所有渠道
            List<String> systemLis = baseMapper.queryActiveCountSystemLis(reqPara);             //查询渠道名称
            List<ActiveCountEntity> queryAllList = baseMapper.queryAllActiveCountList(reqPara); //查询全部渠道集合
            String[] dateArray = DateUtils.getWithinSevenDays();

            //处理queryAllList得到每日流失人数
            List<ActiveCountEntity> awayAllCountList = getCalculateAwayCountList(queryAllList);

            for (String system : systemLis) {
                systemList.add(system);
                dataList = new ArrayList<>();    //存放data
                xNameList = new ArrayList<>();   //存放date（横坐标）
                for(String date:dateArray){
                    xNameList.add(date);
                    int a = 0;
                    for (ActiveCountEntity queryAllEntity : awayAllCountList) {
                        if (system.equals(queryAllEntity.getSystem()) && date.equals(queryAllEntity.getDate())) {
                            dataList.add(queryAllEntity.getCount());
                            a = 1;
                            break;
                        }
                    }
                    if(a == 0){
                        dataList.add("0");
                    }
                }


                series = new GraphSeries();
                //series.setStack("总量");
                series.setType(ctType);
                series.setName(system);
                series.setData(dataList);
                rtnList.add(series);
            }

            graphRoot.setLegData(systemList);
            graphRoot.setXName(xNameList);
            graphRoot.setSeries(rtnList);
        }

        return graphRoot;
    }


    /**
     * 处理queryList得到每日流失人数
     *
     * @param queryList
     * @return
     */

    public List<ActiveCountEntity> getCalculateAwayCountList(List<ActiveCountEntity> queryList) {

        List<ActiveCountEntity> awayCountList = new ArrayList<>();  //每日流失人数集合
        ActiveCountEntity activeCountEntity = new ActiveCountEntity();

        for (int i = 0; i < queryList.size(); i++) {
            ActiveCountEntity queryEntity1 = queryList.get(i);
            Map<String, Object> stepMap = sysConfigService.queryStepConfig(queryEntity1.getSystem(),queryEntity1.getBanknum(),"active");
            String[] stepIds = (String[])stepMap.get("stepIds");

            if (stepIds[0].equals(queryEntity1.getStepId())) {
                //步骤1
                for (int j = 0; j < queryList.size(); j++) {
                    ActiveCountEntity queryEntity2 = queryList.get(j);
                    if (queryEntity1.getSystem().equals(queryEntity2.getSystem())
                            && queryEntity1.getDate().equals(queryEntity2.getDate())
                            && stepIds[stepIds.length-1].equals(queryEntity2.getStepId())) {
                        //同一天且为步骤成功步数,计算当天流失人数
                        String countStr = StrKit.SubStrByBigDecimal(queryEntity1.getCount(), queryEntity2.getCount());
                        activeCountEntity = new ActiveCountEntity();
                        activeCountEntity.setSystem(queryEntity1.getSystem());
                        activeCountEntity.setBanknum(queryEntity1.getBanknum());
                        activeCountEntity.setDate(queryEntity1.getDate());
                        activeCountEntity.setCount(countStr);    //当天流失人数
                        awayCountList.add(activeCountEntity);

                        continue;
                    }
                }
            }
        }

        return awayCountList;

    }

    @Override
    public FunnelRoot queryActiveFunnelCountList(Map reqPara) {
        //激活流失漏斗
        String systemId = (String) reqPara.get("systemId");       //渠道id
        String bankNum = (String) reqPara.get("bankNum");         //银行号
        logger.info("****银行号：" + bankNum + ";渠道ID:" + systemId);

        FunnelRoot funnelRoot = new FunnelRoot();
        FunnelSeries funnelSeries = new FunnelSeries();
        FunnelData funnelData = new FunnelData();
        funnelRoot.setTitle("激活流失漏斗");

        List<FunnelSeries> FunnelSeriesList = new ArrayList<>();           //组装后list
        List<FunnelData> FunnelDataList = new ArrayList<>();               //数据data
        List<String> legData = new ArrayList<>();                          //步骤

        Map<String, Object> stepMap = sysConfigService.queryStepConfig(systemId,bankNum,"active");
        String[] stepIds = (String[])stepMap.get("stepIds");
        String[] stepNames = (String[])stepMap.get("stepNames");

        if (StringUtils.isNotBlank(systemId)) {
        //激活流失漏斗单渠道查询
            List<ActiveCountEntity> queryFunnelList = baseMapper.queryActiveFunnelCountList(reqPara);
            legData.add(systemId);
            for(int i= 0; i<stepIds.length; i++) {
                for (ActiveCountEntity queryFunnelEntity : queryFunnelList) {
                    if(stepIds[i].equals(queryFunnelEntity.getStepId())) {
                        funnelData = new FunnelData();
                        funnelData.setName(stepNames[i]);
                        funnelData.setValue(queryFunnelEntity.getCount());
                        FunnelDataList.add(funnelData);
                        break;
                    }
                }
            }

            funnelSeries.setName(systemId);
            funnelSeries.setData(FunnelDataList);
            FunnelSeriesList.add(funnelSeries);

            funnelRoot.setLegData(legData);
            funnelRoot.setSeries(FunnelSeriesList);
            }/*else{
            //查询所有渠道
            List<String> systemLis = baseMapper.queryActiveCountSystemLis(reqPara);             //查询渠道名称
            List<ActiveCountEntity> queryAllList = baseMapper.queryAllActiveFunnelCountList(reqPara); //查询全部渠道集合
            for (String system : systemLis) {
                legData.add(system);
                FunnelDataList = new ArrayList<>();   //存放data
                funnelSeries = new FunnelSeries();
                for (ActiveCountEntity queryAllEntity : queryAllList) {
                    if (system.equals(queryAllEntity.getSystem())) {
                        funnelData = new FunnelData();
                        funnelData.setName(getStepName(queryAllEntity.getStepId()));
                        funnelData.setValue(queryAllEntity.getCount());
                        FunnelDataList.add(funnelData);
                    }
                }

                funnelSeries.setName(system);
                funnelSeries.setData(FunnelDataList);
                FunnelSeriesList.add(funnelSeries);
            }

            funnelRoot.setLegData(legData);
            funnelRoot.setSeries(FunnelSeriesList);

        }*/

        return funnelRoot;

    }


    @Override
    public Map queryCurrActiveCount(Map reqPara) {

        if("bankcoas".equals((String) reqPara.get("systemId"))){
            return null;
        }

        String systemId = (String) reqPara.get("systemId");       //渠道id
        String bankNum = (String) reqPara.get("bankNum");         //银行号

        List<ActiveCountEntity> activeCountList = baseMapper.queryCurrActiveCount(reqPara);

        Map<String, Object> rtnMap = new HashMap<>();
        String activeCount = "0";

        Map<String, Object> stepMap = sysConfigService.queryStepConfig(systemId,bankNum,"active");
        String[] stepIds = (String[])stepMap.get("stepIds");

        if(activeCountList!=null && activeCountList.size()>0){
            for (ActiveCountEntity activeCountEntity:activeCountList) {
                if(stepIds[stepIds.length-1].equals(activeCountEntity.getStepId())){
                    activeCount = activeCountEntity.getCount() != null ? activeCountEntity.getCount() : "0";
                    break;
                }
            }
        }
        rtnMap.put("activeCount",activeCount);
        return rtnMap;
    }

}