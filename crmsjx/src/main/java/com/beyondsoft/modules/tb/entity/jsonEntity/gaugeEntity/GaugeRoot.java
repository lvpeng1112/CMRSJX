package com.beyondsoft.modules.tb.entity.jsonEntity.gaugeEntity;

import lombok.Data;

import java.util.List;

/**
 * 仪表盘模型
 */
@Data
public class GaugeRoot {

    private List<GaugeSeries> series;
}
