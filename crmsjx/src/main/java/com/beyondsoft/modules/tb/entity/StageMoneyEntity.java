package com.beyondsoft.modules.tb.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import lombok.Data;

/**
 * 每日分期金额
 * 
 * @author wanchangweng
 * @email 
 * @date 2019-07-12 09:52:42
 */
@Data
@TableName("tb_stage_money")
public class StageMoneyEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 系统标识
	 */
	@TableId
	private String system;
	/**
	 * 银行号
	 */
	private String banknum;
	/**
	 * 导入日期
	 */
	private String date;
	/**
	 * 分期类型
	 */
	private String stageType;
	/**
	 * 当日分期金额
	 */
	private String money;

}
