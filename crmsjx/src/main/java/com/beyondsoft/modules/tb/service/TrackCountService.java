package com.beyondsoft.modules.tb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.modules.tb.entity.StageTrackEntity;
import com.beyondsoft.common.utils.R;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * stageTrack
 *
 * @author hlwang
 * @email
 * @date 2019-07-12 09:52:42
 */
public interface TrackCountService extends IService<StageTrackEntity> {

    /**
     * stageTrack报表下载
     */
    R trackReportDown(String bankNum, String startTime, String endTime, HttpServletResponse response);


    R saveOrUpdate(Map reqPara);

}

