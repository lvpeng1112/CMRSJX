package com.beyondsoft.modules.tb.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.tb.entity.StageTrackEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author hlwang
 * @email
 * @date 2020-04-12 09:52:42
 */
@Mapper
public interface StageTrackDao extends BaseMapper<StageTrackEntity> {


    List<StageTrackEntity> queryStageTrackCountList(Map reqPara);


    void saveOrUpdate(Map reqPara);
}
