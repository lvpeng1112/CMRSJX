package com.beyondsoft.modules.tb.service.impl;

import com.beyondsoft.modules.sys.dao.SysConfigDao;
import com.beyondsoft.modules.sys.entity.SysConfigEntity;
import com.beyondsoft.modules.tb.dao.ApplyCountDao;
import com.beyondsoft.modules.tb.entity.ApplyCountEntity;
import com.beyondsoft.modules.tb.service.ExternalCountService;
import com.beyondsoft.cache.PropCache;
import com.beyondsoft.common.utils.DateKit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service("externalCountService")
public class ExternalCountServiceImpl implements ExternalCountService {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private ApplyCountDao applyCountDao;

    @Autowired(required = false)
    private SysConfigDao sysConfigDao;


    @Override
    public Map queryApplyCountList(Map reqPara) {

        Map<String, Object> rtnMap = new HashMap<>();

        List<ApplyCountEntity> applyCountList = applyCountDao.queryCurrApplyCountList(reqPara);

        rtnMap.put("applyCountList",applyCountList);

        return rtnMap;
    }

    @Override
    public Map flushProp(Map reqPara) {

        Map<String, Object> rtnMap = new HashMap<>();
        try{

            logger.info("++++++加载参数开始 start time" + DateKit.getCurrDate(DateKit.yyyyMMddHHmmss));
            long start = System.currentTimeMillis();

            List<SysConfigEntity> SysConfigEntityList = sysConfigDao.queryParamKeyList();

            for(int i=0; i< SysConfigEntityList.size(); i++){
                SysConfigEntity prop = (SysConfigEntity)SysConfigEntityList.get(i);
                PropCache.putCache(prop.getParamKey(),
                        prop.getParamValue());
            }
            long end = System.currentTimeMillis();
            logger.info("加载参数缓存数：" + PropCache.getCacheSize() + " 耗时：" + (end - start) + "ms");

        }catch(Exception e){
            logger.info("刷新参数缓存出错",e);

            rtnMap.put("message",e.getMessage());
        }
        return rtnMap;
    }


}