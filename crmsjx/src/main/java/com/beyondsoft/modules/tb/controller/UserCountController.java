package com.beyondsoft.modules.tb.controller;

import java.util.HashMap;
import java.util.Map;

import com.beyondsoft.common.utils.DateKit;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.common.utils.*;
import com.beyondsoft.modules.tb.entity.jsonEntity.gaugeEntity.GaugeRoot;
import com.beyondsoft.modules.tb.entity.jsonEntity.graphEntity.GraphRoot;
import com.beyondsoft.modules.tb.entity.jsonEntity.motionEntity.MotionRoot;
import com.beyondsoft.modules.tb.entity.jsonEntity.pieEntity.PieRoot;
import com.beyondsoft.modules.tb.service.ActiveCountService;
import com.beyondsoft.modules.tb.service.StageCountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.beyondsoft.modules.tb.service.UserCountService;


/**
 * tb_user_count
 *
 * @author wanchangweng
 * @email
 * @date 2019-07-18 15:11:03
 */
@RestController
@RequestMapping("tb/userCount")
public class UserCountController {
    @Autowired
    private UserCountService userCountService;

    @Autowired
    private ActiveCountService activeCountService;

    @Autowired
    private StageCountService stageCountService;



    /**
     * 根据系统，查询每日PV
     * 上送systemId不为：null,空时，查询单一渠道渠道，否则查询所有渠道
     */
    @RequestMapping("/queryPvList")
    public R queryPvList(@RequestParam Map<String, Object> reqPara) {


        /*public R queryPvList() {
            Map reqPara = new HashMap<String, String>();
            reqPara.put("bankNum","0461");
            //reqPara.put("systemId","weixin");
            reqPara.put("ctType","line");*/

        //查询数据并处理
        GraphRoot graphRoot = userCountService.queryPvList(reqPara);

        return R.ok().put("rtnMap", graphRoot);
    }


    /**
     * 根据系统，查询每日UV
     * 上送systemId不为：null,空时，查询单一渠道渠道，否则查询所有渠道
     */
    @RequestMapping("/queryUvList")
    public R queryUvList(@RequestParam Map<String, Object> reqPara) {

        //查询数据并处理
        GraphRoot graphRoot = userCountService.queryUvList(reqPara);

        return R.ok().put("rtnMap", graphRoot);
    }

    /**
     * 根据系统，查询每日用户增量
     * 上送systemId，bankId
     * 上送systemId不为：null,空时，查询单一渠道渠道，否则查询所有渠道
     */
    @RequestMapping("/queryUserList")
    public R queryUserList(@RequestParam Map<String, Object> reqPara) {

        //查询数据并处理
        GraphRoot graphRoot = userCountService.queryUserList(reqPara);

        return R.ok().put("rtnMap", graphRoot);
    }


    /**
     * 根据银行号查询新增用户分布，并通过系统分组求和  饼图展示
     */
    @RequestMapping("/queryNewUserList")
    public R queryNewUserList(@RequestParam Map<String, Object> reqPara) {

        //查询数据并处理
        PieRoot pieRoot = userCountService.queryNewUserList(reqPara);
        return R.ok().put("rtnMap", pieRoot);
    }


    /**
     * 统计实时流量
     */
    @RequestMapping("/querySpeedList")
    public R querySpeedList(@RequestParam Map<String, Object> reqPara) {

        //查询数据并处理
        GaugeRoot gaugeRoot = userCountService.querySpeedList(reqPara);
        return R.ok().put("rtnMap", gaugeRoot);
    }


    /**
     * 昨日pv，uv，激活，分期数据统计  面板展示
     */
    @RequestMapping("/queryAllCountList")
    public R queryAllCountList(@RequestParam Map<String, Object> reqPara) {


        //按系统，银行，日期查询 pv、uv、user
        Map userMap = userCountService.queryCurrUserCount(reqPara);

        //按系统，银行，日期查询分期笔数
        Map stageMap = stageCountService.queryCurrStageCount(reqPara);

        //按系统，银行，日期查询激活数量
        Map activeMap = activeCountService.queryCurrActiveCount(reqPara);

        Map<String, Object> rtnMap = new HashMap<>();
        rtnMap.put("userMap",userMap);
        rtnMap.put("stageMap",stageMap);
        rtnMap.put("activeMap",activeMap);
        return R.ok().put("rtnMap", rtnMap);
    }

    /**
     * 当日实时pv，uv，激活，分期数据统计  面板展示
     */
    @RequestMapping("/queryPresentCount")
    public R queryPresentCount(@RequestParam Map<String, Object> reqPara) {

        //查询数据并处理
        Map rtnMap = userCountService.queryPresentCount(reqPara);

        return R.ok().put("rtnMap", rtnMap);
    }

    /**
     * 查询所有渠道访问量动态折线图
     */
    @RequestMapping("/queryPVDynamicCount")
    public R queryPVDynamicCount(@RequestParam Map<String, Object> reqPara) {

        //查询数据并处理
        MotionRoot motionRoot = userCountService.queryPVDynamicCount(reqPara);

        return R.ok().put("rtnMap", motionRoot);
    }

    /**
     * 查询昨日不同系统PV量，饼图展示
     */
    @RequestMapping("/queryYesterdayPVCount")
    public R queryYesterdayPVCount(@RequestParam Map<String, Object> reqPara) {
        //获取昨天日期
        String date = DateKit.getNextDayFate("yyyyMMdd",-1);
        reqPara.put("date", date);

        //查询数据并处理
        PieRoot pieRoot = userCountService.queryYesterdayPVCount(reqPara);

        return R.ok().put("rtnMap", pieRoot);
    }

}
