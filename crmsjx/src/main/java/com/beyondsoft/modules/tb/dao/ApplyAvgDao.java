package com.beyondsoft.modules.tb.dao;

import com.beyondsoft.modules.tb.entity.ApplyAvgEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.*;

/**
 * 元素平均停留时间表
 * 
 * @author wanchangweng
 * @email 
 * @date 2019-11-12 16:34:08
 */
@Mapper
public interface ApplyAvgDao extends BaseMapper<ApplyAvgEntity> {

    List<ApplyAvgEntity> queryApplyAvgCountList(Map reqPara);
	
}
