package com.beyondsoft.modules.tb.service.impl;

import com.beyondsoft.common.utils.*;
import com.beyondsoft.modules.tb.dao.UserCountDao;
import com.beyondsoft.modules.tb.entity.UserCountEntity;
import com.beyondsoft.modules.tb.entity.jsonEntity.gaugeEntity.GaugeData;
import com.beyondsoft.modules.tb.entity.jsonEntity.gaugeEntity.GaugeDetail;
import com.beyondsoft.modules.tb.entity.jsonEntity.gaugeEntity.GaugeRoot;
import com.beyondsoft.modules.tb.entity.jsonEntity.gaugeEntity.GaugeSeries;
import com.beyondsoft.modules.tb.entity.jsonEntity.graphEntity.GraphRoot;
import com.beyondsoft.modules.tb.entity.jsonEntity.graphEntity.GraphSeries;
import com.beyondsoft.modules.tb.entity.jsonEntity.motionEntity.MotionRoot;
import com.beyondsoft.modules.tb.entity.jsonEntity.motionEntity.MotionSeries;
import com.beyondsoft.modules.tb.entity.jsonEntity.pieEntity.PieData;
import com.beyondsoft.modules.tb.entity.jsonEntity.pieEntity.PieRoot;
import com.beyondsoft.modules.tb.entity.jsonEntity.pieEntity.PieSeries;
import com.beyondsoft.modules.tb.service.UserCountService;
import com.beyondsoft.common.utils.*;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


@Service("userCountService")
public class UserCountServiceImpl extends ServiceImpl<UserCountDao, UserCountEntity> implements UserCountService {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private RedisUtils redisUtils;


    @Override
    public GraphRoot queryPvList(Map reqPara) {
        //查询用户PV
        String systemId = (String) reqPara.get("systemId");      //渠道id
        String bankNum = (String) reqPara.get("bankNum");        //银行号
        String ctType = (String) reqPara.get("ctType");          //图表类型  line：折线图  bar：柱状图
        logger.info("****银行号："+bankNum+";渠道ID:"+systemId);

        GraphRoot graphRoot = new GraphRoot();
        GraphSeries series = new GraphSeries();
        graphRoot.setTitle("PV");

        List<GraphSeries> rtnList = new ArrayList<>();      //组装后list
        List<String> dataList = new ArrayList<>();          //存放data
        List<String> xNameList = new ArrayList<>();         //存放date（横坐标）
        List<String> systemList = new ArrayList<>();        //系统名称
        String[] dateArray = DateUtils.getWithinSevenDays();

        if (StringUtils.isNotBlank(systemId)) {
            //查询单渠道
            List<UserCountEntity> queryList = baseMapper.queryUserCountList(reqPara);
            systemList.add(systemId);
            for(String date:dateArray){
                xNameList.add(date);
                int a = 0;
                for (UserCountEntity queryEntity : queryList) {
                    if (date.equals(queryEntity.getDate())) {
                        dataList.add(queryEntity.getPv());
                        a = 1;
                        break;
                    }
                }
                if(a == 0){
                    dataList.add("0");
                }
            }


            //series.setStack("总量");
            series.setType(ctType);
            series.setName(systemId);
            series.setData(dataList);
            rtnList.add(series);

            graphRoot.setLegData(systemList);
            graphRoot.setXName(xNameList);
            graphRoot.setSeries(rtnList);
        } else {
            //查询所有渠道
            List<String> systemLis = baseMapper.queryUserCountSystemLis(reqPara);           //查询渠道名称
            List<UserCountEntity> queryAllList = baseMapper.queryAllUserCountList(reqPara); //查询全部渠道集合
            for (String system : systemLis) {
                systemList.add(system);
                dataList = new ArrayList<>();    //存放data
                xNameList = new ArrayList<>();   //存放date（横坐标）
                for(String date:dateArray){
                    xNameList.add(date);
                    int a = 0;
                    for (UserCountEntity queryAllEntity : queryAllList) {
                        if (system.equals(queryAllEntity.getSystem()) && date.equals(queryAllEntity.getDate())) {
                                dataList.add(queryAllEntity.getPv());
                                a = 1;
                                break;
                            }
                        }
                        if(a == 0){
                            dataList.add("0");
                        }
                }

                series = new GraphSeries();
                //series.setStack("总量");
                series.setType(ctType);
                series.setName(system);
                series.setData(dataList);
                rtnList.add(series);
            }

            graphRoot.setLegData(systemList);
            graphRoot.setXName(xNameList);
            graphRoot.setSeries(rtnList);
        }

        return graphRoot;
    }


    @Override
    public GraphRoot queryUvList(Map reqPara) {
        //查询用户UV
        String systemId = (String) reqPara.get("systemId");      //渠道id
        String bankNum = (String) reqPara.get("bankNum");        //银行号
        String ctType = (String) reqPara.get("ctType");          //图表类型  line：折线图  bar：柱状图
        logger.info("****银行号："+bankNum+";渠道ID:"+systemId);

        GraphRoot graphRoot = new GraphRoot();
        GraphSeries series = new GraphSeries();
        graphRoot.setTitle("UV");

        List<GraphSeries> rtnList = new ArrayList<>();      //组装后list
        List<String> dataList = new ArrayList<>();          //存放data
        List<String> xNameList = new ArrayList<>();         //存放date（横坐标）
        List<String> systemList = new ArrayList<>();        //系统名称

        if (StringUtils.isNotBlank(systemId)) {
            //查询单渠道
            List<UserCountEntity> queryList = baseMapper.queryUserCountList(reqPara);
            systemList.add(systemId);
            for (UserCountEntity queryEntity : queryList) {
                dataList.add(queryEntity.getUv());
                xNameList.add(queryEntity.getDate());
            }

            //series.setStack("总量");
            series.setType(ctType);
            series.setName(systemId);
            series.setData(dataList);
            rtnList.add(series);

            graphRoot.setLegData(systemList);
            graphRoot.setXName(xNameList);
            graphRoot.setSeries(rtnList);
        } else {
            //查询所有渠道
            List<String> systemLis = baseMapper.queryUserCountSystemLis(reqPara);           //查询渠道名称
            List<UserCountEntity> queryAllList = baseMapper.queryAllUserCountList(reqPara); //查询全部渠道集合
            String[] dateArray = DateUtils.getWithinSevenDays();
            for (String system : systemLis) {
                systemList.add(system);
                dataList = new ArrayList<>();    //存放data
                xNameList = new ArrayList<>();   //存放date（横坐标）

                for(String date:dateArray){
                    xNameList.add(date);
                    int a = 0;
                    for (UserCountEntity queryAllEntity : queryAllList) {
                        if (system.equals(queryAllEntity.getSystem()) && date.equals(queryAllEntity.getDate())) {
                            dataList.add(queryAllEntity.getUv());
                            a = 1;
                            break;
                        }
                    }
                    if(a == 0){
                        dataList.add("0");
                    }

                }

                series = new GraphSeries();
                //series.setStack("总量");
                series.setType(ctType);
                series.setName(system);
                series.setData(dataList);
                rtnList.add(series);
            }

            graphRoot.setLegData(systemList);
            graphRoot.setXName(xNameList);
            graphRoot.setSeries(rtnList);
        }

        return graphRoot;
    }

    @Override
    public GraphRoot queryUserList(Map reqPara) {
        //查询用户User增量
        String systemId = (String) reqPara.get("systemId");      //渠道id
        String bankNum = (String) reqPara.get("bankNum");        //银行号
        String ctType = (String) reqPara.get("ctType");          //图表类型  line：折线图  bar：柱状图
        logger.info("****银行号："+bankNum+";渠道ID:"+systemId);

        GraphRoot graphRoot = new GraphRoot();
        GraphSeries series = new GraphSeries();
        graphRoot.setTitle("用户增量");

        List<GraphSeries> rtnList = new ArrayList<>();      //组装后list
        List<String> dataList = new ArrayList<>();          //存放data
        List<String> xNameList = new ArrayList<>();         //存放date（横坐标）
        List<String> systemList = new ArrayList<>();        //系统名称

        if (StringUtils.isNotBlank(systemId)) {
            //查询单渠道
            List<UserCountEntity> queryList = baseMapper.queryUserCountList(reqPara);
            systemList.add(systemId);
            for (UserCountEntity queryEntity : queryList) {
                xNameList.add(queryEntity.getDate());
                //计算用户增量
                reqPara.put("date", DateKit.calcDay(queryEntity.getDate(),-1));
                String userIncCount = queryUserIncCount(queryEntity.getUser(),reqPara);
                dataList.add(userIncCount);

            }

            //series.setStack("总量");
            series.setType(ctType);
            series.setName(systemId);
            series.setData(dataList);
            rtnList.add(series);

            graphRoot.setLegData(systemList);
            graphRoot.setXName(xNameList);
            graphRoot.setSeries(rtnList);
        } else {
            //查询所有渠道
            List<String> systemLis = baseMapper.queryUserCountSystemLis(reqPara);           //查询渠道名称
            List<UserCountEntity> queryAllList = baseMapper.queryAllUserCountList(reqPara); //查询全部渠道集合
            String[] dateArray = DateUtils.getWithinSevenDays();
            for (String system : systemLis) {
                systemList.add(system);
                dataList = new ArrayList<>();    //存放data
                xNameList = new ArrayList<>();   //存放date（横坐标）

                for(String date:dateArray){
                    xNameList.add(date);
                    int a = 0;
                    for (UserCountEntity queryAllEntity : queryAllList) {
                        if (system.equals(queryAllEntity.getSystem()) && date.equals(queryAllEntity.getDate())) {
                            //计算用户增量
                            reqPara.put("date",DateKit.calcDay(queryAllEntity.getDate(),-1));
                            reqPara.put("systemId",system);
                            String userIncCount = queryUserIncCount(queryAllEntity.getUser(),reqPara);
                            dataList.add(userIncCount);
                            a = 1;
                            break;
                        }
                    }
                    if(a == 0){
                        dataList.add("0");
                    }
                }

                series = new GraphSeries();
                //series.setStack("总量");
                series.setType(ctType);
                series.setName(system);
                series.setData(dataList);
                rtnList.add(series);
            }

            graphRoot.setLegData(systemList);
            graphRoot.setXName(xNameList);
            graphRoot.setSeries(rtnList);
        }

        return graphRoot;
    }

    @Override
    public PieRoot queryNewUserList(Map reqPara) {

        String bankNum = (String) reqPara.get("bankNum");            //银行号
        logger.info("****银行号："+bankNum);

        PieRoot pieRoot =  new PieRoot();
        PieSeries pieSeries = new PieSeries();
        PieData pieData = new PieData();
        pieRoot.setTitle("新增用户分布");

        List<PieSeries> pieSeriesList = new ArrayList<>();           //组装后list
        List<PieData> pieDataList = new ArrayList<>();               //数据data
        List<String> legData = new ArrayList<>();                    //系统名称

        //查出分组数据 近30日用户增量
        List<UserCountEntity> querySumList = baseMapper.queryUserIncLis(reqPara);
        for (UserCountEntity querySumEntity:querySumList) {
            legData.add(querySumEntity.getSystem());
            pieData = new PieData();
            pieData.setName(querySumEntity.getSystem());
            pieData.setValue(querySumEntity.getUser());
            pieDataList.add(pieData);
        }

        pieSeries.setName("访问来源");
        pieSeries.setData(pieDataList);
        pieSeriesList.add(pieSeries);

        pieRoot.setLegData(legData);
        pieRoot.setSeries(pieSeriesList);

        return pieRoot;

    }

    @Override
    public GaugeRoot querySpeedList(Map reqPara) {
        //每秒访问实时流量
        String systemId = (String) reqPara.get("systemId");      //渠道id
        logger.info("****系统Id："+systemId);

        GaugeRoot gaugeRoot = new GaugeRoot();
        GaugeSeries gaugeSeries = new GaugeSeries();

        List<GaugeSeries> gaugeSeriesList = new ArrayList<>();           //组装后list
        List<GaugeData> gaugeDataList = new ArrayList<>();               //数据data

        Date de = new Date(System.currentTimeMillis()-1000);             //获取系统时间的前一秒
        String newDateTime = DateKit.getFormaterDate("yyyyMMddHHmmss",de);
        String key = RedisKeys.getBeehiveMonitorKey(systemId+newDateTime);

        String value = redisUtils.get(key);                              //实时流量
        if(value == null){
            value = "0";
        }

        GaugeData gaugeData = new GaugeData();
        gaugeData.setName(systemId+"实时流量");
        gaugeData.setValue(value);
        gaugeDataList.add(gaugeData);

        GaugeDetail gaugeDetail = new GaugeDetail();
        gaugeDetail.setFormatter(value);

        gaugeSeries.setData(gaugeDataList);
        gaugeSeries.setDetail(gaugeDetail);
        gaugeSeries.setName("实时流量");

        gaugeSeriesList.add(gaugeSeries);
        gaugeRoot.setSeries(gaugeSeriesList);

        return gaugeRoot;
    }

    @Override
    public Map queryCurrUserCount(Map reqPara) {

        String date = (String) reqPara.get("date");              //日期

        String pvCount = "0";
        String uvCount = "0";
        String userCount = "0";
        String userIncCount = "0";    //用户增量
        Map<String, Object> rtnMap = new HashMap<>();

        List<UserCountEntity> userCountList = baseMapper.queryCurrUserCount(reqPara);
         if(userCountList!=null && userCountList.size()>0){
             UserCountEntity userCountEntity = userCountList.get(0);
             pvCount = userCountEntity.getPv() != null ? userCountEntity.getPv() : "0";
             uvCount = userCountEntity.getUv() != null ? userCountEntity.getUv() : "0";
             userCount = userCountEntity.getUser() != null ? userCountEntity.getUser() : "0";

             //计算用户增量
             Map reqPara1 = new HashMap<String, String>();
             reqPara1.put("systemId",(String) reqPara.get("systemId"));
             reqPara1.put("bankNum",(String) reqPara.get("bankNum"));
             reqPara1.put("date", DateKit.calcDay(date,-1));
             userIncCount = queryUserIncCount(userCount,reqPara1);
         }
        rtnMap.put("pvCount",pvCount);
        rtnMap.put("uvCount",uvCount);
        rtnMap.put("userCount",userCount);
        rtnMap.put("userIncCount",userIncCount);
        return rtnMap;
    }

    public String queryUserIncCount(String userCount, Map reqPara){
        String userIncCount = "0";
        List<UserCountEntity> userIncCountList = baseMapper.queryCurrUserCount(reqPara);
        if(userIncCountList!=null && userIncCountList.size()>0){
            UserCountEntity userIncCountEntity = userIncCountList.get(0);
            String userCount1 = userIncCountEntity.getUser() != null ? userIncCountEntity.getUser() : "0";
            userIncCount = StrKit.SubStrByBigDecimal(userCount, userCount1);
        }

        return userIncCount;
    }

    @Override
    public Map queryPresentCount(Map reqPara) {
        //查询当前实时监控数据
        Map<String, Object> rtnMap = new HashMap<>();

        String bankNum = (String) reqPara.get("bankNum");            //银行号
        logger.info("****银行号："+bankNum);
        String systemId = (String) reqPara.get("systemId");      //渠道id
        logger.info("****系统Id："+systemId);

        String currDate = DateKit.getCurrDate("yyyyMMdd");

        //按银行统计当天PV量
        String key1 = RedisKeys.getBeehiveMonitorPvKey(bankNum + ":" + currDate);
        String bankPv = redisUtils.get(key1);

        //按银行，系统统计当天PV量
        String sysKey1 = RedisKeys.getBeehiveMonitorPvKey(bankNum +":"+ systemId + ":" + currDate);
        String bankSysPv = redisUtils.get(sysKey1);

        //按银行统计当天UV量
        String key2 = RedisKeys.getBeehiveMonitorUvKey(bankNum + ":" + currDate);
        String bankUv = redisUtils.get(key2);

        //按银行，系统统计当天UV量
        String sysKey2 = RedisKeys.getBeehiveMonitorUvKey(bankNum +":"+ systemId +  ":" + currDate);
        String bankSysUv = redisUtils.get(sysKey2);

        //按银行统计当天激活成功笔数
        String key3 = RedisKeys.getBeehiveMonitorActiveKey(bankNum + ":" + currDate);
        String bankActive = redisUtils.get(key3);

        //按银行统计当天激活完成第一步笔数
        String step1Key3 = RedisKeys.getBeehiveMonitorActiveKey(bankNum + ":step1" + currDate);
        String bankStep1Active = redisUtils.get(step1Key3);

        //按银行，系统统计当天激活成功笔数
        String sysKey3 = RedisKeys.getBeehiveMonitorActiveKey(bankNum +":"+ systemId + ":" + currDate);
        String bankSysActive = redisUtils.get(sysKey3);

        //按银行统计当天分期成功笔数
        String key4 = RedisKeys.getBeehiveMonitorStageKey(bankNum + ":" + currDate);
        String bankStage = redisUtils.get(key4);

        //按银行统计当天分期完成第一步笔数
        String step1Key4 = RedisKeys.getBeehiveMonitorStageKey(bankNum + ":step1" + currDate);
        String bankStep1Stage = redisUtils.get(step1Key4);

        //按银行，系统统计当天分期成功笔数
        String sysKey4 = RedisKeys.getBeehiveMonitorStageKey(bankNum +":"+ systemId + ":" + currDate);
        String bankSysStage = redisUtils.get(sysKey4);

        //分期流失笔数
        bankStage = bankStage != null ? bankStage : "0";
        bankStep1Stage = bankStep1Stage != null ? bankStep1Stage : "0";
        String stageAway = StrKit.SubStrByBigDecimal(bankStep1Stage,bankStage);

        rtnMap.put("bankPv", bankPv != null ? bankPv : "0");
        rtnMap.put("bankSysPv", bankSysPv != null ? bankSysPv : "0");
        rtnMap.put("bankUv", bankUv != null ? bankUv : "0");
        rtnMap.put("bankSysUv", bankSysUv != null ? bankSysUv : "0");
        rtnMap.put("bankActive", bankActive != null ? bankActive : "0");
        rtnMap.put("bankSysActive", bankSysActive != null ? bankSysActive : "0");
        rtnMap.put("bankStage", bankStage != null ? bankStage : "0");
        rtnMap.put("bankSysStage", bankSysStage != null ? bankSysStage : "0");
        rtnMap.put("stageAway",stageAway);

        return rtnMap;
    }

    @Override
    public MotionRoot queryPVDynamicCount(Map reqPara) {
        //查询所有渠道访问量动态折线图
        String bankNum = (String) reqPara.get("bankNum");            //银行号
        logger.info("****银行号："+bankNum);
        String systemId = (String) reqPara.get("systemId");          //渠道id
        logger.info("****系统Id："+systemId);

        String currDate = DateKit.getCurrDate("yyyyMMdd");
        String currDate1 = DateKit.getCurrDate("yyyy-MM-dd HH:mm:ss");

        //按银行，系统统计当天PV量
        String sysKey = RedisKeys.getBeehiveMonitorPvKey(bankNum +":"+ systemId + ":" + currDate);
        String bankSysPv = redisUtils.get(sysKey);

        MotionRoot motionRoot = new MotionRoot();
        MotionSeries series = new MotionSeries();

        List<String> dataList = new ArrayList<>();
        List<MotionSeries> seriesList = new ArrayList<>();

        dataList.add(currDate1);
        dataList.add(bankSysPv != null ? bankSysPv : "0");
        series.setData(dataList);
        series.setName(systemId);
        seriesList.add(series);
        motionRoot.setTitle("动态折线图");
        motionRoot.setSeries(seriesList);

        return motionRoot;
    }

    @Override
    public PieRoot queryYesterdayPVCount(Map reqPara) {
        //查询昨日不同系统PV量，饼图展示
        String bankNum = (String) reqPara.get("bankNum");            //银行号
        logger.info("****银行号："+bankNum);

        PieRoot pieRoot =  new PieRoot();
        PieSeries pieSeries = new PieSeries();
        PieData pieData = new PieData();
        pieRoot.setTitle("PV量分布");

        List<PieSeries> pieSeriesList = new ArrayList<>();           //组装后list
        List<PieData> pieDataList = new ArrayList<>();               //数据data
        List<String> legData = new ArrayList<>();                    //系统名称

        //查出分组数据 近30日用户增量
        List<UserCountEntity> querySumList = baseMapper.queryYesterdayPVCount(reqPara);
        for (UserCountEntity querySumEntity:querySumList) {
            legData.add(querySumEntity.getSystem());
            pieData = new PieData();
            pieData.setName(querySumEntity.getSystem());
            pieData.setValue(querySumEntity.getPv());
            pieDataList.add(pieData);
        }

        pieSeries.setName("系统来源");
        pieSeries.setData(pieDataList);
        pieSeriesList.add(pieSeries);

        pieRoot.setLegData(legData);
        pieRoot.setSeries(pieSeriesList);

        return pieRoot;
    }


}