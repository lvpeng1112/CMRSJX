package com.beyondsoft.modules.tb.entity.jsonEntity.funnelEntity;

import lombok.Data;

import java.util.List;

@Data
public class FunnelSeries {

    private String name;

    private String type = "funnel";

    private String width ="40%";

    private List<FunnelData> data;
}
