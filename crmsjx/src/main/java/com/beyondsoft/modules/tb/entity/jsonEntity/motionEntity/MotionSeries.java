package com.beyondsoft.modules.tb.entity.jsonEntity.motionEntity;

import lombok.Data;

import java.util.List;

@Data
public class MotionSeries {

    //数据类别/名称
    private String name;

    //时间 Mon Jul 24 2000 00:00:00 GMT+0800 (中国标准时间)
    private String time;

    //数据集合
    private List<String> data;

}
