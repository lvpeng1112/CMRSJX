package com.beyondsoft.modules.tb.entity.jsonEntity.gaugeEntity;

import lombok.Data;

@Data
public class GaugeData {

    private String name;

    private String value;
}
