package com.beyondsoft.modules.tb.dao;

import com.beyondsoft.modules.tb.entity.ActiveCountEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 近30日激活人数
 * 
 * @author wanchangweng
 * @email 
 * @date 2019-07-12 09:52:42
 */
@Mapper
public interface ActiveCountDao extends BaseMapper<ActiveCountEntity> {

    List<ActiveCountEntity> queryAllActiveCountList(Map reqPara);

    List<ActiveCountEntity> queryActiveCountList(Map reqPara);

    List<ActiveCountEntity> queryAllActiveSucCountList(Map reqPara);

    List<ActiveCountEntity> queryActiveSucCountList(Map reqPara);

    List<String> queryActiveCountSystemLis(Map reqPara);

    List<ActiveCountEntity> queryActiveFunnelCountList(Map reqPara);

    List<ActiveCountEntity> queryCurrActiveCount(Map reqPara);

    List<ActiveCountEntity> queryAllActiveFunnelCountList(Map reqPara);

}
