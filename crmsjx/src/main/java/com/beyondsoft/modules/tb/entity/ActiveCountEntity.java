package com.beyondsoft.modules.tb.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import lombok.Data;

/**
 * 近30日激活人数
 * 
 * @author wanchangweng
 * @email 
 * @date 2019-07-12 09:52:42
 */
@Data
@TableName("tb_active_count")
public class ActiveCountEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 系统标识
	 */
	@TableId
	private String system;
	/**
	 * 银行号
	 */
	private String banknum;
	/**
	 * 导入日期
	 */
	private String date;
	/**
	 * 页面Id
	 */
	private String pageId;
	/**
	 * 步骤标识
	 */
	private String stepId;
	/**
	 * 激活访问人数
	 */
	private String count;

}
