package com.beyondsoft.modules.tb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.modules.tb.entity.ApplyCountEntity;
import com.beyondsoft.modules.tb.entity.jsonEntity.funnelEntity.FunnelRoot;
import com.beyondsoft.modules.tb.entity.jsonEntity.graphEntity.GraphRoot;
import com.beyondsoft.modules.tb.entity.jsonEntity.pieEntity.PieRoot;

import java.util.Map;

/**
 * 网申数量
 *
 * @author wanchangweng
 * @email 
 * @date 2019-11-12 16:34:08
 */
public interface ApplyCountService extends IService<ApplyCountEntity> {

    /**
     *  查询昨日网申用户访问量分时统计 饼图展示
     * @param reqPara
     * @return
     */
    PieRoot queryApplyTimeCount(Map reqPara);


    /**
     * 网申流失漏斗查询  漏斗图
     * @param reqPara
     * @return
     */
    FunnelRoot queryApplyFunnelCountList(Map reqPara);


    /**
     * 网申元素停留时间排行榜，用于柱状图，折线图展示
     * @param reqPara
     * @return
     */
    GraphRoot queryApplyAvgCountList(Map reqPara);


    /**
     * 网申流失趋势查询，用于柱状图，折线图展示
     * @param reqPara
     * @return
     */
    GraphRoot queryApplyAwayCountList(Map reqPara);

    /**
     * 按系统，银行，日期网申进件，网申流失量 面板展示
     * @param reqPara
     * @return
     */
    public Map  queryCurrApplyCountList(Map reqPara);


}

