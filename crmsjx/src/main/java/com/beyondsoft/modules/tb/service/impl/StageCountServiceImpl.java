package com.beyondsoft.modules.tb.service.impl;

import com.beyondsoft.modules.sys.service.SysConfigService;
import com.beyondsoft.modules.tb.dao.StageCountDao;
import com.beyondsoft.modules.tb.dao.StageMoneyDao;
import com.beyondsoft.modules.tb.dao.StageTimeDao;
import com.beyondsoft.modules.tb.entity.StageCountEntity;
import com.beyondsoft.modules.tb.entity.StageMoneyEntity;
import com.beyondsoft.modules.tb.entity.StageTimeEntity;
import com.beyondsoft.modules.tb.entity.jsonEntity.funnelEntity.FunnelData;
import com.beyondsoft.modules.tb.entity.jsonEntity.funnelEntity.FunnelRoot;
import com.beyondsoft.modules.tb.entity.jsonEntity.funnelEntity.FunnelSeries;
import com.beyondsoft.modules.tb.entity.jsonEntity.graphEntity.GraphRoot;
import com.beyondsoft.modules.tb.entity.jsonEntity.graphEntity.GraphSeries;
import com.beyondsoft.modules.tb.entity.jsonEntity.pieEntity.PieData;
import com.beyondsoft.modules.tb.entity.jsonEntity.pieEntity.PieRoot;
import com.beyondsoft.modules.tb.entity.jsonEntity.pieEntity.PieSeries;
import com.beyondsoft.modules.tb.service.StageCountService;
import com.beyondsoft.common.utils.DateUtils;
import com.beyondsoft.common.utils.StrKit;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


@Service("stageCountService")
public class StageCountServiceImpl extends ServiceImpl<StageCountDao, StageCountEntity> implements StageCountService {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private StageMoneyDao stageMoneyDao;

    @Autowired(required = false)
    private StageTimeDao stageTimeDao;

    @Autowired
    private SysConfigService sysConfigService;

    @Override
    public GraphRoot queryStageCountList(Map reqPara) {
        //分期笔数趋势
        String systemId = (String) reqPara.get("systemId");          //渠道id
        String bankNum = (String) reqPara.get("bankNum");            //银行号
        String stageType = (String) reqPara.get("stageType");        //分期类型
        String ctType = (String) reqPara.get("ctType");              //图表类型  line：折线图  bar：柱状图
        logger.info("****银行号：" + bankNum + ";渠道ID:" + systemId + "分期类型：" + stageType);

        GraphRoot graphRoot = new GraphRoot();
        GraphSeries series = new GraphSeries();
        graphRoot.setTitle(StrKit.stagesTranslate(stageType) + "每日分期笔数");

        List<GraphSeries> rtnList = new ArrayList<>();                //组装后list
        List<String> dataList = new ArrayList<>();                    //存放data
        List<String> xNameList = new ArrayList<>();                   //存放date（横坐标）
        List<String> systemList = new ArrayList<>();                  //系统名称

        if (StringUtils.isNotBlank(systemId)) {
            //查询单渠道
            List<StageCountEntity> queryList = baseMapper.queryStageCountList(reqPara);

            //处理queryList获取各系统成功的笔数
            List<StageCountEntity> stageSucCountList = getStageSucCountList(queryList);

            systemList.add(systemId);
            for (StageCountEntity queryEntity : stageSucCountList) {
                dataList.add(queryEntity.getCount());
                xNameList.add(queryEntity.getDate());
            }

            //series.setStack("总量");
            series.setType(ctType);
            series.setName(systemId);
            series.setData(dataList);
            rtnList.add(series);

            graphRoot.setLegData(systemList);
            graphRoot.setXName(xNameList);
            graphRoot.setSeries(rtnList);
        } else {
            //查询所有渠道
            List<String> systemLis = baseMapper.queryStageCountSystemLis(reqPara);  //查询渠道名称
            List<StageCountEntity> queryAllList = baseMapper.queryAllStageCountList(reqPara);
            String[] dateArray = DateUtils.getWithinSevenDays();

            //处理queryList获取各系统成功的笔数
            List<StageCountEntity> stageSucCountList = getStageSucCountList(queryAllList);

            for (String system : systemLis) {
                systemList.add(system);
                dataList = new ArrayList<>();    //存放data
                xNameList = new ArrayList<>();   //存放date（横坐标）

                for(String date:dateArray){
                    xNameList.add(date);
                    int a = 0;
                    for (StageCountEntity queryAllEntity : stageSucCountList) {
                        if (system.equals(queryAllEntity.getSystem()) && date.equals(queryAllEntity.getDate())) {
                            dataList.add(queryAllEntity.getCount());
                            a = 1;
                            break;
                        }
                    }
                    if(a == 0){
                        dataList.add("0");
                    }
                }


                series = new GraphSeries();
                //series.setStack("总量");
                series.setType(ctType);
                series.setName(system);
                series.setData(dataList);
                rtnList.add(series);
            }

            graphRoot.setLegData(systemList);
            graphRoot.setXName(xNameList);
            graphRoot.setSeries(rtnList);
        }

        return graphRoot;
    }

    @Override
    public GraphRoot queryStageAwayCountList(Map reqPara) {
        //分期流失趋势
        String systemId = (String) reqPara.get("systemId");          //渠道id
        String bankNum = (String) reqPara.get("bankNum");            //银行号
        String stageType = (String) reqPara.get("stageType");        //分期类型
        String ctType = (String) reqPara.get("ctType");              //图表类型  line：折线图  bar：柱状图

        logger.info("****银行号：" + bankNum + ";渠道ID:" + systemId + "分期类型：" + stageType);

        GraphRoot graphRoot = new GraphRoot();
        GraphSeries series = new GraphSeries();
        graphRoot.setTitle(StrKit.stagesTranslate(stageType) + "流失趋势");

        List<GraphSeries> rtnList = new ArrayList<>();               //组装后list
        List<String> dataList = new ArrayList<>();                   //存放data
        List<String> xNameList = new ArrayList<>();                  //存放date（横坐标）
        List<String> systemList = new ArrayList<>();                 //系统名称

        if (StringUtils.isNotBlank(systemId)) {
            //查询单渠道
            List<StageCountEntity> queryList = baseMapper.queryStageCountList(reqPara);

            //处理queryList得到每日流失人数
            List<StageCountEntity> awayCountList = getCalculateAwayCountList(queryList);

            systemList.add(systemId);
            for (StageCountEntity queryEntity : awayCountList) {
                dataList.add(queryEntity.getCount());
                xNameList.add(queryEntity.getDate());
            }

            //series.setStack("总量");
            series.setType(ctType);
            series.setName(systemId);
            series.setData(dataList);
            rtnList.add(series);

            graphRoot.setLegData(systemList);
            graphRoot.setXName(xNameList);
            graphRoot.setSeries(rtnList);
        } else {
            //查询所有渠道
            List<String> systemLis = baseMapper.queryStageCountSystemLis(reqPara);            //查询渠道名称
            List<StageCountEntity> queryAllList = baseMapper.queryAllStageCountList(reqPara); //查询全部渠道集合
            String[] dateArray = DateUtils.getWithinSevenDays();

            //处理queryAllList得到每日流失人数
            List<StageCountEntity> awayAllCountList = getCalculateAwayCountList(queryAllList);

            for (String system : systemLis) {
                systemList.add(system);
                dataList = new ArrayList<>();   //存放data
                xNameList = new ArrayList<>();   //存放date（横坐标）
                for(String date:dateArray){
                    xNameList.add(date);
                    int a = 0;
                    for (StageCountEntity queryAllEntity : awayAllCountList) {
                        if (system.equals(queryAllEntity.getSystem()) && date.equals(queryAllEntity.getDate())) {
                            dataList.add(queryAllEntity.getCount());
                            a = 1;
                            break;
                        }
                    }
                    if(a == 0){
                        dataList.add("0");
                    }
                }

                series = new GraphSeries();
                //series.setStack("总量");
                series.setType(ctType);
                series.setName(system);
                series.setData(dataList);
                rtnList.add(series);
            }

            graphRoot.setLegData(systemList);
            graphRoot.setXName(xNameList);
            graphRoot.setSeries(rtnList);
        }

        return graphRoot;

    }


    /**
     * 处理queryList获取各系统成功的笔数
     *
     * @param queryList
     * @return
     */

    public List<StageCountEntity> getStageSucCountList(List<StageCountEntity> queryList) {

        List<StageCountEntity> stageSucCountList = new ArrayList<>();  //每日分期成功笔数集合
        for (StageCountEntity stageCountEntity : queryList) {

            Map<String, Object> stepMap = sysConfigService.queryStepConfig(stageCountEntity.getSystem(),stageCountEntity.getBanknum(),stageCountEntity.getStageType());
            String[] stepIds = (String[])stepMap.get("stepIds");

            boolean flag = getBoolean(stageCountEntity,stepIds[stepIds.length-1]);   //判断是否满足条件
            if (flag) {
                stageSucCountList.add(stageCountEntity);
            }
        }

        return stageSucCountList;
    }


    /**
     * @param stageCountEntity
     * @return
     */
    public boolean getBoolean(StageCountEntity stageCountEntity,String step) {

        //账单分期  成功步数step
        if (("billStage".equals(stageCountEntity.getStageType()) && step.equals(stageCountEntity.getStepId()))) {
            return true;
        }

        //现金分期  成功步数step
        if (("cashStage".equals(stageCountEntity.getStageType()) && step.equals(stageCountEntity.getStepId()))) {
            return true;
        }

        //消费分期分期  成功步数step
        if (("purStage".equals(stageCountEntity.getStageType()) && step.equals(stageCountEntity.getStepId()))) {
            return true;
        }

        //按日贷分期  成功步数step
        if ("loandayStage".equals(stageCountEntity.getStageType()) && step.equals(stageCountEntity.getStepId())) {

            return true;
        }

        return false;
    }

    ;

    /**
     * 处理queryList得到每日流失人数
     *
     * @param queryList
     * @return
     */

    public List<StageCountEntity> getCalculateAwayCountList(List<StageCountEntity> queryList) {

        List<StageCountEntity> awayCountList = new ArrayList<>();  //每日流失人数集合
        StageCountEntity stageCountEntity = new StageCountEntity();

        for (int i = 0; i < queryList.size(); i++) {
            StageCountEntity queryEntity1 = queryList.get(i);

            Map<String, Object> stepMap = sysConfigService.queryStepConfig(queryEntity1.getSystem(),queryEntity1.getBanknum(),queryEntity1.getStageType());
            String[] stepIds = (String[])stepMap.get("stepIds");

            if (stepIds[0].equals(queryEntity1.getStepId())) {
                //步骤1
                for (int j = 0; j < queryList.size(); j++) {
                    StageCountEntity queryEntity2 = queryList.get(j);
                    boolean flag = getBoolean(queryEntity2,stepIds[stepIds.length-1]);   //判断是否满足条件
                    if (queryEntity1.getSystem().equals(queryEntity2.getSystem())
                            && queryEntity1.getDate().equals(queryEntity2.getDate())
                            && queryEntity1.getStageType().equals(queryEntity2.getStageType())
                            && flag) {
                        //同一天且分期成功,计算当天流失人数
                        String countStr = StrKit.SubStrByBigDecimal(queryEntity1.getCount(), queryEntity2.getCount());
                        stageCountEntity = new StageCountEntity();
                        stageCountEntity.setSystem(queryEntity1.getSystem());
                        stageCountEntity.setBanknum(queryEntity1.getBanknum());
                        stageCountEntity.setDate(queryEntity1.getDate());
                        stageCountEntity.setStageType(queryEntity1.getStageType());
                        stageCountEntity.setCount(countStr);    //当天流失人数
                        awayCountList.add(stageCountEntity);

                        continue;
                    }
                }
            }
        }

        return awayCountList;

    }

    @Override
    public FunnelRoot queryStageFunnelCountList(Map reqPara) {
        //分期流失漏斗查询
        String systemId = (String) reqPara.get("systemId");       //渠道id
        String bankNum = (String) reqPara.get("bankNum");         //银行号
        String stageType = (String) reqPara.get("stageType");     //分期类型
        logger.info("****银行号：" + bankNum + ";渠道ID:" + systemId + "分期类型：" + stageType);

        FunnelRoot funnelRoot = new FunnelRoot();
        FunnelSeries funnelSeries = new FunnelSeries();
        FunnelData funnelData = new FunnelData();
        funnelRoot.setTitle("分期流失漏斗");

        List<FunnelSeries> FunnelSeriesList = new ArrayList<>();           //组装后list
        List<FunnelData> FunnelDataList = new ArrayList<>();               //数据data
        List<String> legData = new ArrayList<>();                          //步骤


        Map<String, Object> stepMap = sysConfigService.queryStepConfig(systemId,bankNum,stageType);
        String[] stepIds = (String[])stepMap.get("stepIds");
        String[] stepNames = (String[])stepMap.get("stepNames");

        if (StringUtils.isNotBlank(systemId)) {
            //分期流失漏斗单渠道查询
            List<StageCountEntity> queryFunnelList = baseMapper.queryStageFunnelCountList(reqPara);
            legData.add(systemId);
            for(int i= 0; i<stepIds.length; i++) {
                for (StageCountEntity queryFunnelEntity : queryFunnelList) {
                    if(stepIds[i].equals(queryFunnelEntity.getStepId())){
                    funnelData = new FunnelData();
                    funnelData.setName(stepNames[i]);
                    funnelData.setValue(queryFunnelEntity.getCount());
                    FunnelDataList.add(funnelData);
                    break;
                    }
                }
            }

            funnelSeries.setName(systemId);
            funnelSeries.setData(FunnelDataList);
            FunnelSeriesList.add(funnelSeries);

            funnelRoot.setLegData(legData);
            funnelRoot.setSeries(FunnelSeriesList);
        }/*else{
            //查询所有渠道
            List<String> systemLis = baseMapper.queryStageCountSystemLis(reqPara);            //查询渠道名称
            List<StageCountEntity> queryAllList = baseMapper.queryAllStageFunnelCountList(reqPara); //查询全部渠道集合
            for (String system : systemLis) {
                legData.add(system);
                FunnelDataList = new ArrayList<>();   //存放data
                funnelSeries = new FunnelSeries();
                for (StageCountEntity queryAllEntity : queryAllList) {
                    if (system.equals(queryAllEntity.getSystem())) {
                        funnelData = new FunnelData();
                        funnelData.setName(getStepName(stageType,queryAllEntity.getStepId()));
                        funnelData.setValue(queryAllEntity.getCount());
                        FunnelDataList.add(funnelData);
                    }
                }

                funnelSeries.setName(system);
                funnelSeries.setData(FunnelDataList);
                FunnelSeriesList.add(funnelSeries);
            }

            funnelRoot.setLegData(legData);
            funnelRoot.setSeries(FunnelSeriesList);

        }*/

        return funnelRoot;
    }

    /**
     * 根据分期类型和分期步骤获取页面步骤Name
     * @param stageType
     * @param stepId
     * @return
     */
    public String getStepName(String stageType, String stepId) {
        String stepName = stepId;
        if ("billStage".equals(stageType) || "cashStage".equals(stageType)) {
            //账单分期或现金分期
            if ("step1".equals(stepId)) {
                stepName = "分期首页";
            }
            if ("step2".equals(stepId)) {
                stepName = "分期确认";
            }
            if ("step3".equals(stepId)) {
                stepName = "分期成功";
            }
            if ("step4".equals(stepId)) {
                stepName = "分期失败";
            }
        }

        if ("purStage".equals(stageType)) {
            //消费分期
            if ("step1".equals(stepId)) {
                stepName = "分期首页";
            }
            if ("step2".equals(stepId)) {
                stepName = "分期页面";
            }
            if ("step3".equals(stepId)) {
                stepName = "分期确认";
            }
            if ("step4".equals(stepId)) {
                stepName = "分期成功";
            }
            if ("step5".equals(stepId)) {
                stepName = "分期失败";
            }
        }

        if ("loandayStage".equals(stageType)) {
            //按日贷
            if ("step1".equals(stepId)) {
                stepName = "申请首页";
            }
            if ("step2".equals(stepId)) {
                stepName = "申请成功";
            }
            if ("step3".equals(stepId)) {
                stepName = "申请失败";
            }
        }

        return stepName;
    }


    @Override
    public PieRoot queryStageTimeList(Map reqPara) {
        //分期时间分布
        String systemId = (String) reqPara.get("systemId");          //渠道id
        String bankNum = (String) reqPara.get("bankNum");            //银行号
        logger.info("****银行号：" + bankNum + "渠道id:" + systemId);

        PieRoot pieRoot = new PieRoot();
        PieSeries pieSeries = new PieSeries();
        PieData pieData = new PieData();
        pieRoot.setTitle("分期时间分布");

        List<PieSeries> pieSeriesList = new ArrayList<>();           //组装后list
        List<PieData> pieDataList = new ArrayList<>();               //数据data
        List<String> legData = new ArrayList<>();                    //时间段集合

        if (StringUtils.isNotBlank(systemId)) {
            //查询单渠道
            List<StageTimeEntity> queryList = stageTimeDao.queryStageTimeList(reqPara);

            //计算各个时间区间的和
            List<StageTimeEntity> sumList = intervalSumList(queryList, null);

            for (StageTimeEntity queryEntity : sumList) {
                legData.add(queryEntity.getTimePeriod());
                pieData = new PieData();
                pieData.setName(queryEntity.getTimePeriod());
                pieData.setValue(queryEntity.getCount());
                pieDataList.add(pieData);
            }

            pieSeries.setName(systemId);
            pieSeries.setData(pieDataList);
            pieSeriesList.add(pieSeries);

            pieRoot.setLegData(legData);
            pieRoot.setSeries(pieSeriesList);

        } else {
            //查询所有渠道
            List<String> systemLis = stageTimeDao.queryStageTimeSystemLis(reqPara);              //查询渠道名称
            List<StageTimeEntity> queryAllList = stageTimeDao.queryAllStageTimeList(reqPara);    //查询全部渠道时间段数量
            //计算各个时间区间的和
            List<StageTimeEntity> sumList = intervalSumList(queryAllList, systemLis);

            for (String system : systemLis) {
                pieDataList = new ArrayList<>();               //数据data
                legData = new ArrayList<>();                   //时间段集合
                for (StageTimeEntity queryAllEntity : sumList) {
                    if (system.equals(queryAllEntity.getSystem())) {
                        legData.add(queryAllEntity.getTimePeriod());
                        pieData = new PieData();
                        pieData.setName(queryAllEntity.getTimePeriod());
                        pieData.setValue(queryAllEntity.getCount());
                        pieDataList.add(pieData);
                    }
                }

                pieSeries = new PieSeries();
                pieSeries.setName(system);
                pieSeries.setData(pieDataList);
                pieSeriesList.add(pieSeries);

            }

            pieRoot.setLegData(legData);
            pieRoot.setSeries(pieSeriesList);

        }
        return pieRoot;
    }


    @Override
    public GraphRoot queryStageLineTimeList(Map reqPara) {
        //分期时间分布折线图展示
        String systemId = (String) reqPara.get("systemId");          //渠道id
        String bankNum = (String) reqPara.get("bankNum");            //银行号
        String stageType = (String) reqPara.get("stageType");        //分期类型
        String ctType = (String) reqPara.get("ctType");              //图表类型  line：折线图  bar：柱状图
        logger.info("****银行号：" + bankNum + ";渠道ID:" + systemId + "分期类型：" + stageType);

        GraphRoot graphRoot = new GraphRoot();
        GraphSeries series = new GraphSeries();
        graphRoot.setTitle(StrKit.stagesTranslate(stageType) + "访问时间分布");

        List<GraphSeries> rtnList = new ArrayList<>();                //组装后list
        List<String> dataList = new ArrayList<>();                    //存放data
        List<String> xNameList = new ArrayList<>();                   //存放date（横坐标）
        List<String> systemList = new ArrayList<>();                  //系统名称

        if (StringUtils.isNotBlank(systemId)) {

            //查询单渠道
            List<StageTimeEntity> queryList = stageTimeDao.queryStageLineTimeList(reqPara);

            systemList.add(systemId);
            for (StageTimeEntity queryEntity : queryList) {
                dataList.add(queryEntity.getCount());
                xNameList.add(queryEntity.getTimePeriod());
            }

            //series.setStack("数量");
            series.setType(ctType);
            series.setName(systemId);
            series.setData(dataList);
            rtnList.add(series);

            graphRoot.setLegData(systemList);
            graphRoot.setXName(xNameList);
            graphRoot.setSeries(rtnList);
        } else {
            //查询所有渠道
            List<String> systemLis = stageTimeDao.queryStageLineTimeSystemLis(reqPara);              //查询渠道名称
            List<StageTimeEntity> queryAllList = stageTimeDao.queryAllStageLineTimeCountList(reqPara);
            String[] timeArray = {"00","01","02","03","04","05","06","07","08","09","10","11","12","13","14",
                    "15","16","17","18","19","20","21","22","23",};
            for (String system : systemLis) {
                systemList.add(system);
                dataList = new ArrayList<>();    //存放data
                xNameList = new ArrayList<>();   //存放date（横坐标）

                for(String time:timeArray){
                    xNameList.add(time);
                    int a = 0;
                    for (StageTimeEntity queryAllEntity : queryAllList) {
                        if (system.equals(queryAllEntity.getSystem()) && time.equals(queryAllEntity.getTimePeriod())) {
                            dataList.add(queryAllEntity.getCount());
                            a = 1;
                            break;
                        }
                    }
                    if(a == 0){
                        dataList.add("0");
                    }
                }


                series = new GraphSeries();
                //series.setStack("总量");
                series.setType(ctType);
                series.setName(system);
                series.setData(dataList);
                rtnList.add(series);
            }

            graphRoot.setLegData(systemList);
            graphRoot.setXName(xNameList);
            graphRoot.setSeries(rtnList);
        }

        return graphRoot;
    }


    /**
     * 计算各个时间区间的和
     *
     * @param queryList
     * @return
     */
    public List<StageTimeEntity> intervalSumList(List<StageTimeEntity> queryList, List<String> systemLis) {
        List<StageTimeEntity> sumList = new ArrayList<>();  //时间区间集合
        StageTimeEntity stageTimeEntity = new StageTimeEntity();
        String[] strArray = {"0_9", "9_12", "12_14", "14_18", "18_21", "21_24"};

        if (systemLis == null) {
            //单渠道
            for (int i = 0; i < strArray.length; i++) {

                String countStr = "0";     //初始化区间累加值
                String system = "";        //渠道名称
                for (int j = 0; j < queryList.size(); j++) {
                    StageTimeEntity stageTimeEntity1 = queryList.get(j);
                    system = stageTimeEntity1.getSystem();
                    String intervalStr = intervalType(stageTimeEntity1.getTimePeriod());  //返回区间
                    if (strArray[i].equals(intervalStr)) {
                        countStr = StrKit.AddStrByBigDecimal(countStr, stageTimeEntity1.getCount());
                    }
                }

                stageTimeEntity = new StageTimeEntity();
                stageTimeEntity.setSystem(system);
                stageTimeEntity.setTimePeriod(strArray[i]);
                stageTimeEntity.setCount(countStr);
                sumList.add(stageTimeEntity);
            }


        } else {
            //多渠道
            for (int i = 0; i < strArray.length; i++) {
                for (String system : systemLis) {
                    String countStr = "0";    //初始化区间累加值
                    for (int j = 0; j < queryList.size(); j++) {
                        StageTimeEntity stageTimeEntity1 = queryList.get(j);
                        String intervalStr = intervalType(stageTimeEntity1.getTimePeriod());  //返回区间
                        if (strArray[i].equals(intervalStr) && system.equals(stageTimeEntity1.getSystem())) {
                            countStr = StrKit.AddStrByBigDecimal(countStr, stageTimeEntity1.getCount());
                        }
                    }

                    stageTimeEntity = new StageTimeEntity();
                    stageTimeEntity.setSystem(system);
                    stageTimeEntity.setTimePeriod(strArray[i]);
                    stageTimeEntity.setCount(countStr);
                    sumList.add(stageTimeEntity);
                }
            }
        }

        return sumList;
    }

    /**
     * 上送时间段返回区间
     *
     * @param str
     * @return
     */
    public String intervalType(String str) {

        String intervalStr = "0_9";

        String str1 = "00,01,02,03,04,05,06,07,08";  //0_9时间段集合
        String str2 = "09,10,11";                    //9_12时间段集合
        String str3 = "12,13";                       //12_14时间段集合
        String str4 = "14,15,16,17";                 //14_18时间段集合
        String str5 = "18,19,20";                    //18_21时间段集合
        String str6 = "21,22,23";                    //21_24时间段集合


        if (str1.contains(str)) {
            intervalStr = "0_9";
            return intervalStr;
        }
        if (str2.contains(str)) {
            intervalStr = "9_12";
            return intervalStr;
        }
        if (str3.contains(str)) {
            intervalStr = "12_14";
            return intervalStr;
        }
        if (str4.contains(str)) {
            intervalStr = "14_18";
            return intervalStr;
        }
        if (str5.contains(str)) {
            intervalStr = "18_21";
            return intervalStr;
        }
        if (str6.contains(str)) {
            intervalStr = "21_24";
            return intervalStr;
        }

        return intervalStr;
    }


    @Override
    public GraphRoot queryStageMoneyList(Map reqPara) {
        //分期金额
        String systemId = (String) reqPara.get("systemId");          //渠道id
        String bankNum = (String) reqPara.get("bankNum");            //银行号
        String stageType = (String) reqPara.get("stageType");        //分期类型
        String ctType = (String) reqPara.get("ctType");              //图表类型  line：折线图  bar：柱状图
        logger.info("****银行号：" + bankNum + ";渠道ID:" + systemId + "分期类型：" + stageType);

        GraphRoot graphRoot = new GraphRoot();
        GraphSeries series = new GraphSeries();
        graphRoot.setTitle(StrKit.stagesTranslate(stageType) + "每日分期金额");

        List<GraphSeries> rtnList = new ArrayList<>();       //组装后list
        List<String> dataList = new ArrayList<>();           //存放data
        List<String> xNameList = new ArrayList<>();          //存放date（横坐标）
        List<String> systemList = new ArrayList<>();         //系统名称

        if (StringUtils.isNotBlank(systemId)) {
            //查询单渠道
            List<StageMoneyEntity> queryList = stageMoneyDao.queryStageMoneyList(reqPara);
            systemList.add(systemId);
            for (StageMoneyEntity queryEntity : queryList) {
                dataList.add(queryEntity.getMoney());
                xNameList.add(queryEntity.getDate());
            }

            //series.setStack("总量");
            series.setType(ctType);
            series.setName(systemId);
            series.setData(dataList);
            rtnList.add(series);

            graphRoot.setLegData(systemList);
            graphRoot.setXName(xNameList);
            graphRoot.setSeries(rtnList);
        } else {
            //查询所有渠道
            List<String> systemLis = stageMoneyDao.queryStageMoneySystemLis(reqPara);  //查询渠道名称
            List<StageMoneyEntity> queryAllList = stageMoneyDao.queryAllStageMoneyList(reqPara);

            for (String system : systemLis) {
                //按渠道查询
                systemList.add(system);
                dataList = new ArrayList<>();    //存放data
                xNameList = new ArrayList<>();   //存放date（横坐标）
                for (StageMoneyEntity queryAllEntity : queryAllList) {
                    if (system.equals(queryAllEntity.getSystem())) {
                        dataList.add(queryAllEntity.getMoney());
                        xNameList.add(queryAllEntity.getDate());
                    }
                }

                series = new GraphSeries();
                //series.setStack("总量");
                series.setType(ctType);
                series.setName(system);
                series.setData(dataList);
                rtnList.add(series);
            }

            graphRoot.setLegData(systemList);
            graphRoot.setXName(xNameList);
            graphRoot.setSeries(rtnList);
        }

        return graphRoot;
    }

    @Override
    public Map queryCurrStageCount(Map reqPara) {

        if("bankcoas".equals((String) reqPara.get("systemId"))){
            return null;
        }

        //查询昨日各个渠道分期笔数
        List<StageCountEntity> currStageCountList = baseMapper.queryCurrStageCountList(reqPara);

        String stageCount = "0";         //所有交易成功总笔数
        String purCount = "0";         //交易分期笔数
        String billCount = "0";        //账单分期笔数
        String cashCount = "0";        //现金分期笔数
        String loandayCount = "0";     //按日贷分期笔数

        for (StageCountEntity stageCountEntity : currStageCountList) {

            Map<String, Object> stepMap = sysConfigService.queryStepConfig(stageCountEntity.getSystem(),stageCountEntity.getBanknum(),stageCountEntity.getStageType());
            String[] stepIds = (String[])stepMap.get("stepIds");

            if ("purStage".equals(stageCountEntity.getStageType()) && stepIds[stepIds.length-1].equals(stageCountEntity.getStepId())) {
                //交易分期成功交易笔数
                purCount = StrKit.AddStrByBigDecimal(purCount, stageCountEntity.getCount());
            }
            if ("billStage".equals(stageCountEntity.getStageType()) && stepIds[stepIds.length-1].equals(stageCountEntity.getStepId())) {
                //账单分期成功交易笔数
                billCount = StrKit.AddStrByBigDecimal(billCount, stageCountEntity.getCount());
            }
            if ("cashStage".equals(stageCountEntity.getStageType()) && stepIds[stepIds.length-1].equals(stageCountEntity.getStepId())) {
                //现金分期分期成功交易笔数
                cashCount = StrKit.AddStrByBigDecimal(cashCount, stageCountEntity.getCount());
            }
            if ("loandayStage".equals(stageCountEntity.getStageType()) && stepIds[stepIds.length-1].equals(stageCountEntity.getStepId())) {
                //按日贷分期成功交易笔数
                loandayCount = StrKit.AddStrByBigDecimal(loandayCount, stageCountEntity.getCount());
            }
        }

        stageCount = StrKit.AddStrByBigDecimal(purCount, billCount, cashCount, loandayCount);

        Map<String, Object> rtnMap = new HashMap<>();
        rtnMap.put("stageCount",stageCount);
        return rtnMap;
    }


}