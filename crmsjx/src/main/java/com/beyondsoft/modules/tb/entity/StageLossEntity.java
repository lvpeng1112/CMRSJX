package com.beyondsoft.modules.tb.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @author hlwang
 * @email 
 * @date 2020-04-12 09:52:42
 */
@Data
@TableName("tb_stage_Loss")
public class StageLossEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	@TableId
	private String userId;
	@TableId
	private String bankId;
	@TableId
	private String oprDate;
	@TableId
	private String oprTime;
	private String systemId;
	private String stepId;
	private String pageId;
	private String itemId;
	private String lastStepId;
	private String lastPageId;
	private String channel;
	private String stageAmt;
	private String stageNper;
	private String stageUse;
	private String rsv1;
	private String rsv2;
	private String rsv3;
	private String numCount;
}
