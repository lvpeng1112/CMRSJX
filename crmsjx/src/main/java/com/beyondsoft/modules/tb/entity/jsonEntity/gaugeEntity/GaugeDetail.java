package com.beyondsoft.modules.tb.entity.jsonEntity.gaugeEntity;

import lombok.Data;

@Data
public class GaugeDetail {

    private String formatter;
}
