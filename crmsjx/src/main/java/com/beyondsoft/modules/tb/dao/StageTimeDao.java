package com.beyondsoft.modules.tb.dao;

import com.beyondsoft.modules.tb.entity.StageTimeEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 分期时间分布
 * 
 * @author wanchangweng
 * @email 
 * @date 2019-07-12 09:52:42
 */
@Mapper
public interface StageTimeDao extends BaseMapper<StageTimeEntity> {

    List<StageTimeEntity> queryAllStageTimeList(Map reqPara);

    List<StageTimeEntity> queryStageTimeList(Map reqPara);

    List<String> queryStageTimeSystemLis(Map reqPara);

    List<StageTimeEntity> queryStageLineTimeList(Map reqPara);

    List<String> queryStageLineTimeSystemLis(Map reqPara);

    List<StageTimeEntity> queryAllStageLineTimeCountList(Map reqPara);

}
