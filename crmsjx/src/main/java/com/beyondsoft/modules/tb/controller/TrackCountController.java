package com.beyondsoft.modules.tb.controller;

import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.tb.service.TrackCountService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;


/**
 * stageTrack报表下载
 *
 * @author huilingwang
 * @email
 * @date 2020-04-14 09:52:42
 */
@RestController
@RequestMapping("tb/trackCount")
public class TrackCountController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private TrackCountService trackCountService;
    /**
     * stageTrack报表下载
     */
    @RequestMapping("/quaryTrackReport")
    public R  queryTrackCountList(@RequestParam Map<String, Object> reqPara ,HttpServletResponse response) {
        String bankNum = (String) reqPara.get("bankNum");
        String startTime = (String) reqPara.get("startTime");
        String endTime = (String) reqPara.get("endTime");
        logger.info("queryTrackCountList ****银行号：" + bankNum + ";startTime:" + startTime + "endTime：" + endTime);
        if(StringUtils.isBlank(bankNum)||StringUtils.isBlank(startTime)||StringUtils.isBlank(endTime)){
            return R.error(999999,"参数有误");
        }else{

            //查询报表并下载报表
            return trackCountService.trackReportDown(bankNum.trim(),startTime.trim(),endTime.trim(), response);
        }
    }



}
