package com.beyondsoft.modules.scheduling.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.common.utils.PageUtils;
import com.beyondsoft.modules.scheduling.entity.JobScheEntity;

import java.util.List;
import java.util.Map;

/**
 * 作业调度记录表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-01-26 14:23:00
 */
public interface JobScheService extends IService<JobScheEntity> {

    PageUtils queryPage(Map<String, Object> params);
    void insertJobSche(Map<String, Object> params);
    void updateJobScheById(Map<String, Object> params);
    int countJobScheById(Map<String, Object> params);
    List<String> selectAllJobScheId();
}

