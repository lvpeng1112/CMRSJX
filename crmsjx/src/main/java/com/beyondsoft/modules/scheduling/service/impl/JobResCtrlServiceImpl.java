package com.beyondsoft.modules.scheduling.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.common.utils.PageUtils;
import com.beyondsoft.common.utils.Query;
import com.beyondsoft.modules.scheduling.dao.JobResCtrlDao;
import com.beyondsoft.modules.scheduling.entity.JobResCtrlEntity;
import com.beyondsoft.modules.scheduling.service.JobResCtrlService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author luochao
 * @email luochao03@beyondsoft.com
 * @date 2021-01-26 14:34:13
 */

@Service("jobResCtrlService")
public class JobResCtrlServiceImpl extends ServiceImpl<JobResCtrlDao, JobResCtrlEntity> implements JobResCtrlService {
    @Autowired(required = false)
    private JobResCtrlDao jobResCtrlDao;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String jobResType = (String)params.get("jobResType");
        IPage<JobResCtrlEntity> page = this.page(
                new Query<JobResCtrlEntity>().getPage(params),
                new QueryWrapper<JobResCtrlEntity>()
                .eq(StringUtils.isNotBlank(jobResType),"JOB_RES_TYPE",jobResType)
        );

        return new PageUtils(page);
    }

    @Override
    public void insertdata(Map<String, Object> params) {
        jobResCtrlDao.insertdata(params);
    }

    @Override
    public void updatedata(Map<String, Object> params) {
        jobResCtrlDao.updatedata(params);
    }

    @Override
    public List<String> selectAllJobId() {
        List<String> list = jobResCtrlDao.selectAllJobId();
        return list;
    }

}