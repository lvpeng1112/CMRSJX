package com.beyondsoft.modules.scheduling.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.beyondsoft.common.utils.PageUtils;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.scheduling.entity.JobSeqEntity;
import com.beyondsoft.modules.scheduling.service.JobSeqService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;





/**
 * 作业依赖关系登记表
 *
 * @author luochao
 * @email luochao03@beyondsoft.com
 * @date 2021-01-26 14:34:13
 */
@RestController
@RequestMapping("generator/jobseq")
public class JobSeqController {
    @Autowired
    private JobSeqService jobSeqService;

    /**
     * 列表
     */
    @RequestMapping("/list")

    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = jobSeqService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info")
    public R info(@RequestParam Map<String, Object> params){
        String jobSeqJobNm = (String)params.get("jobSeqJobNm");

        String jobSeqPreJob = (String) params.get("jobSeqPreJob");
        LambdaQueryWrapper<JobSeqEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(JobSeqEntity::getJobSeqJobNm,jobSeqJobNm).eq(JobSeqEntity::getJobSeqPreJob,jobSeqPreJob);
        JobSeqEntity one = jobSeqService.getOne(queryWrapper);

        return R.ok().put("jobSeq",one);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")

    public R save(@RequestParam Map<String, Object> params){
//		jobSeqService.save(jobSeq);
        String jobSeqJobNm = (String)params.get("jobSeqJobNm");
        List<String> list =jobSeqService.selectAllJobSeqId();
        List<String> listId = new ArrayList<>();
        for (int i = 0;i<list.size();i++){
            listId.add(list.get(i).replaceAll("\\s*", ""));
        }
        if(!listId.contains(jobSeqJobNm)) {
        jobSeqService.insertJobSeqData(params);
            return R.ok();
        }else {
            return R.error(999999, "作业名称：" +jobSeqJobNm + ",已存在，请重新填写！");
        }
    }

    /**
     * 修改
     */
    @RequestMapping("/update")

    public R update(@RequestParam Map<String, Object> params){
        String id = (String)params.get("id");
        String jobSeqJobNm = (String)params.get("jobSeqJobNm");
        jobSeqJobNm=jobSeqJobNm.replaceAll("\\s*", "");
        String newid=id.replaceAll("\\s*", "");
        List<String> list =jobSeqService.selectAllJobSeqId();
        List<String> listId = new ArrayList<>();
        params.put("newid",newid);
        for (int i = 0;i<list.size();i++){
            listId.add(list.get(i).replaceAll("\\s*", ""));
        }
        if(listId.contains(jobSeqJobNm) && !jobSeqJobNm.equals(newid)){
            return R.error(999999, "作业名称：" + jobSeqJobNm + ",已存在，请重新填写！");
        }else {
            jobSeqService.updateJobSeqById(params);
            return R.ok();
        }
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")

    public R delete(@RequestBody String[] jobSeqJobNms){
		jobSeqService.removeByIds(Arrays.asList(jobSeqJobNms));

        return R.ok();
    }
}
