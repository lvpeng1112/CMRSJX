package com.beyondsoft.modules.scheduling.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.scheduling.entity.JobMetadataEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 作业元数据登记表
 * 
 * @author yangshaowen
 * @email yangshaowen@beyondsoft.com
 * @date 2021-01-26 14:50:10
 */
@Mapper
public interface JobMetadataDao extends BaseMapper<JobMetadataEntity> {

  public void updatedata(Map map);

  public void insertdata(Map map);

  public List<String> selectAllJobId();
	
}
