package com.beyondsoft.modules.scheduling.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.scheduling.entity.JobSeqEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 作业依赖关系登记表
 *
 * @author luochao
 * @email luochao03@beyondsoft.com
 * @date 2021-01-26 14:34:13
 */
@Mapper
public interface JobSeqDao extends BaseMapper<JobSeqEntity> {
	void insertJobSeqData(Map<String, Object> params);
	List<String> selectAllJobSeqId();
	void updateJobSeqById(Map<String, Object> params);
}
