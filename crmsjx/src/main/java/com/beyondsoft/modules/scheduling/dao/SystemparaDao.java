package com.beyondsoft.modules.scheduling.dao;

import com.beyondsoft.modules.scheduling.entity.SystemparaEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import java.util.Map;

/**
 * 系统参数表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-01-26 15:00:15
 */
@Mapper
public interface SystemparaDao extends BaseMapper<SystemparaEntity> {

    List<SystemparaEntity> querySystemparaInfo(Map<String, Object> params);
	
}
