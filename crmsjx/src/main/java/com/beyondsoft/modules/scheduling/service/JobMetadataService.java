package com.beyondsoft.modules.scheduling.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.common.utils.PageUtils;
import com.beyondsoft.modules.scheduling.entity.JobMetadataEntity;


import java.util.List;
import java.util.Map;

/**
 * 作业元数据登记表
 *
 * @author yangshaowen
 * @email yangshaowen@beyondsoft.com
 * @date 2021-01-26 14:50:10
 */
public interface JobMetadataService extends IService<JobMetadataEntity> {

    PageUtils queryPage(Map<String, Object> params);

    public void updatedata(Map<String, Object> params);

    public void insertdata(Map<String, Object> params);

    public List<String> selectAllJobId();
}

