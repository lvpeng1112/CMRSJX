package com.beyondsoft.modules.scheduling.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.common.utils.PageUtils;
import com.beyondsoft.modules.scheduling.entity.JobResCtrlEntity;

import java.util.List;
import java.util.Map;

/**
 * 作业资源控制登记表
 *
 * @author luochao
 * @email luochao03@beyondsoft.com
 * @date 2021-01-26 14:34:13
 */
public interface JobResCtrlService extends IService<JobResCtrlEntity> {

    PageUtils queryPage(Map<String, Object> params);

    public void insertdata(Map<String, Object> params);

    public void updatedata(Map<String, Object> params);

    public List<String> selectAllJobId();
}

