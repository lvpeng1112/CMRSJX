package com.beyondsoft.modules.scheduling.service.impl;


import com.beyondsoft.common.utils.PageUtils;
import com.beyondsoft.common.utils.Query;
import com.beyondsoft.modules.scheduling.dao.JobScheDao;
import com.beyondsoft.modules.scheduling.entity.JobScheEntity;
import com.beyondsoft.modules.scheduling.service.JobScheService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;



@Service("jobScheService")
public class JobScheServiceImpl extends ServiceImpl<JobScheDao, JobScheEntity> implements JobScheService {
    @Autowired(required = false)
    private JobScheDao jobScheDao;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String jobNm = (String)params.get("jobNm");
        IPage<JobScheEntity> page = this.page(
                new Query<JobScheEntity>().getPage(params),
                new QueryWrapper<JobScheEntity>()
                .eq(StringUtils.isNotBlank(jobNm),"JOB_NM",jobNm)
        );

        return new PageUtils(page);
    }

    @Override
    public void insertJobSche(Map<String, Object> params) {
        jobScheDao.insertJobSche(params);
    }

    @Override
    public void updateJobScheById(Map<String, Object> params) {
        jobScheDao.updateJobScheById(params);
    }

    @Override
    public int countJobScheById(Map<String, Object> params) {
        int i = jobScheDao.countJobScheById(params);
        return i;
    }

    @Override
    public List<String> selectAllJobScheId() {
        List<String> strings = jobScheDao.selectAllJobScheId();
        return strings;
    }
}