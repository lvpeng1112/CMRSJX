package com.beyondsoft.modules.scheduling.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.scheduling.entity.JobResCtrlEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 作业资源控制登记表
 *
 * @author luochao
 * @email luochao03@beyondsoft.com
 * @date 2021-01-26 14:34:13
 */
@Mapper
public interface JobResCtrlDao extends BaseMapper<JobResCtrlEntity> {

    public void insertdata(Map map);

    public void updatedata(Map map);

    public List<String> selectAllJobId();
}
