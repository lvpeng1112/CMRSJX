package com.beyondsoft.modules.scheduling.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.common.utils.PageUtils;
import com.beyondsoft.modules.scheduling.entity.JobSeqEntity;

import java.util.List;
import java.util.Map;

/**
 * 作业依赖关系登记表
 *
 * @author luochao
 * @email luochao03@beyondsoft.com
 * @date 2021-01-26 14:34:13
 *
 */
public interface JobSeqService extends IService<JobSeqEntity> {

    PageUtils queryPage(Map<String, Object> params);
    void insertJobSeqData(Map<String, Object> params);
    List<String> selectAllJobSeqId();
    void updateJobSeqById(Map<String, Object> params);
}

