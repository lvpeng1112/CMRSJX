package com.beyondsoft.modules.scheduling.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.beyondsoft.common.utils.PageUtils;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.scheduling.entity.JobResCtrlEntity;
import com.beyondsoft.modules.scheduling.service.JobResCtrlService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;




/**
 * 作业资源控制登记表
 *
 * @author luochao
 * @email luochao03@beyondsoft.com
 * @date 2021-01-26 14:34:13
 */
@RestController
@RequestMapping("generator/jobresctrl")
public class JobResCtrlController {
    @Autowired
    private JobResCtrlService jobResCtrlService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = jobResCtrlService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info")
    public R info(@RequestParam Map<String, Object> params){
        String jobResType = (String)params.get("jobResType");
		JobResCtrlEntity jobResCtrl = jobResCtrlService.getById(jobResType);
        return R.ok().put("jobResCtrl", jobResCtrl);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestParam Map<String, Object> params){
        String jobResType = (String)params.get("jobResType");
        List<String> list =jobResCtrlService.selectAllJobId();
        List<String> listId = new ArrayList<>();
        for (int i = 0;i<list.size();i++){
            listId.add(list.get(i).replaceAll("\\s*", ""));
        }
        if(!listId.contains(jobResType)) {
            jobResCtrlService.insertdata(params);
            return R.ok();
        }else {
            return R.error(999999, "作业资源类型：" + jobResType + ",已存在，请重新填写！");
        }
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestParam Map<String, Object> params){
        String id = (String)params.get("id");
        String jobResType = (String)params.get("jobResType");
        jobResType=jobResType.replaceAll("\\s*", "");
        String newid=id.replaceAll("\\s*", "");
        List<String> list =jobResCtrlService.selectAllJobId();
        List<String> listId = new ArrayList<>();
        params.put("newid",newid);
        for (int i = 0;i<list.size();i++){
            listId.add(list.get(i).replaceAll("\\s*", ""));
        }
        if(listId.contains(jobResType) && !jobResType.equals(newid)){
            return R.error(999999, "作业资源类型：" + jobResType + ",已存在，请重新填写！");
        }else {
            jobResCtrlService.updatedata(params);
            return R.ok();
        }
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody String[] jobResTypes){
		jobResCtrlService.removeByIds(Arrays.asList(jobResTypes));
        return R.ok();
    }

}
