package com.beyondsoft.modules.scheduling.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

import lombok.Data;

/**
 * 作业调度记录表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-01-26 14:23:00
 */
@Data
@TableName("etl.job_sche")
public class JobScheEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId(value = "JOB_SEQ_ID")
	private String jobSeqId;
	/**
	 * 
	 */

	private String jobNm;
	/**
	 * 
	 */
	private String jobStatus;
	/**
	 * 
	 */
	private Integer jobPrio;
	/**
	 * 
	 */
	private String jobScheDate;
	/**
	 * 
	 */
	private String syscode;
	/**
	 * 
	 */
	private String delfile;
	/**
	 * 
	 */
	private String flag;
	/**
	 * 
	 */
	private String jobBeginDt;
	/**
	 * 
	 */
	private Time jobBeginTm;
	/**
	 * 
	 */
	private String jobEndDt;
	/**
	 * 
	 */
	private Time jobEndTm;

}
