package com.beyondsoft.modules.scheduling.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.modules.scheduling.entity.SystemparaEntity;
import java.util.Map;
import java.util.List;

/**
 * 系统参数表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-01-26 15:00:15
 */
public interface SystemparaService extends IService<SystemparaEntity> {

    List<SystemparaEntity> querySystemparaInfo(Map<String, Object> params);
}

