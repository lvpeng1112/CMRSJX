package com.beyondsoft.modules.scheduling.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 系统参数表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-01-26 15:00:15
 */
@Data
@TableName("etl.systempara")
public class SystemparaEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private String curdate;
	/**
	 * 
	 */
	private String procstep;
	/**
	 * 
	 */
	private String cury;
	/**
	 * 
	 */
	private String curm;
	/**
	 * 
	 */
	private String curd;
	/**
	 * 
	 */
	private String curw;
	/**
	 * 
	 */
	private String som;
	/**
	 * 
	 */
	private String eom;
	/**
	 * 
	 */
	private String soy;
	/**
	 * 
	 */
	private String eoy;
	/**
	 * 
	 */
	private String isTen;
	/**
	 * 
	 */
	private String isQuarter;
	/**
	 * 
	 */
	private String isSyear;
	/**
	 * 
	 */
	private String host;
	/**
	 * 
	 */
	private String eastType;
	/**
	 * 
	 */
	private String eastBeginDate;
	/**
	 * 
	 */
	private String eastEndDate;
	/**
	 * 
	 */
	private String dateBeginDate;
	/**
	 * 
	 */
	private String dateBeginDateStr;
	/**
	 * 
	 */
	private String dateBeginDateStrShort;
	/**
	 * 
	 */
	private String dateEndDate;
	/**
	 * 
	 */
	private String dateEndDateStr;
	/**
	 * 
	 */
	private String dateEndDateStrShort;
	/**
	 * 
	 */
	private Integer dataDays;
	/**
	 * 
	 */
	private Integer fileDays;

}
