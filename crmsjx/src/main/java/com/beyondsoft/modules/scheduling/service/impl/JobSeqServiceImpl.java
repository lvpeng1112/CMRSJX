package com.beyondsoft.modules.scheduling.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.common.utils.PageUtils;
import com.beyondsoft.common.utils.Query;
import com.beyondsoft.modules.scheduling.dao.JobSeqDao;
import com.beyondsoft.modules.scheduling.entity.JobSeqEntity;
import com.beyondsoft.modules.scheduling.service.JobSeqService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * @author luochao
 * @email luochao03@beyondsoft.com
 * @date 2021-01-26 14:34:13
 */
@Service("jobSeqService")
public class JobSeqServiceImpl extends ServiceImpl<JobSeqDao, JobSeqEntity> implements JobSeqService {
    @Autowired(required = false)
    private JobSeqDao jobSeqDao;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String jobSeqJobNm = (String)params.get("jobSeqJobNm");
        IPage<JobSeqEntity> page = this.page(
                new Query<JobSeqEntity>().getPage(params),
                new QueryWrapper<JobSeqEntity>().eq(StringUtils.isNotBlank(jobSeqJobNm),"JOB_SEQ_JOB_NM",jobSeqJobNm)
        );

        return new PageUtils(page);
    }

    @Override
    public void insertJobSeqData(Map<String, Object> params) {
        jobSeqDao.insertJobSeqData(params);
    }

    @Override
    public List<String> selectAllJobSeqId() {
        List<String> strings = jobSeqDao.selectAllJobSeqId();
        return strings;
    }

    @Override
    public void updateJobSeqById(Map<String, Object> params) {
        jobSeqDao.updateJobSeqById(params);
    }

}