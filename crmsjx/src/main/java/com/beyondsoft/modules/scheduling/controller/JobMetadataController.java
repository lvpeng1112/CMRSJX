package com.beyondsoft.modules.scheduling.controller;

import java.util.*;

import com.beyondsoft.common.utils.R;
import com.beyondsoft.common.utils.PageUtils;
import com.beyondsoft.modules.ftp.service.FtpDepositIntDiffConfigService;
import com.beyondsoft.modules.scheduling.dao.JobMetadataDao;
import com.beyondsoft.modules.scheduling.entity.JobMetadataEntity;
import com.beyondsoft.modules.scheduling.service.JobMetadataService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;




/**
 * 作业元数据登记表
 *
 * @author yangshaowen
 * @email yangshaowen@beyondsoft.com
 * @date 2021-01-26 14:50:10
 */
@RestController
@RequestMapping("generator/jobmetadata")
public class JobMetadataController {
    @Autowired
    private JobMetadataService jobMetadataService;
    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = jobMetadataService.queryPage(params);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info")
    public R info(@RequestParam Map<String, Object> params){
        String jobSeqId = (String) params.get("jobSeqId");
		JobMetadataEntity jobMetadata = jobMetadataService.getById(jobSeqId);

        return R.ok().put("jobMetadata", jobMetadata);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestParam Map<String, Object> params){
        String jobSeqId = (String)params.get("jobSeqId");
        List<String> list = jobMetadataService.selectAllJobId();
        List<String> listId = new ArrayList<>();
        for (int i = 0;i<list.size();i++){
            listId.add(list.get(i).replaceAll("\\s*", ""));
        }
        if(!listId.contains(jobSeqId)){
            jobMetadataService.insertdata(params);
            return R.ok();
        }else{
            return R.error(999999, "作业ID：" + jobSeqId + ",已存在，请重新填写！");
        }

    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestParam Map<String, Object> params){
        String id = (String)params.get("id");
        String jobSeqId = (String)params.get("jobSeqId");
        jobSeqId=jobSeqId.replaceAll("\\s*", "");
        List<String> list = jobMetadataService.selectAllJobId();
        List<String> listId = new ArrayList<>();
        String newid=id.replaceAll("\\s*", "");
        params.put("newid",newid);
        for (int i = 0;i<list.size();i++){
            listId.add(list.get(i).replaceAll("\\s*", ""));
        }
/*        System.out.println(listId);
        System.out.println("newid="+newid);
        System.out.println("jobSeqId="+jobSeqId);
        System.out.println(listId.contains(jobSeqId));
        System.out.println(!jobSeqId.equals(newid));
        System.out.println(listId.contains(jobSeqId) && !jobSeqId.equals(newid));*/
        if(listId.contains(jobSeqId) && !jobSeqId.equals(newid)){
            return R.error(999999, "作业ID：" + jobSeqId + ",已存在，请重新填写！");
        }else{
            jobMetadataService.updatedata(params);
            return R.ok();

        }
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody String[] jobSeqIds){
        System.out.println(jobSeqIds);
		jobMetadataService.removeByIds(Arrays.asList(jobSeqIds));
        return R.ok();
    }

}
