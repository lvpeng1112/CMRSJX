package com.beyondsoft.modules.scheduling.service.impl;

import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.modules.scheduling.dao.SystemparaDao;
import com.beyondsoft.modules.scheduling.entity.SystemparaEntity;
import com.beyondsoft.modules.scheduling.service.SystemparaService;
import java.util.List;
import java.util.Map;


@Service("systemparaService")
public class SystemparaServiceImpl extends ServiceImpl<SystemparaDao, SystemparaEntity> implements SystemparaService {
    @Autowired(required = false)
    private SystemparaDao systemparaDao;

    @Override
    public List<SystemparaEntity> querySystemparaInfo(Map<String, Object> params) {
        List<SystemparaEntity> systemparaInfoList = new ArrayList<>();
        systemparaInfoList = systemparaDao.querySystemparaInfo(params);
        return systemparaInfoList;
    }

}