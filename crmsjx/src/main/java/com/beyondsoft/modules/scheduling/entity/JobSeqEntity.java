package com.beyondsoft.modules.scheduling.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import lombok.Data;

/**
 * 作业依赖关系登记表
 *
 * @author luochao
 * @email luochao03@beyondsoft.com
 * @date 2021-01-26 14:34:13
 */
@Data
@TableName("etl.job_seq")
public class JobSeqEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 作业名称
	 */
    @TableId
	private String jobSeqJobNm;
	/**
	 * 作业英文名称
	 */
	private String jobSeqPreJob;

}
