package com.beyondsoft.modules.scheduling.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.beyondsoft.modules.scheduling.entity.SystemparaEntity;
import com.beyondsoft.modules.scheduling.service.SystemparaService;
import java.util.List;
import com.beyondsoft.common.utils.R;


/**
 * 系统参数表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-01-26 15:00:15
 */
@RestController
@RequestMapping("/scheduling/systempara")
public class SystemparaController {
    @Autowired
    private SystemparaService systemparaService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        List<SystemparaEntity> systemparaInfoList = systemparaService.querySystemparaInfo(params);
        return R.ok().put("list", systemparaInfoList);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{curdate}")
    public R info(@PathVariable("curdate") String curdate){
		SystemparaEntity systempara = systemparaService.getById(curdate);

        return R.ok().put("systempara", systempara);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody SystemparaEntity systempara){
		systemparaService.save(systempara);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody SystemparaEntity systempara){
		systemparaService.updateById(systempara);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Date[] curdates){
		systemparaService.removeByIds(Arrays.asList(curdates));

        return R.ok();
    }

}
