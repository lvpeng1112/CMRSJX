package com.beyondsoft.modules.scheduling.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.beyondsoft.common.utils.PageUtils;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.scheduling.entity.JobScheEntity;
import com.beyondsoft.modules.scheduling.service.JobScheService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;





/**
 * 作业调度记录表
 *
 * @author wangzhiyuan
 * @email wangzhiyuan01@beyondsoft.com
 * @date 2021-01-26 14:23:00
 */
@RestController
@RequestMapping("generator/jobsche")
public class JobScheController {
    @Autowired
    private JobScheService jobScheService;

    /**
     * 列表
     */
    @RequestMapping("/list")

    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = jobScheService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info")
    public R info(@RequestParam Map<String, Object> params){
        String jobSeqId = (String)params.get("jobSeqId");

        String jobNm = (String) params.get("jobNm");
        String jobStatus = (String) params.get("jobStatus");
        LambdaQueryWrapper<JobScheEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(JobScheEntity::getJobSeqId,jobSeqId).eq(JobScheEntity::getJobNm,jobNm)
                .eq(JobScheEntity::getJobStatus,jobStatus);
        JobScheEntity jobSche = jobScheService.getOne(queryWrapper);

        return R.ok().put("jobSche", jobSche);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
//    @RequiresPermissions("generator:jobsche:save")
    public R save(@RequestParam Map<String, Object> params){
//		jobScheService.save(jobSche);
//        String jobSeqId = (String)params.get("jobSeqId");

//        JobScheEntity byId = jobScheService.getById(jobSeqId.trim());
//        int count = jobScheService.countJobScheById(params);
//        if(count>0){
//
//            return R.error("作业序号已经存在，请重新填写");
//        }else{
//            jobScheService.insertJobSche(params);
//            return R.ok();
//        }
        String jobSeqId = (String)params.get("jobSeqId");
        List<String> list = jobScheService.selectAllJobScheId();
        List<String> listId = new ArrayList<>();
        for (int i = 0;i<list.size();i++){
            listId.add(list.get(i).replaceAll("\\s*", ""));
        }
        if(!listId.contains(jobSeqId)){
            jobScheService.insertJobSche(params);
            return R.ok();
        }else{
            return R.error(999999, "作业序号：" + jobSeqId + ",已存在，请重新填写！");
        }
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
//    @RequiresPermissions("generator:jobsche:update")
    public R update(@RequestParam Map<String, Object> params){
//		jobScheService.updateById(jobSche);
//        String jobSeqId = (String)params.get("jobSeqId");
//        int count = jobScheService.countJobScheById(params);
//        if(count>0){
//            return R.error("作业序号已经存在，请重新填写");
//        }else{
//            jobScheService.insertJobSche(params);
//            return R.ok();
//        }
        String id = (String)params.get("id");
        String jobSeqId = (String)params.get("jobSeqId");
        jobSeqId=jobSeqId.replaceAll("\\s*", "");
        List<String> list = jobScheService.selectAllJobScheId();
        List<String> listId = new ArrayList<>();
        String newid=id.replaceAll("\\s*", "");
        params.put("newid",newid);
        for (int i = 0;i<list.size();i++){
            listId.add(list.get(i).replaceAll("\\s*", ""));
        }
        if(listId.contains(jobSeqId) && !jobSeqId.equals(newid)){
            return R.error(999999, "作业序号：" + jobSeqId + ",已存在，请重新填写！");
        }else{
            jobScheService.updateJobScheById(params);
            return R.ok();

        }
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody String[] jobSeqIds){
		jobScheService.removeByIds(Arrays.asList(jobSeqIds));

        return R.ok();
    }

}
