package com.beyondsoft.modules.scheduling.service.impl;

import com.beyondsoft.common.utils.PageUtils;
import com.beyondsoft.common.utils.Query;
import com.beyondsoft.modules.scheduling.dao.JobMetadataDao;
import com.beyondsoft.modules.scheduling.entity.JobMetadataEntity;
import com.beyondsoft.modules.scheduling.service.JobMetadataService;
import com.beyondsoft.modules.team.dao.TeamManagerDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;


@Service("jobMetadataService")
public class JobMetadataServiceImpl extends ServiceImpl<JobMetadataDao, JobMetadataEntity> implements JobMetadataService {
    @Autowired(required = false)
    private JobMetadataDao jobMetadataDao;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String jobSeqId = (String)params.get("jobSeqId");
        IPage<JobMetadataEntity> page = this.page(
                new Query<JobMetadataEntity>().getPage(params),
                new QueryWrapper<JobMetadataEntity>()
                .eq(StringUtils.isNotBlank(jobSeqId),"JOB_SEQ_ID",jobSeqId)
        );

        return new PageUtils(page);
    }

    @Override
    public void updatedata(Map<String, Object> params) {
        jobMetadataDao.updatedata(params);
    }

    @Override
    public void insertdata(Map<String, Object> params) {
        jobMetadataDao.insertdata(params);
    }

    @Override
    public List<String> selectAllJobId() {
        List<String> list = jobMetadataDao.selectAllJobId();
        return list;
    }

}