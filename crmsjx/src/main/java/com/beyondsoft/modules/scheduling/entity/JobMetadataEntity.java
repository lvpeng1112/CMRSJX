package com.beyondsoft.modules.scheduling.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 作业元数据登记表
 *
 * @author yangshaowen
 * @email yangshaowen@beyondsoft.com
 * @date 2021-01-26 14:50:10
 */
@Data
@TableName("etl.job_metadata")
public class JobMetadataEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private String jobSeqId;
	/**
	 * 
	 */
	private String jobNm;
	/**
	 * 
	 */
	private String jobCnm;
	/**
	 * 
	 */
	private String jobResType;
	/**
	 * 
	 */
	private String jobRunType;
	/**
	 * 
	 */
	private String jobRunDt;
	/**
	 * 
	 */
	private String jobType;
	/**
	 * 
	 */
	private String jobCmd;
	/**
	 * 
	 */
	private String jobPar;
	/**
	 * 
	 */
	private Integer jobPrio;
	/**
	 * 
	 */
	private Integer jobWkres;
	/**
	 * 
	 */
	private Integer jobTimes;
	/**
	 * 
	 */
	private String classNum;
	/**
	 * 
	 */
	private String tableNum;
	/**
	 * 
	 */
	private String syscode;
	/**
	 * 
	 */
	private String delfile;
	/**
	 * 
	 */
	private String flag;
	/**
	 * 
	 */
	private String remark;

}
