package com.beyondsoft.modules.scheduling.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import lombok.Data;

/**
 * 作业资源控制登记表
 *
 * @author luochao
 * @email luochao03@beyondsoft.com
 * @date 2021-01-26 14:34:13
 */
@Data
@TableName("etl.job_res_ctrl")
public class JobResCtrlEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 作业资源类型
	 */
	@TableId
	private String jobResType;
	/**
	 * 资源名称
	 */
	private String jobResNm;
	/**
	 * 最大资源量
	 */
	private Integer jobResMax;
	/**
	 * 可用资源量
	 */
	private Integer jobResIdle;
	/**
	 * 备注
	 */
	private String remark;

}
