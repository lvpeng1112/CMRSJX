/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.index.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.index.entity.*;
import com.beyondsoft.modules.sys.entity.SysUserEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 指标管理
 *
 */
@Mapper
public interface IndexDao extends BaseMapper<IndexEntity> {

	public List<Map<String, Object>> queryDictDetail(Map map);

	SysUserEntity queryByUserId(Long userId);

	public List<String> findOrgId(Map reqPara);

	List<IndexEntity> queryIndexCodeList();

	List<IndexEntity> queryIndexDateList(Map reqPara);

	public int queryIndexDataListNb(Map reqPara);

	List<IndexEntity> queryIndexDataList(Map reqPara);

	List<IndexEntity> queryRefIndIdListDataList();

	List<IndexEntity> queryIndexDetail(Map reqPara);

	IndexEntity queryIndexDataById(Map reqPara);

	IndexEntity queryIndexDataByNm(Map reqPara);

	List<IndexCalRuleEntity> queryIndexCalRuleDataList(Map reqPara);

	IndexCalRuleEntity queryIndexCalRuleById(Map reqPara);

	public void saveIndexData(IndexCalRuleEntity index);

	public void saveIndexCalRuleData(Map reqPara);

	void updateIndexData(IndexCalRuleEntity index);

	void deleteIndex(Map reqPara);

	void updateIndexCalRuleInvalid(Map reqPara);

	List<IndexScoreTule> queryIndexScoreTuleDetail(Map reqPara);

	public void saveIndexScoreTuleData(Map reqPara);

	void deleteIndexScoreTule(Map reqPara);

}
