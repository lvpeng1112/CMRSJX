/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.index.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.index.dao.IndexDao;
import com.beyondsoft.modules.index.entity.*;
import com.beyondsoft.modules.index.service.IndexService;
import com.beyondsoft.modules.sys.entity.SysUserEntity;
import com.beyondsoft.utils.StrUtil;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 指标
 *
 */
@Service("IndexService")
@Transactional
public class IndexServiceImpl extends ServiceImpl<IndexDao,IndexEntity> implements IndexService {
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired(required = false)
	private IndexDao indexDao;

	public SysUserEntity getUser() {
		return (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
	}

	public Long getUserId() {
		return getUser().getUserId();
	}

	@Override
	public List<Map<String, Object>> queryDictDetail(Map map) {
        return indexDao.queryDictDetail(map);
	}

	@Override
	public List<String> findOrgId(Map reqPara) {
		return null;
	}

    @Override
    public List<IndexEntity> queryIndexCodeList() {
        return indexDao.queryIndexCodeList();
    }

    @Override
	public int queryIndexDataListNb(Map reqPara) {
        int nb = indexDao.queryIndexDataListNb(reqPara);
        return nb;
	}

	@Override
	public List<IndexEntity> queryIndexDataList(Map reqPara) {
        int pageNo = Integer.parseInt((String) reqPara.get("page"));
        int pageCount = Integer.parseInt((String)reqPara.get("limit"));
        int offset = ((pageNo - 1) * pageCount);
        reqPara.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
        reqPara.put("offset", offset);
        List<IndexEntity> queryIndexDataList = indexDao.queryIndexDataList(reqPara);
        return queryIndexDataList;
	}

    @Override
    public List<IndexEntity> queryRefIndIdListDataList() {
        List<IndexEntity> queryRefIndIdListDataList = indexDao.queryRefIndIdListDataList();
        return queryRefIndIdListDataList;
    }

    @Override
    public List<IndexEntity> queryIndexDetail(Map reqPara) {
        List<IndexEntity> queryIndexDetail = indexDao.queryIndexDetail(reqPara);
        return queryIndexDetail;
    }

    @Override
    public List<IndexCalRuleEntity> queryIndexCalRuleDataList(Map reqPara) {
        return indexDao.queryIndexCalRuleDataList(reqPara);
    }

    @Override
    @Transactional
    public R saveIndexData(IndexCalRuleEntity index) {
        Map indRulMap = new HashMap();
        Map indScoreMap = new HashMap();
        SysUserEntity sysUserEntity = indexDao.queryByUserId(getUserId());
//        String indId = (String)reqPara.get("indId");//指标id
//        String indType = (String)reqPara.get("indType");//指标类型
//        String indName = (String)reqPara.get("indName");//指标名称
//        String sdate = (String)reqPara.get("sdate");//生效时间
//        String edate = (String)reqPara.get("edate");//失效时间
        String indId = index.getIndId();//指标id
        String indType = index.getIndType();//指标类型
        String indName = index.getIndName();//指标名称
        String sdate = index.getSdate();//生效时间
        String edate = index.getEdate();//失效时间
            try {
                SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat df= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = new Date(System.currentTimeMillis());
                if(("").equals(sdate) || sdate == null){//生效日期 如不填写，生效日期为操作当日
                    index.setSdate(formatter.format(date));//生效日期
//                    reqPara.put("sdate", formatter.format(date));//生效日期
                }
                if(("").equals(edate) || edate == null){//失效日期 如不填写，失效日期为"9999-12-31"
//                    reqPara.put("edate", "9999-12-31");//失效日期
                    index.setEdate("9999-12-31");//失效日期
                }
//                reqPara.put("operId",sysUserEntity.getUsername());//操作员名称
//                reqPara.put("operTime", df.format(date));//操作时间
                index.setOperId(sysUserEntity.getUsername());//操作员名称
                index.setOperTime(df.format(date));//操作时间
                indexDao.saveIndexData(index);//执行新增指标信息数据
                if("S".equals(indType)){//如果指标类型是派生指标，进行指标因子信息保存
                    String indCalRuleId = StrUtil.genRandomNum(10);//指标因子id(随机生成)
//                    String calType = (String)reqPara.get("calType");//派生计算类型
//                    String refIndId = (String)reqPara.get("refIndId");//引用指标id
                    String calType = index.getCalType();//派生计算类型
                    String refIndId = index.getRefIndId();//引用指标id
                    indRulMap.put("indCalRuleId",indCalRuleId);
                    indRulMap.put("indId",indId);
                    indRulMap.put("indName",indName);
                    indRulMap.put("calType",calType);
                    indRulMap.put("refIndId",refIndId);
                    indRulMap.put("operId",sysUserEntity.getUsername());
                    indRulMap.put("operTime", df.format(date));
                    indexDao.saveIndexCalRuleData(indRulMap);//执行新增指标因子信息数据
//                    String tabledata = (String)reqPara.get("tabledata");//指标得分table集合数组
//                    JSONObject jsonObj = JSON.parseObject(tabledata);
//                    JSONArray indxArrIdComp = jsonObj.getJSONArray("indxArrIdComp");
//                    Map tabledataList = JSONObject.parseObject(tabledata);
//                    for (int i = 0; i < tabledataList.size(); i++) {
//                        tabledataList.get(i).get();
//                    }
//                    String dictTypId = detail.get("dictTypId").toString();
                    List<IndexScoreTule> tableList = index.getP();
                    for (int i = 0; i < tableList.size(); i++) {
                        indScoreMap.put("indCalRuleId",indCalRuleId);
                        indScoreMap.put("sInterval",tableList.get(i).getSinterval());
                        indScoreMap.put("eInterval",tableList.get(i).getEinterval());
                        indScoreMap.put("score",tableList.get(i).getScore());
                        indScoreMap.put("operId",sysUserEntity.getUsername());
                        indScoreMap.put("operTime", df.format(date));
                        indexDao.saveIndexScoreTuleData(indScoreMap);//执行新增指标得分计算信息数据
                    }
                }
            } catch (Exception e) {
                logger.info("saveIndexData error  is", e);
                return R.error(999999, e.getMessage());
        }
        return R.ok();
    }

    @Override
    @Transactional
    public R updateIndexData(IndexCalRuleEntity index) {
        final String s = StrUtil.genRandomNum(10);// 生成随机因子id
	    Map map = new HashMap();
        Map reqPara = new HashMap();
//        Map updateMap = new HashMap();
        Map deleteMap = new HashMap();
        Map queryIndRulMap = new HashMap();
        Map indScoreMap = new LinkedHashMap();
        List<String> dateList = new ArrayList<>();
        StringBuilder sb1 = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        String queryIndId = "";
        String queryIndName = "";
        String queryIndCalRuleId = "";
        String queryCalType = "";
        String queryRefIndId = "";
        SysUserEntity sysUserEntity = indexDao.queryByUserId(getUserId());
        String indId =index.getIndId();//指标id
        String indName = index.getIndName();//指标名字
        String indCalRuleId = index.getIndCalRuleId();//指标因子id
        String calType = index.getCalType();//指标因子派生计算类型
        String refIndId = index.getRefIndId();//引用指标id
        String sdate = index.getSdate();//开始时间
        String edate = index.getEdate();//结束时间
        String indType = index.getIndType();//指标类型
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat df= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        if(("").equals(sdate) || sdate == null){//生效日期 如不填写，生效日期为操作当日
            sdate = formatter.format(date);
//            updateMap.put("sdate", formatter.format(sdate));//生效日期
            index.setSdate(formatter.format(sdate));//生效日期
            reqPara.put("sdate", formatter.format(sdate));//生效日期
        }else{
            index.setSdate(sdate);//生效日期
            reqPara.put("sdate", sdate);//生效日期
        }
        if(("").equals(edate)  || edate == null){//失效日期 如不填写，失效日期为"9999-12-31"
            reqPara.put("edate", "9999-12-31");//失效日期
        }else{
            reqPara.put("edate", edate);//失效日期
        }
        reqPara.put("operId",sysUserEntity.getUsername());//操作员名称
        reqPara.put("operTime", df.format(date));//操作时间
        index.setOperId(sysUserEntity.getUsername());//操作员名称
        index.setOperTime( df.format(date));//操作时间
        try {
            reqPara.put("indId",indId);
            IndexEntity indexEntity = indexDao.queryIndexDataById(reqPara);//查询当天指标是否已经存在
            if(indexEntity != null){
                indexDao.deleteIndex(reqPara);//为了保证该指标当天只有一条数据，根据指标编号和开始时间进行删除
                map.put("indId",indId);
                if(("").equals(sdate) || sdate == null){//生效日期 如不填写，生效日期为操作当日
                    map.put("sdate",formatter.format(date));
                }else{
                    map.put("sdate",sdate);
                }
                List<IndexEntity> queryIndexDateList = indexDao.queryIndexDateList(map);//查询该指标表里面开始日期与结束日期
                for (int i = 0; i < queryIndexDateList.size(); i++) {
                    dateList.add(queryIndexDateList.get(i).getSdate());//将该指标下的所有开始日期进行存放
                    Date sDate = formatter.parse(queryIndexDateList.get(i).getSdate()); //开始日期
                    if(formatter.parse(sdate).compareTo(sDate) < 0){//修改时：开始日期大于该指标表里已存在的开始日期，将已存在的数据结束日期改为此次修改的开始日期，形成拉链表
                        deleteMap.put("indId",indId);
                        deleteMap.put("sdate",queryIndexDateList.get(i).getSdate());//该指标已经存在的开始时间
                        indexDao.deleteIndex(deleteMap);//修改时：开始日期小于该指标表里已存在的开始日期，将已存在的数据删除
                    }
                }
                indexDao.saveIndexData(index);//执行新增指标信息数据
                if("S".equals(indType)){//如果指标类型是派生指标，进行指标因子信息保存
                    queryIndRulMap.put("indCalRuleId",indCalRuleId);
                    queryIndRulMap.put("indId",indId);
                    List<IndexCalRuleEntity> queryIndexCalRuleDataList = indexDao.queryIndexCalRuleDataList(queryIndRulMap);//查询是否存在指标因子数据
                    queryIndRulMap.clear();
                    reqPara.put("refIndId",refIndId);
                    if(queryIndexCalRuleDataList.size() > 0){
                        for (int j = 0; j < queryIndexCalRuleDataList.size(); j++) {
                            queryIndId = queryIndexCalRuleDataList.get(j).getIndId();
                            queryIndName = queryIndexCalRuleDataList.get(j).getIndName();
                            queryIndCalRuleId = queryIndexCalRuleDataList.get(j).getIndCalRuleId();
                            queryCalType = queryIndexCalRuleDataList.get(j).getCalType();
                            queryRefIndId = queryIndexCalRuleDataList.get(j).getRefIndId();
                            sb1.append(indId).append(indName).append(indCalRuleId).append(calType).append(refIndId);
                            sb2.append(queryIndId).append(queryIndName).append(queryIndCalRuleId).append(queryCalType).append(queryRefIndId);
                            if(!sb1.equals(sb2)){//有更新，上一条记录即为无效
                                queryIndRulMap.put("status",0);//0-无效、1-生效
                                queryIndRulMap.put("indCalRuleId",queryIndCalRuleId);
                                queryIndRulMap.put("indId",queryIndId);
                                queryIndRulMap.put("refIndId",queryRefIndId);
                                indexDao.updateIndexCalRuleInvalid(queryIndRulMap);
                            }
                            reqPara.put("indCalRuleId",s);//指标因子id
                            reqPara.put("indId",indId);//指标id
                            reqPara.put("indName",indName);//指标名称
                            reqPara.put("calType",calType);//派生计算类型
                            indexDao.saveIndexCalRuleData(reqPara);//执行新增指标因子信息数据
                            reqPara.clear();
                            //全增全删指标得分计算信息
                            indexDao.deleteIndexScoreTule(queryIndRulMap);//删除指标得分计算信息
                            List<IndexScoreTule> tableList = index.getP();
                            for (int i = 0; i < tableList.size(); i++) {
                                indScoreMap.put("indCalRuleId",s);
                                indScoreMap.put("sInterval",tableList.get(i).getSinterval());
                                indScoreMap.put("eInterval",tableList.get(i).getEinterval());
                                indScoreMap.put("score",tableList.get(i).getScore());
                                indScoreMap.put("operId",sysUserEntity.getUsername());
                                indScoreMap.put("operTime", df.format(date));
                                indexDao.saveIndexScoreTuleData(indScoreMap);//执行新增指标得分计算信息数据
                            }
                        }
                    }else{
                        reqPara.put("indCalRuleId",s);//指标因子id
                        reqPara.put("indId",indId);//指标id
                        reqPara.put("indName",indName);//指标名称
                        reqPara.put("calType",calType);//派生计算类型
                        indexDao.saveIndexCalRuleData(reqPara);//执行新增指标因子信息数据
                        reqPara.clear();
                        //执行保存指标得分信息
                        List<IndexScoreTule> tableList = index.getP();
                        for (int i = 0; i < tableList.size(); i++) {
                            indScoreMap.put("indCalRuleId",s);
                            indScoreMap.put("sInterval",tableList.get(i).getSinterval());
                            indScoreMap.put("eInterval",tableList.get(i).getEinterval());
                            indScoreMap.put("score",tableList.get(i).getScore());
                            indScoreMap.put("operId",sysUserEntity.getUsername());
                            indScoreMap.put("operTime", df.format(date));
                            indexDao.saveIndexScoreTuleData(indScoreMap);//执行新增指标得分计算信息数据
                        }
                    }
                }
            }
            else{
                map.put("indId",indId);
                if(("").equals(sdate) || sdate == null){//生效日期 如不填写，生效日期为操作当日
                    map.put("sdate",formatter.format(date));
                }else{
                    map.put("sdate",sdate);
                }
                List<IndexEntity> queryIndexDateList = indexDao.queryIndexDateList(map);//查询该指标表里面开始日期与结束日期
                for (int i = 0; i < queryIndexDateList.size(); i++) {
                    dateList.add(queryIndexDateList.get(i).getSdate());//将该指标下的所有开始日期进行存放
                    Date sDate = formatter.parse(queryIndexDateList.get(i).getSdate()); //开始日期
                    Date eDate = formatter.parse(queryIndexDateList.get(i).getEdate());//结束日期
                    if(formatter.parse(sdate).compareTo(sDate) > 0){//修改时：开始日期大于该指标表里已存在的开始日期，将已存在的数据结束日期改为此次修改的开始日期，形成拉链表
                        index.setIndId(indId);
                        index.setEdate(sdate);
                        index.setSdate(queryIndexDateList.get(i).getSdate());//该指标已经存在的开始时间
                        if(formatter.parse(sdate).compareTo(eDate) < 0){//为了生成拉链表，只修改该指标已存在结束时间大于当前修改开始时间的数据
                            indexDao.updateIndexData(index);
                        }
                    }else{
                        deleteMap.put("indId",indId);
                        deleteMap.put("sdate",queryIndexDateList.get(i).getSdate());//该指标已经存在的开始时间
                        indexDao.deleteIndex(deleteMap);//修改时：开始日期小于该指标表里已存在的开始日期，将已存在的数据删除
                    }
                }
                //重新赋值开始时间与结束时间，执行新增新的一条数据，以此形成拉链表
                reqPara.put("sdate",sdate);
                indexDao.saveIndexData(index);//执行新增指标信息数据
                if("S".equals(indType)){//如果指标类型是派生指标，进行指标因子信息保存
                    queryIndRulMap.put("indCalRuleId",indCalRuleId);
                    queryIndRulMap.put("indId",indId);
                    List<IndexCalRuleEntity> queryIndexCalRuleDataList = indexDao.queryIndexCalRuleDataList(queryIndRulMap);//查询是否存在指标因子数据
                    queryIndRulMap.clear();
                    reqPara.put("refIndId",refIndId);
                    if(queryIndexCalRuleDataList.size() > 0){
                        for (int j = 0; j < queryIndexCalRuleDataList.size(); j++) {
                            queryIndId = queryIndexCalRuleDataList.get(j).getIndId();
                            queryIndName = queryIndexCalRuleDataList.get(j).getIndName();
                            queryIndCalRuleId = queryIndexCalRuleDataList.get(j).getIndCalRuleId();
                            queryCalType = queryIndexCalRuleDataList.get(j).getCalType();
                            queryRefIndId = queryIndexCalRuleDataList.get(j).getRefIndId();
                            sb1.append(indId).append(indName).append(indCalRuleId).append(calType).append(refIndId);
                            sb2.append(queryIndId).append(queryIndName).append(queryIndCalRuleId).append(queryCalType).append(queryRefIndId);
                            if(!sb1.equals(sb2)){//有更新，上一条记录即为无效
                                queryIndRulMap.put("status",0);//0-无效、1-生效
                                queryIndRulMap.put("indCalRuleId",queryIndCalRuleId);
                                queryIndRulMap.put("indId",queryIndId);
                                queryIndRulMap.put("refIndId",queryRefIndId);
                                indexDao.updateIndexCalRuleInvalid(queryIndRulMap);
                            }
                        }
                    }else{
                        reqPara.put("indCalRuleId",s);//指标因子id
                        reqPara.put("indId",indId);//指标id
                        reqPara.put("indName",indName);//指标名称
                        reqPara.put("calType",calType);//派生计算类型
                        indexDao.saveIndexCalRuleData(reqPara);//执行新增指标因子信息数据
                        reqPara.clear();
                    }
                    //全增全删指标得分计算信息
                    indexDao.deleteIndexScoreTule(indScoreMap);//删除指标得分计算信息
                    List<IndexScoreTule> tableList = index.getP();
                    for (int i = 0; i < tableList.size(); i++) {
                        indScoreMap.put("indCalRuleId",s);
                        indScoreMap.put("sInterval",tableList.get(i).getSinterval());
                        indScoreMap.put("eInterval",tableList.get(i).getEinterval());
                        indScoreMap.put("score",tableList.get(i).getScore());
                        indScoreMap.put("operId",sysUserEntity.getUsername());
                        indScoreMap.put("operTime", df.format(date));
                        indexDao.saveIndexScoreTuleData(indScoreMap);//执行新增指标得分计算信息数据
                    }
                }
            }

        } catch (Exception e) {
            logger.info("updateIndexData error  is", e);
            return R.error(999999, e.getMessage());
        }
        return R.ok();
    }

    @Override
    @Transactional
    public R deleteIndex(Map reqPara) {
        Map queryIndRulMap = new HashMap();
        Map indScoreMap = new HashMap();
        String indId = (String)reqPara.get("indId");//指标id
        try {
            indexDao.deleteIndex(reqPara);//删除指标信息数据
            queryIndRulMap.put("status",0);//0-无效、1-生效
            queryIndRulMap.put("indId",indId);
            indexDao.updateIndexCalRuleInvalid(queryIndRulMap);//将对应的指标因子更新为失效
            //根据指标id查询指标因子id
            List<IndexCalRuleEntity> queryIndexCalRuleDataList = indexDao.queryIndexCalRuleDataList(queryIndRulMap);
            for (int i = 0; i < queryIndexCalRuleDataList.size() ; i++) {
                indScoreMap.put("indCalRuleId",queryIndexCalRuleDataList.get(i).getIndCalRuleId());
                indexDao.deleteIndexScoreTule(indScoreMap);//删除指标得分计算信息
            }
        } catch (Exception e) {
            logger.info("deleteIndex error  is", e);
            return R.error(999999, e.getMessage());
        }
        return R.ok();
    }
}
