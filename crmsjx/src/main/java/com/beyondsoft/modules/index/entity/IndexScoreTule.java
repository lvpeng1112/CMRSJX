package com.beyondsoft.modules.index.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
/**
 * '指标计算因子得分计算公式实体类
 *
 */
@Data
@TableName("ind_score_tule")
public class IndexScoreTule implements Serializable {
    private static final long serialVersionUID = 1L;
    private String indCalRuleId;
    private String sinterval;
    private String einterval;
    private String score;
    private String operId;
    private String operTime;
}
