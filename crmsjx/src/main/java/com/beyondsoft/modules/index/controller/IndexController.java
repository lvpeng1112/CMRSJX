package com.beyondsoft.modules.index.controller;

import com.beyondsoft.common.annotation.SysLog;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.index.dao.IndexDao;
import com.beyondsoft.modules.index.entity.*;
import com.beyondsoft.modules.index.service.IndexService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author luochao
 * @email
 * @date 2020-11-9
 */
@RestController
@RequestMapping("index/queryIndex")
public class IndexController {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private IndexService indexService;

    @Autowired(required = false)
    private IndexDao indexDao;

    /**
     * 指标列表
     */
    @GetMapping("/list")
    public R list(@RequestParam Map<String, Object> params) {
        List<IndexEntity> queryIndexDataList = indexService.queryIndexDataList(params);//指标列表数据
        int count = indexService.queryIndexDataListNb(params);//指标列表总数
        return R.ok().put("queryIndexDataList", queryIndexDataList).put("count",count);
    }

    /**
     * 引用指标接口
     */
    @GetMapping("/refIndIdList")
    public R refIndIdList() {
        List<IndexEntity> queryRefIndIdListDataList = indexService.queryRefIndIdListDataList();//引用指标数据
        return R.ok().put("queryRefIndIdListDataList", queryRefIndIdListDataList);
    }

    /**
     * 获取所有下拉框数据接口
     */
    @GetMapping("/getSelectList")
    public R getSelectlist(@RequestParam Map<String, Object> params) {
        Map map = new HashMap();
        //指标类型
        map.put("busintypeid","IND_TYPE_CODE");
        List<Map<String, Object>> indexTypeList = indexService.queryDictDetail(map);
        map.clear();
        //指标维度
        map.put("busintypeid","IND_DIM_CODE");
        List<Map<String, Object>> indexDimList = indexService.queryDictDetail(map);
        map.clear();
        //业务条线
        map.put("busintypeid","BUSS_LINE_CODE");
        List<Map<String, Object>> bussLineList = indexService.queryDictDetail(map);
        map.clear();
        //业务类型
        map.put("busintypeid","BUSS_TYPE_CODE");
        List<Map<String, Object>> bussTypeList = indexService.queryDictDetail(map);
        map.clear();
        //指标度量
        map.put("busintypeid","VAL_TYPE_CODE");
        List<Map<String, Object>> valTypeList = indexService.queryDictDetail(map);
        map.clear();
        //考核对象
        map.put("busintypeid","ACT_OBJ_CODE");
        List<Map<String, Object>> actObjList = indexService.queryDictDetail(map);
        map.clear();
        //派生计算类型
        map.put("busintypeid","CAL_TYPE_CODE");
        List<Map<String, Object>> calTypeList = indexService.queryDictDetail(map);
        map.clear();
        //比较方式
        map.put("busintypeid","COMP_TYPE_CODE");
        List<Map<String, Object>> comTypeList = indexService.queryDictDetail(map);
        map.clear();
        //加减符号
        map.put("busintypeid","CAL_CONN_CODE");
        List<Map<String, Object>> calConnList = indexService.queryDictDetail(map);
        map.clear();
        return R.ok().put("indexTypeList",indexTypeList).put("indexDimList",indexDimList)
                .put("bussLineList",bussLineList).put("bussTypeList",bussTypeList)
                .put("valTypeList",valTypeList).put("actObjList",actObjList).put("calTypeList",calTypeList)
                .put("comTypeList",comTypeList).put("calConnList",calConnList);
    }

    /**
     * 指标新增之前先获得指标表指标号当前最大五位数
     */
    @GetMapping("/queryMaxIndeCode")
    public R queryMaxIndeCode(@RequestParam Map<String, Object> params) {
        List<String> stringList = new ArrayList<>();
        List<IndexEntity> indexCodeList = indexService.queryIndexCodeList();
        Integer maxIndeCode = null;
        String  stringIndeCode = "";
        if(indexCodeList == null){
            stringIndeCode = "00000";//如果指标表无数据，则默认后五位数字为00000；
            return R.ok().put("maxIndeCode",stringIndeCode);
        }else{
            for (int i = 0; i <indexCodeList.size() ; i++) {
                String indId = indexCodeList.get(i).getIndId();
                stringList.add(indId.substring(4,indId.length()));
            }
            maxIndeCode = Integer.valueOf(Collections.max(stringList))+1;//指标表当前最大五位数字+1
            //判断是否满足五位数，不满足补0
            if (maxIndeCode.toString().length() == 5){
                return R.ok().put("maxIndeCode",maxIndeCode);
            } else if (maxIndeCode.toString().length() == 4){
                stringIndeCode = "0"+maxIndeCode;
                return R.ok().put("maxIndeCode",stringIndeCode);
            }else if (maxIndeCode.toString().length() == 3){
                stringIndeCode = "00"+maxIndeCode;
                return R.ok().put("maxIndeCode",stringIndeCode);
            }else if (maxIndeCode.toString().length() == 2){
                stringIndeCode = "000"+maxIndeCode;
                return R.ok().put("maxIndeCode",stringIndeCode);
            }else{
                stringIndeCode = "0000"+maxIndeCode;
                return R.ok().put("maxIndeCode",stringIndeCode);
            }
        }
    }

    /**
     * 修改指标时回显数据查询接口
     */
    @GetMapping("/queryIndexDetail")
    public R queryIndexDetail(@RequestParam Map<String, Object> params) {
        Map map = new HashMap();
        List<IndexCalRuleEntity> queryIndexCalRuleDataList = new ArrayList<IndexCalRuleEntity>();
        List<IndexEntity> queryIndexDetail = indexService.queryIndexDetail(params);//指标数据
        String indId = queryIndexDetail.get(0).getIndId();
        String indType = queryIndexDetail.get(0).getIndType();
        if("S".equals(indType)){
            map.put("indId",indId);
            map.put("status","1");
            map.put("calType",2);
            queryIndexCalRuleDataList = indexService.queryIndexCalRuleDataList(map);
            map.clear();
            String indCalRuleId = queryIndexCalRuleDataList.get(0).getIndCalRuleId();
            map.put("indCalRuleId",indCalRuleId);
            List<IndexScoreTule> indexScoreTuleList = indexDao.queryIndexScoreTuleDetail(map);
            return R.ok().put("queryIndexDetail", queryIndexDetail).put("queryIndexCalRuleDataList", queryIndexCalRuleDataList).put("indexScoreTuleList", indexScoreTuleList);
        }
//        else if("C".equals(indType)){
//            map.put("indId",indId);
//            map.put("calType",3);
//            queryIndexCalRuleDataList = indexService.queryIndexCalRuleDataList(map);
//            map.clear();
//            String indCalRuleId = queryIndexCalRuleDataList.get(0).getIndCalRuleId();
//            map.put("indCalRuleId",indCalRuleId);
//            List<IndexScoreTule> indexScoreTuleList = indexDao.queryIndexScoreTuleDetail(map);
//            return R.ok().put("queryIndexDetail", queryIndexDetail).put("queryIndexCalRuleDataList", queryIndexCalRuleDataList).put("indexScoreTuleList", indexScoreTuleList);
//        }
        else{
            return R.ok().put("queryIndexDetail", queryIndexDetail);
        }
    }

    /**
     * 指标新增
     */
    @PostMapping(("/saveIndexData"))
    @SysLog("指标数据新增")
    public R saveIndexData(@RequestBody IndexCalRuleEntity index) {
        Map reqPara = new HashMap();
        String indId = index.getIndId();
        String sdate = index.getSdate();
        String indName = index.getIndName();
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(System.currentTimeMillis());
        index.setIndId(indId);
        if(("").equals(sdate) || sdate == null){//生效日期 如不填写，生效日期为操作当日
            index.setSdate(formatter.format(date));//生效日期
            reqPara.put("sdate",formatter.format(date));
        }else{
            index.setSdate(sdate);//生效日期
            reqPara.put("sdate",sdate);
        }
        reqPara.put("indId",indId);
        IndexEntity indexEntityId = indexDao.queryIndexDataById(reqPara);//查询指标是否已经存在
        reqPara.clear();
        reqPara.put("indName",indName);
        IndexEntity indexEntityNm = indexDao.queryIndexDataByNm(reqPara);//查询指标名称是否存在
        if(indexEntityId != null || indexEntityNm != null){
            return R.error(999999, "该指标已存在，请重新输入！");
        }else{
            indexService.saveIndexData(index);
            return R.ok();
        }
    }

    /**
     * 指标修改
     */
    @PostMapping("/updateIndexData")
    @SysLog("指标数据修改")
    public R updateIndexData(@RequestBody IndexCalRuleEntity index) {
        Map reqPara = new HashMap();
        String indId = index.getIndId();
        String sdate = index.getSdate();
        String indName = index.getIndName();
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(System.currentTimeMillis());
        index.setIndId(indId);
        if(("").equals(sdate) || sdate == null){//生效日期 如不填写，生效日期为操作当日
            index.setSdate(formatter.format(date));//生效日期
            reqPara.put("sdate",formatter.format(date));
        }else{
            index.setSdate(sdate);//生效日期
            reqPara.put("sdate",sdate);
        }
        reqPara.put("indId",indId);
        IndexEntity indexEntityId = indexDao.queryIndexDataById(reqPara);//查询指标是否已经存在
        //修改前的指标名称
        String indNameBefo = indexEntityId.getIndName();
        if(!indName.equals(indNameBefo)){//如果修改前和修改后的指标名称不一致，则进行校验
            reqPara.clear();
            reqPara.put("indName",indName);
            IndexEntity indexEntityNm = indexDao.queryIndexDataByNm(reqPara);//查询指标是否已经存在
            if(indexEntityNm != null){
                return R.error(999999,"该指标名称已存在，请重新输入！");
            }else{
                try {
                    indexService.updateIndexData(index);
                } catch (Exception e) {
                    logger.info("updateIndexData error  is", e);
                    return R.error(999999, e.getMessage());
                }
                return R.ok();
            }
        }else{
            try {
                indexService.updateIndexData(index);
            } catch (Exception e) {
                logger.info("updateIndexData error  is", e);
                return R.error(999999, e.getMessage());
            }
            return R.ok();
        }

    }

    /**
     * 指标删除
     */
    @DeleteMapping("/deleteIndex")
    @SysLog("指标数据删除")
    public R deleteIndex(@RequestParam Map<String, Object> params) {
        try {
            indexService.deleteIndex(params);
        } catch (Exception e) {
            logger.info("deleteIndex error  is", e);
            return R.error(999999, e.getMessage());
        }
        return R.ok();
    }
}
