/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.index.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.common.utils.PageUtils;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.index.entity.*;
import com.beyondsoft.modules.team.entity.CsrMgrNameListEntity;
import com.beyondsoft.modules.team.entity.TeamAndEmployeeEntity;
import com.beyondsoft.modules.team.entity.TeamManagerHisEntity;

import java.util.List;
import java.util.Map;


/**
 * 指标管理
 *
 * @author
 */
public interface IndexService extends IService<IndexEntity> {

	public List<Map<String, Object>> queryDictDetail(Map map);

	public List<String> findOrgId(Map reqPara);

	List<IndexEntity> queryIndexCodeList();

	public int queryIndexDataListNb(Map reqPara);

	List<IndexEntity> queryIndexDataList(Map reqPara);

	List<IndexEntity> queryRefIndIdListDataList();

	List<IndexEntity> queryIndexDetail(Map reqPara);

	List<IndexCalRuleEntity> queryIndexCalRuleDataList(Map reqPara);

	public R saveIndexData(IndexCalRuleEntity index);

	public R updateIndexData(IndexCalRuleEntity index);

	public R deleteIndex(Map reqPara);
}
