package com.beyondsoft.modules.index.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * '指标计算因子信息实体类
 *
 */
@Data
@TableName("ind_cal_rule")
public class IndexCalRuleEntity extends IndexEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    private String indCalRuleId;
    private String indId;
    private String indName;
    private String calType;
    private String calTypeNm;
    private String refIndId;
    private String compType;
    private String compTypeNm;
    private String scoreTule;
    private String seriNum;
    private String calConn;
    private String calConnNm;
    private String formula;
    private String operId;
    private String operTime;
    private String status;
    private String remark1;
    private String remark2;

    private List<IndexScoreTule> p;
}
