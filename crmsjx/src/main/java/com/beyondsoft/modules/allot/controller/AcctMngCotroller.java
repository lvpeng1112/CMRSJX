package com.beyondsoft.modules.allot.controller;

import com.beyondsoft.common.annotation.SysLog;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.allot.entity.AcctAllot;
import com.beyondsoft.modules.allot.entity.AcctMng;
import com.beyondsoft.modules.allot.service.AcctMngService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/allot")
public class AcctMngCotroller {

    @Autowired
    private AcctMngService acctMngService;

    /**
     * 数据列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params) {
        List<AcctMng> list = acctMngService.queryPage(params);
        int count = acctMngService.getCount(params);
        return R.ok().put("dataList", list).put("count", count);
    }

    /**
     * 数据列表
     */
    @RequestMapping("/getAllotList")
    public R getAllotList(@RequestParam Map<String, Object> params) {
        List<AcctAllot> list = acctMngService.getAllotList(params);
        return R.ok().put("dataList", list);
}

    /**
     * 更新或新增nb_team数据
     */
    @PostMapping("/insertAllotList")
    @SysLog("修改分配比例")
    public R insertAllotList(@RequestBody List<AcctAllot> allt) {
        List<AcctAllot> allots = new ArrayList<>();
        String psJN = "";
        String acctId = "";
        String[] str = new String [2];
        for (AcctAllot acctAllot: allt) {
            if(acctAllot.getPsJN() == null ){
                acctAllot.setPsJN(psJN);
                acctAllot.setAcctId(acctId);
            }
            else{
                psJN = acctAllot.getPsJN() ;
                acctId = acctAllot.getAcctId();
            }
            if(acctAllot.getEmpId().indexOf(",") > -1){
                str = acctAllot.getEmpId().split(",");
                acctAllot.setEmpId(str[0]);
                acctAllot.setEmpName(str[1]);
            }
            allots.add(acctAllot);
        }
        //根据流水号删除
        acctMngService.deleteAcctAllot(psJN);
        //插入
        acctMngService.insertAcctAllot(allots);
        //修改状态
        acctMngService.applyMngStatus(psJN);
        return R.ok();
    }

    /**
     * 申请认领
     */
    @RequestMapping("/applyAllort")
    @SysLog("认领")
    public R applyAllort(@RequestParam Map<String, Object> params) {
        acctMngService.applyAllort(params);
        return R.ok();
    }

}

