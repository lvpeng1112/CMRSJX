package com.beyondsoft.modules.allot.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("PMM_C_ACCT_MNG_ALLOT")
public class AcctAllot {
    private  String psJN ;
    private  String acctId ;
    private  String empId ;
    private  String empName ;
    private  int allotPct ;
}
