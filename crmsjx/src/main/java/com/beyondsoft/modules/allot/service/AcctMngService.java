package com.beyondsoft.modules.allot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.modules.allot.entity.AcctAllot;
import com.beyondsoft.modules.allot.entity.AcctMng;

import java.util.List;
import java.util.Map;

public interface AcctMngService extends IService<AcctMng> {

    List<AcctMng> queryPage(Map<String, Object> params);

    int getCount(Map<String, Object> params);

    List<AcctAllot> getAllotList(Map<String, Object> params);

    void deleteAcctAllot(String psJN);

    void insertAcctAllot(List<AcctAllot> allots);

    void applyAllort(Map<String, Object> params);

    void applyMngStatus(String psJN);
}
