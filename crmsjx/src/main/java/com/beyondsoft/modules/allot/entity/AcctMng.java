package com.beyondsoft.modules.allot.entity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class AcctMng {
    private  String postingDate ;
    private  String postingTime ;
    private  String brch ;
    private  String brName ;
    private  String itemCd ;
    private String sfm ;
    private  String sfmName ;
    private BigDecimal lcyAmt ;
    private BigDecimal fcyAmt ;
    private  String glifRef ;
    private  String psJN ;
    private  String custId ;
    private  String custName ;
    private  String empId ;
    private  String empName ;
    private  int allotPct ;
    private String applyId ;
    private String status ;

}
