package com.beyondsoft.modules.allot.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.modules.allot.dao.AcctMngDao;
import com.beyondsoft.modules.allot.entity.AcctAllot;
import com.beyondsoft.modules.allot.entity.AcctMng;
import com.beyondsoft.modules.allot.service.AcctMngService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service("acctMngService")
@Transactional
public class AcctMngServiceImpl extends ServiceImpl<AcctMngDao, AcctMng> implements AcctMngService {
    @Autowired(required = false)
    private AcctMngDao acctMngDao ;

    @Override
    public List<AcctMng> queryPage(Map<String, Object> params) {
        //通过机构号查询结果
        params = getPageParams(params);
        List<AcctMng> list = acctMngDao.getMngList(params);
        return list;
    }

    @Override
    public int getCount(Map<String, Object> params) {
        return acctMngDao.getCount(params);
    }

    @Override
    public List<AcctAllot> getAllotList(Map<String, Object> params) {
        return acctMngDao.getAllotList(params);
    }

    @Override
    public void deleteAcctAllot(String psJN) {
        acctMngDao.deleteAcctAllot(psJN);
    }

    @Override
    public void insertAcctAllot(List<AcctAllot> allots) {
        acctMngDao.insertAcctAllot(allots);
    }

    @Override
    public void applyAllort(Map<String, Object> params) {
        acctMngDao.applyAllort(params);
    }

    @Override
    public void applyMngStatus(String psJN) {
        acctMngDao.applyMngStatus(psJN);
    }

    public Map<String, Object> getPageParams(Map<String, Object> params){
        int pageNo = Integer.parseInt((String) params.get("page"));
        int pageCount = Integer.parseInt((String)params.get("limit"));
        int offset = ((pageNo - 1) * pageCount);
        params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
        params.put("offset", offset);
        return params ;
    }

}
