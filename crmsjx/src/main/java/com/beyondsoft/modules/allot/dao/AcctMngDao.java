package com.beyondsoft.modules.allot.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.allot.entity.AcctAllot;
import com.beyondsoft.modules.allot.entity.AcctMng;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface AcctMngDao extends BaseMapper<AcctMng> {

    List<AcctMng> getMngList(Map<String, Object> params);

    int getCount(Map<String, Object> params);

    List<AcctAllot> getAllotList(Map<String, Object> params);

    void deleteAcctAllot(String psJN);

    void insertAcctAllot(List<AcctAllot> allots);

    void applyAllort(Map<String, Object> params);

    void applyMngStatus(String psJN);
}
