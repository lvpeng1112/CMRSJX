package com.beyondsoft.modules.factor.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.factor.entity.YearendPerformanceFactorHis;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface YearendPerformanceFactorHisDao extends BaseMapper<YearendPerformanceFactorHis> {
   void insertFactorHisList(YearendPerformanceFactorHis yearendPerformanceFactorHis);
}
