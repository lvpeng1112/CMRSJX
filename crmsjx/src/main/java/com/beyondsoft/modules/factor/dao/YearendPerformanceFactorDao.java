package com.beyondsoft.modules.factor.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.aofp.entity.Town;
import com.beyondsoft.modules.factor.entity.YearendPerformanceFactorHis;
import com.beyondsoft.modules.grade.entity.YearCheckGrade;
import com.beyondsoft.modules.factor.entity.YearendPerformanceFactor;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository("YearendPerformanceFactorDao")
public interface YearendPerformanceFactorDao extends BaseMapper<YearendPerformanceFactor> {
    /**
     * 查询村镇银行编号和名称
     * @return  List<Town>
     */
    @Select("select b.chdeptcode chdeptcode ,b.chdeptname  chdeptname from NPMM.C_MNG_CHECK_BASE_INFO a inner JOIN npmm.DEPARTMENT b ON a.ORG_ID = b.CHDEPTCODE\n" +
            "GROUP  BY  b.chdeptcode  ,b.chdeptname ")
    List<Town> queryChadDress();

    /**
     * 查询所有的村镇银行编号
     * @return
     */
    @Select("select chdeptcode, chdeptname  from npmm.DEPARTMENT")
    List<Town> queryAllChadDress();
    List<YearCheckGrade> queryByYr(YearCheckGrade checkGrade);

    List<YearCheckGrade> queryAllChdetCode();

    List<String> queryAllEmp();

    List<YearendPerformanceFactor> queryFactorListInfo(Map<String, Object> reqPara);

    int queryFactorListNb(Map reqPara);

    int checkOrgId(Map<String, Object> params);

    String queryOrgNm(Map<String, Object> params);

    void deleteFactorList(YearendPerformanceFactor yearendPerformanceFactor);

    void insertFactorList(List<YearendPerformanceFactor> list);

    int queryFactorByOrgIdCustIdYr(Map<String, Object> params);

    YearendPerformanceFactor queryFactorInfoByOrgIdCustIdYr(Map<String, Object> params);

    List<YearendPerformanceFactorHis> queryFactorListHisInfo(Map<String, Object> params);

    int queryFactorListHisInfoNb(Map<String, Object> params);

    public List<Map<String, Object>> queryDictDetail(Map map);

    void insertFactorHisList(List<YearendPerformanceFactorHis> yearendPerformanceFactorHis);

    void insertFactorHisListUpdate(List<YearendPerformanceFactorHis> yearendPerformanceFactorHis);

    void insertFactorHis(YearendPerformanceFactorHis yearendPerformanceFactorHis);

    List<YearendPerformanceFactor> queryAllOrg();

    void updateFactor(Map map);

    List<YearendPerformanceFactor> queryFactorByYr(Map map);

    void updateFactorOne(YearendPerformanceFactor yearendPerformanceFactor);
}
