package com.beyondsoft.modules.factor.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@ApiModel(value = "年终绩效系数维护历史表")
@TableName("C_YEAREND_PERFORMANCE_FACTOR")
@Data
public class YearendPerformanceFactor implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "机构编号")
    private String branchId;

    @ApiModelProperty(value = "机构名称")
    private String branchNm;
    @ApiModelProperty(value = "客户经理考核系数")
    @JsonSerialize(using= ToStringSerializer.class)
    private BigDecimal custFactor;
    @ApiModelProperty(value = "团队负责人考核系数")
    @JsonSerialize(using= ToStringSerializer.class)
    private BigDecimal teamFactor;
    @TableField("YR")
    @ApiModelProperty(value = "生效年度")
    private String yr;

}
