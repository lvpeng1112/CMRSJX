package com.beyondsoft.modules.factor.controller;

import com.alibaba.druid.util.StringUtils;
import com.beyondsoft.common.utils.DateUtils;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.common.utils.ShiroUtils;
import com.beyondsoft.modules.aofp.entity.CMngCheckBaseInfoHis;
import com.beyondsoft.modules.factor.dao.YearendPerformanceFactorDao;
import com.beyondsoft.modules.factor.dao.YearendPerformanceFactorHisDao;
import com.beyondsoft.modules.factor.entity.YearendPerformanceFactor;
import com.beyondsoft.modules.factor.entity.YearendPerformanceFactorHis;
import com.beyondsoft.modules.factor.service.YearendPerformanceFactorService;
import com.beyondsoft.modules.grade.dao.YearCheckGradeDao;
import com.beyondsoft.modules.grade.entity.YearCheckGrade;
import com.beyondsoft.modules.grade.service.YearCheckGradeService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/factor/yearendPerformanceFactor")
public class YearendPerformanceFactorController {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    YearendPerformanceFactorDao yearendPerformanceFactorDao;
    @Resource
    YearendPerformanceFactorService yearendPerformanceFactorService;

    @RequestMapping("/list")
    @ApiOperation("分页查询")
    public R list(@RequestParam Map<String, Object> params) {
        Map map = new HashMap();
        if(null == params){
            return R.error("入参为空");
        }
        if(StringUtils.isEmpty((String) params.get("page")) && StringUtils.isEmpty((String) params.get("limit"))){
            return R.error("缺少必传分页参数");
        }
        List<YearendPerformanceFactor> gradeList = yearendPerformanceFactorService.queryFactorListInfo(params);
        int i = yearendPerformanceFactorService.queryFactorListNb(params);
        return R.ok().put("gradeList",gradeList).put("count",i);
    }

    @RequestMapping("/importExcel")
    @ApiOperation("导入excel")
    public R importExcel(@RequestParam(value = "file") MultipartFile file,String org, String isKhy) throws Exception {
        String fileName = file.getOriginalFilename();
        logger.info("导入表格文件为:{}",fileName);

        return yearendPerformanceFactorService.batchImport(fileName, file, org, isKhy);
    }

    @RequestMapping("/insertData")
    @ApiOperation("新增数据")
    public R insertData(@RequestParam Map<String, Object> params){
//        int i = yearendPerformanceFactorService.queryFactorByOrgIdCustIdYr(params);
//        if(i==0){
            yearendPerformanceFactorService.insertData(params);
            return R.ok();
//        }else {
//            return R.error("该客户经理已存在");
//        }
    }
    @RequestMapping("/updateData")
    @ApiOperation("修改数据")
    public R updateData(@RequestParam Map<String, Object> params){
            yearendPerformanceFactorService.updateData(params);
            return R.ok();
    }

    @RequestMapping("/delete")
    @ApiOperation("删除数据")
    public R deleteData(@RequestParam Map<String, Object> params){
        yearendPerformanceFactorService.deleteFactorList(params);
        return R.ok();
    }

    @RequestMapping("/hisList")
    @ApiOperation("查询数据")
    public R queryFactorListHisInfo(@RequestParam Map<String, Object> params){
        if(null == params){
            return R.error("入参为空");
        }
        if(StringUtils.isEmpty((String) params.get("page")) && StringUtils.isEmpty((String) params.get("limit"))){
            return R.error("缺少必传分页参数");
        }

        List<YearendPerformanceFactorHis> factorList = yearendPerformanceFactorService.queryFactorListHisInfo(params);
        int i = yearendPerformanceFactorService.queryFactorListHisInfoNb(params);

        return R.ok().put("factorList",factorList).put("count",i);
    }
}