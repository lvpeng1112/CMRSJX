package com.beyondsoft.modules.factor.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.factor.entity.YearendPerformanceFactor;
import com.beyondsoft.modules.factor.entity.YearendPerformanceFactorHis;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;


public interface YearendPerformanceFactorService extends IService<YearendPerformanceFactor> {
      List<YearendPerformanceFactor> queryFactorListInfo(Map<String, Object> params);

      int queryFactorListNb(Map<String, Object> params);

      R batchImport(String fileName, MultipartFile file, String org, String isKhy) throws Exception;

      void insertData(Map<String, Object> params);

       void updateData(Map<String, Object> params);

      int queryFactorByOrgIdCustIdYr(Map<String, Object> params);

      void deleteFactorList(Map<String, Object> params);

      List<YearendPerformanceFactorHis> queryFactorListHisInfo(Map<String, Object> params);

      int queryFactorListHisInfoNb(Map<String, Object> params);

}
