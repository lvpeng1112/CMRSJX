package com.beyondsoft.modules.factor.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.common.utils.DateUtils;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.common.utils.ShiroUtils;
import com.beyondsoft.datasource.annotation.DataSource;
import com.beyondsoft.modules.aofp.entity.CMngCheckBaseInfo;
import com.beyondsoft.modules.factor.dao.YearendPerformanceFactorHisDao;
import com.beyondsoft.modules.factor.entity.YearendPerformanceFactorHis;
import com.beyondsoft.modules.grade.dao.YearCheckGradeDao;
import com.beyondsoft.modules.grade.entity.YearCheckGrade;
import com.beyondsoft.modules.factor.dao.YearendPerformanceFactorDao;
import com.beyondsoft.modules.factor.entity.YearendPerformanceFactor;
import com.beyondsoft.modules.factor.service.YearendPerformanceFactorService;
import com.beyondsoft.utils.DateUtil;
import com.beyondsoft.utils.StrUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.*;

@Service
public class YearendPerformanceFactorServiceImpl extends ServiceImpl<YearendPerformanceFactorDao, YearendPerformanceFactor> implements YearendPerformanceFactorService {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    YearCheckGradeDao yearCheckGradeDao;
    @Resource
    YearendPerformanceFactorDao yearendPerformanceFactorDao;
    @Override
    @DataSource(value = "second")
    public List<YearendPerformanceFactor> queryFactorListInfo(Map<String, Object> params){
        int pageNo = Integer.parseInt((String) params.get("page"));
        int pageCount = Integer.parseInt((String)params.get("limit"));
        int offset = ((pageNo - 1) * pageCount);
        params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
        params.put("offset", offset);
        List<YearendPerformanceFactor> factors = yearendPerformanceFactorDao.queryFactorListInfo(params);
//        for(int i=0;i<factors.size();i++){
//            YearendPerformanceFactor y = factors.get(i);
//            BigDecimal custFactor = y.getCustFactor();
//            BigDecimal mul = new BigDecimal(100);
//            custFactor = custFactor.multiply(mul);
//            y.setCustFactor(custFactor);
//            BigDecimal teamFactor = y.getTeamFactor();
//            teamFactor.multiply(mul);
//            teamFactor=teamFactor.multiply(mul);;
//            y.setTeamFactor(teamFactor);
//            factors.set(i,y);
//        }
        return factors;
    }

    @Override
    @DataSource(value = "second")
    public int queryFactorListNb(Map<String, Object> params){
        int nb = yearendPerformanceFactorDao.queryFactorListNb(params);
        return nb;
    }

    @Override
    @DataSource(value = "second")
    public R batchImport(String fileName, MultipartFile file, String org, String isKhy) {
        Map map = new HashMap();
        Workbook workbook = null;
        if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
            return R.error("上传格式不正确");
        }
        String extString = fileName.substring(fileName.lastIndexOf("."));

        List<YearendPerformanceFactor> factorsList = new ArrayList<>();
        List<YearendPerformanceFactorHis> hisList = new ArrayList<>();
        try {
            InputStream inputStream = file.getInputStream();
            if (".xls".equals(extString)) {
                HSSFWorkbook wb = new HSSFWorkbook(inputStream);
                workbook = wb;
            } else if (".xlsx".equals(extString)) {
                XSSFWorkbook wb = new XSSFWorkbook(inputStream);
                workbook = wb;
            }
            int sheets = workbook.getNumberOfSheets();
            for (int i = 0; i < sheets; i++) {
                Sheet sheetAt = workbook.getSheetAt(i);
                //获取多少行
                int lastRowNum = sheetAt.getLastRowNum();
                for (int j = 1; j <= lastRowNum; j++) {
                    Row row = sheetAt.getRow(j);
                    if (row != null) {
                        YearendPerformanceFactor yearendPerformanceFactor = new YearendPerformanceFactor();
                        for (Cell cell : row) {
                            cell.setCellType(Cell.CELL_TYPE_STRING);
                        }
                        // 必须输入项check
                        if (row.getCell(0) == null || row.getCell(0).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，村镇银行号列没有数据");
                        }else if (row.getCell(1) == null || row.getCell(1).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，村镇银行名称列没有数据");
                        }else if (row.getCell(2) == null || row.getCell(2).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，客户经理年终绩效系数列没有数据");
                        }else if (row.getCell(3) == null || row.getCell(3).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，团队负责人年终绩效系数列没有数据");
                        }else if (row.getCell(4) == null || row.getCell(4).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，生效年度列没有数据");
                        }
                        // 月度日期格式check
                        String yr = row.getCell(4).getStringCellValue();
                        String inputStr = yr + "0101";
                        if (!DateUtil.isDate(inputStr)) {
                            return R.error("第" + (j + 1) + "行,年度【" + yr + "】不是正确的日期格式！例:YYYY");
                        }

                        // 机构编号存在check
                        String orgId = row.getCell(0).getStringCellValue();
                        Map<String, Object> orgParams = new HashMap<>();
                        orgParams.put("orgId", orgId);
                        orgParams.put("org", org);
                        orgParams.put("isKhy", isKhy);
                        int count = yearendPerformanceFactorDao.checkOrgId(orgParams);
                        if (count == 0) {
                            return R.error("第" + (j + 1) + "行,您没有权限导入其他机构的数据，机构编号【" + orgId + "】！");
                        }
                        String orgNm = yearendPerformanceFactorDao.queryOrgNm(orgParams);

                        // 客户经理年终考核系数长度check
                        String custFactor = row.getCell(2).getStringCellValue();
                        if (!StrUtil.isNumeric(custFactor)) {
                            return R.error("第" + (j + 1) + "行,客户经理年终考核系数【" + custFactor + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(custFactor, 20 , 2)) {
                            return R.error("第" + (j + 1) + "行,客户经理年终考核系数【" + custFactor + "】不能超过20位整数2位小数！");
                        }
                        // 团队负责人考核系数长度check
                        String teamFactor = row.getCell(3).getStringCellValue();
                        if (!StrUtil.isNumeric(teamFactor)) {
                            return R.error("第" + (j + 1) + "行,考核系数【" + teamFactor + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(teamFactor, 20, 2)) {
                            return R.error("第" + (j + 1) + "行,考核系数【" + teamFactor + "】不能超过20位整数2位小数！");
                        }


                        //执行插入方法
                        yearendPerformanceFactor.setYr(yr);
                        yearendPerformanceFactor.setBranchId(orgId);
                        yearendPerformanceFactor.setBranchNm(orgNm);
                        Double  custFactorD = Double.valueOf(custFactor);
                        custFactorD=custFactorD/100;

                        BigDecimal custFactorB = new BigDecimal(custFactorD);
                        yearendPerformanceFactor.setCustFactor(custFactorB);
//                        yearendPerformanceFactor.setCheckFactor(factorDouble);
                        Double  teamFactorD = Double.valueOf(teamFactor);
                        teamFactorD=teamFactorD/100;
                        BigDecimal teamFactorB = new BigDecimal(teamFactorD);
                        yearendPerformanceFactor.setTeamFactor(teamFactorB);
                        factorsList.add(yearendPerformanceFactor);
                        yearendPerformanceFactorDao.deleteFactorList(yearendPerformanceFactor);
                        yearendPerformanceFactorDao.insertFactorList(factorsList);
                        factorsList.clear();
                        //插入历史记录表
                        YearendPerformanceFactorHis yearendPerformanceFactorHis = new YearendPerformanceFactorHis();
                        yearendPerformanceFactorHis.setYr(yr);
                        yearendPerformanceFactorHis.setBranchId(orgId);
                        yearendPerformanceFactorHis.setBranchNm(orgNm);

                        yearendPerformanceFactorHis.setCustFactorAfter(custFactorB);
                        yearendPerformanceFactorHis.setTeamFactorAfter(teamFactorB);
                        yearendPerformanceFactorHis.setOperatedate(DateUtils.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
                        yearendPerformanceFactorHis.setOperator(ShiroUtils.getUserEntity().getName());
                        yearendPerformanceFactorHis.setLogo("批量导入");//操作标识
                        hisList.add(yearendPerformanceFactorHis);
                        yearendPerformanceFactorDao.insertFactorHisList(hisList);
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                workbook.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return R.ok("导入成功");
    }

    @Override
    @DataSource(value = "second")
    public int queryFactorByOrgIdCustIdYr(Map<String, Object> params) {
        int i = yearendPerformanceFactorDao.queryFactorByOrgIdCustIdYr(params);
        return i;
    }

    @Override
    @DataSource(value = "second")
    public void deleteFactorList(Map<String, Object> params) {
        String orgId =(String) params.get("orgId");

        String yr = (String) params.get("yr");

        YearendPerformanceFactor yearendPerformanceFactor = yearendPerformanceFactorDao.queryFactorInfoByOrgIdCustIdYr(params);

        HashMap<String, Object> map = new HashMap<>();
        map.put("orgId",orgId);
        String orgNm = yearendPerformanceFactorDao.queryOrgNm(map);
        yearendPerformanceFactor.setBranchNm(orgNm);

        List<YearendPerformanceFactor> cMngCheckBaseInfosBefo = new ArrayList<>();
        List<YearendPerformanceFactor> cMngCheckBaseInfosAfter = new ArrayList<>();
        cMngCheckBaseInfosBefo.add(yearendPerformanceFactor);
        record(cMngCheckBaseInfosBefo,cMngCheckBaseInfosAfter,"删除");
        yearendPerformanceFactorDao.deleteFactorList(yearendPerformanceFactor);
    }

    @Override
    @DataSource(value = "second")
    public List<YearendPerformanceFactorHis> queryFactorListHisInfo(Map<String, Object> params) {
        int pageNo = Integer.parseInt((String) params.get("page"));
        int pageCount = Integer.parseInt((String)params.get("limit"));
        int offset = ((pageNo - 1) * pageCount);
        params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
        params.put("offset", offset);

        List<YearendPerformanceFactorHis> factorList = yearendPerformanceFactorDao.queryFactorListHisInfo(params);
//        for(int i=0;i<factorList.size();i++){
//            YearendPerformanceFactorHis y = factorList.get(i);
//            BigDecimal mul = new BigDecimal(100);
//
//            BigDecimal custFactorBefo = y.getCustFactorBefo();
//            if(custFactorBefo!=null) {
//                custFactorBefo = custFactorBefo.multiply(mul);
//                y.setCustFactorBefo(custFactorBefo);
//            }
//            BigDecimal teamFactorBefo = y.getTeamFactorBefo();
//            if(teamFactorBefo!=null) {
//                teamFactorBefo = teamFactorBefo.multiply(mul);
//                y.setTeamFactorBefo(teamFactorBefo);
//            }
//            BigDecimal custFactorAfter = y.getCustFactorAfter();
//            if(custFactorAfter!=null) {
//                custFactorAfter = custFactorAfter.multiply(mul);
//                y.setCustFactorAfter(custFactorAfter);
//            }
//            BigDecimal teamFactorAfter = y.getTeamFactorAfter();
//            if(teamFactorAfter!=null) {
//                teamFactorAfter = teamFactorAfter.multiply(mul);
//                y.setTeamFactorAfter(teamFactorAfter);
//            }
//            factorList.set(i,y);
//        }
        return factorList;
    }

    @Override
    @DataSource(value = "second")
    public int queryFactorListHisInfoNb(Map<String, Object> params) {
        int i = yearendPerformanceFactorDao.queryFactorListHisInfoNb(params);
        return i;
    }

//    @Override
//    public void insertDataAllOrg(Map<String, Object> params) {
//        List<YearendPerformanceFactor> factors = yearendPerformanceFactorDao.queryAllOrg();
//
//        String custId = (String) params.get("custId");
//        String custNm = (String) params.get("custNm");
//        String checkFactor = (String) params.get("checkFactor");
//        Double checkFactorD = Double.valueOf(checkFactor);
//        String yr = (String) params.get("yr");
//        for(YearendPerformanceFactor y:factors){
//            y.setCustId(custId);
//            y.setCustNm(custNm);
//            y.setCheckFactor(checkFactorD);
//            y.setYr(yr);
//        }
//        yearendPerformanceFactorDao.insertFactorList(factors);
//    }

    @Override
    @DataSource(value = "second")
    public void insertData(Map params) {
        String orgId = (String)params.get("orgId");
        String custFactor = (String) params.get("custFactor");
        Double custFactorD = Double.valueOf(custFactor);
        custFactorD=custFactorD/100;
        String teamFactor = (String) params.get("teamFactor");
        Double teamFactorD = Double.valueOf(teamFactor);
        teamFactorD=teamFactorD/100;
        String yr = (String) params.get("yr");

        HashMap<String, Object> map = new HashMap<>();
        map.put("orgId",orgId);
        String orgNm = yearendPerformanceFactorDao.queryOrgNm(map);

        YearendPerformanceFactor yearendPerformanceFactor = new YearendPerformanceFactor();
        yearendPerformanceFactor.setBranchId(orgId);
        yearendPerformanceFactor.setBranchNm(orgNm);
        BigDecimal custFactorB = new BigDecimal(custFactorD);
        yearendPerformanceFactor.setCustFactor(custFactorB);
        BigDecimal teamFactorB = new BigDecimal(teamFactorD);
        yearendPerformanceFactor.setTeamFactor(teamFactorB);

        yearendPerformanceFactor.setYr(yr);

        List<YearendPerformanceFactor> cMngCheckBaseInfosBefo = new ArrayList<>();
        List<YearendPerformanceFactor> cMngCheckBaseInfosAfter = new ArrayList<>();
        cMngCheckBaseInfosAfter.add(yearendPerformanceFactor);
        record(cMngCheckBaseInfosBefo,cMngCheckBaseInfosAfter,"新增");

        List<YearendPerformanceFactor> factorsList = new ArrayList<>();
        factorsList.add(yearendPerformanceFactor);
        yearendPerformanceFactorDao.deleteFactorList(yearendPerformanceFactor);
        yearendPerformanceFactorDao.insertFactorList(factorsList);
    }
    @Override
    @DataSource(value = "second")
    public void updateData(Map params) {
        String oneClickFlag = (String)params.get("oneClickFlag");
        if("0".equals(oneClickFlag)) {
            String orgId = (String) params.get("orgId");
            String custFactor = (String) params.get("custFactor");
            Double custFactorD = Double.valueOf(custFactor);
            custFactorD=custFactorD/100;
            String teamFactor = (String) params.get("teamFactor");
            Double teamFactorD = Double.valueOf(teamFactor);
            teamFactorD=teamFactorD/100;
            String yr = (String) params.get("yr");

            HashMap<String, Object> map = new HashMap<>();
            map.put("orgId", orgId);
            String orgNm = yearendPerformanceFactorDao.queryOrgNm(map);

            YearendPerformanceFactor yearendPerformanceFactor = new YearendPerformanceFactor();
            yearendPerformanceFactor.setBranchId(orgId);
            yearendPerformanceFactor.setBranchNm(orgNm);
            BigDecimal custFactorB = new BigDecimal(custFactorD);
            yearendPerformanceFactor.setCustFactor(custFactorB);
            BigDecimal teamFactorB = new BigDecimal(teamFactorD);
            yearendPerformanceFactor.setTeamFactor(teamFactorB);

            yearendPerformanceFactor.setYr(yr);

            List<YearendPerformanceFactor> cMngCheckBaseInfosBefo = new ArrayList<>();
            YearendPerformanceFactor yearendPerformanceFactorInfo = yearendPerformanceFactorDao.queryFactorInfoByOrgIdCustIdYr(params);
            cMngCheckBaseInfosBefo.add(yearendPerformanceFactorInfo);

            List<YearendPerformanceFactor> cMngCheckBaseInfosAfter = new ArrayList<>();
            cMngCheckBaseInfosAfter.add(yearendPerformanceFactor);
            record(cMngCheckBaseInfosBefo, cMngCheckBaseInfosAfter, "修改");

            List<YearendPerformanceFactor> factorsList = new ArrayList<>();
            factorsList.add(yearendPerformanceFactor);

            yearendPerformanceFactorDao.updateFactorOne(yearendPerformanceFactor);

        }else if("1".equals(oneClickFlag)){
            String yr = (String) params.get("yr");
            String custFactor = (String) params.get("custFactor");
            Double custFactorD = Double.valueOf(custFactor);
            custFactorD=custFactorD/100;
            String teamFactor = (String) params.get("teamFactor");
            Double teamFactorD = Double.valueOf(teamFactor);
            teamFactorD=teamFactorD/100;
            HashMap<String, Object> map = new HashMap<>();
            map.put("yr",yr);
            map.put("custFactor",custFactorD);
            map.put("teamFactor",teamFactorD);
            List<YearendPerformanceFactor> factorsBefo = yearendPerformanceFactorDao.queryFactorByYr(map);
            yearendPerformanceFactorDao.updateFactor(map);
            List<YearendPerformanceFactor> factorsAfter= yearendPerformanceFactorDao.queryFactorByYr(map);
            record(factorsBefo,factorsAfter,"修改");
        }
    }
    @DataSource(value = "second")
    public void record(List<YearendPerformanceFactor> yearendPerformanceFactorsBefo, List<YearendPerformanceFactor> yearendPerformanceFactorsAfter,String logo){
        ArrayList<YearendPerformanceFactorHis> yearendPerformanceFactorHisList = new ArrayList<>();
        int count=0;
        if(yearendPerformanceFactorsBefo.size()>0){
            count=yearendPerformanceFactorsBefo.size();
        }else if(yearendPerformanceFactorsAfter.size()>0){
            count=yearendPerformanceFactorsAfter.size();
        }
        int i;
        for(i=0;i<count;i++){
            YearendPerformanceFactorHis yearendPerformanceFactorHis = new YearendPerformanceFactorHis();
            if(yearendPerformanceFactorsBefo.size() > 0){
                yearendPerformanceFactorHis.setBranchId(yearendPerformanceFactorsBefo.get(i).getBranchId());
                yearendPerformanceFactorHis.setBranchNm(yearendPerformanceFactorsBefo.get(i).getBranchNm());
                yearendPerformanceFactorHis.setCustFactorBefo(yearendPerformanceFactorsBefo.get(i).getCustFactor());
                yearendPerformanceFactorHis.setTeamFactorBefo(yearendPerformanceFactorsBefo.get(i).getTeamFactor());
                yearendPerformanceFactorHis.setYr(yearendPerformanceFactorsBefo.get(i).getYr());
            }
            if (yearendPerformanceFactorsAfter.size() > 0){
                yearendPerformanceFactorHis.setBranchId(yearendPerformanceFactorsAfter.get(i).getBranchId());
                yearendPerformanceFactorHis.setBranchNm(yearendPerformanceFactorsAfter.get(i).getBranchNm());
                yearendPerformanceFactorHis.setCustFactorAfter(yearendPerformanceFactorsAfter.get(i).getCustFactor());
                yearendPerformanceFactorHis.setTeamFactorAfter(yearendPerformanceFactorsAfter.get(i).getTeamFactor());
                yearendPerformanceFactorHis.setYr(yearendPerformanceFactorsAfter.get(i).getYr());
            }
            yearendPerformanceFactorHis.setOperatedate(DateUtils.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
            yearendPerformanceFactorHis.setOperator(ShiroUtils.getUserEntity().getName());
            yearendPerformanceFactorHis.setLogo(logo);//操作标识
            yearendPerformanceFactorHisList.add(yearendPerformanceFactorHis);
        }
        if("修改".equals(logo)){
            yearendPerformanceFactorDao.insertFactorHisListUpdate(yearendPerformanceFactorHisList);
        }else if ("删除".equals(logo)){
            yearendPerformanceFactorDao.insertFactorHis(yearendPerformanceFactorHisList.get(0));
        }
        else{
            yearendPerformanceFactorDao.insertFactorHisList(yearendPerformanceFactorHisList);
        }
    }
}
