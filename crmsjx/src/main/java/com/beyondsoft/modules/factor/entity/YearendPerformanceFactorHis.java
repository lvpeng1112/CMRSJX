package com.beyondsoft.modules.factor.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@ApiModel(value = "年终绩效系数维护表")
@TableName("C_YEAREND_PERFORMANCE_FACTOR_HIS")
@Data
public class YearendPerformanceFactorHis implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "机构编号")
    private String branchId;

    @ApiModelProperty(value = "机构名称")
    private String branchNm;


    @ApiModelProperty(value = "客户经理考核系数调整前")
    @JsonSerialize(using= ToStringSerializer.class)
    private BigDecimal custFactorBefo;
    
    @ApiModelProperty(value = "客户经理考核系数调整后")
    @JsonSerialize(using= ToStringSerializer.class)
    private BigDecimal custFactorAfter;

    @ApiModelProperty(value = "团队负责人考核系数调整前")
    @JsonSerialize(using= ToStringSerializer.class)
    private BigDecimal teamFactorBefo;

    @ApiModelProperty(value = "团队负责人考核系数调整后")
    @JsonSerialize(using= ToStringSerializer.class)
    private BigDecimal teamFactorAfter;

    @ApiModelProperty(value = "生效年度")
    private String yr;

    @ApiModelProperty(value = "操作日期")
    private String operatedate;
    @ApiModelProperty(value = "操作员")
    private String operator;
    @ApiModelProperty(value = "历史标识")
    private String logo;
}
