package com.beyondsoft.modules.newTeam.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;


/**
 *
 *
 * @author luochao
 */
@Data
@TableName("nb_team_emplee_new")
public class TeamAndEmployeeEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	private String chTeamCode;
	private String chTeamName;
	private String chOrgId;
    private String chdeptname;
	private String chEmpleeCode;
	private String chEmpleeName;
	private String setupDt;
	private String chLastModify;
	private String chLastModifier;
	private String status;
    private String flag;
}
