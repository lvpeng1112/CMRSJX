/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.newTeam.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.newTeam.entity.TeamAndEmployeeEntity;
import com.beyondsoft.modules.newTeam.entity.TeamManagerEntity;
import com.beyondsoft.modules.newTeam.entity.TeamManagerHisEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 团队管理
 *
 */
@Mapper
public interface NewTeamManagerDao extends BaseMapper<TeamManagerEntity> {

	//查询客户经理数据信息
	List<Map<String,String>> empleeList(Map reqPara);
	//查询所选机构下面的团队所有团队编号
	List<String> queryTeamCodeByOrgId(Map map);
	//查询团队信息总数
	int queryTeamDataListNb(Map reqPara);
	//查询团队信息
	List<TeamManagerEntity> queryTeamDataList(Map<String, Object> params);
	//保存团队信息
	void insertTeamData(TeamManagerEntity teamManagerEntity);
    //保存团队员工关联表数据
	void insertEmployTeam(Map reqPara);
	//查询团队长所属机构
	String selectLeaderOrg(Map map);
	//查询添加机构下团队名称是否存在重复
	int checkTeamNmExist(Map reqPara);
    //查询添加在已选日期客户经理是否已经加入其它团队
    TeamAndEmployeeEntity checkEmpleeExist(Map reqPara);
	//查询团队绑定成员信息
	List<TeamAndEmployeeEntity> queryTeamEmployeeByTeamId(Map reqPara);
	//修改团队信息
	void updateTeamData(TeamManagerEntity teamManagerEntity);
	//修改团队员工关联表数据
	void updateEmployTeam(Map reqPara);
	//查询团队下面是否有团队成员
    int checkEmployeesExist(Map reqPara);
	//删除团队数据信息
    void deleteTeamData(Map reqPara);
    //删除团队成员数据信息
    void deleteTeamAndEmployee(Map reqPara);
	//查看团队成员列表数量
	int queryTeamEmployeeNb(Map reqPara);
	//查看团队成员列表
	List<TeamAndEmployeeEntity> queryTeamEmployeeList(Map reqPara);
	//查询团队负责人信息
    List<TeamAndEmployeeEntity> teamLeaderData(Map reqPara);
	//保存团队信息
	void insertTeamManagerHisData(TeamManagerHisEntity teamManagerHisEntity);
	//查询团队历史数量
    int queryTeamHisListNb(Map reqPara);
    //查询团队历史记录
    List<TeamManagerHisEntity> queryTeamHisList(Map reqPara);
}
