/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.newTeam.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.modules.newTeam.dao.NewTeamManagerDao;
import com.beyondsoft.modules.newTeam.entity.TeamAndEmployeeEntity;
import com.beyondsoft.modules.newTeam.entity.TeamManagerEntity;
import com.beyondsoft.modules.newTeam.entity.TeamManagerHisEntity;
import com.beyondsoft.modules.newTeam.service.NewTeamManagerService;
import com.beyondsoft.modules.sys.entity.SysUserEntity;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * 团队
 *
 */
@Service("newTeamManagerService")
@Transactional
public class NewTeamManagerServiceImpl extends ServiceImpl<NewTeamManagerDao,TeamManagerEntity> implements NewTeamManagerService {
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired(required = false)
	private NewTeamManagerDao newTeamManagerDao;

	public SysUserEntity getUser() {
		return (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
	}

	public Long getUserId() {
		return getUser().getUserId();
	}
	//查询客户经理数据信息
	@Override
	public List<Map<String, String>> empleeList(Map reqPara) {
		return newTeamManagerDao.empleeList(reqPara);
	}

	//查询所选机构下面的团队所有团队编号
	@Override
	public List<String> queryTeamCodeByOrgId(Map map) {
		return newTeamManagerDao.queryTeamCodeByOrgId(map);
	}

	//查询团队信息总数
	@Override
	public int queryTeamDataListNb(Map reqPara) {
		String chOrgId = (String)reqPara.get("chOrgId");
		reqPara.put("chOrgId", chOrgId);
		return newTeamManagerDao.queryTeamDataListNb(reqPara);
	}
	//查询团队信息
	@Override
	public List<TeamManagerEntity> queryTeamDataList(Map<String, Object> params) {
		List<TeamManagerEntity> teamList = newTeamManagerDao.queryTeamDataList(params);
		return teamList;
	}
	//保存团队信息
	@Override
	public void insertTeamData(TeamManagerEntity teamManagerEntity) {
		newTeamManagerDao.insertTeamData(teamManagerEntity);
	}
	//保存团队员工关联表数据
	@Override
	public void insertEmployTeam(Map reqPara) {
		newTeamManagerDao.insertEmployTeam(reqPara);
	}
	//查询团队长所属机构
	@Override
	public String selectLeaderOrg(Map map) {
		return newTeamManagerDao.selectLeaderOrg(map);
	}
    //查询添加机构下团队名称是否存在
	@Override
	public int checkTeamNmExist(Map reqPara) {
		return newTeamManagerDao.checkTeamNmExist(reqPara);
	}
	//查询添加在已选日期客户经理是否已经加入其它团队
	@Override
	public TeamAndEmployeeEntity checkEmpleeExist(Map reqPara) {
		return newTeamManagerDao.checkEmpleeExist(reqPara);
	}
	//查询团队绑定成员信息
	@Override
	public List<TeamAndEmployeeEntity> queryTeamEmployeeByTeamId(Map reqPara) {
		return newTeamManagerDao.queryTeamEmployeeByTeamId(reqPara);
	}
	//修改团队信息
	@Override
	public void updateTeamData(TeamManagerEntity teamManagerEntity) {
		newTeamManagerDao.updateTeamData(teamManagerEntity);
	}
	//修改团队员工关联表数据
	@Override
	public void updateEmployTeam(Map reqPara) {
		newTeamManagerDao.updateEmployTeam(reqPara);
	}
	//查询团队下面是否有团队成员
	@Override
	public int checkEmployeesExist(Map reqPara) {
		return newTeamManagerDao.checkEmployeesExist(reqPara);
	}

	//删除团队数据信息
	@Override
	public void deleteTeamData(Map reqPara) {
		newTeamManagerDao.deleteTeamData(reqPara);
	}
	//删除团队成员数据信息
	@Override
	public void deleteTeamAndEmployee(Map reqPara) {
		newTeamManagerDao.deleteTeamAndEmployee(reqPara);
	}
	//查看团队成员列表数量
	@Override
	public int queryTeamEmployeeNb(Map reqPara) {
		return newTeamManagerDao.queryTeamEmployeeNb(reqPara);
	}
	//查看团队成员列表
	@Override
	public List<TeamAndEmployeeEntity> queryTeamEmployeeList(Map reqPara) {
		return newTeamManagerDao.queryTeamEmployeeList(reqPara);
	}
    //查询团队负责人信息
    @Override
    public List<TeamAndEmployeeEntity> teamLeaderData(Map reqPara) {
        return newTeamManagerDao.teamLeaderData(reqPara);
    }

    //保存团队历史操作信息
	@Override
	public void insertTeamManagerHisData(TeamManagerHisEntity teamManagerHisEntity) {
		newTeamManagerDao.insertTeamManagerHisData(teamManagerHisEntity);
	}
    //查询团队历史数量
    @Override
    public int queryTeamHisListNb(Map reqPara) {
        return newTeamManagerDao.queryTeamHisListNb(reqPara);
    }
    //查询团队历史记录
    @Override
    public List<TeamManagerHisEntity> queryTeamHisList(Map reqPara) {
        return newTeamManagerDao.queryTeamHisList(reqPara);
    }
}
