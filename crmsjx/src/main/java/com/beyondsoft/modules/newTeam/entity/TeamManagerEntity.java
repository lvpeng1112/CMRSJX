/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.newTeam.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 团队实体类
 *
 */
@Data
@TableName("nb_team_new")
public class TeamManagerEntity  implements Serializable {
	private static final long serialVersionUID = 1L;
	private String chTeamCode;
	private String chTeamName;
	private String chTeamDesc;
	private String chOrgId;
	private String chdeptname;
	private String leaderId;
	private String leaderIdOld;
	private String leaderName;
	private String termStartDt;
	private String termEndDt;
    private String setupDt;
	private String createUserId;
	private String chLastModify;
	private String chLastModifier;
    private String status;
	private List<TeamAndEmployeeEntity> list;
}
