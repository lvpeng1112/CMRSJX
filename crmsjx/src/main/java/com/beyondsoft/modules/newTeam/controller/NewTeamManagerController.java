package com.beyondsoft.modules.newTeam.controller;

import com.beyondsoft.common.annotation.SysLog;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.job.dao.ScheduleJobDao;
import com.beyondsoft.modules.newTeam.entity.TeamAndEmployeeEntity;
import com.beyondsoft.modules.newTeam.entity.TeamManagerEntity;
import com.beyondsoft.modules.newTeam.entity.TeamManagerHisEntity;
import com.beyondsoft.modules.newTeam.service.NewTeamManagerService;
import com.beyondsoft.modules.sys.entity.SysUserEntity;
import com.beyondsoft.utils.PgsqlUtil;
import com.beyondsoft.utils.StrUtil;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author luochao
 * @email
 * @date 2021-06-24
 */
@RestController
@RequestMapping("teamNew/teamManager")
public class NewTeamManagerController {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource(name = "transactionManager")
    private PlatformTransactionManager platformTransactionManager;

    @Autowired
    private NewTeamManagerService newTeamManagerService;

    @Autowired(required = false)
    ScheduleJobDao scheduleJobDao;

    public SysUserEntity getUser() {
        return (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
    }

    public Long getUserId() {
        return getUser().getUserId();
    }

    /**
     * 团队页面打开查询所有客户经理信息
     */
    @RequestMapping("/empleeList")
    public R empleeList(@RequestParam Map<String, Object> reqPara){
        String initCode = getUser().getOrgId();
        reqPara.put("org", initCode);
        List<Map<String,String>> empleeList = newTeamManagerService.empleeList(reqPara);
        return R.ok().put("empleeList", empleeList);
    }

    /**
     * 团队列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params) {
        String chOrgId = (String)params.get("chOrgId");
        int pageNo = Integer.parseInt((String) params.get("page"));
        int pageCount = Integer.parseInt((String)params.get("limit"));
        int offset = ((pageNo - 1) * pageCount);
        params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
        params.put("offset", offset);
        params.put("chOrgId", chOrgId);
        List<TeamManagerEntity> teamList = newTeamManagerService.queryTeamDataList(params);
        int count = newTeamManagerService.queryTeamDataListNb(params);
        return   R.ok().put("teamList", teamList).put("count", count);
    }

    /**
     * 新增团队信息
     */
    @RequestMapping("/insertTeamData")
    @SysLog("新增团队信息")
    @Transactional(rollbackFor = Exception.class)
    public R insertTeamData(@RequestBody TeamManagerEntity teamManagerEntity) throws ParseException {
        //关闭事务自动提交
        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
        TransactionStatus transactionStatus = platformTransactionManager.getTransaction(def);
        //事务标识
        boolean target = true;
        //团队负责人编号
        String leaderId = teamManagerEntity.getLeaderId();
        //团队名称
        String chTeamName = teamManagerEntity.getChTeamName();
        //团队所属月份
        String setupDt = teamManagerEntity.getSetupDt();
        //前台传过来的团队成员数据
        List<TeamAndEmployeeEntity> employeeList = teamManagerEntity.getList();
        //团队状态(团队状态:0：加入团队(团队创建) 1：离开团队(团队解散))
        String status = "0";
        teamManagerEntity.setStatus(status);
        //创建人
        String userName = getUser().getName();
        teamManagerEntity.setCreateUserId(userName);
        //最后修改人
        teamManagerEntity.setChLastModifier(userName);
        //团队创建日期
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(System.currentTimeMillis());
        //当前系统时间
        String now = formatter.format(date);
        teamManagerEntity.setChLastModify(formatter.format(date));
        //查询团队长所属机构
        Map params = new HashMap();
        params.put("custMngrId",leaderId);
        String chOrgId = newTeamManagerService.selectLeaderOrg(params);
        params.put("chTeamName",chTeamName);
        params.put("setupDt",setupDt);
        int count = newTeamManagerService.checkTeamNmExist(params);
        //查询前台传过来的团队负责人名称
        params.put("ch_emplee_code",leaderId);
        List<TeamAndEmployeeEntity> leaderData = newTeamManagerService.teamLeaderData(params);
        if(count > 0){
            target = false;
            platformTransactionManager.rollback(transactionStatus);    // 添加失败 回滚事务；
            return R.error(999999, "团队数据在" + setupDt + "月份,已存在，请重新填写！");
        }else{
            //新增团队信息
            //设置团队机构号
            teamManagerEntity.setChOrgId(chOrgId);
            //如果是添加团队其他所属月份数据，则先查出该团队编号，设置团队团队编号
            Map cheMap = new HashMap();
            cheMap.put("chTeamName",chTeamName);
            cheMap.put("chOrgId",getUser().getOrgId());
            List<TeamManagerEntity> teamList = newTeamManagerService.queryTeamDataList(cheMap);
            if(teamList.size() > 0){
                //如果团队存在，则继续使用原本团队编号
                teamManagerEntity.setChTeamCode(teamList.get(0).getChTeamCode());
                //判断该团队上一个团队长任期结束时间，如果任期结束时间大于当前操作时间，则将任期结束时间修改为当前操作时间
                for (int i = 0; i <teamList.size() ; i++) {
                    String termEndDt = teamList.get(i).getTermEndDt();
                    Date sDate = formatter.parse(termEndDt); //原团队负责人任期结束时间
                    if(formatter.parse(now).compareTo(sDate) < 0 && !teamManagerEntity.getLeaderId().equals(teamList.get(i).getLeaderId())){
                        //如果团队负责人改变，先查询该团队原负责人的任期结束时间，如果大于当前操作的系统时间，则将任期结束时间修改为当前系统操作时间
                        TeamManagerEntity entity = new TeamManagerEntity();
                        entity.setChTeamCode(teamList.get(i).getChTeamCode());
                        entity.setTermEndDt(teamManagerEntity.getTermStartDt());//卸任团队负责人时间为新团队负责人上任时间
                        entity.setLeaderIdOld(teamList.get(i).getLeaderId());
                        newTeamManagerService.updateTeamData(entity);
                        //新增团队长卸任团队负责人操作历史
                        TeamManagerHisEntity teamManagerHisEntity = new TeamManagerHisEntity();
                        teamManagerHisEntity.setChTeamCode(teamList.get(i).getChTeamCode());
                        teamManagerHisEntity.setChTeamName(teamList.get(i).getChTeamName());
                        teamManagerHisEntity.setChOrgId(teamList.get(i).getChOrgId());
                        teamManagerHisEntity.setChUpdateKtyp("卸任团队负责人");
                        teamManagerHisEntity.setChEmpleeCode(teamList.get(i).getLeaderId());
                        teamManagerHisEntity.setChEmpleeName(teamList.get(i).getLeaderName());
                        teamManagerHisEntity.setStartDtBefo(teamList.get(i).getTermStartDt());
                        teamManagerHisEntity.setEndDtBefo(teamList.get(i).getTermEndDt());
                        teamManagerHisEntity.setStartDtAfter(teamList.get(i).getTermStartDt());
                        teamManagerHisEntity.setEndDtAfter(teamManagerEntity.getTermStartDt());
                        teamManagerHisEntity.setSetupDt(teamList.get(i).getSetupDt());
                        teamManagerHisEntity.setChLastModify(now);
                        teamManagerHisEntity.setChLastModifier(userName);
                        newTeamManagerService.insertTeamManagerHisData(teamManagerHisEntity);
                    }
                }
                newTeamManagerService.insertTeamData(teamManagerEntity);
                //新增团队长担任团队负责人操作历史
                TeamManagerHisEntity teamManagerHisEntity = new TeamManagerHisEntity();
                teamManagerHisEntity.setChTeamCode(teamList.get(0).getChTeamCode());
                teamManagerHisEntity.setChTeamName(teamManagerEntity.getChTeamName());
                teamManagerHisEntity.setChOrgId(chOrgId);
                teamManagerHisEntity.setChUpdateKtyp("担任团队负责人");
                teamManagerHisEntity.setChEmpleeCode(teamManagerEntity.getLeaderId());
                teamManagerHisEntity.setChEmpleeName(leaderData.get(0).getChEmpleeName());
                teamManagerHisEntity.setStartDtBefo("-");
                teamManagerHisEntity.setEndDtBefo("-");
                teamManagerHisEntity.setStartDtAfter(teamManagerEntity.getTermStartDt());
                teamManagerHisEntity.setEndDtAfter(teamManagerEntity.getTermEndDt());
                teamManagerHisEntity.setSetupDt(teamManagerEntity.getSetupDt());
                teamManagerHisEntity.setChLastModify(now);
                teamManagerHisEntity.setChLastModifier(userName);
                newTeamManagerService.insertTeamManagerHisData(teamManagerHisEntity);
                //新增团队成员数据
                TeamAndEmployeeEntity employeeEntity = insertTeamData(employeeList, teamList.get(0).getChTeamCode(), setupDt, chOrgId);
                if(!"success".equals(employeeEntity.getFlag())){
                    target = false;
                    platformTransactionManager.rollback(transactionStatus);    // 添加失败 回滚事务；
                    return R.error(999999, "所选团队成员：" + employeeEntity.getChEmpleeName() + "在" + setupDt + ",已加入：" + employeeEntity.getChTeamName() + "团队。请重新选择！");
                }
                //新增添加团队成员历史操作记录
                for (int i = 0; i < employeeList.size(); i++) {
                    TeamManagerHisEntity teamManagerHis = new TeamManagerHisEntity();
                    teamManagerHis.setChTeamCode(teamList.get(0).getChTeamCode());
                    teamManagerHis.setChTeamName(chTeamName);
                    teamManagerHis.setChOrgId(chOrgId);
                    teamManagerHis.setChUpdateKtyp("新增团队成员");
                    teamManagerHis.setChEmpleeCode(employeeList.get(i).getChEmpleeCode());
                    teamManagerHis.setChEmpleeName(employeeList.get(i).getChEmpleeName());
                    teamManagerHis.setStartDtBefo("-");
                    teamManagerHis.setEndDtBefo("-");
                    teamManagerHis.setStartDtAfter("-");
                    teamManagerHis.setEndDtAfter("-");
                    teamManagerHis.setSetupDt(teamManagerEntity.getSetupDt());
                    teamManagerHis.setChLastModify(formatter.format(date));
                    teamManagerHis.setChLastModifier(userName);
                    newTeamManagerService.insertTeamManagerHisData(teamManagerHis);
                }
            }else{
                //查询该机构下所有的团队编号，如果没有，则机构号+000001，如果存在则新增时在最大编号后+1，类似于0100000001,0100000002，...
                String chTeamCode = "";
                Integer maxTeamCode = null;
                List<String> strings = new ArrayList<>();
                Map orgMap = new HashMap();
                orgMap.put("chOrgId",chOrgId);
                List<String> stringList = newTeamManagerService.queryTeamCodeByOrgId(orgMap);
                if(stringList .size()==0){
                    chTeamCode = chOrgId + "000001";//如果不存在则当前机构号+000001
                }else{
                    for (int i = 0; i <stringList.size() ; i++) {
                        String teamCode = stringList.get(i);
                        strings.add(teamCode.substring(4,teamCode.length()));
                    }
                    //如果存在，则机构号+当前机构团队最大编号+1
                    maxTeamCode = Integer.valueOf(Collections.max(strings))+1;
                    //判断是否满足六位数，不满足补0
                    if (maxTeamCode.toString().length() == 6){
                        chTeamCode = chOrgId.substring(0,4) + maxTeamCode;
                    } else if (maxTeamCode.toString().length() == 5){
                        chTeamCode = chOrgId.substring(0,4) + "0" + maxTeamCode;
                    }else if (maxTeamCode.toString().length() == 4){
                        chTeamCode = chOrgId.substring(0,4) + "00" + maxTeamCode;
                    }else if (maxTeamCode.toString().length() == 3){
                        chTeamCode = chOrgId.substring(0,4) + "000" + maxTeamCode;
                    }else if (maxTeamCode.toString().length() == 2){
                        chTeamCode = chOrgId.substring(0,4) + "0000" + maxTeamCode;
                    }else{
                        chTeamCode = chOrgId.substring(0,4) + "00000" + maxTeamCode;
                    }
                }
                teamManagerEntity.setChTeamCode(chTeamCode);
                newTeamManagerService.insertTeamData(teamManagerEntity);
                //新增团队长担任团队负责人操作历史
                TeamManagerHisEntity teamManagerHisEntity = new TeamManagerHisEntity();
                teamManagerHisEntity.setChTeamCode(chTeamCode);
                teamManagerHisEntity.setChTeamName(teamManagerEntity.getChTeamName());
                teamManagerHisEntity.setChOrgId(chOrgId);
                teamManagerHisEntity.setChUpdateKtyp("担任团队负责人");
                teamManagerHisEntity.setChEmpleeCode(teamManagerEntity.getLeaderId());
                teamManagerHisEntity.setChEmpleeName(leaderData.get(0).getChEmpleeName());
                teamManagerHisEntity.setStartDtBefo("-");
                teamManagerHisEntity.setEndDtBefo("-");
                teamManagerHisEntity.setStartDtAfter(teamManagerEntity.getTermStartDt());
                teamManagerHisEntity.setEndDtAfter(teamManagerEntity.getTermEndDt());
                teamManagerHisEntity.setSetupDt(teamManagerEntity.getSetupDt());
                teamManagerHisEntity.setChLastModify(now);
                teamManagerHisEntity.setChLastModifier(userName);
                newTeamManagerService.insertTeamManagerHisData(teamManagerHisEntity);
                //新增团队成员数据
                TeamAndEmployeeEntity employeeEntity = insertTeamData(employeeList, chTeamCode, setupDt, chOrgId);
                if(!"success".equals(employeeEntity.getFlag())){
                    target = false;
                    platformTransactionManager.rollback(transactionStatus);    // 添加失败 回滚事务；
                    return R.error(999999, "所选团队成员：" + employeeEntity.getChEmpleeName() + "在" + setupDt + ",已加入：" + employeeEntity.getChTeamName() + "团队。请重新选择！");
                }
                //新增添加团队成员历史操作记录
                for (int i = 0; i < employeeList.size(); i++) {
                    TeamManagerHisEntity teamManagerHis = new TeamManagerHisEntity();
                    teamManagerHis.setChTeamCode(chTeamCode);
                    teamManagerHis.setChTeamName(chTeamName);
                    teamManagerHis.setChOrgId(chOrgId);
                    teamManagerHis.setChUpdateKtyp("新增团队成员");
                    teamManagerHis.setChEmpleeCode(employeeList.get(i).getChEmpleeCode());
                    teamManagerHis.setChEmpleeName(employeeList.get(i).getChEmpleeName());
                    teamManagerHis.setStartDtBefo("-");
                    teamManagerHis.setEndDtBefo("-");
                    teamManagerHis.setStartDtAfter("-");
                    teamManagerHis.setEndDtAfter("-");
                    teamManagerHis.setSetupDt(teamManagerEntity.getSetupDt());
                    teamManagerHis.setChLastModify(formatter.format(date));
                    teamManagerHis.setChLastModifier(userName);
                    newTeamManagerService.insertTeamManagerHisData(teamManagerHis);
                }
            }
            if(target == false){
                platformTransactionManager.rollback(transactionStatus);    // 添加失败 回滚事务；
            }else{
                platformTransactionManager.commit(transactionStatus);      //提交事务
            }
            return R.ok();
        }
    }

    /**
     * 修改时回显人员信息
     */
    @RequestMapping("/shouwTeamEmployeeList")
    public R shouwTeamEmployeeList(@RequestParam Map<String, Object> params){
        //查询团队绑定成员信息
        List<TeamAndEmployeeEntity> empleeList = newTeamManagerService.queryTeamEmployeeByTeamId(params);
        return R.ok().put("empleeList", empleeList);
    }

    /**
     * 添加团队成员公共方法
     * @param employeeList
     * @param chTeamCode
     * @return
     */
    public  TeamAndEmployeeEntity insertTeamData(List<TeamAndEmployeeEntity> employeeList,String chTeamCode,String setupDt,String chOrgId){
        Map params = new HashMap();
        //创建人
        String userName = getUser().getName();
        //当前系统时间
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(System.currentTimeMillis());
        for (int i = 0; i <employeeList.size() ; i++) {
            Map reqPara = new HashMap();
            //团队成员编号
            String chEmpleeCode = employeeList.get(i).getChEmpleeCode();
            //团队成员名称
            String chEmpleeName = employeeList.get(i).getChEmpleeName();
            reqPara.put("chEmpleeCode",chEmpleeCode);
            reqPara.put("setupDt",setupDt);
            reqPara.put("chOrgId",chOrgId);
            //新增团队成员信息之前查询是否已经加入其它团队
            TeamAndEmployeeEntity employeeEntity = newTeamManagerService.checkEmpleeExist(reqPara);
            if(employeeEntity !=null){
                return employeeEntity;
//                return R.error(999999, "所选团队成员：" + chEmpleeName + "在" + setupDt + ",已加入：" + chTeamName + "团队。请重新选择！");
            }else{
                //新增团队成员信息
                params.put("chTeamCode",chTeamCode);
                params.put("chOrgId",chOrgId);
                params.put("chEmpleeCode",chEmpleeCode);
                params.put("chEmpleeName",chEmpleeName);
                params.put("chLastModify",formatter.format(date));
                params.put("chLastModifier",userName);
                params.put("status","0");
                params.put("setupDt",setupDt);
                newTeamManagerService.insertEmployTeam(params);
            }
        }
        TeamAndEmployeeEntity employee = new TeamAndEmployeeEntity();
        employee.setFlag("success");
        return employee;
    }

    /**
     * 修改团队信息
     */
    @RequestMapping("/updateTeamData")
    @SysLog("修改团队信息")
    @Transactional(rollbackFor = Exception.class)
    public R updateTeamData(@RequestBody TeamManagerEntity teamManagerEntity) throws ParseException {
        //关闭事务自动提交
        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
        TransactionStatus transactionStatus = platformTransactionManager.getTransaction(def);
        //事务标识
        boolean target = true;
        Map params = new HashMap();
        Map teamParams = new HashMap();
        Map empParams = new HashMap();
        //团队编号
        String chTeamCode = teamManagerEntity.getChTeamCode();
        //团队名称
        String chTeamName = teamManagerEntity.getChTeamName();
        //前台传过来的团队所属日期
        String setupDt = teamManagerEntity.getSetupDt();
        //修改之前查询原团队数据
        teamParams.put("chTeamCode",chTeamCode);
        teamParams.put("chTeamName",chTeamName);
        teamParams.put("setupDt",setupDt);
        teamParams.put("chOrgId",teamManagerEntity.getChOrgId());
        List<TeamManagerEntity> teamList = newTeamManagerService.queryTeamDataList(teamParams);
        //修改之前团队成员信息
        empParams.put("chTeamCode",chTeamCode);
        empParams.put("setupDt",setupDt);
        empParams.put("chOrgId",teamManagerEntity.getChOrgId());
        List<TeamAndEmployeeEntity> employeeListOld = newTeamManagerService.queryTeamEmployeeByTeamId(empParams);
        //原团队负责人编号
        String leaderIdOld = teamList.get(0).getLeaderId();
        //前台传过来的团队负责人编号
        String leaderIdNew = teamManagerEntity.getLeaderId();
        //前台传过来的团队成员数据
        List<TeamAndEmployeeEntity> employeeListNew = teamManagerEntity.getList();
        //获取原团队成员编号组成的集合
        List<String> listA = new ArrayList<>();
        for (int i = 0; i <employeeListOld.size() ; i++) {
            listA.add(employeeListOld.get(i).getChEmpleeCode());
        }
        //获取新团队成员编号组成的集合
        List<String> listB = new ArrayList<>();
        for (int i = 0; i <employeeListNew.size() ; i++) {
            listB.add(employeeListNew.get(i).getChEmpleeCode());
        }
        //创建人
        String userName = getUser().getName();
        //设置最后修改人
        teamManagerEntity.setChLastModifier(userName);
        //原团队负责人任期结束时间
        String termEndDtOld = teamList.get(0).getTermEndDt();
        //当前系统时间
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(System.currentTimeMillis());
        String now = formatter.format(date);
        //查询团队长所属机构
        params.put("custMngrId",leaderIdNew);
        String chOrgId = newTeamManagerService.selectLeaderOrg(params);
        //查询前台传过来的团队负责人名称
        List<TeamAndEmployeeEntity> leaderData = newTeamManagerService.teamLeaderData(params);
          if(!leaderIdOld.equals(leaderIdNew)){
            //如果团队负责人改变，新增一条关于该团队的数据
            Date sDate = formatter.parse(termEndDtOld); //原团队负责人任期结束时间
            if(formatter.parse(now).compareTo(sDate) < 0 ){//先查询该团队原负责人的任期结束时间，如果大于当前操作的系统时间，则将任期结束时间修改为新的团队负责人上任时间
                TeamManagerEntity entity = new TeamManagerEntity();
                entity.setChTeamCode(chTeamCode);
                entity.setTermEndDt(teamManagerEntity.getTermStartDt());
                entity.setLeaderIdOld(leaderIdOld);
                entity.setStatus("1");//离开团队
                newTeamManagerService.updateTeamData(entity);
                //新增团队长卸任团队负责人操作历史
                TeamManagerHisEntity teamManagerHisEntity = new TeamManagerHisEntity();
                teamManagerHisEntity.setChTeamCode(teamList.get(0).getChTeamCode());
                teamManagerHisEntity.setChTeamName(teamList.get(0).getChTeamName());
                teamManagerHisEntity.setChOrgId(teamList.get(0).getChOrgId());
                teamManagerHisEntity.setChUpdateKtyp("卸任团队负责人");
                teamManagerHisEntity.setChEmpleeCode(teamList.get(0).getLeaderId());
                teamManagerHisEntity.setChEmpleeName(teamList.get(0).getLeaderName());
                teamManagerHisEntity.setStartDtBefo(teamList.get(0).getTermStartDt());
                teamManagerHisEntity.setEndDtBefo(teamList.get(0).getTermEndDt());
                teamManagerHisEntity.setStartDtAfter(teamList.get(0).getTermStartDt());
                teamManagerHisEntity.setEndDtAfter(teamManagerEntity.getTermStartDt());
                teamManagerHisEntity.setSetupDt(teamList.get(0).getSetupDt());
                teamManagerHisEntity.setChLastModify(now);
                teamManagerHisEntity.setChLastModifier(userName);
                newTeamManagerService.insertTeamManagerHisData(teamManagerHisEntity);
            }else {
                TeamManagerEntity entity = new TeamManagerEntity();
                entity.setChTeamCode(chTeamCode);
                entity.setLeaderIdOld(leaderIdOld);
                entity.setStatus("1");//离开团队
                newTeamManagerService.updateTeamData(entity);
                //新增团队长卸任团队负责人操作历史
                TeamManagerHisEntity teamManagerHisEntity = new TeamManagerHisEntity();
                teamManagerHisEntity.setChTeamCode(teamList.get(0).getChTeamCode());
                teamManagerHisEntity.setChTeamName(teamList.get(0).getChTeamName());
                teamManagerHisEntity.setChOrgId(teamList.get(0).getChOrgId());
                teamManagerHisEntity.setChUpdateKtyp("卸任团队负责人");
                teamManagerHisEntity.setChEmpleeCode(teamList.get(0).getLeaderId());
                teamManagerHisEntity.setChEmpleeName(teamList.get(0).getLeaderName());
                teamManagerHisEntity.setStartDtBefo(teamList.get(0).getTermStartDt());
                teamManagerHisEntity.setEndDtBefo(teamList.get(0).getTermEndDt());
                teamManagerHisEntity.setStartDtAfter(teamList.get(0).getTermStartDt());
                teamManagerHisEntity.setEndDtAfter(teamList.get(0).getTermEndDt());
                teamManagerHisEntity.setSetupDt(teamList.get(0).getSetupDt());
                teamManagerHisEntity.setChLastModify(now);
                teamManagerHisEntity.setChLastModifier(userName);
                newTeamManagerService.insertTeamManagerHisData(teamManagerHisEntity);
            }
            //新增一条关于该团队的新的团队负责人数据
            //新增团队和团队成员相关数据
            teamManagerEntity.setChTeamCode(chTeamCode);
            teamManagerEntity.setChOrgId(chOrgId);
            teamManagerEntity.setChLastModify(now);
            teamManagerEntity.setChLastModifier(userName);
            teamManagerEntity.setCreateUserId(userName);
            teamManagerEntity.setStatus("0");
            newTeamManagerService.insertTeamData(teamManagerEntity);
            //新增团队长担任团队负责人操作历史
            TeamManagerHisEntity teamManagerHisEntity = new TeamManagerHisEntity();
            teamManagerHisEntity.setChTeamCode(teamList.get(0).getChTeamCode());
            teamManagerHisEntity.setChTeamName(teamManagerEntity.getChTeamName());
            teamManagerHisEntity.setChOrgId(chOrgId);
            teamManagerHisEntity.setChUpdateKtyp("担任团队负责人");
            teamManagerHisEntity.setChEmpleeCode(teamManagerEntity.getLeaderId());
            teamManagerHisEntity.setChEmpleeName(leaderData.get(0).getChEmpleeName());
            teamManagerHisEntity.setStartDtBefo("-");
            teamManagerHisEntity.setEndDtBefo("-");
            teamManagerHisEntity.setStartDtAfter(teamManagerEntity.getTermStartDt());
            teamManagerHisEntity.setEndDtAfter(teamManagerEntity.getTermEndDt());
            teamManagerHisEntity.setSetupDt(teamManagerEntity.getSetupDt());
            teamManagerHisEntity.setChLastModify(now);
            teamManagerHisEntity.setChLastModifier(userName);
            newTeamManagerService.insertTeamManagerHisData(teamManagerHisEntity);
            //如果团队成员有移除，则将团队成员状态设置为离开团队
            for (int i = 0; i < listA.size(); i++) {
                String empleeCode = listA.get(i);//团队成员编号
                if(!listB.contains(empleeCode)){//不在前台传过来的团队成员集合中，则为移除操作
                    Map reqMap = new HashMap();
                    reqMap.put("chTeamCode",chTeamCode);
                    reqMap.put("chEmpleeCode",empleeCode);
                    reqMap.put("status",1);//离开团队
                    reqMap.put("chLastModify",now);
                    reqMap.put("chLastModifier",userName);
                    newTeamManagerService.updateEmployTeam(reqMap);
                    TeamManagerHisEntity teamManagerHis = new TeamManagerHisEntity();
                    teamManagerHis.setChTeamCode(chTeamCode);
                    teamManagerHis.setChTeamName(chTeamName);
                    teamManagerHis.setChOrgId(chOrgId);
                    teamManagerHis.setChUpdateKtyp("移除团队成员");
                    teamManagerHis.setChEmpleeCode(employeeListOld.get(i).getChEmpleeCode());
                    teamManagerHis.setChEmpleeName(employeeListOld.get(i).getChEmpleeName());
                    teamManagerHis.setStartDtBefo("-");
                    teamManagerHis.setEndDtBefo("-");
                    teamManagerHis.setStartDtAfter("-");
                    teamManagerHis.setEndDtAfter("-");
                    teamManagerHis.setSetupDt(teamManagerEntity.getSetupDt());
                    teamManagerHis.setChLastModify(formatter.format(date));
                    teamManagerHis.setChLastModifier(userName);
                    newTeamManagerService.insertTeamManagerHisData(teamManagerHis);
                }
            }
              //如果团队成员有新增，则将添加一条新增团队成员历史操作记录
              for (int i = 0; i < listB.size(); i++) {
                  String empleeCode = listB.get(i);//团队成员编号
                  if(!listA.contains(empleeCode)){//不在前台传过来的团队成员集合中，则为新增操作
                      TeamManagerHisEntity teamManagerHis = new TeamManagerHisEntity();
                      teamManagerHis.setChTeamCode(chTeamCode);
                      teamManagerHis.setChTeamName(chTeamName);
                      teamManagerHis.setChOrgId(chOrgId);
                      teamManagerHis.setChUpdateKtyp("新增团队成员");
                      teamManagerHis.setChEmpleeCode(employeeListNew.get(i).getChEmpleeCode());
                      teamManagerHis.setChEmpleeName(employeeListNew.get(i).getChEmpleeName());
                      teamManagerHis.setStartDtBefo("-");
                      teamManagerHis.setEndDtBefo("-");
                      teamManagerHis.setStartDtAfter("-");
                      teamManagerHis.setEndDtAfter("-");
                      teamManagerHis.setSetupDt(teamManagerEntity.getSetupDt());
                      teamManagerHis.setChLastModify(formatter.format(date));
                      teamManagerHis.setChLastModifier(userName);
                      newTeamManagerService.insertTeamManagerHisData(teamManagerHis);
                  }
              }
            //删除没有移除的原团队成员(全删全增)
            for (int i = 0; i < listB.size(); i++) {
                String empleeCode = listB.get(i);//团队成员编号
                if(listA.contains(empleeCode)){//不在前台传过来的团队成员集合中，则为移除操作
                    Map delMap = new HashMap();
                    delMap.put("chTeamCode",chTeamCode);
                    delMap.put("chEmpleeCode",employeeListNew.get(i).getChEmpleeCode());
                    delMap.put("setupDt",employeeListNew.get(i).getSetupDt());
                    newTeamManagerService.deleteTeamAndEmployee(delMap);
                }
            }
           //新增团队成员数据
          TeamAndEmployeeEntity employeeEntity = insertTeamData(employeeListNew, chTeamCode, setupDt, chOrgId);
          if(!"success".equals(employeeEntity.getFlag())){
              target = false;
              platformTransactionManager.rollback(transactionStatus);    // 添加失败 回滚事务；
              return R.error(999999, "所选团队成员：" + employeeEntity.getChEmpleeName() + "在" + setupDt + ",已加入：" + employeeEntity.getChTeamName() + "团队。请重新选择！");
          }
        }else if(leaderIdOld.equals(leaderIdNew)){
            //如果团队负责人没有改变,则直接修改团队信息
            newTeamManagerService.updateTeamData(teamManagerEntity);
            //如果团队成员有移除，则将团队成员状态设置为离开团队
            for (int i = 0; i < listA.size(); i++) {
                String empleeCode = listA.get(i);//团队成员编号
                if(!listB.contains(empleeCode)){//不在前台传过来的团队成员集合中，则为移除操作
                    Map reqMap = new HashMap();
                    reqMap.put("chTeamCode",chTeamCode);
                    reqMap.put("chEmpleeCode",empleeCode);
                    reqMap.put("status",1);//离开团队
                    reqMap.put("chLastModify",now);
                    reqMap.put("chLastModifier",userName);
                    newTeamManagerService.updateEmployTeam(reqMap);
                    TeamManagerHisEntity teamManagerHis = new TeamManagerHisEntity();
                    teamManagerHis.setChTeamCode(chTeamCode);
                    teamManagerHis.setChTeamName(chTeamName);
                    teamManagerHis.setChOrgId(chOrgId);
                    teamManagerHis.setChUpdateKtyp("移除团队成员");
                    teamManagerHis.setChEmpleeCode(employeeListOld.get(i).getChEmpleeCode());
                    teamManagerHis.setChEmpleeName(employeeListOld.get(i).getChEmpleeName());
                    teamManagerHis.setStartDtBefo("-");
                    teamManagerHis.setEndDtBefo("-");
                    teamManagerHis.setStartDtAfter("-");
                    teamManagerHis.setEndDtAfter("-");
                    teamManagerHis.setSetupDt(teamManagerEntity.getSetupDt());
                    teamManagerHis.setChLastModify(formatter.format(date));
                    teamManagerHis.setChLastModifier(userName);
                    newTeamManagerService.insertTeamManagerHisData(teamManagerHis);
                }
            }
              //如果团队成员有新增，则将添加一条新增团队成员历史操作记录
              for (int i = 0; i < listB.size(); i++) {
                  String empleeCode = listB.get(i);//团队成员编号
                  if(!listA.contains(empleeCode)){//不在前台传过来的团队成员集合中，则为新增操作
                      TeamManagerHisEntity teamManagerHis = new TeamManagerHisEntity();
                      teamManagerHis.setChTeamCode(chTeamCode);
                      teamManagerHis.setChTeamName(chTeamName);
                      teamManagerHis.setChOrgId(chOrgId);
                      teamManagerHis.setChUpdateKtyp("新增团队成员");
                      teamManagerHis.setChEmpleeCode(employeeListNew.get(i).getChEmpleeCode());
                      teamManagerHis.setChEmpleeName(employeeListNew.get(i).getChEmpleeName());
                      teamManagerHis.setStartDtBefo("-");
                      teamManagerHis.setEndDtBefo("-");
                      teamManagerHis.setStartDtAfter("-");
                      teamManagerHis.setEndDtAfter("-");
                      teamManagerHis.setSetupDt(teamManagerEntity.getSetupDt());
                      teamManagerHis.setChLastModify(formatter.format(date));
                      teamManagerHis.setChLastModifier(userName);
                      newTeamManagerService.insertTeamManagerHisData(teamManagerHis);
                  }
              }
            //删除没有移除的原团队成员(全删全增)
            for (int i = 0; i < listB.size(); i++) {
                String empleeCode = listB.get(i);//团队成员编号
                if(listA.contains(empleeCode)){//前台传过来的团队成员集合中
                    Map delMap = new HashMap();
                    delMap.put("chTeamCode",chTeamCode);
                    delMap.put("chEmpleeCode",employeeListNew.get(i).getChEmpleeCode());
                    delMap.put("setupDt",employeeListNew.get(i).getSetupDt());
                    newTeamManagerService.deleteTeamAndEmployee(delMap);
                }
            }
              //新增团队成员数据
              TeamAndEmployeeEntity employeeEntity = insertTeamData(employeeListNew, chTeamCode, setupDt, chOrgId);
              if(!"success".equals(employeeEntity.getFlag())){
                  target = false;
                  platformTransactionManager.rollback(transactionStatus);    // 添加失败 回滚事务；
                  return R.error(999999, "所选团队成员：" + employeeEntity.getChEmpleeName() + "在" + setupDt + ",已加入：" + employeeEntity.getChTeamName() + "团队。请重新选择！");
              }
            //如果团队负责人任期时间改动，则新增团队负责人任期时间变更操作
            if(!teamList.get(0).getTermStartDt().equals(teamManagerEntity.getTermStartDt()) || !teamList.get(0).getTermEndDt().equals(teamManagerEntity.getTermEndDt())){
                //新增团队长卸任团队负责人操作历史
                TeamManagerHisEntity teamManagerHisEntity = new TeamManagerHisEntity();
                teamManagerHisEntity.setChTeamCode(teamList.get(0).getChTeamCode());
                teamManagerHisEntity.setChTeamName(teamList.get(0).getChTeamName());
                teamManagerHisEntity.setChOrgId(teamList.get(0).getChOrgId());
                teamManagerHisEntity.setChUpdateKtyp("团队负责人任期时间变更");
                teamManagerHisEntity.setChEmpleeCode(teamList.get(0).getLeaderId());
                teamManagerHisEntity.setChEmpleeName(teamList.get(0).getLeaderName());
                teamManagerHisEntity.setStartDtBefo(teamList.get(0).getTermStartDt());
                teamManagerHisEntity.setEndDtBefo(teamList.get(0).getTermEndDt());
                teamManagerHisEntity.setStartDtAfter(teamManagerEntity.getTermStartDt());
                teamManagerHisEntity.setEndDtAfter(teamManagerEntity.getTermEndDt());
                teamManagerHisEntity.setSetupDt(teamList.get(0).getSetupDt());
                teamManagerHisEntity.setChLastModify(now);
                teamManagerHisEntity.setChLastModifier(userName);
                newTeamManagerService.insertTeamManagerHisData(teamManagerHisEntity);
            }
        }
        if(target == false){
            platformTransactionManager.rollback(transactionStatus);    // 添加失败 回滚事务；
        }else{
            platformTransactionManager.commit(transactionStatus);      //提交事务
        }
        return R.ok();
    }

    /**
     * 删除团队信息
     */
    @RequestMapping("/deleteTeamData")
    @SysLog("删除团队信息")
    public R deleteTeamData(@RequestParam Map<String, Object> reqPara) {
        //团队编号
        String chTeamCode = (String)reqPara.get("chTeamCode");
        //团队所属月份
        String setupDt = (String)reqPara.get("setupDt");
        //操作人
        String userName = getUser().getName();
        //当前系统时间
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(System.currentTimeMillis());
        //修改之前查询原团队数据
        Map teamParams = new HashMap();
        teamParams.put("chTeamCode",chTeamCode);
        teamParams.put("setupDt",setupDt);
        List<TeamManagerEntity> teamList = newTeamManagerService.queryTeamDataList(teamParams);
        int i = newTeamManagerService.checkEmployeesExist(reqPara);
        if(i > 0){
            return R.error(999999, "该团队下面存在队员，不可删除！如果删除请将团队下面成员进行移除。");
        }else{
            TeamManagerEntity entity = new TeamManagerEntity();
            entity.setChTeamCode(chTeamCode);
            entity.setSetupDt(setupDt);
            entity.setStatus("1");//离开团队,意味着团队删除(逻辑删除)
            newTeamManagerService.updateTeamData(entity);
            TeamManagerHisEntity teamManagerHis = new TeamManagerHisEntity();
            teamManagerHis.setChTeamCode(chTeamCode);
            teamManagerHis.setChTeamName(teamList.get(0).getChTeamName());
            teamManagerHis.setChOrgId(teamList.get(0).getChOrgId());
            teamManagerHis.setChUpdateKtyp("团队删除");
            teamManagerHis.setChEmpleeCode("-");
            teamManagerHis.setChEmpleeName("-");
            teamManagerHis.setStartDtBefo("-");
            teamManagerHis.setEndDtBefo("-");
            teamManagerHis.setStartDtAfter("-");
            teamManagerHis.setEndDtAfter("-");
            teamManagerHis.setSetupDt(teamList.get(0).getSetupDt());
            teamManagerHis.setChLastModify(formatter.format(date));
            teamManagerHis.setChLastModifier(userName);
            newTeamManagerService.insertTeamManagerHisData(teamManagerHis);
            return R.ok();
        }
    }

    /**
     * 团队成员信息
     * @param params
     * @return
     */
    @SysLog("查看团队成员信息详情")
    @RequestMapping("/teamEmployeelist")
    public R teamEmployeelist(@RequestParam Map<String, Object> params) {
        String chOrgId = (String)params.get("chOrgId");
        int pageNo = Integer.parseInt((String) params.get("page"));
        int pageCount = Integer.parseInt((String)params.get("limit"));
        int offset = ((pageNo - 1) * pageCount);
        params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
        params.put("offset", offset);
        params.put("chOrgId", chOrgId);
        List<TeamAndEmployeeEntity> teamEmployeelist = newTeamManagerService.queryTeamEmployeeList(params);
        int count = newTeamManagerService.queryTeamEmployeeNb(params);
        return   R.ok().put("teamEmployeelist", teamEmployeelist).put("count", count);
    }

    /**
     * 团队操作日志信息
     * @param params
     * @return
     */
    @SysLog("查看团队操作日志信息详情")
    @RequestMapping("/teamHislist")
    public R teamHislist(@RequestParam Map<String, Object> params) {
        String chOrgId = (String)params.get("chOrgId");
        int pageNo = Integer.parseInt((String) params.get("page"));
        int pageCount = Integer.parseInt((String)params.get("limit"));
        int offset = ((pageNo - 1) * pageCount);
        params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
        params.put("offset", offset);
        params.put("chOrgId", chOrgId);
        List<TeamManagerHisEntity> teamHislist = newTeamManagerService.queryTeamHisList(params);
        int count = newTeamManagerService.queryTeamHisListNb(params);
        return   R.ok().put("teamHislist", teamHislist).put("count", count);
    }

    /**
     * 同步大数据平台团队数据
     * @param params
     * @return
     */
    @SysLog("同步大数据平台团队数据")
    @RequestMapping("/batchTeamData")
    public R batchTeamData(@RequestParam Map<String, Object> params) {
        //团队编号
        String chTeamCode = (String) params.get("chTeamCode");
        //团队所属月份
        String setupDt = (String) params.get("setupDt");
        params.put("chTeamCode",chTeamCode);
        params.put("setupDt",setupDt);
        //查询团队信息表数据
        List<Map<String, Object>> teamDataList = scheduleJobDao.selectTeamDate(params);
        //查询团队信息表数据
        List<Map<String,Object>> teamEmployeeDataList = scheduleJobDao.selectTeamEmployeeDate(params);
        //执行插入团队表数据操作
        Connection conn1 = PgsqlUtil.getConn();
        PreparedStatement pre1 = null; //预编译
        ResultSet rs1 = null;
        long TimeStart = System.currentTimeMillis();//获取当前时间
        //先根据团队编号与团队所属月份执行删除数据操作
        String sqlOne = "delete from npmm.NB_TEAM_NEW where ch_team_code = '" + chTeamCode + "' and setup_dt = "+ "'" + setupDt + "'";
        //执行插入数据操作
        String sqlTwo = "INSERT\n" +
                "INTO\n" +
                "    NPMM.NB_TEAM_NEW\n" +
                "    (\n" +
                "        CH_TEAM_CODE,\n" +
                "        CH_TEAM_NAME,\n" +
                "        CH_TEAM_DESC,\n" +
                "        CH_ORG_ID,\n" +
                "        LEADER_ID,\n" +
                "        TERM_START_DT,\n" +
                "        TERM_END_DT,\n" +
                "        SETUP_DT,\n" +
                "        CREATE_USER_ID,\n" +
                "        CH_LAST_MODIFY,\n" +
                "        CH_LAST_MODIFIER,\n" +
                "        STATUS\n" +
                "    )\n" +
                "    VALUES\n" +
                "    (\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?\n" +
                "    )";
        try {
            // 取消自动提交（由于要进行多次操作）
            conn1.setAutoCommit(false);
            pre1 = conn1.prepareStatement(sqlOne);
            pre1.executeUpdate();
            pre1 = conn1.prepareStatement(sqlTwo);
            for (int i = 0; i < teamDataList.size(); i++) {
                pre1.setString(1, teamDataList.get(i).get("CH_TEAM_CODE").toString());
                pre1.setString(2, teamDataList.get(i).get("CH_TEAM_NAME").toString());
                pre1.setString(3, teamDataList.get(i).get("CH_TEAM_DESC").toString());
                pre1.setString(4, teamDataList.get(i).get("CH_ORG_ID").toString());
                pre1.setString(5, teamDataList.get(i).get("LEADER_ID").toString());
                pre1.setString(6, teamDataList.get(i).get("TERM_START_DT").toString());
                pre1.setString(7, teamDataList.get(i).get("TERM_END_DT").toString());
                pre1.setString(8, teamDataList.get(i).get("SETUP_DT").toString());
                pre1.setString(9, teamDataList.get(i).get("CREATE_USER_ID").toString());
                pre1.setString(10, teamDataList.get(i).get("CH_LAST_MODIFY").toString());
                pre1.setString(11, teamDataList.get(i).get("CH_LAST_MODIFIER").toString());
                pre1.setString(12, teamDataList.get(i).get("STATUS").toString());
                pre1.addBatch();//批量插入
            }
            pre1.executeBatch();
            conn1.commit();
            pre1.clearBatch();
        } catch (SQLException e) {
            e.printStackTrace();
            return R.error(9999,"同步数据失败！请联系管理员。");
            // 发生异常，回滚
        } finally {
            PgsqlUtil.close(conn1, pre1, rs1);
        }

        //执行插入团队表数据操作
        Connection conn2 = PgsqlUtil.getConn();
        PreparedStatement pre2 = null; //预编译
        ResultSet rs2 = null;
        //先根据团队编号与团队所属月份执行删除数据操作
        String sqlThree = "delete from npmm.NB_TEAM_EMPLEE_NEW where ch_team_code = '" + chTeamCode + "' and setup_dt = "+ "'" + setupDt + "'";
        //执行插入数据操作
        String sqlFour = "INSERT\n" +
                "INTO\n" +
                "    NPMM.NB_TEAM_EMPLEE_NEW\n" +
                "    (\n" +
                "        CH_TEAM_CODE,\n" +
                "        CH_ORG_ID,\n" +
                "        CH_EMPLEE_CODE,\n" +
                "        CH_EMPLEE_NAME,\n" +
                "        SETUP_DT,\n" +
                "        CH_LAST_MODIFY,\n" +
                "        CH_LAST_MODIFIER,\n" +
                "        STATUS\n" +
                "    )\n" +
                "    VALUES\n" +
                "    (\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?,\n" +
                "        ?\n" +
                "    )";
        try {
            // 取消自动提交（由于要进行多次操作）
            conn2.setAutoCommit(false);
            pre2 = conn2.prepareStatement(sqlThree);
            pre2.executeUpdate();
            pre2 = conn2.prepareStatement(sqlFour);
            for (int i = 0; i <teamEmployeeDataList.size() ; i++) {
                pre2.setString(1,teamEmployeeDataList.get(i).get("CH_TEAM_CODE").toString());
                pre2.setString(2,teamEmployeeDataList.get(i).get("CH_ORG_ID").toString());
                pre2.setString(3,teamEmployeeDataList.get(i).get("CH_EMPLEE_CODE").toString());
                pre2.setString(4,teamEmployeeDataList.get(i).get("CH_EMPLEE_NAME").toString());
                pre2.setString(5,teamEmployeeDataList.get(i).get("SETUP_DT").toString());
                pre2.setString(6,teamEmployeeDataList.get(i).get("CH_LAST_MODIFY").toString());
                pre2.setString(7,teamEmployeeDataList.get(i).get("CH_LAST_MODIFIER").toString());
                pre2.setString(8,teamEmployeeDataList.get(i).get("STATUS").toString());
                pre2.addBatch();//批量插入
            }
            pre2.executeBatch();
            conn2.commit();
            pre2.clearBatch();
        } catch (SQLException e) {
            e.printStackTrace();
            return R.error(9999,"同步数据失败！请联系管理员。");
            // 发生异常，回滚
        } finally {
            PgsqlUtil.close(conn2, pre2, rs2);
        }

        long TimeStop = System.currentTimeMillis();
        System.out.println("总耗时：" + (TimeStop - TimeStart));
        return R.ok();
    }
}
