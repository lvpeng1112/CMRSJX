/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft.modules.newTeam.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 团队历史实体类
 *
 */
@Data
@TableName("nb_team_his_new")
public class TeamManagerHisEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	private String chTeamCode;
	private String chTeamName;
	private String chOrgId;
    private String chdeptname;
	private String chUpdateKtyp;
	private String chEmpleeCode;
	private String chEmpleeName;
	private String startDtBefo;
	private String endDtBefo;
    private String startDtAfter;
    private String endDtAfter;
    private String setupDt;
	private String chLastModify;
	private String chLastModifier;
}
