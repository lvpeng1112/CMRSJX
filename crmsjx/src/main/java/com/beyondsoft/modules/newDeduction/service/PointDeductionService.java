package com.beyondsoft.modules.newDeduction.service;
import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.newDeduction.entity.PointDeductionEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
public interface PointDeductionService extends IService<PointDeductionEntity>{
    List<PointDeductionEntity> queryDeductionList(Map map);
    int queryDeductionListNb(Map map);

    R batchImport(String fileName, MultipartFile file, String org, String isKhy) throws Exception;

    void insertSingleData(PointDeductionEntity pointDeductionEntity);

    void updatedata(PointDeductionEntity pointDeductionEntity);
    void deletedata(PointDeductionEntity pointDeductionEntity);
    PointDeductionEntity getDataByYrBraIdCusId(Map<String, Object> params);
}
