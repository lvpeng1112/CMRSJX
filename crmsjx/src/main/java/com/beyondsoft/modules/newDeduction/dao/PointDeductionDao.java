package com.beyondsoft.modules.newDeduction.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.newDeduction.entity.PointDeductionEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository("PointDeductionDao")
public interface PointDeductionDao extends BaseMapper<PointDeductionEntity>{
    List<PointDeductionEntity> queryDeductionList(Map map);
    int queryDeductionListNb(Map map);
    void insertSingleData(PointDeductionEntity pointDeductionEntity);
    String queryOrgNm(Map<String, Object> params);
    void deletedata(PointDeductionEntity pointDeductionEntity);
    void updatedata(PointDeductionEntity pointDeductionEntity);
    List<PointDeductionEntity> queryYearHis(PointDeductionEntity pointDeductionEntity);
    int checkOrgId(Map<String, Object> params);
    void insertDeductionList(List<PointDeductionEntity> list);
    PointDeductionEntity getDataByYrBraIdCusId(Map<String, Object> params);
}
