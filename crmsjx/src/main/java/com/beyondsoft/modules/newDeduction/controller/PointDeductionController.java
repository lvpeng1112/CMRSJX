package com.beyondsoft.modules.newDeduction.controller;

import com.alibaba.druid.util.StringUtils;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.newDeduction.entity.PointDeductionEntity;
import com.beyondsoft.modules.newDeduction.entity.PointDeductionHistoryEntity;
import com.beyondsoft.modules.newDeduction.service.PointDeductionHistoryService;
import com.beyondsoft.modules.newDeduction.service.PointDeductionService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/deduction/pointDeduction")
public class PointDeductionController {
    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private PointDeductionService pointDeductionService;

    @Autowired
    private PointDeductionHistoryService pointDeductionHisService;

    @RequestMapping("/list")
    @ApiOperation("分页查询")
    public R list(@RequestParam Map<String, Object> params){
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy");
        Date date = new Date(System.currentTimeMillis());
        //当前系统年份
        String now = formatter.format(date);
        //当前系统年份区间
        String stDt = now + "-01";
        String enDt = now + "-12";
        if(null == params){
            return R.error("入参为空");
        }
        if(StringUtils.isEmpty((String) params.get("page")) && StringUtils.isEmpty((String) params.get("limit"))){
            return R.error("缺少必传分页参数");
        }
//        //年度
//        String yr = (String) params.get("yr");
//        //月度开始时间
//        String startDt = (String) params.get("startDt");
//        //月度结束时间
//        String endDt = (String) params.get("endDt");
//        //前台默认展示当年内控扣分
//        if("".equals(yr) && "".equals(startDt) && "".equals(endDt)){
//            params.put("now",now);
//            params.put("startDt",stDt);
//            params.put("endDt",enDt);
//        }
        List<PointDeductionEntity> pointDeductionEntityList = pointDeductionService.queryDeductionList(params);
        int count = pointDeductionService.queryDeductionListNb(params);
        return R.ok().put("pointDeductionEntityList",pointDeductionEntityList).put("count",count);
    }

    @RequestMapping("/importExcel")
    @ApiOperation("导入excel")
    public R importExcel(@RequestParam(value = "file") MultipartFile file, String org, String isKhy) throws Exception {
        String fileName = file.getOriginalFilename();
        logger.info("导入表格文件为:{}",fileName);
        return pointDeductionService.batchImport(fileName, file, org, isKhy);
    }

    @PostMapping("/insertSingleData")
    @ApiOperation("新增单条数据")
    public R insertSingleData(@RequestBody PointDeductionEntity pointDeductionEntity){
        String yr = pointDeductionEntity.getYr();
        String custId = pointDeductionEntity.getCustId();
        String branchId = pointDeductionEntity.getBranchId();
        String flag = pointDeductionEntity.getFlag();
        Map<String, Object> map = new HashMap<>();
        map.put("yr",yr);
        map.put("branchId",branchId);
        map.put("custId",custId);
        PointDeductionEntity dataByYrBraIdCusId = pointDeductionService.getDataByYrBraIdCusId(map);
        if(dataByYrBraIdCusId!=null&&!"true".equals(flag)){
            if(dataByYrBraIdCusId.getRemark()==null){
                dataByYrBraIdCusId.setRemark("");
            }
            return R.ok("tip").put("data",dataByYrBraIdCusId);
        }
        pointDeductionService.insertSingleData(pointDeductionEntity);
        return R.ok();
    }

    @PostMapping("/updatedata")
    @ApiOperation("修改单条数据")
    public R updatedata(@RequestBody PointDeductionEntity pointDeductionEntity){
        pointDeductionService.updatedata(pointDeductionEntity);
        return R.ok();
    }

    @PostMapping("/deletedata")
    @ApiOperation("删除单条数据")
    public R deletedata(@RequestBody PointDeductionEntity pointDeductionEntity){
        pointDeductionService.deletedata(pointDeductionEntity);
        return R.ok();
    }

    @ApiOperation("日志分页查询")
    @RequestMapping("/querytHisData")
    public R querytHisData(@RequestParam Map<String, Object> params){
        int pageNo = Integer.parseInt((String) params.get("page"));
        int pageCount = Integer.parseInt((String)params.get("limit"));
        int offset = ((pageNo - 1) * pageCount);
        params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
        params.put("offset", offset);
        List<PointDeductionHistoryEntity> pointDeductionHisList = pointDeductionHisService.querytHisData(params);
        int i = pointDeductionHisService.querytHisDataNb(params);
        return R.ok().put("pointDeductionHisList",pointDeductionHisList).put("count",i);
    }
}
