package com.beyondsoft.modules.newDeduction.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.datasource.annotation.DataSource;
import com.beyondsoft.modules.newDeduction.dao.PointDeductionHistoryDao;
import com.beyondsoft.modules.newDeduction.entity.PointDeductionHistoryEntity;
import com.beyondsoft.modules.newDeduction.service.PointDeductionHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
@Service
public class PointDeductionHistoryServiceImpl extends ServiceImpl<PointDeductionHistoryDao, PointDeductionHistoryEntity> implements PointDeductionHistoryService{
    @Autowired
    PointDeductionHistoryDao pointDeductionHisDao;

    @Override
    @DataSource(value = "second")
    public List<PointDeductionHistoryEntity> querytHisData(Map<String, Object> params) {
        List<PointDeductionHistoryEntity> list = pointDeductionHisDao.querytHisData(params);
        return list;
    }

    @Override
    @DataSource(value = "second")
    public int querytHisDataNb(Map<String, Object> params) {
        int i = pointDeductionHisDao.querytHisDataNb(params);
        return i;
    }
}
