package com.beyondsoft.modules.newDeduction.entity;

import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.security.Timestamp;


@ApiModel(value = "内控扣分导入表")
@TableName("C_POINT_DEDUCTION")
@Data
public class PointDeductionEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "月度")
    private String yr;

    @ApiModelProperty(value = "机构编号")
    private String branchId;

    @ApiModelProperty(value = "机构名称")
    private String branchNm;

    @ApiModelProperty(value = "客户经理编号")
    private String custId;

    @ApiModelProperty(value = "客户经理名称")
    private String custNm;

    @ApiModelProperty(value = "内控扣分")
    @JsonSerialize(using = ToStringSerializer.class)
    private Double pointDeduction;

    @ApiModelProperty(value = "扣分类型")
    private String deductionType;

    @ApiModelProperty(value = "操作员")
    private String operators;

    @ApiModelProperty(value = "操作时间")
    private String operateDate;

    @ApiModelProperty(value = "扣分原因")
    private String remark;

    @ApiModelProperty(value = "覆盖标志")
    private String flag;
}
