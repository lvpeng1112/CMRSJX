package com.beyondsoft.modules.newDeduction.entity;

import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.security.Timestamp;

@ApiModel(value = "内控扣分历史表")
@TableName("C_POINT_DEDUCTION_HIS")
@Data
public class PointDeductionHistoryEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "时间")
    private String yr;

    @ApiModelProperty(value = "机构编号")
    private String branchId;

    @ApiModelProperty(value = "机构名称")
    private String branchNm;

    @ApiModelProperty(value = "客户经理编号")
    private String custId;

    @ApiModelProperty(value = "客户经理名称")
    private String custNm;

    private String deductionType;
    @ApiModelProperty(value = "内控扣分-调整前")
    @JsonSerialize(using = ToStringSerializer.class)
    private Double pointDeductionBefo;

    @ApiModelProperty(value = "扣分原因-调整前")
    private String remarkBefo;

    @ApiModelProperty(value = "内控扣分-调整后")
    @JsonSerialize(using = ToStringSerializer.class)
    private Double pointDeductionAfter;

    @ApiModelProperty(value = "扣分原因-调整后")
    private String remarkAfter;

    @ApiModelProperty(value = "操作员")
    private String operators;

    @ApiModelProperty(value = "操作时间")
    private String operateDate;

    /*    @TableField(exist = false)*/
    @ApiModelProperty(value = "操作标识")
    private String logo;
}
