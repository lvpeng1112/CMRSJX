package com.beyondsoft.modules.newDeduction.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.newDeduction.dao.PointDeductionHistoryDao;
import com.beyondsoft.datasource.annotation.DataSource;
import com.beyondsoft.modules.newDeduction.entity.PointDeductionHistoryEntity;
import com.beyondsoft.modules.sys.entity.SysUserEntity;
import com.beyondsoft.utils.DateUtil;
import com.beyondsoft.utils.StrUtil;
import com.beyondsoft.common.utils.ShiroUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import com.beyondsoft.modules.newDeduction.dao.PointDeductionDao;
import com.beyondsoft.modules.newDeduction.entity.PointDeductionEntity;
import com.beyondsoft.modules.newDeduction.service.PointDeductionService;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PointDeductionServiceImpl extends ServiceImpl<PointDeductionDao, PointDeductionEntity> implements PointDeductionService {
    protected Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    PointDeductionDao pointDeductionDao;
    @Autowired
    PointDeductionHistoryDao pointDeductionHisDao;
    @Override
    @DataSource(value = "second")
    public List<PointDeductionEntity> queryDeductionList(Map map) {
        String limit = (String)map.get("limit");
        if(limit.length()>10){
            limit = "2147483647";
        }
        int pageNo = Integer.parseInt((String) map.get("page"));
        int pageCount = Integer.parseInt(limit);
        int offset = ((pageNo - 1) * pageCount);
        map.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
        map.put("offset", offset);
        List<PointDeductionEntity> pointDeductionEntityList = pointDeductionDao.queryDeductionList(map);
        return pointDeductionEntityList;
    }

    @Override
    @DataSource(value = "second")
    public int queryDeductionListNb(Map map) {
        int count = pointDeductionDao.queryDeductionListNb(map);
        return count;
    }

    @Override
    @DataSource(value = "second")
    public R batchImport(String fileName, MultipartFile file, String org, String isKhy) throws Exception {
        Map map = new HashMap();
        Workbook workbook = null;
        if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
            return R.error("上传格式不正确");
        }
        String extString = fileName.substring(fileName.lastIndexOf("."));
        List<PointDeductionEntity> deductionList = new ArrayList<>();
        List<PointDeductionHistoryEntity> pointDeductionHisList = new ArrayList<>();
        try{
            InputStream inputStream = file.getInputStream();
            if (".xls".equals(extString)){
                HSSFWorkbook wb = new HSSFWorkbook(inputStream);
                workbook = wb;
            }else if (".xlsx".equals(extString)){
                XSSFWorkbook wb = new XSSFWorkbook(inputStream);
                workbook = wb;
            }
            int sheets = workbook.getNumberOfSheets();
            for (int i = 0; i < sheets; i++){
                Sheet sheetAt = workbook.getSheetAt(i);
                //获取多少行
                int lastRowNum = sheetAt.getLastRowNum();
                for (int j = 1; j <= lastRowNum; j++){
                    Row row = sheetAt.getRow(j);
                    if (row != null){
                        PointDeductionEntity pointDeductionEntity = new PointDeductionEntity();
                        PointDeductionHistoryEntity pointDeductionHis = new PointDeductionHistoryEntity();
                        for (Cell cell : row){
                            cell.setCellType(Cell.CELL_TYPE_STRING);
                        }
                        if (row.getCell(0) == null || row.getCell(0).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，年/月度列没有数据");
                        }else if (row.getCell(1) == null || row.getCell(1).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，机构编号列没有数据");
                        }else if (row.getCell(3) == null || row.getCell(3).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，客户经理编号列没有数据");
                        }else if (row.getCell(4) == null || row.getCell(4).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，客户经理名称列没有数据");
                        }else if (row.getCell(5) == null || row.getCell(5).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，内控扣分列没有数据");
                        }else if (row.getCell(6) == null || row.getCell(5).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，扣分原因列没有数据");
                        }else if (row.getCell(7) == null || row.getCell(7).getStringCellValue() == ""){
                            return R.error("第" + (j + 1) + "行，扣分类型列没有数据");
                        }
                        String yr = row.getCell(0).getStringCellValue();
                        LocalDate now = LocalDate.now();
                        String year =String.valueOf(now.getYear());
                        String yearlast =String.valueOf(now.getYear()-1);
                        DateTimeFormatter dfDate = DateTimeFormatter.ofPattern("yyyyMMdd");
                        String nowF = dfDate.format(now);
                        if(yr.compareTo(yearlast)<0){
                            return R.error("第" + (j + 1) + "行,不能导入年度【" + yr + "】的扣分,年度扣分最晚于次年一月底前录入");
                        }
                        if(nowF.compareTo(year+"0131")>0&&yr.substring(0,4).equals(yearlast)){
                            return R.error("第" + (j + 1) + "行,不能导入年度【" + yr + "】的扣分,年度扣分最晚于次年一月底前录入");
                        }
                        if(yr.length()==4){
                            String inputStr = yr + "0101";
                            if (!DateUtil.isDate(inputStr)){
                                return R.error("第" + (j + 1) + "行,年度【" + yr + "】不是正确的日期格式！例:YYYY");
                            }
                        }else if (yr.length()==7){
                            String a = yr.replace("-","");
                            String inputStr = a + "01";
                            if (!DateUtil.isDate(inputStr)){
                                return R.error("第" + (j + 1) + "行,月度【" + yr + "】不是正确的日期格式！例:YYYY-MM");
                            }
                        }else{
                            return R.error("第" + (j + 1) + "行,日期【" + yr + "】不是正确的日期格式！例:YYYY-MM或YYYY");
                        }

                        // 机构编号存在check
                        String orgId = row.getCell(1).getStringCellValue();
                        Map<String, Object> orgParams = new HashMap<>();
                        orgParams.put("orgId", orgId);
                        orgParams.put("org", org);
                        orgParams.put("isKhy", isKhy);
                        int count = pointDeductionDao.checkOrgId(orgParams);
                        if (count == 0){
                            return R.error("第" + (j + 1) + "行,您没有权限导入其他机构的数据，机构编号【" + orgId + "】！");
                        }
                        String orgNm = pointDeductionDao.queryOrgNm(orgParams);
                        // 客户经理编号长度check
                        String custId = row.getCell(3).getStringCellValue();
                        if (custId.length() > 90){
                            return R.error("第" + (j + 1) + "行,客户经理编号【" + custId + "】长度不能超过90位！");
                        }
                        String custNm = row.getCell(4).getStringCellValue();
                        if (StrUtil.chineseLen(custNm) > 300){
                            return R.error("第" + (j + 1) + "行,客户名称【" + custNm + "】长度不能超过300位！");
                        }
                        // 内控扣分长度check
                        String deduction = row.getCell(5).getStringCellValue();
                        if (!StrUtil.isNumeric(deduction)) {
                            return R.error("第" + (j + 1) + "行,内控扣分【" + deduction + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(deduction, 3 , 2)) {
                            return R.error("第" + (j + 1) + "行,内控扣分【" + deduction + "】不能超过3位整数2位小数！");
                        }
                        double a = Double.parseDouble(deduction);
                        if(a>100.00){
                            return R.error("第" + (j + 1) + "行,内控扣分【" + deduction + "】不能大于100！");
                        }else if (a<=0){
                            return R.error("第" + (j + 1) + "行,内控扣分【" + deduction + "】不能为0或负数！");
                        }
                        String remark = row.getCell(6).getStringCellValue();
                        if(StrUtil.chineseLen(remark) > 3000){
                            return R.error("第" + (j + 1) + "行,扣分原因【" + remark + "】字数超过最大长度，最多可输入1000个字！");
                        }
                        String deductionType = row.getCell(7).getStringCellValue();
                        if(yr.length()==4){
                            if(!deductionType.equals("年度扣分")){
                                return R.error("第" + (j + 1) + "行,日期填写的为年份，扣分类型【" + deductionType + "】不是年度扣分！");
                            }
                        }else if(yr.length()==7){
                            if(!deductionType.equals("月度扣分")){
                                return R.error("第" + (j + 1) + "行,日期填写的为月份，扣分类型【" + deductionType + "】不是月度扣分！");
                            }
                        }
//                        if(!deductionType.equals("月度扣分") || !deductionType.equals("月度扣分")){
//                            return R.error("第" + (j + 1) + "行,内控类型【" + deductionType + "】不是月度扣分或年度扣分！");
//                        }
                        //执行插入方法
                        pointDeductionEntity.setYr(yr);
                        pointDeductionEntity.setBranchId(orgId);
                        pointDeductionEntity.setBranchNm(orgNm);
                        pointDeductionEntity.setCustId(custId);
                        pointDeductionEntity.setCustNm(custNm);
                        Double deductionDouble = Double.valueOf(deduction);
                        pointDeductionEntity.setPointDeduction(deductionDouble);
                        pointDeductionEntity.setOperators(ShiroUtils.getUserEntity().getName());
                        pointDeductionEntity.setRemark(remark);
                        pointDeductionEntity.setDeductionType(deductionType);
                        for (int s = 0;s<deductionList.size();s++){
                           if (deductionList.get(s).getYr().equals(pointDeductionEntity.getYr()) && deductionList.get(s).getBranchId().equals(pointDeductionEntity.getBranchId()) &&
                           deductionList.get(s).getCustId().equals(pointDeductionEntity.getCustId())){
                               deductionList.remove(s);
                           }
                        }
                        deductionList.add(pointDeductionEntity);
                        pointDeductionDao.deletedata(pointDeductionEntity);
                        pointDeductionHis.setYr(yr);
                        pointDeductionHis.setBranchId(orgId);
                        pointDeductionHis.setBranchNm(orgNm);
                        pointDeductionHis.setCustId(custId);
                        pointDeductionHis.setCustNm(custNm);
                        pointDeductionHis.setDeductionType(deductionType);
                        pointDeductionHis.setPointDeductionAfter(deductionDouble);
                        pointDeductionHis.setRemarkAfter(deduction);
                        pointDeductionHis.setOperators(ShiroUtils.getUserEntity().getName());
                        pointDeductionHis.setLogo("批量插入");
                        pointDeductionHisList.add(pointDeductionHis);
                    }
                }
            }
            pointDeductionDao.insertDeductionList(deductionList);
            pointDeductionHisDao.insertHisList(pointDeductionHisList);
        }catch (IOException e){
            e.printStackTrace();
        }finally{
            try {
                workbook.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        }
        return R.ok("导入成功");
    }

    @Override
    @DataSource(value = "second")
    public void insertSingleData(PointDeductionEntity pointDeductionEntity) {
        String logo = "新增";
        Map map = new HashMap();
        String orgId = pointDeductionEntity.getBranchId();
        System.out.println(orgId);
        map.put("orgId",orgId);
        String orgNm = pointDeductionDao.queryOrgNm(map);
        SysUserEntity userEntity = ShiroUtils.getUserEntity();
        pointDeductionEntity.setOperators(userEntity.getName());
        pointDeductionEntity.setBranchNm(orgNm);
        System.out.println(pointDeductionEntity);
        pointDeductionDao.deletedata(pointDeductionEntity);
        pointDeductionDao.insertSingleData(pointDeductionEntity);
        List<PointDeductionEntity> pointDeductionEntitiesBefo =new ArrayList<>();
        List<PointDeductionEntity> pointDeductionEntitiesAfter =new ArrayList<>();
        pointDeductionEntitiesAfter.add(pointDeductionEntity);
        record(pointDeductionEntitiesBefo,pointDeductionEntitiesAfter,logo);
    }

    @Override
    @DataSource(value = "second")
    public void updatedata(PointDeductionEntity pointDeductionEntity) {
        String logo = "修改";
        SysUserEntity userEntity = ShiroUtils.getUserEntity();
        pointDeductionEntity.setOperators(userEntity.getName());
        List<PointDeductionEntity> pointDeductionEntityBefo =pointDeductionDao.queryYearHis(pointDeductionEntity);
        pointDeductionDao.deletedata(pointDeductionEntity);
        pointDeductionDao.insertSingleData(pointDeductionEntity);
        List<PointDeductionEntity> pointDeductionEntityAfter =pointDeductionDao.queryYearHis(pointDeductionEntity);
        record(pointDeductionEntityBefo,pointDeductionEntityAfter,logo);
    }

    @Override
    @DataSource(value = "second")
    public void deletedata(PointDeductionEntity pointDeductionEntity) {
        String logo = "删除";
        List<PointDeductionEntity> pointDeductionEntityBefo =pointDeductionDao.queryYearHis(pointDeductionEntity);
        List<PointDeductionEntity> pointDeductionEntityAfter =new ArrayList<>();
        record(pointDeductionEntityBefo,pointDeductionEntityAfter,logo);
        pointDeductionDao.deletedata(pointDeductionEntity);
    }

    @Override
    @DataSource(value = "second")
    public PointDeductionEntity getDataByYrBraIdCusId(Map<String, Object> params) {
        PointDeductionEntity dataByYrBraIdCusId = pointDeductionDao.getDataByYrBraIdCusId(params);
        return dataByYrBraIdCusId;
    }
    @DataSource(value = "second")
    public void record(List<PointDeductionEntity> pointDeductionsBefo, List<PointDeductionEntity> pointDeductionsAfter, String logo){
        PointDeductionHistoryEntity pointDeductionHis = new PointDeductionHistoryEntity();
        if (pointDeductionsBefo.size()>0){
            pointDeductionHis.setYr(pointDeductionsBefo.get(0).getYr());
            pointDeductionHis.setBranchId(pointDeductionsBefo.get(0).getBranchId());
            pointDeductionHis.setBranchNm(pointDeductionsBefo.get(0).getBranchNm());
            pointDeductionHis.setCustId(pointDeductionsBefo.get(0).getCustId());
            pointDeductionHis.setCustNm(pointDeductionsBefo.get(0).getCustNm());
            pointDeductionHis.setPointDeductionBefo(pointDeductionsBefo.get(0).getPointDeduction());
            pointDeductionHis.setRemarkBefo(pointDeductionsBefo.get(0).getRemark());
            pointDeductionHis.setDeductionType(pointDeductionsBefo.get(0).getDeductionType());
        }
        if (pointDeductionsAfter.size()>0){
            pointDeductionHis.setYr(pointDeductionsAfter.get(0).getYr());
            pointDeductionHis.setBranchId(pointDeductionsAfter.get(0).getBranchId());
            pointDeductionHis.setBranchNm(pointDeductionsAfter.get(0).getBranchNm());
            pointDeductionHis.setCustId(pointDeductionsAfter.get(0).getCustId());
            pointDeductionHis.setCustNm(pointDeductionsAfter.get(0).getCustNm());
            pointDeductionHis.setPointDeductionAfter(pointDeductionsAfter.get(0).getPointDeduction());
            pointDeductionHis.setRemarkAfter(pointDeductionsAfter.get(0).getRemark());
            pointDeductionHis.setDeductionType(pointDeductionsAfter.get(0).getDeductionType());
        }
        pointDeductionHis.setOperators(ShiroUtils.getUserEntity().getName());
        pointDeductionHis.setLogo(logo);
        pointDeductionHisDao.insertHis(pointDeductionHis);
    }
}
