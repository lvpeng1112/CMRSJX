package com.beyondsoft.modules.newDeduction.dao;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.newDeduction.entity.PointDeductionHistoryEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
@Mapper
@Repository("PointDeductionHistoryDao")
public interface PointDeductionHistoryDao extends BaseMapper<PointDeductionHistoryEntity>{
    void insertHis(PointDeductionHistoryEntity pointDeductionHis);
    void insertHisList(List<PointDeductionHistoryEntity> list);
    List<PointDeductionHistoryEntity> querytHisData(Map<String, Object> params);
    List<Map<String, Object>> queryDictDetail(Map<String, Object> params);
    int querytHisDataNb(Map<String, Object> params);
}
