package com.beyondsoft.modules.newDeduction.service;
import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.modules.newDeduction.entity.PointDeductionHistoryEntity;

import java.util.List;
import java.util.Map;
public interface PointDeductionHistoryService extends IService<PointDeductionHistoryEntity>{

    List<PointDeductionHistoryEntity> querytHisData(Map<String, Object> params);

    int querytHisDataNb(Map<String, Object> params);
}
