package com.beyondsoft.modules.home.controller;

import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.ftp.dao.FtpLoanConstConfigDao;
import com.beyondsoft.modules.home.service.HomeService;
import com.beyondsoft.modules.sys.entity.SysUserEntity;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

/**
 * 主页
 *
 * @author luochao 2021-07-08
 */
@RestController
@RequestMapping("/home")
public class HomeController {

    @Autowired(required = false)
    private HomeService homeService;

    @Autowired(required = false)
    private FtpLoanConstConfigDao ftpLoanConstConfigDao;

    public SysUserEntity getUser() {
        return (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
    }

    public Long getUserId() {
        return getUser().getUserId();
    }

    /**
     * 查询基本信息
     */
    @RequestMapping("/informationList")
    public R informationList(@RequestParam Map<String, Object> params) {
        //当月绩效
        String myPerformance = "";
        SimpleDateFormat formatter= new SimpleDateFormat("yyyyMM");
        Date date = new Date();
        //当前系统年份
        String now = formatter.format(date);
        //查询机构信息
        String initCode = (String) params.get("initCode");
        //当前用户是否是考核员 0：是 1：否
        String isKhy = (String) params.get("isKhy");
        //当前用户是否是团队长 0：是 1：否
        String isTdz = (String) params.get("isTdz");
        if (initCode != null || initCode != "") {
            params.put("mngId", initCode);
        } else {
            params.put("mngId", "-99999");
        }
        String myOrgNm = homeService.queryMyOrgNm(params);
        if (myOrgNm == null || myOrgNm == "") {
            myOrgNm = "暂无机构";
        }
        //查询跑批时间
        String nowData = ftpLoanConstConfigDao.queryCurdate();
        //查询团队负责人或者客户经理当月绩效
        Map map = new HashMap();
        map.put("QRY_MTH",now);//当前月份
        map.put("ORG_P",initCode);//所属机构
        map.put("CUST_MNGR_ID",getUser().getUsername());//登录用户名
        map.put("isTdz",isTdz);//是否是团队长
        if("0".equals(isKhy) && !"0".equals("isTdz")){
            myPerformance = "0";
        }else{
            myPerformance = homeService.queryMyPerformance(map);
        }
        return R.ok().put("myOrgNm", myOrgNm).put("nowData", nowData).put("myPerformance",myPerformance);
    }

    /**
     * 指标echarts图
     */
    @RequestMapping("/indexTargetList")
    public R indexTargetList(@RequestParam Map<String, Object> params) {
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy");
        SimpleDateFormat format= new SimpleDateFormat("yyyy-MM");
        Date date = new Date();
        //当前系统年份
        String now = formatter.format(date);
        //当前系统月份
        String nowMth = format.format(date);
       //指标id
        String indexId = (String) params.get("indexId");
        //登录人员机构编号
        String orgId = getUser().getOrgId();
        //当前用户是否是考核员 0：是 1：否
        String isKhy = (String) params.get("isKhy");
        //当前用户是否是团队长 0：是 1：否
        String isTdz = (String) params.get("isTdz");
        params.put("YR",now);
        params.put("ORG_ID",orgId);
        params.put("indexId",indexId);
        //如果不是团队长，则查询本人的指标目标值
        if("1".equals(isTdz)){
            params.put("CUST_MNGR_ID",getUser().getUsername());
        }else if("0".equals(isTdz)){//如果是团队长，则团队长指标目标值是自己团队下面所有客户经理指标目标的总和
//            Map map = new HashMap();
            params.put("LEADER_ID",getUser().getUsername());
            params.put("SETUP_DT",nowMth);
            params.put("CH_ORG_ID",orgId);
            //查询团队长所属团队所有团队成员编号
//            List<String> list = homeService.queryTeamEmployeeCode(map);
//            params.put("list",list);//团队成员编号集合
        }
        //查询指标目标值
        List<Map<String, Object>> tagerValue = homeService.queryIndexTagerValue(params);
        List<String> dataList = new ArrayList<>();//日期集合
        List<String> currValList = new ArrayList<>();//当前值集合
        List<String> targetValList = new ArrayList<>();//目标值集合
        tagerValue.remove(null);//移除null元素
        if(!tagerValue.isEmpty()){//集合元素不为空
            for (int i = 0; i < tagerValue.size(); i++) {
                if("CP1400016".equals(indexId)){//存款日均余额
                    targetValList.add(tagerValue.get(i).get("DEPOSIT_DT_AVG_BAL_1").toString());
                    targetValList.add(tagerValue.get(i).get("DEPOSIT_DT_AVG_BAL_2").toString());
                    targetValList.add(tagerValue.get(i).get("DEPOSIT_DT_AVG_BAL_3").toString());
                    targetValList.add(tagerValue.get(i).get("DEPOSIT_DT_AVG_BAL_4").toString());
                    targetValList.add(tagerValue.get(i).get("DEPOSIT_DT_AVG_BAL_5").toString());
                    targetValList.add(tagerValue.get(i).get("DEPOSIT_DT_AVG_BAL_6").toString());
                    targetValList.add(tagerValue.get(i).get("DEPOSIT_DT_AVG_BAL_7").toString());
                    targetValList.add(tagerValue.get(i).get("DEPOSIT_DT_AVG_BAL_8").toString());
                    targetValList.add(tagerValue.get(i).get("DEPOSIT_DT_AVG_BAL_9").toString());
                    targetValList.add(tagerValue.get(i).get("DEPOSIT_DT_AVG_BAL_10").toString());
                    targetValList.add(tagerValue.get(i).get("DEPOSIT_DT_AVG_BAL_11").toString());
                    targetValList.add(tagerValue.get(i).get("DEPOSIT_DT_AVG_BAL_12").toString());
                }else if("CP0400017".equals(indexId)){//贷款日均余额
                    targetValList.add(tagerValue.get(i).get("LOAN_DT_AVG_BAL_1").toString());
                    targetValList.add(tagerValue.get(i).get("LOAN_DT_AVG_BAL_2").toString());
                    targetValList.add(tagerValue.get(i).get("LOAN_DT_AVG_BAL_3").toString());
                    targetValList.add(tagerValue.get(i).get("LOAN_DT_AVG_BAL_4").toString());
                    targetValList.add(tagerValue.get(i).get("LOAN_DT_AVG_BAL_5").toString());
                    targetValList.add(tagerValue.get(i).get("LOAN_DT_AVG_BAL_6").toString());
                    targetValList.add(tagerValue.get(i).get("LOAN_DT_AVG_BAL_7").toString());
                    targetValList.add(tagerValue.get(i).get("LOAN_DT_AVG_BAL_8").toString());
                    targetValList.add(tagerValue.get(i).get("LOAN_DT_AVG_BAL_9").toString());
                    targetValList.add(tagerValue.get(i).get("LOAN_DT_AVG_BAL_10").toString());
                    targetValList.add(tagerValue.get(i).get("LOAN_DT_AVG_BAL_11").toString());
                    targetValList.add(tagerValue.get(i).get("LOAN_DT_AVG_BAL_12").toString());
                }else if("CP2400014".equals(indexId)){//FTP净收入
                    targetValList.add(tagerValue.get(i).get("NET_INCM_1").toString());
                    targetValList.add(tagerValue.get(i).get("NET_INCM_2").toString());
                    targetValList.add(tagerValue.get(i).get("NET_INCM_3").toString());
                    targetValList.add(tagerValue.get(i).get("NET_INCM_4").toString());
                    targetValList.add(tagerValue.get(i).get("NET_INCM_5").toString());
                    targetValList.add(tagerValue.get(i).get("NET_INCM_6").toString());
                    targetValList.add(tagerValue.get(i).get("NET_INCM_7").toString());
                    targetValList.add(tagerValue.get(i).get("NET_INCM_8").toString());
                    targetValList.add(tagerValue.get(i).get("NET_INCM_9").toString());
                    targetValList.add(tagerValue.get(i).get("NET_INCM_10").toString());
                    targetValList.add(tagerValue.get(i).get("NET_INCM_11").toString());
                    targetValList.add(tagerValue.get(i).get("NET_INCM_12").toString());
                }else if("CP2400021".equals(indexId)){//存款新开户数
                    targetValList.add(tagerValue.get(i).get("DEPOSIT_NEW_OPEN_NUM_1").toString());
                    targetValList.add(tagerValue.get(i).get("DEPOSIT_NEW_OPEN_NUM_2").toString());
                    targetValList.add(tagerValue.get(i).get("DEPOSIT_NEW_OPEN_NUM_3").toString());
                    targetValList.add(tagerValue.get(i).get("DEPOSIT_NEW_OPEN_NUM_4").toString());
                    targetValList.add(tagerValue.get(i).get("DEPOSIT_NEW_OPEN_NUM_5").toString());
                    targetValList.add(tagerValue.get(i).get("DEPOSIT_NEW_OPEN_NUM_6").toString());
                    targetValList.add(tagerValue.get(i).get("DEPOSIT_NEW_OPEN_NUM_7").toString());
                    targetValList.add(tagerValue.get(i).get("DEPOSIT_NEW_OPEN_NUM_8").toString());
                    targetValList.add(tagerValue.get(i).get("DEPOSIT_NEW_OPEN_NUM_9").toString());
                    targetValList.add(tagerValue.get(i).get("DEPOSIT_NEW_OPEN_NUM_10").toString());
                    targetValList.add(tagerValue.get(i).get("DEPOSIT_NEW_OPEN_NUM_11").toString());
                    targetValList.add(tagerValue.get(i).get("DEPOSIT_NEW_OPEN_NUM_12").toString());
                }else{//贷款管户户数
                    targetValList.add(tagerValue.get(i).get("LOAN_MNG_CUST_CUST_NUM_1").toString());
                    targetValList.add(tagerValue.get(i).get("LOAN_MNG_CUST_CUST_NUM_2").toString());
                    targetValList.add(tagerValue.get(i).get("LOAN_MNG_CUST_CUST_NUM_3").toString());
                    targetValList.add(tagerValue.get(i).get("LOAN_MNG_CUST_CUST_NUM_4").toString());
                    targetValList.add(tagerValue.get(i).get("LOAN_MNG_CUST_CUST_NUM_5").toString());
                    targetValList.add(tagerValue.get(i).get("LOAN_MNG_CUST_CUST_NUM_6").toString());
                    targetValList.add(tagerValue.get(i).get("LOAN_MNG_CUST_CUST_NUM_7").toString());
                    targetValList.add(tagerValue.get(i).get("LOAN_MNG_CUST_CUST_NUM_8").toString());
                    targetValList.add(tagerValue.get(i).get("LOAN_MNG_CUST_CUST_NUM_9").toString());
                    targetValList.add(tagerValue.get(i).get("LOAN_MNG_CUST_CUST_NUM_10").toString());
                    targetValList.add(tagerValue.get(i).get("LOAN_MNG_CUST_CUST_NUM_11").toString());
                    targetValList.add(tagerValue.get(i).get("LOAN_MNG_CUST_CUST_NUM_12").toString());
                }
            }
        }
        //查询指标当前值
        //系统当前年份一月
        String startDt = now + "01";
        //系统当前年份十二月
        String endDt = now + "12";
        Map currMap = new HashMap();
        currMap.put("isTdz",isTdz);
        currMap.put("startDt",startDt);
        currMap.put("endDt",endDt);
        currMap.put("ORG_ID",orgId);
        currMap.put("indexId",indexId);
        //如果不是团队长，则查询本人的指标目标值
        if("1".equals(isTdz)){
            currMap.put("CUST_MNGR_ID",getUser().getUsername());
        }else if("0".equals(isTdz)){//如果是团队长
            currMap.put("TEAM_ID",getUser().getUsername());
        }
        List<Map<String, Object>> currentValue = homeService.queryIndexCurrentValue(currMap);
        currentValue.remove(null);//移除null元素
        if(!currentValue.isEmpty()){//集合元素不为空
            for (int i = 0; i <currentValue.size() ; i++) {
                if("CP1400016".equals(indexId)){//存款日均余额
                    dataList.add(currentValue.get(i).get("qry_mth").toString());
                    currValList.add(currentValue.get(i).get("dt_avg_dep_bal").toString());
                }else if("CP0400017".equals(indexId)){//贷款日均余额
                    dataList.add(currentValue.get(i).get("qry_mth").toString());
                    currValList.add(currentValue.get(i).get("dt_avg_loan_bal").toString());
                }else if("CP2400014".equals(indexId)){//FTP净收入
                    dataList.add(currentValue.get(i).get("qry_mth").toString());
                    currValList.add(currentValue.get(i).get("ftp_incm").toString());
                }else if("CP2400021".equals(indexId)){//存款新开户数
                    dataList.add(currentValue.get(i).get("qry_mth").toString());
                    currValList.add(currentValue.get(i).get("dep_open_num").toString());
                }else{//贷款管户户数
                    dataList.add(currentValue.get(i).get("qry_mth").toString());
                    currValList.add(currentValue.get(i).get("loan_cust_num").toString());
                }
            }
        }
        String[] dataArr = dataList.toArray(new String[dataList.size()]);//日期数组
        String[] currValArr = currValList.toArray(new String[currValList.size()]);//当前值数组
        String[] targetValArr = targetValList.toArray(new String[targetValList.size()]);//目标值数组
        return R.ok().put("targetValArr",targetValArr).put("dataArr",dataArr).put("currValArr",currValArr);
    }
}
