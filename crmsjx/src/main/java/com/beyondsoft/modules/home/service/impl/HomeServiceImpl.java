package com.beyondsoft.modules.home.service.impl;

import com.beyondsoft.datasource.annotation.DataSource;
import com.beyondsoft.modules.home.dao.HomeDao;
import com.beyondsoft.modules.home.service.HomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("homeService")
public class HomeServiceImpl  implements HomeService {

    @Autowired(required = false)
    private HomeDao homeDao;

    @Override
    public List<Map<String,Object>> queryIndex(Map map) {
        return homeDao.queryIndex(map);
    }

    @Override
    public String queryMyOrgNm(Map map) {
        return homeDao.queryMyOrgNm(map);
    }

    @Override
    @DataSource(value = "second")
    public String queryMyPerformance(Map map) {
        return homeDao.queryMyPerformance(map);
    }

    @Override
    public List<Map<String, Object>> queryIndexTagerValue(Map map) {
        return homeDao.queryIndexTagerValue(map);
    }

    @Override
    @DataSource(value = "second")
    public List<Map<String, Object>> queryIndexCurrentValue(Map map) {
        return homeDao.queryIndexCurrentValue(map);
    }

    @Override
    public List<String> queryTeamEmployeeCode(Map map) {
        return homeDao.queryTeamEmployeeCode(map);
    }
}
