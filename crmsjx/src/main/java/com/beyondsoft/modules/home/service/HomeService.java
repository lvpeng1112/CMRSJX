package com.beyondsoft.modules.home.service;

import java.util.List;
import java.util.Map;

public interface HomeService{
    //查询我的指标
    List<Map<String, Object>> queryIndex(Map map);
    //查询我的机构名称
    String queryMyOrgNm(Map map);
    //查询我的当月绩效
    String queryMyPerformance(Map map);
    //查询指标目标值
    List<Map<String,Object>> queryIndexTagerValue(Map map);
    //查询指标当前值
    List<Map<String,Object>> queryIndexCurrentValue(Map map);
    //查询团队长所属团队的所有团队客户经理的编号
    List<String> queryTeamEmployeeCode(Map map);
}
