package com.beyondsoft.modules.grade.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.grade.entity.MonthlyCheckGrade;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;


public interface MonthlyCheckGradeService  extends IService<MonthlyCheckGrade> {
      List<MonthlyCheckGrade> queryGradeListInfo(Map<String, Object> params);

      int queryGradeListNb(Map<String, Object> params);

      R batchImport(String fileName, MultipartFile file, String org, String isKhy) throws Exception;
}
