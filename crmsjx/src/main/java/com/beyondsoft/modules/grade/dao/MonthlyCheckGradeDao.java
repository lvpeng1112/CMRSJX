package com.beyondsoft.modules.grade.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.beyondsoft.modules.grade.entity.MonthlyCheckGrade;
import com.beyondsoft.modules.aofp.entity.Town;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository("MonthlyCheckGradeDao")
public interface MonthlyCheckGradeDao extends BaseMapper<MonthlyCheckGrade> {
    /**
     * 查询村镇银行编号和名称
     * @return  List<Town>
     */
    @Select("select b.chdeptcode chdeptcode ,b.chdeptname  chdeptname from NPMM.C_MNG_CHECK_BASE_INFO a inner JOIN npmm.DEPARTMENT b ON a.ORG_ID = b.CHDEPTCODE\n" +
            "GROUP  BY  b.chdeptcode  ,b.chdeptname ")
    List<Town> queryChadDress();

    /**
     * 查询所有的村镇银行编号
     * @return
     */
    @Select("select chdeptcode, chdeptname  from npmm.DEPARTMENT")
    List<Town> queryAllChadDress();
    List<MonthlyCheckGrade> queryByYr(MonthlyCheckGrade checkGrade);

    List<MonthlyCheckGrade> queryAllChdetCode();

    List<String> queryAllEmp();

    List<MonthlyCheckGrade> queryGradeListInfo (Map<String,Object> reqPara);

    int queryGradeListNb(Map reqPara);

    int checkOrgId(Map<String, Object> params);

    String queryOrgNm(Map<String, Object> params);

    void deleteGradeList(MonthlyCheckGrade checkGrade);

    void insertGradeList(List<MonthlyCheckGrade> list);
}
