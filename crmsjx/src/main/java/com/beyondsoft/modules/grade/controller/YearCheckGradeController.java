package com.beyondsoft.modules.grade.controller;

import com.alibaba.druid.util.StringUtils;
import com.beyondsoft.common.utils.DateUtils;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.common.utils.ShiroUtils;
import com.beyondsoft.modules.grade.dao.YearCheckGradeDao;
import com.beyondsoft.modules.grade.entity.YearCheckGrade;
import com.beyondsoft.modules.grade.service.YearCheckGradeService;
import com.beyondsoft.modules.sys.entity.SysUserEntity;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;

@RestController
@RequestMapping("/grade/yearCheckGrade")
public class YearCheckGradeController {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    YearCheckGradeService yearCheckGradeService;
    @Autowired
    YearCheckGradeDao yearCheckGradeDao;

    @RequestMapping("/list")
    @ApiOperation("分页查询")
    public R list(@RequestParam Map<String, Object> params) {
        Map map = new HashMap();
        if(null == params){
            return R.error("入参为空");
        }
        if(StringUtils.isEmpty((String) params.get("page")) && StringUtils.isEmpty((String) params.get("limit"))){
            return R.error("缺少必传分页参数");
        }
        List<YearCheckGrade> gradeList = yearCheckGradeService.queryGradeListInfo(params);
        int i = yearCheckGradeService.queryGradeListNb(params);
        return R.ok().put("gradeList",gradeList).put("count",i);
    }

    @RequestMapping("/importExcel")
    @ApiOperation("导入excel")
    public R importExcel(@RequestParam(value = "file") MultipartFile file,String org, String isKhy) throws Exception {

        String fileName = file.getOriginalFilename();
        logger.info("导入表格文件为:{}",fileName);

        return yearCheckGradeService.batchImport(fileName, file, org, isKhy);
    }
}