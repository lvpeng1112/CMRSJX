package com.beyondsoft.modules.grade.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.beyondsoft.common.utils.DateUtils;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.common.utils.ShiroUtils;
import com.beyondsoft.datasource.annotation.DataSource;
import com.beyondsoft.modules.grade.entity.YearCheckGrade;
import com.beyondsoft.modules.grade.dao.YearCheckGradeDao;
import com.beyondsoft.modules.grade.service.YearCheckGradeService;
import com.beyondsoft.modules.aofp.utils.Utils;
import com.beyondsoft.utils.DateUtil;
import com.beyondsoft.utils.StrUtil;
import com.microsoft.schemas.office.visio.x2012.main.CellType;
import net.bytebuddy.asm.Advice;
import net.bytebuddy.implementation.bytecode.Throw;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.*;

@Service
public class YearCheckGradeServiceImpl extends ServiceImpl<YearCheckGradeDao, YearCheckGrade> implements YearCheckGradeService {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    YearCheckGradeDao yearCheckGradeDao;

    @Override
    @DataSource(value = "second")
    public List<YearCheckGrade> queryGradeListInfo(Map<String, Object> params){
        int pageNo = Integer.parseInt((String) params.get("page"));
        int pageCount = Integer.parseInt((String)params.get("limit"));
        int offset = ((pageNo - 1) * pageCount);
        params.put("pageCountAndOffset", pageCount + (pageNo - 1) * pageCount);
        params.put("offset", offset);
        return yearCheckGradeDao.queryGradeListInfo(params);
    }

    @Override
    @DataSource(value = "second")
    public int queryGradeListNb(Map<String, Object> params){
        int nb = yearCheckGradeDao.queryGradeListNb(params);
        return nb;
    }

    @Override
    @DataSource(value = "second")
    public R batchImport(String fileName, MultipartFile file, String org, String isKhy) {
        Map map = new HashMap();
        Workbook workbook = null;
        if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
            return R.error("上传格式不正确");
        }
        String extString = fileName.substring(fileName.lastIndexOf("."));

        List<YearCheckGrade> gradeList = new ArrayList<>();
        try {
            InputStream inputStream = file.getInputStream();
            if (".xls".equals(extString)) {
                HSSFWorkbook wb = new HSSFWorkbook(inputStream);
                workbook = wb;
            } else if (".xlsx".equals(extString)) {
                XSSFWorkbook wb = new XSSFWorkbook(inputStream);
                workbook = wb;
            }
            int sheets = workbook.getNumberOfSheets();
            for (int i = 0; i < sheets; i++) {
                Sheet sheetAt = workbook.getSheetAt(i);
                //获取多少行
                int lastRowNum = sheetAt.getLastRowNum();
                for (int j = 1; j <= lastRowNum; j++) {
                    Row row = sheetAt.getRow(j);
                    if (row != null) {
                        YearCheckGrade checkGrade = new YearCheckGrade();
                        for (Cell cell : row) {
                            cell.setCellType(Cell.CELL_TYPE_STRING);
                        }
                        // 必须输入项check
                        if (row.getCell(0) == null || row.getCell(0).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，年度列没有数据");
                        }else if (row.getCell(1) == null || row.getCell(1).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，机构编号列没有数据");
                        }else if (row.getCell(3) == null || row.getCell(3).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，客户经理编号列没有数据");
                        }else if (row.getCell(4) == null || row.getCell(4).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，客户经理名称列没有数据");
                        }else if (row.getCell(5) == null || row.getCell(5).getStringCellValue() == "") {
                            return R.error("第" + (j + 1) + "行，考核评分列没有数据");
                        }
                        // 月度日期格式check
                        String yr = row.getCell(0).getStringCellValue();
                        String inputStr = yr + "0101";
                        if (!DateUtil.isDate(inputStr)) {
                            return R.error("第" + (j + 1) + "行,年度【" + yr + "】不是正确的日期格式！例:YYYY");
                        }

                        // 机构编号存在check
                        String orgId = row.getCell(1).getStringCellValue();
                        Map<String, Object> orgParams = new HashMap<>();
                        orgParams.put("orgId", orgId);
                        orgParams.put("org", org);
                        orgParams.put("isKhy", isKhy);
                        int count = yearCheckGradeDao.checkOrgId(orgParams);
                        if (count == 0) {
                            return R.error("第" + (j + 1) + "行,您没有权限导入其他机构的数据，机构编号【" + orgId + "】！");
                        }
                        String orgNm = yearCheckGradeDao.queryOrgNm(orgParams);

                        // 客户经理编号长度check
                        String custId = row.getCell(3).getStringCellValue();
                        if (custId.length() > 90) {
                            return R.error("第" + (j + 1) + "行,客户经理编号【" + custId + "】长度不能超过90位！");
                        }
                        // 客户经理名称长度check
                        String custNm = row.getCell(4).getStringCellValue();
                        if (StrUtil.chineseLen(custNm) > 300) {
                            return R.error("第" + (j + 1) + "行,客户名称【" + custNm + "】长度不能超过300位！");
                        }
                        // 考核评分长度check
                        String grade = row.getCell(5).getStringCellValue();
                        if (!StrUtil.isNumeric(grade)) {
                            return R.error("第" + (j + 1) + "行,考核评分【" + grade + "】必须为数字！");
                        }
                        if (!StrUtil.checkNumLen(grade, 18 , 2)) {
                            return R.error("第" + (j + 1) + "行,考核评分【" + grade + "】不能超过18位整数2位小数！");
                        }

                        //执行插入方法
                        checkGrade.setYr(yr);
                        checkGrade.setBranchId(orgId);
                        checkGrade.setBranchNm(orgNm);
                        checkGrade.setCustId(custId);
                        checkGrade.setCustNm(custNm);
                        checkGrade.setCheckGrade(grade);
                        checkGrade.setOperators(ShiroUtils.getUserEntity().getUsername());
                        gradeList.add(checkGrade);
                        yearCheckGradeDao.deleteGradeList(checkGrade);
                    }
                }
            }
            yearCheckGradeDao.insertGradeList(gradeList);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                workbook.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return R.ok("导入成功");
    }
}
