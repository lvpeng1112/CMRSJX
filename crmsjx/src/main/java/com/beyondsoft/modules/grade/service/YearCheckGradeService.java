package com.beyondsoft.modules.grade.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.beyondsoft.common.utils.R;
import com.beyondsoft.modules.grade.entity.YearCheckGrade;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;


public interface YearCheckGradeService  extends IService<YearCheckGrade> {
      List<YearCheckGrade> queryGradeListInfo(Map<String, Object> params);

      int queryGradeListNb(Map<String, Object> params);

      R batchImport(String fileName, MultipartFile file, String org, String isKhy) throws Exception;
}
