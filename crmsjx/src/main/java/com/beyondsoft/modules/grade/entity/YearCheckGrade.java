package com.beyondsoft.modules.grade.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
@ApiModel(value = "内控年度考核评分导入表")
@TableName("C_YEAR_CHECK_GRADE")
@Data
public class YearCheckGrade implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "年度")
    private String yr;

    @ApiModelProperty(value = "机构编号")
    private String branchId;

    @ApiModelProperty(value = "机构名称")
    private String branchNm;

    @ApiModelProperty(value = "客户经理编号")
    private String custId;

    @ApiModelProperty(value = "客户经理名称")
    private String custNm;

    @ApiModelProperty(value = "考核评分")
    private String checkGrade;

    @ApiModelProperty(value = "操作员")
    private String operators;

    @ApiModelProperty(value = "操作时间")
    private String operateDate;

}
