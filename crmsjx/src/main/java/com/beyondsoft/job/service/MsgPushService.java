package com.beyondsoft.job.service;


import com.alibaba.fastjson.JSONObject;
import com.beyondsoft.cache.PropCache;
import com.beyondsoft.utils.EncryptUtils;
import com.beyondsoft.utils.HttpKit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class MsgPushService {

    private Logger logger = LoggerFactory.getLogger(getClass());

    public Map<String,String> sendTrackMsg( Map<String, Object> reqPara){

        Map<String,String> map = new HashMap<>();
        //获取userId bankId pageId
        String userId = (String) reqPara.get("userId");
        String bankId = (String) reqPara.get("bankId");
        String pageId = (String) reqPara.get("pageId");

        String EncKey = PropCache.getCacheInfo("defaultweixinpushEncKey");
        String EncVec = PropCache.getCacheInfo("defaultweixinpushEncVec");


        //根据银行配置配置推送报文
        JSONObject data = new JSONObject();
        String templateId = PropCache.getCacheInfo(bankId+"weixinpush"+"templateId");
        String title =PropCache.getCacheInfo(bankId+"weixin"+pageId+"Push"+"pushTitle");
        String keyword1=PropCache.getCacheInfo(bankId+"weixin"+pageId+"Push"+"keyword1");
        String keyword2=PropCache.getCacheInfo(bankId+"weixin"+pageId+"Push"+"keyword2");
        String keyword3=PropCache.getCacheInfo(bankId+"weixin"+pageId+"Push"+"keyword3");
        String remark=PropCache.getCacheInfo(bankId+"weixin"+pageId+"Push"+"remark");
        String url = PropCache.getCacheInfo(bankId+"weixin"+pageId+"Push"+"url");
        String pushUrl =PropCache.getCacheInfo("beehive"+"weixinpush"+"pushUrl");

        //初始化标题
        JSONObject titleobj = new JSONObject();
        titleobj.put("value", title);
        titleobj.put("color", "#000000");

        //初始化业务内容1
        JSONObject keyword1obj = new JSONObject();
        keyword1obj.put("value", keyword1);
        keyword1obj.put("color", "#000000");

        //初始化业务内容2
        JSONObject keyword2obj = new JSONObject();
        keyword2obj.put("value", keyword2);
        keyword2obj.put("color", "#000000");

        //初始化业务内容3
        JSONObject keyword3obj = new JSONObject();
        keyword3obj.put("value", keyword3);
        keyword3obj.put("color", "#000000");

        //初始化备注
        JSONObject remarkobj = new JSONObject();
        remarkobj.put("value", remark);
        remarkobj.put("color", "#000000");

        data.put("first",titleobj);
        data.put("keyword1",keyword1obj);
        data.put("keyword2",keyword2obj);
        data.put("keyword3",keyword3obj);
        data.put("remark",remarkobj);

        //拼装推送报文
        JSONObject objReq = new JSONObject();
        objReq.put("touser", userId);
        objReq.put("template_id", templateId);
        objReq.put("topcolor", "#FFFFFF");
        objReq.put("data",data);
        //添加url
        objReq.put("url", url);

        //封装发送

        JSONObject sendObj = new JSONObject();
        sendObj.put("bankNum",bankId);
        sendObj.put("value",objReq);
        sendObj.put("toUser",userId);

        //转成String加密传输
        try{
            String sendStr = sendObj.toString();
            logger.info("beehive push: "+sendStr);
            sendStr = EncryptUtils.getInstance(EncKey,EncVec).encrypt(sendStr);
            Map<String,String> sendMap = new HashMap<>();
            sendMap.put("data",sendStr);
            String rtn = HttpKit.get(pushUrl,sendMap);
            rtn = EncryptUtils.getInstance(EncKey,EncVec).decrypt(rtn);
            JSONObject rtnObj = JSONObject.parseObject(rtn);
            logger.info("beehive push: "+rtn);

            map.put("rtnMsg", (String) rtnObj.get("rtnMsg"));
            map.put("rtnCode", (String) rtnObj.get("rtnCode"));
            //调用微信推送
        }catch (Exception e){
            e.printStackTrace();
        }

        return map;

    }

  /* public static  void main(String args[]) throws Exception {

        String EncKey = "CupDataMobileBankESystem";
        String EncVec = "CUPD37MD";
        //String a="{\"touser\":\"ov9uis0IIjkdzZC4LpMATZUP4cko\",\"template_id\":\"Ghi_ukbClrYVy60gs_oWjgBtLlg16F3xLU9HVZqPil8\",\"topcolor\":\"#FFFFFF\",\"url\":\"https://trip.cupdata.com/ump/coreDispatcher.action?bankCode=6442&data=?bankCode=6442&data=WoKtHGgDtWRzwD%2BODFgoRZ8msGpHT0ezb%2F8Qmwgrcl9U8U2y3i9uZvHSLFa1JWix%2BAHvsfDWWVyC4dSoE9SFCQ8dMeEK4O9wOln9ffn%2FypQ4SduapCqrZ9PWGVLmHIeQsmMsF3R8Y%2Fu37V9UK0AvG%2Ba4yilTJ7BDkPVrajJZ%士\",\"color\":\"#000000\"},\"productType\":{\"value\":\"您尾号4394的信用卡最新交易信息\",\"color\":\"#000000\"},\"time\":{\"value\":\"2020年01月05日11时04分\",\"color\":\"#000000\"},\"type\":{\"value\":\"消费\",\"color\":\"#000000\"},\"number\":{\"value\":\"人民币350.00元\",\"color\":\"#000000\"},\"remark\":{\"value\":\"当前可用额度：490.64元\\n\\n恭喜您获得一次抽奖机会！点击查看“详情”单笔交易金额满99元（含）即可获得1次抽奖机会，每日限4次。月月刷好礼，越刷越惊喜！话费、视频会员、运动装备多种好礼任您挑选，详询公众号“福利”“权益服务”“月月刷好礼”活动。\\r\\n\",\"color\":\"#FF0000\"}}}";

        //根据银行配置配置推送报文
        String userId = "oqf5zs7S6zAy95t1ZRjUM6mu3Jtg";
        String bankId = "6403";
        JSONObject data = new JSONObject();

        //初始化标题
        JSONObject titleobj = new JSONObject();
        titleobj.put("value", "title");
        titleobj.put("color", "#000000");

        //初始化业务内容1
        JSONObject keyword1obj = new JSONObject();
        keyword1obj.put("value", "keyword1Info");
        keyword1obj.put("color", "#000000");

        //初始化业务内容2
        JSONObject keyword2obj = new JSONObject();
        keyword2obj.put("value", "keyword2Info");
        keyword2obj.put("color", "#000000");

        //初始化业务内容3
        JSONObject keyword3obj = new JSONObject();
        keyword3obj.put("value", "keyword2Info");
        keyword3obj.put("color", "#000000");

        //初始化备注
        JSONObject remarkobj = new JSONObject();
        remarkobj.put("value", "remark");
        remarkobj.put("color", "#000000");

        data.put("first",titleobj);
        data.put("keyword1",keyword1obj);
        data.put("keyword2",keyword2obj);
        data.put("keyword3",keyword3obj);
        data.put("remarkobj",remarkobj);

        //拼装推送报文
        JSONObject objReq = new JSONObject();
        objReq.put("touser", userId);
        //objReq.put("touser", "oqf5zs-3s-sUxY3HOrhRjBfNKesE");
        //objReq.put("template_id", PropCache.getCacheInfo(bankId+"|"+pageId));
        objReq.put("template_id", "RnmDk-JHgRkRezreKm2Iz4uW5a0tJEmU1mjHwXtdbkI");
        objReq.put("topcolor", "#FFFFFF");
        objReq.put("data",data);

        //添加url
        String url = "http://web.cupdata.com/weixin/";
        objReq.put("url", url);

        //封装发送

        JSONObject sendObj = new JSONObject();
        sendObj.put("bankNum",bankId);
        sendObj.put("value",objReq);
        sendObj.put("toUser","ov9uis0IIjkdzZC4LpMATZUP4cko");

       // JSONObject json=JSONObject.fromObject(recData);
        String pushUrl ="http://10.192.247.194:46002/wxpush/WxTempMsgPush";

        //转成String加密传输
        try{
            String sendStr = sendObj.toString();
            System.out.println(sendStr);
            sendStr = EncryptUtils.getInstance(EncKey,EncVec).encrypt(sendStr);
            System.out.println(sendStr);
            Map<String,String> sendMap = new HashMap<>();
            sendMap.put("data",sendStr);
            String rtn = HttpKit.get(pushUrl,sendMap);
            rtn = EncryptUtils.getInstance(EncKey,EncVec).decrypt(rtn);

            System.out.println(rtn);
            //调用微信推送
        }catch (Exception e){
            e.printStackTrace();
        }

    }*/

}
