package com.beyondsoft.job;

import com.beyondsoft.config.XxlJobConfig;
import com.beyondsoft.utils.DateUtil;
import com.beyondsoft.utils.JobUtils;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@JobHandler(value = "beehiveJobHandler")
public class BeehiveJobHandler extends IJobHandler {

    private static Logger log = LogManager.getLogger(XxlJobConfig.class);

    public ReturnT<String> execute(String param) throws Exception{
        log.info("XxlJob init");

        String startTime ="";
        String endTime ="";

        //定时任务时间12点05 18点05 00点05
        String currDate = DateUtil.getCurrDate("yyyyMMdd");
        String currTime = DateUtil.getCurrDate("HHmmss");
        String currHour = currTime.substring(0,1);
        if("00".equals(currHour)) {
            currDate = DateUtil.getPreDay("yyyyMMdd");
            startTime = "180000";
            endTime = "235959";
            log.info(currDate+"第三批");
        }
        if("12".equals(currHour)){
            startTime ="000000";
            endTime ="115959";
            log.info(currDate+"第一批");
        }
        if("18".equals(currHour)){
            startTime ="120000";
            endTime ="175959";
            log.info(currDate+"第二批");
        }

        //手动执行 读取param参数  格式：manual|startTime|endTime|currDate
        if(param.contains("manual")){
            String[] paramArray = param.split("\\|");
            startTime = paramArray[1];
            endTime = paramArray[2];
            currDate = paramArray[3];
        }

        JobUtils.jobUtils.lossTrack(startTime,endTime,currDate);
        return SUCCESS;
    }
}
