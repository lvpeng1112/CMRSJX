/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.beyondsoft;

import com.beyondsoft.cache.DealCache;
import com.beyondsoft.common.utils.ApplicationContextHelper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;


@SpringBootApplication
public class CrmsjxApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(CrmsjxApplication.class, args);
		init();
	}

	// 继承SpringBootServletInitializer 实现configure 方便打war 外部服务器部署。
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(CrmsjxApplication.class);
	}

	public static void init(){

		DealCache dealCache = (DealCache)ApplicationContextHelper.getBean("dealCache");
		dealCache.initProp();
	}

}